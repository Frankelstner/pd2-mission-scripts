Mission scripts converted into a text representation. The source data is equivalent to a Lua table.

The "levels raw" folder contains the results of the conversion where mission-script-specific 
fields like "class", "delay", "enabled", "trigger_times" have been given special treatment. 
Ids of mission script elements, statics and instances are usually replaced by their names; 
duplicate names are avoided by attaching another index at the end. 
Additionally, redundant parts have been stripped from path names.

The "levels" folder contains the result of a more sophisticated conversion. 
Unreachable or permanently disabled elements are removed. 
Elements (if possible) are inlined into others. 
Some elements are renamed if the old name matched the default name for that element type. 
A print function for each element type writes the text describing the actual effect of the element, 
incorporating the knowledge contained in lib\managers\mission. 
This conversion is prone to errors, so the results are less reliable than the ones in "levels raw". 
The print functions for certain elements also irreversibly remove some information.
But the files can be read more quickly.

The other folders contain .sequence_manager files converted to text. 
The paths have not been modified whatsoever, so you could merge them into your own file dump.
However, this means that the mission scripts refer to sequences with paths that do not exist.
The issue is easily resolved by just searching for the filename only, because those are unique
and unaffected by the shortened path names.