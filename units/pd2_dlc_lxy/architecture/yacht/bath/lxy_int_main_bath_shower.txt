unit:
	sequence 'enable_interaction':
		Enable interaction.
		Show graphic_group 'icons'.
	sequence 'disable_interaction':
		Disable interaction.
		Hide graphic_group 'icons'.
	sequence 'interact':
		Run sequence 'anim_open_door'.
	sequence 'anim_open_door':
		Hide graphic_group 'icons'.
		Disable interaction.
		Enable animation_group 'anim':
			from 0/30
			to 30/30
		Play audio 'bar_main_bath_shower_open_finished' at 'interact'.
	sequence 'anim_close_door':
		Enable animation_group 'anim':
			from 30/30
			speed -1
			to 0/30
		Play audio 'bar_main_bath_shower_close' at 'interact'.
