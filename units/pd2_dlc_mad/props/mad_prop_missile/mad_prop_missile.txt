unit:
	sequence 'anim_intro':
		Enable animation_group 'anim_missile':
			from 0/30
			to 135/30
		effect 'effects/payday2/particles/fire/fire_trail_missile':
			parent object( 'e_effect_01' )
			position v()
			store_id_list_var 'burning_id_list_1'
		effect 'effects/payday2/particles/fire/fire_trail_missile':
			parent object( 'e_effect_02' )
			position v()
			store_id_list_var 'burning_id_list_2'
		effect 'effects/payday2/particles/fire/fire_trail_missile':
			parent object( 'e_effect_03' )
			position v()
			store_id_list_var 'burning_id_list_3'
		effect 'effects/payday2/particles/smoke_trail/smoke_trail':
			parent object( 'e_effect_01' )
			position v()
			store_id_list_var 'smoke_id_list_1'
		effect 'effects/payday2/particles/smoke_trail/smoke_trail':
			parent object( 'e_effect_02' )
			position v()
			store_id_list_var 'smoke_id_list_2'
		effect 'effects/payday2/particles/smoke_trail/smoke_trail':
			parent object( 'e_effect_03' )
			position v()
			store_id_list_var 'smoke_id_list_3'
		Disable object 'g_box_04'.
		Run sequence 'kill_effects'. (DELAY 135/30)
		speed 0.5
	sequence 'kill_effects':
		stop_effect:
			id_list_var 'burning_id_list_1'
			instant True
		stop_effect:
			id_list_var 'burning_id_list_2'
			instant True
		stop_effect:
			id_list_var 'burning_id_list_3'
			instant True
		stop_effect:
			id_list_var 'smoke_id_list_1'
			instant True
		stop_effect:
			id_list_var 'smoke_id_list_2'
			instant True
		stop_effect:
			id_list_var 'smoke_id_list_3'
			instant True
		Hide graphic_group 'missiles'.
	sequence 'hide':
		Hide graphic_group 'missiles'.
		Disable object 'g_box_04'.
	sequence 'show':
		Show graphic_group 'missiles'.
		Enable object 'g_box_04'.
