unit:
	sequence 'anim_idle':
		animation_redirect 'idle'.
		Play audio 'sam_site_moving_sideways_loop' at 'snd'.
	sequence 'anim_power_down':
		animation_redirect 'power_down'.
		Play audio 'sam_site_interrupt' at 'snd'.
	sequence 'anim_fire_1':
		animation_redirect 'fire_1'.
		Run sequence 'int_seq_effects_1'.
		Run sequence 'int_seq_hit_missile_fire_1'.
		Play audio 'sam_site_fire_sequence' at 'snd'.
		Play audio 'sam_site_missilie_fly' at 'snd_missile_1'. (DELAY 10)
	sequence 'anim_fire_2':
		animation_redirect 'fire_2'.
		Run sequence 'int_seq_effects_2'.
		Run sequence 'int_seq_hit_missile_fire_2'.
		Play audio 'sam_site_fire_sequence' at 'snd'.
		Play audio 'sam_site_missilie_fly' at 'snd_missile_2'. (DELAY 10)
	sequence 'int_seq_effects_1': (DELAY 300/30)
		effect 'effects/payday2/particles/fire/fire_trail_missile':
			parent object( 'e_effect_missile_1' )
			position v()
			store_id_list_var 'burning_id_list_1'
		effect 'effects/payday2/particles/fire/fire_trail_missile':
			parent object( 'e_effect_missile_2' )
			position v()
			store_id_list_var 'burning_id_list_2'
		Disable body 'body_animated_missile_right'.
	sequence 'int_seq_effects_2': (DELAY 300/30)
		effect 'effects/payday2/particles/fire/fire_trail_missile':
			parent object( 'e_effect_missile_3' )
			position v()
			store_id_list_var 'burning_id_list_3'
		effect 'effects/payday2/particles/fire/fire_trail_missile':
			parent object( 'e_effect_missile_4' )
			position v()
			store_id_list_var 'burning_id_list_4'
		Disable body 'body_animated_missile_left'.
	sequence 'int_seq_hit_missile_fire_1': (DELAY 318/30)
		Disable object 'g_missile_1_lod0'.
		Disable object 'g_missile_1_lod1'.
		Disable object 'g_missile_1_lod2'.
		Disable object 'g_missile_2_lod0'.
		Disable object 'g_missile_2_lod1'.
		Disable object 'g_missile_2_lod2'.
		stop_effect:
			id_list_var 'burning_id_list_1'
			instant False
		stop_effect:
			id_list_var 'burning_id_list_2'
			instant False
	sequence 'int_seq_hit_missile_fire_2': (DELAY 318/30)
		Disable object 'g_missile_6_lod0'.
		Disable object 'g_missile_6_lod1'.
		Disable object 'g_missile_6_lod2'.
		Disable object 'g_missile_8_lod0'.
		Disable object 'g_missile_8_lod1'.
		Disable object 'g_missile_8_lod2'.
		stop_effect:
			id_list_var 'burning_id_list_3'
			instant False
		stop_effect:
			id_list_var 'burning_id_list_4'
			instant False
	sequence 'hide':
		Hide graphic_group 'g_sam'.
		Enable decal_mesh 'dm_glass_unbreakabal'.
		Enable decal_mesh 'dm_metal'.
		Enable decal_mesh 'dm_metal_missile'.
		Enable decal_mesh 'dm_metal_base'.
		Enable decal_mesh 'dm_rubber'.
		Disable body 'body_static'.
		Disable body 'body_animated_base'.
		Disable body 'body_animated_missile'.
		Disable body 'body_animated_missile_left'.
		Disable body 'body_animated_missile_right'.
	sequence 'show':
		Show graphic_group 'g_sam'.
		Enable decal_mesh 'dm_glass_unbreakabal'.
		Enable decal_mesh 'dm_metal'.
		Enable decal_mesh 'dm_metal_missile'.
		Enable decal_mesh 'dm_metal_base'.
		Enable decal_mesh 'dm_rubber'.
		Enable body 'body_static'.
		Enable body 'body_animated_base'.
		Enable body 'body_animated_missile'.
		Enable body 'body_animated_missile_left'.
		Enable body 'body_animated_missile_right'.
