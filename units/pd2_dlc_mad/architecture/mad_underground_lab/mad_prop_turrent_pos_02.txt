unit:
	sequence 'open':
		Disable object 'g_closed'.
		Enable object 'g_open'.
	sequence 'close':
		Enable object 'g_closed'.
		Disable object 'g_open'.
