unit:
	sequence 'state_interaction_enable':
		Enable interaction.
		Enable object 'g_door_contour'.
	sequence 'state_interaction_disable':
		Disable interaction.
		Disable object 'g_door_contour'.
	sequence 'state_vis_hide':
		Disable body 'body_frame'.
		Disable body 'body_door'.
		Disable body 'body_handle'.
		Disable decal_mesh 'dm_metal_01'.
		Disable decal_mesh 'dm_metal_02'.
		Hide graphic_group 'grp_door'.
	sequence 'state_vis_show':
		Enable body 'body_frame'.
		Enable body 'body_door'.
		Enable body 'body_handle'.
		Enable decal_mesh 'dm_metal_01'.
		Enable decal_mesh 'dm_metal_02'.
		Show graphic_group 'grp_door'.
	sequence 'state_door_closed':
		Enable animation_group 'anim':
			from 0
			to 0
	sequence 'state_door_opened':
		Enable animation_group 'anim':
			from 150/30
			to 150/30
	sequence 'anim_door_open':
		Enable animation_group 'anim':
			from 0
			speed 1
			to 150/30
		Play audio 'born_mountain_door_open' at 'rp_bor_prop_mountain_door'.
	sequence 'anim_door_close':
		Enable animation_group 'anim':
			from 150/30
			speed -1
			to 0
	sequence 'interact':
		Run sequence 'anim_door_open'.
