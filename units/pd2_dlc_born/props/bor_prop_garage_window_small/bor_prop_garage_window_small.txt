unit:
	sequence 'destroy_01':
		TRIGGER TIMES 1
		Disable object 'g_glass_01'.
		Play audio 'window_small_shatter' at 'e_glass_01'.
		effect 'effects/particles/dest/glass_small_dest':
			parent object( 'e_glass_01' )
			position v()
	sequence 'destroy_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_02'.
		Play audio 'window_small_shatter' at 'e_glass_02'.
		effect 'effects/particles/dest/glass_small_dest':
			parent object( 'e_glass_02' )
			position v()
	sequence 'destroy_03':
		TRIGGER TIMES 1
		Disable object 'g_glass_03'.
		Play audio 'window_small_shatter' at 'e_glass_03'.
		effect 'effects/particles/dest/glass_small_dest':
			parent object( 'e_glass_03' )
			position v()
	sequence 'destroy_08':
		TRIGGER TIMES 1
		Disable object 'g_glass_08'.
		Play audio 'window_small_shatter' at 'e_glass_08'.
		effect 'effects/particles/dest/glass_small_dest':
			parent object( 'e_glass_08' )
			position v()
	sequence 'destroy_09':
		TRIGGER TIMES 1
		Disable object 'g_glass_09'.
		Play audio 'window_small_shatter' at 'e_glass_09'.
		effect 'effects/particles/dest/glass_small_dest':
			parent object( 'e_glass_09' )
			position v()
	sequence 'destroy_10':
		TRIGGER TIMES 1
		Disable object 'g_glass_10'.
		Play audio 'window_small_shatter' at 'e_glass_10'.
		effect 'effects/particles/dest/glass_small_dest':
			parent object( 'e_glass_10' )
			position v()
	sequence 'destroy_15':
		TRIGGER TIMES 1
		Disable object 'g_glass_15'.
		Play audio 'window_small_shatter' at 'e_glass_15'.
		effect 'effects/particles/dest/glass_small_dest':
			parent object( 'e_glass_15' )
			position v()
	sequence 'destroy_16':
		TRIGGER TIMES 1
		Disable object 'g_glass_16'.
		Play audio 'window_small_shatter' at 'e_glass_16'.
		effect 'effects/particles/dest/glass_small_dest':
			parent object( 'e_glass_16' )
			position v()
	sequence 'destroy_17':
		TRIGGER TIMES 1
		Disable object 'g_glass_17'.
		Play audio 'window_small_shatter' at 'e_glass_17'.
		effect 'effects/particles/dest/glass_small_dest':
			parent object( 'e_glass_17' )
			position v()
	sequence 'destroy_glass':
		TRIGGER TIMES 1
		Disable object 'g_glass_01'.
		Disable object 'g_glass_02'.
		Disable object 'g_glass_03'.
		Disable object 'g_glass_08'.
		Disable object 'g_glass_09'.
		Disable object 'g_glass_10'.
		Disable object 'g_glass_15'.
		Disable object 'g_glass_16'.
		Disable object 'g_glass_17'.
		Disable object 's_s'.
		Disable object 'g_ind_ext_dock_bld_window_solid'.
		Play audio 'window_small_shatter' at 'e_01'.
		effect 'effects/payday2/particles/window/storefront_window_large':
			parent object( 'e_01' )
			position v()
		Disable body 'glass_body_whole'.
		Disable body 'glass_body_01'.
		Disable body 'glass_body_02'.
		Disable body 'glass_body_03'.
		Disable body 'glass_body_08'.
		Disable body 'glass_body_09'.
		Disable body 'glass_body_10'.
		Disable body 'glass_body_15'.
		Disable body 'glass_body_16'.
		Disable body 'glass_body_17'.
		Cause alert with 12 m radius.
		spawn_unit 'units/payday2/architecture/ind/ind_ext_dock_bld/ind_ext_dock_bld_window_solid_debris':
			position object_pos('e_debris')
			rotation object_rot('e_debris')
	body 'glass_body_01'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_01'.
	body 'glass_body_02'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_02'.
	body 'glass_body_03'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_03'.
	body 'glass_body_08'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_08'.
	body 'glass_body_09'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_09'.
	body 'glass_body_10'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_10'.
	body 'glass_body_15'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_15'.
	body 'glass_body_16'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_16'.
	body 'glass_body_17'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_17'.
	body 'glass_body_whole'
		Upon receiving 5 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_glass'.
