unit:
	sequence 'int_seq_hide_all':
		Disable object 'g_body'.
		Disable object 'g_hands'.
		Disable object 'g_head_chains'.
		Disable object 'g_head_dragan'.
		Disable object 'g_head_dallas'.
		Disable object 'g_head_hoxton'.
		Disable object 'g_head_old_hoxton'.
		Disable object 'g_head_john_wick'.
		Disable object 'g_head_wolf'.
		Disable object 'g_head_sokol'.
		Disable object 'g_sokol_mask_on'.
		Disable object 'g_sokol_mask_off'.
		Disable object 'g_sokol_hair'.
		Disable object 'g_head_jacket'.
		Disable object 'g_body_jacket'.
		Disable object 'g_head_jiro'.
		Disable object 'g_body_jiro'.
		Disable object 'g_body_bodhi'.
		Disable object 'g_head_bodhi'.
		Disable object 'g_head_jimmy'.
		Disable object 'g_body_jimmy'.
		Disable object 'g_jimmy_mask_on'.
		Disable object 'g_jimmy_mask_off'.
	sequence 'var_mtr_chains':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/pd2_dlc_chill/characters/shared_character/mtr_chains'.
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Enable object 'g_head_chains'.
	sequence 'var_mtr_dallas':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/pd2_dlc_chill/characters/shared_character/mtr_dallas'.
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Enable object 'g_head_dallas'.
	sequence 'var_mtr_hoxton':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/pd2_dlc_chill/characters/shared_character/mtr_hoxton'.
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Enable object 'g_head_hoxton'.
	sequence 'var_mtr_dragan':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/pd2_dlc_chill/characters/shared_character/mtr_dragan'.
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Enable object 'g_head_dragan'.
	sequence 'var_mtr_jacket':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/pd2_dlc_chill/characters/shared_character/mtr_jacket'.
		Enable object 'g_head_jacket'.
		Enable object 'g_body_jacket'.
	sequence 'var_mtr_old_hoxton':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/pd2_dlc_chill/characters/shared_character/mtr_old_hoxton'.
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Enable object 'g_head_old_hoxton'.
	sequence 'var_mtr_wolf':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/pd2_dlc_chill/characters/shared_character/mtr_wolf'.
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Enable object 'g_head_wolf'.
	sequence 'var_mtr_john_wick':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/pd2_dlc_chill/characters/shared_character/mtr_john_wick'.
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Enable object 'g_head_john_wick'.
	sequence 'var_mtr_sokol':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/pd2_dlc_chill/characters/shared_character/mtr_sokol'.
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Enable object 'g_head_sokol'.
		Enable object 'g_sokol_mask_off'.
		Enable object 'g_sokol_hair'.
	sequence 'var_mtr_jiro':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/pd2_dlc_chill/characters/shared_character/mtr_jiro'.
		Enable object 'g_hands'.
		Enable object 'g_head_jiro'.
		Enable object 'g_body_jiro'.
	sequence 'var_mtr_bodhi':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/pd2_dlc_chill/characters/shared_character/mtr_bodhi'.
		Enable object 'g_head_bodhi'.
		Enable object 'g_body_bodhi'.
	sequence 'mask_on_sokol':
		Enable object 'g_sokol_mask_on'.
		Disable object 'g_sokol_mask_off'.
		Disable object 'g_sokol_hair'.
	sequence 'mask_off_sokol':
		Disable object 'g_sokol_mask_on'.
		Enable object 'g_sokol_mask_off'.
		Enable object 'g_sokol_hair'.
	sequence 'mask_on_jimmy':
		Enable object 'g_jimmy_mask_on'.
		Disable object 'g_jimmy_mask_off'.
	sequence 'mask_off_jimmy':
		Disable object 'g_jimmy_mask_on'.
		Enable object 'g_jimmy_mask_off'.
	sequence 'var_mtr_jimmy':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/pd2_dlc_chill/characters/shared_character/mtr_jimmy'.
		Enable object 'g_body_jimmy'.
		Enable object 'g_head_jimmy'.
		Enable object 'g_jimmy_mask_off'.
		Enable object 'g_jimmy_mask_on'.
	sequence 'var_model_01':
		Disable object 'g_vest_small'.
		Disable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'var_model_02':
		Enable object 'g_vest_small'.
		Disable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'var_model_03':
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'var_model_04':
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Enable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'var_model_05':
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Enable object 'g_vest_shoulder'.
		Enable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'var_model_06':
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Enable object 'g_vest_shoulder'.
		Enable object 'g_vest_neck'.
		Enable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'var_model_07':
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Enable object 'g_vest_shoulder'.
		Enable object 'g_vest_neck'.
		Enable object 'g_vest_thies'.
		Enable object 'g_vest_leg_arm'.
	sequence 'minigame_win':
		Call function: interaction.play_minigame_vo('win')
	sequence 'minigame_lose':
		Call function: interaction.play_minigame_vo('lose')
