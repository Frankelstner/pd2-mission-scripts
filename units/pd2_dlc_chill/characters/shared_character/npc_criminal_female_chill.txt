unit:
	sequence 'var_mtr_fem1':
		material_config 'units/payday2/characters/npc_criminal_female_1/mtr_fem1'.
		Enable object 'g_head_fem1'.
	sequence 'mask_on':
		Disable object 'g_hair_cloth1'.
		Disable object 'g_hair_cloth2'.
		Disable object 'g_hair_cloth3'.
		Enable object 'g_hair_mask_cloth1'.
		Enable object 'g_hair_mask_cloth2'.
		Enable object 'g_hair_mask_cloth3'.
	sequence 'mask_off':
		Enable object 'g_hair_cloth1'.
		Enable object 'g_hair_cloth2'.
		Enable object 'g_hair_cloth3'.
		Disable object 'g_hair_mask_cloth1'.
		Disable object 'g_hair_mask_cloth2'.
		Disable object 'g_hair_mask_cloth3'.
	sequence 'var_model_01':
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Disable object 'g_vest_small'.
		Disable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'var_model_02':
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Enable object 'g_vest_small'.
		Disable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'var_model_03':
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'var_model_04':
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Enable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'var_model_05':
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Enable object 'g_vest_shoulder'.
		Enable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'var_model_06':
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Enable object 'g_vest_shoulder'.
		Enable object 'g_vest_neck'.
		Enable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'var_model_07':
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Enable object 'g_vest_shoulder'.
		Enable object 'g_vest_neck'.
		Enable object 'g_vest_thies'.
		Enable object 'g_vest_leg_arm'.
