unit:
	open = 0
	sequence 'state_open':
		Enable animation_group 'anim_open':
			from 150/30
			to 150/30
		Disable object 'g_rings'.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'interact':
		Run sequence 'int_seq_open_crate'.
		Call function: interaction.set_tweak_data('hold_remove_parts')
		If open == 1: Disable object 'g_rings'.
		If open == 1: Play audio 'bar_bag_generic_finished' at 'anim_lid'.
		If open == 1: Play audio 'food_lift_bag_drop' at 'anim_lid'.
		open = 1
	sequence 'int_seq_open_crate':
		TRIGGER TIMES 1
		Enable animation_group 'anim_open':
			from 0/30
			to 150/30
		Play audio 'mystery_box_open' at 'anim_lid'.
	sequence 'hide':
		Disable object 'g_lid'.
		Disable object 'g_box'.
		Disable object 'g_rings'.
		Disable decal_mesh 'dm_metal'.
		Disable decal_mesh 'dm_metal_lid'.
		Disable decal_mesh 'dm_cloth'.
		Disable decal_mesh 'dm_cloth_lid'.
		Disable decal_mesh 'dm_wood'.
		Disable decal_mesh 'dm_wood_lid'.
		Disable body 'body_static'.
		Disable body 'body_animated'.
	sequence 'show':
		Enable object 'g_lid'.
		Enable object 'g_box'.
		Enable object 'g_rings'.
		Enable decal_mesh 'dm_metal'.
		Enable decal_mesh 'dm_metal_lid'.
		Enable decal_mesh 'dm_cloth'.
		Enable decal_mesh 'dm_cloth_lid'.
		Enable decal_mesh 'dm_wood'.
		Enable decal_mesh 'dm_wood_lid'.
		Enable body 'body_static'.
		Enable body 'body_animated'.
