unit:
	sequence 'hide':
		Hide graphic_group 'grp_graphics'.
	sequence 'show':
		Show graphic_group 'grp_graphics'.
	sequence 'state_lit':
		Disable object 'g_lod0'.
		Disable object 'g_lod1'.
		Disable object 'g_lod2'.
		Enable object 'g_lit_lod0'.
		Enable object 'g_lit_lod1'.
		Enable object 'g_lit_lod2'.
	sequence 'state_unlit':
		Enable object 'g_lod0'.
		Enable object 'g_lod1'.
		Enable object 'g_lod2'.
		Disable object 'g_lit_lod0'.
		Disable object 'g_lit_lod1'.
		Disable object 'g_lit_lod2'.
