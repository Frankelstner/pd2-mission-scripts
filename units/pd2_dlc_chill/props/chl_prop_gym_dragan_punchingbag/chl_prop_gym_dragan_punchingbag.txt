unit:
	body 'dynamic_body_07'
		Upon receiving 10 melee damage, execute:
			Run sequence 'int_seq_dmg_1'.
	sequence 'int_seq_dmg_1':
		Enable object 's_s'.
		Play audio 'punching_bag_hit' at 'snd'.
		Run sequence 'int_seq_reset_collision'.
	sequence 'int_seq_reset_collision':
		Set melee damage to 0.
