unit:
	sequence 'state_power_off':
		material_config 'units/pd2_dlc_chill/props/chl_prop_control_screen/chl_prop_control_screen_off'.
	sequence 'state_power_on':
		material_config 'units/pd2_dlc_chill/props/chl_prop_control_screen/chl_prop_control_screen'.
	sequence 'state_interaction_enabled':
		Enable interaction.
		Show graphic_group 'OutlineGroup'.
	sequence 'state_interaction_disabled':
		Disable interaction.
		Hide graphic_group 'OutlineGroup'.
	sequence 'interact'.
	sequence 'load'.
