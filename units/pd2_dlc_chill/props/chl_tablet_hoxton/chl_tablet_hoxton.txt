unit:
	sequence 'state_intaction_enabled':
		Enable interaction.
	sequence 'state_interaction_disabled':
		Disable interaction.
	sequence 'state_outline_enabled':
		material_config 'units/payday2/props/gen_interactable_prop_ipad/gen_interactable_prop_ipad_contour'.
		Show graphic_group 'OutlineGroup'.
	sequence 'state_outline_disabled':
		material_config 'units/payday2/props/gen_interactable_prop_ipad/gen_interactable_prop_ipad'.
		Hide graphic_group 'OutlineGroup'.
	sequence 'state_show':
		Run sequence 'display_off'.
	sequence 'state_hide':
		Disable object 'g_iphone'.
		Disable object 'g_display_on'.
		Disable object 'g_display_off'.
	sequence 'display_off':
		Enable object 'g_iphone'.
		Disable object 'g_display_on'.
		Enable object 'g_display_off'.
	sequence 'display_on':
		Enable object 'g_display_on'.
		Disable object 'g_display_off'.
		Enable object 'g_iphone'.
	sequence 'interact':
		Run sequence 'state_hide'.
		Play audio 'put_down_server_parts' at 'rp_ipad'.
	sequence 'load'.
