unit:
	sequence 'hide':
		Hide graphic_group 'roulette_lods'.
		Disable body 'static_body'.
	sequence 'show':
		Show graphic_group 'roulette_lods'.
		Enable body 'static_body'.
	sequence 'enable_interaction':
		Enable object 'g_spinner_outline'.
		Enable interaction.
	sequence 'disable_interaction':
		Disable object 'g_spinner_outline'.
		Disable interaction.
	sequence 'interact':
		Disable object 'g_spinner_outline'.
	sequence 'red':
		Play audio 'Play_rcp_wheelspin_comment' at 'snd_02'.
		Enable object 'g_ball'.
		Run sequence 'slot_'..pick('1','14','9','18','7','12','3','32','19','21','25','34','27','36','30','32','5','16' ).
		Play audio 'roulette_start' at 'snd'.
	sequence 'black':
		Play audio 'Play_rcp_wheelspin_comment' at 'snd_02'.
		Enable object 'g_ball'.
		Run sequence 'slot_'..pick('20','31','22','29','28','35','26','15','4','2','17','6','13','11','8','10','24','33' ).
		Play audio 'roulette_start' at 'snd'.
	sequence 'int_seq_sound_finnish':
		Stop audio 'roulette_start' at 'snd'.
		Play audio 'roulette_finnish' at 'snd'.
	sequence 'sound_play_rcp_player_lose':
		Play audio 'Play_rcp_house_wins' at 'snd_02'.
	sequence 'sound_play_rcp_player_wins':
		Play audio 'Play_rcp_player_wins' at 'snd_02'.
	sequence 'sound_play_rcp_no_bets':
		Play audio 'Play_rcp_no_bets' at 'snd_02'.
	sequence 'sound_play_rcp_intro':
		Play audio 'Play_rcp_idle_comment' at 'snd_02'.
	sequence 'sound_play_rcp_place_bets':
		Play audio 'Play_rcp_activate' at 'snd_02'.
	sequence 'slot_1':
		Play audio 'Play_rcp_number_01' at 'snd_02'. (DELAY 152/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 152/30
		Enable animation_group 'anim_spin_falloff': (DELAY 0/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 150/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 150/30)
		Run sequence 'anim_done'. (DELAY 152/30)
	sequence 'slot_20':
		Play audio 'Play_rcp_number_20' at 'snd_02'. (DELAY 154/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 154/30
		Enable animation_group 'anim_spin_falloff': (DELAY 2/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 152/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 152/30)
		Run sequence 'anim_done'. (DELAY 154/30)
	sequence 'slot_14':
		Play audio 'Play_rcp_number_14' at 'snd_02'. (DELAY 156/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 156/30
		Enable animation_group 'anim_spin_falloff': (DELAY 4/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 154/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 154/30)
		Run sequence 'anim_done'. (DELAY 156/30)
	sequence 'slot_31':
		Play audio 'Play_rcp_number_31' at 'snd_02'. (DELAY 158/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 158/30
		Enable animation_group 'anim_spin_falloff': (DELAY 6/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 156/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 156/30)
		Run sequence 'anim_done'. (DELAY 158/30)
	sequence 'slot_9':
		Play audio 'Play_rcp_number_09' at 'snd_02'. (DELAY 160/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 160/30
		Enable animation_group 'anim_spin_falloff': (DELAY 8/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 158/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 158/30)
		Run sequence 'anim_done'. (DELAY 160/30)
	sequence 'slot_22':
		Play audio 'Play_rcp_number_22' at 'snd_02'. (DELAY 162/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 162/30
		Enable animation_group 'anim_spin_falloff': (DELAY 10/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 160/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 160/30)
		Run sequence 'anim_done'. (DELAY 162/30)
	sequence 'slot_18':
		Play audio 'Play_rcp_number_18' at 'snd_02'. (DELAY 164/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 164/30
		Enable animation_group 'anim_spin_falloff': (DELAY 12/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 162/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 162/30)
		Run sequence 'anim_done'. (DELAY 164/30)
	sequence 'slot_29':
		Play audio 'Play_rcp_number_29' at 'snd_02'. (DELAY 166/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 166/30
		Enable animation_group 'anim_spin_falloff': (DELAY 14/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 164/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 164/30)
		Run sequence 'anim_done'. (DELAY 166/30)
	sequence 'slot_7':
		Play audio 'Play_rcp_number_07' at 'snd_02'. (DELAY 168/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 168/30
		Enable animation_group 'anim_spin_falloff': (DELAY 18/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 166/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 166/30)
		Run sequence 'anim_done'. (DELAY 168/30)
	sequence 'slot_28':
		Play audio 'Play_rcp_number_28' at 'snd_02'. (DELAY 170/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 170/30
		Enable animation_group 'anim_spin_falloff': (DELAY 20/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 168/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 168/30)
		Run sequence 'anim_done'. (DELAY 170/30)
	sequence 'slot_12':
		Play audio 'Play_rcp_number_12' at 'snd_02'. (DELAY 172/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 172/30
		Enable animation_group 'anim_spin_falloff': (DELAY 22/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 170/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 170/30)
		Run sequence 'anim_done'. (DELAY 172/30)
	sequence 'slot_35':
		Play audio 'Play_rcp_number_35' at 'snd_02'. (DELAY 174/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 174/30
		Enable animation_group 'anim_spin_falloff': (DELAY 24/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 172/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 172/30)
		Run sequence 'anim_done'. (DELAY 174/30)
	sequence 'slot_3':
		Play audio 'Play_rcp_number_03' at 'snd_02'. (DELAY 176/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 176/30
		Enable animation_group 'anim_spin_falloff': (DELAY 26/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 174/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 174/30)
		Run sequence 'anim_done'. (DELAY 176/30)
	sequence 'slot_26':
		Play audio 'Play_rcp_number_26' at 'snd_02'. (DELAY 178/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 178/30
		Enable animation_group 'anim_spin_falloff': (DELAY 28/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 176/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 176/30)
		Run sequence 'anim_done'. (DELAY 178/30)
	sequence 'slot_0':
		Play audio 'Play_rcp_number_00' at 'snd_02'. (DELAY 180/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 180/30
		Enable animation_group 'anim_spin_falloff': (DELAY 30/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 178/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 178/30)
		Run sequence 'anim_done'. (DELAY 180/30)
	sequence 'slot_32':
		Play audio 'Play_rcp_number_32' at 'snd_02'. (DELAY 184/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 182/30
		Enable animation_group 'anim_spin_falloff': (DELAY 32/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 180/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 180/30)
		Run sequence 'anim_done'. (DELAY 184/30)
	sequence 'slot_15':
		Play audio 'Play_rcp_number_15' at 'snd_02'. (DELAY 182/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 184/30
		Enable animation_group 'anim_spin_falloff': (DELAY 34/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 182/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 182/30)
		Run sequence 'anim_done'. (DELAY 185/30)
	sequence 'slot_19':
		Play audio 'Play_rcp_number_19' at 'snd_02'. (DELAY 186/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 186/30
		Enable animation_group 'anim_spin_falloff': (DELAY 36/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 184/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 184/30)
		Run sequence 'anim_done'. (DELAY 186/30)
	sequence 'slot_4':
		Play audio 'Play_rcp_number_04' at 'snd_02'. (DELAY 190/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 188/30
		Enable animation_group 'anim_spin_falloff': (DELAY 38/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 186/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 186/30)
		Run sequence 'anim_done'. (DELAY 190/30)
	sequence 'slot_21':
		Play audio 'Play_rcp_number_21' at 'snd_02'. (DELAY 188/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 190/30
		Enable animation_group 'anim_spin_falloff': (DELAY 40/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 188/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 188/30)
		Run sequence 'anim_done'.
	sequence 'slot_2':
		Play audio 'Play_rcp_number_02' at 'snd_02'. (DELAY 192/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 192/30
		Enable animation_group 'anim_spin_falloff': (DELAY 42/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 190/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 190/30)
		Run sequence 'anim_done'. (DELAY 192/30)
	sequence 'slot_25':
		Play audio 'Play_rcp_number_25' at 'snd_02'. (DELAY 194/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 194/30
		Enable animation_group 'anim_spin_falloff': (DELAY 44/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 192/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 192/30)
		Run sequence 'anim_done'. (DELAY 194/30)
	sequence 'slot_17':
		Play audio 'Play_rcp_number_17' at 'snd_02'. (DELAY 196/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 196/30
		Enable animation_group 'anim_spin_falloff': (DELAY 46/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 194/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 194/30)
		Run sequence 'anim_done'. (DELAY 196/30)
	sequence 'slot_34':
		Play audio 'Play_rcp_number_34' at 'snd_02'. (DELAY 198/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 198/30
		Enable animation_group 'anim_spin_falloff': (DELAY 48/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 196/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 196/30)
		Run sequence 'anim_done'. (DELAY 198/30)
	sequence 'slot_6':
		Play audio 'Play_rcp_number_06' at 'snd_02'. (DELAY 200/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 200/30
		Enable animation_group 'anim_spin_falloff': (DELAY 50/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 198/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 198/30)
		Run sequence 'anim_done'. (DELAY 200/30)
	sequence 'slot_27':
		Play audio 'Play_rcp_number_27' at 'snd_02'. (DELAY 202/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 202/30
		Enable animation_group 'anim_spin_falloff': (DELAY 52/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 200/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 200/30)
		Run sequence 'anim_done'. (DELAY 202/30)
	sequence 'slot_13':
		Play audio 'Play_rcp_number_13' at 'snd_02'. (DELAY 204/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 204/30
		Enable animation_group 'anim_spin_falloff': (DELAY 54/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 202/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 202/30)
		Run sequence 'anim_done'. (DELAY 204/30)
	sequence 'slot_36':
		Play audio 'Play_rcp_number_36' at 'snd_02'. (DELAY 206/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 206/30
		Enable animation_group 'anim_spin_falloff': (DELAY 56/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 204/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 204/30)
		Run sequence 'anim_done'. (DELAY 206/30)
	sequence 'slot_11':
		Play audio 'Play_rcp_number_11' at 'snd_02'. (DELAY 208/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 208/30
		Enable animation_group 'anim_spin_falloff': (DELAY 58/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 206/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 206/30)
		Run sequence 'anim_done'. (DELAY 208/30)
	sequence 'slot_30':
		Play audio 'Play_rcp_number_30' at 'snd_02'. (DELAY 210/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 210/30
		Enable animation_group 'anim_spin_falloff': (DELAY 60/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 208/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 208/30)
		Run sequence 'anim_done'. (DELAY 210/30)
	sequence 'slot_8':
		Play audio 'Play_rcp_number_08' at 'snd_02'. (DELAY 212/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 212/30
		Enable animation_group 'anim_spin_falloff': (DELAY 62/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 210/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 210/30)
		Run sequence 'anim_done'. (DELAY 212/30)
	sequence 'slot_32':
		Play audio 'Play_rcp_number_32' at 'snd_02'. (DELAY 214/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 214/30
		Enable animation_group 'anim_spin_falloff': (DELAY 64/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 212/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 212/30)
		Run sequence 'anim_done'. (DELAY 214/30)
	sequence 'slot_10':
		Play audio 'Play_rcp_number_10' at 'snd_02'. (DELAY 216/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 216/30
		Enable animation_group 'anim_spin_falloff': (DELAY 66/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 214/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 214/30)
		Run sequence 'anim_done'. (DELAY 216/30)
	sequence 'slot_5':
		Play audio 'Play_rcp_number_05' at 'snd_02'. (DELAY 218/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 218/30
		Enable animation_group 'anim_spin_falloff': (DELAY 68/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 216/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 216/30)
		Run sequence 'anim_done'. (DELAY 218/30)
	sequence 'slot_24':
		Play audio 'Play_rcp_number_24' at 'snd_02'. (DELAY 220/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 220/30
		Enable animation_group 'anim_spin_falloff': (DELAY 70/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 218/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 218/30)
		Run sequence 'anim_done'. (DELAY 220/30)
	sequence 'slot_16':
		Play audio 'Play_rcp_number_16' at 'snd_02'. (DELAY 222/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 222/30
		Enable animation_group 'anim_spin_falloff': (DELAY 72/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 220/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 220/30)
		Run sequence 'anim_done'. (DELAY 222/30)
	sequence 'slot_33':
		Play audio 'Play_rcp_number_33' at 'snd_02'. (DELAY 224/30)
		Enable animation_group 'anim_spinner':
			from 0/30
			to 400/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 224/30
		Enable animation_group 'anim_spin_falloff': (DELAY 74/30)
			from 0/30
			to 120/30
		Enable animation_group 'anim_ball': (DELAY 222/30)
			from 120/30
			to 130/30
		Run sequence 'int_seq_sound_finnish'. (DELAY 222/30)
		Run sequence 'anim_done'. (DELAY 224/30)
	sequence 'reset':
		Enable animation_group 'anim_spinner':
			from 0/30
			to 0/30
		Enable animation_group 'anim_spin_ball':
			from 0/30
			to 0/30
		Enable animation_group 'anim_ball':
			from 0/30
			to 0/30
		Enable animation_group 'anim_spin_falloff':
			from 0/30
			to 0/30
		Enable animation_group 'anim_initial':
			from 0/30
			to 0/30
		Disable object 'g_ball'.
	sequence 'anim_done'.
