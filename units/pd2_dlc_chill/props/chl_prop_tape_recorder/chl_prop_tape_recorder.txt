unit:
	int = 0
	sequence 'set_new_interaction':
		int = 1
	sequence 'hide_disable_interaction':
		Hide graphic_group 'tape_recorder'.
		Disable body 'body_static'.
		Hide graphic_group 'outline'.
		Disable interaction.
	sequence 'show_enable_interaction':
		Show graphic_group 'tape_recorder'.
		Enable body 'body_static'.
		Show graphic_group 'outline'.
		Enable interaction.
	sequence 'interact':
		If int == 0: Stop audio 'jacket_tape_recorder_play' at 'snd'.
		If int == 0: Play audio 'jacket_tape_recorder_play' at 'snd'.
