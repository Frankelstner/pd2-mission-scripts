unit:
	sequence 'int_seq_break_bottle':
		effect 'effects/particles/dest/bottle_dest_tequila':
			parent object( 'rp_chl_prop_tequila_bottle' )
			position v()
		Play audio 'bottle_break' at 'rp_chl_prop_tequila_bottle'.
		Disable object 'g_g'.
		Disable body 'body_static'.
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_bottle'.
