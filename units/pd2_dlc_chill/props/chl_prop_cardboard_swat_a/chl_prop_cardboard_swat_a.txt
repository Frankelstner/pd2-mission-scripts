unit:
	visable = 1
	dead = 0
	body 'body_collision_cardboard'
		Upon receiving 10 damage, execute:
			If dead == 0: Run sequence 'done_target_dead'.
			Run sequence 'reset_damage'.
	body 'body_collision_head'
		Upon receiving 10 damage, execute:
			If dead == 0: Run sequence 'done_target_headshot'.
			Run sequence 'reset_damage'.
	sequence 'reset_damage':
		Set damage damage to 0.
	sequence 'state_down':
		Disable body 'body_collision_cardboard'.
		Disable body 'body_collision_head'.
		Enable animation_group 'anim_retract':
			from 30/30
			speed 1
			to 30/30
	sequence 'anim_down_done':
		visable = 0
	sequence 'done_target_dead':
		Disable body 'body_collision_cardboard'.
		Disable body 'body_collision_head'.
		Set damage damage to 0.
		dead = 1
	sequence 'done_target_headshot':
		effect 'effects/payday2/particles/wood/wood_door_shotgun_dmg_in':
			parent object( 'e_effect' )
			position v()
		Disable body 'body_collision_cardboard'.
		Disable body 'body_collision_head'.
		Hide graphic_group 'gg_body_whole'.
		Show graphic_group 'gg_body_shattered'.
		Play audio 'kill_room_cutouts_hs' at 'e_effect'.
		Set damage damage to 0.
		dead = 1
	sequence 'anim_up':
		dead = 0
		Enable body 'body_collision_cardboard'.
		Enable body 'body_collision_head'.
		Show graphic_group 'gg_body_whole'.
		Hide graphic_group 'gg_body_shattered'.
		Enable decal_mesh 'dm_wood'.
		If visable == 1: Play audio 'kill_room_cutouts_up' at 'snd'.
		Enable animation_group 'anim_retract':
			from 30/30
			speed -1
			to 0/30
	sequence 'anim_down':
		Enable animation_group 'anim_retract':
			from 0/30
			speed 1
			to 30/30
		If visable == 1: Play audio 'kill_room_cutouts_down' at 'snd'.
		Run sequence 'anim_down_done'. (DELAY 30/30)
		Disable decal_mesh 'dm_wood'. (DELAY 30/30)
	sequence 'state_hide':
		visable = 0
		Hide graphic_group 'gg_body_whole'.
		Hide graphic_group 'gg_body_shattered'.
		Hide graphic_group 'g_base_group'.
		Disable body 'body_base_static'.
		Disable body 'body_collision_cardboard'.
		Disable body 'body_collision_head'.
		Disable decal_mesh 'dm_steel_base'.
		Disable decal_mesh 'dm_steel_board'.
		Disable decal_mesh 'dm_wood'.
	sequence 'state_show':
		visable = 1
		Show graphic_group 'gg_body_whole'.
		Show graphic_group 'gg_body_shattered'.
		Show graphic_group 'g_base_group'.
		Enable body 'body_base_static'.
		Enable body 'body_collision_cardboard'.
		Enable body 'body_collision_head'.
		Enable decal_mesh 'dm_steel_base'.
		Enable decal_mesh 'dm_steel_board'.
		Enable decal_mesh 'dm_wood'.
