unit:
	sequence 'stop':
		material 'mat_screen01':
			state 'pause'
		material 'mat_screen02':
			state 'pause'
		material 'mat_screen03':
			state 'pause'
		material 'mat_screen04':
			state 'pause'
	sequence 'play':
		EXECUTE ON STARTUP
		material 'mat_screen01':
			state 'play'
		material 'mat_screen02':
			state 'play'
		material 'mat_screen03':
			state 'play'
		material 'mat_screen04':
			state 'play'
		Run sequence 'start_uv_anim_'..pick('sheet_1','sheet_2','sheet_3','sheet_4','sheet_5','sheet_6','sheet_7','sheet_8','sheet_9','sheet_10','sheet_11'). (DELAY 1/30)
		startup True
	sequence 'start_uv_anim_sheet_1':
		Disable object 'g_screen_off'.
		Enable object 'g_screen01'.
		Disable object 'g_screen02'.
		Disable object 'g_screen03'.
		Disable object 'g_screen04'.
		material 'mat_screen01':
			state 'pause'
		Run sequence 'sheet1_time_0'.
	sequence 'sheet1_time_0':
		material 'mat_screen01':
			time 0
		Run sequence 'sheet1_time_1'. (DELAY 2/30)
	sequence 'sheet1_time_1':
		material 'mat_screen01':
			time 0.125
		Run sequence 'sheet1_time_2'. (DELAY 2/30)
	sequence 'sheet1_time_2':
		material 'mat_screen01':
			time 0.25
		Run sequence 'sheet1_time_3'. (DELAY 90/30)
	sequence 'sheet1_time_3':
		material 'mat_screen01':
			time 0
		Run sequence 'sheet1_time_4'. (DELAY 2/30)
	sequence 'sheet1_time_4':
		material 'mat_screen01':
			time 0
		Run sequence 'sheet1_time_5'. (DELAY 1/30)
	sequence 'sheet1_time_5':
		material 'mat_screen01':
			time 0.625
		Run sequence 'sheet1_time_6'. (DELAY 30/30)
	sequence 'sheet1_time_6':
		material 'mat_screen01':
			time 0.25
		Run sequence 'sheet1_time_7'. (DELAY 1/30)
	sequence 'sheet1_time_7':
		material 'mat_screen01':
			time 0.75
		Run sequence 'sheet1_time_8'. (DELAY 60/30)
	sequence 'sheet1_time_8':
		material 'mat_screen01':
			time 0
		Run sequence 'sheet1_time_9'. (DELAY 1/30)
	sequence 'sheet1_time_9':
		material 'mat_screen01':
			time 0.625
		Run sequence 'sheet1_time_10'. (DELAY 30/30)
	sequence 'sheet1_time_10':
		material 'mat_screen01':
			time 0.25
		Run sequence 'sheet1_time_11'. (DELAY 1/30)
	sequence 'sheet1_time_11':
		material 'mat_screen01':
			time 0.875
		Run sequence 'sheet1_time_12'. (DELAY 60/30)
	sequence 'sheet1_time_12':
		material 'mat_screen01':
			time 0.875
		Run sequence 'start_uv_anim_'..pick('sheet_1','sheet_2','sheet_3','sheet_4','sheet_5','sheet_6','sheet_7','sheet_8','sheet_9','sheet_10','sheet_11'). (DELAY 1/30)
	sequence 'start_uv_anim_sheet_2':
		Disable object 'g_screen_off'.
		Disable object 'g_screen01'.
		Enable object 'g_screen02'.
		Disable object 'g_screen03'.
		Disable object 'g_screen04'.
		material 'mat_screen02':
			state 'pause'
		Run sequence 'sheet2_time_0'.
	sequence 'sheet2_time_0':
		material 'mat_screen02':
			time 0.75
		Run sequence 'sheet2_time_1'. (DELAY 2/30)
	sequence 'sheet2_time_1':
		material 'mat_screen02':
			time 0.125
		Run sequence 'sheet2_time_2'. (DELAY 60/30)
	sequence 'sheet2_time_2':
		material 'mat_screen02':
			time 0.25
		Run sequence 'sheet2_time_3'. (DELAY 60/30)
	sequence 'sheet2_time_3':
		material 'mat_screen02':
			time 0.375
		Run sequence 'sheet2_time_4'. (DELAY 60/30)
	sequence 'sheet2_time_4':
		material 'mat_screen02':
			time 0.5
		Run sequence 'sheet2_time_5'. (DELAY 60/30)
	sequence 'sheet2_time_5':
		material 'mat_screen02':
			time 0.625
		Run sequence 'sheet2_time_6'. (DELAY 60/30)
	sequence 'sheet2_time_6':
		material 'mat_screen02':
			time 0
		Run sequence 'sheet2_time_7'. (DELAY 60/30)
	sequence 'sheet2_time_7':
		material 'mat_screen02':
			time 0.875
		Run sequence 'start_uv_anim_'..pick('sheet_1','sheet_2','sheet_3','sheet_4','sheet_5','sheet_6','sheet_7','sheet_8','sheet_9','sheet_10','sheet_11'). (DELAY 2/30)
	sequence 'start_uv_anim_sheet_3':
		Disable object 'g_screen_off'.
		Disable object 'g_screen01'.
		Disable object 'g_screen02'.
		Enable object 'g_screen03'.
		Disable object 'g_screen04'.
		material 'mat_screen03':
			state 'pause'
		Run sequence 'sheet3_time_0'.
	sequence 'sheet3_time_0':
		material 'mat_screen03':
			time 0
		Run sequence 'sheet3_time_1'. (DELAY 2/30)
	sequence 'sheet3_time_1':
		material 'mat_screen03':
			time 0.125
		Run sequence 'sheet3_time_2'. (DELAY 2/30)
	sequence 'sheet3_time_2':
		material 'mat_screen03':
			time 0.25
		Run sequence 'sheet3_time_3'. (DELAY 2/30)
	sequence 'sheet3_time_3':
		material 'mat_screen03':
			time 0.375
		Run sequence 'sheet3_time_4'. (DELAY 2/30)
	sequence 'sheet3_time_4':
		material 'mat_screen03':
			time 0.5
		Run sequence 'sheet3_time_5'. (DELAY 2/30)
	sequence 'sheet3_time_5':
		material 'mat_screen03':
			time 0.625
		Run sequence 'sheet3_time_6'. (DELAY 2/30)
	sequence 'sheet3_time_6':
		material 'mat_screen03':
			time 0.75
		Run sequence 'sheet3_time_7'. (DELAY 2/30)
	sequence 'sheet3_time_7':
		material 'mat_screen03':
			time 0.875
		Run sequence 'sheet3_time_8'. (DELAY 2/30)
	sequence 'sheet3_time_8':
		material 'mat_screen03':
			time 0
		Run sequence 'sheet3_time_9'. (DELAY 2/30)
	sequence 'sheet3_time_9':
		material 'mat_screen03':
			time 0.125
		Run sequence 'sheet3_time_10'. (DELAY 2/30)
	sequence 'sheet3_time_10':
		material 'mat_screen03':
			time 0.25
		Run sequence 'sheet3_time_11'. (DELAY 2/30)
	sequence 'sheet3_time_11':
		material 'mat_screen03':
			time 0.375
		Run sequence 'sheet3_time_12'. (DELAY 2/30)
	sequence 'sheet3_time_12':
		material 'mat_screen03':
			time 0.5
		Run sequence 'sheet3_time_13'. (DELAY 2/30)
	sequence 'sheet3_time_13':
		material 'mat_screen03':
			time 0.625
		Run sequence 'sheet3_time_14'. (DELAY 2/30)
	sequence 'sheet3_time_14':
		material 'mat_screen03':
			time 0.75
		Run sequence 'sheet3_time_15'. (DELAY 2/30)
	sequence 'sheet3_time_15':
		material 'mat_screen03':
			time 0.875
		Run sequence 'start_uv_anim_'..pick('sheet_1','sheet_2','sheet_3','sheet_4','sheet_5','sheet_6','sheet_7','sheet_8','sheet_9','sheet_10','sheet_11'). (DELAY 2/30)
	sequence 'start_uv_anim_sheet_4':
		Disable object 'g_screen_off'.
		Disable object 'g_screen01'.
		Disable object 'g_screen02'.
		Disable object 'g_screen03'.
		Enable object 'g_screen04'.
		material 'mat_screen04':
			state 'pause'
		Run sequence 'sheet4_time_0'.
	sequence 'sheet4_time_0':
		material 'mat_screen04':
			time 0
		Run sequence 'sheet4_time_1'. (DELAY 2/30)
	sequence 'sheet4_time_1':
		material 'mat_screen04':
			time 0.125
		Run sequence 'sheet4_time_2'. (DELAY 2/30)
	sequence 'sheet4_time_2':
		material 'mat_screen04':
			time 0.25
		Run sequence 'sheet4_time_3'. (DELAY 2/30)
	sequence 'sheet4_time_3':
		material 'mat_screen04':
			time 0.375
		Run sequence 'sheet4_time_4'. (DELAY 2/30)
	sequence 'sheet4_time_4':
		material 'mat_screen04':
			time 0.5
		Run sequence 'sheet4_time_5'. (DELAY 2/30)
	sequence 'sheet4_time_5':
		material 'mat_screen04':
			time 0.625
		Run sequence 'sheet4_time_6'. (DELAY 2/30)
	sequence 'sheet4_time_6':
		material 'mat_screen04':
			time 0.75
		Run sequence 'sheet4_time_7'. (DELAY 2/30)
	sequence 'sheet4_time_7':
		material 'mat_screen04':
			time 0.875
		Run sequence 'sheet4_time_8'. (DELAY 2/30)
	sequence 'sheet4_time_8':
		material 'mat_screen04':
			time 0
		Run sequence 'sheet4_time_9'. (DELAY 2/30)
	sequence 'sheet4_time_9':
		material 'mat_screen04':
			time 0.125
		Run sequence 'sheet4_time_10'. (DELAY 2/30)
	sequence 'sheet4_time_10':
		material 'mat_screen04':
			time 0.25
		Run sequence 'sheet4_time_11'. (DELAY 2/30)
	sequence 'sheet4_time_11':
		material 'mat_screen04':
			time 0.375
		Run sequence 'sheet4_time_12'. (DELAY 2/30)
	sequence 'sheet4_time_12':
		material 'mat_screen04':
			time 0.5
		Run sequence 'sheet4_time_13'. (DELAY 2/30)
	sequence 'sheet4_time_13':
		material 'mat_screen04':
			time 0.625
		Run sequence 'sheet4_time_14'. (DELAY 2/30)
	sequence 'sheet4_time_14':
		material 'mat_screen04':
			time 0.75
		Run sequence 'sheet4_time_15'. (DELAY 2/30)
	sequence 'sheet4_time_15':
		material 'mat_screen04':
			time 0.875
		Run sequence 'start_uv_anim_'..pick('sheet_1','sheet_2','sheet_3','sheet_4','sheet_5','sheet_6','sheet_7','sheet_8','sheet_9','sheet_10','sheet_11'). (DELAY 2/30)
	sequence 'start_uv_anim_sheet_5':
		Disable object 'g_screen_off'.
		Enable object 'g_screen01'.
		Disable object 'g_screen02'.
		Disable object 'g_screen03'.
		Disable object 'g_screen04'.
		material 'mat_screen01':
			state 'pause'
		Run sequence 'sheet5_time_0'.
	sequence 'sheet5_time_0':
		material 'mat_screen01':
			time 0.5
		Run sequence 'sheet5_time_1'. (DELAY 15/30)
	sequence 'sheet5_time_1':
		material 'mat_screen01':
			time 0.375
		Run sequence 'sheet5_time_2'. (DELAY 15/30)
	sequence 'sheet5_time_2':
		material 'mat_screen01':
			time 0.5
		Run sequence 'sheet5_time_3'. (DELAY 15/30)
	sequence 'sheet5_time_3':
		material 'mat_screen01':
			time 0.375
		Run sequence 'sheet5_time_4'. (DELAY 15/30)
	sequence 'sheet5_time_4':
		material 'mat_screen01':
			time 0.5
		Run sequence 'sheet5_time_5'. (DELAY 15/30)
	sequence 'sheet5_time_5':
		material 'mat_screen01':
			time 0.375
		Run sequence 'sheet5_time_6'. (DELAY 15/30)
	sequence 'sheet5_time_6':
		material 'mat_screen01':
			time 0.5
		Run sequence 'sheet5_time_7'. (DELAY 15/30)
	sequence 'sheet5_time_7':
		material 'mat_screen01':
			time 0.375
		Run sequence 'sheet5_time_8'. (DELAY 15/30)
	sequence 'sheet5_time_8':
		material 'mat_screen01':
			time 0.5
		Run sequence 'start_uv_anim_'..pick('sheet_1','sheet_2','sheet_3','sheet_4','sheet_5','sheet_6','sheet_7','sheet_8','sheet_9','sheet_10','sheet_11'). (DELAY 2/30)
	sequence 'start_uv_anim_sheet_6':
		Disable object 'g_screen_off'.
		Disable object 'g_screen01'.
		Enable object 'g_screen02'.
		Disable object 'g_screen03'.
		Disable object 'g_screen04'.
		material 'mat_screen02':
			state 'pause'
		Run sequence 'sheet6_time_0'.
	sequence 'sheet6_time_0':
		material 'mat_screen02':
			time 0.75
		Run sequence 'sheet6_time_1'. (DELAY 2/30)
	sequence 'sheet6_time_1':
		material 'mat_screen02':
			time 0
		Run sequence 'start_uv_anim_'..pick('sheet_1','sheet_2','sheet_3','sheet_4','sheet_5','sheet_6','sheet_7','sheet_8','sheet_9','sheet_10','sheet_11'). (DELAY 60/30)
	sequence 'start_uv_anim_sheet_7':
		Disable object 'g_screen_off'.
		Disable object 'g_screen01'.
		Enable object 'g_screen02'.
		Disable object 'g_screen03'.
		Disable object 'g_screen04'.
		material 'mat_screen02':
			state 'pause'
		Run sequence 'sheet7_time_0'.
	sequence 'sheet7_time_0':
		material 'mat_screen02':
			time 0.75
		Run sequence 'sheet7_time_1'. (DELAY 2/30)
	sequence 'sheet7_time_1':
		material 'mat_screen02':
			time 0.125
		Run sequence 'start_uv_anim_'..pick('sheet_1','sheet_2','sheet_3','sheet_4','sheet_5','sheet_6','sheet_7','sheet_8','sheet_9','sheet_10','sheet_11'). (DELAY 60/30)
	sequence 'start_uv_anim_sheet_8':
		Disable object 'g_screen_off'.
		Disable object 'g_screen01'.
		Enable object 'g_screen02'.
		Disable object 'g_screen03'.
		Disable object 'g_screen04'.
		material 'mat_screen02':
			state 'pause'
		Run sequence 'sheet8_time_0'.
	sequence 'sheet8_time_0':
		material 'mat_screen02':
			time 0.75
		Run sequence 'sheet8_time_1'. (DELAY 2/30)
	sequence 'sheet8_time_1':
		material 'mat_screen02':
			time 0.25
		Run sequence 'start_uv_anim_'..pick('sheet_1','sheet_2','sheet_3','sheet_4','sheet_5','sheet_6','sheet_7','sheet_8','sheet_9','sheet_10','sheet_11'). (DELAY 60/30)
	sequence 'start_uv_anim_sheet_9':
		Disable object 'g_screen_off'.
		Disable object 'g_screen01'.
		Enable object 'g_screen02'.
		Disable object 'g_screen03'.
		Disable object 'g_screen04'.
		material 'mat_screen02':
			state 'pause'
		Run sequence 'sheet9_time_0'.
	sequence 'sheet9_time_0':
		material 'mat_screen02':
			time 0.75
		Run sequence 'sheet9_time_1'. (DELAY 2/30)
	sequence 'sheet9_time_1':
		material 'mat_screen02':
			time 0.375
		Run sequence 'start_uv_anim_'..pick('sheet_1','sheet_2','sheet_3','sheet_4','sheet_5','sheet_6','sheet_7','sheet_8','sheet_9','sheet_10','sheet_11'). (DELAY 60/30)
	sequence 'start_uv_anim_sheet_10':
		Disable object 'g_screen_off'.
		Disable object 'g_screen01'.
		Enable object 'g_screen02'.
		Disable object 'g_screen03'.
		Disable object 'g_screen04'.
		material 'mat_screen02':
			state 'pause'
		Run sequence 'sheet10_time_0'.
	sequence 'sheet10_time_0':
		material 'mat_screen02':
			time 0.75
		Run sequence 'sheet10_time_1'. (DELAY 2/30)
	sequence 'sheet10_time_1':
		material 'mat_screen02':
			time 0.5
		Run sequence 'start_uv_anim_'..pick('sheet_1','sheet_2','sheet_3','sheet_4','sheet_5','sheet_6','sheet_7','sheet_8','sheet_9','sheet_10','sheet_11'). (DELAY 60/30)
	sequence 'start_uv_anim_sheet_11':
		Disable object 'g_screen_off'.
		Disable object 'g_screen01'.
		Enable object 'g_screen02'.
		Disable object 'g_screen03'.
		Disable object 'g_screen04'.
		material 'mat_screen02':
			state 'pause'
		Run sequence 'sheet11_time_0'.
	sequence 'sheet11_time_0':
		material 'mat_screen02':
			time 0.75
		Run sequence 'sheet11_time_1'. (DELAY 2/30)
	sequence 'sheet11_time_1':
		material 'mat_screen02':
			time 0.625
		Run sequence 'start_uv_anim_'..pick('sheet_1','sheet_2','sheet_3','sheet_4','sheet_5','sheet_6','sheet_7','sheet_8','sheet_9','sheet_10','sheet_11'). (DELAY 60/30)
	sequence 'state_screen_off':
		Enable object 'g_screen_off'.
		Disable object 'g_screen01'.
		Disable object 'g_screen02'.
		Disable object 'g_screen03'.
		Disable object 'g_screen04'.
