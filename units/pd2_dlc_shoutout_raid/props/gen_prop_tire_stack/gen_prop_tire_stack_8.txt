unit:
	sequence 'make_dynamic':
		Disable body 'body_static'.
		Enable body 'body_dynamic_01'.
		Enable body 'body_dynamic_02'.
		Enable body 'body_dynamic_03'.
		Enable body 'body_dynamic_04'.
		Enable body 'body_dynamic_05'.
		Enable body 'body_dynamic_06'.
		Enable body 'body_dynamic_07'.
		Enable body 'body_dynamic_08'.
	body 'body_static'
		Upon receiving 4 bullet hits or 50 explosion damage, execute:
			Run sequence 'make_dynamic'.
		Upon receiving 100 damage, execute:
			Run sequence 'make_dynamic'. (DELAY 3/30)
	sequence 'car_destructable':
		Run sequence 'make_dynamic'.
