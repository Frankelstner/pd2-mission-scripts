unit:
	body 'body_static'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'int_seq_burn'.
			Run sequence 'int_seq_reset_bullet_endurance'.
		Upon receiving 100 damage, execute:
			Run sequence 'int_seq_explode_barrel'. (DELAY 3/30)
	body 'body_dynamic'
		Upon receiving 100 damage, execute:
			Run sequence 'int_seq_explode_barrel'. (DELAY 3/30)
		Upon receiving 10 collision damage, execute:
			Run sequence 'int_seq_reset_collision_endurance'.
			Run sequence 'int_seq_car_hit'.
	sequence 'int_seq_make_dynamic':
		TRIGGER TIMES 1
		slot:
			slot 11
		Disable body 'body_static'.
		Enable body 'body_dynamic'.
	sequence 'int_seq_burn':
		effect 'effects/payday2/particles/fire/small_burning':
			align dest_normal
			parent object('e_effect')
			position pos - object_pos('e_effect')
			store_id_list_var 'burning_id_list'
		Run sequence 'int_seq_explode_barrel'. (DELAY 4)
	sequence 'int_seq_explode_barrel':
		TRIGGER TIMES 1
		stop_effect:
			id_list_var 'burning_id_list'
			instant True
		Disable body 'body_static'.
		Disable body 'body_dynamic'.
		Call function: base.detonate(object_pos( 'e_effect' ),700,250,5)
		Play audio 'hit_oil_drum' at 'e_effect'.
		Hide graphic_group 'grp_g_barrel'.
		Disable decal_mesh 'dm_metal_catwalk'.
		Disable object 's_s'.
		spawn_unit 'units/pd2_dlc_shoutout_raid/props/gen_explosive_barrel/spawn_barrel_debris':
			position object_pos('e_effect')
			rotation rot(0, 0, rand()*360)
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('e_effect') ),50,3).
	sequence 'int_seq_car_hit':
		Play audio 'hit_oil_drum' at 'e_effect'.
	sequence 'int_seq_reset_collision_endurance':
		Set collision damage to 0.
	sequence 'int_seq_reset_bullet_endurance':
		Set bullet damage to 0.
	sequence 'car_destructable':
		Run sequence 'int_seq_make_dynamic'.
