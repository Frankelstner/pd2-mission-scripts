unit:
	sequence 'startup_seq':
		EXECUTE ON STARTUP
		Play audio 'geiger_meter_loop_start' at 'snd_gieger'.
		startup True
	sequence 'state_interact_enable':
		Enable interaction.
	sequence 'state_interact_disable':
		Disable interaction.
	sequence 'state_vis_hide':
		Disable interaction.
		Disable object 'g_g'.
		Disable body 'body_static'.
	sequence 'state_vis_show':
		Enable object 'g_g'.
		Enable body 'body_static'.
	sequence 'interact':
		Play audio 'geiger_meter_loop_stop' at 'snd_gieger'.
		Run sequence 'state_vis_hide'.
	sequence 'load'.
