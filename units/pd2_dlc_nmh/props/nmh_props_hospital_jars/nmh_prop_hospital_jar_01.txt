unit:
	sequence 'int_seq_break':
		Disable object 'g_jar_05'.
		Disable body 'body_static'.
		effect 'effects/particles/dest/glass_small_dest_less_smoke':
			parent object( 'rp_nmh_prop_hospital_jar_01' )
			position v()
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break'.
