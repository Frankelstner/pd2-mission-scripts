unit:
	sequence '	enable_interaction	':
		Enable interaction.
		Show graphic_group 'grp_outline'.
		Disable object 'g_screen_progress'.
		Enable object 'g_screen_background'.
		Disable object 'g_screen_fail'.
		Disable object 'g_screen_cleaning'.
		Enable object 'g_screen_insert_sample'.
		Disable object 'g_screen_success'.
		Disable object 'g_progress_bar'.
		Disable object 'g_empty_background'.
	sequence '	disable_interaction		':
		Disable interaction.
		Hide graphic_group 'grp_outline'.
		Disable object 'g_screen_progress'.
		Disable object 'g_screen_background'.
		Disable object 'g_screen_fail'.
		Disable object 'g_screen_cleaning'.
		Disable object 'g_screen_insert_sample'.
		Disable object 'g_screen_success'.
		Disable object 'g_progress_bar'.
		Enable object 'g_empty_background'.
		Play audio 'hos_bloodtest_end' at 'interact'.
	sequence '	hide	':
		Disable body 'body_lid'.
		Disable body 'body_base'.
		Disable object 'g_screen_progress'.
		Disable object 'g_screen_background'.
		Disable object 'g_screen_fail'.
		Disable object 'g_screen_cleaning'.
		Disable object 'g_screen_insert_sample'.
		Disable object 'g_screen_success'.
		Disable object 'g_lid'.
		Disable object 'g_centrifuge'.
		Disable object 'g_blood_sample'.
		Disable object 'g_progress_bar'.
		Disable object 'g_base'.
	sequence '	show	':
		Enable body 'body_lid'.
		Enable body 'body_base'.
		Disable object 'g_screen_progress'.
		Disable object 'g_screen_background'.
		Disable object 'g_screen_fail'.
		Disable object 'g_screen_cleaning'.
		Enable object 'g_screen_insert_sample'.
		Disable object 'g_screen_success'.
		Enable object 'g_lid'.
		Enable object 'g_centrifuge'.
		Disable object 'g_blood_sample'.
		Enable object 'g_progress_bar'.
		Enable object 'g_base'.
	sequence 'int_seq_anim_cleaning_start':
		material 'mat_blood_splatter':
			state 'play -0.2'
			time 1
	sequence 'int_seq_anim_break_start':
		material 'mat_blood_splatter':
			state 'play 15'
			time 0
	sequence 'int_seq_close_lid':
		Enable animation_group 'anim_open':
			from 0/30
			to 40/30
	sequence 'int_seq_open_lid':
		Enable animation_group 'anim_open':
			from 40/30
			speed -1
			to 0/30
	sequence 'int_seq_spin':
		Enable animation_group 'anim_spin':
			speed 1
		Play audio 'hos_bloodtest_start' at 'interact'.
	sequence 'int_seq_stop_spin':
		Enable animation_group 'anim_spin':
			speed 0
		Play audio 'hos_bloodtest_end' at 'interact'.
	sequence 'int_seq_start_progress_bar':
		Enable animation_group 'anim_progress_bar':
			from 1/30
			to 900/30
		Enable object 'g_progress_bar'.
	sequence 'int_seq_restart_progress_bar':
		Enable animation_group 'anim_progress_bar':
			from 0/30
			to 0/30
		Enable object 'g_progress_bar'.
	sequence 'interact':
		Disable object 'g_blood_sample'.
		Run sequence 'int_seq_spin'. (DELAY 45/30)
		Run sequence 'int_seq_close_lid'.
		Run sequence 'int_seq_start_progress_bar'. (DELAY 45/30)
		Enable object 'g_screen_progress'.
		Disable object 'g_screen_fail'.
		Disable object 'g_screen_cleaning'.
		Disable object 'g_screen_insert_sample'.
		Disable object 'g_screen_success'.
	sequence 'fail':
		Run sequence 'int_seq_restart_progress_bar'.
		Disable object 'g_blood_sample'.
		Disable object 'g_screen_progress'.
		Disable object 'g_progress_bar'.
		Disable object 'g_screen_background'.
		Enable object 'g_screen_fail'.
		Disable object 'g_screen_cleaning'.
		Disable object 'g_screen_insert_sample'.
		Disable object 'g_screen_success'.
		Enable object 'g_blood'.
		Enable object 'g_lid_blood'.
		Enable object 'g_centrifuge_blood'.
		Run sequence 'int_seq_anim_break_start'.
		Play audio 'hos_bloodtest_denied' at 'interact'.
	sequence 'cleaning':
		Run sequence 'int_seq_stop_spin'.
		Run sequence 'done_cleaning'. (DELAY 10)
		Disable object 'g_progress_bar'.
		Disable object 'g_screen_progress'.
		Disable object 'g_screen_background'.
		Disable object 'g_screen_fail'.
		Enable object 'g_screen_cleaning'.
		Disable object 'g_screen_insert_sample'.
		Disable object 'g_screen_success'.
		Run sequence 'int_seq_anim_cleaning_start'.
		Play audio 'hos_bloodtest_end' at 'interact'.
	sequence 'success':
		Run sequence 'int_seq_stop_spin'.
		Run sequence 'int_seq_open_lid'.
		Disable object 'g_blood_sample'.
		Disable object 'g_screen_progress'.
		Disable object 'g_screen_fail'.
		Disable object 'g_screen_cleaning'.
		Disable object 'g_screen_insert_sample'.
		Enable object 'g_screen_success'.
		Play audio 'hos_bloodtest_accepted' at 'interact'.
	sequence 'done_cleaning':
		Run sequence 'int_seq_open_lid'.
		Run sequence 'int_seq_restart_progress_bar'.
		Enable interaction. (DELAY 40/30)
		Show graphic_group 'grp_outline'.
		Disable object 'g_screen_progress'.
		Disable object 'g_progress_bar'.
		Enable object 'g_screen_background'.
		Disable object 'g_screen_fail'.
		Disable object 'g_screen_cleaning'.
		Enable object 'g_screen_insert_sample'.
		Disable object 'g_screen_success'.
		Disable object 'g_blood'.
		Disable object 'g_lid_blood'.
		Disable object 'g_centrifuge_blood'.
