unit:
	body 'window1'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_01'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_02'.
	sequence 'destroy_01':
		TRIGGER TIMES 1
		Disable object 'g_1'.
		Enable object 'g_1_dmg'.
		Play audio 'glass_crack' at 'e_1'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_1' (alarm reason: 'glass').
	sequence 'destroy_02':
		TRIGGER TIMES 1
		Disable object 'g_1'.
		Disable object 'g_1_dmg'.
		Play audio 'window_medium_shatter' at 'e_1'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_1')
			position v()
		Disable body 'window1'.
		Disable decal_mesh 'dm_glass_breakable_1'.
		Cause alert with 12 m radius.
	body 'window2'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_03'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_04'.
	sequence 'destroy_03':
		TRIGGER TIMES 1
		Disable object 'g_2'.
		Enable object 'g_2_dmg'.
		Play audio 'glass_crack' at 'e_2'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_2' (alarm reason: 'glass').
	sequence 'destroy_04':
		TRIGGER TIMES 1
		Disable object 'g_2'.
		Disable object 'g_2_dmg'.
		Play audio 'window_medium_shatter' at 'e_2'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_2')
			position v()
		Disable body 'window2'.
		Disable decal_mesh 'dm_glass_breakable_2'.
		Cause alert with 12 m radius.
	body 'window3'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_05'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_06'.
	sequence 'destroy_05':
		TRIGGER TIMES 1
		Disable object 'g_3'.
		Enable object 'g_3_dmg'.
		Play audio 'glass_crack' at 'e_3'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_3' (alarm reason: 'glass').
	sequence 'destroy_06':
		TRIGGER TIMES 1
		Disable object 'g_3'.
		Disable object 'g_3_dmg'.
		Play audio 'window_medium_shatter' at 'e_3'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_3')
			position v()
		Disable body 'window3'.
		Disable decal_mesh 'dm_glass_breakable_3'.
		Cause alert with 12 m radius.
	body 'window4'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_07'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_08'.
	sequence 'destroy_07':
		TRIGGER TIMES 1
		Disable object 'g_4'.
		Enable object 'g_4_dmg'.
		Play audio 'glass_crack' at 'e_4'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_4' (alarm reason: 'glass').
	sequence 'destroy_08':
		TRIGGER TIMES 1
		Disable object 'g_4'.
		Disable object 'g_4_dmg'.
		Play audio 'window_medium_shatter' at 'e_4'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_4')
			position v()
		Disable body 'window4'.
		Disable decal_mesh 'dm_glass_breakable_4'.
		Cause alert with 12 m radius.
