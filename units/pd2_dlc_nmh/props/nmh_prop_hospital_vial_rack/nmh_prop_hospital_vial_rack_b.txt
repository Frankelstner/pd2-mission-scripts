unit:
	body 'body_vials_01'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_vials_01'.
	body 'body_vials_02'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_vials_02'.
	body 'body_vials_03'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_vials_03'.
	sequence 'int_seq_break_vials_01':
		Disable object 'g_tubes_01'.
		effect 'effects/particles/dest/glass_vial_filled':
			parent object( 'e_effect_01' )
			position v()
	sequence 'int_seq_break_vials_02':
		Disable object 'g_tubes_02'.
		effect 'effects/particles/dest/glass_vial':
			parent object( 'e_effect_02' )
			position v()
	sequence 'int_seq_break_vials_03':
		Disable object 'g_tubes_03'.
		effect 'effects/particles/dest/glass_vial':
			parent object( 'e_effect_03' )
			position v()
