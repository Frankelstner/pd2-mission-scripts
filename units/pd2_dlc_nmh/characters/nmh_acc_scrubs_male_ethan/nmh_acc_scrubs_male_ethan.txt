unit:
	sequence 'pick_nametag':
		EXECUTE ON STARTUP
		Run sequence 'set_'..pick('par','tob','dav','sim','luc','iva').
		startup True
	sequence 'set_par':
		Enable object 'g_par'.
		Disable object 'g_tobias'.
		Disable object 'g_david'.
		Disable object 'g_simon'.
		Disable object 'g_lucas'.
		Disable object 'g_ivan'.
	sequence 'set_tob':
		Disable object 'g_par'.
		Enable object 'g_tobias'.
		Disable object 'g_david'.
		Disable object 'g_simon'.
		Disable object 'g_lucas'.
		Disable object 'g_ivan'.
	sequence 'set_dav':
		Disable object 'g_par'.
		Disable object 'g_tobias'.
		Enable object 'g_david'.
		Disable object 'g_simon'.
		Disable object 'g_lucas'.
		Disable object 'g_ivan'.
	sequence 'set_sim':
		Disable object 'g_par'.
		Disable object 'g_tobias'.
		Disable object 'g_david'.
		Enable object 'g_simon'.
		Disable object 'g_lucas'.
		Disable object 'g_ivan'.
	sequence 'set_luc':
		Disable object 'g_par'.
		Disable object 'g_tobias'.
		Disable object 'g_david'.
		Disable object 'g_simon'.
		Enable object 'g_lucas'.
		Disable object 'g_ivan'.
	sequence 'set_iva':
		Disable object 'g_par'.
		Disable object 'g_tobias'.
		Disable object 'g_david'.
		Disable object 'g_simon'.
		Disable object 'g_lucas'.
		Enable object 'g_ivan'.
