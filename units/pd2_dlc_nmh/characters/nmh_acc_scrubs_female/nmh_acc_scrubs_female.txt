unit:
	sequence 'pick_nametag':
		EXECUTE ON STARTUP
		Run sequence 'set_'..pick('mel','mal','mik').
		startup True
	sequence 'set_mel':
		Disable object 'g_anna'.
		Enable object 'g_melanie'.
		Disable object 'g_david'.
		Disable object 'g_simon'.
		Disable object 'g_lucas'.
		Disable object 'g_malin'.
	sequence 'set_mal':
		Disable object 'g_anna'.
		Disable object 'g_melanie'.
		Disable object 'g_david'.
		Disable object 'g_simon'.
		Disable object 'g_lucas'.
		Enable object 'g_malin'.
	sequence 'set_mik':
		Disable object 'g_anna'.
		Disable object 'g_melanie'.
		Enable object 'g_david'.
		Disable object 'g_simon'.
		Disable object 'g_lucas'.
		Disable object 'g_malin'.
