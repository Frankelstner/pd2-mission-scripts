unit:
	door_anim = 0
	sequence 'show':
		Enable body 'body_static'.
		Enable body 'window1'.
		Enable decal_mesh 'dm_glass_unbreakable'.
		Enable decal_mesh 'dm_metal'.
		Enable decal_mesh 'dm_plastic'.
		Enable object 'g_glass_01'.
		Disable object 'g_glass_01_dmg'.
		Enable object 'g_g'.
	sequence 'hide':
		Disable body 'body_static'.
		Disable body 'window1'.
		Disable decal_mesh 'dm_glass_unbreakable'.
		Disable decal_mesh 'dm_metal'.
		Disable decal_mesh 'dm_plastic'.
		Disable object 'g_glass_01'.
		Disable object 'g_glass_01_dmg'.
		Disable object 'g_g'.
	sequence 'anim_open_door_90_in':
		Enable animation_group 'anim_door':
			from 0/30
			to 34/30
		door_anim = 1
	sequence 'anim_open_door_90_out':
		Enable animation_group 'anim_door':
			from 35/30
			to 70/30
		door_anim = 2
	sequence 'anim_open_door_96_in':
		Enable animation_group 'anim_door':
			from 71/30
			to 96/30
		door_anim = 3
	sequence 'anim_open_door_96_out':
		Enable animation_group 'anim_door':
			from 97/30
			to 128/30
		door_anim = 4
	sequence 'int_seq_anim_close_door_90_in':
		Enable animation_group 'anim_door':
			from 34/30
			speed -1
			to 0/30
	sequence 'int_seq_anim_close_door_90_out':
		Enable animation_group 'anim_door':
			from 70/30
			speed -1
			to 35/30
	sequence 'int_seq_anim_close_door_96_in':
		Enable animation_group 'anim_door':
			from 96/30
			speed -1
			to 71/30
	sequence 'int_seq_anim_close_door_96_out':
		Enable animation_group 'anim_door':
			from 128/30
			speed -1
			to 97/30
	sequence 'anim_close':
		If door_anim == 1: Run sequence 'int_seq_anim_close_door_90_in'.
		If door_anim == 2: Run sequence 'int_seq_anim_close_door_90_out'.
		If door_anim == 3: Run sequence 'int_seq_anim_close_door_96_in'.
		If door_anim == 4: Run sequence 'int_seq_anim_close_door_96_out'.
	sequence 'state_door_close':
		Enable animation_group 'anim_door':
			from 0/30
			to 0/30
	body 'window1'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_01'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_02'.
	sequence 'destroy_01':
		TRIGGER TIMES 1
		Disable object 'g_glass_01'.
		Enable object 'g_glass_01_dmg'.
		Play audio 'glass_crack' at 'e_glass_01'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_01' (alarm reason: 'glass').
	sequence 'destroy_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_01'.
		Disable object 'g_glass_01_dmg'.
		Play audio 'window_medium_shatter' at 'e_glass_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_01')
			position v()
		Disable body 'window1'.
		Disable decal_mesh 'dm_glass_unbreakable'.
		Cause alert with 12 m radius.
