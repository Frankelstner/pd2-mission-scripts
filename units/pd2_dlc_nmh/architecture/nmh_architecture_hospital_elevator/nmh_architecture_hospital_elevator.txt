unit:
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'anim_open_hatch':
		Enable animation_group 'anim_hatch':
			from 0/30
			to 57/30
	sequence 'anim_open_hatch_var':
		Enable animation_group 'anim_hatch':
			from 58/30
			to 114/30
	sequence 'interact'.
