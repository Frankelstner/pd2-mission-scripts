unit:
	sequence 'destroy_01':
		TRIGGER TIMES 1
		Disable object 'g_glass_01'.
		Disable decal_mesh 'dm_glass_breakable_01'.
		Play audio 'window_large_shatter' at 'e_glass_01'.
		effect 'effects/payday2/particles/window/mallcrasher_window_large':
			parent object('e_glass_01')
			position v()
		Disable body 'window_glass_01'.
		Cause alert with 12 m radius.
	sequence 'destroy_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_02'.
		Disable decal_mesh 'dm_glass_breakable_02'.
		Play audio 'window_large_shatter' at 'e_glass_02'.
		effect 'effects/payday2/particles/window/mallcrasher_window_large':
			parent object('e_glass_02')
			position v()
		Disable body 'window_glass_02'.
		Cause alert with 12 m radius.
	body 'window_glass_01'
		Upon receiving 4 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_01'.
	body 'window_glass_02'
		Upon receiving 4 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_02'.
