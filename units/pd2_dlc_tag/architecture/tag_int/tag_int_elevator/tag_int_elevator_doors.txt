unit:
	sequence 'state_interaction_enabled':
		Enable interaction.
		Enable object 'g_outline_lod0'.
	sequence 'state_interaction_disabled':
		Disable interaction.
		Disable object 'g_outline_lod0'.
	sequence 'state_button_glow_enabled':
		Enable object 'g_button_glow'.
	sequence 'state_button_glow_disabled':
		Disable object 'g_button_glow'.
	sequence 'doors_open':
		Enable animation_group 'anim_door_left':
			from 0/30
			speed 1
			to 60/30
		Enable animation_group 'anim_door_right':
			from 0/30
			speed 1
			to 60/30
		Play audio 'elevator_doors_open' at 'interaction'.
	sequence 'doors_close':
		Enable animation_group 'anim_door_left':
			from 61/30
			speed 1
			to 121/30
		Enable animation_group 'anim_door_right':
			from 61/30
			speed 1
			to 121/30
		Play audio 'elevator_doors_close' at 'interaction'.
	sequence 'interact':
		Enable animation_group 'anim_door_left':
			from 0/30
			speed 1
			to 60/30
		Enable animation_group 'anim_door_right':
			from 0/30
			speed 1
			to 60/30
		Play audio 'elevator_doors_open' at 'interaction'.
		Enable object 'g_button_glow'.
