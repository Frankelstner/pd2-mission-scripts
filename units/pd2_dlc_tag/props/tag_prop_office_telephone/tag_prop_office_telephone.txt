unit:
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'show':
		Show graphic_group 'grp_phone'.
		Enable decal_mesh 'dm_plastic'.
		Enable body 'body_static'.
	sequence 'hide':
		Hide graphic_group 'grp_phone'.
		Disable decal_mesh 'dm_plastic'.
		Disable body 'body_static'.
	sequence 'interact'.
