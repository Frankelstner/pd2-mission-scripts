unit:
	sequence 'hide':
		Disable object 'g_g'.
		Disable object 'g_g_01'.
		Disable object 'g_g_02'.
		Disable object 'g_g_03'.
		Disable object 'g_g_04'.
		Disable object 'g_g_05'.
		Disable object 'g_g_06'.
	sequence 'show':
		Enable object 'g_g'.
		Disable object 'g_g_01'.
		Disable object 'g_g_02'.
		Disable object 'g_g_03'.
		Disable object 'g_g_04'.
		Disable object 'g_g_05'.
		Disable object 'g_g_06'.
	sequence 'operations_room':
		Enable object 'g_g'.
		Enable object 'g_g_01'.
		Disable object 'g_g_02'.
		Disable object 'g_g_03'.
		Disable object 'g_g_04'.
		Disable object 'g_g_05'.
		Disable object 'g_g_06'.
	sequence 'kitchen':
		Enable object 'g_g'.
		Disable object 'g_g_01'.
		Enable object 'g_g_02'.
		Disable object 'g_g_03'.
		Disable object 'g_g_04'.
		Disable object 'g_g_05'.
		Disable object 'g_g_06'.
	sequence 'training_room':
		Enable object 'g_g'.
		Disable object 'g_g_01'.
		Disable object 'g_g_02'.
		Enable object 'g_g_03'.
		Disable object 'g_g_04'.
		Disable object 'g_g_05'.
		Disable object 'g_g_06'.
	sequence 'server_room':
		Enable object 'g_g'.
		Disable object 'g_g_01'.
		Disable object 'g_g_02'.
		Disable object 'g_g_03'.
		Enable object 'g_g_04'.
		Disable object 'g_g_05'.
		Disable object 'g_g_06'.
	sequence 'storage_room':
		Enable object 'g_g'.
		Disable object 'g_g_01'.
		Disable object 'g_g_02'.
		Disable object 'g_g_03'.
		Disable object 'g_g_04'.
		Enable object 'g_g_05'.
		Disable object 'g_g_06'.
	sequence 'hr_room':
		Enable object 'g_g'.
		Disable object 'g_g_01'.
		Disable object 'g_g_02'.
		Disable object 'g_g_03'.
		Disable object 'g_g_04'.
		Disable object 'g_g_05'.
		Enable object 'g_g_06'.
