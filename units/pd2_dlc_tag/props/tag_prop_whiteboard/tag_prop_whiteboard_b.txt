unit:
	sequence 'anim_move_01':
		Enable animation_group 'anim_move':
			from 0/30
			to 80/30
		Play audio 'bar_move_furniture_finish_whiteboard' at 'snd'.
	sequence 'anim_move_02':
		Enable animation_group 'anim_move':
			from 81/30
			to 160/30
		Play audio 'bar_move_furniture_finish_whiteboard' at 'snd'.
	sequence 'hide':
		Disable body 'body_static'.
		Disable decal_mesh 'dm_plastic'.
		Disable decal_mesh 'dm_metal'.
		Hide graphic_group 'grp_board'.
	sequence 'show':
		Enable body 'body_static'.
		Enable decal_mesh 'dm_metal'.
		Enable decal_mesh 'dm_plastic'.
		Show graphic_group 'grp_board'.
