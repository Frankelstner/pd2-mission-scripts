unit:
	sequence 'hide':
		Disable object 'g_g'.
		Disable body 'static_body'.
	sequence 'show':
		Enable object 'g_g'.
		Enable body 'static_body'.
	sequence 'interact'.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
