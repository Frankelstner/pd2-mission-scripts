unit:
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'hide':
		Disable object 'g_g'.
		Disable object 'g_sallad'.
		Disable body 'static_body'.
	sequence 'show':
		Enable object 'g_g'.
		Enable object 'g_sallad'.
		Enable body 'static_body'.
	sequence 'hide_chimichanga':
		Disable object 'g_g'.
	sequence 'interact':
		Play audio 'pick_up_plate' at 'rp_tag_pku_chimichanga'.
	sequence 'load':
		Play audio 'pick_up_plate' at 'rp_tag_pku_chimichanga'.
