unit:
	sequence 'A1':
		Enable animation_group 'anim':
			from 0/30
			to 250/30
		Run sequence 'done_A1'. (DELAY 250/30)
	sequence 'done_A1'.
	sequence 'A1_reverse':
		Enable animation_group 'anim':
			from 250/30
			speed -1
			to 0/30
		Run sequence 'done_A1_reverse'. (DELAY 250/30)
	sequence 'done_A1_reverse'.
	sequence 'A2':
		Enable animation_group 'anim':
			from 260/30
			to 1100/30
		Run sequence 'done_A2'. (DELAY 840/30)
	sequence 'done_A2'.
	sequence 'A2_reverse':
		Enable animation_group 'anim':
			from 1100/30
			speed -1
			to 260/30
		Run sequence 'done_A2_reverse'. (DELAY 840/30)
	sequence 'done_A2_reverse'.
	sequence 'A3':
		Enable animation_group 'anim':
			from 1110/30
			to 1510/30
		Run sequence 'done_A3'. (DELAY 400/30)
	sequence 'done_A3'.
	sequence 'A3_reverse':
		Enable animation_group 'anim':
			from 1510/30
			speed -1
			to 1110/30
		Run sequence 'done_A3_reverse'. (DELAY 400/30)
	sequence 'done_A3_reverse'.
	sequence 'B1':
		Enable animation_group 'anim':
			from 1520/30
			to 2010/30
		Run sequence 'done_B1'. (DELAY 490/30)
	sequence 'done_B1'.
	sequence 'B1_reverse':
		Enable animation_group 'anim':
			from 2010/30
			speed -1
			to 1520/30
		Run sequence 'done_B1_reverse'. (DELAY 490/30)
	sequence 'done_B1_reverse'.
	sequence 'B2':
		Enable animation_group 'anim':
			from 2020/30
			to 3010/30
		Run sequence 'done_B2'. (DELAY 990/30)
	sequence 'done_B2'.
	sequence 'B2_reverse':
		Enable animation_group 'anim':
			from 3010/30
			speed -1
			to 2020/30
		Run sequence 'done_B2_reverse'. (DELAY 990/30)
	sequence 'done_B2_reverse'.
	sequence 'C1':
		Enable animation_group 'anim':
			from 3020/30
			to 4010/30
		Run sequence 'done_C1'. (DELAY 990/30)
	sequence 'done_C1'.
	sequence 'C1_reverse':
		Enable animation_group 'anim':
			from 4010/30
			speed -1
			to 3020/30
		Run sequence 'done_C1_reverse'. (DELAY 990/30)
	sequence 'done_C1_reverse'.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'show':
		Show graphic_group 'lootbag_grp'.
		Enable body 'static_body'.
	sequence 'hide':
		Hide graphic_group 'lootbag_grp'.
		Disable interaction.
		Disable body 'static_body'.
