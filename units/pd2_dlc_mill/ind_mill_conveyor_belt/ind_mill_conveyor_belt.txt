unit:
	sequence 'anim_belt_start':
		Enable animation_group 'anim_belt':
			from 0/30
			loop True
			to 35/30
	sequence 'anim_belt_stop':
		Enable animation_group 'anim_belt':
			speed 0
	sequence 'anim_belt_resume':
		Enable animation_group 'anim_belt':
			speed 1
	sequence 'anim_belt_reverse':
		Enable animation_group 'anim_belt':
			speed -1
	sequence 'turn_0':
		Enable animation_group 'anim_root':
			from 2/30
			to 2/30
	sequence 'turn_90':
		Enable animation_group 'anim_root':
			from 8/30
			to 8/30
	sequence 'turn_180':
		Enable animation_group 'anim_root':
			from 17/30
			to 17/30
	sequence 'turn_270':
		Enable animation_group 'anim_root':
			from 27/30
			to 27/30
