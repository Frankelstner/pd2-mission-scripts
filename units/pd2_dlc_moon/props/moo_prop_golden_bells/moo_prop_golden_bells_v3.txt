unit:
	sequence 'reset':
		Enable animation_group 'anim_bells':
			from 0/30
			speed 1.5
			to 22/30
		Play audio 'moon_bell_impact_03' at 'rp_moo_prop_golden_bells_v3'.
		Set damage damage to 0. (DELAY 15/30)
	body 'body_static'
		Upon receiving 10 damage, execute:
			Run sequence 'reset'.
