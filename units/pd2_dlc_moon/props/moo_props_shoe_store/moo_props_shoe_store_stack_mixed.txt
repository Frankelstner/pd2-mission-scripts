unit:
	sequence 'make_dynamic':
		Disable body 'static_body'.
		Enable body 'dynamic_body_01'.
		Enable body 'dynamic_body_02'.
		Enable body 'dynamic_body_03'.
		Enable body 'dynamic_body_04'.
		Enable body 'dynamic_body_05'.
		Enable body 'dynamic_body_06'.
		Enable body 'dynamic_body_07'.
		Enable body 'dynamic_body_08'.
		Enable body 'dynamic_body_09'.
		Enable body 'dynamic_body_10'.
	sequence 'spawn_shoe_01':
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_01_dynamic':
			position object_pos('e_pos_01')
			rotation object_rot('e_pos_01')
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_01_dynamic':
			position object_pos('e_pos_02')
			rotation object_rot('e_pos_02')
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_03_dynamic':
			position object_pos('e_pos_03')
			rotation object_rot('e_pos_03')
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_03_dynamic':
			position object_pos('e_pos_04')
			rotation object_rot('e_pos_04')
		slot:
			slot 11
	sequence 'spawn_shoe_02':
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_02_dynamic':
			position object_pos('e_pos_01')
			rotation object_rot('e_pos_01')
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_02_dynamic':
			position object_pos('e_pos_02')
			rotation object_rot('e_pos_02')
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_01_dynamic':
			position object_pos('e_pos_03')
			rotation object_rot('e_pos_03')
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_01_dynamic':
			position object_pos('e_pos_04')
			rotation object_rot('e_pos_04')
		slot:
			slot 11
	sequence 'spawn_shoe_03':
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_03_dynamic':
			position object_pos('e_pos_01')
			rotation object_rot('e_pos_01')
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_03_dynamic':
			position object_pos('e_pos_02')
			rotation object_rot('e_pos_02')
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_03_dynamic':
			position object_pos('e_pos_03')
			rotation object_rot('e_pos_03')
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_03_dynamic':
			position object_pos('e_pos_04')
			rotation object_rot('e_pos_04')
		slot:
			slot 11
	sequence 'spawn_shoe_04':
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_04_dynamic':
			position object_pos('e_pos_01')
			rotation object_rot('e_pos_01')
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_04_dynamic':
			position object_pos('e_pos_02')
			rotation object_rot('e_pos_02')
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_05_dynamic':
			position object_pos('e_pos_03')
			rotation object_rot('e_pos_03')
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_05_dynamic':
			position object_pos('e_pos_04')
			rotation object_rot('e_pos_04')
		slot:
			slot 11
	sequence 'spawn_shoe_05':
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_05_dynamic':
			position object_pos('e_pos_01')
			rotation object_rot('e_pos_01')
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_05_dynamic':
			position object_pos('e_pos_02')
			rotation object_rot('e_pos_02')
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_02_dynamic':
			position object_pos('e_pos_03')
			rotation object_rot('e_pos_03')
		spawn_unit 'units/payday2/props/com_prop_store_shoes/com_prop_store_shoes_02_dynamic':
			position object_pos('e_pos_04')
			rotation object_rot('e_pos_04')
		slot:
			slot 11
	body 'static_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'make_dynamic'.
			Run sequence 'spawn_shoe_'..pick('01','02','03','04','05').
