unit:
	sequence 'destroy_ball_a':
		Disable object 'g_ball_a'.
		Disable body 'body_ball_a'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_a')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_a'.
	sequence 'destroy_ball_b':
		Disable object 'g_ball_b'.
		Disable body 'body_ball_b'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_b')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_b'.
	sequence 'destroy_ball_c':
		Disable object 'g_ball_c'.
		Disable body 'body_ball_c'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_c')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_c'.
	sequence 'destroy_ball_d':
		Disable object 'g_ball_d'.
		Disable body 'body_ball_d'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_d')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_d'.
	sequence 'destroy_ball_e':
		Disable object 'g_ball_e'.
		Disable body 'body_ball_e'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_e')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_e'.
	sequence 'destroy_ball_f':
		Disable object 'g_ball_f'.
		Disable body 'body_ball_f'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_f')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_f'.
	sequence 'destroy_ball_g':
		Disable object 'g_ball_g'.
		Disable body 'body_ball_g'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_g')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_g'.
	sequence 'destroy_ball_h':
		Disable object 'g_ball_h'.
		Disable body 'body_ball_h'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_h')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_h'.
	sequence 'destroy_ball_i':
		Disable object 'g_ball_i'.
		Disable body 'body_ball_i'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_i')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_i'.
	sequence 'destroy_ball_j':
		Disable object 'g_ball_j'.
		Disable body 'body_ball_j'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_j')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_j'.
	sequence 'destroy_ball_k':
		Disable object 'g_ball_k'.
		Disable body 'body_ball_k'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_k')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_k'.
	sequence 'destroy_ball_l':
		Disable object 'g_ball_l'.
		Disable body 'body_ball_l'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_l')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_l'.
	sequence 'destroy_ball_m':
		Disable object 'g_ball_m'.
		Disable body 'body_ball_m'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_m')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_m'.
	sequence 'destroy_ball_n':
		Disable object 'g_ball_n'.
		Disable body 'body_ball_n'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_n')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_n'.
	sequence 'destroy_ball_o':
		Disable object 'g_ball_o'.
		Disable body 'body_ball_o'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_o')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_o'.
	sequence 'destroy_ball_p':
		Disable object 'g_ball_p'.
		Disable body 'body_ball_p'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_p')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_p'.
	sequence 'destroy_ball_q':
		Disable object 'g_ball_q'.
		Disable body 'body_ball_q'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_q')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_q'.
	sequence 'destroy_ball_r':
		Disable object 'g_ball_r'.
		Disable body 'body_ball_r'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_r')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_r'.
	sequence 'destroy_ball_s':
		Disable object 'g_ball_s'.
		Disable body 'body_ball_s'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_s')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_s'.
	sequence 'destroy_ball_t':
		Disable object 'g_ball_t'.
		Disable body 'body_ball_t'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_t')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_t'.
	sequence 'destroy_ball_u':
		Disable object 'g_ball_u'.
		Disable body 'body_ball_u'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_u')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_u'.
	sequence 'destroy_ball_v':
		Disable object 'g_ball_v'.
		Disable body 'body_ball_v'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_v')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_v'.
	sequence 'destroy_ball_w':
		Disable object 'g_ball_w'.
		Disable body 'body_ball_w'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_w')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_w'.
	sequence 'destroy_ball_x':
		Disable object 'g_ball_x'.
		Disable body 'body_ball_x'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_x')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_x'.
	sequence 'destroy_ball_y':
		Disable object 'g_ball_y'.
		Disable body 'body_ball_y'.
		effect 'effects/particles/dest/bottle_dest':
			parent object('e_ball_y')
			position v()
		Play audio 'vase_shatter_large' at 'e_ball_y'.
	sequence 'destroy_bell_a':
		Disable body 'body_bell_a'.
		Enable body 'body_bell_dynamic_a'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object('e_bell_a')
			position v()
		Play audio 'vase_shatter_large' at 'e_bell_a'.
	sequence 'destroy_bell_b':
		Disable body 'body_bell_b'.
		Enable body 'body_bell_dynamic_b'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object('e_bell_b')
			position v()
		Play audio 'vase_shatter_large' at 'e_bell_b'.
	sequence 'destroy_bell_c':
		Disable body 'body_bell_c'.
		Enable body 'body_bell_dynamic_c'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object('e_bell_c')
			position v()
		Play audio 'vase_shatter_large' at 'e_bell_c'.
	sequence 'destroy_bell_d':
		Disable body 'body_bell_d'.
		Enable body 'body_bell_dynamic_d'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object('e_bell_d')
			position v()
		Play audio 'vase_shatter_large' at 'e_bell_d'.
	sequence 'destroy_bell_e':
		Disable body 'body_bell_e'.
		Enable body 'body_bell_dynamic_e'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object('e_bell_e')
			position v()
		Play audio 'vase_shatter_large' at 'e_bell_e'.
	sequence 'destroy_bell_f':
		Disable body 'body_bell_f'.
		Enable body 'body_bell_dynamic_f'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object('e_bell_f')
			position v()
		Play audio 'vase_shatter_large' at 'e_bell_f'.
	sequence 'destroy_bell_g':
		Disable body 'body_bell_g'.
		Enable body 'body_bell_dynamic_g'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object('e_bell_g')
			position v()
		Play audio 'vase_shatter_large' at 'e_bell_g'.
	sequence 'destroy_bell_h':
		Disable body 'body_bell_h'.
		Enable body 'body_bell_dynamic_h'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object('e_bell_h')
			position v()
		Play audio 'vase_shatter_large' at 'e_bell_h'.
	sequence 'destroy_bell_i':
		Disable body 'body_bell_i'.
		Enable body 'body_bell_dynamic_i'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object('e_bell_i')
			position v()
		Play audio 'vase_shatter_large' at 'e_bell_i'.
	sequence 'destroy_bell_j':
		Disable body 'body_bell_j'.
		Enable body 'body_bell_dynamic_j'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object('e_bell_j')
			position v()
		Play audio 'vase_shatter_large' at 'e_bell_j'.
	sequence 'destroy_bell_k':
		Disable body 'body_bell_k'.
		Enable body 'body_bell_dynamic_k'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object('e_bell_k')
			position v()
		Play audio 'vase_shatter_large' at 'e_bell_k'.
	sequence 'destroy_bell_l':
		Disable body 'body_bell_l'.
		Enable body 'body_bell_dynamic_l'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object('e_bell_l')
			position v()
		Play audio 'vase_shatter_large' at 'e_bell_l'.
	sequence 'destroy_bell_m':
		Disable body 'body_bell_m'.
		Enable body 'body_bell_dynamic_m'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object('e_bell_m')
			position v()
		Play audio 'vase_shatter_large' at 'e_bell_m'.
	sequence 'destroy_bell_n':
		Disable body 'body_bell_n'.
		Enable body 'body_bell_dynamic_n'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object('e_bell_n')
			position v()
		Play audio 'vase_shatter_large' at 'e_bell_n'.
	sequence 'destroy_bell_o':
		Disable body 'body_bell_o'.
		Enable body 'body_bell_dynamic_o'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object('e_bell_o')
			position v()
		Play audio 'vase_shatter_large' at 'e_bell_o'.
	sequence 'destroy_bell_p':
		Disable body 'body_bell_p'.
		Enable body 'body_bell_dynamic_p'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object('e_bell_p')
			position v()
		Play audio 'vase_shatter_large' at 'e_bell_p'.
	body 'body_ball_a'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_a'.
	body 'body_ball_b'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_b'.
	body 'body_ball_c'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_c'.
	body 'body_ball_d'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_d'.
	body 'body_ball_e'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_e'.
	body 'body_ball_f'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_f'.
	body 'body_ball_g'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_g'.
	body 'body_ball_h'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_h'.
	body 'body_ball_i'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_i'.
	body 'body_ball_j'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_j'.
	body 'body_ball_k'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_k'.
	body 'body_ball_l'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_l'.
	body 'body_ball_m'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_m'.
	body 'body_ball_n'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_n'.
	body 'body_ball_o'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_o'.
	body 'body_ball_p'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_p'.
	body 'body_ball_q'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_q'.
	body 'body_ball_r'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_r'.
	body 'body_ball_s'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_s'.
	body 'body_ball_t'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_t'.
	body 'body_ball_u'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_u'.
	body 'body_ball_v'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_v'.
	body 'body_ball_w'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_w'.
	body 'body_ball_x'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_x'.
	body 'body_ball_y'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_ball_y'.
	body 'body_bell_a'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_bell_a'.
	body 'body_bell_b'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_bell_b'.
	body 'body_bell_c'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_bell_c'.
	body 'body_bell_d'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_bell_d'.
	body 'body_bell_e'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_bell_e'.
	body 'body_bell_f'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_bell_f'.
	body 'body_bell_g'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_bell_g'.
	body 'body_bell_h'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_bell_h'.
	body 'body_bell_i'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_bell_i'.
	body 'body_bell_j'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_bell_j'.
	body 'body_bell_k'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_bell_k'.
	body 'body_bell_l'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_bell_l'.
	body 'body_bell_m'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_bell_m'.
	body 'body_bell_n'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_bell_n'.
	body 'body_bell_o'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_bell_o'.
	body 'body_bell_p'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_bell_p'.
