unit:
	on = 1
	sequence 'on':
		Enable object 'g_lights_on'.
		Disable object 'g_lights_off'.
		on = 1
	sequence 'off':
		Disable object 'g_lights_on'.
		Enable object 'g_lights_off'.
		on = 0
	body 'body_glass01'
		Upon receiving 1 bullet hit or 20 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_glass01_shatter'.
		Upon receiving 2 bullet hits or 50 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_glass01_crack'.
	sequence 'int_seq_glass01_shatter':
		Disable object 'g_glass_01'.
		Enable object 'g_glass_01shatter'.
		Disable object 'g_glass_01crack'.
	sequence 'int_seq_glass01_crack':
		Disable body 'body_glass01'.
		Disable object 'g_glass_01shatter'.
		Enable object 'g_glass_01crack'.
		Play audio 'window_large_shatter' at 'e_e_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_e_01')
			position v()
		Cause alert with 12 m radius.
	body 'body_glass02'
		Upon receiving 1 bullet hit or 20 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_glass02_shatter'.
		Upon receiving 2 bullet hits or 50 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_glass02_crack'.
	sequence 'int_seq_glass02_shatter':
		Disable object 'g_glass_02'.
		Enable object 'g_glass_02shatter'.
		Disable object 'g_glass_02crack'.
	sequence 'int_seq_glass02_crack':
		Disable body 'body_glass02'.
		Disable object 'g_glass_02shatter'.
		Enable object 'g_glass_02crack'.
		Play audio 'window_large_shatter' at 'e_e_02'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_e_02')
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_e_05')
			position v()
		Cause alert with 12 m radius.
	body 'body_glass03'
		Upon receiving 1 bullet hit or 20 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_glass03_shatter'.
		Upon receiving 2 bullet hits or 50 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_glass03_crack'.
	sequence 'int_seq_glass03_shatter':
		Disable object 'g_glass_03'.
		Enable object 'g_glass_03shatter'.
		Disable object 'g_glass_03crack'.
	sequence 'int_seq_glass03_crack':
		Disable body 'body_glass03'.
		Disable object 'g_glass_03shatter'.
		Enable object 'g_glass_03crack'.
		Play audio 'window_large_shatter' at 'e_e_03'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_e_03')
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_e_04')
			position v()
		Cause alert with 12 m radius.
