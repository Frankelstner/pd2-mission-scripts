unit:
	sequence 'anim_open':
		Enable animation_group 'anim_grp_door':
			from 0/30
			speed 1/5
			to 30/30
	sequence 'anim_close':
		Enable animation_group 'anim_grp_door':
			from 30/30
			speed -1/5
			to 0/30
	sequence '__reset_animation__':
		Call function: base.move_to_stored_pos()
	sequence '__allow_sync_reset_animation__':
		Call function: base.allow_sync_stored_pos('true')
	sequence 'anim_pta_arrive_1':
		Call function: base.store_current_pos()
		animation_redirect 'pta_arrive_1'.
		Play audio 'truck_peta_arrive_anim_01' at 'snd'.
		Run sequence 'done_car_anim'. (DELAY 1283/30)
	sequence 'anim_pta_arrive_2':
		Call function: base.store_current_pos()
		animation_redirect 'pta_arrive_2'.
		Play audio 'truck_peta_arrive_anim_02' at 'snd'.
		Run sequence 'done_car_anim'. (DELAY 1330/30)
	sequence 'anim_pta_arrive_3':
		Call function: base.store_current_pos()
		animation_redirect 'pta_arrive_3'.
		Play audio 'truck_peta_arrive_anim_03' at 'snd'.
		Run sequence 'done_car_anim'. (DELAY 895/30)
	sequence 'done_car_anim'.
