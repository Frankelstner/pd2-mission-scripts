unit:
	sequence 'spawn_dummy':
		Enable interaction.
		Enable object 'g_drill_outline'.
		Enable object 'g_drill_base_outline'.
		Disable object 'g_drill_'.
		Disable object 'g_drill_base'.
		Enable body 'static_body'.
		Call function: timer_gui.set_visible(False)
	sequence 'hide_dummy':
		Disable interaction.
		Disable body 'static_body'.
		Disable object 'g_drill'.
		Disable object 'g_drill_outline'.
		Disable object 'g_drill_base'.
		Disable object 'g_drill_base_outline'.
		Call function: timer_gui.set_visible(False)
	sequence 'done':
		Call function: timer_gui.done()
	sequence 'timer_done'.
	sequence 'first_interact':
		TRIGGER TIMES 1
		Disable object 'g_drill_outline'.
		Disable object 'g_drill_base_outline'.
		Enable object 'g_drill'.
		Enable object 'g_drill_base'.
		Enable animation_group 'drilling':
			from 0
			speed 1
			to 100
	sequence 'interact':
		Run sequence 'first_interact'.
		Call function: timer_gui.set_visible(True)
		Call function: timer_gui.start('60')
