unit:
	sequence 'hide':
		Disable interaction.
		Disable object 'g_planks'.
		Disable object 'g_sunray'.
		Disable object 's_planks'.
		Disable object 'g_planks_dmg'.
		Disable object 's_planks_dmg'.
		Hide graphic_group 'plankicongroup'.
	sequence 'show_planks':
		Disable interaction.
		Enable object 'g_planks'.
		Enable object 'g_sunray'.
		Enable object 's_planks'.
		Disable object 'g_planks_dmg'.
		Disable object 's_planks_dmg'.
		Enable body 'planks_body'.
		Enable body 'ai_vision_block'.
		Enable decal_mesh 'g_planks'.
		Hide graphic_group 'plankicongroup'.
		Play audio 'barricade_window_end' at 'a_interact'.
	sequence 'enable_interaction':
		Enable interaction.
		Disable object 'g_planks'.
		Disable object 'g_sunray'.
		Disable object 's_planks'.
		Disable object 'g_planks_dmg'.
		Disable object 's_planks_dmg'.
		Show graphic_group 'plankicongroup'.
	sequence 'interact':
		Run sequence 'show_planks'.
	sequence 'destroy_planks':
		Disable object 'g_planks'.
		Disable object 's_planks'.
		Disable object 'g_sunray'.
		Disable decal_mesh 'g_planks'.
		Enable object 'g_planks_dmg'.
		Enable object 's_planks_dmg'.
		Disable body 'planks_body'.
		Disable body 'ai_vision_block'.
		Enable body 'planks_body_damaged'.
		Enable body 'ai_vision_block_damaged'.
		Hide graphic_group 'plankicongroup'.
		effect 'effects/particles/dest/stair_dest':
			parent object('e_effect')
			position v()
		spawn_unit 'units/pd2_dlc_peta/props/pta_prop_barn_planks_barricade/pta_prop_barn_planks_debris':
			position object_pos('e_effect')
			rotation object_rot('e_effect')
	body 'planks_body'
		Upon receiving 15 bullet hits or 10 explosion damage, execute:
			Run sequence 'destroy_planks'.
