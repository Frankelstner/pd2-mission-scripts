unit:
	sequence 'state_door_open':
		Enable object 'g_opened_door'.
		Disable object 'g_closed_door'.
		Enable body 'collision_open'.
		Disable body 'collision_closed'.
		Disable object 'g_opened_door_mirror'.
		Disable object 'g_closed_door_mirror'.
		Disable body 'collision_open_mirror'.
		Disable body 'collision_closed_mirror'.
		Disable decal_mesh 'dm_wood_closed'.
		Enable decal_mesh 'dm_wood_opened'.
		Disable decal_mesh 'dm_wood_opened_mirror'.
		Disable decal_mesh 'dm_wood_closed_mirror'.
	sequence 'state_door_open_mirror':
		Disable object 'g_opened_door'.
		Disable object 'g_closed_door'.
		Disable body 'collision_open'.
		Disable body 'collision_closed'.
		Enable object 'g_opened_door_mirror'.
		Disable object 'g_closed_door_mirror'.
		Enable body 'collision_open_mirror'.
		Disable body 'collision_closed_mirror'.
		Disable decal_mesh 'dm_wood_closed'.
		Disable decal_mesh 'dm_wood_opened'.
		Enable decal_mesh 'dm_wood_opened_mirror'.
		Disable decal_mesh 'dm_wood_closed_mirror'.
	sequence 'state_door_closed':
		Disable object 'g_opened_door'.
		Enable object 'g_closed_door'.
		Disable body 'collision_open'.
		Enable body 'collision_closed'.
		Disable object 'g_opened_door_mirror'.
		Disable object 'g_closed_door_mirror'.
		Disable body 'collision_open_mirror'.
		Disable body 'collision_closed_mirror'.
		Enable decal_mesh 'dm_wood_closed'.
		Disable decal_mesh 'dm_wood_opened'.
		Disable decal_mesh 'dm_wood_opened_mirror'.
		Disable decal_mesh 'dm_wood_closed_mirror'.
	sequence 'state_door_closed_mirror':
		Disable object 'g_opened_door'.
		Disable object 'g_closed_door'.
		Disable body 'collision_open'.
		Disable body 'collision_closed'.
		Disable object 'g_opened_door_mirror'.
		Enable object 'g_closed_door_mirror'.
		Disable body 'collision_open_mirror'.
		Enable body 'collision_closed_mirror'.
		Disable decal_mesh 'dm_wood_closed'.
		Disable decal_mesh 'dm_wood_opened'.
		Disable decal_mesh 'dm_wood_opened_mirror'.
		Enable decal_mesh 'dm_wood_closed_mirror'.
