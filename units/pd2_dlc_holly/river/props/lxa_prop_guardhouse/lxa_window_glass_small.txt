unit:
	sequence 'damage':
		TRIGGER TIMES 1
		Disable object 'g_glass'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_01' (alarm reason: 'glass').
	sequence 'destroy':
		TRIGGER TIMES 1
		Disable object 'g_glass'.
		Play audio 'window_large_shatter' at 'e_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_01' )
			position v()
		Cause alert with 12 m radius.
		Disable body 'glass_body'.
	body 'glass_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy'.
