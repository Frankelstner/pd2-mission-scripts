unit:
	sequence 'show':
		Enable decal_mesh 'dm_concrete'.
		Show graphic_group 'gg_concrete_fence_6m'.
		Enable body 'body_static'.
	sequence 'hide':
		Disable decal_mesh 'dm_concrete'.
		Hide graphic_group 'gg_concrete_fence_6m'.
		Disable body 'body_static'.
