unit:
	sequence 'turret_spawn':
		Call function: base.spawn_module('units/pd2_dlc_dah/vehicles/dah_turret_ceiling/dah_turret_ceiling','spawn','turret1')
	sequence 'turret_activate':
		Enable animation_group 'anim':
			from 0
		Call function: base.run_module_function('turret1','base','activate_as_module','combatant','ceiling_turret_module_no_idle') (DELAY 8/30)
	sequence 'turret_ceiling_activate':
		Enable animation_group 'anim':
			from 0
		Call function: base.run_module_function('turret1','base','activate_as_module','combatant','ceiling_turret_module') (DELAY 8/30)
	sequence 'turret_disable':
		Call function: base.run_module_function('turret1','character_damage','disable')
	sequence 'done_turret_destroyed'.
	sequence 'hide':
		Disable body 'body_static'.
		Disable decal_mesh 'g_frame'.
		Disable decal_mesh 'g_hatch_right_1'.
		Disable decal_mesh 'g_hatch_left_1'.
		Disable decal_mesh 'g_hatch_right_2'.
		Disable decal_mesh 'g_hatch_left_2'.
		Disable object 'g_hatch_right_1'.
		Disable object 'g_hatch_left_1'.
		Disable object 'g_hatch_right_2'.
		Disable object 'g_hatch_left_2'.
		Disable object 'g_frame'.
		Enable object 'body_editor'.
	sequence 'show':
		Enable body 'body_static'.
		Enable decal_mesh 'g_frame'.
		Enable decal_mesh 'g_hatch_right_1'.
		Enable decal_mesh 'g_hatch_left_1'.
		Enable decal_mesh 'g_hatch_right_2'.
		Enable decal_mesh 'g_hatch_left_2'.
		Enable object 'g_hatch_right_1'.
		Enable object 'g_hatch_left_1'.
		Enable object 'g_hatch_right_2'.
		Enable object 'g_hatch_left_2'.
		Enable object 'g_frame'.
		Disable object 'body_editor'.
