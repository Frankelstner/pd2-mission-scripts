unit:
	sequence 'off':
		Disable animation_group 'rotate':
			loop True
	sequence 'really slow':
		Enable animation_group 'rotate':
			loop True
			speed 0.45
	sequence 'slow':
		Enable animation_group 'rotate':
			loop True
	sequence 'fast':
		Enable animation_group 'rotate':
			loop True
			speed 4
