unit:
	sequence 'lamp_on':
		Show graphic_group 'light_rays'.
		Enable object 'g_glow'.
		Enable object 'g_g6'.
		Enable object 'g_g'.
		Enable object 'g_lamp_on'.
		Disable object 'g_lamp_off'.
	sequence 'lamp_off':
		Show graphic_group 'light_rays'.
		Disable object 'g_glow'.
		Disable object 'g_g6'.
		Disable object 'g_g'.
		Disable object 'g_lamp_on'.
		Enable object 'g_lamp_off'.
