unit:
	sequence 'break_glass':
		Disable object 'g_glass'.
		Enable object 'g_glass_shattered'.
		Play audio 'glass_crack' at 'e_glass'.
	sequence 'make_dynamic':
		slot:
			slot 18
		Disable body 'static_body'.
		Enable body 'dynamic_body'.
		Call function World:play_physic_effect('core/physic_effects/sequencemanager_push',dest_unit:body('dynamic_body'),dest_unit:body('dynamic_body'):rotation():z()* 300,5).
	body 'static_body'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'break_glass'.
		Upon receiving 2 bullet hits or 5 melee damage, execute:
			Run sequence 'make_dynamic'.
		Upon receiving 10 explosion damage, execute:
			Run sequence 'break_glass'.
			Run sequence 'make_dynamic'.
