unit:
	sequence 'enable_interaction':
		Enable interaction.
		Show graphic_group 'grp_icon'.
	sequence 'disable_interaction':
		Disable interaction.
		Hide graphic_group 'grp_icon'.
	sequence 'interact':
		Run sequence 'int_seq_cut_glass'.
		Hide graphic_group 'grp_icon'.
	sequence 'int_seq_cut_glass':
		Disable object 'g_glass'.
		Enable object 'g_hole'.
		Disable body 'body_glass'.
		Enable body 'body_glass_hole'.
	body 'body_glass'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_crack_glass'.
		Upon receiving 3 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_glass'.
	sequence 'int_seq_crack_glass':
		Disable object 'g_glass'.
		Enable object 'g_glass_shattered'.
		Run sequence 'glass_broken'.
		Disable interaction.
		Hide graphic_group 'grp_icon'.
	sequence 'int_seq_break_glass':
		Disable body 'body_glass'.
		Disable object 'g_glass'.
		Disable object 'g_glass_shattered'.
		Play audio 'window_small_shatter' at 'e_effect_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect_01')
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect_02')
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect_03')
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect_04')
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect_05')
			position v()
		Disable interaction.
		Hide graphic_group 'grp_icon'.
		Run sequence 'glass_broken'.
	body 'body_glass_hole'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_crack_glass_hole'.
		Upon receiving 3 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_glass_hole'.
	sequence 'int_seq_crack_glass_hole':
		Disable object 'g_hole'.
		Enable object 'g_hole_shattered'.
		Run sequence 'glass_broken'.
		Disable interaction.
		Hide graphic_group 'grp_icon'.
	sequence 'int_seq_break_glass_hole':
		Disable body 'body_glass_hole'.
		Disable object 'g_hole'.
		Disable object 'g_hole_shattered'.
		Play audio 'window_small_shatter' at 'e_effect_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect_01')
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect_02')
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect_03')
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect_04')
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect_05')
			position v()
		Disable interaction.
		Hide graphic_group 'grp_icon'.
		Run sequence 'glass_broken'.
	sequence 'glass_broken'.
	sequence 'glass_cracked'.
