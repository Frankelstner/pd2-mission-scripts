unit:
	body 'body_static_01'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_01'.
	body 'body_static_02'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_02'.
	body 'body_static_03'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_03'.
	body 'body_static_04'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_04'.
	body 'body_static_05'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_05'.
	body 'body_static_06'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_06'.
	body 'body_static_07'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_07'.
	body 'body_static_08'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_08'.
	body 'body_static_09'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_09'.
	body 'body_static_10'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_10'.
	body 'body_static_11'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_11'.
	body 'body_static_12'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_12'.
	body 'body_static_13'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_13'.
	body 'body_static_14'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_14'.
	body 'body_static_15'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_15'.
	body 'body_static_16'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_16'.
	body 'body_static_17'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_17'.
	body 'body_static_18'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_18'.
	body 'body_static_19'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_19'.
	body 'body_static_20'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_20'.
	body 'body_static_21'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_destroy_glass_21'.
	sequence 'int_seq_destroy_glass_01':
		Disable object 'g_glass_01'.
		Enable object 'g_glass_broken_01'.
		Disable body 'body_static_01'.
		Enable body 'body_static_broken_01'.
		Disable decal_mesh 'dm_glass_01'.
		Enable decal_mesh 'dm_glass_broken_01'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_01' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_01'.
	sequence 'int_seq_destroy_glass_02':
		Disable object 'g_glass_02'.
		Enable object 'g_glass_broken_02'.
		Disable body 'body_static_02'.
		Enable body 'body_static_broken_02'.
		Disable decal_mesh 'dm_glass_02'.
		Enable decal_mesh 'dm_glass_broken_02'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_02' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_02'.
	sequence 'int_seq_destroy_glass_03':
		Disable object 'g_glass_03'.
		Enable object 'g_glass_broken_03'.
		Disable body 'body_static_03'.
		Enable body 'body_static_broken_03'.
		Disable decal_mesh 'dm_glass_03'.
		Enable decal_mesh 'dm_glass_broken_03'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_03' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_03'.
	sequence 'int_seq_destroy_glass_04':
		Disable object 'g_glass_04'.
		Enable object 'g_glass_broken_04'.
		Disable body 'body_static_04'.
		Enable body 'body_static_broken_04'.
		Disable decal_mesh 'dm_glass_04'.
		Enable decal_mesh 'dm_glass_broken_04'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_04' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_04'.
	sequence 'int_seq_destroy_glass_05':
		Disable object 'g_glass_05'.
		Enable object 'g_glass_broken_05'.
		Disable body 'body_static_05'.
		Enable body 'body_static_broken_05'.
		Disable decal_mesh 'dm_glass_05'.
		Enable decal_mesh 'dm_glass_broken_05'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_05' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_05'.
	sequence 'int_seq_destroy_glass_06':
		Disable object 'g_glass_06'.
		Enable object 'g_glass_broken_06'.
		Disable body 'body_static_06'.
		Enable body 'body_static_broken_06'.
		Disable decal_mesh 'dm_glass_06'.
		Enable decal_mesh 'dm_glass_broken_06'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_06' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_06'.
	sequence 'int_seq_destroy_glass_07':
		Disable object 'g_glass_07'.
		Enable object 'g_glass_broken_07'.
		Disable body 'body_static_07'.
		Enable body 'body_static_broken_07'.
		Disable decal_mesh 'dm_glass_07'.
		Enable decal_mesh 'dm_glass_broken_07'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_07' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_07'.
	sequence 'int_seq_destroy_glass_08':
		Disable object 'g_glass_08'.
		Enable object 'g_glass_broken_08'.
		Disable body 'body_static_08'.
		Enable body 'body_static_broken_08'.
		Disable decal_mesh 'dm_glass_08'.
		Enable decal_mesh 'dm_glass_broken_08'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_08' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_08'.
	sequence 'int_seq_destroy_glass_09':
		Disable object 'g_glass_09'.
		Enable object 'g_glass_broken_09'.
		Disable body 'body_static_09'.
		Enable body 'body_static_broken_09'.
		Disable decal_mesh 'dm_glass_09'.
		Enable decal_mesh 'dm_glass_broken_09'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_09' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_09'.
	sequence 'int_seq_destroy_glass_10':
		Disable object 'g_glass_10'.
		Enable object 'g_glass_broken_10'.
		Disable body 'body_static_10'.
		Enable body 'body_static_broken_10'.
		Disable decal_mesh 'dm_glass_10'.
		Enable decal_mesh 'dm_glass_broken_10'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_10' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_10'.
	sequence 'int_seq_destroy_glass_11':
		Disable object 'g_glass_11'.
		Enable object 'g_glass_broken_11'.
		Disable body 'body_static_11'.
		Enable body 'body_static_broken_11'.
		Disable decal_mesh 'dm_glass_11'.
		Enable decal_mesh 'dm_glass_broken_11'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_11' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_11'.
	sequence 'int_seq_destroy_glass_12':
		Disable object 'g_glass_12'.
		Enable object 'g_glass_broken_12'.
		Disable body 'body_static_12'.
		Enable body 'body_static_broken_12'.
		Disable decal_mesh 'dm_glass_12'.
		Enable decal_mesh 'dm_glass_broken_12'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_12' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_12'.
	sequence 'int_seq_destroy_glass_13':
		Disable object 'g_glass_13'.
		Enable object 'g_glass_broken_13'.
		Disable body 'body_static_13'.
		Enable body 'body_static_broken_13'.
		Disable decal_mesh 'dm_glass_13'.
		Enable decal_mesh 'dm_glass_broken_13'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_13' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_13'.
	sequence 'int_seq_destroy_glass_14':
		Disable object 'g_glass_14'.
		Enable object 'g_glass_broken_14'.
		Disable body 'body_static_14'.
		Enable body 'body_static_broken_14'.
		Disable decal_mesh 'dm_glass_14'.
		Enable decal_mesh 'dm_glass_broken_14'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_14' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_14'.
	sequence 'int_seq_destroy_glass_15':
		Disable object 'g_glass_15'.
		Enable object 'g_glass_broken_15'.
		Disable body 'body_static_15'.
		Enable body 'body_static_broken_15'.
		Disable decal_mesh 'dm_glass_15'.
		Enable decal_mesh 'dm_glass_broken_15'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_15' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_15'.
	sequence 'int_seq_destroy_glass_16':
		Disable object 'g_glass_16'.
		Enable object 'g_glass_broken_16'.
		Disable body 'body_static_16'.
		Enable body 'body_static_broken_16'.
		Disable decal_mesh 'dm_glass_16'.
		Enable decal_mesh 'dm_glass_broken_16'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_16' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_16'.
	sequence 'int_seq_destroy_glass_17':
		Disable object 'g_glass_17'.
		Enable object 'g_glass_broken_17'.
		Disable body 'body_static_17'.
		Enable body 'body_static_broken_17'.
		Disable decal_mesh 'dm_glass_17'.
		Enable decal_mesh 'dm_glass_broken_17'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_17' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_17'.
	sequence 'int_seq_destroy_glass_18':
		Disable object 'g_glass_18'.
		Enable object 'g_glass_broken_18'.
		Disable body 'body_static_18'.
		Enable body 'body_static_broken_18'.
		Disable decal_mesh 'dm_glass_18'.
		Enable decal_mesh 'dm_glass_broken_18'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_18' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_18'.
	sequence 'int_seq_destroy_glass_19':
		Disable object 'g_glass_19'.
		Enable object 'g_glass_broken_19'.
		Disable body 'body_static_19'.
		Enable body 'body_static_broken_19'.
		Disable decal_mesh 'dm_glass_19'.
		Enable decal_mesh 'dm_glass_broken_19'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_19' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_19'.
	sequence 'int_seq_destroy_glass_20':
		Disable object 'g_glass_20'.
		Enable object 'g_glass_broken_20'.
		Disable body 'body_static_20'.
		Enable body 'body_static_broken_20'.
		Disable decal_mesh 'dm_glass_20'.
		Enable decal_mesh 'dm_glass_broken_20'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_20' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_20'.
	sequence 'int_seq_destroy_glass_21':
		Disable object 'g_glass_21'.
		Enable object 'g_glass_broken_21'.
		Disable body 'body_static_21'.
		Enable body 'body_static_broken_21'.
		Disable decal_mesh 'dm_glass_21'.
		Enable decal_mesh 'dm_glass_broken_21'.
		effect 'effects/payday2/particles/window/dah_glass_break_dark': (DELAY 1/30)
			parent object( 'e_effect_21' )
			position v()
		Play audio 'window_small_shatter' at 'e_effect_21'.
