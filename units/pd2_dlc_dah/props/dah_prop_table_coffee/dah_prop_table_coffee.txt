unit:
	sequence 'var_01':
		Enable body 'body_static':
			visibility True
		Enable body 'body_static01':
			visibility True
		Show graphic_group 'grp_01'.
		Show graphic_group 'grp_02'.
	sequence 'var_02':
		Enable body 'body_static':
			visibility True
		Enable body 'body_static01':
			visibility True
		Show graphic_group 'grp_01'.
		Hide graphic_group 'grp_02'.
	sequence 'destroy_champange':
		TRIGGER TIMES 1
		effect 'effects/particles/dest/bottle_dest':
			parent 'g_glass_g'
		Play audio 'pot_large_shatter' at 'g_glass_g'.
		Disable object 'g_glass_g'.
		Disable decal_mesh 'dm_glass_breakable'.
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_champange'.
