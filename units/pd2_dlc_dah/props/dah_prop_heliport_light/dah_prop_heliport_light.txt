unit:
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break'.
	sequence 'int_seq_break':
		TRIGGER TIMES 1
		Disable body 'body_static'.
		Disable light 'lo_omni'.
		Enable body 'body_collision_01'.
		Enable body 'body_collision_02'.
		Enable body 'body_collision_03'.
		Enable body 'body_collision_04'.
		Enable body 'body_collision_05'.
		Enable body 'body_collision_06'.
		Disable decal_mesh 'dm_metal'.
		Disable object 's_s'.
		Hide graphic_group 'main_grp'.
		Show graphic_group 'grp_dmg_01'.
		Show graphic_group 'grp_dmg_02'.
		Show graphic_group 'grp_dmg_03'.
		Show graphic_group 'grp_dmg_04'.
		Show graphic_group 'grp_dmg_05'.
		Show graphic_group 'grp_dmg_06'.
