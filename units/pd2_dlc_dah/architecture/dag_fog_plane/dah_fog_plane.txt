unit:
	sequence 'small':
		Enable object 'g_g'.
		Disable object 'g_big'.
		Disable object 'g_round'.
		Disable object 'g_smallest'.
	sequence 'big':
		Disable object 'g_g'.
		Enable object 'g_big'.
		Disable object 'g_round'.
		Disable object 'g_smallest'.
	sequence 'round':
		Disable object 'g_g'.
		Disable object 'g_big'.
		Enable object 'g_round'.
		Disable object 'g_smallest'.
	sequence 'smallest':
		Disable object 'g_g'.
		Disable object 'g_big'.
		Disable object 'g_round'.
		Enable object 'g_smallest'.
