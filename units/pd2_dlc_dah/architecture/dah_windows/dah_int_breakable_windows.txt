unit:
	sequence 'destroy_01':
		TRIGGER TIMES 1
		Disable object 'g_window_01'.
		Enable object 'g_window_01_dmg_01'.
		Enable object 'g_window_01_dmg_02'.
		Enable body 'body_window_01_dmg_01'.
		Enable body 'body_window_01_dmg_02'.
		Disable body 'body_window_01'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_01_01' (alarm reason: 'glass').
	sequence 'destroy_dmg_01_01':
		TRIGGER TIMES 1
		Disable object 'g_window_01'.
		Disable object 'g_window_01_dmg_01'.
		Play audio 'window_medium_shatter' at 'e_window_01_01'.
		Disable decal_mesh 'g_window_01'.
		Disable decal_mesh 'g_window_01_dmg_01'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_01_01' )
			position v()
		Disable body 'body_window_01_dmg_01'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_01_01' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	sequence 'destroy_dmg_01_02':
		TRIGGER TIMES 1
		Disable object 'g_window_01'.
		Disable object 'g_window_01_dmg_02'.
		Play audio 'window_medium_shatter' at 'e_window_01_02'.
		Disable decal_mesh 'g_window_01'.
		Disable decal_mesh 'g_window_01_dmg_02'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_01_02' )
			position v()
		Disable body 'body_window_01_dmg_02'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_01_02' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_01'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_01'.
	body 'body_window_01_dmg_01'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_01_01'.
	body 'body_window_01_dmg_02'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_01_02'.
	sequence 'destroy_02':
		TRIGGER TIMES 1
		Disable object 'g_window_02'.
		Enable object 'g_window_02_dmg'.
		Enable body 'body_window_02_dmg'.
		Disable body 'body_window_02'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_02' (alarm reason: 'glass').
	sequence 'destroy_dmg_02':
		TRIGGER TIMES 1
		Disable object 'g_window_02'.
		Disable object 'g_window_02_dmg'.
		Play audio 'window_medium_shatter' at 'e_window_02'.
		Disable decal_mesh 'g_window_02'.
		Disable decal_mesh 'g_window_02_dmg'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_02' )
			position v()
		Disable body 'body_window_02_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_02' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_02'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_02'.
	body 'body_window_02_dmg'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_02'.
	sequence 'destroy_03':
		TRIGGER TIMES 1
		Disable object 'g_window_03'.
		Enable object 'g_window_03_dmg_01'.
		Enable object 'g_window_03_dmg_02'.
		Enable object 'g_window_03_dmg_03'.
		Enable body 'body_window_03_dmg_01'.
		Enable body 'body_window_03_dmg_02'.
		Enable body 'body_window_03_dmg_03'.
		Disable body 'body_window_03'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_03_01' (alarm reason: 'glass').
	sequence 'destroy_dmg_03_01':
		TRIGGER TIMES 1
		Disable object 'g_window_03'.
		Disable object 'g_window_03_dmg_01'.
		Play audio 'window_medium_shatter' at 'e_window_03_01'.
		Disable decal_mesh 'g_window_03'.
		Disable decal_mesh 'g_window_03_dmg_01'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_03_01' )
			position v()
		Disable body 'body_window_03_dmg_01'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_03_01' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	sequence 'destroy_dmg_03_02':
		TRIGGER TIMES 1
		Disable object 'g_window_03'.
		Disable object 'g_window_03_dmg_02'.
		Play audio 'window_medium_shatter' at 'e_window_03_02'.
		Disable decal_mesh 'g_window_03'.
		Disable decal_mesh 'g_window_03_dmg_02'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_03_02' )
			position v()
		Disable body 'body_window_03_dmg_02'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_03_02' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	sequence 'destroy_dmg_03_03':
		TRIGGER TIMES 1
		Disable object 'g_window_03'.
		Disable object 'g_window_03_dmg_03'.
		Play audio 'window_medium_shatter' at 'e_window_03_03'.
		Disable decal_mesh 'g_window_03'.
		Disable decal_mesh 'g_window_03_dmg_03'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_03_03' )
			position v()
		Disable body 'body_window_03_dmg_03'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_03_03' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_03'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_03'.
	body 'body_window_03_dmg_01'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_03_01'.
	body 'body_window_03_dmg_02'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_03_02'.
	body 'body_window_03_dmg_03'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_03_03'.
	sequence 'destroy_04':
		TRIGGER TIMES 1
		Disable object 'g_window_04'.
		Enable object 'g_window_04_dmg_01'.
		Enable object 'g_window_04_dmg_02'.
		Enable body 'body_window_04_dmg_01'.
		Enable body 'body_window_04_dmg_02'.
		Disable body 'body_window_04'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_04_01' (alarm reason: 'glass').
	sequence 'destroy_dmg_04_01':
		TRIGGER TIMES 1
		Disable object 'g_window_04'.
		Disable object 'g_window_04_dmg_01'.
		Play audio 'window_medium_shatter' at 'e_window_04_01'.
		Disable decal_mesh 'g_window_04'.
		Disable decal_mesh 'g_window_04_dmg_01'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_04_01' )
			position v()
		Disable body 'body_window_04_dmg_01'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_04_01' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	sequence 'destroy_dmg_04_02':
		TRIGGER TIMES 1
		Disable object 'g_window_04'.
		Disable object 'g_window_04_dmg_02'.
		Play audio 'window_medium_shatter' at 'e_window_04_02'.
		Disable decal_mesh 'g_window_04'.
		Disable decal_mesh 'g_window_04_dmg_02'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_04_02' )
			position v()
		Disable body 'body_window_04_dmg_02'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_04_02' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_04'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_04'.
	body 'body_window_04_dmg_01'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_04_01'.
	body 'body_window_04_dmg_02'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_04_02'.
	sequence 'destroy_05':
		TRIGGER TIMES 1
		Disable object 'g_window_05'.
		Enable object 'g_window_05_dmg'.
		Enable body 'body_window_05_dmg'.
		Disable body 'body_window_05'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_05' (alarm reason: 'glass').
	sequence 'destroy_dmg_05':
		TRIGGER TIMES 1
		Disable object 'g_window_05'.
		Disable object 'g_window_05_dmg'.
		Play audio 'window_medium_shatter' at 'e_window_05'.
		Disable decal_mesh 'g_window_05'.
		Disable decal_mesh 'g_window_05_dmg'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_05' )
			position v()
		Disable body 'body_window_05_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_05' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_05'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_05'.
	body 'body_window_05_dmg'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_05'.
	sequence 'destroy_06':
		TRIGGER TIMES 1
		Disable object 'g_window_06'.
		Enable object 'g_window_06_dmg'.
		Enable body 'body_window_06_dmg'.
		Disable body 'body_window_06'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_06' (alarm reason: 'glass').
	sequence 'destroy_dmg_06':
		TRIGGER TIMES 1
		Disable object 'g_window_06'.
		Disable object 'g_window_06_dmg'.
		Play audio 'window_medium_shatter' at 'e_window_06'.
		Disable decal_mesh 'g_window_06'.
		Disable decal_mesh 'g_window_06_dmg'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_06' )
			position v()
		Disable body 'body_window_06_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_06' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_06'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_06'.
	body 'body_window_06_dmg'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_06'.
	sequence 'destroy_07':
		TRIGGER TIMES 1
		Disable object 'g_window_07'.
		Enable object 'g_window_07_dmg_01'.
		Enable object 'g_window_07_dmg_02'.
		Enable body 'body_window_07_dmg_01'.
		Enable body 'body_window_07_dmg_02'.
		Disable body 'body_window_07'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_07_02' (alarm reason: 'glass').
	sequence 'destroy_dmg_07_01':
		TRIGGER TIMES 1
		Disable object 'g_window_07'.
		Disable object 'g_window_07_dmg_01'.
		Play audio 'window_medium_shatter' at 'e_window_07_01'.
		Disable decal_mesh 'g_window_07'.
		Disable decal_mesh 'g_window_07_dmg_01'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_07_01' )
			position v()
		Disable body 'body_window_07_dmg_01'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_07_02' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	sequence 'destroy_dmg_07_02':
		TRIGGER TIMES 1
		Disable object 'g_window_07'.
		Disable object 'g_window_07_dmg_02'.
		Play audio 'window_medium_shatter' at 'e_window_07_02'.
		Disable decal_mesh 'g_window_07'.
		Disable decal_mesh 'g_window_07_dmg_02'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_07_02' )
			position v()
		Disable body 'body_window_07_dmg_02'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_07_02' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_07'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_07'.
	body 'body_window_07_dmg_01'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_07_01'.
	body 'body_window_07_dmg_02'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_07_02'.
	sequence 'destroy_08':
		TRIGGER TIMES 1
		Disable object 'g_window_08'.
		Enable object 'g_window_08_dmg'.
		Enable body 'body_window_08_dmg'.
		Disable body 'body_window_08'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_08' (alarm reason: 'glass').
	sequence 'destroy_dmg_08':
		TRIGGER TIMES 1
		Disable object 'g_window_08'.
		Disable object 'g_window_08_dmg'.
		Play audio 'window_medium_shatter' at 'e_window_08'.
		Disable decal_mesh 'g_window_08'.
		Disable decal_mesh 'g_window_08_dmg'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_08' )
			position v()
		Disable body 'body_window_08_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_08' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_08'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_08'.
	body 'body_window_08_dmg'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_08'.
	sequence 'destroy_09':
		TRIGGER TIMES 1
		Disable object 'g_window_09'.
		Enable object 'g_window_09_dmg_01'.
		Enable object 'g_window_09_dmg_02'.
		Enable object 'g_window_09_dmg_03'.
		Enable body 'body_window_09_dmg_01'.
		Enable body 'body_window_09_dmg_02'.
		Enable body 'body_window_09_dmg_03'.
		Disable body 'body_window_09'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_09_01' (alarm reason: 'glass').
	sequence 'destroy_dmg_09_01':
		TRIGGER TIMES 1
		Disable object 'g_window_09'.
		Disable object 'g_window_09_dmg_01'.
		Play audio 'window_medium_shatter' at 'e_window_09_01'.
		Disable decal_mesh 'g_window_09'.
		Disable decal_mesh 'g_window_09_dmg_01'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_09_01' )
			position v()
		Disable body 'body_window_09_dmg_01'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_09_01' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	sequence 'destroy_dmg_09_02':
		TRIGGER TIMES 1
		Disable object 'g_window_09'.
		Disable object 'g_window_09_dmg_02'.
		Play audio 'window_medium_shatter' at 'e_window_09_02'.
		Disable decal_mesh 'g_window_09'.
		Disable decal_mesh 'g_window_09_dmg_02'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_09_02' )
			position v()
		Disable body 'body_window_09_dmg_02'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_09_02' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	sequence 'destroy_dmg_09_03':
		TRIGGER TIMES 1
		Disable object 'g_window_09'.
		Disable object 'g_window_09_dmg_03'.
		Play audio 'window_medium_shatter' at 'e_window_09_03'.
		Disable decal_mesh 'g_window_09'.
		Disable decal_mesh 'g_window_09_dmg_03'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_09_03' )
			position v()
		Disable body 'body_window_09_dmg_03'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_09_03' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_09'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_09'.
	body 'body_window_09_dmg_01'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_09_01'.
	body 'body_window_09_dmg_02'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_09_02'.
	body 'body_window_09_dmg_03'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_09_03'.
	sequence 'destroy_10':
		TRIGGER TIMES 1
		Disable object 'g_window_10'.
		Enable object 'g_window_10_dmg'.
		Enable body 'body_window_10_dmg'.
		Disable body 'body_window_10'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_10' (alarm reason: 'glass').
	sequence 'destroy_dmg_10':
		TRIGGER TIMES 1
		Disable object 'g_window_10'.
		Disable object 'g_window_10_dmg'.
		Play audio 'window_medium_shatter' at 'e_window_10'.
		Disable decal_mesh 'g_window_10'.
		Disable decal_mesh 'g_window_10_dmg'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_10' )
			position v()
		Disable body 'body_window_10_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_10' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_10'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_10'.
	body 'body_window_10_dmg'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_10'.
	sequence 'destroy_11':
		TRIGGER TIMES 1
		Disable object 'g_window_11'.
		Enable object 'g_window_11_dmg_01'.
		Enable object 'g_window_11_dmg_02'.
		Enable body 'body_window_11_dmg_01'.
		Enable body 'body_window_11_dmg_02'.
		Disable body 'body_window_11'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_11_01' (alarm reason: 'glass').
	sequence 'destroy_dmg_11_01':
		TRIGGER TIMES 1
		Disable object 'g_window_11'.
		Disable object 'g_window_11_dmg_01'.
		Play audio 'window_medium_shatter' at 'e_window_11_01'.
		Disable decal_mesh 'g_window_11'.
		Disable decal_mesh 'g_window_11_dmg_01'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_11_01' )
			position v()
		Disable body 'body_window_11_dmg_01'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_11_01' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	sequence 'destroy_dmg_11_02':
		TRIGGER TIMES 1
		Disable object 'g_window_11'.
		Disable object 'g_window_11_dmg_02'.
		Play audio 'window_medium_shatter' at 'e_window_11_02'.
		Disable decal_mesh 'g_window_11'.
		Disable decal_mesh 'g_window_11_dmg_02'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_11_02' )
			position v()
		Disable body 'body_window_11_dmg_02'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_11_02' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_11'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_11'.
	body 'body_window_11_dmg_01'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_11_01'.
	body 'body_window_11_dmg_02'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_11_02'.
	sequence 'destroy_12':
		TRIGGER TIMES 1
		Disable object 'g_window_12'.
		Enable object 'g_window_12_dmg'.
		Enable body 'body_window_12_dmg'.
		Disable body 'body_window_12'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_12' (alarm reason: 'glass').
	sequence 'destroy_dmg_12':
		TRIGGER TIMES 1
		Disable object 'g_window_12'.
		Disable object 'g_window_12_dmg'.
		Play audio 'window_medium_shatter' at 'e_window_12'.
		Disable decal_mesh 'g_window_12'.
		Disable decal_mesh 'g_window_12_dmg'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_12' )
			position v()
		Disable body 'body_window_12_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_12' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_12'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_12'.
	body 'body_window_12_dmg'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_12'.
	sequence 'destroy_13':
		TRIGGER TIMES 1
		Disable object 'g_window_13'.
		Enable object 'g_window_13_dmg'.
		Enable body 'body_window_13_dmg'.
		Disable body 'body_window_13'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_13' (alarm reason: 'glass').
	sequence 'destroy_dmg_13':
		TRIGGER TIMES 1
		Disable object 'g_window_13'.
		Disable object 'g_window_13_dmg'.
		Play audio 'window_medium_shatter' at 'e_window_13'.
		Disable decal_mesh 'g_window_13'.
		Disable decal_mesh 'g_window_13_dmg'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_13' )
			position v()
		Disable body 'body_window_13_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_13' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_13'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_13'.
	body 'body_window_13_dmg'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_13'.
	sequence 'destroy_14':
		TRIGGER TIMES 1
		Disable object 'g_window_14'.
		Enable object 'g_window_14_dmg'.
		Enable body 'body_window_14_dmg'.
		Disable body 'body_window_14'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_14' (alarm reason: 'glass').
	sequence 'destroy_dmg_14':
		TRIGGER TIMES 1
		Disable object 'g_window_14'.
		Disable object 'g_window_14_dmg'.
		Play audio 'window_medium_shatter' at 'e_window_14'.
		Disable decal_mesh 'g_window_14'.
		Disable decal_mesh 'g_window_14_dmg'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_14' )
			position v()
		Disable body 'body_window_14_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_14' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_14'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_14'.
	body 'body_window_14_dmg'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_14'.
	sequence 'destroy_15':
		TRIGGER TIMES 1
		Disable object 'g_window_15'.
		Enable object 'g_window_15_dmg'.
		Enable body 'body_window_15_dmg'.
		Disable body 'body_window_15'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_15' (alarm reason: 'glass').
	sequence 'destroy_dmg_15':
		TRIGGER TIMES 1
		Disable object 'g_window_15'.
		Disable object 'g_window_15_dmg'.
		Play audio 'window_medium_shatter' at 'e_window_15'.
		Disable decal_mesh 'g_window_15'.
		Disable decal_mesh 'g_window_15_dmg'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_15' )
			position v()
		Disable body 'body_window_15_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_15' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_15'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_15'.
	body 'body_window_15_dmg'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_15'.
	sequence 'destroy_16':
		TRIGGER TIMES 1
		Disable object 'g_window_16'.
		Enable object 'g_window_16_dmg'.
		Enable body 'body_window_16_dmg'.
		Disable body 'body_window_16'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_16' (alarm reason: 'glass').
	sequence 'destroy_dmg_16':
		TRIGGER TIMES 1
		Disable object 'g_window_16'.
		Disable object 'g_window_16_dmg'.
		Play audio 'window_medium_shatter' at 'e_window_16'.
		Disable decal_mesh 'g_window_16'.
		Disable decal_mesh 'g_window_16_dmg'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_16' )
			position v()
		Disable body 'body_window_16_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_16' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_16'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_16'.
	body 'body_window_16_dmg'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_16'.
	sequence 'destroy_17':
		TRIGGER TIMES 1
		Disable object 'g_window_17'.
		Enable object 'g_window_17_dmg_01'.
		Enable object 'g_window_17_dmg_02'.
		Enable body 'body_window_17_dmg_01'.
		Enable body 'body_window_17_dmg_02'.
		Disable body 'body_window_17'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_17_01' (alarm reason: 'glass').
	sequence 'destroy_dmg_17_01':
		TRIGGER TIMES 1
		Disable object 'g_window_17'.
		Disable object 'g_window_17_dmg_01'.
		Play audio 'window_medium_shatter' at 'e_window_17_01'.
		Disable decal_mesh 'g_window_17'.
		Disable decal_mesh 'g_window_17_dmg_01'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_17_01' )
			position v()
		Disable body 'body_window_17_dmg_01'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_17_02' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	sequence 'destroy_dmg_17_02':
		TRIGGER TIMES 1
		Disable object 'g_window_17'.
		Disable object 'g_window_17_dmg_02'.
		Play audio 'window_medium_shatter' at 'e_window_17_02'.
		Disable decal_mesh 'g_window_17'.
		Disable decal_mesh 'g_window_17_dmg_02'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_17_02' )
			position v()
		Disable body 'body_window_17_dmg_02'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_17_02' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_17'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_17'.
	body 'body_window_17_dmg_01'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_17_01'.
	body 'body_window_17_dmg_02'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_17_02'.
	sequence 'destroy_18':
		TRIGGER TIMES 1
		Disable object 'g_window_18'.
		Enable object 'g_window_18_dmg_01'.
		Enable object 'g_window_18_dmg_02'.
		Enable body 'body_window_18_dmg_01'.
		Enable body 'body_window_18_dmg_02'.
		Disable body 'body_window_18'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_18_01' (alarm reason: 'glass').
	sequence 'destroy_dmg_18_01':
		TRIGGER TIMES 1
		Disable object 'g_window_18'.
		Disable object 'g_window_18_dmg_01'.
		Play audio 'window_medium_shatter' at 'e_window_18_01'.
		Disable decal_mesh 'g_window_18'.
		Disable decal_mesh 'g_window_18_dmg_01'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_18_01' )
			position v()
		Disable body 'body_window_18_dmg_01'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_18_01' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	sequence 'destroy_dmg_18_02':
		TRIGGER TIMES 1
		Disable object 'g_window_18'.
		Disable object 'g_window_18_dmg_02'.
		Play audio 'window_medium_shatter' at 'e_window_18_02'.
		Disable decal_mesh 'g_window_18'.
		Disable decal_mesh 'g_window_18_dmg_02'.
		effect 'effects/particles/window/bank_window_vault':
			parent object( 'e_window_18_02' )
			position v()
		Disable body 'body_window_18_dmg_02'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_18_02' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_18'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_18'.
	body 'body_window_18_dmg_01'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_18_01'.
	body 'body_window_18_dmg_02'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'destroy_dmg_18_02'.
