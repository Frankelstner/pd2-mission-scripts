unit:
	body 'window'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_01'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_02'.
	sequence 'destroy_01':
		TRIGGER TIMES 1
		Disable object 'g_glass_01'.
		Enable object 'g_glass_01_dmg'.
		Play audio 'glass_crack' at 'e_glass_01'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_01' (alarm reason: 'glass').
	sequence 'destroy_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_01'.
		Disable object 'g_glass_01_dmg'.
		Play audio 'window_medium_shatter' at 'e_glass_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_01')
			position v()
		Disable body 'window'.
		Disable decal_mesh 'dm_glass_breakable_01'.
		Cause alert with 12 m radius.
