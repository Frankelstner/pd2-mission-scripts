unit:
	number_order = 1
	sequence 'set_number_0':
		Disable object 'g_error'.
		If number_order == 1: Enable object 'g_number_0_pos_01'.
		If number_order == 2: Enable object 'g_number_0_pos_02'.
		If number_order == 3: Enable object 'g_number_0_pos_03'.
		If number_order == 4: Enable object 'g_number_0_pos_04'.
		If number_order == 3: filter = 'filter_third'
		If number_order == 3: number_order = 4
		If number_order == 2: filter = 'filter_second'
		If number_order == 2: number_order = 3
		If number_order == 1: filter = 'filter_first'
		If number_order == 1: number_order = 2
	sequence 'set_number_1':
		Disable object 'g_error'.
		If number_order == 1: Enable object 'g_number_1_pos_01'.
		If number_order == 2: Enable object 'g_number_1_pos_02'.
		If number_order == 3: Enable object 'g_number_1_pos_03'.
		If number_order == 4: Enable object 'g_number_1_pos_04'.
		If number_order == 3: filter = 'filter_third'
		If number_order == 3: number_order = 4
		If number_order == 2: filter = 'filter_second'
		If number_order == 2: number_order = 3
		If number_order == 1: filter = 'filter_first'
		If number_order == 1: number_order = 2
	sequence 'set_number_2':
		Disable object 'g_error'.
		If number_order == 1: Enable object 'g_number_2_pos_01'.
		If number_order == 2: Enable object 'g_number_2_pos_02'.
		If number_order == 3: Enable object 'g_number_2_pos_03'.
		If number_order == 4: Enable object 'g_number_2_pos_04'.
		If number_order == 3: filter = 'filter_third'
		If number_order == 3: number_order = 4
		If number_order == 2: filter = 'filter_second'
		If number_order == 2: number_order = 3
		If number_order == 1: filter = 'filter_first'
		If number_order == 1: number_order = 2
	sequence 'set_number_3':
		Disable object 'g_error'.
		If number_order == 1: Enable object 'g_number_3_pos_01'.
		If number_order == 2: Enable object 'g_number_3_pos_02'.
		If number_order == 3: Enable object 'g_number_3_pos_03'.
		If number_order == 4: Enable object 'g_number_3_pos_04'.
		If number_order == 3: filter = 'filter_third'
		If number_order == 3: number_order = 4
		If number_order == 2: filter = 'filter_second'
		If number_order == 2: number_order = 3
		If number_order == 1: filter = 'filter_first'
		If number_order == 1: number_order = 2
	sequence 'set_number_4':
		Disable object 'g_error'.
		If number_order == 1: Enable object 'g_number_4_pos_01'.
		If number_order == 2: Enable object 'g_number_4_pos_02'.
		If number_order == 3: Enable object 'g_number_4_pos_03'.
		If number_order == 4: Enable object 'g_number_4_pos_04'.
		If number_order == 3: filter = 'filter_third'
		If number_order == 3: number_order = 4
		If number_order == 2: filter = 'filter_second'
		If number_order == 2: number_order = 3
		If number_order == 1: filter = 'filter_first'
		If number_order == 1: number_order = 2
	sequence 'set_number_5':
		Disable object 'g_error'.
		If number_order == 1: Enable object 'g_number_5_pos_01'.
		If number_order == 2: Enable object 'g_number_5_pos_02'.
		If number_order == 3: Enable object 'g_number_5_pos_03'.
		If number_order == 4: Enable object 'g_number_5_pos_04'.
		If number_order == 3: filter = 'filter_third'
		If number_order == 3: number_order = 4
		If number_order == 2: filter = 'filter_second'
		If number_order == 2: number_order = 3
		If number_order == 1: filter = 'filter_first'
		If number_order == 1: number_order = 2
	sequence 'set_number_6':
		Disable object 'g_error'.
		If number_order == 1: Enable object 'g_number_6_pos_01'.
		If number_order == 2: Enable object 'g_number_6_pos_02'.
		If number_order == 3: Enable object 'g_number_6_pos_03'.
		If number_order == 4: Enable object 'g_number_6_pos_04'.
		If number_order == 3: filter = 'filter_third'
		If number_order == 3: number_order = 4
		If number_order == 2: filter = 'filter_second'
		If number_order == 2: number_order = 3
		If number_order == 1: filter = 'filter_first'
		If number_order == 1: number_order = 2
	sequence 'set_number_7':
		Disable object 'g_error'.
		If number_order == 1: Enable object 'g_number_7_pos_01'.
		If number_order == 2: Enable object 'g_number_7_pos_02'.
		If number_order == 3: Enable object 'g_number_7_pos_03'.
		If number_order == 4: Enable object 'g_number_7_pos_04'.
		If number_order == 3: filter = 'filter_third'
		If number_order == 3: number_order = 4
		If number_order == 2: filter = 'filter_second'
		If number_order == 2: number_order = 3
		If number_order == 1: filter = 'filter_first'
		If number_order == 1: number_order = 2
	sequence 'set_number_8':
		Disable object 'g_error'.
		If number_order == 1: Enable object 'g_number_8_pos_01'.
		If number_order == 2: Enable object 'g_number_8_pos_02'.
		If number_order == 3: Enable object 'g_number_8_pos_03'.
		If number_order == 4: Enable object 'g_number_8_pos_04'.
		If number_order == 3: filter = 'filter_third'
		If number_order == 3: number_order = 4
		If number_order == 2: filter = 'filter_second'
		If number_order == 2: number_order = 3
		If number_order == 1: filter = 'filter_first'
		If number_order == 1: number_order = 2
	sequence 'set_number_9':
		Disable object 'g_error'.
		If number_order == 1: Enable object 'g_number_9_pos_01'.
		If number_order == 2: Enable object 'g_number_9_pos_02'.
		If number_order == 3: Enable object 'g_number_9_pos_03'.
		If number_order == 4: Enable object 'g_number_9_pos_04'.
		If number_order == 3: filter = 'filter_third'
		If number_order == 3: number_order = 4
		If number_order == 2: filter = 'filter_second'
		If number_order == 2: number_order = 3
		If number_order == 1: filter = 'filter_first'
		If number_order == 1: number_order = 2
	sequence 'show_opened_text':
		Enable object 'g_opened'.
		Disable object 'g_error'.
		Disable object 'g_number_0_pos_01'.
		Disable object 'g_number_0_pos_02'.
		Disable object 'g_number_0_pos_03'.
		Disable object 'g_number_0_pos_04'.
		Disable object 'g_number_1_pos_01'.
		Disable object 'g_number_1_pos_02'.
		Disable object 'g_number_1_pos_03'.
		Disable object 'g_number_1_pos_04'.
		Disable object 'g_number_2_pos_01'.
		Disable object 'g_number_2_pos_02'.
		Disable object 'g_number_2_pos_03'.
		Disable object 'g_number_2_pos_04'.
		Disable object 'g_number_3_pos_01'.
		Disable object 'g_number_3_pos_02'.
		Disable object 'g_number_3_pos_03'.
		Disable object 'g_number_3_pos_04'.
		Disable object 'g_number_4_pos_01'.
		Disable object 'g_number_4_pos_02'.
		Disable object 'g_number_4_pos_03'.
		Disable object 'g_number_4_pos_04'.
		Disable object 'g_number_5_pos_01'.
		Disable object 'g_number_5_pos_02'.
		Disable object 'g_number_5_pos_03'.
		Disable object 'g_number_5_pos_04'.
		Disable object 'g_number_6_pos_01'.
		Disable object 'g_number_6_pos_02'.
		Disable object 'g_number_6_pos_03'.
		Disable object 'g_number_6_pos_04'.
		Disable object 'g_number_7_pos_01'.
		Disable object 'g_number_7_pos_02'.
		Disable object 'g_number_7_pos_03'.
		Disable object 'g_number_7_pos_04'.
		Disable object 'g_number_8_pos_01'.
		Disable object 'g_number_8_pos_02'.
		Disable object 'g_number_8_pos_03'.
		Disable object 'g_number_8_pos_04'.
		Disable object 'g_number_9_pos_01'.
		Disable object 'g_number_9_pos_02'.
		Disable object 'g_number_9_pos_03'.
		Disable object 'g_number_9_pos_04'.
	sequence 'hide_opened_text':
		Disable object 'g_opened'.
	sequence 'show_error_text':
		Disable object 'g_opened'.
		Enable object 'g_error'.
		Disable object 'g_number_0_pos_01'.
		Disable object 'g_number_0_pos_02'.
		Disable object 'g_number_0_pos_03'.
		Disable object 'g_number_0_pos_04'.
		Disable object 'g_number_1_pos_01'.
		Disable object 'g_number_1_pos_02'.
		Disable object 'g_number_1_pos_03'.
		Disable object 'g_number_1_pos_04'.
		Disable object 'g_number_2_pos_01'.
		Disable object 'g_number_2_pos_02'.
		Disable object 'g_number_2_pos_03'.
		Disable object 'g_number_2_pos_04'.
		Disable object 'g_number_3_pos_01'.
		Disable object 'g_number_3_pos_02'.
		Disable object 'g_number_3_pos_03'.
		Disable object 'g_number_3_pos_04'.
		Disable object 'g_number_4_pos_01'.
		Disable object 'g_number_4_pos_02'.
		Disable object 'g_number_4_pos_03'.
		Disable object 'g_number_4_pos_04'.
		Disable object 'g_number_5_pos_01'.
		Disable object 'g_number_5_pos_02'.
		Disable object 'g_number_5_pos_03'.
		Disable object 'g_number_5_pos_04'.
		Disable object 'g_number_6_pos_01'.
		Disable object 'g_number_6_pos_02'.
		Disable object 'g_number_6_pos_03'.
		Disable object 'g_number_6_pos_04'.
		Disable object 'g_number_7_pos_01'.
		Disable object 'g_number_7_pos_02'.
		Disable object 'g_number_7_pos_03'.
		Disable object 'g_number_7_pos_04'.
		Disable object 'g_number_8_pos_01'.
		Disable object 'g_number_8_pos_02'.
		Disable object 'g_number_8_pos_03'.
		Disable object 'g_number_8_pos_04'.
		Disable object 'g_number_9_pos_01'.
		Disable object 'g_number_9_pos_02'.
		Disable object 'g_number_9_pos_03'.
		Disable object 'g_number_9_pos_04'.
	sequence 'reset':
		Disable object 'g_number_0_pos_01'.
		Disable object 'g_number_0_pos_02'.
		Disable object 'g_number_0_pos_03'.
		Disable object 'g_number_0_pos_04'.
		Disable object 'g_number_1_pos_01'.
		Disable object 'g_number_1_pos_02'.
		Disable object 'g_number_1_pos_03'.
		Disable object 'g_number_1_pos_04'.
		Disable object 'g_number_2_pos_01'.
		Disable object 'g_number_2_pos_02'.
		Disable object 'g_number_2_pos_03'.
		Disable object 'g_number_2_pos_04'.
		Disable object 'g_number_3_pos_01'.
		Disable object 'g_number_3_pos_02'.
		Disable object 'g_number_3_pos_03'.
		Disable object 'g_number_3_pos_04'.
		Disable object 'g_number_4_pos_01'.
		Disable object 'g_number_4_pos_02'.
		Disable object 'g_number_4_pos_03'.
		Disable object 'g_number_4_pos_04'.
		Disable object 'g_number_5_pos_01'.
		Disable object 'g_number_5_pos_02'.
		Disable object 'g_number_5_pos_03'.
		Disable object 'g_number_5_pos_04'.
		Disable object 'g_number_6_pos_01'.
		Disable object 'g_number_6_pos_02'.
		Disable object 'g_number_6_pos_03'.
		Disable object 'g_number_6_pos_04'.
		Disable object 'g_number_7_pos_01'.
		Disable object 'g_number_7_pos_02'.
		Disable object 'g_number_7_pos_03'.
		Disable object 'g_number_7_pos_04'.
		Disable object 'g_number_8_pos_01'.
		Disable object 'g_number_8_pos_02'.
		Disable object 'g_number_8_pos_03'.
		Disable object 'g_number_8_pos_04'.
		Disable object 'g_number_9_pos_01'.
		Disable object 'g_number_9_pos_02'.
		Disable object 'g_number_9_pos_03'.
		Disable object 'g_number_9_pos_04'.
		number_order = 1
	sequence 'hide_error_text':
		Disable object 'g_error'.
