unit:
	sequence 'play_fan_animation':
		Enable animation_group 'anim_fan_group':
			loop True
			speed 1
	sequence 'play_fan_animation_fast':
		Enable animation_group 'anim_fan_group':
			loop True
			speed 2
	sequence 'play_fan_animation_slow':
		Enable animation_group 'anim_fan_group':
			loop True
			speed 0.1
	sequence 'state_power_off':
		Disable object 'g_cone'.
		Disable object 'g_plane'.
		Disable light 'ls_spotlight'.
		Enable animation_group 'anim_fan_group':
			speed 0
	sequence 'state_power_on':
		Enable object 'g_cone'.
		Enable object 'g_plane'.
		Enable light 'ls_spotlight'.
		Enable animation_group 'anim_fan_group':
			loop True
			speed 0.1
	sequence 'state_fan_still':
		Enable animation_group 'anim_fan_group':
			from 0/30
			to 0/30
