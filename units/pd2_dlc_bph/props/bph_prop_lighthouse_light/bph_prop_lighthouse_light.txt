unit:
	sequence 'int_seq_hide_light':
		Disable object 'g_glowplane'.
		Disable body 'body_light'.
		Enable effect_spawner 'sparks'.
		Disable effect_spawner 'lighthouse'.
		Run sequence 'done_break'.
	sequence 'spin':
		EXECUTE ON STARTUP
		Enable animation_group 'anim_rotation':
			loop True
			speed 0.25
		startup True
	sequence 'done_break'.
	body 'body_light'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_hide_light'.
