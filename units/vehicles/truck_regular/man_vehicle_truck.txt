unit:
	sequence 'kill_window_front':
		TRIGGER TIMES 1
		Enable object 'g_window_front_dmg'.
		Enable object 'g_window_front'.
	sequence 'kill_window_right':
		TRIGGER TIMES 1
		Enable object 'g_window_right_dmg'.
		Enable object 'g_window_right'.
	sequence 'kill_window_left':
		TRIGGER TIMES 1
		Enable object 'g_window_left_dmg'.
		Enable object 'g_window_left'.
	body 'window_front'
		Upon receiving 1 bullet hit or 10 explosion damage or 20 melee damage, execute:
			Run sequence 'kill_window_front'.
	body 'window_right'
		Upon receiving 1 bullet hit or 10 explosion damage or 20 melee damage, execute:
			Run sequence 'kill_window_right'.
	body 'window_left'
		Upon receiving 1 bullet hit or 10 explosion damage or 20 melee damage, execute:
			Run sequence 'kill_window_left'.
	sequence 'trigger_car_alarm_infinitely':
		Disable object 'g_lights'.
		Disable object 'g_lights_rear_effect'.
		Disable object 'g_lights_left_effect'.
		Disable object 'g_lights_right_effect'.
		Enable animation_group 'alarm_lights':
			from 0/30
			loop True
			to 20/30
		Enable object 'g_lights_il'.
		Enable light 'ls_left'.
		Enable light 'ls_right'.
		Enable light 'ls_rear'.
	sequence 'turn_headlights_on':
		Enable object 'g_lights_il'.
		Enable light 'ls_left'.
		Enable light 'ls_right'.
		Enable light 'ls_rear'.
	sequence 'turn_fake_lights_on':
		Enable object 'g_lights'.
		Enable object 'g_lights_rear_effect'.
		Enable object 'g_lights_left_effect'.
		Enable object 'g_lights_right_effect'.
		Disable object 'g_lights_il'.
		Disable light 'ls_right'.
		Disable light 'ls_left'.
		Disable light 'ls_rear'.
	sequence 'turn_lights_off':
		Disable object 'g_lights'.
		Disable object 'g_lights_il'.
		Disable object 'g_lights_rear_effect'.
		Disable object 'g_lights_left_effect'.
		Disable object 'g_lights_right_effect'.
		Disable light 'ls_right'.
		Disable light 'ls_left'.
		Disable light 'ls_rear'.
