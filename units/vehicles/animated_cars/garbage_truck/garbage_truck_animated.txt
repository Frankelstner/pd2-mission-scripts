unit:
	sequence 'getaway_bank_animation':
		Enable animation_group 'animation':
			from 0
			to 6
		Play audio 'garbage_truck_anim_01' at 'a_anim'.
		Run sequence 'getaway_bank_animation_finished'. (DELAY 6)
	sequence 'getaway_bank_animation_finished':
		Enable object 'g_body'.
	sequence 'getaway_bank_short_animation':
		Enable animation_group 'animation':
			from 3.666
		Play audio 'garbage_truck_anim_02' at 'a_anim'.
		Run sequence 'getaway_bank_short_animation_finnished'. (DELAY 2.283)
	sequence 'getaway_bank_short_animation_finnished':
		Disable object 'g_body'.
	sequence 'getaway_slght_state':
		Enable animation_group 'animation':
			from 140/30
			speed 0
	sequence 'getaway_slght_anim':
		Enable animation_group 'animation':
			from 140/30
			speed 1
		Play audio 'garbage_truck_anim_03' at 'a_anim'.
		Run sequence 'getaway_slght_anim_finnished'. (DELAY 55/30)
	sequence 'getaway_slght_anim_finnished'.
	sequence 'hide_character':
		Disable object 'g_body'.
	sequence 'show':
		Enable body 'animated_body'.
		Disable animation_group 'animation':
			from 0
		Enable object 'g_window'.
		Enable object 'g_garbagetruck'.
		Enable object 'g_body'.
		Enable object 'g_left_wheel'.
		Enable object 'g_right_wheel'.
		Enable decal_mesh 'dm_rubber'.
		Enable decal_mesh 'dm_plastic'.
		Enable decal_mesh 'dm_sheetmetal'.
		Enable decal_mesh 'dm_glas'.
		Enable decal_mesh 'dm_metal'.
		Enable decal_mesh 'dm_right_wheel_rubber'.
		Enable decal_mesh 'dm_right_wheel_sheetmetal'.
		Enable decal_mesh 'dm_left_wheel_rubber'.
		Enable decal_mesh 'dm_left_wheel_sheetmetal'.
	sequence 'hide':
		Disable body 'animated_body'.
		Disable animation_group 'animation':
			from 0
		Disable object 'g_window'.
		Disable object 'g_garbagetruck'.
		Disable object 'g_body'.
		Disable object 'g_left_wheel'.
		Disable object 'g_right_wheel'.
		Disable decal_mesh 'dm_rubber'.
		Disable decal_mesh 'dm_plastic'.
		Disable decal_mesh 'dm_sheetmetal'.
		Disable decal_mesh 'dm_glas'.
		Disable decal_mesh 'dm_metal'.
		Disable decal_mesh 'dm_right_wheel_rubber'.
		Disable decal_mesh 'dm_right_wheel_sheetmetal'.
		Disable decal_mesh 'dm_left_wheel_rubber'.
		Disable decal_mesh 'dm_left_wheel_sheetmetal'.
