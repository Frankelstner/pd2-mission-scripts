unit:
	sequence 'hide':
		Disable object 'g_vehicle_police_ny'.
		Disable object 'g_window_right_rear'.
		Disable object 'g_window_right_rear_dmg'.
		Disable object 'g_right_door_window'.
		Disable object 'g_right_door_window_dmg'.
		Disable object 'g_window_left_rear'.
		Disable object 'g_window_left_rear_dmg'.
		Disable object 'g_left_door_window_dmg'.
		Disable object 'g_left_door_window'.
		Disable object 'g_window_back_dmg'.
		Disable object 'g_window_back'.
		Disable object 'g_window_front_dmg'.
		Disable object 'g_window_front'.
		Disable object 'g_window_interior'.
		Disable object 'g_right_door'.
		Disable object 'g_left_door'.
		Disable object 'g_wheels'.
		Disable object 'g_orange'.
		Disable object 'g_base'.
		Disable object 'g_illum'.
		Disable object 'g_cup'.
		Disable object 'g_illum1'.
		Disable object 'g_cop'.
		Disable effect_spawner 'es_e_efx'.
	sequence 'show':
		Enable object 'g_vehicle_police_ny'.
		Enable object 'g_window_right_rear'.
		Disable object 'g_window_right_rear_dmg'.
		Enable object 'g_right_door_window'.
		Disable object 'g_right_door_window_dmg'.
		Enable object 'g_window_left_rear'.
		Disable object 'g_window_left_rear_dmg'.
		Disable object 'g_left_door_window_dmg'.
		Enable object 'g_left_door_window'.
		Disable object 'g_window_back_dmg'.
		Enable object 'g_window_back'.
		Disable object 'g_window_front_dmg'.
		Enable object 'g_window_front'.
		Enable object 'g_window_interior'.
		Enable object 'g_right_door'.
		Enable object 'g_left_door'.
		Enable object 'g_wheels'.
		Enable object 'g_orange'.
		Enable object 'g_base'.
		Enable object 'g_illum'.
		Enable object 'g_cup'.
		Enable object 'g_illum1'.
		Enable object 'g_cop'.
		Enable effect_spawner 'es_e_efx'.
	sequence 'hide_cop':
		Disable object 'g_cop'.
	sequence 'bank_drive_in_01':
		Play audio 'policecar_approaching' at 'police_siren'.
		Run sequence 'hide'.
		Run sequence 'drive_in_01'. (DELAY 30)
	sequence 'drive_in_01':
		Enable animation_group 'anim': (DELAY 2.5)
			end_time 150/30
			time 15/30
		Play audio 'policecar_closing' at 'police_siren'. (DELAY 2.5)
		Play audio 'policecar_arrive' at 'police_siren'. (DELAY 3.5)
		Enable object 'g_police_lights_on_red'.
		Enable object 'g_police_lights_on_blue'.
		Disable object 'g_police_lights_off'.
		Run sequence 'show'. (DELAY 2.5)
		Run sequence 'bank_drive_in_01_finished'. (DELAY 6.5)
	sequence 'bank_drive_in_01_finished'.
	sequence 'bank_drive_in_02':
		Play audio 'policecar_approaching' at 'police_siren'.
		Run sequence 'hide'.
		Run sequence 'drive_in_02'. (DELAY 30)
	sequence 'drive_in_02':
		Enable animation_group 'anim':
			end_time 360/30
			time 160/30
		Play audio 'policecar_closing' at 'police_siren'.
		Play audio 'policecar_arrive' at 'police_siren'. (DELAY 3.5)
		Enable object 'g_police_lights_on_red'.
		Enable object 'g_police_lights_on_blue'.
		Disable object 'g_police_lights_off'.
		Run sequence 'show'.
		Run sequence 'bank_drive_in_02_finished'. (DELAY 200/30)
	sequence 'bank_drive_in_02_finished'.
	sequence 'bank_drive_in_03':
		Play audio 'policecar_approaching' at 'police_siren'.
		Run sequence 'hide'.
		Run sequence 'drive_in_03'. (DELAY 30)
	sequence 'drive_in_03':
		Enable animation_group 'anim':
			end_time 640/30
			time 415/30
		Play audio 'policecar_closing' at 'police_siren'.
		Play audio 'policecar_arrive' at 'police_siren'. (DELAY 4.5)
		Enable object 'g_police_lights_on_red'.
		Enable object 'g_police_lights_on_blue'.
		Disable object 'g_police_lights_off'.
		Run sequence 'show'.
		Run sequence 'bank_drive_in_03_finished'. (DELAY 225/30)
	sequence 'bank_drive_in_03_finished'.
	sequence 'bank_drive_in_04':
		Play audio 'policecar_approaching' at 'police_siren'.
		Run sequence 'hide'.
		Run sequence 'drive_in_04'. (DELAY 30)
	sequence 'drive_in_04':
		Enable animation_group 'anim': (DELAY 2)
			end_time 865/30
			time 720/30
		Play audio 'policecar_closing' at 'police_siren'.
		Play audio 'policecar_arrive' at 'police_siren'. (DELAY 4.3)
		Enable object 'g_police_lights_on_red'.
		Enable object 'g_police_lights_on_blue'.
		Disable object 'g_police_lights_off'.
		Run sequence 'show'. (DELAY 2)
		Run sequence 'bank_drive_in_04_finished'. (DELAY 6.3)
	sequence 'bank_drive_in_04_finished'.
	sequence 'kill_tire_front_right':
		Enable animation_group 'ag_tire_front_right':
			end_time 25/30
			time 0/30
		Play audio 'tire_blow' at 'a_tire_front_right'.
		effect 'effects/particles/bullet_hit/misc/misc_tire':
			align dest_normal
			position pos
	body 'tire_front_right'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'kill_tire_front_right'.
	sequence 'kill_tire_front_left':
		Enable animation_group 'ag_tire_front_left':
			end_time 25/30
			time 0/30
		Play audio 'tire_blow' at 'a_tire_front_left'.
		effect 'effects/particles/bullet_hit/misc/misc_tire':
			align dest_normal
			position pos
	body 'tire_front_left'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'kill_tire_front_left'.
	sequence 'kill_tire_rear_right':
		Enable animation_group 'ag_tire_rear_right':
			end_time 25/30
			time 0/30
		Play audio 'tire_blow' at 'a_tire_rear_right'.
		effect 'effects/particles/bullet_hit/misc/misc_tire':
			align dest_normal
			position pos
	body 'tire_rear_right'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'kill_tire_rear_right'.
	sequence 'kill_tire_rear_left':
		Enable animation_group 'ag_tire_rear_left':
			end_time 25/30
			time 0/30
		Play audio 'tire_blow' at 'a_tire_rear_left'.
		effect 'effects/particles/bullet_hit/misc/misc_tire':
			align dest_normal
			position pos
	body 'tire_rear_left'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'kill_tire_rear_left'.
	sequence 'kill_window_front_01':
		TRIGGER TIMES 1
		Enable object 'g_window_front_dmg'.
		Disable object 'g_window_front'.
	body 'window_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_front_01'.
	sequence 'kill_window_right_front_01':
		TRIGGER TIMES 1
		Enable object 'g_right_door_window_dmg'.
		Disable object 'g_right_door_window'.
	sequence 'kill_window_right_front_02':
		TRIGGER TIMES 1
		Disable object 'g_right_door_window_dmg'.
		Disable decal_mesh 'dm_right_door_window'.
		Play audio 'window_small_shatter' at 'e_window_right_front'.
		effect 'effects/particles/window/vehicle_window':
			parent object( 'e_window_right_front' )
			position v()
		Disable body 'window_right_front'.
	body 'window_right_front'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'kill_window_right_front_01'.
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_front_02'.
	sequence 'kill_window_right_rear_01':
		TRIGGER TIMES 1
		Enable object 'g_window_right_rear_dmg'.
		Disable object 'g_window_right_rear'.
	sequence 'kill_window_right_rear_02':
		TRIGGER TIMES 1
		Disable object 'g_window_right_rear_dmg'.
		Disable decal_mesh 'dm_window_right_rear'.
		Play audio 'window_small_shatter' at 'e_window_right_rear'.
		effect 'effects/particles/window/vehicle_window':
			parent object( 'e_window_right_rear' )
			position v()
		Disable body 'window_right_rear'.
	body 'window_right_rear'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'kill_window_right_rear_01'.
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_rear_02'.
	sequence 'kill_window_left_front_01':
		TRIGGER TIMES 1
		Enable object 'g_left_door_window_dmg'.
		Disable object 'g_left_door_window'.
	sequence 'kill_window_left_front_02':
		TRIGGER TIMES 1
		Disable object 'g_left_door_window_dmg'.
		Disable decal_mesh 'dm_left_door_window'.
		Play audio 'window_small_shatter' at 'e_window_left_front'.
		effect 'effects/particles/window/vehicle_window':
			parent object( 'e_window_left_front' )
			position v()
		Disable body 'window_left_front'.
	body 'window_left_front'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'kill_window_left_front_01'.
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_front_02'.
	sequence 'kill_window_left_rear_01':
		TRIGGER TIMES 1
		Enable object 'g_window_left_rear_dmg'.
		Disable object 'g_window_left_rear'.
	sequence 'kill_window_left_rear_02':
		TRIGGER TIMES 1
		Disable object 'g_window_left_rear_dmg'.
		Disable decal_mesh 'dm_window_left_rear'.
		Play audio 'window_small_shatter' at 'e_window_left_rear'.
		effect 'effects/particles/window/vehicle_window':
			parent object( 'e_window_left_rear' )
			position v()
		Disable body 'window_left_rear'.
	body 'window_left_rear'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'kill_window_left_rear_01'.
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_rear_02'.
	sequence 'kill_window_back_01':
		TRIGGER TIMES 1
		Enable object 'g_window_back_dmg'.
		Disable object 'g_window_back'.
	body 'window_back'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_back_01'.
