unit:
	sequence 'animation1':
		Enable animation_group 'animation':
			end_time 75/24
			time 1/24
	sequence 'animation2':
		Enable animation_group 'animation':
			end_time 402/24
			time 85/24
	sequence 'animation3':
		Enable animation_group 'animation':
			end_time 703/24
			time 430/24
	sequence 'animation4':
		Enable animation_group 'animation':
			end_time 766/24
			time 730/24
	sequence 'animation5':
		Enable animation_group 'animation':
			end_time 872/24
			time 836/24
	sequence 'animation6':
		Enable animation_group 'animation':
			end_time 956/24
			time 900/24
	sequence 'animation7':
		Enable animation_group 'animation':
			end_time 1215/24
			time 980/24
	sequence 'animation8':
		Enable animation_group 'animation':
			end_time 1488/24
			time 1240/24
