unit:
	sequence 'disable_hitbox1':
		Disable body 'car1_animation1_hitbox'.
	body 'car1_animation1_hitbox'
		Upon receiving 3 bullet hits or 10 explosion damage, execute:
			Run sequence 'done_hitbox1'.
	sequence 'open_door_left_front':
		Enable animation_group 'door_left_front':
			from 0
			to 34/30
		Play audio 'und_cardoor_open' at 'a_door_left_front'.
	sequence 'open_door_left_back':
		Enable animation_group 'door_left_back':
			from 0
			to 34/30
		Play audio 'und_cardoor_open' at 'a_door_left_back'.
	sequence 'open_door_right_front':
		Enable animation_group 'door_right_front':
			from 0
			to 34/30
		Play audio 'suburban_door' at 'a_door_right_front'.
	sequence 'open_door_right_back':
		Enable animation_group 'door_right_back':
			from 0
			to 34/30
		Play audio 'suburban_door' at 'a_door_right_back'.
	sequence 'close_door_left_front':
		Enable animation_group 'door_left_front':
			from 40/30
			to 50/30
		Play audio 'und_cardoor_close' at 'a_door_left_front'.
	sequence 'close_door_left_back':
		Enable animation_group 'door_left_back':
			from 40/30
			to 50/30
		Play audio 'und_cardoor_close' at 'a_door_left_back'.
	sequence 'close_door_right_front':
		Enable animation_group 'door_right_front':
			from 40/30
			to 50/30
		Play audio 'und_cardoor_close' at 'a_door_right_front'.
	sequence 'close_door_right_back':
		Enable animation_group 'door_right_back':
			from 40/30
			to 50/30
		Play audio 'und_cardoor_close' at 'a_door_right_back'.
	sequence 'anim1':
		Enable animation_group 'anim':
			from 0/30
			to 356/30
		Play audio 'und_car_1_and_2' at 'body'.
		Run sequence 'done_anim1'. (DELAY 356/30)
	sequence 'anim2':
		Enable animation_group 'anim':
			from 400/30
			to 732/30
		Play audio 'und_car_1_and_2' at 'body'.
		Run sequence 'done_anim2'. (DELAY 332/30)
	sequence 'anim3':
		Enable animation_group 'anim':
			from 830/30
			to 1100/30
		Play audio 'und_car_3' at 'body'.
		Run sequence 'done_anim3'. (DELAY 270/30)
	sequence 'anim4':
		Enable animation_group 'anim':
			from 1203/30
			to 1525/30
		Play audio 'und_car_4' at 'body'.
		Run sequence 'done_anim4'. (DELAY 322/30)
	sequence 'done_anim1'.
	sequence 'done_anim2'.
	sequence 'done_anim3'.
	sequence 'done_anim4'.
	sequence 'done_hitbox1':
		Disable body 'car1_animation1_hitbox'.
	sequence 'done_hitbox2':
		Disable body 'car1_animation2_hitbox'.
	sequence 'kill_window_front_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_front_dmg'.
		Disable object 'g_glass_front'.
	body 'window_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_front_01'.
	sequence 'kill_window_left_front_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_front_l_dmg'.
		Disable object 'g_glass_front_l'.
	sequence 'kill_window_left_front_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_front_l_dmg'.
		Disable decal_mesh 'dm_glass_front_l'.
		Play audio 'window_small_shatter' at 'a_glass_front_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_front_l' )
			position v()
		Disable body 'window_left_front'.
	body 'window_left_front'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'kill_window_left_front_01'.
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_front_01'.
	sequence 'kill_window_right_front_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_front_r_dmg'.
		Disable object 'g_glass_front_r'.
	sequence 'kill_window_right_front_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_front_r_dmg'.
		Disable decal_mesh 'dm_glass_front_r'.
		Play audio 'window_small_shatter' at 'a_glass_front_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_front_r' )
			position v()
		Disable body 'window_right_front'.
	body 'window_right_front'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'kill_window_right_front_01'.
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_front_01'.
	sequence 'kill_window_left_middle_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_middle_l_dmg'.
		Disable object 'g_glass_middle_l'.
	sequence 'kill_window_left_middle_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_middle_l_dmg'.
		Disable decal_mesh 'dm_glass_middle_l'.
		Play audio 'window_small_shatter' at 'a_glass_middle_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_middle_l' )
			position v()
		Disable body 'window_left_middle'.
	body 'window_left_middle'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'kill_window_left_middle_01'.
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_middle_01'.
	sequence 'kill_window_right_middle_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_middle_r_dmg'.
		Disable object 'g_glass_middle_r'.
	sequence 'kill_window_right_middle_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_middle_r_dmg'.
		Disable decal_mesh 'dm_glass_middle_r'.
		Play audio 'window_small_shatter' at 'a_glass_middle_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_middle_r' )
			position v()
		Disable body 'window_right_middle'.
	body 'window_right_middle'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'kill_window_right_middle_01'.
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_middle_01'.
	sequence 'kill_window_left_rear_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_back_l_dmg'.
		Disable object 'g_glass_back_l'.
	sequence 'kill_window_left_rear_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_back_l_dmg'.
		Disable decal_mesh 'dm_glass_back_l'.
		Play audio 'window_small_shatter' at 'a_glass_back_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_back_l' )
			position v()
		Disable body 'window_left_rear'.
	body 'window_left_rear'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'kill_window_left_rear_01'.
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_rear_01'.
	sequence 'kill_window_right_rear_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_back_r_dmg'.
		Disable object 'g_glass_back_r'.
	sequence 'kill_window_right_rear_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_back_r_dmg'.
		Disable decal_mesh 'dm_glass_back_r'.
		Play audio 'window_small_shatter' at 'a_glass_back_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_back_r' )
			position v()
		Disable body 'window_right_rear'.
	body 'window_right_rear'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'kill_window_right_rear_01'.
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_rear_01'.
	sequence 'kill_window_back_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_back_dmg'.
		Disable object 'g_glass_back'.
	body 'window_back'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_back_01'.
