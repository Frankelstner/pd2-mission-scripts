unit:
	sequence 'hide':
		Disable interaction.
		Disable object 'g_g'.
	sequence 'show':
		Enable interaction.
		Enable object 'g_g'.
	sequence 'show_and_enable_interaction':
		Enable interaction.
		Enable object 'g_g'.
		Run sequence 'enable_interaction'.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'interact':
		Run sequence 'done_crowbar'.
		Run sequence 'hide'.
	sequence 'done_crowbar'.
