unit:
	sequence 'interacteble':
		Enable interaction.
	sequence 'not_interacteble':
		Disable interaction.
	sequence 'pickup':
		Enable interaction.
		Disable object 'g_thermite'. (DELAY 0.1)
		Disable object 'g_thermite_decal'. (DELAY 0.1)
	sequence 'interact':
		Run sequence 'pickup'.
