unit:
	sequence 'set_visible':
		Enable interaction.
		Disable object 'g_pile_02'.
		Disable object 'g_des_prop_gascan_02'.
		Disable object 'g_thermite_decal_02'.
		Enable object 'g_des_prop_gascan_interact'.
	sequence 'interact':
		Run sequence 'enable'.
	sequence 'enable':
		Disable interaction.
		Enable object 'g_des_prop_gascan_02'.
		Enable object 'g_thermite_decal_02'.
		Enable object 'g_pile_02'.
		Disable object 'g_des_prop_gascan_interact'.
		Add attention/detection preset 'prop_state_civ_ene_ntl' to 'rp_pile_02' (alarm reason: 'fire').
	sequence 'kill':
		Disable interaction.
		Disable object 'g_pile_02'.
		Disable object 'g_des_prop_gascan_02'.
		Disable object 'g_thermite_decal_02'.
		Disable object 'g_des_prop_gascan_interact'.
	sequence 'set_unvisible'.
