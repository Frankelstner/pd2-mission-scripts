unit:
	sequence 'hidden':
		Disable animation_group 'anim'.
		Disable object 'g_bag'.
	sequence 'alley_drop':
		Enable animation_group 'anim':
			from 0
			to 69/30
		Enable object 'g_bag'.
	sequence 'rooftop_drop':
		Enable animation_group 'anim':
			from 90/30
			to 140/30
		Enable object 'g_bag'.
	sequence 'balcony_drop':
		Enable animation_group 'anim':
			from 158/30
			to 220/30
		Enable object 'g_bag'.
