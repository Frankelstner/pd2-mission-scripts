unit:
	sequence 'hide':
		Stop audio 'window_c4_plant' at 'sound'.
		Disable object 'g_c4'.
		Disable object 'g_c4_glow'.
		Disable body 'static_body'.
		Disable light 'li_red'.
		Disable interaction.
	sequence 'show':
		Play audio 'window_c4_plant' at 'sound'.
		Enable body 'static_body'.
		Enable object 'g_c4'.
		Disable object 'g_c4_glow'.
		Disable light 'li_red'.
		Disable interaction.
	sequence 'show_interactive':
		Enable interaction.
		Enable body 'static_body'.
		Disable object 'g_c4'.
		Enable object 'g_c4_glow'.
		Disable light 'li_red'.
	sequence 'default':
		Run sequence 'show_interactive'.
		reset_editable_state True
	sequence 'flicker_light':
		Enable light 'li_red'. (DELAY 0.1)
		Disable light 'li_red'. (DELAY 0.3)
		Enable light 'li_red'. (DELAY 0.4)
		Disable light 'li_red'. (DELAY 0.6)
		Enable light 'li_red'. (DELAY 0.7)
		Disable light 'li_red'. (DELAY 0.9)
		Enable light 'li_red'. (DELAY 1)
		Disable light 'li_red'. (DELAY 1.2)
		Enable light 'li_red'. (DELAY 1.3)
		Disable light 'li_red'. (DELAY 1.5)
		Enable light 'li_red'. (DELAY 1.6)
		Disable light 'li_red'. (DELAY 1.8)
		Enable light 'li_red'. (DELAY 1.9)
		Disable light 'li_red'. (DELAY 2.1)
		Enable light 'li_red'. (DELAY 2.2)
		Disable light 'li_red'. (DELAY 2.4)
		Enable light 'li_red'. (DELAY 2.5)
		Disable light 'li_red'. (DELAY 2.7)
		Enable light 'li_red'. (DELAY 2.8)
		Disable light 'li_red'. (DELAY 3)
		Enable light 'li_red'. (DELAY 3.1)
		Disable light 'li_red'. (DELAY 3.3)
		Enable light 'li_red'. (DELAY 3.4)
		Disable light 'li_red'. (DELAY 3.6)
		Enable light 'li_red'. (DELAY 3.7)
		Disable light 'li_red'. (DELAY 3.9)
		Enable light 'li_red'. (DELAY 4)
		Disable light 'li_red'. (DELAY 4.2)
		Enable light 'li_red'. (DELAY 4.3)
		Disable light 'li_red'. (DELAY 4.5)
		Enable light 'li_red'. (DELAY 4.6)
		Disable light 'li_red'. (DELAY 4.8)
		Enable light 'li_red'. (DELAY 4.9)
		Disable light 'li_red'. (DELAY 5)
	sequence 'trigger_expolsive':
		Disable interaction.
		Enable body 'static_body'.
		Enable object 'g_c4'.
		Disable object 'g_c4_glow'.
		Run sequence 'flicker_light'.
		Disable object 'g_c4'. (DELAY 5)
		Disable body 'static_body'. (DELAY 5)
		Play audio 'window_c4_plant' at 'sound'.
	sequence 'interact':
		Disable interaction.
		Enable body 'static_body'.
		Enable object 'g_c4'.
		Disable object 'g_c4_glow'.
