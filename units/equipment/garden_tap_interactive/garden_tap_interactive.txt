unit:
	sequence 'enable_interaction':
		Enable interaction.
		Run sequence 'mtr_glow'.
	sequence 'disable_interaction':
		Disable interaction.
		Run sequence 'mtr_normal'.
	sequence 'mtr_normal':
		material_config 'units/equipment/garden_tap_interactive/garden_tap_interactive'.
	sequence 'mtr_glow':
		material_config 'units/equipment/garden_tap_interactive/garden_tap_interactive_contour'.
	sequence 'hide':
		Disable interaction.
		Disable object 'g_pipe'.
		Disable object 'g_hose_on_ground'.
	sequence 'show':
		Disable interaction.
		Enable object 'g_pipe'.
		Enable object 'g_hose_on_ground'.
	sequence 'attach_hose':
		Enable object 'g_hose'.
		Disable object 'g_hose_on_ground'.
	sequence 'interact':
		Run sequence 'disable_interaction'.
