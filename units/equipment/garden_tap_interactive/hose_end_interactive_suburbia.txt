unit:
	sequence 'hide':
		Disable interaction.
		Hide graphic_group 'no_lod_group'.
		Hide graphic_group 'lod_group'.
	sequence 'show':
		Enable interaction.
		Hide graphic_group 'no_lod_group'.
		Show graphic_group 'lod_group'.
	sequence 'show_no_interact':
		Disable interaction.
		Hide graphic_group 'no_lod_group'.
		Show graphic_group 'lod_group'.
	sequence 'show_end_cap':
		Disable interaction.
		Show graphic_group 'no_lod_group'.
		Hide graphic_group 'lod_group'.
	sequence 'interact':
		Disable interaction.
		Show graphic_group 'no_lod_group'.
		Hide graphic_group 'lod_group'.
