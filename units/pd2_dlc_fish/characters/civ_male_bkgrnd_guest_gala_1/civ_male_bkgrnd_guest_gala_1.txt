unit:
	sequence 'anim_dancing':
		Run sequence 'int_seq_random_'..pick('dance1','dance2','dance3','dance4','dance5','dance6').
	sequence 'anim_cheering':
		Run sequence 'int_seq_random_'..pick('cheer1','cheer2','cheer3','cheer4','cheer5','cheer6').
	sequence 'anim_clapping':
		Run sequence 'int_seq_random_'..pick('clap1','clap2','clap3','clap4','clap5','clap6').
	sequence 'anim_idle':
		Run sequence 'int_seq_random_'..pick('idle1','idle2','idle3','idle4','idle5','idle6').
	sequence 'anim_drink_idle':
		Run sequence 'int_seq_random_'..pick('drink_idle1','drink_idle2','drink_idle3','drink_idle4','drink_idle5','drink_idle6').
	sequence 'anim_talk_idle':
		Run sequence 'int_seq_random_'..pick('talk_idle1','talk_idle2','talk_idle3').
	sequence 'anim_clapping_2':
		Run sequence 'int_seq_random_'..pick('hand_clap_idle1','hand_clap_idle2','hand_clap_idle3').
	sequence 'anim_panic':
		animation_redirect 'panic_male'.
	sequence 'anim_cm_dj_arm_up':
		animation_redirect 'cm_dj_arm_up'.
	sequence 'anim_cm_dj_celebrate':
		animation_redirect 'cm_dj_celebrate'.
	sequence 'anim_cm_dj_mixing':
		animation_redirect 'cm_dj_mixing'.
	sequence 'anim_cm_dj_crouch':
		animation_redirect 'cm_dj_crouch'.
	sequence 'anim_cm_talk_slow_gestures':
		animation_redirect 'cm_talk_slow_gestures'.
	sequence 'anim_cm_talk_medium_gestures':
		animation_redirect 'cm_talk_medium_gestures'.
	sequence 'anim_cm_talk_strong_gestures':
		animation_redirect 'cm_talk_strong_gestures'.
	sequence 'anim_cm_dance_var1':
		animation_redirect 'cm_dance_var1'.
	sequence 'anim_cm_dance_var2':
		animation_redirect 'cm_dance_var2'.
	sequence 'anim_cm_dance_var3':
		animation_redirect 'cm_dance_var3'.
	sequence 'anim_cm_cheer_var1':
		animation_redirect 'cm_cheer_var1'.
	sequence 'anim_cm_cheer_var2':
		animation_redirect 'cm_cheer_var2'.
	sequence 'anim_cm_cheer_var3':
		animation_redirect 'cm_cheer_var3'.
	sequence 'anim_cm_hand_clap_var1':
		animation_redirect 'cm_hand_clap1'.
	sequence 'anim_cm_hand_clap_var2':
		animation_redirect 'cm_hand_clap2'.
	sequence 'anim_cm_hand_clap_var3':
		animation_redirect 'cm_hand_clap3'.
	sequence 'anim_cm_hand_clap_idle_var1':
		animation_redirect 'cm_hand_clap_idle_var1'.
	sequence 'anim_cm_hand_clap_idle_var2':
		animation_redirect 'cm_hand_clap_idle_var2'.
	sequence 'anim_cm_hand_clap_idle_var3':
		animation_redirect 'cm_hand_clap_idle_var3'.
	sequence 'anim_cm_idle_var1':
		animation_redirect 'cm_idle_1'.
	sequence 'anim_cm_idle_var2':
		animation_redirect 'cm_idle_2'.
	sequence 'anim_cm_idle_var3':
		animation_redirect 'cm_idle_3'.
	sequence 'anim_cm_drink_idle_var1':
		animation_redirect 'cm_drink_idle1_var1'.
	sequence 'anim_cm_drink_idle_var2':
		animation_redirect 'cm_drink_idle1_var2'.
	sequence 'anim_cm_drink_idle_var3':
		animation_redirect 'cm_drink_idle2_var1'.
	sequence 'anim_cm_drink_idle_var4':
		animation_redirect 'cm_drink_idle2_var2'.
	sequence 'anim_cm_drink_idle_var5':
		animation_redirect 'cm_drink_idle3_var1'.
	sequence 'anim_cm_drink_idle_var6':
		animation_redirect 'cm_drink_idle3_var2'.
	sequence 'int_seq_random_dance1':
		Run sequence 'anim_cm_dance_'..pick('var1','var2','var3','var1','var2','var3').
	sequence 'int_seq_random_dance2':
		Run sequence 'anim_cm_dance_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 0.5)
	sequence 'int_seq_random_dance3':
		Run sequence 'anim_cm_dance_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 1)
	sequence 'int_seq_random_dance4':
		Run sequence 'anim_cm_dance_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 1.5)
	sequence 'int_seq_random_dance5':
		Run sequence 'anim_cm_dance_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 2)
	sequence 'int_seq_random_dance6':
		Run sequence 'anim_cm_dance_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 2.5)
	sequence 'int_seq_random_cheer1':
		Run sequence 'anim_cm_cheer_'..pick('var1','var2','var3','var1','var2','var3').
	sequence 'int_seq_random_cheer2':
		Run sequence 'anim_cm_cheer_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 0.5)
	sequence 'int_seq_random_cheer3':
		Run sequence 'anim_cm_cheer_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 1)
	sequence 'int_seq_random_cheer4':
		Run sequence 'anim_cm_cheer_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 1.5)
	sequence 'int_seq_random_cheer5':
		Run sequence 'anim_cm_cheer_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 2)
	sequence 'int_seq_random_cheer6':
		Run sequence 'anim_cm_cheer_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 2.5)
	sequence 'int_seq_random_clap1':
		Run sequence 'anim_cm_hand_clap_'..pick('var1','var2','var3','var1','var2','var3').
	sequence 'int_seq_random_clap2':
		Run sequence 'anim_cm_hand_clap_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 0.5)
	sequence 'int_seq_random_clap3':
		Run sequence 'anim_cm_hand_clap_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 1)
	sequence 'int_seq_random_clap4':
		Run sequence 'anim_cm_hand_clap_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 1.5)
	sequence 'int_seq_random_clap5':
		Run sequence 'anim_cm_hand_clap_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 2)
	sequence 'int_seq_random_clap6':
		Run sequence 'anim_cm_hand_clap_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 2.5)
	sequence 'int_seq_random_idle1':
		Run sequence 'anim_cm_idle_'..pick('var1','var2','var3','var1','var2','var3').
	sequence 'int_seq_random_idle2':
		Run sequence 'anim_cm_idle_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 0.5)
	sequence 'int_seq_random_idle3':
		Run sequence 'anim_cm_idle_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 1)
	sequence 'int_seq_random_idle4':
		Run sequence 'anim_cm_idle_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 1.5)
	sequence 'int_seq_random_idle5':
		Run sequence 'anim_cm_idle_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 2)
	sequence 'int_seq_random_idle6':
		Run sequence 'anim_cm_idle_'..pick('var1','var2','var3','var1','var2','var3'). (DELAY 2.5)
	sequence 'int_seq_random_drink_idle1':
		Run sequence 'anim_cm_drink_idle_'..pick('var1','var2','var3','var4','var5','var6').
	sequence 'int_seq_random_drink_idle2':
		Run sequence 'anim_cm_drink_idle_'..pick('var1','var2','var3','var4','var5','var6'). (DELAY 0.5)
	sequence 'int_seq_random_drink_idle3':
		Run sequence 'anim_cm_drink_idle_'..pick('var1','var2','var3','var4','var5','var6'). (DELAY 1)
	sequence 'int_seq_random_drink_idle4':
		Run sequence 'anim_cm_drink_idle_'..pick('var1','var2','var3','var4','var5','var6'). (DELAY 1.5)
	sequence 'int_seq_random_drink_idle5':
		Run sequence 'anim_cm_drink_idle_'..pick('var1','var2','var3','var4','var5','var6'). (DELAY 2)
	sequence 'int_seq_random_drink_idle6':
		Run sequence 'anim_cm_drink_idle_'..pick('var1','var2','var3','var4','var5','var6'). (DELAY 2.5)
	sequence 'int_seq_random_talk_idle1':
		Run sequence 'anim_cm_talk_'..pick('slow_gestures','medium_gestures','strong_gestures').
	sequence 'int_seq_random_talk_idle2':
		Run sequence 'anim_cm_talk_'..pick('slow_gestures','medium_gestures','strong_gestures'). (DELAY 0.5)
	sequence 'int_seq_random_talk_idle3':
		Run sequence 'anim_cm_talk_'..pick('slow_gestures','medium_gestures','strong_gestures'). (DELAY 1)
	sequence 'int_seq_random_hand_clap_idle1':
		Run sequence 'anim_cm_hand_clap_idle_'..pick('var1','var2','var3').
	sequence 'int_seq_random_hand_clap_idle2':
		Run sequence 'anim_cm_hand_clap_idle_'..pick('var1','var2','var3'). (DELAY 0.5)
	sequence 'int_seq_random_hand_clap_idle3':
		Run sequence 'anim_cm_hand_clap_idle_'..pick('var1','var2','var3'). (DELAY 1)
