unit:
	sequence 'interact':
		Disable object 'g_login_screen'.
		Disable object 'g_hack_screen'.
	sequence 'state_password_interact':
		Enable interaction.
		Enable object 'g_login_screen'.
		Disable object 'g_hack_screen'.
		Call function: timer_gui.set_visible(False)
	sequence 'state_hack_interact':
		Enable interaction.
		Call function: interaction.set_tweak_data('hold_hack_server_room')
		Disable object 'g_login_screen'.
		Disable object 'g_hack_screen'.
	sequence 'state_show_hack_screen':
		Disable object 'g_login_screen'.
		Enable object 'g_hack_screen'.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'state_start_unlocking':
		Call function: timer_gui.set_visible(True)
		Call function: timer_gui.start('20')
	sequence 'activate':
		Call function: timer_gui.set_visible(False)
	sequence 'deactivate':
		Call function: timer_gui.set_visible(False)
	sequence 'power_off':
		Call function: timer_gui.set_powered(False)
	sequence 'power_on':
		Call function: timer_gui.set_powered(True)
		Call function: timer_gui.set_visible(True)
	sequence 'set_jammed':
		Call function: timer_gui.set_jammed(True)
	sequence 'set_unjammed':
		Call function: timer_gui.set_jammed(False)
	sequence 'jammed_trigger'.
	sequence 'timer_done'.
