unit:
	sequence 'anim_open':
		Enable animation_group 'anim_hatch':
			from 0/30
			to 30/30
		Play audio 'spa_elevator_hatch_open' at 'rp_lxy_int_election_elevator_exterior'.
	sequence 'anim_close':
		Enable animation_group 'anim_hatch':
			from 30/30
			speed -1
			to 0/30
	sequence 'interact':
		Run sequence 'anim_open'.
	sequence 'enable_interaction':
		Enable interaction.
		Show graphic_group 'outline_grp'.
	sequence 'disable_interaction':
		Disable interaction.
		Hide graphic_group 'outline_grp'.
