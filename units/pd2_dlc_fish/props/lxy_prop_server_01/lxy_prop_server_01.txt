unit:
	sequence 'anim_open':
		Enable animation_group 'anim_open':
			from 0/30
			to 30/30
		Hide graphic_group 'cool'.
		Show graphic_group 'over_warmed'.
		Enable effect_spawner 'steam'.
		Play audio 'fish_server_door_small_open' at 'snd'.
	sequence 'anim_close':
		Enable animation_group 'anim_open':
			from 30/30
			speed -1
			to 0/30
		Show graphic_group 'cool'.
		Hide graphic_group 'over_warmed'.
