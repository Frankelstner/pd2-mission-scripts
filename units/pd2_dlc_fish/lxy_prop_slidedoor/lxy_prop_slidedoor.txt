unit:
	sequence 'anim_open':
		Enable animation_group 'anim_door':
			from 0/25
			to 25/25
		Run sequence 'done_opened'. (DELAY 25/30)
		Hide graphic_group 'icons'.
		Play audio 'bar_sliding_door_open_finish' at 'interact'.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
	sequence 'anim_close':
		Enable animation_group 'anim_door':
			from 25/25
			speed -1
			to 0/25
	sequence 'state_door_open':
		Disable interaction.
		Hide graphic_group 'icons'.
		Enable animation_group 'anim_door':
			from 25/25
			to 25/25
	sequence 'state_door_closed':
		Enable animation_group 'anim_door':
			from 0/25
			to 0/25
	sequence 'enable_interaction':
		Enable interaction.
		Show graphic_group 'icons'.
	sequence 'disable_interaction':
		Disable interaction.
		Hide graphic_group 'icons'.
	sequence 'interact':
		Run sequence 'anim_open'.
	sequence 'state_door_hide':
		Run sequence 'disable_interaction'.
		Enable decal_mesh 'dm_metal_01'.
		Enable decal_mesh 'dm_metal_02'.
		Enable decal_mesh 'dm_wood_01'.
		Enable decal_mesh 'dm_wood_02'.
		Enable object 'g_door_left'.
		Enable object 'g_door_right'.
		Enable object 's_door_02'.
		Enable object 's_door_01'.
		Enable body 'body_collision_01'.
		Enable body 'body_collision_02'.
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
	sequence 'state_door_show':
		Run sequence 'enable_interaction'.
		Enable decal_mesh 'dm_metal_01'.
		Enable decal_mesh 'dm_metal_02'.
		Enable decal_mesh 'dm_wood_01'.
		Enable decal_mesh 'dm_wood_02'.
		Enable object 'g_door_left'.
		Enable object 'g_door_right'.
		Enable object 's_door_02'.
		Enable object 's_door_01'.
		Enable body 'body_collision_01'.
		Enable body 'body_collision_02'.
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
	body 'body_hitbox_door_handle_in'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'int_seq_bullet_hit_out'.
	body 'body_hitbox_door_handle_out'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'int_seq_bullet_hit_in'.
	sequence 'int_seq_bullet_hit_in':
		Run sequence 'anim_open'.
		Run sequence 'disable_interaction'.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Disable object 'g_door_left'.
		Enable object 'g_door_left_broken'.
		Disable object 'g_door_right'.
		Enable object 'g_door_right_broken'.
		Disable decal_mesh 'dm_metal_01'.
		Disable decal_mesh 'dm_metal_02'.
		Disable decal_mesh 'dm_wood_01'.
		Disable decal_mesh 'dm_wood_02'.
		Enable decal_mesh 'dm_wood_broken_01'.
		Enable decal_mesh 'dm_wood_broken_02'.
	sequence 'int_seq_bullet_hit_out':
		Run sequence 'anim_open'.
		Run sequence 'disable_interaction'.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Enable object 'g_door_left_broken'.
		Disable object 'g_door_left'.
		Enable object 'g_door_right_broken'.
		Disable object 'g_door_right'.
		Disable decal_mesh 'dm_metal_01'.
		Disable decal_mesh 'dm_metal_02'.
		Disable decal_mesh 'dm_wood_01'.
		Disable decal_mesh 'dm_wood_02'.
		Enable decal_mesh 'dm_wood_broken_01'.
		Enable decal_mesh 'dm_wood_broken_02'.
	sequence 'done_opened'.
