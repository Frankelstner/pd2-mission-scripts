unit:
	sequence 'startup':
		EXECUTE ON STARTUP
		Play audio 'wp_rpg_rocket_flyby' at 'snd'.
		startup True
	sequence 'explode':
		Call function: base.bullet_hit()
	body 'static_body'
		Upon receiving 1 bullet hit, execute:
			Run sequence 'explode'.
