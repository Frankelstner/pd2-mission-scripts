unit:
	state_01 = 0
	state_02 = 0
	state_03 = 0
	sequence 'make_dynamic_01':
		TRIGGER TIMES 1
		Enable body 'body_collision_01'.
		Disable body 'body_collision_hitbox_01'.
		Play audio 'statue_break_small' at 'e_pos_01'.
		effect 'effects/payday2/particles/destruction/dest_statue_bust':
			parent object( 'e_pos_01' )
			position object_pos('e_pos_01')
			rotation object_rot('e_pos_01')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('e_pos_01') ),30,10).
	sequence 'make_dynamic_02':
		TRIGGER TIMES 1
		Enable body 'body_collision_02'.
		Disable body 'body_collision_hitbox_02'.
		Play audio 'statue_break_small' at 'e_pos_03'.
		effect 'effects/payday2/particles/destruction/dest_statue_bust':
			parent object( 'e_pos_03' )
			position object_pos('e_pos_03')
			rotation object_rot('e_pos_03')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('e_pos_03') ),30,10).
	sequence 'make_dynamic_03':
		TRIGGER TIMES 1
		state_03 = 1
		Enable body 'body_collision_03'.
		Disable body 'body_collision_hitbox_03'.
		Play audio 'statue_break_medium' at 'e_pos_02'.
		effect 'effects/payday2/particles/destruction/dest_statue_bust':
			parent object( 'e_pos_02' )
			position object_pos('e_pos_02')
			rotation object_rot('e_pos_02')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('e_pos_04') ),30,10).
	sequence 'make_dynamic_04':
		TRIGGER TIMES 1
		Enable body 'body_collision_04'.
		Disable body 'body_collision_hitbox_04'.
		state_02 = 1
		state_01 = 0
		Play audio 'statue_break_small' at 'e_pos_04'.
		effect 'effects/payday2/particles/destruction/dest_statue_bust':
			parent object( 'e_pos_04' )
			position object_pos('e_pos_04')
			rotation object_rot('e_pos_04')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('e_pos_04') ),30,10).
	sequence 'activate_state_01':
		Disable body 'body_collision_hitbox_04'.
		Disable body 'body_collision_hitbox_03'.
		Disable body 'body_collision_hitbox_02'.
		Disable body 'body_collision_hitbox_01'.
		Enable body 'body_state_1_dynamic'.
		Play audio 'statue_break_large' at 'e_pos_04'.
	sequence 'activate_state_02':
		state_03 = 1
		state_01 = 0
		Disable body 'body_collision_hitbox_03'.
		Disable body 'body_collision_hitbox_04'.
		Enable body 'body_state_2_dynamic'.
		Disable body 'body_state_1_dynamic'.
		Play audio 'statue_break_medium' at 'e_pos_04'.
	sequence 'activate_state_03':
		Disable body 'body_collision_hitbox_01'.
		Disable body 'body_collision_hitbox_02'.
		Enable body 'body_state_3_dynamic'.
		Play audio 'statue_break_large' at 'e_pos_04'.
	sequence 'disable_state_03':
		Disable body 'body_state_3_dynamic'.
	sequence 'disable_state_02':
		Disable body 'body_state_2_dynamic'.
	sequence 'disable_state_01':
		Disable body 'body_state_1_dynamic'.
	sequence 'disable_explosive':
		Disable body 'body_explosives'.
	body 'body_explosives'
		Upon receiving 10 explosion damage, execute:
			Run sequence 'make_dynamic_01'.
			Run sequence 'make_dynamic_02'.
			Run sequence 'make_dynamic_03'.
			Run sequence 'make_dynamic_04'.
			Run sequence 'disable_explosive'.
	body 'body_collision_hitbox_01'
		Upon receiving 2 bullet hits or 30 melee damage, execute:
			Run sequence 'activate_state_01'.
			If state_03 == 1: Run sequence 'disable_state_01'.
			If state_03 == 1: Run sequence 'activate_state_03'.
	body 'body_collision_hitbox_02'
		Upon receiving 2 bullet hits or 30 melee damage, execute:
			Run sequence 'make_dynamic_02'.
	body 'body_collision_hitbox_03'
		Upon receiving 2 bullet hits or 30 melee damage, execute:
			If state_02 == 0: Run sequence 'activate_state_02'.
			If state_02 == 1: Run sequence 'make_dynamic_03'.
	body 'body_collision_hitbox_04'
		Upon receiving 2 bullet hits or 30 melee damage, execute:
			Run sequence 'make_dynamic_04'.
	body 'body_state_1_dynamic'
		Upon receiving 2 bullet hits or 1 collision damage or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'disable_state_01'.
			Run sequence 'make_dynamic_04'.
			Run sequence 'make_dynamic_03'.
			Run sequence 'make_dynamic_02'.
			Run sequence 'make_dynamic_01'.
	body 'body_state_2_dynamic'
		Upon receiving 2 bullet hits or 1 collision damage or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'disable_state_02'.
			Run sequence 'make_dynamic_03'.
			Run sequence 'make_dynamic_04'.
	body 'body_state_3_dynamic'
		Upon receiving 2 bullet hits or 1 collision damage or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'disable_state_03'.
			Run sequence 'make_dynamic_01'.
			Run sequence 'make_dynamic_02'.
