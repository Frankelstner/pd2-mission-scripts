unit:
	sequence 'anim_open':
		Enable animation_group 'anim_door':
			from 0/30
			to 30/30
		Play audio 'generic_door_wood_open' at 'a_door_01'.
		Play audio 'generic_door_wood_open' at 'a_door_02'.
	sequence 'anim_close':
		Enable animation_group 'anim_door':
			from 30/30
			speed -1
			to 0/30
