unit:
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break'.
	sequence 'int_seq_break':
		TRIGGER TIMES 1
		Disable body 'body_static'.
		Enable body 'body_collision_01'.
		Enable body 'body_collision_02'.
		Enable body 'body_collision_03'.
		Enable body 'body_collision_04'.
		Enable body 'body_collision_05'.
		Enable body 'body_collision_06'.
		Enable body 'body_collision_07'.
		Disable decal_mesh 'dm_concrete'.
		Disable object 's_s'.
		Hide graphic_group 'gfx_grp'.
		Show graphic_group 'grp_dmg_01'.
		Show graphic_group 'grp_dmg_02'.
		Show graphic_group 'grp_dmg_03'.
		Show graphic_group 'grp_dmg_04'.
		Show graphic_group 'grp_dmg_05'.
		Play audio 'vase_shatter_large' at 'e_pos'.
