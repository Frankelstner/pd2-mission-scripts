unit:
	sequence 'destroy_lamp':
		TRIGGER TIMES 1
		effect 'effects/particles/dest/bottle_dest':
			parent 'g_glass'
		Play audio 'pot_large_shatter' at 'g_glass'.
		Disable object 'g_glass'.
		Disable decal_mesh 'dm_glass_breakable'.
		Disable effect_spawner 'fire'.
		Disable effect_spawner 'flare'.
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_lamp'.
	sequence 'light_on':
		Enable animation_group 'anim_flickering_light'. (DELAY rand()*2)
		Enable light 'lo_omni'.
		Enable effect_spawner 'fire'.
		Enable effect_spawner 'flare'.
	sequence 'light_off':
		Disable animation_group 'anim_flickering_light':
			loop True
		Disable light 'lo_omni'.
		Disable effect_spawner 'fire'.
		Disable effect_spawner 'flare'.
