unit:
	sequence 'break':
		Disable body 'body_static'.
		Disable object 'g_lod0'.
		Disable object 'g_lod1'.
		Enable body 'body_collision_01'.
		Enable body 'body_collision_02'.
		Enable body 'body_collision_03'.
		Enable body 'body_collision_04'.
		Enable body 'body_collision_05'.
		Enable object 'g_dmg_lod0'.
		Enable object 's_s'.
		Enable decal_mesh 'dm_metal'.
		Enable decal_mesh 'dm_glass_breakable'.
		effect 'effects/particles/dest/ceramic_cup_dest':
			position dest_unit_pos()
		Play audio 'pot_large_shatter' at 'e_pos'.
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break'.
