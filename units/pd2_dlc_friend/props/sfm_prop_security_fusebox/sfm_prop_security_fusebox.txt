unit:
	open = 0
	sequence 'state_interaction_enabled':
		Enable interaction.
		If open == 0: Show graphic_group 'icon'.
		If open == 0: Enable body 'body_saw'.
	sequence 'state_interaction_disable':
		Disable interaction.
		Hide graphic_group 'icon'.
		Disable body 'body_saw'.
	sequence 'anim_door_open':
		Disable interaction.
		Disable body 'body_door'.
		Enable animation_group 'anim_open':
			from 0/30
			to 21/30
		Hide graphic_group 'icon'.
		Enable object 'g_outline'.
		Call function: interaction.set_tweak_data('rewire_friend_fuse_box')
		open = 1
	sequence 'anim_door_close':
		Enable interaction.
		Enable body 'body_door'.
		Show graphic_group 'icon'.
		Enable animation_group 'anim_open':
			from 21/30
			speed -1
			to 0/30
		Call function: interaction.set_tweak_data('pick_lock_x_axis')
		open = 0
	sequence 'hack':
		Enable interaction.
		Enable body 'body_fusebox'.
		Disable object 'g_cables'.
		Enable object 'g_cables_clamped'.
		Disable object 'g_outline'.
		Hide graphic_group 'icon'.
	sequence 'hide':
		Disable interaction.
		Disable body 'body_fusebox'.
		Disable body 'body_door'.
		Disable object 'g_cables'.
		Disable object 'g_cables_clamped'.
		Disable object 'g_base'.
		Disable object 'g_door'.
		Disable object 's_door'.
		Disable object 's_base'.
		Hide graphic_group 'icon'.
	sequence 'show':
		Enable interaction.
		Enable body 'body_fusebox'.
		Enable body 'body_door'.
		Enable object 'g_cables'.
		Enable object 'g_cables_clamped'.
		Enable object 'g_base'.
		Enable object 'g_door'.
		Enable object 's_door'.
		Enable object 's_base'.
		Show graphic_group 'icon'.
	body 'body_saw'
		Upon receiving 30 saw damage, execute:
			If open == 1: Run sequence 'hack'.
			If open == 0: Run sequence 'anim_door_open'.
	sequence 'interact':
		If open == 1: Run sequence 'hack'.
		If open == 0: Run sequence 'anim_door_open'.
