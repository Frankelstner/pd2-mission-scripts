unit:
	sequence 'break':
		Play audio 'light_bulb_smash' at 'g_lod0'.
		Disable object 'g_lod0'.
		Show graphic_group 'damaged'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent  'e_pos' 
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break'.
