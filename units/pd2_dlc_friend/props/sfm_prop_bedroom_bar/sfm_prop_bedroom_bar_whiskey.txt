unit:
	sequence 'break':
		Enable body 'body_collision_01'.
		Enable body 'body_collision_02'.
		Enable body 'body_collision_03'.
		Disable body 'body_static'.
		Play audio 'window_small_shatter' at 'e_effect_01'.
		effect 'effects/particles/dest/brown_bottle_dest':
			parent object( 'e_effect_01' )
			position v()
		Disable object 'g_lod0'.
		Show graphic_group 'dmg_pieces'.
		Disable decal_mesh 'dm_glass_breakable'.
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break'.
