unit:
	sequence 'break':
		Play audio 'glass_prop_shatter_small' at 'rp_sfm_prop_office_glass'.
		effect 'effects/particles/dest/brown_bottle_dest':
			parent object( 'rp_sfm_prop_office_glass' )
			position v()
		Hide graphic_group 'glass'.
		Disable decal_mesh 'dm_glass'.
		Disable body 'body_static'.
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break'.
