unit:
	landing_sequence = 0
	sequence 'show_safe':
		Enable object 'g_glass'.
		Disable object 'g_magnet_glow'.
		Enable object 'g_magnet'.
		Enable object 'g_magnet_chain'.
		Enable object 'g_wire'.
		Enable object 'g_magnet'.
		Enable object 'g_magnet_chain'.
		Enable object 'g_safe'.
		Enable object 'g_handle'.
		Enable object 'g_analog'.
		Enable object 'g_door'.
		Enable object 's_magnet'.
	sequence 'hide_safe':
		Disable object 'g_glass'.
		Disable object 'g_magnet_glow'.
		Disable object 'g_safe'.
		Disable object 'g_handle'.
		Disable object 'g_analog'.
		Disable object 'g_door'.
	sequence 'hidden':
		Disable interaction.
		Disable animation_group 'ag_main_rotor'.
		Disable animation_group 'a_main_animation'.
		Disable body 'static_body'.
		Disable body 'c_safe'.
		Disable body 'main_rotor'.
		Disable body 'second_rotor'.
		Disable body 'c_magnet'.
		Disable object 'g_magnet_glow'.
		Disable object 'g_main_rotor'.
		Disable object 'g_second_rotor'.
		Disable object 'g_body_nypd'.
		Disable object 'g_interior_hull'.
		Disable object 'g_magnet'.
		Disable object 'g_magnet_chain'.
		Disable object 'g_wire'.
		Disable object 's_body'.
		Disable object 'g_glass'.
		Disable object 'g_safe'.
		Disable object 'g_handle'.
		Disable object 'g_analog'.
		Disable object 'g_door'.
		Disable object 's_magnet'.
	sequence 'show':
		Enable animation_group 'ag_main_rotor'.
		Enable object 'g_interior_hull'.
		Enable object 'g_body_nypd'.
		Enable object 'g_main_rotor'.
		Enable object 'g_second_rotor'.
		Enable body 'static_body'.
		Enable body 'c_safe'.
		Enable body 'main_rotor'.
		Enable body 'second_rotor'.
		Enable object 'g_wire'.
		Enable object 'g_magnet'.
		Enable object 'g_magnet_chain'.
		Disable object 'g_magnet_glow'.
		Disable object 'g_glass'.
		Enable object 's_body'.
		Enable object 'g_safe'.
		Enable object 'g_handle'.
		Enable object 'g_analog'.
		Enable object 'g_door'.
		Enable object 's_magnet'.
	sequence 'main_rotor':
		EXECUTE ON STARTUP
		Enable animation_group 'ag_main_rotor'.
		Disable body 'main_rotor'.
		Disable body 'second_rotor'.
		startup True
	sequence 'redi_apartment_hover':
		animation_redirect 'apartment_hover'.
	sequence 'aprt_start':
		animation_redirect 'apartment_enter'.
	sequence 'aprt_land':
		animation_redirect 'apartment_to_panicroom'.
	sequence 'aprt_attach_pnc':
		animation_redirect 'apartment_repel_to_panicroom'.
	sequence 'aprt_takeoff':
		animation_redirect 'apartment_liftoff_panicroom'.
	sequence 'aprt_drop_bag ':
		animation_redirect 'aprt_drop_bag'.
	sequence 'cne_arrive_loop ':
		animation_redirect 'cane_arrive_loop'.
		Play audio 'cane_heli_arrive' at 'body'.
	sequence 'cne_exit ':
		animation_redirect 'cane_exit'.
		Play audio 'cane_heli_leave' at 'body'.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'show_interactive':
		Enable interaction.
		Enable body 'static_body'.
		Enable body 'c_magnet'.
		Disable object 'g_magnet'.
		Enable object 'g_magnet_chain'.
		Enable object 'g_magnet_glow'.
	sequence 'interact':
		Disable interaction.
		Enable body 'static_body'.
		Enable object 'g_magnet'.
		Enable object 'g_magnet_chain'.
		Disable object 'g_magnet_glow'.
