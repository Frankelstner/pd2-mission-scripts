unit:
	sequence 'sound_packing_start':
		Play audio 'packing_box_loop' at 'snd'.
	sequence 'sound_packing_stop':
		Play audio 'packing_box_stop' at 'snd'.
