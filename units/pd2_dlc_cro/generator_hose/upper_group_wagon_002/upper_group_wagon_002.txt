unit:
	sequence 'hide':
		Disable object 'g_upper_group_wagon_002'.
	sequence 'show':
		Enable object 'g_upper_group_wagon_002'.
		Enable object 's_upper_group_wagon_002'.
	sequence 'hide_transparent':
		Disable object 'g_upper_group_wagon_002_transparent'.
	sequence 'show_transparent':
		Enable object 'g_upper_group_wagon_002_transparent'.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'interact':
		Run sequence 'hide_transparent'.
		Run sequence 'show'.
		Run sequence 'disable_interaction'.
