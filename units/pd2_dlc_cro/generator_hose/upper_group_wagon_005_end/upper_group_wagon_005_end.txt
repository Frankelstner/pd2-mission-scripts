unit:
	sequence 'hide':
		Disable object 'g_upper_group_wagon_005_end'.
		Disable object 's_upper_group_wagon_005_end'.
	sequence 'show':
		Enable object 'g_upper_group_wagon_005_end'.
		Enable object 's_upper_group_wagon_005_end'.
	sequence 'hide_transparent':
		Disable object 'g_upper_group_wagon_005_end_transparent'.
	sequence 'show_transparent':
		Enable object 'g_upper_group_wagon_005_end_transparent'.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'interact':
		Run sequence 'hide_transparent'.
		Run sequence 'show'.
		Run sequence 'disable_interaction'.
	sequence 'sound_water_start':
		Play audio 'hose_connect_pour' at 'snd'.
	sequence 'sound_water_stop':
		Play audio 'hose_connect_pour_stop' at 'snd'.
		Stop audio 'hose_connect_pour' at 'snd'.
