unit:
	sequence 'light_on':
		Enable animation_group 'anim':
			from 0
			loop True
			to 119/30
		Enable object 'g_g_on'.
		Disable object 'g_g_off'.
		Enable object 'g_light_cone'.
		Enable effect_spawner 'sea_flare':
			effect effects/payday2/particles/flares/sea_flare_big_01
			object 'es_sea_flare'
	sequence 'light_off':
		Disable animation_group 'anim':
			from 0
			loop True
			to 119/30
		Disable object 'g_g_on'.
		Enable object 'g_g_off'.
		Disable object 'g_light_cone'.
		Disable effect_spawner 'sea_flare':
			effect effects/payday2/particles/flares/sea_flare_big_01
			object 'es_sea_flare'
