unit:
	sequence 'sawed':
		Disable object 'g_icon_saw'.
	body 'saw_collider'
		Upon receiving 37.5 saw damage, execute:
			Run sequence 'sawed'.
