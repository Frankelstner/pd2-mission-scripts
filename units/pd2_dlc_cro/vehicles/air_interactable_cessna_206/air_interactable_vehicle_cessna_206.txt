unit:
	sequence 'anim_door_open_pilot':
		Enable animation_group 'ag_door_pilot':
			from 0/30
			to 15/30
	sequence 'anim_door_open_passanger':
		Enable animation_group 'ag_door_passanger_back':
			from 0/30
			to 15/30
	sequence 'start_propeller':
		Enable animation_group 'ag_propeller_start':
			from 0/30
			to 120/30
		Enable animation_group 'ag_propeller': (DELAY 120/30)
			from 0/30
			loop True
			to 30/30
	sequence 'stop_propeller':
		Disable animation_group 'ag_propeller'.
		Enable animation_group 'ag_propeller_stop':
			from 0/30
			to 150/30
	sequence 'anim_land':
		Enable animation_group 'ag_land':
			from 0/30
		Run sequence 'show'.
		Run sequence 'done_landed'. (DELAY 700/30)
		Play audio 'air_interactable_vehicle_cessna_206' at 'a_propeller'.
	sequence 'anim_drive_in_1':
		Run sequence 'start_propeller'.
		Enable animation_group 'ag_drive_in_1': (DELAY 200/30)
			from 250/30
			to 285/30
		Run sequence 'stop_propeller'. (DELAY 235/30)
		Run sequence 'done_drive_in_1'. (DELAY 235/30)
	sequence 'anim_drive_in_2':
		Run sequence 'start_propeller'.
		Enable animation_group 'ag_drive_in_2': (DELAY 200/30)
			from 250/30
			to 333/30
		Run sequence 'stop_propeller'. (DELAY 283/30)
		Run sequence 'done_drive_in_2'. (DELAY 283/30)
	sequence 'anim_drive_in_3':
		Run sequence 'start_propeller'.
		Enable animation_group 'ag_drive_in_3': (DELAY 200/30)
			from 250/30
			to 379/30
		Run sequence 'stop_propeller'. (DELAY 329/30)
		Run sequence 'done_drive_in_3'. (DELAY 329/30)
	sequence 'anim_drive_in_4':
		Run sequence 'start_propeller'.
		Enable animation_group 'ag_drive_in_4': (DELAY 200/30)
			from 250/30
			to 365/30
		Run sequence 'stop_propeller'. (DELAY 315/30)
		Run sequence 'done_drive_in_4'. (DELAY 315/30)
	sequence 'anim_drive_in_5':
		Run sequence 'start_propeller'.
		Enable animation_group 'ag_drive_in_5': (DELAY 200/30)
			from 250/30
			to 382/30
		Run sequence 'stop_propeller'. (DELAY 332/30)
		Run sequence 'done_drive_in_5'. (DELAY 332/30)
	sequence 'anim_drive_out_1':
		Run sequence 'start_propeller'.
		Enable animation_group 'ag_drive_in_1': (DELAY 200/30)
			from 285/30
			to 415/30
		Run sequence 'stop_propeller'. (DELAY 330/30)
		Run sequence 'done_drive_out_1'. (DELAY 330/30)
	sequence 'anim_drive_out_2':
		Run sequence 'start_propeller'.
		Enable animation_group 'ag_drive_in_2': (DELAY 200/30)
			from 333/30
			to 415/30
		Run sequence 'stop_propeller'. (DELAY 282/30)
		Run sequence 'done_drive_out_2'. (DELAY 282/30)
	sequence 'anim_drive_out_3':
		Run sequence 'start_propeller'.
		Enable animation_group 'ag_drive_in_3': (DELAY 200/30)
			from 379/30
			to 415/30
		Run sequence 'stop_propeller'. (DELAY 236/30)
		Run sequence 'done_drive_out_3'. (DELAY 236/30)
	sequence 'anim_drive_out_4':
		Run sequence 'start_propeller'.
		Enable animation_group 'ag_drive_in_4': (DELAY 200/30)
			from 365/30
			to 519/30
		Run sequence 'stop_propeller'. (DELAY 354/30)
		Run sequence 'done_drive_out_4'. (DELAY 354/30)
	sequence 'anim_drive_out_5':
		Run sequence 'start_propeller'.
		Enable animation_group 'ag_drive_in_5': (DELAY 200/30)
			from 382/30
			to 550/30
		Run sequence 'stop_propeller'. (DELAY 368/30)
		Run sequence 'done_drive_out_5'. (DELAY 368/30)
	sequence 'anim_takeof':
		Run sequence 'start_propeller'.
		Enable animation_group 'ag_takeof': (DELAY 200/30)
			from 550/30
		Run sequence 'stop_propeller'. (DELAY 470/30)
		Run sequence 'hide'. (DELAY 470/30)
	sequence 'hide':
		Disable body 'body_door_pilot'.
		Disable body 'body_door_passanger_front'.
		Disable body 'body_door_passanger_back'.
		Disable body 'body_cesna'.
		Disable body 'body_player_blocker'.
		Disable decal_mesh 'g_cessna'.
		Disable object 'g_cessna'.
		Disable object 'g_door_passanger_back'.
		Disable object 'g_door_passanger_front'.
		Disable object 'g_door_pilot'.
		Disable object 'g_propeller'.
		Disable object 'g_seats_back'.
		Disable object 'g_seats_front'.
		Disable object 'g_seats_middle'.
		Disable object 'g_wheel_back'.
		Disable object 'g_wheel_front'.
		Disable object 'g_wheel_front_holder'.
		Disable object 'g_window_passanger_back'.
		Disable object 'g_window_passanger_front'.
		Disable object 'g_window_pilot'.
		Disable object 'g_window_static_01'.
		Disable object 'g_window_static_02'.
		Disable object 'g_window_static_03'.
		Disable object 'g_window_static_04'.
		Disable object 'g_window_static_05'.
		Disable object 'g_window_static_06'.
		Disable object 'g_window_static_07'.
		Disable object 'g_driver'.
		Disable object 's_cessna'.
		Disable object 's_door_passanger_back'.
		Disable object 's_door_passanger_front'.
		Disable object 's_door_pilot'.
		Disable object 's_propeller'.
	sequence 'show':
		Enable body 'body_door_pilot'.
		Enable body 'body_door_passanger_front'.
		Enable body 'body_door_passanger_back'.
		Enable body 'body_cesna'.
		Enable body 'body_player_blocker'.
		Enable decal_mesh 'g_cessna'.
		Enable object 'g_cessna'.
		Enable object 'g_door_passanger_back'.
		Enable object 'g_door_passanger_front'.
		Enable object 'g_door_pilot'.
		Enable object 'g_propeller'.
		Enable object 'g_seats_back'.
		Enable object 'g_seats_front'.
		Enable object 'g_seats_middle'.
		Enable object 'g_wheel_back'.
		Enable object 'g_wheel_front'.
		Enable object 'g_wheel_front_holder'.
		Enable object 'g_window_passanger_back'.
		Enable object 'g_window_passanger_front'.
		Enable object 'g_window_pilot'.
		Enable object 'g_window_static_01'.
		Enable object 'g_window_static_02'.
		Enable object 'g_window_static_03'.
		Enable object 'g_window_static_04'.
		Enable object 'g_window_static_05'.
		Enable object 'g_window_static_06'.
		Enable object 'g_window_static_07'.
		Enable object 'g_driver'.
		Enable object 's_cessna'.
		Enable object 's_door_passanger_back'.
		Enable object 's_door_passanger_front'.
		Enable object 's_door_pilot'.
		Enable object 's_propeller'.
	sequence 'done_landed'.
	sequence 'done_drive_in_1'.
	sequence 'done_drive_in_2'.
	sequence 'done_drive_in_3'.
	sequence 'done_drive_in_4'.
	sequence 'done_drive_in_5'.
	sequence 'done_drive_out_1'.
	sequence 'done_drive_out_2'.
	sequence 'done_drive_out_3'.
	sequence 'done_drive_out_4'.
	sequence 'done_drive_out_5'.
