unit:
	sequence 'anim_police_response':
		Enable animation_group 'anim':
			from 0/30
			to 251/30
		Run sequence 'done_police_response'. (DELAY 251/30)
		Play audio 'truck_stage_03_anim_02' at 'snd'.
	sequence 'done_police_response':
		Disable body 'body_anim_blocker'.
	sequence 'anim_door_right_open':
		Enable animation_group 'ag_door_right':
			from 0/30
			to 15/30
	sequence 'anim_door_left_open':
		Enable animation_group 'ag_door_left':
			from 0/30
			to 15/30
	sequence 'anim_door_right_close':
		Enable animation_group 'ag_door_right':
			from 15/30
			speed -1
			to 0/30
	sequence 'anim_door_left_close':
		Enable animation_group 'ag_door_left':
			from 15/30
			speed -1
			to 0/30
	sequence 'anim_lid_top':
		Enable animation_group 'ag_lid_top'.
	sequence 'state_door_right_open':
		Enable animation_group 'ag_door_right':
			from 15/30
			speed 0
			to 15/30
	sequence 'state_door_left_open':
		Enable animation_group 'ag_door_left':
			from 15/30
			speed 0
			to 15/30
	sequence 'state_door_right_close':
		Enable animation_group 'ag_door_right':
			from 0/30
			speed 0
			to 0/30
	sequence 'state_door_left_close':
		Enable animation_group 'ag_door_left':
			from 0/30
			speed 0
			to 0/30
	sequence 'state_hide':
		Disable body 'body_body'.
		Disable body 'body_anim_blocker'.
		Disable body 'body_left_door'.
		Disable body 'body_right_door'.
		Disable body 'body_top_door'.
		Disable body 'body_wheel_rear'.
		Disable body 'body_wheel_right'.
		Disable body 'body_wheel_left'.
		Disable body 'blocker_mover'.
		Disable body 'blocker_mover_more'.
		Disable body 'bag_blocker'.
		Disable decal_mesh 'g_body'.
		Disable decal_mesh 'g_left_door'.
		Disable decal_mesh 'g_right_door'.
		Disable decal_mesh 'g_top_door'.
		Disable decal_mesh 'g_wheel_rear'.
		Disable decal_mesh 'g_wheel_right'.
		Disable decal_mesh 'g_wheel_left'.
		Hide graphic_group 'grp_vehicle_truck_police'.
	sequence 'state_show':
		Enable body 'body_body'.
		Enable body 'body_anim_blocker'.
		Enable body 'body_left_door'.
		Enable body 'body_right_door'.
		Enable body 'body_top_door'.
		Enable body 'body_wheel_rear'.
		Enable body 'body_wheel_right'.
		Enable body 'body_wheel_left'.
		Enable body 'blocker_mover'.
		Enable body 'blocker_mover_more'.
		Enable body 'bag_blocker'.
		Enable decal_mesh 'g_body'.
		Enable decal_mesh 'g_left_door'.
		Enable decal_mesh 'g_right_door'.
		Enable decal_mesh 'g_top_door'.
		Enable decal_mesh 'g_wheel_rear'.
		Enable decal_mesh 'g_wheel_right'.
		Enable decal_mesh 'g_wheel_left'.
		Show graphic_group 'grp_vehicle_truck_police'.
