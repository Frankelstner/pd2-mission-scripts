unit:
	sequence 'open':
		Disable object 'g_lock'.
		Disable object 'lock'.
		Disable object 'dm_lock'.
		Disable object 's_lock'.
	body 'lock'
		Upon receiving 37.5 saw damage, execute:
			Run sequence 'open'.
	sequence 'hide':
		Disable object 'g_holder'.
		Disable object 'g_lock'.
		Disable object 's_holder':
			shadow_caster False
		Disable object 's_lock':
			shadow_caster False
		Disable body 'lock'.
		Disable body 'holder'.
	sequence 'show':
		Enable object 'g_holder'.
		Enable object 'g_lock'.
		Enable object 's_holder':
			shadow_caster True
		Enable object 's_lock':
			shadow_caster True
		Enable body 'lock'.
		Enable body 'holder'.
