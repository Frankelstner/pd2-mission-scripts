unit:
	open = 0
	int = 0
	sequence 'alert':
		Cause alert with 12 m radius.
	sequence 'interaction_disable':
		Disable interaction.
	sequence 'interaction_enable':
		Enable interaction.
		If int == 1: Run sequence 'int_seq_set_var_0'.
		If int == 1: filter = 'filter_fix2'
		If int == 1: int = 2
		If open == 1: filter = 'filter_open'
		If open == 1: int = 1
	sequence 'int_seq_text_close':
		Call function: interaction.set_tweak_data('invisible_interaction_close')
	sequence 'int_seq_text_open':
		Call function: interaction.set_tweak_data('invisible_interaction_open')
	sequence 'int_seq_set_var_0':
		If open == 1: filter = 'filter_open'
		If open == 1: open = 0
	sequence 'int_seq_set_var_1':
		If open == 0: filter = 'filter_close'
		If open == 0: open = 1
	sequence 'interact':
		Play audio 'pick_up_crowbar' at 'g_ball'.
	sequence 'load'.
