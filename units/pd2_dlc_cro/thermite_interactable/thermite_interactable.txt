unit:
	sequence 'enable':
		Enable interaction.
		Enable object 'g_icon'.
	sequence 'disable':
		Disable interaction.
		Disable object 'g_icon'.
	sequence 'state_interaction_thermite':
		Call function: interaction.set_tweak_data('thermite')
	sequence 'state_interaction_thermite_paste':
		Call function: interaction.set_tweak_data('apply_thermite_paste')
	sequence 'interact':
		Run sequence 'disable'.
