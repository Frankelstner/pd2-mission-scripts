unit:
	sequence 'action_thermite_burn_top':
		effect 'effects/payday2/particles/fire/thermite_long_burn':
			parent object( 'e_top' )
			position v()
		Enable animation_group 'hole_top': (DELAY 16)
			from 2/30
			to 2/30
		Run sequence 'done_thermite_burn'. (DELAY 90)
	sequence 'action_thermite_burn_left':
		effect 'effects/payday2/particles/fire/thermite_long_burn':
			parent object( 'e_left' )
			position v()
		Enable animation_group 'hole_left': (DELAY 16)
			from 2/30
			to 2/30
		Run sequence 'done_thermite_burn'. (DELAY 90)
	sequence 'action_thermite_burn_right':
		effect 'effects/payday2/particles/fire/thermite_long_burn':
			parent object( 'e_right' )
			position v()
		Enable animation_group 'hole_right': (DELAY 16)
			from 2/30
			to 2/30
		Run sequence 'done_thermite_burn'. (DELAY 86)
	sequence 'done_thermite_burn'.
	sequence 'action_open_vault':
		TRIGGER TIMES 1
		Disable object 'g_hatch_a'.
		Disable object 'g_hatch_b'.
		Disable body 'body_hatch_a'.
		Disable body 'body_hatch_b'.
		Disable object 's_hatch_a'.
		Disable object 's_hatch_b'.
		spawn_unit 'units/pd2_dlc_cro/eus_vehicle_train_cargo_carriage_vault/train_hatch':
			position object_pos('push_a')
			rotation object_rot('push_a')
		spawn_unit 'units/pd2_dlc_cro/eus_vehicle_train_cargo_carriage_vault/train_hatch':
			position object_pos('push_b')
			rotation object_rot('push_b')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('spawn_a') ),10,2).
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('spawn_b') ),10,2).
		effect 'effects/particles/explosions/suburbia_safe_explosion':
			parent object( 'e_water_front_l' )
			position v()
		effect 'effects/particles/explosions/suburbia_safe_explosion':
			parent object( 'e_water_front_r' )
			position v()
		effect 'effects/particles/explosions/suburbia_safe_explosion':
			parent object( 'e_water_rear_l' )
			position v()
		effect 'effects/particles/explosions/suburbia_safe_explosion':
			parent object( 'e_water_rear_r' )
			position v()
		Disable body 'body_block'.
		Disable object 'g_tra_int'.
		Disable object 'g_l_vault_door'.
		Disable object 'g_r_vault_door'.
		Disable object 's_tra_int_vault'.
		Enable object 'g_tra_int_expl'.
		Enable object 'g_l_train_explode'.
		Enable object 'g_r_train_explode'.
	sequence 'action_open_vault_left':
		TRIGGER TIMES 1
		Disable object 'g_hatch_a'.
		Disable object 'g_hatch_b'.
		Disable body 'body_hatch_a'.
		Disable body 'body_hatch_b'.
		Disable object 's_hatch_a'.
		Disable object 's_hatch_b'.
		spawn_unit 'units/pd2_dlc_cro/eus_vehicle_train_cargo_carriage_vault/train_hatch':
			position object_pos('push_a')
			rotation object_rot('push_a')
		spawn_unit 'units/pd2_dlc_cro/eus_vehicle_train_cargo_carriage_vault/train_hatch':
			position object_pos('push_b')
			rotation object_rot('push_b')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('spawn_a') ),10,2).
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('spawn_b') ),10,2).
		effect 'effects/particles/explosions/suburbia_safe_explosion':
			parent object( 'e_water_front_l' )
			position v()
		effect 'effects/particles/explosions/suburbia_safe_explosion':
			parent object( 'e_water_rear_l' )
			position v()
		Disable body 'body_block'.
		Disable object 'g_tra_int'.
		Enable object 'g_l_train_explode'.
		Disable object 'g_l_vault_door'.
		Disable object 's_tra_int_vault'.
		Enable object 'g_tra_int_expl'.
		Enable body 'body_r_vault_door'.
	sequence 'action_open_vault_right':
		TRIGGER TIMES 1
		Disable object 'g_hatch_a'.
		Disable object 'g_hatch_b'.
		Disable body 'body_hatch_a'.
		Disable body 'body_hatch_b'.
		Disable object 's_hatch_a'.
		Disable object 's_hatch_b'.
		spawn_unit 'units/pd2_dlc_cro/eus_vehicle_train_cargo_carriage_vault/train_hatch':
			position object_pos('push_a')
			rotation object_rot('push_a')
		spawn_unit 'units/pd2_dlc_cro/eus_vehicle_train_cargo_carriage_vault/train_hatch':
			position object_pos('push_b')
			rotation object_rot('push_b')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('spawn_a') ),10,2).
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('spawn_b') ),10,2).
		effect 'effects/particles/explosions/suburbia_safe_explosion':
			parent object( 'e_water_front_r' )
			position v()
		effect 'effects/particles/explosions/suburbia_safe_explosion':
			parent object( 'e_water_rear_r' )
			position v()
		Disable body 'body_block'.
		Disable object 'g_tra_int'.
		Enable object 'g_r_train_explode'.
		Disable object 'g_r_vault_door'.
		Disable object 's_tra_int_vault'.
		Enable object 'g_tra_int_expl'.
		Enable body 'body_l_vault_door'.
	sequence 'enable_vault_collision':
		Disable body 'body_hide'.
		Enable body 'body_vault'.
	sequence 'state_vis_hide_vault':
		Disable body 'body_block'.
		Enable object 'g_tra_int'.
		Disable object 's_tra_int_vault'.
		Disable object 'g_tra_int_vault'.
	sequence 'state_vis_show_vault':
		Run sequence 'enable_vault_collision'.
		Enable body 'body_block'.
		Disable object 'g_tra_int'.
		Enable object 's_tra_int_vault'.
		Enable object 'g_r_vault_door'.
		Enable object 'g_l_vault_door'.
	sequence 'open_hatch_a':
		Enable animation_group 'hatch_a':
			from 0/20
			to 30/30
	sequence 'open_hatch_b':
		Enable animation_group 'hatch_b':
			from 0/20
			to 30/30
	sequence 'close_hatch_a':
		Enable animation_group 'hatch_a':
			from 20/30
			speed -1
			to 0/30
	sequence 'close_hatch_b':
		Enable animation_group 'hatch_b':
			from 20/30
			speed -1
			to 0/30
	sequence 'swobj_breach_hatch_a':
		TRIGGER TIMES 1
		Disable body 'body_hatch_a'.
		Disable object 'g_hatch_a'.
		Disable object 's_hatch_a'.
		effect 'effects/particles/explosions/explosion_grenade':
			parent object( 'spawn_a' )
			position v()
		Play audio 'c4_explode_metal' at 'spawn_a'.
	sequence 'swobj_breach_hatch_b':
		TRIGGER TIMES 1
		Disable body 'body_hatch_b'.
		Disable object 'g_hatch_b'.
		Disable object 's_hatch_b'.
		effect 'effects/particles/explosions/explosion_grenade':
			parent object( 'spawn_b' )
			position v()
		Play audio 'c4_explode_metal' at 'spawn_b'.
