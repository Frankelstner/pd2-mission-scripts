unit:
	spawn_bool = 1
	sequence 'var_set_color_white':
		material_config 'units/payday2/props/gen_prop_container_murky/gen_prop_container_murky_2'.
		Enable object 'g_1'.
		Disable object 'g_2'.
		Disable object 'g_3'.
	sequence 'var_set_color_red':
		material_config 'units/payday2/props/gen_prop_container_murky/gen_prop_container_murky_1'.
		Disable object 'g_1'.
		Enable object 'g_2'.
		Disable object 'g_3'.
	sequence 'var_set_color_green':
		material_config 'units/payday2/props/gen_prop_container_murky/gen_prop_container_murky_2'.
		Disable object 'g_1'.
		Enable object 'g_2'.
		Disable object 'g_3'.
	sequence 'var_set_color_blue':
		material_config 'units/payday2/props/gen_prop_container_murky/gen_prop_container_murky_2'.
		Disable object 'g_1'.
		Disable object 'g_2'.
		Enable object 'g_3'.
	sequence 'var_set_color_yellow':
		material_config 'units/payday2/props/gen_prop_container_murky/gen_prop_container_murky_1'.
		Disable object 'g_1'.
		Disable object 'g_2'.
		Enable object 'g_3'.
	sequence 'state_vis_hide':
		Disable body 'body_container'.
		Disable decal_mesh 'dm_metal'.
		Hide graphic_group 'grp_container'.
	sequence 'state_vis_show':
		Enable body 'body_container'.
		Enable decal_mesh 'dm_metal'.
		Show graphic_group 'grp_container'.
	sequence 'state_vis_hide_murky_logo':
		Disable object 'g_logo'.
	sequence 'spawn_content_1':
		If spawn_bool == 1: 
			spawn_unit 'units/payday2/props/ind_prop_dock_box/ind_prop_dock_box_wall_open_1':
				position object_pos('sp_spawn')
				rotation object_rot('sp_spawn')
		If spawn_bool == 1: 
			spawn_unit 'units/payday2/props/ind_prop_dock_box/ind_prop_dock_box_wall_open_1':
				position object_pos('sp_spawn_inverse')
				rotation object_rot('sp_spawn_inverse')
		spawn_bool = 0
	sequence 'spawn_content_2':
		If spawn_bool == 1: 
			spawn_unit 'units/payday2/props/ind_prop_dock_box/ind_prop_dock_box_wall_open_2':
				position object_pos('sp_spawn')
				rotation object_rot('sp_spawn')
		spawn_bool = 0
	sequence 'spawn_content_3':
		If spawn_bool == 1: 
			spawn_unit 'units/payday2/props/ind_prop_dock_box/ind_prop_dock_box_wall_open_3':
				position object_pos('sp_spawn')
				rotation object_rot('sp_spawn')
		spawn_bool = 0
	sequence 'spawn_content_4':
		If spawn_bool == 1: 
			spawn_unit 'units/payday2/props/ind_prop_dock_box/ind_prop_dock_box_wall_open_4':
				position object_pos('sp_spawn')
				rotation object_rot('sp_spawn')
		spawn_bool = 0
	sequence 'spawn_content_5':
		If spawn_bool == 1: 
			spawn_unit 'units/payday2/props/ind_prop_dock_box/ind_prop_dock_box_wall_open':
				position object_pos('sp_spawn')
				rotation object_rot('sp_spawn')
		spawn_bool = 0
	sequence 'spawn_content_6':
		If spawn_bool == 1: 
			spawn_unit 'units/payday2/props/ind_prop_dock_box/ind_prop_dock_box_wall_open_1':
				position object_pos('sp_spawn')
				rotation -object_rot('sp_spawn')
		spawn_bool = 0
