unit:
	sequence 'show':
		Enable object 'g_rope_ladder'.
	sequence 'hide':
		Disable object 'g_rope_ladder'.
	sequence 'throw_ladder':
		Enable animation_group 'anim':
			from 0/30
