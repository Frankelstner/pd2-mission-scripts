unit:
	sequence 'default_roof':
		Disable body 'body_hole_mover_blocker'.
		Enable body 'body_fill'.
		Disable body 'body_hole'.
		Disable object 'g_hole'.
		Enable object 'g_fill'.
		Enable object 's_s'.
		Enable object 's_fill'.
		Disable object 's_hole'.
		Enable decal_mesh 'dm_plaster_fill'.
		Enable decal_mesh 'dm_wood_fill'.
		Disable decal_mesh 'dm_wood_hole'.
		Disable decal_mesh 'dm_plaster_hole'.
		Disable decal_mesh 'dm_concrete_hole'.
	sequence 'broken_roof':
		Enable body 'body_hole_mover_blocker'.
		Disable body 'body_fill'.
		Enable body 'body_hole'.
		Enable object 'g_hole'.
		Disable object 'g_fill'.
		Enable object 's_s'.
		Disable object 's_fill'.
		Enable object 's_hole'.
		Disable decal_mesh 'dm_plaster_fill'.
		Disable decal_mesh 'dm_wood_fill'.
		Enable decal_mesh 'dm_wood_hole'.
		Enable decal_mesh 'dm_plaster_hole'.
		Enable decal_mesh 'dm_concrete_hole'.
