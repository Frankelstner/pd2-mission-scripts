unit:
	open = 0
	sequence 'color_variation_orig':
		material_config 'units/payday2/vehicles/str_vehicle_car_crossover/str_vehicle_car_crossover'.
	sequence 'color_variation_orange':
		material_config 'units/pd2_dlc_flat/vehicles/flt_vehicle_car_crossover/flt_vehicle_car_crossover_orange'.
	sequence 'color_variation_white':
		material_config 'units/pd2_dlc_flat/vehicles/flt_vehicle_car_crossover/flt_vehicle_car_crossover_white'.
	sequence 'state_interaction_enabled':
		Enable interaction.
	sequence 'state_interaction_disabled':
		Disable interaction.
	sequence 'state_lights_on':
		Enable light 'li_light_back'.
		Enable light 'li_light_front'.
		Enable object 'g_lights_il'.
	sequence 'state_lights_off':
		Disable light 'li_light_back'.
		Disable light 'li_light_front'.
		Disable object 'g_lights_il'.
	sequence 'state_vis_show':
		Enable interaction.
		Enable body 'body_vehicle'.
		Enable body 'body_door_front_left'.
		Enable body 'body_door_front_right'.
		Enable body 'body_door_rear_left'.
		Enable body 'body_door_rear_right'.
		Enable body 'body_trunk'.
		Enable body 'body_tire_front_right'.
		Enable body 'body_tire_front_left'.
		Enable body 'body_tire_rear_right'.
		Enable body 'body_tire_rear_left'.
		Enable body 'body_win_front_left'.
		Enable body 'body_win_front_right'.
		Enable body 'body_win_rear_left'.
		Enable body 'body_win_rear_right'.
		Enable body 'body_win_rear2_left'.
		Enable body 'body_win_rear2_right'.
		Enable body 'body_win_front'.
		Enable body 'body_win_rear'.
		Enable decal_mesh 'dm_vehicle'.
		Enable decal_mesh 'dm_door_front_l'.
		Enable decal_mesh 'dm_door_front_r'.
		Enable decal_mesh 'dm_door_rear_l'.
		Enable decal_mesh 'dm_door_rear_r'.
		Enable decal_mesh 'dm_trunk'.
		Enable decal_mesh 'dm_tire_front_left'.
		Enable decal_mesh 'dm_tire_rear_left'.
		Enable decal_mesh 'dm_tire_front_right'.
		Enable decal_mesh 'dm_tire_rear_right'.
		Show graphic_group 'grp_vehicle'.
	sequence 'state_vis_hide':
		Disable interaction.
		Disable body 'body_vehicle'.
		Disable body 'body_door_front_left'.
		Disable body 'body_door_front_right'.
		Disable body 'body_door_rear_left'.
		Disable body 'body_door_rear_right'.
		Disable body 'body_trunk'.
		Disable body 'body_tire_front_right'.
		Disable body 'body_tire_front_left'.
		Disable body 'body_tire_rear_right'.
		Disable body 'body_tire_rear_left'.
		Disable body 'body_win_front_left'.
		Disable body 'body_win_front_right'.
		Disable body 'body_win_rear_left'.
		Disable body 'body_win_rear_right'.
		Disable body 'body_win_rear2_left'.
		Disable body 'body_win_rear2_right'.
		Disable body 'body_win_front'.
		Disable body 'body_win_rear'.
		Disable decal_mesh 'dm_vehicle'.
		Disable decal_mesh 'dm_door_front_l'.
		Disable decal_mesh 'dm_door_front_r'.
		Disable decal_mesh 'dm_door_rear_l'.
		Disable decal_mesh 'dm_door_rear_r'.
		Disable decal_mesh 'dm_trunk'.
		Disable decal_mesh 'dm_tire_front_left'.
		Disable decal_mesh 'dm_tire_rear_left'.
		Disable decal_mesh 'dm_tire_front_right'.
		Disable decal_mesh 'dm_tire_rear_right'.
		Hide graphic_group 'grp_vehicle'.
	sequence 'anim_door_front_left_open':
		Enable animation_group 'door_front_left':
			from 0/30
			to 15/30
	sequence 'anim_door_front_left_close':
		Enable animation_group 'door_front_left':
			from 15/30
			speed -1
			to 0/30
	sequence 'anim_door_front_right_open':
		Enable animation_group 'door_front_right':
			from 0/30
			to 15/30
	sequence 'anim_door_front_right_close':
		Enable animation_group 'door_front_right':
			from 15/30
			speed -1
			to 0/30
	sequence 'anim_door_rear_left_open':
		Enable animation_group 'door_rear_left':
			from 0/30
			to 15/30
	sequence 'anim_door_rear_left_close':
		Enable animation_group 'door_rear_left':
			from 15/30
			speed -1
			to 0/30
	sequence 'anim_door_rear_right_open':
		Enable animation_group 'door_rear_right':
			from 0/30
			to 15/30
	sequence 'anim_door_rear_right_close':
		Enable animation_group 'door_rear_right':
			from 15/30
			speed -1
			to 0/30
	sequence 'anim_trunk_open':
		Enable animation_group 'trunk':
			from 0/30
			to 15/30
		Play audio 'open_car_trunk' at 'anim_trunk'.
		open = 1
	sequence 'anim_trunk_close':
		Enable animation_group 'trunk':
			from 15/30
			speed -1
			to 0/30
		open = 0
	sequence 'interact':
		If open == 1: Run sequence 'int_trunk_close'.
		If open == 0: Run sequence 'anim_trunk_open'.
		If open == 2: Run sequence 'anim_trunk_close'.
	sequence 'int_trunk_close':
		Enable animation_group 'trunk':
			from 15/30
			speed -1
			to 0/30
		open = 2
	body 'body_win_front_left'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_win_front_left_crc'.
		Upon receiving 2 bullet hits or 20 explosion damage or 20 melee damage, execute:
			Run sequence 'int_seq_win_front_left_sht'.
	sequence 'int_seq_win_front_left_crc':
		TRIGGER TIMES 1
		Disable object 'g_win_front_l'.
		Enable object 'g_win_front_l_dmg'.
		Play audio 'glass_crack' at 'e_win_front_l'.
	sequence 'int_seq_win_front_left_sht':
		TRIGGER TIMES 1
		Disable body 'body_win_front_left'.
		Disable object 'g_win_front_l'.
		Disable object 'g_win_front_l_dmg'.
		Play audio 'window_small_shatter' at 'e_win_front_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_win_front_l' )
			position v()
	body 'body_win_front_right'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_win_front_right_crc'.
		Upon receiving 2 bullet hits or 20 explosion damage or 20 melee damage, execute:
			Run sequence 'int_seq_win_front_right_sht'.
	sequence 'int_seq_win_front_right_crc':
		TRIGGER TIMES 1
		Disable object 'g_win_front_r'.
		Enable object 'g_win_front_r_dmg'.
		Play audio 'glass_crack' at 'e_win_front_r'.
	sequence 'int_seq_win_front_right_sht':
		TRIGGER TIMES 1
		Disable body 'body_win_front_right'.
		Disable object 'g_win_front_r'.
		Disable object 'g_win_front_r_dmg'.
		Play audio 'window_small_shatter' at 'e_win_front_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_win_front_r' )
			position v()
	body 'body_win_rear_left'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_win_rear_left_crc'.
		Upon receiving 2 bullet hits or 20 explosion damage or 20 melee damage, execute:
			Run sequence 'int_seq_win_rear_left_sht'.
	sequence 'int_seq_win_rear_left_crc':
		TRIGGER TIMES 1
		Disable object 'g_win_rear_l'.
		Enable object 'g_win_rear_l_dmg'.
		Play audio 'glass_crack' at 'e_win_rear_l'.
	sequence 'int_seq_win_rear_left_sht':
		TRIGGER TIMES 1
		Disable body 'body_win_rear_left'.
		Disable object 'g_win_rear_l'.
		Disable object 'g_win_rear_l_dmg'.
		Play audio 'window_small_shatter' at 'e_win_rear_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_win_rear_l' )
			position v()
	body 'body_win_rear_right'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_win_rear_right_crc'.
		Upon receiving 2 bullet hits or 20 explosion damage or 20 melee damage, execute:
			Run sequence 'int_seq_win_rear_right_sht'.
	sequence 'int_seq_win_rear_right_crc':
		TRIGGER TIMES 1
		Disable object 'g_win_rear_r'.
		Enable object 'g_win_rear_r_dmg'.
		Play audio 'glass_crack' at 'e_win_rear_r'.
	sequence 'int_seq_win_rear_right_sht':
		TRIGGER TIMES 1
		Disable body 'body_win_rear_right'.
		Disable object 'g_win_rear_r'.
		Disable object 'g_win_rear_r_dmg'.
		Play audio 'window_small_shatter' at 'e_win_rear_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_win_rear_r' )
			position v()
	body 'body_win_front'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_win_front_crc'.
	sequence 'int_seq_win_front_crc':
		TRIGGER TIMES 1
		Disable object 'g_win_front'.
		Enable object 'g_win_front_dmg'.
		Play audio 'glass_crack' at 'g_win_front'.
	body 'body_win_rear'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_win_rear_crc'.
	sequence 'int_seq_win_rear_crc':
		TRIGGER TIMES 1
		Disable object 'g_win_rear'.
		Enable object 'g_win_rear_dmg'.
		Play audio 'glass_crack' at 'g_win_rear'.
	body 'body_win_rear2_left'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_win_rear2_left_crc'.
		Upon receiving 2 bullet hits or 20 explosion damage or 20 melee damage, execute:
			Run sequence 'int_seq_win_rear2_left_sht'.
	sequence 'int_seq_win_rear2_left_crc':
		TRIGGER TIMES 1
		Disable object 'g_win_rear2_l'.
		Enable object 'g_win_rear2_l_dmg'.
		Play audio 'glass_crack' at 'e_win_rear2_l'.
	sequence 'int_seq_win_rear2_left_sht':
		TRIGGER TIMES 1
		Disable body 'body_win_rear2_left'.
		Disable object 'g_win_rear2_l'.
		Disable object 'g_win_rear2_l_dmg'.
		Play audio 'window_small_shatter' at 'e_win_rear2_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_win_rear2_l' )
			position v()
	body 'body_win_rear2_right'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_win_rear2_right_crc'.
		Upon receiving 2 bullet hits or 20 explosion damage or 20 melee damage, execute:
			Run sequence 'int_seq_win_rear2_right_sht'.
	sequence 'int_seq_win_rear2_right_crc':
		TRIGGER TIMES 1
		Disable object 'g_win_rear2_r'.
		Enable object 'g_win_rear2_r_dmg'.
		Play audio 'glass_crack' at 'e_win_rear2_r'.
	sequence 'int_seq_win_rear2_right_sht':
		TRIGGER TIMES 1
		Disable body 'body_win_rear2_right'.
		Disable object 'g_win_rear2_r'.
		Disable object 'g_win_rear2_r_dmg'.
		Play audio 'window_small_shatter' at 'e_win_rear2_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_win_rear2_r' )
			position v()
