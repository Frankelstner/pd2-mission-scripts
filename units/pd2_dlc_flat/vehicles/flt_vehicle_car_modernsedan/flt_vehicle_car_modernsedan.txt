unit:
	sequence 'orig':
		material_config 'units/payday2/vehicles/str_vehicle_car_modernsedan/str_vehicle_car_modernsedan'.
	sequence 'red':
		material_config 'units/pd2_dlc_flat/vehicles/flt_vehicle_car_modernsedan/flt_vehicle_car_modernsedan_red'.
	sequence 'black':
		material_config 'units/pd2_dlc_flat/vehicles/flt_vehicle_car_modernsedan/flt_vehicle_car_modernsedan_black'.
	sequence 'int_seq_shatter_win_side_left_1':
		TRIGGER TIMES 1
		Disable object 'g_windows_side_left'.
		Enable object 'g_windows_side_left_dmg'.
	sequence 'int_seq_shatter_win_side_left_2':
		TRIGGER TIMES 1
		Disable body 'body_windows_side_left'.
		Disable object 'g_windows_side_left_dmg'.
		Disable decal_mesh 'dm_windows_side_left'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_windows_side_left' )
			position v()
		Play audio 'window_small_shatter' at 'e_windows_side_left'.
	sequence 'int_seq_shatter_win_side_right_1':
		TRIGGER TIMES 1
		Disable object 'g_windows_side_right'.
		Enable object 'g_windows_side_right_dmg'.
	sequence 'int_seq_shatter_win_side_right_2':
		TRIGGER TIMES 1
		Disable body 'body_windows_side_right'.
		Disable object 'g_windows_side_right_dmg'.
		Disable decal_mesh 'dm_windows_side_right'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_windows_side_right' )
			position v()
		Play audio 'window_small_shatter' at 'e_windows_side_right'.
	sequence 'int_seq_shatter_win_front_1':
		TRIGGER TIMES 1
		Disable object 'g_windows_front'.
		Enable object 'g_windows_front_dmg'.
	sequence 'int_seq_shatter_win_front_2':
		TRIGGER TIMES 1
		Disable object 'g_windows_front'.
		Disable object 'g_windows_front_dmg'.
	sequence 'int_seq_shatter_win_rear_1':
		TRIGGER TIMES 1
		Disable object 'g_windows_back'.
		Enable object 'g_windows_back_dmg'.
	sequence 'int_seq_shatter_win_rear_2':
		TRIGGER TIMES 1
		Disable body 'body_windows_back'.
		Disable object 'g_windows_back_dmg'.
		Disable decal_mesh 'dm_windows_back'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_windows_back' )
			position v()
		Play audio 'window_small_shatter' at 'e_windows_back'.
	sequence 'trigger_car_alarm':
		Play audio 'car_alarm_on' at 'snd_car_alarm'.
		Enable animation_group 'alarm_lights':
			from 0/30
			loop True
			to 20/30
		Enable object 'g_lights'.
		Enable light 'ls_right'.
		Enable light 'ls_left'.
		Enable light 'ls_rear'.
		Run sequence 'car_alarm_of'. (DELAY 435/30)
	sequence 'car_alarm_of':
		Disable object 'g_lights'.
		Disable light 'ls_right'.
		Disable light 'ls_left'.
		Disable light 'ls_rear'.
	body 'body_windows_side_left'
		Upon receiving 5 bullet hits or 10 explosion damage or 2.5 saw damage or 10 melee damage, execute:
			Run sequence 'int_seq_shatter_win_side_left_1'.
		Upon receiving 10 bullet hits or 10 explosion damage or 5 saw damage or 40 melee damage, execute:
			Run sequence 'int_seq_shatter_win_side_left_2'.
	body 'body_windows_side_right'
		Upon receiving 5 bullet hits or 10 explosion damage or 2.5 saw damage or 10 melee damage, execute:
			Run sequence 'int_seq_shatter_win_side_right_1'.
		Upon receiving 10 bullet hits or 10 explosion damage or 5 saw damage or 40 melee damage, execute:
			Run sequence 'int_seq_shatter_win_side_right_2'.
	body 'body_windows_front'
		Upon receiving 5 bullet hits or 10 explosion damage or 12.5 saw damage or 10 melee damage, execute:
			Run sequence 'int_seq_shatter_win_front_1'.
	body 'body_windows_back'
		Upon receiving 5 bullet hits or 10 explosion damage or 2.5 saw damage or 10 melee damage, execute:
			Run sequence 'int_seq_shatter_win_rear_1'.
