unit:
	sequence 'startup_times':
		EXECUTE ON STARTUP
		Run sequence 'start_time_'..pick('1','2','3','4','5','6').
		startup True
	sequence 'start_time_1':
		Enable animation_group 'ag_turn_slow': (DELAY 10/30)
			loop True
	sequence 'start_time_2':
		Enable animation_group 'ag_turn_slow': (DELAY 20/30)
			loop True
	sequence 'start_time_3':
		Enable animation_group 'ag_turn_slow': (DELAY 30/30)
			loop True
	sequence 'start_time_4':
		Enable animation_group 'ag_turn_slow': (DELAY 40/30)
			loop True
	sequence 'start_time_5':
		Enable animation_group 'ag_turn_slow': (DELAY 50/30)
			loop True
	sequence 'start_time_6':
		Enable animation_group 'ag_turn_slow': (DELAY 60/30)
			loop True
	sequence 'broken_slow':
		Disable object 'g_whole_slow_off'.
		Disable object 'g_broken_lod0'.
		Enable object 'g_broken_slow_lod0'.
		Enable object 'g_contact_lod0'.
		Disable object 'g_whole_lod0'.
		Disable object 'g_whole_slow_lod0'.
		Disable object 'g_whole_off'.
		Disable light 'lo_omni'.
		Disable body 'body_collision_01'.
		Disable animation_group 'ag_turn_pendulum'.
		Enable animation_group 'ag_turn_slow':
			from 0
			loop True
			to 133
	sequence 'broken_pendulum':
		Disable object 'g_whole_slow_off'.
		Enable object 'g_broken_lod0'.
		Disable object 'g_broken_slow_lod0'.
		Enable object 'g_contact_lod0'.
		Disable object 'g_whole_lod0'.
		Disable object 'g_whole_slow_lod0'.
		Disable object 'g_whole_off'.
		Disable light 'lo_omni'.
		Disable body 'body_collision_02'.
		Disable animation_group 'ag_turn_slow'.
		Enable animation_group 'ag_turn_pendulum':
			from 0
			loop True
			to 133
	sequence 'turn_on_slow':
		Disable object 'g_whole_slow_off'.
		Disable object 'g_broken_lod0'.
		Disable object 'g_broken_slow_lod0'.
		Enable object 'g_contact_lod0'.
		Disable object 'g_whole_lod0'.
		Enable object 'g_whole_slow_lod0'.
		Disable object 'g_whole_off'.
		Enable light 'lo_omni'.
		Disable animation_group 'ag_turn_pendulum'.
		Enable animation_group 'ag_turn_slow':
			from 0
			loop True
			to 133
	sequence 'turn_off_slow':
		Enable object 'g_whole_slow_off'.
		Disable object 'g_broken_lod0'.
		Disable object 'g_broken_slow_lod0'.
		Enable object 'g_contact_lod0'.
		Disable object 'g_whole_lod0'.
		Disable object 'g_whole_slow_lod0'.
		Disable object 'g_whole_off'.
		Disable light 'lo_omni'.
		Disable animation_group 'ag_turn_pendulum'.
		Enable animation_group 'ag_turn_slow':
			from 0
			loop True
			to 133
	sequence 'turn_on_pendulum':
		Disable object 'g_whole_slow_off'.
		Disable object 'g_broken_lod0'.
		Disable object 'g_broken_slow_lod0'.
		Enable object 'g_contact_lod0'.
		Enable object 'g_whole_lod0'.
		Disable object 'g_whole_slow_lod0'.
		Disable object 'g_whole_off'.
		Enable light 'lo_omni'.
		Disable animation_group 'ag_turn_slow'.
		Enable animation_group 'ag_turn_pendulum':
			from 0
			loop True
			to 133
	sequence 'turn_off_pendulum':
		Disable object 'g_whole_slow_off'.
		Disable object 'g_broken_lod0'.
		Disable object 'g_broken_slow_lod0'.
		Enable object 'g_contact_lod0'.
		Disable object 'g_whole_lod0'.
		Disable object 'g_whole_slow_lod0'.
		Enable object 'g_whole_off'.
		Disable light 'lo_omni'.
		Disable animation_group 'ag_turn_slow'.
		Enable animation_group 'ag_turn_pendulum':
			from 0
			loop True
			to 133
	sequence 'kill_light_bulb':
		Disable light 'lo_omni'.
		Disable object 'g_whole_slow_lod0'.
		Disable object 'g_broken_slow_lod0'.
		Disable object 'g_whole_lod0'.
		Enable object 'g_broken_lod0'.
		Disable object 'g_whole_slow_off'.
		Disable object 'g_whole_off'.
		Disable body 'body_collision_01'.
		Disable body 'body_collision_02'.
		Play audio 'light_bulb_smash' at 'c_sphere_01'.
		Play audio 'light_bulb_smash' at 'c_sphere_02'.
	body 'body_collision_01'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'kill_light_bulb'.
