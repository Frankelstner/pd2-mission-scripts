unit:
	sequence 'destroy_01':
		TRIGGER TIMES 1
		Disable body 'body_collision_01'.
		Disable object 'g_glass_01'.
		Disable decal_mesh 'dm_glass_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_01')
			position v()
	sequence 'destroy_02':
		TRIGGER TIMES 1
		Disable body 'body_collision_02'.
		Disable object 'g_glass_02'.
		Disable decal_mesh 'dm_glass_02'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_02')
			position v()
	body 'body_collision_01'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_01'.
	body 'body_collision_02'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_02'.
