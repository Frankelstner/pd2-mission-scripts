unit:
	sequence 'make_dynamic':
		material_config 'units/pd2_dlc_flat/props/flt_prop_coffe_cup/flt_prop_coffe_cup'.
		Enable body 'dynamic_shard_00'.
		Enable body 'dynamic_shard_01'.
		Enable body 'dynamic_shard_02'.
		Enable body 'dynamic_shard_03'.
		Disable body 'static_body'.
		Play audio 'vase_break' at 'e_pow'.
		Show graphic_group 'grp_dmg'.
		Hide graphic_group 'grp_static'.
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('e_pow') ),12,8).
		slot:
			slot 18
	body 'static_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'make_dynamic'.
