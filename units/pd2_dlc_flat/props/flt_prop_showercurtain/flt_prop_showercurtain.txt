unit:
	sequence 'default':
		Enable object 'g_lod0'.
		Enable object 'g_lod1'.
		Enable object 'g_right_lod0'.
		Enable object 'g_right_lod1'.
		Enable object 'g_right_lod2'.
		Enable object 'g_left_lod0'.
		Enable object 'g_left_lod1'.
		Enable object 'g_left_lod2'.
		Enable body 'showercurtain_body'.
		Enable decal_mesh 'dm_thin_layer_left'.
		Enable decal_mesh 'dm_thin_layer_right'.
		reset_editable_state True
	sequence 'Left_Only':
		Enable object 'g_lod0'.
		Enable object 'g_lod1'.
		Disable object 'g_right_lod0'.
		Disable object 'g_right_lod1'.
		Disable object 'g_right_lod2'.
		Enable object 'g_left_lod0'.
		Enable object 'g_left_lod1'.
		Enable object 'g_left_lod2'.
		Enable body 'showercurtain_body'.
		Enable decal_mesh 'dm_thin_layer_left'.
		Disable decal_mesh 'dm_thin_layer_right'.
	sequence 'Right_Only':
		Enable object 'g_lod0'.
		Enable object 'g_lod1'.
		Enable object 'g_right_lod0'.
		Enable object 'g_right_lod1'.
		Enable object 'g_right_lod2'.
		Disable object 'g_left_lod0'.
		Disable object 'g_left_lod1'.
		Disable object 'g_left_lod2'.
		Enable body 'showercurtain_body'.
		Enable decal_mesh 'dm_thin_layer_left'.
		Enable decal_mesh 'dm_thin_layer_right'.
