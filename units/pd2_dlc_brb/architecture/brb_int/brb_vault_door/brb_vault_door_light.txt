unit:
	sequence 'enable_interaction':
		Enable interaction.
		Enable object 'g_outline_lod_0'.
	sequence 'disable_interaction':
		Disable interaction.
		Disable object 'g_outline_lod_0'.
	sequence 'int_seq_startup':
		EXECUTE ON STARTUP
		material 'mat_glow':
			time 0
		material 'mat_glow':
			state 'pause'
		startup True
	sequence 'anim_open':
		Enable animation_group 'open':
			from 0/30
			to 600/30
		Play audio 'bank_vault_door_light_open_brb' at 'snd'.
		Disable body 'body_mover_blocker'. (DELAY 600/30)
	sequence 'anim_close':
		Enable animation_group 'open':
			from 600/30
			speed -1
			to 0/30
	sequence 'state_thermite_hinge_01':
		Enable effect_spawner 'effect_hinge_01'.
		Play audio 'thermite_paste_start' at 'e_effect_hinge_01'.
		Enable object 'g_thermite_hinge_01'.
	sequence 'state_thermite_hinge_02':
		Enable effect_spawner 'effect_hinge_02'.
		Play audio 'thermite_paste_start' at 'e_effect_hinge_02'.
		Enable object 'g_thermite_hinge_02'.
	sequence 'state_thermite_lock_01':
		Enable effect_spawner 'effect_lock_01'.
		Play audio 'thermite_paste_start' at 'e_effect_lock_01'.
		Enable object 'g_thermite_lock_01'.
	sequence 'state_thermite_lock_02':
		Enable effect_spawner 'effect_lock_02'.
		Play audio 'thermite_paste_start' at 'e_effect_lock_02'.
		Enable object 'g_thermite_lock_02'.
	sequence 'state_hinge_01_destroy':
		material 'mat_glow':
			state 'play -0.08'
			time 1
		Show graphic_group 'grp_hinge_broken_top'.
		Hide graphic_group 'grp_hinge_whole_top'.
		Disable effect_spawner 'effect_hinge_01'. (DELAY 2)
		Disable object 'g_thermite_hinge_01'. (DELAY 2)
		Play audio 'thermite_paste_finish' at 'e_effect_hinge_01'. (DELAY 2)
		effect 'effects/payday2/particles/fire/thermite_fade':
			parent object( 'e_effect_hinge_01' )
			position v()
	sequence 'state_hinge_02_destroy':
		material 'mat_glow':
			state 'play -0.08'
			time 1
		Show graphic_group 'grp_hinge_broken_bottom'.
		Hide graphic_group 'grp_hinge_whole_bottom'.
		Disable effect_spawner 'effect_hinge_02'. (DELAY 2)
		Disable object 'g_thermite_hinge_02'. (DELAY 2)
		Play audio 'thermite_paste_finish' at 'e_effect_hinge_02'. (DELAY 2)
		effect 'effects/payday2/particles/fire/thermite_fade':
			parent object( 'e_effect_hinge_02' )
			position v()
	sequence 'state_lock_01_destroy':
		material 'mat_glow':
			state 'play -0.08'
			time 1
		Hide graphic_group 'grp_lock_01_whole'.
		Show graphic_group 'grp_lock_01_broken'.
		Hide graphic_group 'grp_lock_box_01'.
		Show graphic_group 'grp_lock_box_broken_01'.
		Disable effect_spawner 'effect_lock_01'. (DELAY 2)
		Disable object 'g_thermite_lock_01'. (DELAY 2)
		Play audio 'thermite_paste_finish' at 'e_effect_lock_01'. (DELAY 2)
		effect 'effects/payday2/particles/fire/thermite_fade':
			parent object( 'e_effect_lock_01' )
			position v()
	sequence 'state_lock_02_destroy':
		material 'mat_glow':
			state 'play -0.08'
			time 1
		Hide graphic_group 'grp_lock_02_whole'.
		Show graphic_group 'grp_lock_02_broken'.
		Hide graphic_group 'grp_lock_box_02'.
		Show graphic_group 'grp_lock_box_broken_02'.
		Disable effect_spawner 'effect_lock_02'. (DELAY 2)
		Disable object 'g_thermite_lock_02'. (DELAY 2)
		Play audio 'thermite_paste_finish' at 'e_effect_lock_02'. (DELAY 2)
		effect 'effects/payday2/particles/fire/thermite_fade':
			parent object( 'e_effect_lock_02' )
			position v()
	sequence 'anim_door_slide_out':
		Enable animation_group 'slide':
			from 0/30
			to 265/30
		Play audio 'brb_winch_animation_pull_02' at 'snd_01'.
	sequence 'state_closed':
		Enable animation_group 'open':
			from 0/30
			to 0/30
	sequence 'state_opened':
		Enable animation_group 'open':
			from 600/30
			to 600/30
	sequence 'open_door':
		Run sequence 'anim_open'.
		Enable body 'body_mover_blocker'.
	sequence 'interact':
		Run sequence 'anim_open'.
		Run sequence 'disable_interaction'.
