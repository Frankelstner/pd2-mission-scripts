unit:
	sequence 'main_doors_closed':
		Show graphic_group 'grp_doors_closed'.
		Enable body 'body_doors_closed'.
		Hide graphic_group 'grp_doors_open'.
		Disable body 'body_doors_open'.
		Hide graphic_group 'grp_doors_crashed_open'.
		Disable body 'body_doors_crashed_open'.
		Disable body 'body_dm_doors_crashed_open'.
		Disable body 'body_doors_crashed_open_nav'.
	sequence 'main_doors_open':
		Hide graphic_group 'grp_doors_closed'.
		Disable body 'body_doors_closed'.
		Show graphic_group 'grp_doors_open'.
		Enable body 'body_doors_open'.
		Hide graphic_group 'grp_doors_crashed_open'.
		Disable body 'body_doors_crashed_open'.
		Disable body 'body_dm_doors_crashed_open'.
		Disable body 'body_doors_crashed_open_nav'.
	sequence 'main_doors_crashed_open':
		Hide graphic_group 'grp_doors_closed'.
		Disable body 'body_doors_closed'.
		Hide graphic_group 'grp_doors_open'.
		Disable body 'body_doors_open'.
		Show graphic_group 'grp_doors_crashed_open'.
		Enable body 'body_doors_crashed_open'.
		Enable body 'body_dm_doors_crashed_open'.
		Disable body 'body_doors_crashed_open_nav'.
	sequence 'main_doors_crashed_open_nav':
		Hide graphic_group 'grp_doors_closed'.
		Disable body 'body_doors_closed'.
		Hide graphic_group 'grp_doors_open'.
		Disable body 'body_doors_open'.
		Show graphic_group 'grp_doors_crashed_open'.
		Enable body 'body_doors_crashed_open'.
		Enable body 'body_dm_doors_crashed_open'.
		Enable body 'body_doors_crashed_open_nav'.
