unit:
	sequence 'sewers_open':
		Show graphic_group 'brb_ext_sewer_open'.
		Enable body 'body_sewer_open'.
		Hide graphic_group 'brb_ext_sewer_closed'.
		Disable body 'body_sewer_closed'.
	sequence 'sewers_closed':
		Hide graphic_group 'brb_ext_sewer_open'.
		Disable body 'body_sewer_open'.
		Show graphic_group 'brb_ext_sewer_closed'.
		Enable body 'body_sewer_closed'.
