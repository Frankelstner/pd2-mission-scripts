unit:
	sequence 'break_01':
		Disable body 'glass_body_01'.
		Disable object 'g_glass_01'.
		Disable decal_mesh 'dm_glass_breakable_01'.
		Enable object 'g_glass_broken_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_01' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_01'.
		Cause alert with 12 m radius.
	body 'glass_body_01'
		Upon receiving 3 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_01'.
