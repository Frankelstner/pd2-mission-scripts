unit:
	sequence 'anim_open':
		Enable animation_group 'anim_open_group':
			from 0/30
			to 60/30
		Disable object 'g_shutter_01'. (DELAY 20/30)
		Disable decal_mesh 'dm_metal_01'. (DELAY 20/30)
		Disable object 's_s_shutter_01'. (DELAY 20/30)
		Disable body 'body_collision_01'. (DELAY 20/30)
		Disable object 'g_shutter_02'. (DELAY 30/30)
		Disable decal_mesh 'dm_metal_02'. (DELAY 30/30)
		Disable object 's_s_shutter_02'. (DELAY 30/30)
		Disable body 'body_collision_02'. (DELAY 30/30)
		Disable object 'g_shutter_03'. (DELAY 41/30)
		Disable decal_mesh 'dm_metal_03'. (DELAY 41/30)
		Disable object 's_s_shutter_03'. (DELAY 41/30)
		Disable body 'body_collision_03'. (DELAY 41/30)
		Disable object 'g_shutter_04'. (DELAY 57/30)
		Disable decal_mesh 'dm_metal_04'. (DELAY 57/30)
		Disable object 's_s_shutter_04'. (DELAY 57/30)
		Disable body 'body_collision_04'. (DELAY 57/30)
		Play audio 'garage_door_electric_start' at 'snd'.
		Play audio 'garage_door_electric_open' at 'snd'. (DELAY 60/30)
	sequence 'anim_close':
		Enable animation_group 'anim_open_group':
			from 60/30
			speed -1
			to 0/30
		Enable object 'g_shutter_01'. (DELAY 40/30)
		Enable decal_mesh 'dm_metal_01'. (DELAY 40/30)
		Enable object 's_s_shutter_01'. (DELAY 40/30)
		Enable body 'body_collision_01'. (DELAY 40/30)
		Enable object 'g_shutter_02'. (DELAY 30/30)
		Enable decal_mesh 'dm_metal_02'. (DELAY 30/30)
		Enable object 's_s_shutter_02'. (DELAY 30/30)
		Enable body 'body_collision_02'. (DELAY 30/30)
		Enable object 'g_shutter_03'. (DELAY 19/30)
		Enable decal_mesh 'dm_metal_03'. (DELAY 19/30)
		Enable object 's_s_shutter_03'. (DELAY 19/30)
		Enable body 'body_collision_03'. (DELAY 19/30)
		Enable object 'g_shutter_04'. (DELAY 0/30)
		Enable decal_mesh 'dm_metal_04'. (DELAY 0/30)
		Enable object 's_s_shutter_04'. (DELAY 0/30)
		Enable body 'body_collision_04'. (DELAY 0/30)
