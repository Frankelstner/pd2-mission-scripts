unit:
	sequence 'explode':
		Call function: base.bullet_hit()
	body 'static_body'
		Upon receiving 1 bullet hit, execute:
			Run sequence 'explode'.
