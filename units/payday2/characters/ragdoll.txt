unit:
	sequence 'kill_tazer_headshot':
		Run sequence 'int_seq_tazer_'..pick('hs_1','hs_2').
	sequence 'int_seq_tazer_hs_1':
		Hide graphic_group 'head'.
		Disable object 'g_head_gore2'.
		Enable object 'g_head_gore1'.
		effect 'effects/payday2/particles/impacts/blood/blood_impact_katana':
			parent object( 'e_gore_1' )
			position v()
		effect 'effects/payday2/particles/impacts/blood/blood_impact_katana':
			parent object( 'e_gore_2' )
			position v()
		Play audio 'expl_gen_head' at 'e_gore_2'.
	sequence 'int_seq_tazer_hs_2':
		Hide graphic_group 'head'.
		Disable object 'g_head_gore1'.
		Enable object 'g_head_gore2'.
		effect 'effects/payday2/particles/impacts/blood/blood_impact_katana':
			parent object( 'e_gore_1' )
			position v()
		effect 'effects/payday2/particles/impacts/blood/blood_impact_katana':
			parent object( 'e_gore_2' )
			position v()
		Play audio 'expl_gen_head' at 'e_gore_2'.
	sequence 'show_tape':
		Enable object 'g_tape_mouth'.
		Enable object 'g_tape_arms'.
		Enable object 'g_tape_waist'.
	sequence 'hide_tape':
		Disable object 'g_tape_mouth'.
		Disable object 'g_tape_arms'.
		Disable object 'g_tape_waist'.
	sequence 'show_tape_cop':
		Enable object 'g_tape'.
	sequence 'hide_tape_cop':
		Disable object 'g_tape'.
	sequence 'mr_brown_headshot':
		material_config 'units/pd2_dlc_rvd/characters/npc_mr_brown/npc_mr_brown_headshot'.
		Disable body 'body'.
		Disable body 'head'.
		Disable body 'mover_blocker'.
	sequence 'spawn_bain_bag':
		Call function: spawn_manager.spawn_unit('bag','Spine2','units/pd2_dlc_bph/props/bph_prop_bain_bag/bph_prop_bain_bag')
	sequence 'disable_hitbox':
		Disable body 'body'.
		Disable body 'head'.
		Disable body 'rag_LeftLeg'.
		Disable body 'rag_RightLeg'.
		Disable body 'rag_RightUpLeg'.
		Disable body 'rag_LeftUpLeg'.
	sequence 'kill_spook_lights':
		Disable effect_spawner 'es_light'.
		Disable light 'point_light'.
		Disable object 'g_il'.
	sequence 'kill_murky_flashlights':
		Disable effect_spawner 'es_light'.
		Disable light 'ls_light'.
	sequence 'turn_on_spook_lights':
		Enable effect_spawner 'es_light'.
		Enable light 'point_light'.
		Enable object 'g_il'.
	sequence 'leg_arm_hitbox':
		Enable body 'body'.
		Enable body 'head'.
		Disable body 'rag_Hips':
			motion 'keyframed'
		Enable body 'rag_LeftUpLeg':
			motion 'keyframed'
		Enable body 'rag_RightUpLeg':
			motion 'keyframed'
		Enable body 'rag_LeftLeg':
			motion 'keyframed'
		Enable body 'rag_RightLeg':
			motion 'keyframed'
		Disable body 'rag_Spine':
			motion 'keyframed'
		Disable body 'rag_Spine1':
			motion 'keyframed'
		Disable body 'rag_Spine2':
			motion 'keyframed'
		Disable body 'rag_LeftForeArm':
			motion 'keyframed'
		Disable body 'rag_RightForeArm':
			motion 'keyframed'
		Disable body 'rag_LeftArm':
			motion 'keyframed'
		Disable body 'rag_RightArm':
			motion 'keyframed'
		Disable body 'rag_Head':
			motion 'keyframed'
		Disable constraint 'LeftUpLeg'.
		Disable constraint 'RightUpLeg'.
		Disable constraint 'LeftLeg'.
		Disable constraint 'RightLeg'.
		Disable constraint 'Spine'.
		Disable constraint 'Spine1'.
		Disable constraint 'Spine2'.
		Disable constraint 'LeftForeArm'.
		Disable constraint 'RightForeArm'.
		Disable constraint 'LeftArm'.
		Disable constraint 'RightArm'.
		Disable constraint 'Head'.
	sequence 'switch_to_ragdoll':
		Disable body 'body'.
		Disable body 'head'.
		Enable body 'rag_Hips':
			motion 'dynamic'
		Enable body 'rag_LeftUpLeg':
			motion 'dynamic'
		Enable body 'rag_RightUpLeg':
			motion 'dynamic'
		Enable body 'rag_LeftLeg':
			motion 'dynamic'
		Enable body 'rag_RightLeg':
			motion 'dynamic'
		Enable body 'rag_Spine':
			motion 'dynamic'
		Enable body 'rag_Spine1':
			motion 'dynamic'
		Enable body 'rag_Spine2':
			motion 'dynamic'
		Enable body 'rag_LeftForeArm':
			motion 'dynamic'
		Enable body 'rag_RightForeArm':
			motion 'dynamic'
		Enable body 'rag_LeftArm':
			motion 'dynamic'
		Enable body 'rag_RightArm':
			motion 'dynamic'
		Enable body 'rag_Head':
			motion 'dynamic'
		Enable constraint 'LeftUpLeg'.
		Enable constraint 'RightUpLeg'.
		Enable constraint 'LeftLeg'.
		Enable constraint 'RightLeg'.
		Enable constraint 'Spine'.
		Enable constraint 'Spine1'.
		Enable constraint 'Spine2'.
		Enable constraint 'LeftForeArm'.
		Enable constraint 'RightForeArm'.
		Enable constraint 'LeftArm'.
		Enable constraint 'RightArm'.
		Enable constraint 'Head'.
	sequence 'freeze_ragdoll':
		body 'rag_Hips':
			motion 'fixed'
		body 'rag_LeftUpLeg':
			motion 'fixed'
		body 'rag_RightUpLeg':
			motion 'fixed'
		body 'rag_LeftLeg':
			motion 'fixed'
		body 'rag_RightLeg':
			motion 'fixed'
		body 'rag_Spine':
			motion 'fixed'
		body 'rag_Spine1':
			motion 'fixed'
		body 'rag_Spine2':
			motion 'fixed'
		body 'rag_LeftForeArm':
			motion 'fixed'
		body 'rag_RightForeArm':
			motion 'fixed'
		body 'rag_LeftArm':
			motion 'fixed'
		body 'rag_RightArm':
			motion 'fixed'
		body 'rag_Head':
			motion 'fixed'
		Disable constraint 'LeftUpLeg'.
		Disable constraint 'RightUpLeg'.
		Disable constraint 'LeftLeg'.
		Disable constraint 'RightLeg'.
		Disable constraint 'Spine'.
		Disable constraint 'Spine1'.
		Disable constraint 'Spine2'.
		Disable constraint 'LeftForeArm'.
		Disable constraint 'RightForeArm'.
		Disable constraint 'LeftArm'.
		Disable constraint 'RightArm'.
		Disable constraint 'Head'.
	sequence 'swich_for_test':
		Disable body 'body'.
		Disable body 'head'.
		Disable body 'mover_blocker'.
		Enable body 'rag_Hips':
			motion 'keyframed'
		Enable body 'rag_LeftUpLeg':
			motion 'keyframed'
		Enable body 'rag_RightUpLeg':
			motion 'keyframed'
		Enable body 'rag_LeftLeg':
			motion 'keyframed'
		Enable body 'rag_RightLeg':
			motion 'keyframed'
		Enable body 'rag_Spine':
			motion 'keyframed'
		Enable body 'rag_Spine1':
			motion 'keyframed'
		Enable body 'rag_Spine2':
			motion 'keyframed'
		Enable body 'rag_LeftForeArm':
			motion 'keyframed'
		Enable body 'rag_RightForeArm':
			motion 'keyframed'
		Enable body 'rag_LeftArm':
			motion 'keyframed'
		Enable body 'rag_RightArm':
			motion 'keyframed'
		Enable body 'rag_Head':
			motion 'keyframed'
		Enable constraint 'LeftUpLeg'.
		Enable constraint 'RightUpLeg'.
		Enable constraint 'LeftLeg'.
		Enable constraint 'RightLeg'.
		Enable constraint 'Spine'.
		Enable constraint 'Spine1'.
		Enable constraint 'Spine2'.
		Enable constraint 'LeftForeArm'.
		Enable constraint 'RightForeArm'.
		Enable constraint 'LeftArm'.
		Enable constraint 'RightArm'.
		Enable constraint 'Head'.
	sequence 'activate_ragdoll_left_arm':
		Run sequence 'swich_for_test'.
		Enable body 'rag_LeftForeArm':
			motion 'dynamic'
		Enable body 'rag_LeftArm':
			motion 'dynamic'
	sequence 'activate_ragdoll_right_arm':
		Run sequence 'swich_for_test'.
		Enable body 'rag_RightForeArm':
			motion 'dynamic'
		Enable body 'rag_RightArm':
			motion 'dynamic'
	sequence 'activate_ragdoll_arms':
		Run sequence 'swich_for_test'.
		Enable body 'rag_RightForeArm':
			motion 'dynamic'
		Enable body 'rag_RightArm':
			motion 'dynamic'
		Enable body 'rag_LeftForeArm':
			motion 'dynamic'
		Enable body 'rag_LeftArm':
			motion 'dynamic'
	sequence 'activate_ragdoll_head':
		Run sequence 'swich_for_test'.
		Enable body 'rag_Head':
			motion 'dynamic'
	sequence 'activate_ragdoll_spine':
		Run sequence 'swich_for_test'.
		Enable body 'rag_Head':
			motion 'dynamic'
		Enable body 'rag_RightForeArm':
			motion 'dynamic'
		Enable body 'rag_RightArm':
			motion 'dynamic'
		Enable body 'rag_LeftForeArm':
			motion 'dynamic'
		Enable body 'rag_LeftArm':
			motion 'dynamic'
		Enable body 'rag_Spine':
			motion 'dynamic'
		Enable body 'rag_Spine1':
			motion 'dynamic'
		Enable body 'rag_Spine2':
			motion 'dynamic'
	sequence 'activate_ragdoll_left_leg':
		Run sequence 'swich_for_test'.
		Enable body 'rag_LeftUpLeg':
			motion 'dynamic'
		Enable body 'rag_LeftLeg':
			motion 'dynamic'
	sequence 'activate_ragdoll_right_leg':
		Run sequence 'swich_for_test'.
		Enable body 'rag_RightUpLeg':
			motion 'dynamic'
		Enable body 'rag_RightLeg':
			motion 'dynamic'
	sequence 'activate_ragdoll_legs':
		Run sequence 'swich_for_test'.
		Enable body 'rag_RightUpLeg':
			motion 'dynamic'
		Enable body 'rag_RightLeg':
			motion 'dynamic'
		Enable body 'rag_LeftUpLeg':
			motion 'dynamic'
		Enable body 'rag_LeftLeg':
			motion 'dynamic'
	sequence 'enable_outline_bain':
		Hide graphic_group 'character'.
		Show graphic_group 'character_outline'.
	sequence 'disable_outline_bain':
		Show graphic_group 'character'.
		Hide graphic_group 'character_outline'.
