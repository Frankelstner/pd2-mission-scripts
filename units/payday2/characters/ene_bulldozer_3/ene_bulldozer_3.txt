unit:
	sequence 'int_seq_helmet_plate':
		TRIGGER TIMES 1
		Disable body 'body_helmet_plate'.
		Disable object 'g_helmet_plate'.
		Disable object 'g_helmet_plate_lod1'.
		Disable decal_mesh 'dm_helmet_plate'.
		Enable body 'body_helmet_glass'.
		Enable object 'g_helmet_glass'.
		Enable object 'g_helmet_glass_lod1'.
		Enable decal_mesh 'dm_helmet_glass'.
		spawn_unit 'units/payday2/characters/ene_acc_bulldozer_helmet_plate_3/ene_acc_bulldozer_helmet_plate_3':
			position object_pos('Head')
			rotation object_rot('Head')
		Play audio 'bulldozer_metal_plate_shatter' at 'Head'.
	sequence 'int_seq_helmet_glass_01':
		TRIGGER TIMES 1
		Disable object 'g_helmet_glass'.
		Disable object 'g_helmet_glass_lod1'.
		Enable object 'g_helmet_glass_crk'.
		Enable object 'g_helmet_glass_crk_lod1'.
	sequence 'int_seq_helmet_glass_02':
		TRIGGER TIMES 1
		Play audio 'bulldozer_visor_shatter' at 'Head'.
		Enable body 'head'.
		Disable body 'body_helmet_glass'.
		Disable body 'body_helmet'.
		Disable object 'g_helmet_glass'.
		Disable object 'g_helmet_glass_lod1'.
		Disable object 'g_helmet_glass_crk'.
		Disable object 'g_helmet_glass_crk_lod1'.
		Disable decal_mesh 'dm_helmet_glass'.
		effect 'effects/particles/bullet_hit/glass_breakable/bullet_hit_glass_breakable':
			damp_time 4
			parent object( 'e_glass' )
			position v()
		effect 'effects/particles/bullet_hit/glass_breakable/bullet_hit_glass_breakable':
			damp_time 5
			parent object( 'e_glass' )
			position v()
		Call function: character_damage.seq_clbk_vizor_shatter() (DELAY 1)
	sequence 'int_seq_armor_chest':
		TRIGGER TIMES 1
		Disable body 'body_armor_chest'.
		Disable object 'g_armor_chest'.
		Disable object 'g_armor_chest_lod1'.
		Disable decal_mesh 'dm_armor_chest'.
		spawn_unit 'units/payday2/characters/ene_acc_bulldozer_chest_2/ene_acc_bulldozer_chest_2':
			position object_pos('Spine1')
			rotation object_rot('Spine1')
		Play audio 'bulldozer_metal_plate_shatter' at 'Spine1'.
	sequence 'int_seq_armor_stomache':
		TRIGGER TIMES 1
		Disable body 'body_armor_stomache'.
		Disable object 'g_armor_stomache'.
		Disable object 'g_armor_stomache_lod1'.
		Disable decal_mesh 'dm_armor_stomache'.
		spawn_unit 'units/payday2/characters/ene_acc_bulldozer_stomache_2/ene_acc_bulldozer_stomache_2':
			position object_pos('Hips')
			rotation object_rot('Hips')
		Play audio 'bulldozer_metal_plate_shatter' at 'Spine1'.
	sequence 'int_seq_armor_back':
		TRIGGER TIMES 1
		Disable body 'body_armor_back'.
		Disable object 'g_armor_back'.
		Disable object 'g_armor_back_lod1'.
		Disable decal_mesh 'dm_armor_back'.
		spawn_unit 'units/payday2/characters/ene_acc_bulldozer_back_2/ene_acc_bulldozer_back_2':
			position object_pos('Spine1')
			rotation object_rot('Spine1')
		Play audio 'bulldozer_metal_plate_shatter' at 'Spine1'.
	sequence 'int_seq_armor_throat':
		TRIGGER TIMES 1
		Disable body 'body_armor_throat'.
		Disable object 'g_throat'.
		Disable object 'g_throat_lod1'.
		spawn_unit 'units/payday2/characters/ene_acc_bulldozer_throat_3/ene_acc_bulldozer_throat_3':
			position object_pos('Spine2')
			rotation object_rot('Spine2')
		Play audio 'bulldozer_collar_plate_shatter' at 'Neck'.
	sequence 'int_seq_armor_neck':
		TRIGGER TIMES 1
		Disable body 'body_armor_neck'.
		Disable object 'g_neck'.
		Disable object 'g_neck_lod1'.
		spawn_unit 'units/payday2/characters/ene_acc_bulldozer_neck_3/ene_acc_bulldozer_neck_3':
			position object_pos('Spine2')
			rotation object_rot('Spine2')
		Play audio 'bulldozer_collar_plate_shatter' at 'Neck'.
	body 'body_helmet_plate'
		Upon receiving 150 damage or 850 explosion damage, execute:
			Run sequence 'int_seq_helmet_plate'.
	body 'body_helmet_glass'
		Upon receiving 80 damage or 750 explosion damage, execute:
			Run sequence 'int_seq_helmet_glass_01'.
		Upon receiving 160 damage or 750 explosion damage, execute:
			Run sequence 'int_seq_helmet_glass_02'.
	body 'body_armor_chest'
		Upon receiving 80 damage or 500 explosion damage, execute:
			Run sequence 'int_seq_armor_chest'.
	body 'body_armor_stomache'
		Upon receiving 80 damage or 500 explosion damage, execute:
			Run sequence 'int_seq_armor_stomache'.
	body 'body_armor_back'
		Upon receiving 80 damage or 500 explosion damage, execute:
			Run sequence 'int_seq_armor_back'.
	body 'body_armor_throat'
		Upon receiving 80 damage or 500 explosion damage, execute:
			Run sequence 'int_seq_armor_throat'.
	body 'body_armor_neck'
		Upon receiving 80 damage or 500 explosion damage, execute:
			Run sequence 'int_seq_armor_neck'.
	sequence 'leg_arm_hitbox':
		Enable body 'body'.
		Enable body 'head'.
		Disable body 'rag_Hips':
			motion 'keyframed'
		Enable body 'rag_LeftUpLeg':
			motion 'keyframed'
		Enable body 'rag_RightUpLeg':
			motion 'keyframed'
		Enable body 'rag_LeftLeg':
			motion 'keyframed'
		Enable body 'rag_RightLeg':
			motion 'keyframed'
		Disable body 'rag_Spine':
			motion 'keyframed'
		Disable body 'rag_Spine1':
			motion 'keyframed'
		Disable body 'rag_Spine2':
			motion 'keyframed'
		Disable body 'rag_LeftForeArm':
			motion 'keyframed'
		Disable body 'rag_RightForeArm':
			motion 'keyframed'
		Disable body 'rag_LeftArm':
			motion 'keyframed'
		Disable body 'rag_RightArm':
			motion 'keyframed'
		Disable body 'rag_Head':
			motion 'keyframed'
		Disable constraint 'LeftUpLeg'.
		Disable constraint 'RightUpLeg'.
		Disable constraint 'LeftLeg'.
		Disable constraint 'RightLeg'.
		Disable constraint 'Spine'.
		Disable constraint 'Spine1'.
		Disable constraint 'Spine2'.
		Disable constraint 'LeftForeArm'.
		Disable constraint 'RightForeArm'.
		Disable constraint 'LeftArm'.
		Disable constraint 'RightArm'.
		Disable constraint 'Head'.
	sequence 'switch_to_ragdoll':
		Disable body 'body'.
		Disable body 'head'.
		Enable body 'rag_Hips':
			motion 'dynamic'
		Enable body 'rag_LeftUpLeg':
			motion 'dynamic'
		Enable body 'rag_RightUpLeg':
			motion 'dynamic'
		Enable body 'rag_LeftLeg':
			motion 'dynamic'
		Enable body 'rag_RightLeg':
			motion 'dynamic'
		Enable body 'rag_Spine':
			motion 'dynamic'
		Enable body 'rag_Spine1':
			motion 'dynamic'
		Enable body 'rag_Spine2':
			motion 'dynamic'
		Enable body 'rag_LeftForeArm':
			motion 'dynamic'
		Enable body 'rag_RightForeArm':
			motion 'dynamic'
		Enable body 'rag_LeftArm':
			motion 'dynamic'
		Enable body 'rag_RightArm':
			motion 'dynamic'
		Enable body 'rag_Head':
			motion 'dynamic'
		Enable constraint 'LeftUpLeg'.
		Enable constraint 'RightUpLeg'.
		Enable constraint 'LeftLeg'.
		Enable constraint 'RightLeg'.
		Enable constraint 'Spine'.
		Enable constraint 'Spine1'.
		Enable constraint 'Spine2'.
		Enable constraint 'LeftForeArm'.
		Enable constraint 'RightForeArm'.
		Enable constraint 'LeftArm'.
		Enable constraint 'RightArm'.
		Enable constraint 'Head'.
	sequence 'freeze_ragdoll':
		body 'rag_Hips':
			motion 'fixed'
		body 'rag_LeftUpLeg':
			motion 'fixed'
		body 'rag_RightUpLeg':
			motion 'fixed'
		body 'rag_LeftLeg':
			motion 'fixed'
		body 'rag_RightLeg':
			motion 'fixed'
		body 'rag_Spine':
			motion 'fixed'
		body 'rag_Spine1':
			motion 'fixed'
		body 'rag_Spine2':
			motion 'fixed'
		body 'rag_LeftForeArm':
			motion 'fixed'
		body 'rag_RightForeArm':
			motion 'fixed'
		body 'rag_LeftArm':
			motion 'fixed'
		body 'rag_RightArm':
			motion 'fixed'
		body 'rag_Head':
			motion 'fixed'
		Disable constraint 'LeftUpLeg'.
		Disable constraint 'RightUpLeg'.
		Disable constraint 'LeftLeg'.
		Disable constraint 'RightLeg'.
		Disable constraint 'Spine'.
		Disable constraint 'Spine1'.
		Disable constraint 'Spine2'.
		Disable constraint 'LeftForeArm'.
		Disable constraint 'RightForeArm'.
		Disable constraint 'LeftArm'.
		Disable constraint 'RightArm'.
		Disable constraint 'Head'.
	sequence 'swich_for_test':
		Disable body 'body'.
		Disable body 'head'.
		Disable body 'mover_blocker'.
		Enable body 'rag_Hips':
			motion 'keyframed'
		Enable body 'rag_LeftUpLeg':
			motion 'keyframed'
		Enable body 'rag_RightUpLeg':
			motion 'keyframed'
		Enable body 'rag_LeftLeg':
			motion 'keyframed'
		Enable body 'rag_RightLeg':
			motion 'keyframed'
		Enable body 'rag_Spine':
			motion 'keyframed'
		Enable body 'rag_Spine1':
			motion 'keyframed'
		Enable body 'rag_Spine2':
			motion 'keyframed'
		Enable body 'rag_LeftForeArm':
			motion 'keyframed'
		Enable body 'rag_RightForeArm':
			motion 'keyframed'
		Enable body 'rag_LeftArm':
			motion 'keyframed'
		Enable body 'rag_RightArm':
			motion 'keyframed'
		Enable body 'rag_Head':
			motion 'keyframed'
		Enable constraint 'LeftUpLeg'.
		Enable constraint 'RightUpLeg'.
		Enable constraint 'LeftLeg'.
		Enable constraint 'RightLeg'.
		Enable constraint 'Spine'.
		Enable constraint 'Spine1'.
		Enable constraint 'Spine2'.
		Enable constraint 'LeftForeArm'.
		Enable constraint 'RightForeArm'.
		Enable constraint 'LeftArm'.
		Enable constraint 'RightArm'.
		Enable constraint 'Head'.
	sequence 'activate_ragdoll_left_arm':
		Run sequence 'swich_for_test'.
		Enable body 'rag_LeftForeArm':
			motion 'dynamic'
		Enable body 'rag_LeftArm':
			motion 'dynamic'
	sequence 'activate_ragdoll_right_arm':
		Run sequence 'swich_for_test'.
		Enable body 'rag_RightForeArm':
			motion 'dynamic'
		Enable body 'rag_RightArm':
			motion 'dynamic'
	sequence 'activate_ragdoll_arms':
		Run sequence 'swich_for_test'.
		Enable body 'rag_RightForeArm':
			motion 'dynamic'
		Enable body 'rag_RightArm':
			motion 'dynamic'
		Enable body 'rag_LeftForeArm':
			motion 'dynamic'
		Enable body 'rag_LeftArm':
			motion 'dynamic'
	sequence 'activate_ragdoll_head':
		Run sequence 'swich_for_test'.
		Enable body 'rag_Head':
			motion 'dynamic'
	sequence 'activate_ragdoll_spine':
		Run sequence 'swich_for_test'.
		Enable body 'rag_Head':
			motion 'dynamic'
		Enable body 'rag_RightForeArm':
			motion 'dynamic'
		Enable body 'rag_RightArm':
			motion 'dynamic'
		Enable body 'rag_LeftForeArm':
			motion 'dynamic'
		Enable body 'rag_LeftArm':
			motion 'dynamic'
		Enable body 'rag_Spine':
			motion 'dynamic'
		Enable body 'rag_Spine1':
			motion 'dynamic'
		Enable body 'rag_Spine2':
			motion 'dynamic'
	sequence 'activate_ragdoll_left_leg':
		Run sequence 'swich_for_test'.
		Enable body 'rag_LeftUpLeg':
			motion 'dynamic'
		Enable body 'rag_LeftLeg':
			motion 'dynamic'
	sequence 'activate_ragdoll_right_leg':
		Run sequence 'swich_for_test'.
		Enable body 'rag_RightUpLeg':
			motion 'dynamic'
		Enable body 'rag_RightLeg':
			motion 'dynamic'
	sequence 'activate_ragdoll_legs':
		Run sequence 'swich_for_test'.
		Enable body 'rag_RightUpLeg':
			motion 'dynamic'
		Enable body 'rag_RightLeg':
			motion 'dynamic'
		Enable body 'rag_LeftUpLeg':
			motion 'dynamic'
		Enable body 'rag_LeftLeg':
			motion 'dynamic'
