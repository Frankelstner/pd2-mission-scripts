unit:
	sequence 'state_normal':
		Enable object 'g_head'.
		Enable object 'g_body'.
		Disable object 'g_hurt1'.
		Disable object 'g_hurt2'.
		Disable object 'g_hurt3'.
		Disable object 'g_body_hurt2'.
		Disable object 'g_body_hurt3'.
	sequence 'state_hurt_1':
		Disable object 'g_head'.
		Enable object 'g_body'.
		Enable object 'g_hurt1'.
		Disable object 'g_hurt2'.
		Disable object 'g_hurt3'.
		Disable object 'g_body_hurt2'.
		Disable object 'g_body_hurt3'.
	sequence 'state_hurt_2':
		Disable object 'g_head'.
		Disable object 'g_body'.
		Disable object 'g_hurt1'.
		Enable object 'g_hurt2'.
		Disable object 'g_hurt3'.
		Enable object 'g_body_hurt2'.
		Disable object 'g_body_hurt3'.
	sequence 'state_hurt_3':
		Disable object 'g_head'.
		Disable object 'g_body'.
		Disable object 'g_hurt1'.
		Disable object 'g_hurt2'.
		Enable object 'g_hurt3'.
		Disable object 'g_body_hurt2'.
		Enable object 'g_body_hurt3'.
	sequence 'state_contour_enable':
		material_config 'units/payday2/characters/civ_male_taxman/civ_male_taxman_contour'.
	sequence 'state_contour_disable':
		material_config 'units/payday2/characters/civ_male_taxman/civ_male_taxman'.
