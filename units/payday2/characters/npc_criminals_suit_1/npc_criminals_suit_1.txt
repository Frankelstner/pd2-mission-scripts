unit:
	armor_state = 1
	shirt_taco = 0
	shirt_snakeskin = 0
	shirt_pink = 0
	body_replace = 0
	sequence 'int_seq_hide_all':
		Disable object 'g_body'.
		Disable object 'g_hands'.
		Disable object 'g_head_chains'.
		Disable object 'g_head_dragan'.
		Disable object 'g_head_dallas'.
		Disable object 'g_head_hoxton'.
		Disable object 'g_head_old_hoxton'.
		Disable object 'g_head_john_wick'.
		Disable object 'g_head_wolf'.
		Disable object 'g_head_sokol'.
		Disable object 'g_sokol_mask_on'.
		Disable object 'g_sokol_mask_off'.
		Disable object 'g_sokol_hair'.
		Disable object 'g_head_jacket'.
		Disable object 'g_body_jacket'.
		Disable object 'g_head_jiro'.
		Disable object 'g_body_jiro'.
		Disable object 'g_body_bodhi'.
		Disable object 'g_head_bodhi'.
		Disable object 'g_head_jimmy'.
		Disable object 'g_body_jimmy'.
		Disable object 'g_jimmy_mask_on'.
		Disable object 'g_jimmy_mask_off'.
		Disable object 'g_head_terry'.
		Disable object 'g_body_terry'.
		Disable object 'g_head_max'.
		Disable object 'g_body_max'.
	sequence 'int_seq_hide_body':
		Disable object 'g_body'.
		Disable object 'g_body_jacket'.
		Disable object 'g_body_jiro'.
		Disable object 'g_body_bodhi'.
		Disable object 'g_body_jimmy'.
		Disable object 'g_body_terry'.
		Disable object 'g_body_max'.
	sequence 'var_mtr_chains':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_chains'.
		If body_replace == 0: Enable object 'g_body'.
		If body_replace == 0: Enable object 'g_hands'.
		Enable object 'g_head_chains'.
	sequence 'var_mtr_chains_cc':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_chains_cc'.
		If body_replace == 0: Enable object 'g_body'.
		If body_replace == 0: Enable object 'g_hands'.
		Enable object 'g_head_chains'.
	sequence 'var_mtr_dallas':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_dallas'.
		If body_replace == 0: Enable object 'g_body'.
		If body_replace == 0: Enable object 'g_hands'.
		Enable object 'g_head_dallas'.
	sequence 'var_mtr_dallas_cc':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_dallas_cc'.
		If body_replace == 0: Enable object 'g_body'.
		If body_replace == 0: Enable object 'g_hands'.
		Enable object 'g_head_dallas'.
	sequence 'var_mtr_hoxton':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_hoxton'.
		If body_replace == 0: Enable object 'g_body'.
		If body_replace == 0: Enable object 'g_hands'.
		Enable object 'g_head_hoxton'.
	sequence 'var_mtr_hoxton_cc':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_hoxton_cc'.
		If body_replace == 0: Enable object 'g_body'.
		If body_replace == 0: Enable object 'g_hands'.
		Enable object 'g_head_hoxton'.
	sequence 'var_mtr_dragan':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_dragan'.
		If body_replace == 0: Enable object 'g_body'.
		If body_replace == 0: Enable object 'g_hands'.
		Enable object 'g_head_dragan'.
	sequence 'var_mtr_dragan_cc':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_dragan_cc'.
		If body_replace == 0: Enable object 'g_body'.
		If body_replace == 0: Enable object 'g_hands'.
		Enable object 'g_head_dragan'.
	sequence 'var_mtr_jacket':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_jacket'.
		Enable object 'g_head_jacket'.
		If body_replace == 0: Enable object 'g_body_jacket'.
	sequence 'var_mtr_jacket_cc':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_jacket_cc'.
		Enable object 'g_head_jacket'.
		If body_replace == 0: Enable object 'g_body_jacket'.
	sequence 'var_mtr_old_hoxton':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_old_hoxton'.
		If body_replace == 0: Enable object 'g_body'.
		If body_replace == 0: Enable object 'g_hands'.
		Enable object 'g_head_old_hoxton'.
	sequence 'var_mtr_old_hoxton_cc':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_old_hoxton_cc'.
		If body_replace == 0: Enable object 'g_body'.
		If body_replace == 0: Enable object 'g_hands'.
		Enable object 'g_head_old_hoxton'.
	sequence 'var_mtr_wolf':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_wolf'.
		If body_replace == 0: Enable object 'g_body'.
		If body_replace == 0: Enable object 'g_hands'.
		Enable object 'g_head_wolf'.
	sequence 'var_mtr_wolf_cc':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_wolf_cc'.
		If body_replace == 0: Enable object 'g_body'.
		If body_replace == 0: Enable object 'g_hands'.
		Enable object 'g_head_wolf'.
	sequence 'var_mtr_john_wick':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_john_wick'.
		If body_replace == 0: Enable object 'g_body'.
		If body_replace == 0: Enable object 'g_hands'.
		Enable object 'g_head_john_wick'.
	sequence 'var_mtr_john_wick_cc':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_john_wick_cc'.
		If body_replace == 0: Enable object 'g_body'.
		If body_replace == 0: Enable object 'g_hands'.
		Enable object 'g_head_john_wick'.
	sequence 'var_mtr_sokol':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_sokol'.
		If body_replace == 0: Enable object 'g_body'.
		If body_replace == 0: Enable object 'g_hands'.
		Enable object 'g_head_sokol'.
		Enable object 'g_sokol_mask_off'.
		Enable object 'g_sokol_hair'.
	sequence 'var_mtr_sokol_cc':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_sokol_cc'.
		If body_replace == 0: Enable object 'g_body'.
		If body_replace == 0: Enable object 'g_hands'.
		Enable object 'g_head_sokol'.
		Enable object 'g_sokol_mask_off'.
		Enable object 'g_sokol_hair'.
	sequence 'var_mtr_jiro':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_jiro'.
		If body_replace == 0: Enable object 'g_hands'.
		Enable object 'g_head_jiro'.
		If body_replace == 0: Enable object 'g_body_jiro'.
	sequence 'var_mtr_jiro_cc':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_jiro_cc'.
		If body_replace == 0: Enable object 'g_hands'.
		Enable object 'g_head_jiro'.
		If body_replace == 0: Enable object 'g_body_jiro'.
	sequence 'var_mtr_bodhi':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_bodhi'.
		Enable object 'g_head_bodhi'.
		If body_replace == 0: Enable object 'g_body_bodhi'.
	sequence 'var_mtr_bodhi_cc':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_bodhi_cc'.
		Enable object 'g_head_bodhi'.
		If body_replace == 0: Enable object 'g_body_bodhi'.
	sequence 'mask_on_sokol':
		Enable object 'g_sokol_mask_on'.
		Disable object 'g_sokol_mask_off'.
		Disable object 'g_sokol_hair'.
	sequence 'mask_off_sokol':
		Disable object 'g_sokol_mask_on'.
		Enable object 'g_sokol_mask_off'.
		Enable object 'g_sokol_hair'.
	sequence 'mask_on_jimmy':
		Enable object 'g_jimmy_mask_on'.
		Disable object 'g_jimmy_mask_off'.
	sequence 'mask_off_jimmy':
		Disable object 'g_jimmy_mask_on'.
		Enable object 'g_jimmy_mask_off'.
	sequence 'var_mtr_jimmy':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_jimmy'.
		If body_replace == 0: Enable object 'g_body_jimmy'.
		Enable object 'g_head_jimmy'.
		Enable object 'g_jimmy_mask_off'.
		Enable object 'g_jimmy_mask_on'.
	sequence 'var_mtr_jimmy_cc':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/payday2/characters/npc_criminals_suit_1/mtr_jimmy_cc'.
		If body_replace == 0: Enable object 'g_body_jimmy'.
		Enable object 'g_head_jimmy'.
		Enable object 'g_jimmy_mask_off'.
		Enable object 'g_jimmy_mask_on'.
	sequence 'var_mtr_terry':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/pd2_dlc_chico/characters/npc_criminals_terry/mtr_criminal_terry'.
		If body_replace == 0: Enable object 'g_body_terry'.
		Enable object 'g_head_terry'.
	sequence 'var_mtr_terry_cc':
		Run sequence 'int_seq_hide_all'.
		material_config 'units/pd2_dlc_chico/characters/npc_criminals_terry/mtr_criminal_terry_cc'.
		If body_replace == 0: Enable object 'g_body_terry'.
		Enable object 'g_head_terry'.
	sequence 'var_mtr_max':
		Run sequence 'int_seq_hide_all'.
		If body_replace == 0: Enable object 'g_body_max'.
		Enable object 'g_head_max'.
	sequence 'var_mtr_max_cc':
		Run sequence 'int_seq_hide_all'.
		If body_replace == 0: Enable object 'g_body_max'.
		Enable object 'g_head_max'.
	sequence 'var_model_01':
		Disable object 'g_vest_small'.
		Disable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
		armor_state = 1
	sequence 'var_model_02':
		Enable object 'g_vest_small'.
		Disable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
		armor_state = 2
	sequence 'var_model_03':
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
		armor_state = 3
	sequence 'var_model_04':
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Enable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
		armor_state = 4
	sequence 'var_model_05':
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Enable object 'g_vest_shoulder'.
		Enable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
		armor_state = 5
	sequence 'var_model_06':
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Enable object 'g_vest_shoulder'.
		Enable object 'g_vest_neck'.
		Enable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
		armor_state = 6
	sequence 'var_model_07':
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Enable object 'g_vest_shoulder'.
		Enable object 'g_vest_neck'.
		Enable object 'g_vest_thies'.
		Enable object 'g_vest_leg_arm'.
		armor_state = 7
	sequence 'minigame_win':
		Call function: interaction.play_minigame_vo('win')
	sequence 'minigame_lose':
		Call function: interaction.play_minigame_vo('lose')
	sequence 'spawn_prop_raincoat':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_glace/characters/glc_acc_raincoat/glc_acc_raincoat')
		Disable object 'g_vest_small'.
		Disable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'despawn_prop_raincoat':
		Call function: spawn_manager.remove_unit('char_mesh')
		If armor_state == 1: Run sequence 'var_model_01'.
		If armor_state == 2: Run sequence 'var_model_02'.
		If armor_state == 3: Run sequence 'var_model_03'.
		If armor_state == 4: Run sequence 'var_model_04'.
		If armor_state == 5: Run sequence 'var_model_05'.
		If armor_state == 6: Run sequence 'var_model_06'.
		If armor_state == 7: Run sequence 'var_model_07'.
	sequence 'spawn_prop_sneak_suit':
		body_replace = 1
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_dah/characters/dah_acc_stealth_suit/dah_acc_stealth_suit')
		Disable object 'g_body'.
		Disable object 'g_body_jacket'.
		Disable object 'g_hands'.
		Disable object 'g_body_jiro'.
		Disable object 'g_body_bodhi'.
		Disable object 'g_body_jimmy'.
		Disable object 'g_vest_small'.
		Disable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'spawn_prop_murky_suit':
		body_replace = 1
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_vit/characters/vit_acc_murky_suit/vit_acc_murky_suit')
		Disable object 'g_body'. (DELAY 5/30)
		Disable object 'g_hands'. (DELAY 5/30)
		Disable object 'g_vest_small'. (DELAY 5/30)
		Disable object 'g_vest_body'. (DELAY 5/30)
		Disable object 'g_vest_shoulder'. (DELAY 5/30)
		Disable object 'g_vest_neck'. (DELAY 5/30)
		Disable object 'g_vest_thies'. (DELAY 5/30)
		Disable object 'g_vest_leg_arm'. (DELAY 5/30)
		Disable object 'g_body'. (DELAY 5/30)
		Disable object 'g_body_jacket'. (DELAY 5/30)
		Disable object 'g_hands'. (DELAY 5/30)
		Disable object 'g_body_jiro'. (DELAY 5/30)
		Disable object 'g_body_bodhi'. (DELAY 5/30)
		Disable object 'g_body_jimmy'. (DELAY 5/30)
		Disable object 'g_vest_small'. (DELAY 5/30)
		Disable object 'g_vest_body'. (DELAY 5/30)
		Disable object 'g_vest_shoulder'. (DELAY 5/30)
		Disable object 'g_vest_neck'. (DELAY 5/30)
		Disable object 'g_vest_thies'. (DELAY 5/30)
		Disable object 'g_vest_leg_arm'. (DELAY 5/30)
	sequence 'despawn_prop_murky_suit':
		body_replace = 0
		Call function: spawn_manager.remove_unit('char_mesh')
		If (dest_unit:parent() or dest_unit):base():character_name() == 'jimmy': Run sequence 'var_mtr_jimmy'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'bodhi': Run sequence 'var_mtr_bodhi'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'dragon': Run sequence 'var_mtr_jiro'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'sokol': Run sequence 'var_mtr_sokol'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'german': Run sequence 'var_mtr_wolf'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'jacket': Run sequence 'var_mtr_jacket'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'dragan': Run sequence 'var_mtr_dragan'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'american': Run sequence 'var_mtr_hoxton'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'russian': Run sequence 'var_mtr_dallas'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'spanish': Run sequence 'var_mtr_chains'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'jowi': Run sequence 'var_mtr_john_wick'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'old_hoxton': Run sequence 'var_mtr_old_hoxton'.
		If armor_state == 1: Run sequence 'var_model_01'.
		If armor_state == 2: Run sequence 'var_model_02'.
		If armor_state == 3: Run sequence 'var_model_03'.
		If armor_state == 4: Run sequence 'var_model_04'.
		If armor_state == 5: Run sequence 'var_model_05'.
		If armor_state == 6: Run sequence 'var_model_06'.
		If armor_state == 7: Run sequence 'var_model_07'.
	sequence 'spawn_prop_scrub':
		body_replace = 1
		If (dest_unit:parent() or dest_unit):base():character_name() == 'russian': Run sequence 'int_seq_nmh_dallas'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'german': Run sequence 'int_seq_nmh_wolf'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'spanish': Run sequence 'int_seq_nmh_chains'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'american': Run sequence 'int_seq_nmh_houston'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'old_hoxton': Run sequence 'int_seq_nmh_hoxton'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'jowi': Run sequence 'int_seq_nmh_john_wick'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'dragan': Run sequence 'int_seq_nmh_dragan'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'dragon': Run sequence 'int_seq_nmh_jiro'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'sokol': Run sequence 'int_seq_nmh_sokol'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'bodhi': Run sequence 'int_seq_nmh_bodhi'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'jacket': Run sequence 'int_seq_nmh_jacket'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'jimmy': Run sequence 'int_seq_nmh_jimmy'.
		Disable object 'g_body'.
		Disable object 'g_body_jacket'.
		Disable object 'g_hands'.
		Disable object 'g_body_jiro'.
		Disable object 'g_body_bodhi'.
		Disable object 'g_body_jimmy'.
		Disable object 'g_vest_small'.
		Disable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'int_seq_nmh_dallas':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_nmh/characters/nmh_acc_scrubs_male/nmh_acc_scrubs_male')
	sequence 'int_seq_nmh_wolf':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_nmh/characters/nmh_acc_scrubs_male_green/nmh_acc_scrubs_male_green')
	sequence 'int_seq_nmh_chains':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_nmh/characters/nmh_acc_scrubs_male_purple/nmh_acc_scrubs_male_purple')
	sequence 'int_seq_nmh_houston':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_nmh/characters/nmh_acc_scrubs_male_darkblue/nmh_acc_scrubs_male_darkblue')
	sequence 'int_seq_nmh_hoxton':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_nmh/characters/nmh_acc_scrubs_male/nmh_acc_scrubs_male')
	sequence 'int_seq_nmh_john_wick':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_nmh/characters/nmh_acc_scrubs_male_green/nmh_acc_scrubs_male_green')
	sequence 'int_seq_nmh_dragan':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_nmh/characters/nmh_acc_scrubs_male_purple/nmh_acc_scrubs_male_purple')
	sequence 'int_seq_nmh_jiro':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_nmh/characters/nmh_acc_scrubs_male_darkblue/nmh_acc_scrubs_male_darkblue')
	sequence 'int_seq_nmh_sokol':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_nmh/characters/nmh_acc_scrubs_male/nmh_acc_scrubs_male')
	sequence 'int_seq_nmh_bodhi':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_nmh/characters/nmh_acc_scrubs_male_green/nmh_acc_scrubs_male_green')
	sequence 'int_seq_nmh_jacket':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_nmh/characters/nmh_acc_scrubs_male_purple/nmh_acc_scrubs_male_purple')
	sequence 'int_seq_nmh_jimmy':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_nmh/characters/nmh_acc_scrubs_male_darkblue/nmh_acc_scrubs_male_darkblue')
	sequence 'spawn_prop_tux':
		body_replace = 1
		If (dest_unit:parent() or dest_unit):base():character_name() == 'russian': Run sequence 'int_seq_dallas'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'german': Run sequence 'int_seq_wolf'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'spanish': Run sequence 'int_seq_chains'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'american': Run sequence 'int_seq_houston'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'old_hoxton': Run sequence 'int_seq_hoxton'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'jowi': Run sequence 'int_seq_john_wick'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'dragan': Run sequence 'int_seq_dragan'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'dragon': Run sequence 'int_seq_jiro'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'sokol': Run sequence 'int_seq_sokol'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'bodhi': Run sequence 'int_seq_bodhi'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'jacket': Run sequence 'int_seq_jacket'.
		If (dest_unit:parent() or dest_unit):base():character_name() == 'jimmy': Run sequence 'int_seq_jimmy'.
		Disable object 'g_body'.
		Disable object 'g_vest_small'.
		Disable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'int_seq_dallas':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_sah/characters/npc_acc_criminal_suit_white_1/npc_acc_criminals_white_tux')
	sequence 'int_seq_wolf':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_sah/characters/npc_acc_criminal_suit_1/npc_acc_criminals_tux')
	sequence 'int_seq_chains':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_sah/characters/npc_acc_criminal_chains_tux/npc_acc_criminal_chains_tux')
	sequence 'int_seq_houston':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_sah/characters/npc_acc_criminal_suit_1/npc_acc_criminals_tux')
	sequence 'int_seq_hoxton':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_sah/characters/npc_acc_criminal_suit_1/npc_acc_criminals_tux')
	sequence 'int_seq_john_wick':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_sah/characters/npc_acc_criminal_john_wick_tux/npc_acc_criminal_john_wick_tux')
	sequence 'int_seq_dragan':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_sah/characters/npc_acc_criminal_suit_1/npc_acc_criminals_tux')
	sequence 'int_seq_jiro':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_sah/characters/npc_acc_criminal_suit_white_1/npc_acc_criminals_white_tux')
	sequence 'int_seq_sokol':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_sah/characters/npc_acc_criminal_suit_1/npc_acc_criminals_tux')
	sequence 'int_seq_bodhi':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_sah/characters/npc_acc_criminal_bodhi_tux/npc_acc_criminal_bodhi_tux')
	sequence 'int_seq_jacket':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_sah/characters/npc_acc_criminal_jacket_tux/npc_acc_criminal_jacket_tux')
	sequence 'int_seq_jimmy':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_sah/characters/npc_acc_criminal_suit_1/npc_acc_criminals_tux')
	sequence 'spawn_prop_winter_suit':
		body_replace = 1
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_wwh/characters/wwh_acc_stealth_suit/wwh_acc_stealth_suit')
		Disable object 'g_body'.
		Disable object 'g_body_jacket'.
		Disable object 'g_hands'.
		Disable object 'g_body_jiro'.
		Disable object 'g_body_bodhi'.
		Disable object 'g_body_jimmy'.
		Disable object 'g_vest_small'.
		Disable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'despawn_prop_winter_suit':
		Call function: spawn_manager.remove_unit('char_mesh')
		If armor_state == 1: Run sequence 'var_model_01'.
		If armor_state == 2: Run sequence 'var_model_02'.
		If armor_state == 3: Run sequence 'var_model_03'.
		If armor_state == 4: Run sequence 'var_model_04'.
		If armor_state == 5: Run sequence 'var_model_05'.
		If armor_state == 6: Run sequence 'var_model_06'.
		If armor_state == 7: Run sequence 'var_model_07'.
