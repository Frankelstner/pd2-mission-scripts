unit:
	sequence 'state_interaction_enabled':
		EXECUTE ON STARTUP
		Enable interaction.
		startup True
	sequence 'state_interaction_disabled':
		Disable interaction.
	sequence 'state_vis_hidden':
		Disable interaction.
		Disable body 'static_body'.
		Disable object 'g_folder'.
	sequence 'state_vis_show':
		Enable body 'static_body'.
		Enable object 'g_folder'.
	sequence 'interact':
		Run sequence 'state_vis_hidden'.
