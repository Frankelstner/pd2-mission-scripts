unit:
	sequence 'smash_glass_01':
		TRIGGER TIMES 1
		Disable object 'g_glass_01'.
		Enable object 'g_glass_dmg_01'.
		Disable body 'body_glass_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_effect_01' )
			position v()
		Play audio 'window_medium_shatter' at 'e_effect_01'.
	sequence 'smash_glass_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_02'.
		Enable object 'g_glass_dmg_02'.
		Disable body 'body_glass_02'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_effect_02' )
			position v()
		Play audio 'window_medium_shatter' at 'e_effect_02'.
	body 'body_glass_01'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'smash_glass_01'.
	body 'body_glass_02'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'smash_glass_02'.
