unit:
	sequence 'lamp_on':
		Enable effect_spawner 'glow_long'.
		Enable object 'g_g'.
		Enable object 'g_g_glow'.
		Disable object 'g_off'.
	sequence 'lamp_off':
		Disable effect_spawner 'glow_long'.
		Disable object 'g_g'.
		Disable object 'g_g_glow'.
		Enable object 'g_off'.
