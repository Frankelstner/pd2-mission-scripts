unit:
	sequence 'lamp_on':
		Enable effect_spawner 'indoor_lamp'.
		Enable object 'g_g'.
		Enable object 'g_g_glow'.
		Disable object 'g_off'.
	sequence 'lamp_off':
		Disable effect_spawner 'indoor_lamp'.
		Disable object 'g_g'.
		Disable object 'g_g_glow'.
		Enable object 'g_off'.
