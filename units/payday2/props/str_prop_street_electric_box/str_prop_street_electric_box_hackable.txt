unit:
	sequence 'interact_enabled':
		Enable interaction.
	sequence 'interact_disabled':
		Disable interaction.
	sequence 'open_door':
		Enable animation_group 'anim_open_door':
			from 0/30
			to 15/30
		Play audio 'generic_door_metal_open' at 'interact'.
	sequence 'interact':
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_effect_01' )
			position v()
		effect 'effects/particles/dest/sparks_lamp_dest': (DELAY 10/30)
			parent object( 'e_effect_02' )
			position v()
	sequence 'state_hide':
		Disable body 'static_body'.
		Disable decal_mesh 'dm_sheet_metal'.
		Disable decal_mesh 'dm_metal'.
		Disable decal_mesh 'dm_concrete'.
		Disable object 'g_g'.
		Disable object 'g_door'.
		Disable object 's_s'.
		Disable object 's_door'.
		Disable interaction.
	sequence 'state_show':
		Enable body 'static_body'.
		Enable decal_mesh 'dm_sheet_metal'.
		Enable decal_mesh 'dm_metal'.
		Enable decal_mesh 'dm_concrete'.
		Enable object 'g_g'.
		Enable object 'g_door'.
		Enable object 's_s'.
		Enable object 's_door'.
