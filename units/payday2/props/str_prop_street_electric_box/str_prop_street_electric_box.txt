unit:
	sequence 'make_dynamic':
		TRIGGER TIMES 1
		Disable body 'static_body'.
		Enable body 'dynamic_body'.
		slot:
			slot 18
	sequence 'car_destructable':
		TRIGGER TIMES 1
		Run sequence 'make_dynamic'.
