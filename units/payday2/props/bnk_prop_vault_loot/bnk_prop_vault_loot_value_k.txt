unit:
	sequence 'enable_interaction':
		EXECUTE ON STARTUP
		Enable interaction.
		startup True
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'state_hide':
		Disable interaction.
		Disable object 'g_g'.
	sequence 'state_show':
		Enable interaction.
		Enable body 'static_body'.
		Enable object 'g_g'.
	sequence 'interact':
		Run sequence 'state_hide'.
	sequence 'money_wrap_1'.
