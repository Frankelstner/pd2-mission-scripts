unit:
	sequence 'broken':
		Disable object 'g_mesh'.
		Enable object 'g_mesh_broken'.
		Disable object 'd_mesh'.
		Enable object 'd_mesh_broken'.
		Play audio 'harp_break' at 'g_mesh'.
	body 'body0'
		Upon receiving 4 bullet hits or 5 melee damage, execute:
			Run sequence 'broken'.
