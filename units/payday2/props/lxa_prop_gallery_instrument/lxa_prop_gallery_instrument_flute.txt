unit:
	sequence 'activate_dynamic':
		Disable body 'static_body'.
		Enable body 'dynamic_body'.
		Play audio 'trombone_break' at 'g_mesh'.
		slot:
			slot 11
	sequence 'shatter_glass_01':
		Disable object 'g_glass_01'.
		Enable object 'g_glass_broken_01'.
		Play audio 'window_small_shatter' at 'g_glass_01'.
		effect 'effects/payday2/particles/window/car_window_small':
			parent  'e_locator_01' 
		Add attention/detection preset 'prop_ene_ntl' to 'e_locator_01' (alarm reason: 'glass').
	sequence 'shatter_glass_02':
		Disable object 'g_glass_02'.
		Enable object 'g_glass_broken_02'.
		Play audio 'window_small_shatter' at 'g_glass_02'.
		effect 'effects/payday2/particles/window/car_window_small':
			parent  'e_locator_02' 
		Add attention/detection preset 'prop_ene_ntl' to 'e_locator_02' (alarm reason: 'glass').
	sequence 'shatter_glass_03':
		Disable object 'g_glass_03'.
		Enable object 'g_glass_broken_03'.
		Play audio 'window_small_shatter' at 'g_glass_03'.
		effect 'effects/payday2/particles/window/car_window_small':
			parent  'e_locator_03' 
		Add attention/detection preset 'prop_ene_ntl' to 'e_locator_03' (alarm reason: 'glass').
	sequence 'shatter_glass_04':
		Disable object 'g_glass_04'.
		Enable object 'g_glass_broken_04'.
		Play audio 'window_small_shatter' at 'g_glass_04'.
		effect 'effects/payday2/particles/window/car_window_small':
			parent  'e_locator_04' 
		Add attention/detection preset 'prop_ene_ntl' to 'e_locator_04' (alarm reason: 'glass').
	sequence 'shatter_glass_05':
		Disable object 'g_glass_05'.
		Enable object 'g_glass_broken_05'.
		Play audio 'window_small_shatter' at 'g_glass_05'.
		effect 'effects/payday2/particles/window/car_window_small':
			parent  'e_locator_05' 
		Add attention/detection preset 'prop_ene_ntl' to 'e_locator_05' (alarm reason: 'glass').
	sequence 'broken_glass_01':
		Disable object 'g_glass_broken_01'.
		Enable object 'g_glass_pieces_01'.
		Disable body 'glass_01'.
		Play audio 'window_small_shatter' at 'g_glass_broken_01'.
		effect 'effects/payday2/particles/window/car_window_small':
			parent  'e_locator_01' 
	sequence 'broken_glass_02':
		Disable object 'g_glass_broken_02'.
		Enable object 'g_glass_pieces_02'.
		Disable body 'glass_02'.
		Play audio 'window_small_shatter' at 'g_glass_broken_02'.
		effect 'effects/payday2/particles/window/car_window_small':
			parent  'e_locator_02' 
	sequence 'broken_glass_03':
		Disable object 'g_glass_broken_03'.
		Enable object 'g_glass_pieces_03'.
		Disable body 'glass_03'.
		Play audio 'window_small_shatter' at 'g_glass_broken_03'.
		effect 'effects/payday2/particles/window/car_window_small':
			parent  'e_locator_03' 
	sequence 'broken_glass_04':
		Disable object 'g_glass_broken_04'.
		Enable object 'g_glass_pieces_04'.
		Disable body 'glass_04'.
		Play audio 'window_small_shatter' at 'g_glass_broken_04'.
		effect 'effects/payday2/particles/window/car_window_small':
			parent  'e_locator_04' 
	sequence 'broken_glass_05':
		Disable object 'g_glass_broken_05'.
		Enable object 'g_glass_pieces_05'.
		Disable body 'glass_05'.
		Play audio 'window_small_shatter' at 'g_glass_broken_05'.
		effect 'effects/payday2/particles/window/car_window_small':
			parent  'e_locator_05' 
	body 'static_body'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'activate_dynamic'.
	body 'glass_01'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'shatter_glass_01'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'broken_glass_01'.
	body 'glass_02'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'shatter_glass_02'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'broken_glass_02'.
	body 'glass_03'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'shatter_glass_03'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'broken_glass_03'.
	body 'glass_04'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'shatter_glass_04'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'broken_glass_04'.
	body 'glass_05'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'shatter_glass_05'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'broken_glass_05'.
