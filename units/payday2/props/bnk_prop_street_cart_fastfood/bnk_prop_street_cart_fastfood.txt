unit:
	sequence 'make_dynamic':
		TRIGGER TIMES 1
		Enable animation_group 'anim':
			from 0
			to 10/30
		Disable body 'static_objects'.
		Disable object 'g_dest_remove'.
		Disable object 's_dest_remove'.
		Enable body 'static_damaged'.
		Disable object 'g_support'.
		Disable object 's_support'.
		Enable object 'g_support_dmg'.
		Enable object 's_support_dmg'.
		Enable object 'g_debris_wheel_01'.
		Enable object 'g_debris_wheel_02'.
		Enable object 's_debris_wheel_01'.
		Enable object 's_debris_wheel_02'.
		Enable body 'debri_wheel_01'.
		Enable body 'debri_wheel_02'.
		Enable body 'dynamic_container_01'.
		Enable body 'dynamic_container_02'.
		Enable body 'dynamic_container_03'.
		Enable body 'dynamic_trashcan'.
		Enable body 'dynamic_sign'.
		Enable body 'dynamic_shelf_01'.
		Enable body 'dynamic_shelf_02'.
		Enable body 'dynamic_shelf_03'.
		Disable object 'g_dyn_debris_01'.
		Disable object 'g_dyn_debris_02'.
		Disable object 'g_dyn_debris_03'.
		Disable object 'g_dyn_debris_04'.
		Disable object 'g_dyn_debris_05'.
		Disable object 'g_dyn_debris_06'.
		Disable object 'g_dyn_debris_07'.
		Disable object 'g_dyn_debris_08'.
		Disable object 'g_dyn_debris_09'.
	body 'static_main_body'
		Upon receiving 30 explosion damage, execute:
			Run sequence 'make_dynamic'.
