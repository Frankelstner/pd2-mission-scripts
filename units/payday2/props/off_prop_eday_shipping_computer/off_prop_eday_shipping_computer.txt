unit:
	int = 0
	database = 0
	sequence 'set_screen_on':
		Disable object 'g_screen_off'.
		Enable object 'g_screen'.
	sequence 'int_seq_scroll':
		Enable object 'g_scrolling'.
		Run sequence 'set_screen_on'.
	sequence 'int_seq_hide_logo':
		Disable object 'g_screen_off'.
		Disable object 'g_logo_01_avalon'.
		Disable object 'g_logo_02_kranich'.
		Disable object 'g_logo_03_omni'.
		Disable object 'g_logo_04_hefty'.
		Disable object 'g_logo_05_tophats'.
		Disable object 'g_logo_06_buffler'.
		Disable object 'g_logo_07_almendia'.
		Disable object 'g_logo_08_ipear'.
		Disable object 'g_logo_09_winder'.
		Disable object 'g_logo_10_falcogini'.
	sequence 'int_seq_hide_region':
		Disable object 'g_screen_off'.
		Disable object 'g_dest_01_washington'.
		Disable object 'g_dest_02_baltimore'.
		Disable object 'g_dest_03_pittsburgh'.
		Disable object 'g_dest_04_qubec'.
		Disable object 'g_dest_05_seattle'.
		Disable object 'g_dest_06_detroit'.
		Disable object 'g_dest_07_newjersey'.
		Disable object 'g_dest_08_newyork'.
		Disable object 'g_dest_09_austin'.
		Disable object 'g_dest_10_longisland'.
	sequence 'state_vis_hide':
		Run sequence 'int_seq_hide_logo'.
		Run sequence 'int_seq_hide_region'.
		Disable object 'g_screen_dmg'.
		Disable object 'g_screen_off'.
		Disable object 'g_screen'.
		Disable body 'static_body'.
		Disable body 'static_screen'.
		Disable decal_mesh 'dm_glass'.
		Hide graphic_group 'grp_computer'.
		Hide graphic_group 'grp_logo'.
		Hide graphic_group 'grp_dest'.
	sequence 'state_vis_show':
		Disable object 'g_screen_off'.
		Enable object 'g_screen'.
		Enable body 'static_body'.
		Enable body 'static_screen'.
		Enable decal_mesh 'dm_glass'.
		Show graphic_group 'grp_computer'.
		Show graphic_group 'grp_logo'.
		Show graphic_group 'grp_dest'.
	sequence 'set_logo_01':
		Run sequence 'int_seq_hide_logo'.
		Enable object 'g_logo_01_avalon'.
		Enable object 'g_screen'.
	sequence 'set_logo_02':
		Run sequence 'int_seq_hide_logo'.
		Enable object 'g_logo_02_kranich'.
		Enable object 'g_screen'.
	sequence 'set_logo_03':
		Run sequence 'int_seq_hide_logo'.
		Enable object 'g_logo_03_omni'.
		Enable object 'g_screen'.
	sequence 'set_logo_04':
		Run sequence 'int_seq_hide_logo'.
		Enable object 'g_logo_04_hefty'.
		Enable object 'g_screen'.
	sequence 'set_logo_05':
		Run sequence 'int_seq_hide_logo'.
		Enable object 'g_logo_05_tophats'.
		Enable object 'g_screen'.
	sequence 'set_logo_06':
		Run sequence 'int_seq_hide_logo'.
		Enable object 'g_logo_06_buffler'.
		Enable object 'g_screen'.
	sequence 'set_logo_07':
		Run sequence 'int_seq_hide_logo'.
		Enable object 'g_logo_07_almendia'.
		Enable object 'g_screen'.
	sequence 'set_logo_08':
		Run sequence 'int_seq_hide_logo'.
		Enable object 'g_logo_08_ipear'.
		Enable object 'g_screen'.
	sequence 'set_logo_09':
		Run sequence 'int_seq_hide_logo'.
		Enable object 'g_logo_09_winder'.
		Enable object 'g_screen'.
	sequence 'set_logo_10':
		Run sequence 'int_seq_hide_logo'.
		Enable object 'g_logo_10_falcogini'.
		Enable object 'g_screen'.
	sequence 'set_region_01':
		Run sequence 'int_seq_hide_region'.
		Enable object 'g_dest_01_washington'.
	sequence 'set_region_02':
		Run sequence 'int_seq_hide_region'.
		Enable object 'g_dest_02_baltimore'.
	sequence 'set_region_03':
		Run sequence 'int_seq_hide_region'.
		Enable object 'g_dest_03_pittsburgh'.
	sequence 'set_region_04':
		Run sequence 'int_seq_hide_region'.
		Enable object 'g_dest_04_qubec'.
	sequence 'set_region_05':
		Run sequence 'int_seq_hide_region'.
		Enable object 'g_dest_05_seattle'.
	sequence 'set_region_06':
		Run sequence 'int_seq_hide_region'.
		Enable object 'g_dest_06_detroit'.
	sequence 'set_region_07':
		Run sequence 'int_seq_hide_region'.
		Enable object 'g_dest_07_newjersey'.
	sequence 'set_region_08':
		Run sequence 'int_seq_hide_region'.
		Enable object 'g_dest_08_newyork'.
	sequence 'set_region_09':
		Run sequence 'int_seq_hide_region'.
		Enable object 'g_dest_09_austin'.
	sequence 'set_region_10':
		Run sequence 'int_seq_hide_region'.
		Enable object 'g_dest_10_longisland'.
	sequence 'starup_seq':
		EXECUTE ON STARTUP
		Call function: timer_gui.set_visible(False)
		startup True
	sequence 'interact'.
	sequence 'int_seq_state_0':
		Call function: timer_gui.start('100')
	sequence 'int_seq_state_1':
		Call function: timer_gui.start('200')
	sequence 'int_seq_state_2':
		Call function: timer_gui.start('300')
	sequence 'int_seq_state_3':
		Call function: timer_gui.start('400')
	sequence 'int_seq_state_4':
		Call function: timer_gui.start('30')
	sequence 'state_interaction_disabled':
		Disable interaction.
	sequence 'state_interaction_enabled':
		Enable interaction.
	sequence 'reset':
		Call function: timer_gui.reset()
		Run sequence 'state_interaction_enabled'.
	sequence 'set_hack_time_state_0':
		int = 0
	sequence 'set_hack_time_state_1':
		int = 1
	sequence 'set_hack_time_state_2':
		int = 2
	sequence 'set_hack_time_state_3':
		int = 3
	sequence 'set_hack_time_state_4':
		int = 4
	sequence 'power_off':
		Call function: timer_gui.set_powered(False)
		Call function: timer_gui.set_visible(False)
		Hide graphic_group 'grp_logo'.
		Hide graphic_group 'grp_dest'.
		Enable object 'g_screen_off'.
		Disable object 'g_screen'.
	sequence 'power_on':
		Call function: timer_gui.set_powered(True)
		Call function: timer_gui.set_visible(True)
		Show graphic_group 'grp_logo'.
		Show graphic_group 'grp_dest'.
		Disable object 'g_screen_off'.
		Enable object 'g_screen'.
	sequence 'state_device_jammed':
		Call function: timer_gui.set_jammed(True)
	sequence 'state_device_resumed':
		Call function: timer_gui.set_jammed(False)
	sequence 'state_device_start':
		Disable object 'g_database1'.
		Disable object 'g_database_all'.
		Call function: timer_gui.set_visible(True)
		If int == 0: Run sequence 'int_seq_state_0'.
		If int == 1: Run sequence 'int_seq_state_1'.
		If int == 2: Run sequence 'int_seq_state_2'.
		If int == 3: Run sequence 'int_seq_state_3'.
		If int == 4: Run sequence 'int_seq_state_4'.
		Add attention/detection preset 'prop_law_scary' (alarm reason: 'computer').
	sequence 'jammed_trigger'.
	sequence 'timer_done':
		Remove attention/detection preset: 'prop_law_scary'
		Enable object 'g_screen'.
		If database == 0: Enable object 'g_database1'.
		If database == 1: Enable object 'g_database_all'.
		Show graphic_group 'grp_logo'.
		Show graphic_group 'grp_dest'.
		Call function: timer_gui.set_visible(False)
		If database == 0: filter = 'check_database_1'
		If database == 0: database = 1
