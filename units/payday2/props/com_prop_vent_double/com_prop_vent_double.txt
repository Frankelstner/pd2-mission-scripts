unit:
	sequence 'release_vent_01':
		TRIGGER TIMES 1
		Disable body 'static_vent_01'.
		Disable object 'g_vent_01'.
		Disable decal_mesh 'dm_sheet_metal_01'.
		Enable body 'debris_01_01'.
		Enable body 'debris_01_02'.
		Enable body 'debris_01_03'.
		Enable body 'debris_01_04'.
		Enable object 'g_debris_01_01'.
		Enable object 'g_debris_01_02'.
		Enable object 'g_debris_01_03'.
		Enable object 'g_debris_01_04'.
		Play audio 'vent_punch_open' at 'g_vent_01'.
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('p_push')),300,5).
	sequence 'release_vent_02':
		TRIGGER TIMES 1
		Disable body 'static_vent_02'.
		Disable object 'g_vent_02'.
		Disable decal_mesh 'dm_sheet_metal_02'.
		Enable body 'debris_02_01'.
		Enable body 'debris_02_02'.
		Enable body 'debris_02_03'.
		Enable body 'debris_02_04'.
		Enable object 'g_debris_02_01'.
		Enable object 'g_debris_02_02'.
		Enable object 'g_debris_02_03'.
		Enable object 'g_debris_02_04'.
		Play audio 'vent_punch_open' at 'g_vent_01'.
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('p_push')):position(),300,5).
	body 'static_vent_01'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'release_vent_01'.
	body 'static_vent_02'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'release_vent_02'.
