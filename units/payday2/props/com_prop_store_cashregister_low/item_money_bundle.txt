unit:
	sequence 'interact_enable':
		EXECUTE ON STARTUP
		Enable interaction.
		Run sequence 'random_'..pick('bundle_1','bundle_2','bundle_1','bundle_2').
		startup True
	sequence 'interact_disable':
		Disable interaction.
	sequence 'no_outline':
		Disable object 'g_outline'.
	sequence 'yes_outline':
		Enable object 'g_outline'.
	sequence 'hidden':
		Disable interaction.
		Disable body 'static_body'.
		Disable object 'g_money_1'.
		Disable object 'g_money_2'.
		Disable object 'g_outline'.
	sequence 'money_wrap_1':
		Run sequence 'hidden'.
	sequence 'interact':
		Run sequence 'hidden'.
	sequence 'random_bundle_1':
		Enable object 'g_money_1'.
		Disable object 'g_money_2'.
	sequence 'random_bundle_2':
		Disable object 'g_money_1'.
		Enable object 'g_money_2'.
