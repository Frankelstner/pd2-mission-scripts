unit:
	sequence 'interact_enable':
		Enable interaction.
	sequence 'interact_disable':
		Disable interaction.
	sequence 'anim_power_on':
		Enable animation_group 'ag_saw_decelerate':
			from 0/30
			speed 0
			to 0/30
		Enable animation_group 'ag_saw_accelerate':
			from 0/30
			to 100/30
		Enable animation_group 'ag_saw_loop': (DELAY 100/30)
			loop True
	sequence 'anim_power_off':
		Disable animation_group 'ag_saw_loop':
			loop True
		Enable animation_group 'ag_saw_decelerate':
			from 0/30
			speed 1
			to 100/30
	sequence 'whole':
		Enable object 'g_sawblade'.
		Disable object 'g_sawblade_broken'.
		Enable object 'g_saw'.
	sequence 'broken':
		Disable object 'g_sawblade'.
		Enable object 'g_sawblade_broken'.
		Enable object 'g_saw'.
	sequence 'interact':
		Run sequence 'whole'.
