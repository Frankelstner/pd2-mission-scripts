unit:
	sequence 'bleed':
		Enable animation_group 'grow'.
		Enable object 'g_blood_01'.
	sequence 'hide':
		Disable object 'g_blood_01'.
