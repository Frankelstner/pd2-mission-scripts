unit:
	sequence 'make_dynamic':
		Disable object 'g_g'.
		Enable object 'g_g_dmg'.
		Disable body 'static_body'.
		Enable body 'dynamic_body'.
		slot:
			slot 11
	body 'static_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'make_dynamic'.
