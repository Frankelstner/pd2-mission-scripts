unit:
	sequence 'anim_lid_open':
		Enable animation_group 'anim_lid':
			from 0/30
			to 12/30
		Play audio 'crash_barrier_open' at 'a_lid'.
	sequence 'anim_lid_close':
		Enable animation_group 'anim_lid':
			from 13/30
			to 26/30
		Play audio 'crash_barrier_close' at 'a_lid'.
	sequence 'state_show':
		Enable body 'lid'.
		Enable body 'base'.
		Enable decal_mesh 'dm_lid'.
		Enable decal_mesh 'dm_base'.
		Enable object 'g_lid'.
		Enable object 'g_base'.
	sequence 'state_hide':
		Disable body 'lid'.
		Disable body 'base'.
		Disable decal_mesh 'dm_lid'.
		Disable decal_mesh 'dm_base'.
		Disable object 'g_lid'.
		Disable object 'g_base'.
