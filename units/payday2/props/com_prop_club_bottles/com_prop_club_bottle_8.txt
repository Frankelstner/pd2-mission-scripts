unit:
	sequence 'destroy':
		TRIGGER TIMES 1
		effect 'effects/particles/dest/green_bottle_dest':
			parent  'g_g' 
		Play audio 'pot_large_shatter' at 'g_g'.
		Disable object 'g_g'.
		Disable body 'bullet_body'.
	body 'bullet_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy'.
