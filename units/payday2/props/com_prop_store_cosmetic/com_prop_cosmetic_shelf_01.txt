unit:
	sequence 'done_hit'.
	sequence 'break_shelf_01':
		Disable body 'static_shelf_01'.
		Disable object 'g_shelf_01'.
		Disable object 'g_g'.
		Enable object 'g_g_dmg'.
		Play audio 'window_small_shatter' at 'g_g'.
		spawn_unit 'units/payday2/props/com_prop_store_cosmetic/debris/com_debris_eyes_coll_a':
			position object_pos('e_spawner_01')
			rotation object_rot('e_spawner_01')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_push_force_01')),40,10).
		Run sequence 'done_hit'.
	sequence 'break_shelf_02':
		Disable body 'static_shelf_02'.
		Disable object 'g_shelf_02'.
		Disable object 'g_g'.
		Enable object 'g_g_dmg'.
		Play audio 'window_small_shatter' at 'g_g'.
		spawn_unit 'units/payday2/props/com_prop_store_cosmetic/debris/com_debris_eyes_coll_a':
			position object_pos('e_spawner_02')
			rotation object_rot('e_spawner_02')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_push_force_02')),40,10).
		Run sequence 'done_hit'.
	sequence 'break_shelf_03':
		Disable body 'static_shelf_03'.
		Disable object 'g_shelf_03'.
		Disable object 'g_g'.
		Enable object 'g_g_dmg'.
		Play audio 'window_small_shatter' at 'g_g'.
		spawn_unit 'units/payday2/props/com_prop_store_cosmetic/debris/com_debris_eyes_coll_a':
			position object_pos('e_spawner_03')
			rotation object_rot('e_spawner_03')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_push_force_03')),40,10).
		Run sequence 'done_hit'.
	sequence 'break_shelf_04':
		Disable body 'static_shelf_04'.
		Disable object 'g_shelf_04'.
		Disable object 'g_g'.
		Enable object 'g_g_dmg'.
		Play audio 'window_small_shatter' at 'g_g'.
		spawn_unit 'units/payday2/props/com_prop_store_cosmetic/debris/com_debris_eyes_coll_a':
			position object_pos('e_spawner_04')
			rotation object_rot('e_spawner_04')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_push_force_04')),40,10).
		Run sequence 'done_hit'.
	body 'static_shelf_01'
		Upon receiving 1 bullet hit or 10 explosion damage or 20 melee damage, execute:
			Run sequence 'break_shelf_01'.
	body 'static_shelf_02'
		Upon receiving 1 bullet hit or 10 explosion damage or 20 melee damage, execute:
			Run sequence 'break_shelf_02'.
	body 'static_shelf_03'
		Upon receiving 1 bullet hit or 10 explosion damage or 20 melee damage, execute:
			Run sequence 'break_shelf_03'.
	body 'static_shelf_04'
		Upon receiving 1 bullet hit or 10 explosion damage or 20 melee damage, execute:
			Run sequence 'break_shelf_04'.
