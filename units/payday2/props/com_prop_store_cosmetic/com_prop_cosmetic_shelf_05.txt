unit:
	sequence 'done_hit'.
	sequence 'break_shelf_01':
		Disable body 'static_shelf_01'.
		Disable object 'g_shelf_01'.
		Play audio 'window_small_shatter' at 'g_shelf_01'.
		spawn_unit 'units/payday2/props/com_prop_store_cosmetic/debris/com_debris_box_coll_a':
			position object_pos('e_spawner_01')
			rotation object_rot('e_spawner_01')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_push_force_01')),100,10).
		Run sequence 'done_hit'.
	sequence 'break_glass':
		Disable body 'static_glass'.
		Disable object 'g_glass'.
		Disable object 'g_g'.
		Enable object 'g_dmg'.
		Play audio 'window_small_shatter' at 'g_g'.
		Run sequence 'done_hit'.
	body 'static_shelf_01'
		Upon receiving 1 bullet hit or 10 explosion damage or 20 melee damage, execute:
			Run sequence 'break_shelf_01'.
	body 'static_glass'
		Upon receiving 1 bullet hit or 10 explosion damage or 20 melee damage, execute:
			Run sequence 'break_glass'.
