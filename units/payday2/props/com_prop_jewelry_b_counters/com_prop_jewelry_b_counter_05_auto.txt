unit:
	spawned_bag = 0
	sequence 'spawn_bagable_items':
		Run sequence 'int_seq_spawn_'..pick('01','02','03','04').
		spawned_bag = 1
	sequence 'spawn_loose_items':
		If spawned_bag == 0: Run sequence 'int_seq_spawn_'..pick('05','06','07').
	sequence 'int_seq_spawn_01':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_box_01':
			position object_pos('sp_spawn_corner')
			rotation object_rot('sp_spawn_corner')
	sequence 'int_seq_spawn_02':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_box_02':
			position object_pos('sp_spawn_corner')
			rotation object_rot('sp_spawn_corner')
	sequence 'int_seq_spawn_03':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_box_03':
			position object_pos('sp_spawn_corner')
			rotation object_rot('sp_spawn_corner')
	sequence 'int_seq_spawn_04':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_box_04':
			position object_pos('sp_spawn_corner')
			rotation object_rot('sp_spawn_corner')
	sequence 'int_seq_spawn_05':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/com_prop_jewelry_bracelet_hanger':
			position object_pos('sp_spawn_middle')
			rotation object_rot('sp_spawn_middle')
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_bracelet':
			position object_pos('sp_spawn_middle')
			rotation object_rot('sp_spawn_middle')
	sequence 'int_seq_spawn_06':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/com_prop_jewelry_display_box_2':
			position object_pos('sp_spawn_middle')
			rotation object_rot('sp_spawn_middle')
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_display_jewel_2_a':
			position object_pos('sp_spawn_middle')
			rotation object_rot('sp_spawn_middle')
	sequence 'int_seq_spawn_07':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/com_prop_jewelry_display_box_2':
			position object_pos('sp_spawn_middle')
			rotation object_rot('sp_spawn_middle')
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_display_jewel_2_b':
			position object_pos('sp_spawn_middle')
			rotation object_rot('sp_spawn_middle')
	sequence 'glass_shatter':
		TRIGGER TIMES 1
		Disable body 'glass_body'.
		effect 'effects/payday2/particles/window/jewelry_counter_1':
			parent object( 'e_glass_01' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_01'.
		Disable object 'g_glass'.
		Disable decal_mesh 'g_glass'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_01' (alarm reason: 'glass').
	body 'glass_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 12.5 saw damage or 5 melee damage, execute:
			Run sequence 'glass_shatter'.
