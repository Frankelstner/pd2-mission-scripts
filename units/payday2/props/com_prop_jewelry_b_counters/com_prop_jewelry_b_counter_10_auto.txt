unit:
	spawned_bag = 0
	sequence 'spawn_bagable_items':
		Run sequence 'int_seq_spawn_bag_v'..pick('1','2').
		spawned_bag = 1
	sequence 'spawn_loose_items':
		If spawned_bag == 0: Run sequence 'int_seq_spawn_'..pick('051','061','071').
		If spawned_bag == 0: Run sequence 'int_seq_spawn_'..pick('052','062','072').
		If spawned_bag == 0: Run sequence 'int_seq_spawn_'..pick('053','063','073').
	sequence 'int_seq_spawn_bag_v1':
		Run sequence 'int_seq_spawn_'..pick('011','021','031','041').
		Run sequence 'int_seq_spawn_'..pick('051','061','071').
	sequence 'int_seq_spawn_bag_v2':
		Run sequence 'int_seq_spawn_'..pick('012','022','032','042').
		Run sequence 'int_seq_spawn_'..pick('053','063','073').
	sequence 'int_seq_spawn_011':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_box_01':
			position object_pos('sp_spawn_corner1')
			rotation object_rot('sp_spawn_corner1')
	sequence 'int_seq_spawn_021':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_box_02':
			position object_pos('sp_spawn_corner1')
			rotation object_rot('sp_spawn_corner1')
	sequence 'int_seq_spawn_031':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_box_03':
			position object_pos('sp_spawn_corner1')
			rotation object_rot('sp_spawn_corner1')
	sequence 'int_seq_spawn_041':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_box_04':
			position object_pos('sp_spawn_corner1')
			rotation object_rot('sp_spawn_corner1')
	sequence 'int_seq_spawn_012':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_box_01':
			position object_pos('sp_spawn_corner2')
			rotation object_rot('sp_spawn_corner2')
	sequence 'int_seq_spawn_022':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_box_02':
			position object_pos('sp_spawn_corner2')
			rotation object_rot('sp_spawn_corner2')
	sequence 'int_seq_spawn_032':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_box_03':
			position object_pos('sp_spawn_corner2')
			rotation object_rot('sp_spawn_corner2')
	sequence 'int_seq_spawn_042':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_box_04':
			position object_pos('sp_spawn_corner2')
			rotation object_rot('sp_spawn_corner2')
	sequence 'int_seq_spawn_051':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/com_prop_jewelry_bracelet_hanger':
			position object_pos('sp_spawn_middle1')
			rotation object_rot('sp_spawn_middle1')
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_bracelet':
			position object_pos('sp_spawn_middle1')
			rotation object_rot('sp_spawn_middle1')
	sequence 'int_seq_spawn_061':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/com_prop_jewelry_display_box_2':
			position object_pos('sp_spawn_middle1')
			rotation object_rot('sp_spawn_middle1')
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_display_jewel_2_a':
			position object_pos('sp_spawn_middle1')
			rotation object_rot('sp_spawn_middle1')
	sequence 'int_seq_spawn_071':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/com_prop_jewelry_display_box_2':
			position object_pos('sp_spawn_middle1')
			rotation object_rot('sp_spawn_middle1')
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_display_jewel_2_b':
			position object_pos('sp_spawn_middle1')
			rotation object_rot('sp_spawn_middle1')
	sequence 'int_seq_spawn_052':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/com_prop_jewelry_bracelet_hanger':
			position object_pos('sp_spawn_middle2')
			rotation object_rot('sp_spawn_middle2')
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_bracelet':
			position object_pos('sp_spawn_middle2')
			rotation object_rot('sp_spawn_middle2')
	sequence 'int_seq_spawn_062':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/com_prop_jewelry_display_box_2':
			position object_pos('sp_spawn_middle2')
			rotation object_rot('sp_spawn_middle2')
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_display_jewel_2_a':
			position object_pos('sp_spawn_middle2')
			rotation object_rot('sp_spawn_middle2')
	sequence 'int_seq_spawn_072':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/com_prop_jewelry_display_box_2':
			position object_pos('sp_spawn_middle2')
			rotation object_rot('sp_spawn_middle2')
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_display_jewel_2_b':
			position object_pos('sp_spawn_middle2')
			rotation object_rot('sp_spawn_middle2')
	sequence 'int_seq_spawn_053':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/com_prop_jewelry_bracelet_hanger':
			position object_pos('sp_spawn_middle3')
			rotation object_rot('sp_spawn_middle3')
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_bracelet':
			position object_pos('sp_spawn_middle3')
			rotation object_rot('sp_spawn_middle3')
	sequence 'int_seq_spawn_063':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/com_prop_jewelry_display_box_2':
			position object_pos('sp_spawn_middle3')
			rotation object_rot('sp_spawn_middle3')
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_display_jewel_2_a':
			position object_pos('sp_spawn_middle3')
			rotation object_rot('sp_spawn_middle3')
	sequence 'int_seq_spawn_073':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/com_prop_jewelry_display_box_2':
			position object_pos('sp_spawn_middle3')
			rotation object_rot('sp_spawn_middle3')
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/spawn_prop_jewelry_display_jewel_2_b':
			position object_pos('sp_spawn_middle3')
			rotation object_rot('sp_spawn_middle3')
	sequence 'glass_shatter':
		TRIGGER TIMES 1
		Disable body 'glass_body'.
		effect 'effects/payday2/particles/window/jewelry_counter_1':
			parent object( 'e_glass_01' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_01'.
		Disable object 'g_glass'.
		Disable decal_mesh 'g_glass'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_01' (alarm reason: 'glass').
	body 'glass_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'glass_shatter'.
