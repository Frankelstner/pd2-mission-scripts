unit:
	body 'body_static'
		Upon receiving 4 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_bullet_hit'.
	sequence 'light_on':
		Enable light 'li_lamp'.
	sequence 'light_off':
		Disable light 'li_lamp'.
	sequence 'int_seq_bullet_hit':
		TRIGGER TIMES 1
		Disable light 'li_lamp'.
		Play audio 'light_bulb_smash' at 'e_effect'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_effect' )
			position v()
