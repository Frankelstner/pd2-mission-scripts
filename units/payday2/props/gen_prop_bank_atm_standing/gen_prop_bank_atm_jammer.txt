unit:
	sequence 'interact':
		Enable object 'g_glow_func1_green'.
		Enable object 'g_ecm'.
		Hide graphic_group 'ecm_icon'.
		Run sequence 'complete'.
	sequence 'complete':
		Disable interaction.
