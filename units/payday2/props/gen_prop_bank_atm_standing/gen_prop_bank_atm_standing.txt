unit:
	secure = 0
	sequence 'harvest':
		material_config 'units/payday2/props/gen_prop_bank_atm_standing/gen_prop_bank_atm_standing'.
	sequence 'capitol':
		material_config 'units/payday2/props/gen_prop_bank_atm_standing/gen_prop_bank_atm_standing_v2'.
	sequence 'generic':
		material_config 'units/payday2/props/gen_prop_bank_atm_standing/gen_prop_bank_atm_standing_v3'.
	sequence 'open_door':
		Play audio 'atm_open' at 'anim_upper_door'.
		Enable animation_group 'door':
			end_time 70/30
			time 0/30
		Enable animation_group 'upper_door':
			end_time 70/30
			time 0/30
		Enable body 'security_br'.
		Enable body 'security_bl'.
		Enable body 'security_ur'.
		Enable body 'security_ul'.
		Disable body 'first_lock'.
		Hide graphic_group 'sawicongroup'.
		Show graphic_group 'iconbr'.
		Show graphic_group 'iconbl'.
		Show graphic_group 'iconur'.
		Show graphic_group 'iconul'.
		Run sequence 'deactivate_door'.
	body 'first_lock'
		Upon receiving 37.5 saw damage, execute:
			Run sequence 'open_door'.
	body 'security_br'
		Upon receiving 12.5 saw damage, execute:
			Run sequence 'unlock_security_br'.
	body 'security_bl'
		Upon receiving 12.5 saw damage, execute:
			Run sequence 'unlock_security_bl'.
	body 'security_ur'
		Upon receiving 12.5 saw damage, execute:
			Run sequence 'unlock_security_ur'.
	body 'security_ul'
		Upon receiving 12.5 saw damage, execute:
			Run sequence 'unlock_security_ul'.
	sequence 'unlock_security_br':
		secure = vars.var_secure + 1
		Disable object 'g_security_br'.
		Enable object 'g_security_br_broken'.
		Disable body 'security_br'.
		If secure == 4: Run sequence 'security_door_fall'.
		Hide graphic_group 'iconbr'.
	sequence 'unlock_security_bl':
		secure = vars.var_secure + 1
		Disable object 'g_security_bl'.
		Enable object 'g_security_bl_broken'.
		Disable body 'security_bl'.
		If secure == 4: Run sequence 'security_door_fall'.
		Hide graphic_group 'iconbl'.
	sequence 'unlock_security_ur':
		secure = vars.var_secure + 1
		Disable object 'g_security_ur'.
		Enable object 'g_security_ur_broken'.
		Disable body 'security_ur'.
		If secure == 4: Run sequence 'security_door_fall'.
		Hide graphic_group 'iconur'.
	sequence 'unlock_security_ul':
		secure = vars.var_secure + 1
		Disable object 'g_security_ul'.
		Enable object 'g_security_ul_broken'.
		Disable body 'security_ul'.
		If secure == 4: Run sequence 'security_door_fall'.
		Hide graphic_group 'iconul'.
	sequence 'security_door_fall':
		TRIGGER TIMES 1
		Enable body 'security_door'.
		Disable object 'g_money_box_1'.
		Disable object 'g_money_box_2'.
		Disable object 'g_money_box_3'.
		Disable object 'g_money_box_4'.
		spawn_unit 'units/payday2/props/gen_prop_bank_atm_standing/gen_prop_bank_atm_standing_spawn':
			position object_pos('rp_atm_standing')
			rotation object_rot('rp_atm_standing')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('a_security') ),30,30).
		Disable object 'g_security_ul_broken'.
		Disable object 'g_security_ur_broken'.
		Disable object 'g_security_bl_broken'.
		Disable object 'g_security_br_broken'.
		Disable object 'g_security_ul'.
		Disable object 'g_security_ur'.
		Disable object 'g_security_bl'.
		Disable object 'g_security_br'.
		Hide graphic_group 'sawicongroup'.
	sequence 'activate_door':
		Call function: base.activate()
	sequence 'deactivate_door':
		Call function: base.deactivate()
	sequence 'open_door_ecm':
		Run sequence 'open_door'.
		Disable body 'security_br'.
		Disable body 'security_bl'.
		Disable body 'security_ur'.
		Disable body 'security_ul'.
		Run sequence 'unlock_security_br'.
		Run sequence 'unlock_security_bl'.
		Run sequence 'unlock_security_ur'.
		Run sequence 'unlock_security_ul'.
		Run sequence 'security_door_fall'. (DELAY 2)
		Disable object 'g_security_door'.
	sequence 'door_opened'.
	sequence 'door_closed'.
	sequence 'ecm_placed'.
	sequence 'all_ecm_placed'.
	sequence 'ecm_completed'.
