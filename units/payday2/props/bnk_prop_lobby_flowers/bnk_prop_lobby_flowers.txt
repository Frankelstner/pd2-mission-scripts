unit:
	body 'static_body'
		Upon receiving 4 bullet hits or 10 melee damage, execute:
			Run sequence 'int_seq_bullet_hit'.
		Upon receiving 10 explosion damage, execute:
			Run sequence 'int_seq_explosion'.
	sequence 'int_seq_bullet_hit':
		Disable body 'static_body'.
		Hide graphic_group 'grp_flowers'.
		Play audio 'pot_large_shatter' at 'e_pos'.
		effect 'effects/payday2/particles/destruction/dest_plant_matter':
			parent object( 'e_pos' )
			position v()
		spawn_unit 'units/payday2/props/bnk_prop_lobby_flowers/spawn_debris_lobby_flowers':
			position object_pos('rp_bnk_prop_lobby_flowers')
			rotation object_rot('rp_bnk_prop_lobby_flowers')
	sequence 'int_seq_explosion':
		Disable body 'static_body'.
		Hide graphic_group 'grp_flowers'.
		Play audio 'pot_large_shatter' at 'e_pos'.
		effect 'effects/payday2/particles/destruction/dest_plant_matter':
			parent object( 'e_pos' )
			position v()
		spawn_unit 'units/payday2/props/bnk_prop_lobby_flowers/spawn_debris_lobby_flowers':
			position object_pos('rp_bnk_prop_lobby_flowers')
			rotation object_rot('rp_bnk_prop_lobby_flowers')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('e_pos') ),40,40).
