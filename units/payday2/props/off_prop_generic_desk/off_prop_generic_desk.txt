unit:
	sequence 'grey':
		material_config 'units/payday2/props/off_prop_generic_desk/off_prop_generic_desk_a'.
	sequence 'black':
		material_config 'units/payday2/props/off_prop_generic_desk/off_prop_generic_desk_b'.
	sequence 'cream':
		material_config 'units/payday2/props/off_prop_generic_desk/off_prop_generic_desk_c'.
	sequence 'blue':
		material_config 'units/payday2/props/off_prop_generic_desk/off_prop_generic_desk_d'.
	sequence 'white':
		material_config 'units/payday2/props/off_prop_generic_desk/off_prop_generic_desk_e'.
