unit:
	sequence 'open':
		Enable animation_group 'anim':
			from 35/30
			to 65/30
		Play audio 'security_door_01' at 'a_door'.
		Disable body 'body_hitbox'.
	sequence 'close':
		Enable animation_group 'anim':
			from 0
			to 30/30
		Play audio 'security_door_01' at 'a_door'.
		Enable body 'body_hitbox'.
	sequence 'saw'.
