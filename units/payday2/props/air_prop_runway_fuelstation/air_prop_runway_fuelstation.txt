unit:
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'hide':
		Disable body 'body_fuelstation'.
		Disable object 'g_air_prop_runway_fuelstation'.
	sequence 'show':
		Enable body 'body_fuelstation'.
		Enable object 'g_air_prop_runway_fuelstation'.
	sequence 'interact':
		Run sequence 'done_interacted'.
	sequence 'done_interacted'.
