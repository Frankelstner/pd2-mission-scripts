unit:
	sequence 'destroy':
		TRIGGER TIMES 1
		Disable object 'g_lounge_tableglass'.
		Enable object 'g_lounge_tableglassbroken'.
		Disable decal_mesh 'dm_glass'.
		Disable body 'glass'.
		Play audio 'window_small_shatter' at 'e'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e')
			position v()
		Cause alert with 12 m radius.
	body 'glass'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy'.
