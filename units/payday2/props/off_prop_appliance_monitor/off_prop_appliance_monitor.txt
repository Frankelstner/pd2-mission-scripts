unit:
	sequence 'on':
		Enable object 'g_screen_on'.
		Disable object 'g_screen_off'.
	sequence 'off':
		Disable object 'g_screen_on'.
		Enable object 'g_screen_off'.
	sequence 'int_seq_dynamic_monitor':
		Enable body 'dynamic_monitor'.
		Disable body 'hitbox_monitors'.
		Disable object 'g_screen_on'.
		Disable object 'g_screen_off'.
		Enable object 'g_screen_broken'.
		Disable decal_mesh 'dm_dm'.
		Play audio 'ceiling_lamp_break' at 'rp_off_prop_appliance_monitor'.
		slot:
			slot 11
	body 'hitbox_monitors'
		Upon receiving 1 bullet hit or 10 explosion damage or 2.5 saw damage or 10 melee damage, execute:
			Run sequence 'int_seq_dynamic_monitor'.
