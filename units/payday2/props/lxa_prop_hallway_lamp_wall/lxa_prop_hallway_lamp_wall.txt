unit:
	sequence 'break':
		Play audio 'light_bulb_smash' at 'g_g'.
		Disable object 'g_g'.
		Disable object 'g_streak'.
		Enable object 'g_dmg'.
		Disable light 'lo_omni'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent  'e_pos' 
	sequence 'light_off':
		Disable light 'lo_omni'.
	sequence 'light_on':
		Enable light 'lo_omni'.
	body 'static_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break'.
