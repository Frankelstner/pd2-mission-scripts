unit:
	state = 0
	sequence 'open':
		If state == 0: 
			Enable animation_group 'gate':
				from 1/30
				speed 1
				to 325/30
			state = 1
	sequence 'close':
		If state == 1: 
			Enable animation_group 'gate':
				from 325/30
				speed -1
				to 1/30
			state = 0
	sequence 'state_open':
		If state == 0: 
			Enable animation_group 'gate':
				from 325/30
				speed 0
				to 325/30
			state = 1
	sequence 'state_closed':
		If state == 1: 
			Enable animation_group 'gate':
				from 1/30
				speed 0
				to 1/30
			state = 0
