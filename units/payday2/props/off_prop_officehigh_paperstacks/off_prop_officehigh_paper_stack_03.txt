unit:
	sequence 'destroy_papers_01':
		TRIGGER TIMES 1
		Disable object 'g_mesh_01'.
		effect 'effects/payday2/particles/destruction/des_paper':
			parent object( 'e_position' )
			position v()
		Disable body 'body0'.
		Play audio 'paper_hit' at 'e_position'.
	sequence 'spawn_papers':
		TRIGGER TIMES 1
		effect 'effects/payday2/particles/destruction/des_paper':
			parent object( 'e_position' )
			position v()
		Play audio 'paper_hit' at 'e_position'.
	body 'body0'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'spawn_papers'.
		Upon receiving 2 bullet hits or 10 explosion damage or 20 melee damage, execute:
			Run sequence 'destroy_papers_01'.
