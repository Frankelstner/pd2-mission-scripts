unit:
	sequence 'make_dynamic':
		TRIGGER TIMES 1
		Disable body 'static_body'.
		Enable body 'dynamic_body'.
		slot:
			slot 11
	sequence 'destroy_01':
		TRIGGER TIMES 1
		Disable object 'g_sign_road'.
		Disable decal_mesh 'dm_sign_road'.
		Disable object 's_s'.
		Disable body 'static_body'.
		Disable body 'dynamic_body'.
		spawn_unit 'units/payday2/props/str_prop_construction_sign_road/str_prop_construction_sign_road_debris':
			position object_pos('e_debris_spawn')
			rotation object_rot('e_debris_spawn')
			transfer_velocity velocity*0.8
		Play audio 'car_hit_wooden_barrier' at 'e_debris_spawn'.
		slot:
			slot 18
	body 'dynamic_body'
		Upon receiving 1 bullet hit or 10 collision damage or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_01'.
	body 'static_body'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'make_dynamic'.
		Upon receiving 10 explosion damage, execute:
			Run sequence 'destroy_01'.
	sequence 'car_destructable':
		TRIGGER TIMES 1
		Run sequence 'make_dynamic'.
