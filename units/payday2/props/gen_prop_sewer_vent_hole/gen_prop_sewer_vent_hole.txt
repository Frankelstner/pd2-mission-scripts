unit:
	secure = 0
	sequence 'open_door':
		Enable animation_group 'door':
			end_time 20/20
			time 0/20
		Disable body 'padlock_l'.
		Disable body 'padlock_r'.
	body 'padlock_l'
		Upon receiving 25 saw damage, execute:
			Run sequence 'unlock_padlock_l'.
	body 'padlock_r'
		Upon receiving 25 saw damage, execute:
			Run sequence 'unlock_padlock_r'.
	sequence 'unlock_padlock_l':
		secure = vars.var_secure + 1
		Disable object 'g_padlock_2'.
		Disable body 'padlock_l'.
		If secure == 2: Run sequence 'open_door'.
		Hide graphic_group 'icon'.
	sequence 'unlock_padlock_r':
		secure = vars.var_secure + 1
		Disable object 'g_padlock_1'.
		Disable body 'padlock_r'.
		If secure == 2: Run sequence 'open_door'.
		Hide graphic_group 'icon_2'.
