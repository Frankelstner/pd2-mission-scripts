unit:
	sequence 'var_set_color_white_front':
		material_config 'units/payday2/props/gen_prop_container_murky/gen_prop_container_murky_2'.
		Enable object 'g_door_01'.
		Disable object 'g_door_02'.
		Disable object 'g_door_03'.
		Enable object 'g_door_01_logo_front'.
		Disable object 'g_door_01_logo_rear'.
		Disable object 'g_door_02_logo_front'.
		Disable object 'g_door_02_logo_rear'.
		Disable object 'g_door_03_logo_front'.
		Disable object 'g_door_03_logo_rear'.
	sequence 'var_set_color_white_rear':
		material_config 'units/payday2/props/gen_prop_container_murky/gen_prop_container_murky_2'.
		Enable object 'g_door_01'.
		Disable object 'g_door_02'.
		Disable object 'g_door_03'.
		Disable object 'g_door_01_logo_front'.
		Enable object 'g_door_01_logo_rear'.
		Disable object 'g_door_02_logo_front'.
		Disable object 'g_door_02_logo_rear'.
		Disable object 'g_door_03_logo_front'.
		Disable object 'g_door_03_logo_rear'.
	sequence 'var_set_color_red_front':
		material_config 'units/payday2/props/gen_prop_container_murky/gen_prop_container_murky_1'.
		Disable object 'g_door_01'.
		Enable object 'g_door_02'.
		Disable object 'g_door_03'.
		Disable object 'g_door_01_logo_front'.
		Disable object 'g_door_01_logo_rear'.
		Enable object 'g_door_02_logo_front'.
		Disable object 'g_door_02_logo_rear'.
		Disable object 'g_door_03_logo_front'.
		Disable object 'g_door_03_logo_rear'.
	sequence 'var_set_color_red_rear':
		material_config 'units/payday2/props/gen_prop_container_murky/gen_prop_container_murky_1'.
		Disable object 'g_door_01'.
		Enable object 'g_door_02'.
		Disable object 'g_door_03'.
		Disable object 'g_door_01_logo_front'.
		Disable object 'g_door_01_logo_rear'.
		Disable object 'g_door_02_logo_front'.
		Enable object 'g_door_02_logo_rear'.
		Disable object 'g_door_03_logo_front'.
		Disable object 'g_door_03_logo_rear'.
	sequence 'var_set_color_green_front':
		material_config 'units/payday2/props/gen_prop_container_murky/gen_prop_container_murky_2'.
		Disable object 'g_door_01'.
		Enable object 'g_door_02'.
		Disable object 'g_door_03'.
		Disable object 'g_door_01_logo_front'.
		Disable object 'g_door_01_logo_rear'.
		Enable object 'g_door_02_logo_front'.
		Disable object 'g_door_02_logo_rear'.
		Disable object 'g_door_03_logo_front'.
		Disable object 'g_door_03_logo_rear'.
	sequence 'var_set_color_green_rear':
		material_config 'units/payday2/props/gen_prop_container_murky/gen_prop_container_murky_2'.
		Disable object 'g_door_01'.
		Enable object 'g_door_02'.
		Disable object 'g_door_03'.
		Disable object 'g_door_01_logo_front'.
		Disable object 'g_door_01_logo_rear'.
		Disable object 'g_door_02_logo_front'.
		Enable object 'g_door_02_logo_rear'.
		Disable object 'g_door_03_logo_front'.
		Disable object 'g_door_03_logo_rear'.
	sequence 'var_set_color_blue_front':
		material_config 'units/payday2/props/gen_prop_container_murky/gen_prop_container_murky_2'.
		Disable object 'g_door_01'.
		Disable object 'g_door_02'.
		Enable object 'g_door_03'.
		Disable object 'g_door_01_logo_front'.
		Disable object 'g_door_01_logo_rear'.
		Disable object 'g_door_02_logo_front'.
		Disable object 'g_door_02_logo_rear'.
		Enable object 'g_door_03_logo_front'.
		Disable object 'g_door_03_logo_rear'.
	sequence 'var_set_color_blue_rear':
		material_config 'units/payday2/props/gen_prop_container_murky/gen_prop_container_murky_2'.
		Disable object 'g_door_01'.
		Disable object 'g_door_02'.
		Enable object 'g_door_03'.
		Disable object 'g_door_01_logo_front'.
		Disable object 'g_door_01_logo_rear'.
		Disable object 'g_door_02_logo_front'.
		Disable object 'g_door_02_logo_rear'.
		Disable object 'g_door_03_logo_front'.
		Enable object 'g_door_03_logo_rear'.
	sequence 'var_set_color_yellow_front':
		material_config 'units/payday2/props/gen_prop_container_murky/gen_prop_container_murky_1'.
		Disable object 'g_door_01'.
		Disable object 'g_door_02'.
		Enable object 'g_door_03'.
		Disable object 'g_door_01_logo_front'.
		Disable object 'g_door_01_logo_rear'.
		Disable object 'g_door_02_logo_front'.
		Disable object 'g_door_02_logo_rear'.
		Enable object 'g_door_03_logo_front'.
		Disable object 'g_door_03_logo_rear'.
	sequence 'var_set_color_yellow_rear':
		material_config 'units/payday2/props/gen_prop_container_murky/gen_prop_container_murky_1'.
		Disable object 'g_door_01'.
		Disable object 'g_door_02'.
		Enable object 'g_door_03'.
		Disable object 'g_door_01_logo_front'.
		Disable object 'g_door_01_logo_rear'.
		Disable object 'g_door_02_logo_front'.
		Disable object 'g_door_02_logo_rear'.
		Disable object 'g_door_03_logo_front'.
		Enable object 'g_door_03_logo_rear'.
	sequence 'state_interaction_enabled':
		Enable interaction.
		Show graphic_group 'icon_grp'.
	sequence 'state_interaction_disabled':
		Disable interaction.
		Hide graphic_group 'icon_grp'.
	sequence 'state_door_open':
		Enable animation_group 'anim_door_back':
			from 60/30
			speed 0
			to 60/30
		Hide graphic_group 'icon_grp'.
	sequence 'state_door_closed':
		Enable animation_group 'anim_door_back':
			from 0/30
			speed 0
			to 0/30
	sequence 'state_vis_hide':
		Hide graphic_group 'grp_container'.
		Hide graphic_group 'icon_grp'.
		Disable body 'body_door_back_right'.
		Disable body 'body_door_back_left'.
		Disable decal_mesh 'dm_lr_door'.
		Disable decal_mesh 'dm_rr_door'.
	sequence 'state_vis_show':
		Show graphic_group 'grp_container'.
		Enable body 'body_door_back_right'.
		Enable body 'body_door_back_left'.
		Enable decal_mesh 'dm_lr_door'.
		Enable decal_mesh 'dm_rr_door'.
	sequence 'anim_door_open':
		Enable animation_group 'anim_door_back':
			from 0/30
			to 60/30
		Play audio 'container_open' at 'lr_door'.
		Hide graphic_group 'icon_grp'.
	sequence 'anim_door_close':
		Enable animation_group 'anim_door_back':
			from 60/30
			speed -1
			to 0/30
		Play audio 'container_close' at 'lr_door'.
		Play audio 'container_close' at 'rr_door'.
	sequence 'interact':
		Run sequence 'anim_door_open'.
