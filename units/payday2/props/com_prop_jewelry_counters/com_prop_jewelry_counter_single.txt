unit:
	sequence 'glass_shatter':
		TRIGGER TIMES 1
		Disable body 'glass_body'.
		effect 'effects/payday2/particles/window/jewelry_counter_1':
			parent object( 'e_glass_01' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_01'.
		Disable object 'g_glass'.
		Disable decal_mesh 'g_glass'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_01' (alarm reason: 'glass').
	body 'glass_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'glass_shatter'.
