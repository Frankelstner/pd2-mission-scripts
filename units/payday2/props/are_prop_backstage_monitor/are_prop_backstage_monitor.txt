unit:
	sequence 'girl'.
	sequence 'hallway_lvl1':
		material_config 'units/payday2/props/are_prop_backstage_monitor/are_prop_backstage_monitor_2'.
	sequence 'hallway_lvl2':
		material_config 'units/payday2/props/are_prop_backstage_monitor/are_prop_backstage_monitor_3'.
	sequence 'hallway_lvl3':
		material_config 'units/payday2/props/are_prop_backstage_monitor/are_prop_backstage_monitor_4'.
	sequence 'arena_monitor_1':
		material_config 'units/payday2/props/are_prop_backstage_monitor/are_prop_backstage_monitor_5'.
	sequence 'arena_monitor_2':
		material_config 'units/payday2/props/are_prop_backstage_monitor/are_prop_backstage_monitor_6'.
	sequence 'arena_monitor_3':
		material_config 'units/payday2/props/are_prop_backstage_monitor/are_prop_backstage_monitor_7'.
	sequence 'arena_monitor_4':
		material_config 'units/payday2/props/are_prop_backstage_monitor/are_prop_backstage_monitor_8'.
	sequence 'state_screen_off':
		Disable object 'g_screen'.
		Play audio 'tv_switch_off' at 'snd'.
	sequence 'state_screen_on':
		Enable object 'g_screen'.
		Play audio 'tv_switch_on' at 'snd'.
	sequence 'state_interaction_enable':
		Enable interaction.
	sequence 'state_interaction_disable':
		Disable interaction.
	sequence 'interact'.
