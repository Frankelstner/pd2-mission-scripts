unit:
	sequence 'close_shutters':
		Enable body 'shutter_body'. (DELAY 10/30)
		Enable decal_mesh 'dm_metal_shutter'.
		Enable animation_group 'shutter':
			from 1/30
			speed 1
			to 40/30
	sequence 'open_shutters':
		Disable body 'shutter_body'. (DELAY 30/30)
		Disable decal_mesh 'dm_metal_shutter'. (DELAY 40/30)
		Enable animation_group 'shutter':
			from 40/30
			speed -1
			to 0/30
