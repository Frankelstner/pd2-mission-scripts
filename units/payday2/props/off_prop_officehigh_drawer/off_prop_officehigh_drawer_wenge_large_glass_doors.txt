unit:
	sequence 'glass_shatter_01':
		TRIGGER TIMES 1
		Disable body 'body_glass_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_01' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_01'.
		Disable object 'g_glass_01'.
		Disable object 'g_handle_01'.
		Disable decal_mesh 'g_glass_01'.
	sequence 'glass_shatter_02':
		TRIGGER TIMES 1
		Disable body 'body_glass_02'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_02' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_02'.
		Disable object 'g_glass_02'.
		Disable object 'g_handle_02'.
		Disable decal_mesh 'g_glass_02'.
	body 'body_glass_01'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'glass_shatter_01'.
	body 'body_glass_02'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'glass_shatter_02'.
