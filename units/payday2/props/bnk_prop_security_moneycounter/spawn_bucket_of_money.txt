unit:
	hidden = 0
	sequence 'enable_interaction':
		EXECUTE ON STARTUP
		If hidden == 0: 
			Enable interaction.
			startup True
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'hide':
		Disable interaction.
		Disable object 'g_money'.
		hidden = 1
	sequence 'show':
		Enable object 'g_money'.
		hidden = 0
	sequence 'show_and_enable_interaction':
		Enable interaction.
		Enable object 'g_money'.
		hidden = 0
	sequence 'interact':
		Run sequence 'hide'.
		effect 'effects/payday2/particles/grab/grab_money':
			parent object( 'interact' )
			position v()
	sequence 'load'.
