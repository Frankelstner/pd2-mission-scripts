unit:
	sequence 'spawn_box_01':
		Disable body 'stc_box_01'.
		Disable object 'g_g_01'.
		spawn_unit 'units/payday2/props/com_prop_shop_pear/debris/com_debris_pear_headphones_box':
			position object_pos('e_spawner_01')
			rotation object_rot('e_spawner_01')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_spawner_01')),100,10).
	sequence 'spawn_box_02':
		Disable body 'stc_box_02'.
		Disable object 'g_g_02'.
		spawn_unit 'units/payday2/props/com_prop_shop_pear/debris/com_debris_pear_headphones_box':
			position object_pos('e_spawner_02')
			rotation object_rot('e_spawner_02')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_spawner_02')),100,10).
	sequence 'spawn_box_03':
		Disable body 'stc_box_03'.
		Disable object 'g_g_03'.
		spawn_unit 'units/payday2/props/com_prop_shop_pear/debris/com_debris_pear_headphones_box':
			position object_pos('e_spawner_03')
			rotation object_rot('e_spawner_03')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_spawner_03')),100,10).
	sequence 'spawn_box_04':
		Disable body 'stc_box_04'.
		Disable object 'g_g_04'.
		spawn_unit 'units/payday2/props/com_prop_shop_pear/debris/com_debris_pear_headphones_box':
			position object_pos('e_spawner_04')
			rotation object_rot('e_spawner_04')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_spawner_04')),100,10).
	sequence 'spawn_box_05':
		Disable body 'stc_box_05'.
		Disable object 'g_g_05'.
		spawn_unit 'units/payday2/props/com_prop_shop_pear/debris/com_debris_pear_headphones_box':
			position object_pos('e_spawner_05')
			rotation object_rot('e_spawner_05')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_spawner_05')),100,10).
	sequence 'spawn_box_06':
		Disable body 'stc_box_06'.
		Disable object 'g_g_06'.
		spawn_unit 'units/payday2/props/com_prop_shop_pear/debris/com_debris_pear_headphones_box':
			position object_pos('e_spawner_06')
			rotation object_rot('e_spawner_06')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_spawner_06')),100,10).
	sequence 'spawn_box_07':
		Disable body 'stc_box_07'.
		Disable object 'g_g_07'.
		spawn_unit 'units/payday2/props/com_prop_shop_pear/debris/com_debris_pear_headphones_box':
			position object_pos('e_spawner_07')
			rotation object_rot('e_spawner_07')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_spawner_07')),100,10).
	sequence 'spawn_box_08':
		Disable body 'stc_box_08'.
		Disable object 'g_g_08'.
		spawn_unit 'units/payday2/props/com_prop_shop_pear/debris/com_debris_pear_headphones_box':
			position object_pos('e_spawner_08')
			rotation object_rot('e_spawner_08')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_spawner_08')),100,10).
	sequence 'spawn_box_09':
		Disable body 'stc_box_09'.
		Disable object 'g_g_09'.
		spawn_unit 'units/payday2/props/com_prop_shop_pear/debris/com_debris_pear_headphones_box':
			position object_pos('e_spawner_09')
			rotation object_rot('e_spawner_09')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_spawner_09')),100,10).
	sequence 'spawn_box_10':
		Disable body 'stc_box_10'.
		Disable object 'g_g_10'.
		spawn_unit 'units/payday2/props/com_prop_shop_pear/debris/com_debris_pear_headphones_box':
			position object_pos('e_spawner_10')
			rotation object_rot('e_spawner_10')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_spawner_10')),100,10).
	sequence 'spawn_box_11':
		Disable body 'stc_box_11'.
		Disable object 'g_g_11'.
		spawn_unit 'units/payday2/props/com_prop_shop_pear/debris/com_debris_pear_headphones_box':
			position object_pos('e_spawner_11')
			rotation object_rot('e_spawner_11')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_spawner_11')),100,10).
	sequence 'spawn_box_12':
		Disable body 'stc_box_12'.
		Disable object 'g_g_12'.
		spawn_unit 'units/payday2/props/com_prop_shop_pear/debris/com_debris_pear_headphones_box':
			position object_pos('e_spawner_12')
			rotation object_rot('e_spawner_12')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_spawner_12')),100,10).
	sequence 'spawn_box_13':
		Disable body 'stc_box_13'.
		Disable object 'g_g_13'.
		spawn_unit 'units/payday2/props/com_prop_shop_pear/debris/com_debris_pear_headphones_box':
			position object_pos('e_spawner_13')
			rotation object_rot('e_spawner_13')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_spawner_13')),100,10).
	sequence 'spawn_box_14':
		Disable body 'stc_box_14'.
		Disable object 'g_g_14'.
		spawn_unit 'units/payday2/props/com_prop_shop_pear/debris/com_debris_pear_headphones_box':
			position object_pos('e_spawner_14')
			rotation object_rot('e_spawner_14')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_spawner_14')),100,10).
	sequence 'spawn_box_15':
		Disable body 'stc_box_15'.
		Disable object 'g_g_15'.
		spawn_unit 'units/payday2/props/com_prop_shop_pear/debris/com_debris_pear_headphones_box':
			position object_pos('e_spawner_15')
			rotation object_rot('e_spawner_15')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_spawner_15')),100,10).
	sequence 'spawn_box_16':
		Disable body 'stc_box_16'.
		Disable object 'g_g_16'.
		spawn_unit 'units/payday2/props/com_prop_shop_pear/debris/com_debris_pear_headphones_box':
			position object_pos('e_spawner_16')
			rotation object_rot('e_spawner_16')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_spawner_16')),100,10).
	body 'stc_box_01'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'spawn_box_01'.
	body 'stc_box_02'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'spawn_box_02'.
	body 'stc_box_03'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'spawn_box_03'.
	body 'stc_box_04'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'spawn_box_04'.
	body 'stc_box_05'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'spawn_box_05'.
	body 'stc_box_06'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'spawn_box_06'.
	body 'stc_box_07'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'spawn_box_07'.
	body 'stc_box_08'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'spawn_box_08'.
	body 'stc_box_09'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'spawn_box_09'.
	body 'stc_box_10'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'spawn_box_10'.
	body 'stc_box_11'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'spawn_box_11'.
	body 'stc_box_12'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'spawn_box_12'.
	body 'stc_box_13'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'spawn_box_13'.
	body 'stc_box_14'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'spawn_box_14'.
	body 'stc_box_15'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'spawn_box_15'.
	body 'stc_box_16'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'spawn_box_16'.
