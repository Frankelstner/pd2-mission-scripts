unit:
	sequence 'make_dynamic':
		Disable body 'static_body'.
		Enable body 'dynamic_body'.
		Disable object 'g_g'.
		Enable object 'g_g_dmg'.
	body 'static_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'make_dynamic'.
	sequence 'int_seq_impact_sound':
		Play audio 'cardboard_box_impact' at 'snd'.
	body 'dynamic_body'
		Upon receiving 1 collision damage, execute:
			Run sequence 'int_seq_impact_sound'.
