unit:
	sequence 'break_glass_01':
		TRIGGER TIMES 1
		Disable body 'b_glass_01'.
		effect 'effects/payday2/particles/window/jewelry_counter_1':
			parent object( 'e_glass_01' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_01'.
		Disable object 'g_glass_01'.
		Disable decal_mesh 'dm_glass_breakable_01'.
	sequence 'break_glass_02':
		TRIGGER TIMES 1
		Disable body 'b_glass_02'.
		effect 'effects/payday2/particles/window/jewelry_counter_1':
			parent object( 'e_glass_02' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_02'.
		Disable object 'g_glass_02'.
		Disable decal_mesh 'dm_glass_breakable_02'.
	body 'b_glass_01'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_glass_01'.
	body 'b_glass_02'
		Upon receiving 3 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_glass_02'.
