unit:
	body 'body_padlock'
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'int_seq_bullet_hit'.
	sequence 'int_seq_bullet_hit':
		Disable interaction.
		Disable body 'body_padlock'.
		Disable object 'g_lock'.
		Hide graphic_group 'icon'.
		Enable animation_group 'anim':
			from 0/30
			to 40/30
	sequence 'interact':
		Run sequence 'int_seq_bullet_hit'.
	sequence 'interaction_enabled':
		Enable interaction.
	sequence 'interaction_disabled':
		Disable interaction.
