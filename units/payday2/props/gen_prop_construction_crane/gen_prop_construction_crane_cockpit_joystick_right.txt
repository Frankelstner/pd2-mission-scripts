unit:
	sequence 'state_interaction_enable':
		Enable interaction.
	sequence 'state_interaction_disable':
		Disable interaction.
	sequence 'interact':
		Play audio 'crane_move_lever' at 'rp_gen_prop_construction_crane_cockpit_joystick_right'.
