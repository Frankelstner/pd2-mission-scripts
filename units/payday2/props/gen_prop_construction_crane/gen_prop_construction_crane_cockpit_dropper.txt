unit:
	sequence 'state_interaction_enable':
		Enable interaction.
	sequence 'state_interaction_disable':
		Disable interaction.
	sequence 'interact':
		Enable animation_group 'use':
			from 0/30
			to 60/30
		Play audio 'crane_release_piggy_lever' at 'rp_gen_prop_construction_crane_cockpit_dropper'.
