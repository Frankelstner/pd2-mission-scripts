unit:
	sequence 'destroy':
		TRIGGER TIMES 1
		Disable object 'g_glass'.
		Enable object 'g_glass_broken'.
		Disable body 'glass_body'.
		Play audio 'window_small_shatter' at 'e_effect'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect')
			position v()
	body 'glass_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy'.
