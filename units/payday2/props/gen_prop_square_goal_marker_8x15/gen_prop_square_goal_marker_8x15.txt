unit:
	sequence 'show':
		Enable object 'g_goal'.
		Enable object 'g_icon'.
	sequence 'hide':
		Disable object 'g_goal'.
		Disable object 'g_icon'.
