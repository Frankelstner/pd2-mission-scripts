unit:
	sequence 'make_dynamic':
		Disable body 'static_body'.
		Enable body 'dynamic_body'.
		Play audio 'painting_break' at 'snd'.
		slot:
			slot 11
	body 'static_body'
		Upon receiving 5 bullet hits or 10 explosion damage or 20 melee damage, execute:
			Run sequence 'make_dynamic'.
