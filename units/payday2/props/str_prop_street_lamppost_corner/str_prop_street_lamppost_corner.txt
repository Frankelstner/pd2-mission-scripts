unit:
	sequence 'make_dynamic':
		spawn_unit 'units/payday2/props/str_prop_street_lamppost_corner/str_prop_street_lamppost_corner_02_il_debris':
			position object_pos('e_effect')
			rotation rot(0, 0, 0)
		Disable object 'g_g_top'.
		Disable object 's_s_top'.
		Disable light 'lo_omni_01'.
		Disable decal_mesh 'dm_glass_breakable'.
		Disable decal_mesh 'dm_metal_top'.
		Disable body 'static_body_top'.
		Disable effect_spawner 'street_light_flare_big'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_effect' )
			position v()
