unit:
	sequence 'state_6':
		Show graphic_group 'grp_lod_ammo'.
		Disable object 'g_ammo_variation_02'.
		Disable object 'g_ammo_variation_03'.
		Disable object 'g_ammo_variation_04'.
		Disable object 'g_ammo_variation_05'.
	sequence 'state_5':
		Hide graphic_group 'grp_lod_ammo'.
		Enable object 'g_ammo_variation_02'.
		Disable object 'g_ammo_variation_03'.
		Disable object 'g_ammo_variation_04'.
		Disable object 'g_ammo_variation_05'.
	sequence 'state_4':
		Hide graphic_group 'grp_lod_ammo'.
		Disable object 'g_ammo_variation_03'.
		Enable object 'g_ammo_variation_02'.
		Disable object 'g_ammo_variation_04'.
		Disable object 'g_ammo_variation_05'.
	sequence 'state_3':
		Hide graphic_group 'grp_lod_ammo'.
		Disable object 'g_ammo_variation_02'.
		Disable object 'g_ammo_variation_04'.
		Enable object 'g_ammo_variation_03'.
		Disable object 'g_ammo_variation_05'.
	sequence 'state_2':
		Hide graphic_group 'grp_lod_ammo'.
		Disable object 'g_ammo_variation_02'.
		Disable object 'g_ammo_variation_03'.
		Disable object 'g_ammo_variation_05'.
		Enable object 'g_ammo_variation_04'.
	sequence 'state_1':
		Hide graphic_group 'grp_lod_ammo'.
		Disable object 'g_ammo_variation_02'.
		Disable object 'g_ammo_variation_03'.
		Disable object 'g_ammo_variation_04'.
		Disable object 'g_ammo_variation_05'.
		Disable interaction.
	sequence 'state_0'.
