unit:
	sequence 'grey':
		material_config 'units/payday2/props/ind_prop_pipeset/ind_prop_pipeset'.
	sequence 'blue':
		material_config 'units/payday2/props/ind_prop_pipeset/ind_prop_pipeset_blue'.
	sequence 'red':
		material_config 'units/payday2/props/ind_prop_pipeset/ind_prop_pipeset_red'.
