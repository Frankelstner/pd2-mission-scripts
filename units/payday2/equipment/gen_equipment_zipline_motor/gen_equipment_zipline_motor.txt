unit:
	sequence 'state_zipline_enable':
		Enable body 'body'.
		Enable object 'g_g'.
		Enable object 'rope'.
		Call function: zipline.set_enabled(True)
	sequence 'state_zipline_disable':
		Disable body 'body'.
		Disable object 'g_g'.
		Disable object 'rope'.
		Call function: zipline.set_enabled(False)
	sequence 'state_zipline_disable_special':
		Call function: zipline.set_enabled(False)
	sequence 'state_zipline_enable_special':
		Call function: zipline.set_enabled(True)
	sequence 'on_person_enter_zipline'.
	sequence 'on_person_exit_zipline'.
	sequence 'on_attached_bag'.
	sequence 'on_detached_bag'.
