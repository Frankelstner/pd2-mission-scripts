unit:
	sequence 'state_1':
		Enable animation_group 'bag':
			from 0/30
			to 0/30
		Enable object 'g_bodybag_3'.
		Enable object 'g_bodybag_2'.
		Disable object 'g_bodybag_1'.
	sequence 'state_2':
		Enable animation_group 'bag':
			from 1/30
			to 1/30
		Enable object 'g_bodybag_3'.
		Disable object 'g_bodybag_2'.
		Disable object 'g_bodybag_1'.
	sequence 'state_3':
		Enable animation_group 'bag':
			from 2/30
			to 2/30
		Disable object 'g_bodybag_3'.
		Disable object 'g_bodybag_2'.
		Disable object 'g_bodybag_1'.
