unit:
	sequence 'state_interaction_enabled':
		Enable interaction.
		Show graphic_group 'drillicongroup'.
	sequence 'state_interaction_disabled':
		Disable interaction.
		Hide graphic_group 'drillicongroup'.
	sequence 'state_contour_enabled':
		material_config 'units/payday2/equipment/gen_equipment_camera_hackingtool/gen_equipment_camera_hackingtool_contour'.
	sequence 'state_contour_disabled':
		material_config 'units/payday2/equipment/gen_equipment_camera_hackingtool/gen_equipment_camera_hackingtool'.
	sequence 'state_vis_hidden':
		Disable interaction.
		Disable object 'g_tool'.
		Hide graphic_group 'drillicongroup'.
		material_config 'units/payday2/equipment/gen_equipment_camera_hackingtool/gen_equipment_camera_hackingtool'.
	sequence 'state_vis_show':
		Enable object 'g_tool'.
		Show graphic_group 'drillicongroup'.
		Enable interaction.
		material_config 'units/payday2/equipment/gen_equipment_camera_hackingtool/gen_equipment_camera_hackingtool_contour'.
	sequence 'interact':
		Disable interaction.
		Run sequence 'state_vis_hidden'.
