unit:
	sequence 'state_door_open':
		Disable interaction.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_open_close':
			from 40/30
			speed 0
			to 40/30
		Enable animation_group 'anim_door_closer':
			from 40/30
			speed 0
			to 40/30
		Run sequence 'deactivate_door'.
	sequence 'state_door_close':
		Enable interaction.
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_open_close':
			from 0/30
			speed 0
			to 0/30
		Enable animation_group 'anim_door_closer':
			from 0/30
			speed 0
			to 0/30
	sequence 'state_door_hide':
		Disable interaction.
		Disable body 'body_control_panel'.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Disable body 'body_frame'.
		Disable body 'body_door'.
		Hide graphic_group 'doors'.
		Run sequence 'deactivate_door'.
	sequence 'state_door_show':
		Enable interaction.
		Enable body 'body_control_panel'.
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
		Enable body 'body_frame'.
		Enable body 'body_door'.
		Show graphic_group 'doors'.
	sequence 'anim_open_door':
		Disable interaction.
		Enable animation_group 'anim_open_close':
			from 0/30
			to 35/30
		Enable animation_group 'anim_door_closer':
			from 0/30
			to 35/30
		Play audio 'generic_door_metal_open' at 'anim_open_close'.
	sequence 'anim_explosion_in':
		Disable interaction.
		Play audio 'c4_explode_metal' at 'door'.
		Disable body 'body_door'.
		Enable animation_group 'anim_door_closer':
			from 90/30
			to 92/30
		spawn_unit 'units/payday2/equipment/gen_interactable_door_keycard_dark/spawn_debris_door_metal_dark':
			position object_pos('rp_gen_interactable_door_keycard')
			rotation object_rot('rp_gen_interactable_door_keycard')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('a_push') ),200,400).
	sequence 'anim_explosion_out':
		Disable interaction.
		Play audio 'c4_explode_metal' at 'door'.
		Enable animation_group 'anim_open_close':
			from 0/30
			speed 3
			to 35/30
		Enable animation_group 'anim_door_closer':
			from 0/30
			speed 3
			to 35/30
	body 'body_hitbox_door_handle_in'
		Upon receiving 300 saw damage, execute:
			Run sequence 'int_seq_saw_in'.
	body 'body_hitbox_door_handle_out'
		Upon receiving 300 saw damage, execute:
			Run sequence 'int_seq_saw_out'.
	sequence 'interact':
		Run sequence 'int_seq_open'.
	sequence 'int_seq_open':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Enable object 'g_door'.
		Disable object 'g_door_frame'.
		Disable object 'g_door_bullet_dst'.
	sequence 'int_seq_breach_common':
		Disable interaction.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Run sequence 'deactivate_door'.
	sequence 'int_seq_bullet_hit_in':
		effect 'effects/payday2/particles/wood/wood_door_shotgun_dmg_in':
			parent object( 'e_door_dst' )
			position v()
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Disable object 'g_door'.
		Enable object 'g_door_frame'.
		Enable object 'g_door_bullet_dst'.
	sequence 'int_seq_bullet_hit_out':
		effect 'effects/payday2/particles/wood/wood_door_shotgun_dmg_out':
			parent object( 'e_door_dst' )
			position v()
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Disable object 'g_door'.
		Enable object 'g_door_frame'.
		Enable object 'g_door_bullet_dst'.
	sequence 'int_seq_saw_in':
		Run sequence 'anim_open_door'.
		Disable object 'g_door'.
		Enable object 'g_door_frame'.
		Enable object 'g_door_bullet_dst'.
		Run sequence 'int_seq_breach_common'.
		Run sequence 'done_opened'.
	sequence 'int_seq_saw_out':
		Run sequence 'anim_open_door'.
		Disable object 'g_door'.
		Enable object 'g_door_frame'.
		Enable object 'g_door_bullet_dst'.
		Run sequence 'int_seq_breach_common'.
		Run sequence 'done_opened'.
	sequence 'int_seq_explosion_in':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_explosion_in'.
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'a_shp_charge_1' )
			position v()
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'a_shp_charge_2' )
			position v()
		Run sequence 'done_exploded'.
		Disable object 'g_door'.
		Enable object 'g_door_frame'.
		Disable object 'g_door_bullet_dst'.
		Disable interaction.
		Run sequence 'done_opened'.
	sequence 'int_seq_explosion_out':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_explosion_out'.
		Run sequence 'done_exploded'.
		Disable object 'g_door'.
		Enable object 'g_door_frame'.
		Enable object 'g_door_bullet_dst'.
		Disable interaction.
		Run sequence 'done_opened'.
	sequence 'int_seq_keycard_panel_complete':
		Enable object 'g_control_panel'.
		Disable object 'g_control_panel_screen'.
		Enable object 'g_control_panel_screen_acces'.
		Enable object 'g_keycard'.
		Disable interaction.
	sequence 'activate_door':
		Call function: base.activate()
		Show graphic_group 'sawicongroup'.
	sequence 'deactivate_door':
		Call function: base.deactivate()
		Hide graphic_group 'sawicongroup'.
	sequence 'explode_door':
		Run sequence 'int_seq_explosion_in'.
	sequence 'power_off':
		Call function: base.set_powered(False)
	sequence 'power_on':
		Call function: base.set_powered(True)
	sequence 'turn_off':
		Call function: base.set_on(False)
	sequence 'turn_on':
		Call function: base.set_on(True)
	sequence 'open_door':
		Run sequence 'int_seq_open'.
	sequence 'open_door_keycard':
		Run sequence 'int_seq_open'.
		Run sequence 'int_seq_keycard_panel_complete'.
	sequence 'open_door_ecm':
		Run sequence 'int_seq_open'.
		Enable object 'g_ecm'.
		Enable object 'g_glow_func1_green'.
		Enable object 'g_glow_func2_green'.
	sequence 'door_opened'.
	sequence 'door_closed'.
	sequence 'drill_placed'.
	sequence 'drill_jammed'.
	sequence 'drill_resumed'.
	sequence 'drill_power_off'.
	sequence 'drill_power_on'.
	sequence 'drill_completed'.
	sequence 'all_drill_placed'.
	sequence 'c4_placed'.
	sequence 'c4_completed'.
	sequence 'all_c4_placed'.
	sequence 'key_placed'.
	sequence 'key_completed'.
	sequence 'all_key_placed'.
	sequence 'ecm_placed'.
	sequence 'all_ecm_placed'.
	sequence 'ecm_completed'.
	sequence 'sobj_swat_breach_in':
		Enable animation_group 'anim_breach_in_out':
			from 0/30
			to 65/30
		Run sequence 'int_seq_slam_door'. (DELAY 63/30)
		Run sequence 'done_swat_breach'.
		Run sequence 'deactivate_door'.
	sequence 'sobj_swat_breach_out':
		Enable animation_group 'anim_breach_in_out':
			from 75/30
			to 140/30
		Run sequence 'anim_explosion_out'. (DELAY 63/30)
		Run sequence 'done_swat_breach'.
		Run sequence 'deactivate_door'.
	sequence 'int_seq_slam_door':
		Enable animation_group 'anim_open_close':
			from 0/30
			speed 3
			to 35/30
		Enable animation_group 'anim_door_closer':
			from 0/30
			speed 3
			to 35/30
		Play audio 'generic_door_metal_open' at 'anim_open_close'.
		Run sequence 'int_seq_breach_common'.
		Run sequence 'done_opened'.
		Hide graphic_group 'sawicongroup'.
	sequence 'done_exploded':
		Run sequence 'done_opened'.
	sequence 'done_opened'.
	sequence 'done_swat_breach'.
