unit:
	sequence 'state_interact_enabled':
		Enable interaction.
	sequence 'state_interact_disabled':
		Disable interaction.
	sequence 'state_screen_correct':
		Enable object 'g_correct'.
		Disable object 'g_error'.
		Disable object 'g_incorrect'.
		Disable object 'g_processing'.
		Play audio 'barcode_scanner_scan_correct' at 'snd'.
	sequence 'state_screen_error':
		Disable object 'g_correct'.
		Enable object 'g_error'.
		Disable object 'g_incorrect'.
		Disable object 'g_processing'.
		Play audio 'barcode_scanner_jam' at 'snd'.
	sequence 'state_screen_incorrect':
		Disable object 'g_correct'.
		Disable object 'g_error'.
		Enable object 'g_incorrect'.
		Disable object 'g_processing'.
		Play audio 'barcode_scanner_scan_incorrect' at 'snd'.
	sequence 'state_screen_processing':
		Disable object 'g_correct'.
		Disable object 'g_error'.
		Disable object 'g_incorrect'.
		Enable object 'g_processing'.
		Play audio 'barcode_scanner_scanning' at 'snd'.
	sequence 'state_barcode_downtown_washington':
		Call function: interaction.set_tweak_data('read_barcode_downtown')
	sequence 'state_barcode_georgetown':
		Call function: interaction.set_tweak_data('read_barcode_brickell')
	sequence 'state_barcode_west_end':
		Call function: interaction.set_tweak_data('read_barcode_edgewater')
	sequence 'state_barcode_foggy_bottom':
		Call function: interaction.set_tweak_data('read_barcode_isles_beach')
	sequence 'state_barcode_shaw':
		Call function: interaction.set_tweak_data('read_barcode_opa_locka')
	sequence 'state_activate_barcode_reader':
		Call function: interaction.set_tweak_data('read_barcode_activate')
		Disable object 'g_correct'.
		Enable object 'g_error'.
		Disable object 'g_incorrect'.
		Disable object 'g_processing'.
	sequence 'interact':
		Disable object 'g_correct'.
		Disable object 'g_error'.
		Disable object 'g_incorrect'.
		Enable object 'g_processing'.
		Play audio 'barcode_scanner_scanning' at 'snd'.
	sequence 'wrong':
		Disable object 'g_correct'.
		Disable object 'g_error'.
		Disable object 'g_incorrect'.
		Enable object 'g_processing'.
		Play audio 'barcode_scanner_scanning' at 'snd'.
