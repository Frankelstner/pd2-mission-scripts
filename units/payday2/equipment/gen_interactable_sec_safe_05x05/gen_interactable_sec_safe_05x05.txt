unit:
	sequence 'spawn_loot_crap_a':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_crap_a':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	sequence 'spawn_loot_crap_b':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_crap_b':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	sequence 'spawn_loot_crap_c':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_crap_c':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	sequence 'spawn_loot_crap_d':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_crap_d':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	sequence 'spawn_loot_crap_e':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_crap_e':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	sequence 'spawn_loot_value_a':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_value_a':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	sequence 'spawn_loot_value_b':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_value_b':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	sequence 'spawn_loot_value_c':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_value_c':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	sequence 'spawn_loot_value_d':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_value_d':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	sequence 'spawn_loot_value_e':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_value_e':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	sequence 'spawn_loot_toast':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_toast':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	sequence 'spawn_loot_empty'.
	sequence 'spawn_loot_money':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_special_money':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	sequence 'spawn_loot_gold':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_special_gold':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	sequence 'spawn_loot_jungle_1_objective_a':
		spawn_unit 'units/payday2/props/gen_prop_loot_blueprint/gen_prop_loot_blueprint':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	sequence 'spawn_loot_jungle_1_objective_b':
		spawn_unit 'units/payday2/pickups/gen_pku_keycard/gen_pku_keycard':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	sequence 'spawn_loot_jungle_1_objective_c':
		spawn_unit 'units/payday2/props/gen_prop_loot_confidential_folder_event/gen_prop_loot_confidential_folder_event':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	type = 0
	level = 0
	sequence 'set_type_1':
		type = 1
	sequence 'set_type_2':
		type = 2
	sequence 'set_type_3':
		type = 3
	sequence 'set_type_4':
		type = 4
	sequence 'set_type_5':
		type = 5
	sequence 'set_level_jungle_1':
		type = 1
		level = 1
		Run sequence 'activate_door'.
	sequence 'set_level_jungle_2':
		type = 1
		level = 2
		Run sequence 'activate_door'.
	sequence 'set_level_rat_2':
		type = 1
		level = 3
		Run sequence 'activate_door'.
	sequence 'set_level_four_stores':
		type = 1
		level = 4
		Run sequence 'activate_door'.
	sequence 'set_level_mia':
		type = 1
		level = 5
		Run sequence 'activate_door'.
	sequence 'spawn':
		If level == 1: Run sequence 'spawn_level_jungle_1'.
		If level == 2: Run sequence 'spawn_level_jungle_2'.
		If level == 3: Run sequence 'spawn_level_rat_2'.
		If level == 4: Run sequence 'spawn_level_four_stores'.
		If level == 5: Run sequence 'spawn_level_mia'.
	sequence 'spawn_level_jungle_1':
		If type == 1: Run sequence 'spawn_loot_'..pick('crap_b','crap_c','crap_d','value_a','value_d','value_e','money','money').
		If type == 3: Run sequence 'spawn_loot_jungle_1_objective_a'.
		If type == 4: Run sequence 'spawn_loot_jungle_1_objective_b'.
		If type == 5: Run sequence 'spawn_loot_jungle_1_objective_c'.
	sequence 'spawn_level_jungle_2':
		If type == 1: Run sequence 'spawn_loot_'..pick('value_a','value_d','value_e','money','money','money','money','money','money','money','money','money','money').
	sequence 'spawn_level_rat_2':
		If type == 1: Run sequence 'spawn_loot_'..pick('crap_b','crap_c','crap_d','value_a','value_d','value_e','money','money').
	sequence 'spawn_level_four_stores':
		If type == 1: Run sequence 'spawn_loot_'..pick('value_a','value_a','value_b','value_b','value_c','value_c','value_d','value_d','value_e','value_e','value_e','value_e','value_e' ).
		If type == 2: Run sequence 'spawn_loot_'..pick('gold','crap_a','crap_b','crap_c','crap_a','crap_b','crap_c','crap_a','crap_b','crap_c','crap_a','crap_b','crap_c','crap_a','crap_b','crap_c','crap_a','crap_b','crap_c' ).
	sequence 'spawn_level_mia':
		If type == 1: Run sequence 'spawn_loot_'..pick('money','money','money','money','money','toast','crap_b').
	sequence 'anim_open_door':
		Enable animation_group 'anim_open_close':
			from 0/30
			to 80/30
		Play audio 'small_safe_open' at 'anim_handle'.
		Hide graphic_group 'ghosticongroup'.
	sequence 'anim_explosion_out':
		Enable animation_group 'anim_explosion_in_out':
			from 0/30
			to 10/30
		Hide graphic_group 'ghosticongroup'.
	sequence 'state_door_open':
		Enable animation_group 'anim_open_close':
			from 80/30
			speed 0
			to 80/30
		Run sequence 'deactivate_door'.
		Hide graphic_group 'ghosticongroup'.
	sequence 'state_door_close':
		Enable animation_group 'anim_open_close':
			from 0/30
			speed 0
			to 0/30
		Show graphic_group 'ghosticongroup'.
	sequence 'state_hide':
		Disable interaction.
		Disable body 'body_hitbox_door_handle_out'.
		Disable body 'body_frame'.
		Disable body 'body_door'.
		Hide graphic_group 'safe'.
		Run sequence 'deactivate_door'.
		Hide graphic_group 'ghosticongroup'.
	sequence 'state_show':
		Enable interaction.
		Enable body 'body_hitbox_door_handle_out'.
		Enable body 'body_frame'.
		Enable body 'body_door'.
		Show graphic_group 'safe'.
		Run sequence 'activate_door'.
		Show graphic_group 'ghosticongroup'.
	sequence 'open_door':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
	sequence 'int_seq_breach_common':
		Disable interaction.
		Disable body 'body_hitbox_door_handle_out'.
		Run sequence 'deactivate_door'.
	sequence 'int_seq_saw_out':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
	sequence 'hide_door':
		Disable body 'body_door'.
		Disable object 'g_door'.
		Disable object 'g_analog'.
		Disable object 'g_handle'.
		Disable decal_mesh 'g_door'.
		Hide graphic_group 'ghosticongroup'.
	sequence 'int_seq_explosion_out':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'hide_door'.
		spawn_unit 'units/payday2/equipment/gen_interactable_sec_safe_05x05/dest_sec_safe_05x05_door':
			position object_pos('spawn_door')
			rotation object_rot('spawn_door')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('a_physic_effect') ),100,20).
		effect 'effects/particles/explosions/explosion_grenade':
			parent object( 'a_shp_charge' )
			position v()
		effect 'effects/particles/dest/smoke_pocket_puff':
			parent object( 'anim_explosion_in_out' )
			position v()
		Play audio 'c4_explode_metal' at 'a_shp_charge'.
		Run sequence 'done_exploded'.
	sequence 'interact':
		Run sequence 'open_door'.
	sequence 'activate_door':
		Call function: base.activate()
		Show graphic_group 'ghosticongroup'.
		Enable interaction.
	sequence 'deactivate_door':
		Call function: base.deactivate()
		Hide graphic_group 'ghosticongroup'.
		Disable interaction.
	sequence 'explode_door':
		Run sequence 'int_seq_explosion_out'.
	sequence 'power_off':
		Call function: base.set_powered(False)
	sequence 'power_on':
		Call function: base.set_powered(True)
	sequence 'turn_off':
		Call function: base.set_on(False)
	sequence 'turn_on':
		Call function: base.set_on(True)
	sequence 'door_opened'.
	sequence 'door_closed'.
	sequence 'drill_placed'.
	sequence 'drill_jammed'.
	sequence 'drill_resumed'.
	sequence 'drill_power_off'.
	sequence 'drill_power_on'.
	sequence 'drill_completed'.
	sequence 'all_drill_placed'.
	sequence 'c4_placed'.
	sequence 'c4_completed'.
	sequence 'all_c4_placed'.
	sequence 'key_placed'.
	sequence 'key_completed'.
	sequence 'all_key_placed'.
	sequence 'done_exploded':
		Run sequence 'done_opened'.
	sequence 'done_opened':
		If type > 0: Run sequence 'spawn'.
