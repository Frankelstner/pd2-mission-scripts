unit:
	sequence 'state_interact_enabled':
		Enable interaction.
		Call function: interaction.set_tweak_data('gen_pku_thermite')
	sequence 'state_interact_enabled_3s':
		Enable interaction.
		Call function: interaction.set_tweak_data('gen_pku_thermite_timer_3sec')
	sequence 'state_interact_disabled':
		Disable interaction.
	sequence 'hide':
		Disable interaction.
		Disable body 'static_body'.
		Disable object 'g_g'.
	sequence 'show':
		Enable body 'static_body'.
		Enable object 'g_g'.
	sequence 'load':
		Run sequence 'hide'.
	sequence 'disable_outline':
		material_config 'units/payday2/equipment/gen_interactable_thermite_backpack/gen_interactable_thermite_backpack_no_outline'.
	sequence 'enable_outline':
		material_config 'units/payday2/equipment/gen_interactable_thermite_backpack/gen_interactable_thermite_backpack'.
