unit:
	sequence 'state_requires_crowbar':
		Call function: interaction.set_tweak_data('crate_loot_crowbar')
	sequence 'state_interaction_enabled':
		Enable interaction.
	sequence 'state_interaction_disabled':
		Disable interaction.
	sequence 'state_vis_show':
		Enable body 'body_closed'.
		Disable body 'body_open'.
		Enable body 'body_blocker_mover'.
		Enable decal_mesh 'g_g'.
		Disable decal_mesh 'g_g_open'.
		Enable object 'g_g'.
		Disable object 'g_g_open'.
		Disable object 's_s'.
		Disable object 's_s_open'.
	sequence 'state_vis_hide':
		Disable interaction.
		Disable body 'body_closed'.
		Disable body 'body_open'.
		Disable body 'body_blocker_mover'.
		Disable decal_mesh 'g_g'.
		Disable decal_mesh 'g_g_open'.
		Disable object 'g_g'.
		Disable object 'g_g_open'.
		Disable object 's_s'.
		Disable object 's_s_open'.
	sequence 'interact':
		Disable body 'body_closed'.
		Enable body 'body_open'.
		Enable body 'body_blocker_mover'.
		Disable decal_mesh 'g_g'.
		Enable decal_mesh 'g_g_open'.
		Disable object 'g_g'.
		Enable object 'g_g_open'.
		Disable object 's_s'.
		Enable object 's_s_open'.
