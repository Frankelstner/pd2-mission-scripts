unit:
	toggle = 0
	sequence 'state_drill_shock_toggled_on':
		toggle = 1
	sequence 'state_drill_shock_toggled_off':
		toggle = 0
	sequence 'state_hide':
		Disable interaction.
		Disable body 'body0'.
		Disable object 'g_drill'.
		Disable object 'g_base'.
		Disable object 'g_toolbag'.
		Disable object 'g_toolbag_empty'.
		Hide graphic_group 'drillicongroup'.
		Call function: timer_gui.set_visible(False)
	sequence 'state_show':
		Enable interaction.
		Enable body 'body0'.
		Show graphic_group 'drillicongroup'.
		Disable object 'g_drill'.
		Disable object 'g_base'.
		Call function: timer_gui.set_visible(True)
	sequence 'activate':
		Enable animation_group 'anim_mover':
			from 0/30
			speed 1
			to 20/30
		Enable body 'body_mover'.
		Disable body 'body_mover'. (DELAY 20/30)
		Enable body 'body0'. (DELAY 20/30)
		Enable interaction.
		Call function: timer_gui.set_visible(False)
		Show graphic_group 'drillicongroup'.
		Enable object 'g_toolbag'.
		Disable object 'g_toolbag_empty'.
		Disable object 'g_drill'.
		Disable object 'g_base'.
	sequence 'reactivate':
		Enable interaction.
	sequence 'deactivate':
		Disable interaction.
		Hide graphic_group 'drillicongroup'.
		Disable object 'g_toolbag'.
		Disable object 'g_toolbag_empty'.
		Call function: timer_gui.set_visible(False)
	sequence 'state_interact_custom':
		toggle = 3
	sequence 'power_off':
		Call function: timer_gui.set_powered(False)
	sequence 'power_on':
		Call function: timer_gui.set_powered(True)
	sequence 'power_jammed':
		Call function: timer_gui.set_jammed(True)
	sequence 'jammed_trigger'.
	sequence 'timer_done'.
	sequence 'interact':
		If toggle == 0: Run sequence 'int_seq_drill_toggled'.
		If toggle == 1: Run sequence 'done_shock'.
		If toggle == 3: Run sequence 'int_seq_drill_custom'.
	sequence 'int_seq_drill_toggled':
		Call function: timer_gui.set_visible(True)
		Call function: timer_gui.start('360')
		Hide graphic_group 'drillicongroup'.
		Disable object 'g_toolbag'.
		Enable object 'g_toolbag_empty'.
		Enable object 'g_drill'.
		Enable object 'g_base'.
	sequence 'int_seq_drill_custom':
		Call function: timer_gui.set_visible(True)
		Call function: timer_gui.start('200')
		Hide graphic_group 'drillicongroup'.
		Disable object 'g_toolbag'.
		Enable object 'g_toolbag_empty'.
		Enable object 'g_drill'.
		Enable object 'g_base'.
	sequence 'done_shock'.
