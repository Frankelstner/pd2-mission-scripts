unit:
	hidden = 0
	sequence 'enable_interaction':
		If hidden == 0: 
			Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'hide':
		Disable interaction.
		Disable object 'g_g'.
		Disable decal_mesh 'g_g'.
		Disable body 'body_money'.
		hidden = 1
	sequence 'show':
		Enable object 'g_g'.
		Enable decal_mesh 'g_g'.
		Enable body 'body_money'.
		hidden = 0
	sequence 'show_and_enable_interaction':
		Enable interaction.
		Enable object 'g_g'.
		Enable decal_mesh 'g_g'.
		Enable body 'body_money'.
		hidden = 0
	sequence 'interact':
		Run sequence 'hide'.
	sequence 'load'.
