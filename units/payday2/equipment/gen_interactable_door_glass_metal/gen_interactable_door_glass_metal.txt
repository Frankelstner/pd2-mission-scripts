unit:
	sequence 'state_door_open':
		Disable interaction.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_door':
			from 183/30
			speed 0
			to 183/30
		Run sequence 'done_opened'.
		Hide graphic_group 'icon'.
	sequence 'state_door_close':
		Enable interaction.
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_door':
			from 0/30
			speed 0
			to 0/30
		Show graphic_group 'icon'.
	sequence 'state_vis_hide':
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Disable body 'body_frame'.
		Disable body 'body_door'.
		Disable body 'body_glass_1'.
		Disable body 'body_glass_2'.
		Disable body 'body_glass_3'.
		Disable body 'body_mover_blocker'.
		Hide graphic_group 'doors'.
		Hide graphic_group 'icon'.
	sequence 'state_vis_show':
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
		Enable body 'body_frame'.
		Enable body 'body_door'.
		Enable body 'body_glass_1'.
		Enable body 'body_glass_2'.
		Enable body 'body_glass_3'.
		Enable body 'body_mover_blocker'.
		Show graphic_group 'doors'.
		Show graphic_group 'icon'.
	sequence 'deactivate_door':
		Disable interaction.
		Hide graphic_group 'icon'.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
	sequence 'anim_open_door':
		Enable animation_group 'anim_door':
			from 163/30
			to 194/30
		Play audio 'generic_door_wood_open' at 'anim_door'.
		Run sequence 'done_opened'. (DELAY 31/30)
		Hide graphic_group 'icon'.
	sequence 'anim_close_door':
		Enable animation_group 'anim_door':
			from 195/30
			to 214/30
		Show graphic_group 'icon'.
	sequence 'anim_explosion_out':
		Play audio 'c4_explode_wood' at 'anim_door'.
		Enable animation_group 'anim_door':
			from 163/30
			speed 3
			to 194/30
		Run sequence 'done_opened'.
		Hide graphic_group 'icon'.
	sequence 'anim_explosion_in':
		Play audio 'c4_explode_wood' at 'anim_door'.
		Enable animation_group 'anim_door':
			from 163/30
			speed 3
			to 194/30
		Run sequence 'done_opened'.
		Hide graphic_group 'icon'.
	body 'body_hitbox_door_handle_in'
		Upon receiving 4 bullet hits, execute:
			Run sequence 'int_seq_bullet_hit_in'.
		Upon receiving 75 saw damage, execute:
			Run sequence 'int_seq_saw_in'.
		Upon receiving 10 explosion damage, execute:
			Run sequence 'int_seq_explosion_in'.
	body 'body_hitbox_door_handle_out'
		Upon receiving 4 bullet hits, execute:
			Run sequence 'int_seq_bullet_hit_out'.
		Upon receiving 75 saw damage, execute:
			Run sequence 'int_seq_saw_out'.
		Upon receiving 10 explosion damage, execute:
			Run sequence 'int_seq_explosion_out'.
	body 'body_glass_1'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 saw damage, execute:
			Run sequence 'int_seq_crack_win_1'.
		Upon receiving 3 bullet hits or 10 explosion damage or 7.5 saw damage or 20 melee damage, execute:
			Run sequence 'int_seq_shatter_win_1'.
	body 'body_glass_2'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 saw damage, execute:
			Run sequence 'int_seq_crack_win_2'.
		Upon receiving 3 bullet hits or 10 explosion damage or 7.5 saw damage or 20 melee damage, execute:
			Run sequence 'int_seq_shatter_win_2'.
	body 'body_glass_3'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 saw damage, execute:
			Run sequence 'int_seq_crack_win_3'.
		Upon receiving 3 bullet hits or 10 explosion damage or 7.5 saw damage or 20 melee damage, execute:
			Run sequence 'int_seq_shatter_win_3'.
	sequence 'int_seq_crack_win_1':
		TRIGGER TIMES 1
		Disable object 'g_glass_1'.
		Enable object 'g_glass_dmg_1'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_1' (alarm reason: 'glass').
	sequence 'int_seq_shatter_win_1':
		TRIGGER TIMES 1
		Disable body 'body_glass_1'.
		Enable body 'body_mover_blocker'.
		Disable object 'g_glass_1'.
		Disable object 'g_glass_dmg_1'.
		Disable decal_mesh 'dm_glass_1'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_1' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_1'.
		Cause alert with 12 m radius.
	sequence 'int_seq_crack_win_2':
		TRIGGER TIMES 1
		Disable object 'g_glass_2'.
		Enable object 'g_glass_dmg_2'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_2' (alarm reason: 'glass').
	sequence 'int_seq_shatter_win_2':
		TRIGGER TIMES 1
		Disable body 'body_glass_2'.
		Enable body 'body_mover_blocker'.
		Disable object 'g_glass_2'.
		Disable object 'g_glass_dmg_2'.
		Disable decal_mesh 'dm_glass_2'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_2' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_2'.
		Cause alert with 12 m radius.
	sequence 'int_seq_crack_win_3':
		TRIGGER TIMES 1
		Disable object 'g_glass_3'.
		Enable object 'g_glass_dmg_3'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_3' (alarm reason: 'glass').
	sequence 'int_seq_shatter_win_3':
		TRIGGER TIMES 1
		Disable body 'body_glass_3'.
		Enable body 'body_mover_blocker'.
		Disable object 'g_glass_3'.
		Disable object 'g_glass_dmg_3'.
		Disable decal_mesh 'dm_glass_3'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_3' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_3'.
		Cause alert with 12 m radius.
	sequence 'int_seq_open':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Enable object 'g_door'.
		Disable object 'g_door_saw_dst'.
	sequence 'int_seq_breach_common':
		Disable interaction.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Hide graphic_group 'icon'.
	sequence 'int_seq_bullet_hit_in':
		effect 'effects/payday2/particles/wood/wood_door_shotgun_dmg_in':
			parent object( 'e_door_dst' )
			position v()
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Enable object 'g_door'.
		Disable object 'g_door_saw_dst'.
	sequence 'int_seq_bullet_hit_out':
		effect 'effects/payday2/particles/wood/wood_door_shotgun_dmg_out':
			parent object( 'e_door_dst' )
			position v()
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Enable object 'g_door'.
		Disable object 'g_door_saw_dst'.
	sequence 'int_seq_saw_in':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Disable object 'g_door'.
		Enable object 'g_door_saw_dst'.
	sequence 'int_seq_saw_out':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Disable object 'g_door'.
		Enable object 'g_door_saw_dst'.
	sequence 'int_seq_explosion_in':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_explosion_in'.
		Run sequence 'done_exploded'.
	sequence 'int_seq_explosion_out':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_explosion_out'.
		Run sequence 'done_exploded'.
	sequence 'interact':
		Run sequence 'int_seq_open'.
	sequence 'sobj_swat_breach_in':
		Enable animation_group 'anim_door':
			from 0/30
			to 83/30
		Run sequence 'int_seq_slam_door'. (DELAY 89/30)
		Run sequence 'done_swat_breach'.
		Hide graphic_group 'icon'.
	sequence 'sobj_swat_breach_out':
		Enable animation_group 'anim_door':
			from 93/30
			to 153/30
		Run sequence 'anim_explosion_out'. (DELAY 90/30)
		Run sequence 'done_swat_breach'.
		Hide graphic_group 'icon'.
	sequence 'int_seq_slam_door':
		Enable animation_group 'anim_door':
			from 163/30
			speed 3
			to 194/30
		Play audio 'generic_door_wood_open' at 'anim_door'.
		Run sequence 'int_seq_breach_common'.
		Run sequence 'done_opened'.
		Hide graphic_group 'icon'.
	sequence 'open_door':
		Run sequence 'int_seq_open'.
	sequence 'done_exploded'.
	sequence 'done_opened'.
	sequence 'done_swat_breach'.
