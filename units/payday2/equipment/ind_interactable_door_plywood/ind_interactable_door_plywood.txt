unit:
	sequence 'state_door_unopenable':
		Disable interaction.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_open_close':
			from 0/30
			speed 0
			to 0/30
		Run sequence 'done_opened'.
		Hide graphic_group 'sawicongroup'.
	sequence 'state_door_open':
		Disable interaction.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_open_close':
			from 40/30
			speed 0
			to 40/30
		Run sequence 'done_opened'.
		Hide graphic_group 'sawicongroup'.
	sequence 'state_door_close':
		Enable interaction.
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_open_close':
			from 0/30
			speed 0
			to 0/30
		Show graphic_group 'sawicongroup'.
	sequence 'state_vis_hide':
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Disable body 'body_frame'.
		Disable body 'body_door'.
		Hide graphic_group 'doors'.
		Run sequence 'deactivate_door'.
		Hide graphic_group 'sawicongroup'.
	sequence 'state_vis_show':
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
		Enable body 'body_frame'.
		Enable body 'body_door'.
		Show graphic_group 'doors'.
		Show graphic_group 'sawicongroup'.
	sequence 'anim_open_door':
		Enable animation_group 'anim_open_close':
			from 0/30
			to 35/30
		Play audio 'generic_door_wood_open' at 'anim_open_close'.
		Hide graphic_group 'sawicongroup'.
	sequence 'anim_explosion_in':
		Enable animation_group 'anim_explosion_in_out':
			from 60/30
			to 80/30
		Play audio 'c4_explode_wood' at 'door'.
		Disable body 'body_door'.
		Disable object 'g_door'.
		Hide graphic_group 'sawicongroup'.
		spawn_unit 'units/payday2/equipment/ind_interactable_door_plywood/spawn_debris_door_wood':
			position object_pos('rp_ind_interactable_door_plywood')
			rotation object_rot('rp_ind_interactable_door_plywood')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('a_push_in') ),200,400).
	sequence 'anim_explosion_out':
		Enable animation_group 'anim_explosion_in_out':
			from 60/30
			to 80/30
		Play audio 'c4_explode_wood' at 'door'.
		Disable body 'body_door'.
		Disable object 'g_door'.
		Hide graphic_group 'sawicongroup'.
		spawn_unit 'units/payday2/equipment/ind_interactable_door_plywood/spawn_debris_door_wood':
			position object_pos('rp_ind_interactable_door_plywood')
			rotation object_rot('rp_ind_interactable_door_plywood')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('a_push_out') ),200,400).
	body 'body_hitbox_door_handle_in'
		Upon receiving 4 bullet hits, execute:
			Run sequence 'int_seq_bullet_hit_in'.
		Upon receiving 75 saw damage, execute:
			Run sequence 'int_seq_saw_in'.
	body 'body_hitbox_door_handle_out'
		Upon receiving 4 bullet hits, execute:
			Run sequence 'int_seq_bullet_hit_out'.
		Upon receiving 75 saw damage, execute:
			Run sequence 'int_seq_saw_out'.
	sequence 'int_seq_open':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Enable object 'g_door'.
		Disable object 'g_door_bullet_dst'.
		Disable object 'g_door_saw_dst'.
	sequence 'int_seq_breach_common':
		Disable interaction.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Run sequence 'deactivate_door'.
	sequence 'int_seq_bullet_hit_in':
		effect 'effects/payday2/particles/wood/wood_door_shotgun_dmg_in':
			parent object( 'e_door_dst' )
			position v()
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Disable object 'g_door'.
		Enable object 'g_door_bullet_dst'.
		Disable object 'g_door_saw_dst'.
	sequence 'int_seq_bullet_hit_out':
		effect 'effects/payday2/particles/wood/wood_door_shotgun_dmg_out':
			parent object( 'e_door_dst' )
			position v()
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Disable object 'g_door'.
		Enable object 'g_door_bullet_dst'.
		Disable object 'g_door_saw_dst'.
	sequence 'int_seq_saw_in':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Disable object 'g_door'.
		Disable object 'g_door_bullet_dst'.
		Enable object 'g_door_saw_dst'.
	sequence 'int_seq_saw_out':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Disable object 'g_door'.
		Disable object 'g_door_bullet_dst'.
		Enable object 'g_door_saw_dst'.
	sequence 'int_seq_explosion_in':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_explosion_in'.
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'a_shp_charge_1' )
			position v()
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'a_shp_charge_2' )
			position v()
		Run sequence 'done_exploded'.
		Run sequence 'done_opened'.
		Disable body 'body_door'.
		Disable object 'g_door_expl_dst'.
		Disable object 'g_door'.
	sequence 'int_seq_explosion_out':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_explosion_out'.
		Run sequence 'done_exploded'.
		Run sequence 'done_opened'.
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'a_shp_charge_1' )
			position v()
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'a_shp_charge_2' )
			position v()
		Disable object 'g_door_expl_dst'.
		Disable object 'g_door'.
	sequence 'interact':
		Run sequence 'int_seq_open'.
	sequence 'activate_door':
		Show graphic_group 'sawicongroup'.
	sequence 'deactivate_door':
		Hide graphic_group 'sawicongroup'.
	sequence 'explode_door':
		Run sequence 'int_seq_explosion_in'.
	sequence 'power_off':
		Call function: base.set_powered(False)
	sequence 'power_on':
		Call function: base.set_powered(True)
	sequence 'turn_off':
		Call function: base.set_on(False)
	sequence 'turn_on':
		Call function: base.set_on(True)
	sequence 'door_opened'.
	sequence 'door_closed'.
	sequence 'drill_placed'.
	sequence 'drill_jammed'.
	sequence 'drill_resumed'.
	sequence 'drill_power_off'.
	sequence 'drill_power_on'.
	sequence 'drill_completed'.
	sequence 'all_drill_placed'.
	sequence 'c4_placed'.
	sequence 'c4_completed'.
	sequence 'all_c4_placed'.
	sequence 'key_placed'.
	sequence 'key_completed'.
	sequence 'all_key_placed'.
	sequence 'sobj_swat_breach_in':
		Enable animation_group 'anim_breach_in_out':
			from 0/30
			to 65/30
		Run sequence 'int_seq_slam_door'. (DELAY 63/30)
		Run sequence 'done_swat_breach'.
		Run sequence 'deactivate_door'.
	sequence 'sobj_swat_breach_out':
		Enable animation_group 'anim_breach_in_out':
			from 75/30
			to 140/30
		Run sequence 'anim_explosion_out'. (DELAY 63/30)
		Run sequence 'done_swat_breach'.
		Run sequence 'deactivate_door'.
	sequence 'int_seq_slam_door':
		Enable animation_group 'anim_open_close':
			from 0/30
			speed 3
			to 35/30
		Play audio 'generic_door_wood_open' at 'anim_open_close'.
		Run sequence 'int_seq_breach_common'.
		Run sequence 'done_opened'.
	sequence 'done_exploded'.
	sequence 'done_opened'.
	sequence 'done_swat_breach'.
