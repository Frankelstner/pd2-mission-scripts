unit:
	sequence 'explode':
		Call function: base.explode()
	body 'body_static'
		Upon receiving 1 bullet hit or 20 explosion damage, execute:
			Run sequence 'explode'.
