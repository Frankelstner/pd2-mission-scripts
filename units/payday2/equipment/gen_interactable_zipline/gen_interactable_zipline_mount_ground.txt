unit:
	sequence 'state_opened':
		Enable animation_group 'anim':
			from 90/30
			speed 0
			to 90/30
	sequence 'state_closed':
		Enable animation_group 'anim':
			from 0/30
			speed 0
			to 0/30
	sequence 'player_deploy':
		Enable animation_group 'anim':
			from 0/30
			to 90/30
		Run sequence 'done_deployed'. (DELAY 90/30)
	sequence 'done_deployed'.
	sequence 'state_outlined_interaction_enabled':
		Enable interaction.
		Enable body 'convex'.
		Enable object 'g_outline'.
		Disable object 'g_leg_1'.
		Disable object 'g_leg_hinge'.
		Disable object 'g_foot_main'.
		Disable object 'g_foot_right'.
		Disable object 'g_foot_left'.
		Disable object 'g_mount'.
		Disable object 'g_truss'.
		Disable object 'g_box_1'.
		Disable object 'g_bolt_1'.
		Disable object 'g_bolt_2'.
		Disable object 'g_bolt_l'.
		Disable object 'g_bolt_r'.
	sequence 'state_outlined_interaction_disabled':
		Disable interaction.
		Disable body 'convex'.
		Disable object 'g_outline'.
		Disable object 'g_leg_1'.
		Disable object 'g_leg_hinge'.
		Disable object 'g_foot_main'.
		Disable object 'g_foot_right'.
		Disable object 'g_foot_left'.
		Disable object 'g_mount'.
		Disable object 'g_truss'.
		Disable object 'g_box_1'.
		Disable object 'g_bolt_1'.
		Disable object 'g_bolt_2'.
		Disable object 'g_bolt_l'.
		Disable object 'g_bolt_r'.
	sequence 'state_outlined_interaction_disabled_no_hide':
		Disable interaction.
		Disable object 'g_outline'.
	sequence 'state_vis_show':
		Enable body 'convex'.
		Enable object 'g_leg_1'.
		Enable object 'g_leg_hinge'.
		Enable object 'g_foot_main'.
		Enable object 'g_foot_right'.
		Enable object 'g_foot_left'.
		Enable object 'g_mount'.
		Enable object 'g_truss'.
		Enable object 'g_box_1'.
		Enable object 'g_bolt_1'.
		Enable object 'g_bolt_2'.
		Enable object 'g_bolt_l'.
		Enable object 'g_bolt_r'.
	sequence 'state_vis_hide':
		Disable body 'convex'.
		Disable object 'g_leg_1'.
		Disable object 'g_leg_hinge'.
		Disable object 'g_foot_main'.
		Disable object 'g_foot_right'.
		Disable object 'g_foot_left'.
		Disable object 'g_mount'.
		Disable object 'g_truss'.
		Disable object 'g_box_1'.
		Disable object 'g_bolt_1'.
		Disable object 'g_bolt_2'.
		Disable object 'g_bolt_l'.
		Disable object 'g_bolt_r'.
	sequence 'interact':
		Run sequence 'state_vis_show'.
		Enable animation_group 'anim':
			from 0/30
			to 90/30
		Play audio 'zip_line_mount' at 'rp_gen_interactable_zipline_mount_ground'.
