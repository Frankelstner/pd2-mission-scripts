unit:
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'interact':
		Run sequence 'complete'.
	sequence 'disable_interact':
		Disable interaction.
	sequence 'complete':
		Disable interaction.
