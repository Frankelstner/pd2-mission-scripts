unit:
	the_one = 0
	hacking = 0
	sequence 'ping_silent':
		Run sequence 'screen_on'.
		Run sequence 'clear'.
		Enable object 'g_ping_denied'.
		Disable object 'g_ping_denied'. (DELAY 2)
	sequence 'ping':
		If the_one == 1: 
			Play audio 'metal_detector_alarm' at 'c_box_01'.
			Run sequence 'ping_show'.
	sequence 'ping_show':
		If hacking == 0: 
			Run sequence 'screen_on'.
			Run sequence 'clear'.
			Enable object 'g_ping_failed'.
			Disable object 'g_ping_failed'. (DELAY 2)
	sequence 'start_hack':
		Run sequence 'screen_on'.
		Run sequence 'clear'.
		Enable object 'g_mon_hack'. (DELAY 0.3)
		Enable object 'g_con'.
		Enable object 'g_connected'. (DELAY 1.5)
		Enable object 'g_starthack'. (DELAY 2.5)
		Enable object 'g_cube'. (DELAY 2.5)
		Enable object 'g_scroll'. (DELAY 2.5)
		Enable animation_group 'anim_spin': (DELAY 1)
			speed 0.5
		Run sequence 'done_start_hack'. (DELAY 20)
		hacking = 1
	sequence 'start_hack_the_one':
		If the_one == 1: 
			Run sequence 'screen_on'.
			Run sequence 'clear'.
			Enable object 'g_mon_hack'. (DELAY 0.3)
			Enable object 'g_con'.
			Enable object 'g_connected'. (DELAY 1.5)
			Enable object 'g_starthack'. (DELAY 2.5)
			Enable object 'g_cube'. (DELAY 2.5)
			Enable object 'g_scroll'. (DELAY 2.5)
			Enable animation_group 'anim_spin': (DELAY 1)
				speed 0.5
			Run sequence 'done_start_hack'. (DELAY 20)
			hacking = 1
	sequence 'finish_hack':
		Disable object 'g_scroll'. (DELAY 0)
		Disable object 'g_cube'. (DELAY 0)
		Disable object 'g_mon_hack'. (DELAY 2)
		Disable object 'g_cube'. (DELAY 2)
		Disable object 'g_connected'. (DELAY 2)
		Disable object 'g_con'. (DELAY 2)
		Disable object 'g_starthack'. (DELAY 2)
		Disable object 'g_ping_denied'. (DELAY 0)
		Disable object 'g_ping_failed'. (DELAY 0)
		Disable object 'g_mon_def'. (DELAY 4)
		If the_one == 0: Run sequence 'done_finish_hack'.
		If the_one == 1: Run sequence 'done_finish_hack_the_one'.
	sequence 'clear':
		Disable object 'g_scroll'. (DELAY 0)
		Disable object 'g_cube'. (DELAY 0)
		Disable object 'g_mon_hack'. (DELAY 0)
		Disable object 'g_cube'. (DELAY 0)
		Disable object 'g_connected'. (DELAY 0)
		Disable object 'g_con'. (DELAY 0)
		Disable object 'g_starthack'. (DELAY 0)
		Disable object 'g_ping_denied'. (DELAY 0)
		Disable object 'g_ping_failed'. (DELAY 0)
	sequence 'screen_on':
		Enable object 'g_mon_def'.
	sequence 'screen_off':
		Run sequence 'clear'.
		Disable object 'g_mon_def'.
	sequence 'the_one':
		the_one = 1
	sequence 'done_start_hack'.
	sequence 'done_finish_hack'.
	sequence 'done_finish_hack_the_one'.
