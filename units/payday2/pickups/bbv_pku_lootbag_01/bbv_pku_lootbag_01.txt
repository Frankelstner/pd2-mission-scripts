unit:
	sequence 'interact':
		Disable interaction.
		Disable body 'hinge_body_1'.
		Disable body 'hinge_body_2'.
		Disable constraint 'bag'.
		Disable object 'bag'.
	sequence 'secured':
		Disable interaction.
		Call function: carry_data.sequence_clbk_secured()
	sequence 'non_usable':
		Disable interaction.
	sequence 'freeze':
		Disable body 'hinge_body_1'.
		Disable body 'hinge_body_2'.
		Disable constraint 'bag'.
