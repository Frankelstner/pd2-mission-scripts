unit:
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'hide':
		Disable interaction.
		Disable object 'g_circuit'.
		Disable decal_mesh 'g_circuit'.
		Disable body 'body_circuit'.
	sequence 'show':
		Enable object 'g_circuit'.
		Enable decal_mesh 'g_circuit'.
		Enable body 'body_circuit'.
	sequence 'show_and_enable_interaction':
		Enable interaction.
		Enable object 'g_circuit'.
		Enable decal_mesh 'g_circuit'.
		Enable body 'body_circuit'.
	sequence 'state_contour_enable':
		material_config 'units/payday2/pickups/gen_pku_circuit_board/gen_pku_circuit_board'.
	sequence 'state_contour_disable':
		material_config 'units/payday2/pickups/gen_pku_circuit_board/gen_pku_circuit_board_no_contour'.
	sequence 'interact':
		Run sequence 'hide'.
	sequence 'load'.
