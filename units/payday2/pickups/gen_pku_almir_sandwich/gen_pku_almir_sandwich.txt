unit:
	hidden = 0
	sequence 'enable_interaction':
		If hidden == 0: 
			Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'hide':
		Disable interaction.
		Disable object 'g_sandwich'.
		hidden = 1
	sequence 'show':
		Enable object 'g_sandwich'.
		hidden = 0
	sequence 'show_and_enable_interaction':
		Enable interaction.
		Enable object 'g_sandwich'.
		hidden = 0
	sequence 'interact':
		Run sequence 'hide'.
		effect 'effects/payday2/particles/grab/grab_coke':
			parent object( 'rp_gen_pku_cocaine' )
			position v()
	sequence 'load'.
