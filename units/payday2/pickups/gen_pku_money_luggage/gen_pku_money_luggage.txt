unit:
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'open':
		Disable interaction.
		Enable animation_group 'open_case':
			from 0/30
			to 60/30
	sequence 'hide':
		Disable interaction.
		Disable body 'static_body'.
		Disable body 'lid_body'.
		Disable object 'g_luggage'.
	sequence 'show':
		Enable object 'g_luggage'.
		Enable body 'static_body'.
		Enable body 'lid_body'.
		Enable body 'static_body'.
	sequence 'show_and_enable_interaction':
		Enable interaction.
		Enable object 'g_luggage'.
		Enable body 'static_body'.
	sequence 'interact':
		Run sequence 'open'.
