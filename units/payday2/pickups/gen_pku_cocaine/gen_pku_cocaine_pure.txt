unit:
	hidden = 0
	sequence 'enable_interaction':
		If hidden == 0: 
			Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'hide':
		Disable interaction.
		Disable object 'g_coke'.
		Disable decal_mesh 'g_coke'.
		Disable body 'body_cocaine'.
		hidden = 1
	sequence 'show':
		Enable object 'g_coke'.
		Enable decal_mesh 'g_coke'.
		Enable body 'body_cocaine'.
		hidden = 0
	sequence 'show_and_enable_interaction':
		Enable interaction.
		Enable object 'g_coke'.
		Enable decal_mesh 'g_coke'.
		Enable body 'body_cocaine'.
		hidden = 0
	sequence 'interact':
		Run sequence 'hide'.
		effect 'effects/payday2/particles/grab/grab_coke':
			parent object( 'rp_gen_pku_cocaine_pure' )
			position v()
	sequence 'load'.
