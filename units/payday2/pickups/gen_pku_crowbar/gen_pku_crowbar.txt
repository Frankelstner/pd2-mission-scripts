unit:
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'enable_outline':
		material_config 'units/payday2/pickups/gen_pku_crowbar/gen_pku_crowbar_outline'.
	sequence 'disable_outline':
		material_config 'units/payday2/pickups/gen_pku_crowbar/gen_pku_crowbar'.
	sequence 'hide':
		Disable interaction.
		Disable body 'static_body'.
		Disable object 'g_crowbar'.
	sequence 'show':
		Enable interaction.
		Enable body 'static_body'.
		Enable object 'g_crowbar'.
	sequence 'interact':
		Run sequence 'hide'.
	sequence 'load':
		Run sequence 'hide'.
