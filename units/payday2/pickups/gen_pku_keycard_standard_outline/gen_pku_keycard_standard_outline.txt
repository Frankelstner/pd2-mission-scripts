unit:
	sequence 'switch_to_glow_mtr':
		material_config 'units/payday2/pickups/gen_pku_keycard/gen_pku_keycard_contour'.
	sequence 'switch_to_no_glow_mtr':
		material_config 'units/payday2/pickups/gen_pku_keycard/gen_pku_keycard'.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'hide':
		Disable interaction.
		Disable object 'g_keycard'.
	sequence 'show':
		Enable interaction.
		Enable object 'g_keycard'.
	sequence 'interact':
		Run sequence 'hide'.
	sequence 'load'.
