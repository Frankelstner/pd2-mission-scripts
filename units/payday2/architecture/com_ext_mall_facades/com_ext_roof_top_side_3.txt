unit:
	sequence 'damage':
		TRIGGER TIMES 1
		Disable object 'g_glass'.
		Enable object 'g_glass_dmg'.
	sequence 'destroy_glass':
		TRIGGER TIMES 1
		Disable object 'g_glass'.
		Disable object 's_s'.
		Disable object 'g_glass_dmg'.
		Enable object 'g_com_ext_roof_top_side_3_broken'.
		Disable object 'g_com_ext_roof_top_side_3'.
		Play audio 'window_large_shatter' at 'e_01'.
		Disable decal_mesh 'dm_glass_breakable'.
		effect 'effects/payday2/particles/window/storefront_window_large':
			parent object( 'e_01' )
			position v()
		effect 'effects/payday2/particles/window/storefront_window_large':
			parent object( 'e_02' )
			position v()
		Disable body 'glass_body'.
		spawn_unit 'units/payday2/architecture/com_ext_mall_facades/com_ext_roof_top_side_3_debris':
			position object_pos('e_debris')
			rotation object_rot('e_debris')
	body 'glass_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'damage'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_glass'.
