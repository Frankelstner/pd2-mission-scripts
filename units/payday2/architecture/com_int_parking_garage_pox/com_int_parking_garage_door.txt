unit:
	sequence 'open_doors':
		Enable animation_group 'garage_door':
			from 0/30
			loop False
			speed 0.5
			to 60/30
		Play audio 'parking_garage_open' at 'rp_com_int_parking_garage_door'.
	sequence 'close_doors':
		Enable animation_group 'garage_door':
			from 60/30
			loop False
			speed -0.5
			to 0/30
		Play audio 'parking_garage_close' at 'rp_com_int_parking_garage_door'.
