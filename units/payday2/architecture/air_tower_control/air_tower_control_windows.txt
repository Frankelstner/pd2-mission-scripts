unit:
	sequence 'damage_001':
		TRIGGER TIMES 1
		Disable object 'g_glass_001'.
		Enable object 'g_glass_001_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_001' (alarm reason: 'glass').
	sequence 'destroy_001':
		TRIGGER TIMES 1
		Disable object 'g_glass_001_dmg'.
		Disable decal_mesh 'dm_glass_001'.
		Disable body 'glass_001_body'.
		Play audio 'window_small_shatter' at 'e_001'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_001')
			position v()
	sequence 'damage_002':
		TRIGGER TIMES 1
		Disable object 'g_glass_002'.
		Enable object 'g_glass_002_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_002' (alarm reason: 'glass').
	sequence 'destroy_002':
		TRIGGER TIMES 1
		Disable object 'g_glass_002_dmg'.
		Disable decal_mesh 'dm_glass_002'.
		Disable body 'glass_002_body'.
		Play audio 'window_small_shatter' at 'e_002'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_002')
			position v()
	sequence 'damage_003':
		TRIGGER TIMES 1
		Disable object 'g_glass_003'.
		Enable object 'g_glass_003_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_003' (alarm reason: 'glass').
	sequence 'destroy_003':
		TRIGGER TIMES 1
		Disable object 'g_glass_003_dmg'.
		Disable decal_mesh 'dm_glass_003'.
		Disable body 'glass_003_body'.
		Play audio 'window_small_shatter' at 'e_003'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_003')
			position v()
	sequence 'damage_004':
		TRIGGER TIMES 1
		Disable object 'g_glass_004'.
		Enable object 'g_glass_004_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_004' (alarm reason: 'glass').
	sequence 'destroy_004':
		TRIGGER TIMES 1
		Disable object 'g_glass_004_dmg'.
		Disable decal_mesh 'dm_glass_004'.
		Disable body 'glass_004_body'.
		Play audio 'window_small_shatter' at 'e_004'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_004')
			position v()
	sequence 'damage_005':
		TRIGGER TIMES 1
		Disable object 'g_glass_005'.
		Enable object 'g_glass_005_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_005' (alarm reason: 'glass').
	sequence 'destroy_005':
		TRIGGER TIMES 1
		Disable object 'g_glass_005_dmg'.
		Disable decal_mesh 'dm_glass_005'.
		Disable body 'glass_005_body'.
		Play audio 'window_small_shatter' at 'e_005'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_005')
			position v()
	sequence 'damage_006':
		TRIGGER TIMES 1
		Disable object 'g_glass_006'.
		Enable object 'g_glass_006_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_006' (alarm reason: 'glass').
	sequence 'destroy_006':
		TRIGGER TIMES 1
		Disable object 'g_glass_006_dmg'.
		Disable decal_mesh 'dm_glass_006'.
		Disable body 'glass_006_body'.
		Play audio 'window_small_shatter' at 'e_006'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_006')
			position v()
	sequence 'damage_007':
		TRIGGER TIMES 1
		Disable object 'g_glass_007'.
		Enable object 'g_glass_007_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_007' (alarm reason: 'glass').
	sequence 'destroy_007':
		TRIGGER TIMES 1
		Disable object 'g_glass_007_dmg'.
		Disable decal_mesh 'dm_glass_007'.
		Disable body 'glass_007_body'.
		Play audio 'window_small_shatter' at 'e_007'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_007')
			position v()
	sequence 'damage_008':
		TRIGGER TIMES 1
		Disable object 'g_glass_008'.
		Enable object 'g_glass_008_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_008' (alarm reason: 'glass').
	sequence 'destroy_008':
		TRIGGER TIMES 1
		Disable object 'g_glass_008_dmg'.
		Disable decal_mesh 'dm_glass_008'.
		Disable body 'glass_008_body'.
		Play audio 'window_small_shatter' at 'e_008'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_008')
			position v()
	sequence 'damage_009':
		TRIGGER TIMES 1
		Disable object 'g_glass_009'.
		Enable object 'g_glass_009_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_009' (alarm reason: 'glass').
	sequence 'destroy_009':
		TRIGGER TIMES 1
		Disable object 'g_glass_009_dmg'.
		Disable decal_mesh 'dm_glass_009'.
		Disable body 'glass_009_body'.
		Play audio 'window_small_shatter' at 'e_009'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_009')
			position v()
	sequence 'damage_010':
		TRIGGER TIMES 1
		Disable object 'g_glass_010'.
		Enable object 'g_glass_010_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_010' (alarm reason: 'glass').
	sequence 'destroy_010':
		TRIGGER TIMES 1
		Disable object 'g_glass_010_dmg'.
		Disable decal_mesh 'dm_glass_010'.
		Disable body 'glass_010_body'.
		Play audio 'window_small_shatter' at 'e_010'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_010')
			position v()
	sequence 'damage_011':
		TRIGGER TIMES 1
		Disable object 'g_glass_011'.
		Enable object 'g_glass_011_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_011' (alarm reason: 'glass').
	sequence 'destroy_011':
		TRIGGER TIMES 1
		Disable object 'g_glass_011_dmg'.
		Disable decal_mesh 'dm_glass_011'.
		Disable body 'glass_011_body'.
		Play audio 'window_small_shatter' at 'e_011'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_011')
			position v()
	sequence 'damage_012':
		TRIGGER TIMES 1
		Disable object 'g_glass_012'.
		Enable object 'g_glass_012_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_012' (alarm reason: 'glass').
	sequence 'destroy_012':
		TRIGGER TIMES 1
		Disable object 'g_glass_012_dmg'.
		Disable decal_mesh 'dm_glass_012'.
		Disable body 'glass_012_body'.
		Play audio 'window_small_shatter' at 'e_012'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_012')
			position v()
	body 'glass_001_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'damage_001'.
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_001'.
	body 'glass_002_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'damage_002'.
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_002'.
	body 'glass_003_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'damage_003'.
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_003'.
	body 'glass_004_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'damage_004'.
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_004'.
	body 'glass_005_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'damage_005'.
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_005'.
	body 'glass_006_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'damage_006'.
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_006'.
	body 'glass_007_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'damage_007'.
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_007'.
	body 'glass_008_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'damage_008'.
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_008'.
	body 'glass_009_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'damage_009'.
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_009'.
	body 'glass_010_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'damage_010'.
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_010'.
	body 'glass_011_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'damage_011'.
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_011'.
	body 'glass_012_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'damage_012'.
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_012'.
