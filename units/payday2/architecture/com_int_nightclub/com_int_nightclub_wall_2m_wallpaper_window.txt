unit:
	sequence 'seq_break_window':
		TRIGGER TIMES 1
		Disable body 'static_glass'.
		Disable object 'g_glass'.
		Disable decal_mesh 'dm_glass'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass' )
			position v()
		Play audio 'window_medium_shatter' at 'e_glass'.
	body 'static_glass'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'seq_break_window'.
