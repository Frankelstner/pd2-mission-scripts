unit:
	body 'body_glass1'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_glass1_shatter'.
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_glass1_crack'.
	sequence 'int_seq_glass1_shatter':
		Disable object 'g_glass_1'.
		Enable object 'g_glass_1shatter'.
		Disable object 'g_glass_1crack'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_e_1' (alarm reason: 'glass').
	sequence 'int_seq_glass1_crack':
		Disable body 'body_glass1'.
		Disable object 'g_glass_1shatter'.
		Enable object 'g_glass_1crack'.
		Play audio 'window_large_shatter' at 'e_e_1'.
		effect 'effects/payday2/particles/window/storefront_window_large':
			parent object('e_e_1')
			position v()
		Cause alert with 12 m radius.
