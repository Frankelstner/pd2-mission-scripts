unit:
	sequence 'destroy_01':
		TRIGGER TIMES 1
		Disable object 'g_1'.
		Enable object 'g_2'.
		Play audio 'glass_crack' at 'e_e'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_e' (alarm reason: 'glass').
	sequence 'destroy_02':
		TRIGGER TIMES 1
		Disable object 'g_1'.
		Disable object 'g_2'.
		Play audio 'window_medium_shatter' at 'e_e'.
		Disable decal_mesh 'dm_1'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_e')
			position v()
		Disable body 'window1'.
		Cause alert with 12 m radius.
	body 'window1'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_01'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_02'.
