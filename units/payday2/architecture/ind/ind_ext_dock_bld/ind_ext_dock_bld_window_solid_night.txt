unit:
	sequence 'damage':
		TRIGGER TIMES 1
		Disable object 'g_glass'.
		Enable object 'g_glass_dmg'.
		Play audio 'glass_crack' at 'e_01'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_01' (alarm reason: 'glass').
	sequence 'destroy_glass':
		TRIGGER TIMES 1
		Disable object 'g_glass'.
		Disable object 's_s'.
		Disable object 'g_glass_dmg'.
		Disable object 'g_ind_ext_dock_bld_window_solid_night'.
		Play audio 'window_large_shatter' at 'e_01'.
		Disable decal_mesh 'dm_glass_breakable'.
		effect 'effects/payday2/particles/window/storefront_window_large':
			parent object( 'e_01' )
			position v()
		Disable body 'glass_body'.
		Cause alert with 12 m radius.
		spawn_unit 'units/payday2/architecture/ind/ind_ext_dock_bld/ind_ext_dock_bld_window_solid_debris_night':
			position object_pos('e_debris')
			rotation object_rot('e_debris')
	body 'glass_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'damage'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_glass'.
