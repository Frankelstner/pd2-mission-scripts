unit:
	sequence 'random_painting_startup':
		EXECUTE ON STARTUP
		Run sequence 'random_'..pick('annabelle_large_1','annabelle_large_2','annabelle_large_3','annabelle_large_4','annabelle_large_5','annabelle_large_6','annabelle_large_7','annabelle_large_8','annabelle_small_1','ben_qwek_large_1','ben_qwek_large_2','ben_qwek_large_3','ben_qwek_large_4','ben_qwek_large_5','ben_qwek_large_6','ben_qwek_large_7','ben_qwek_large_8','ben_qwek_large_9','ben_qwek_large_10','ben_qwek_large_11','ben_qwek_large_12','ben_qwek_large_13','ben_qwek_large_14','ben_qwek_large_15','ben_qwek_large_16','ben_qwek_medium_1','ben_qwek_medium_2','ben_qwek_medium_3','ben_qwek_medium_4','ben_qwek_small_1').
		startup True
	secure = 0
	important = 0
	sequence 'state_set_important':
		important = 1
		Enable interaction.
		Enable object 'g_important'.
	sequence 'state_interaction_enable':
		Enable interaction.
	sequence 'state_interaction_disable':
		Disable interaction.
	sequence 'open':
		Enable animation_group 'anim':
			from 24/30
			speed 2
			to 88/30
		Play audio 'painting_steel_bar_down' at 'a_bars'.
		If important == 1: Enable interaction.
		Disable body 'hitbox_body_01'.
		Disable body 'hitbox_body_02'.
		Disable body 'hitbox_body_03'.
		Disable body 'hitbox_body_04'.
		Disable body 'hitbox_body_05'.
		Hide graphic_group 'sawicongroup'.
	sequence 'close':
		Enable animation_group 'anim':
			from 0
			to 24/30
		Play audio 'painting_steel_bar_up' at 'a_bars'.
		Disable interaction.
		Enable body 'hitbox_body_01'.
		Enable body 'hitbox_body_02'.
		Enable body 'hitbox_body_03'.
		Enable body 'hitbox_body_04'.
		Enable body 'hitbox_body_05'.
		Enable object 'g_bars_sawed'.
		Show graphic_group 'sawicongroup'.
	sequence 'make_dynamic_body_01':
		secure = vars.var_secure + 1
		Enable object 'g_bar_sawed_01'.
		Disable object 'g_bar_01'.
		Disable body 'hitbox_body_01'.
		Enable body 'dynamic_body_01'.
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('dynamic_target_01')),16,15).
		If secure == 5: Run sequence 'lootable_a'.
	sequence 'make_dynamic_body_02':
		secure = vars.var_secure + 1
		Enable object 'g_bar_sawed_02'.
		Disable object 'g_bar_02'.
		Disable body 'hitbox_body_02'.
		Enable body 'dynamic_body_02'.
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('dynamic_target_02')),25,15).
		If secure == 5: Run sequence 'lootable_a'.
	sequence 'make_dynamic_body_03':
		secure = vars.var_secure + 1
		Enable object 'g_bar_sawed_03'.
		Disable object 'g_bar_03'.
		Disable body 'hitbox_body_03'.
		Enable body 'dynamic_body_03'.
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('dynamic_target_03')),25,15).
		If secure == 5: Run sequence 'lootable_a'.
	sequence 'make_dynamic_body_04':
		secure = vars.var_secure + 1
		Enable object 'g_bar_sawed_04'.
		Disable object 'g_bar_04'.
		Disable body 'hitbox_body_04'.
		Enable body 'dynamic_body_04'.
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('dynamic_target_04')),25,15).
		If secure == 5: Run sequence 'lootable_a'.
	sequence 'make_dynamic_body_05':
		secure = vars.var_secure + 1
		Enable object 'g_bar_sawed_05'.
		Disable object 'g_bar_05'.
		Disable body 'hitbox_body_05'.
		Enable body 'dynamic_body_05'.
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('dynamic_target_05')),16,15).
		If secure == 5: Run sequence 'lootable_a'.
	sequence 'lootable_a':
		If important == 1: Enable interaction.
		Hide graphic_group 'sawicongroup'.
	sequence 'load':
		important = 0
		Disable interaction.
		Hide graphic_group 'grp_paintings'.
		Enable object 'g_important'.
		Disable body 'body_painting_large'.
		Disable body 'body_painting_medium_long'.
		Disable body 'body_painting_medium'.
		Disable body 'body_painting_small'.
		Disable decal_mesh 'g_large'.
		Disable decal_mesh 'g_medium_long'.
		Disable decal_mesh 'g_medium'.
		Disable decal_mesh 'g_small'.
		Play audio 'steal_painting_end' at 'int_location'.
	sequence 'int_seq_painting_small':
		Disable body 'body_painting_large'.
		Disable body 'body_painting_medium_long'.
		Disable body 'body_painting_medium'.
		Enable body 'body_painting_small'.
		Disable decal_mesh 'g_large'.
		Disable decal_mesh 'g_medium_long'.
		Disable decal_mesh 'g_medium'.
		Enable decal_mesh 'g_small'.
		Disable object 'g_large'.
		Disable object 'g_medium_long'.
		Disable object 'g_medium'.
		Enable object 'g_small'.
	sequence 'int_seq_painting_medium':
		Disable body 'body_painting_large'.
		Disable body 'body_painting_medium_long'.
		Enable body 'body_painting_medium'.
		Disable body 'body_painting_small'.
		Disable decal_mesh 'g_large'.
		Disable decal_mesh 'g_medium_long'.
		Enable decal_mesh 'g_medium'.
		Disable decal_mesh 'g_small'.
		Disable object 'g_large'.
		Disable object 'g_medium_long'.
		Enable object 'g_medium'.
		Disable object 'g_small'.
	sequence 'int_seq_painting_medium_long':
		Disable body 'body_painting_large'.
		Enable body 'body_painting_medium_long'.
		Disable body 'body_painting_medium'.
		Disable body 'body_painting_small'.
		Disable decal_mesh 'g_large'.
		Enable decal_mesh 'g_medium_long'.
		Disable decal_mesh 'g_medium'.
		Disable decal_mesh 'g_small'.
		Disable object 'g_large'.
		Enable object 'g_medium_long'.
		Disable object 'g_medium'.
		Disable object 'g_small'.
	sequence 'int_seq_painting_large':
		Enable body 'body_painting_large'.
		Disable body 'body_painting_medium_long'.
		Disable body 'body_painting_medium'.
		Disable body 'body_painting_small'.
		Enable decal_mesh 'g_large'.
		Disable decal_mesh 'g_medium_long'.
		Disable decal_mesh 'g_medium'.
		Disable decal_mesh 'g_small'.
		Enable object 'g_large'.
		Disable object 'g_medium_long'.
		Disable object 'g_medium'.
		Disable object 'g_small'.
	body 'hitbox_body_01'
		Upon receiving 12.5 saw damage, execute:
			Run sequence 'make_dynamic_body_01'.
	body 'hitbox_body_02'
		Upon receiving 12.5 saw damage, execute:
			Run sequence 'make_dynamic_body_02'.
	body 'hitbox_body_03'
		Upon receiving 12.5 saw damage, execute:
			Run sequence 'make_dynamic_body_03'.
	body 'hitbox_body_04'
		Upon receiving 12.5 saw damage, execute:
			Run sequence 'make_dynamic_body_04'.
	body 'hitbox_body_05'
		Upon receiving 12.5 saw damage, execute:
			Run sequence 'make_dynamic_body_05'.
	sequence 'random_annabelle_large_1':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_annabelle_large_1'.
	sequence 'random_annabelle_large_2':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_annabelle_large_2'.
	sequence 'random_annabelle_large_3':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_annabelle_large_3'.
	sequence 'random_annabelle_large_4':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_annabelle_large_4'.
	sequence 'random_annabelle_large_5':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_annabelle_large_5'.
	sequence 'random_annabelle_large_6':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_annabelle_large_6'.
	sequence 'random_annabelle_large_7':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_annabelle_large_7'.
	sequence 'random_annabelle_large_8':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_annabelle_large_8'.
	sequence 'random_ben_qwek_large_1':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_large_1'.
	sequence 'random_ben_qwek_large_2':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_large_2'.
	sequence 'random_ben_qwek_large_3':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_large_3'.
	sequence 'random_ben_qwek_large_4':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_large_4'.
	sequence 'random_ben_qwek_large_5':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_large_5'.
	sequence 'random_ben_qwek_large_6':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_large_6'.
	sequence 'random_ben_qwek_large_7':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_large_7'.
	sequence 'random_ben_qwek_large_8':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_large_8'.
	sequence 'random_ben_qwek_large_9':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_large_9'.
	sequence 'random_ben_qwek_large_10':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_large_10'.
	sequence 'random_ben_qwek_large_11':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_large_11'.
	sequence 'random_ben_qwek_large_12':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_large_12'.
	sequence 'random_ben_qwek_large_13':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_large_13'.
	sequence 'random_ben_qwek_large_14':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_large_14'.
	sequence 'random_ben_qwek_large_15':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_large_15'.
	sequence 'random_ben_qwek_large_16':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_large_16'.
	sequence 'random_darius_large_1':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_darius_large_1'.
	sequence 'random_darius_large_2':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_darius_large_2'.
	sequence 'random_darius_large_3':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_darius_large_3'.
	sequence 'random_darius_large_4':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_darius_large_4'.
	sequence 'random_darius_large_5':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_darius_large_5'.
	sequence 'random_darius_large_6':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_darius_large_6'.
	sequence 'random_ray_large_1':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ray_large_1'.
	sequence 'random_ray_large_2':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ray_large_2'.
	sequence 'random_ray_large_3':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ray_large_3'.
	sequence 'random_ray_large_4':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ray_large_4'.
	sequence 'random_ray_large_5':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ray_large_5'.
	sequence 'random_ray_large_6':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ray_large_6'.
	sequence 'random_ray_large_7':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ray_large_7'.
	sequence 'random_ray_large_8':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ray_large_8'.
	sequence 'random_ray_large_9':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ray_large_9'.
	sequence 'random_ray_large_10':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ray_large_10'.
	sequence 'random_ray_large_11':
		Run sequence 'int_seq_painting_large'.
		material_config 'units/payday2/props/shared_textures/random_ray_large_11'.
	sequence 'random_ben_qwek_medium_1':
		Run sequence 'int_seq_painting_medium'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_medium_1'.
	sequence 'random_ben_qwek_medium_2':
		Run sequence 'int_seq_painting_medium'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_medium_2'.
	sequence 'random_ben_qwek_medium_3':
		Run sequence 'int_seq_painting_medium'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_medium_3'.
	sequence 'random_ben_qwek_medium_4':
		Run sequence 'int_seq_painting_medium_long'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_medium_4'.
	sequence 'random_darius_medium_1':
		Run sequence 'int_seq_painting_medium_long'.
		material_config 'units/payday2/props/shared_textures/random_darius_medium_1'.
	sequence 'random_darius_medium_2':
		Run sequence 'int_seq_painting_medium_long'.
		material_config 'units/payday2/props/shared_textures/random_darius_medium_2'.
	sequence 'random_darius_medium_3':
		Run sequence 'int_seq_painting_medium_long'.
		material_config 'units/payday2/props/shared_textures/random_darius_medium_3'.
	sequence 'random_darius_medium_4':
		Run sequence 'int_seq_painting_medium_long'.
		material_config 'units/payday2/props/shared_textures/random_darius_medium_4'.
	sequence 'random_darius_medium_5':
		Run sequence 'int_seq_painting_medium_long'.
		material_config 'units/payday2/props/shared_textures/random_darius_medium_5'.
	sequence 'random_darius_medium_6':
		Run sequence 'int_seq_painting_medium_long'.
		material_config 'units/payday2/props/shared_textures/random_darius_medium_6'.
	sequence 'random_darius_medium_7':
		Run sequence 'int_seq_painting_medium_long'.
		material_config 'units/payday2/props/shared_textures/random_darius_medium_7'.
	sequence 'random_darius_medium_8':
		Run sequence 'int_seq_painting_medium'.
		material_config 'units/payday2/props/shared_textures/random_darius_medium_8'.
	sequence 'random_darius_medium_9':
		Run sequence 'int_seq_painting_medium'.
		material_config 'units/payday2/props/shared_textures/random_darius_medium_9'.
	sequence 'random_darius_medium_10':
		Run sequence 'int_seq_painting_medium'.
		material_config 'units/payday2/props/shared_textures/random_darius_medium_10'.
	sequence 'random_darius_medium_11':
		Run sequence 'int_seq_painting_medium'.
		material_config 'units/payday2/props/shared_textures/random_darius_medium_11'.
	sequence 'random_darius_medium_12':
		Run sequence 'int_seq_painting_medium'.
		material_config 'units/payday2/props/shared_textures/random_darius_medium_12'.
	sequence 'random_darius_medium_13':
		Run sequence 'int_seq_painting_medium'.
		material_config 'units/payday2/props/shared_textures/random_darius_medium_13'.
	sequence 'random_ray_medium_1':
		Run sequence 'int_seq_painting_medium_long'.
		material_config 'units/payday2/props/shared_textures/random_ray_medium_1'.
	sequence 'random_ray_medium_2':
		Run sequence 'int_seq_painting_medium_long'.
		material_config 'units/payday2/props/shared_textures/random_ray_medium_2'.
	sequence 'random_ray_medium_3':
		Run sequence 'int_seq_painting_medium_long'.
		material_config 'units/payday2/props/shared_textures/random_ray_medium_3'.
	sequence 'random_ray_medium_4':
		Run sequence 'int_seq_painting_medium_long'.
		material_config 'units/payday2/props/shared_textures/random_ray_medium_4'.
	sequence 'random_ray_medium_5':
		Run sequence 'int_seq_painting_medium_long'.
		material_config 'units/payday2/props/shared_textures/random_ray_medium_5'.
	sequence 'random_ray_medium_6':
		Run sequence 'int_seq_painting_medium'.
		material_config 'units/payday2/props/shared_textures/random_ray_medium_6'.
	sequence 'random_ray_medium_7':
		Run sequence 'int_seq_painting_medium'.
		material_config 'units/payday2/props/shared_textures/random_ray_medium_7'.
	sequence 'random_annabelle_small_1':
		Run sequence 'int_seq_painting_small'.
		material_config 'units/payday2/props/shared_textures/random_annabelle_small_1'.
	sequence 'random_ben_qwek_small_1':
		Run sequence 'int_seq_painting_small'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_small_1'.
	sequence 'random_ben_qwek_small_2':
		Run sequence 'int_seq_painting_small'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_small_2'.
	sequence 'random_ben_qwek_small_3':
		Run sequence 'int_seq_painting_small'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_small_3'.
	sequence 'random_ben_qwek_small_4':
		Run sequence 'int_seq_painting_small'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_small_4'.
	sequence 'random_ben_qwek_small_5':
		Run sequence 'int_seq_painting_small'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_small_5'.
	sequence 'random_ben_qwek_small_6':
		Run sequence 'int_seq_painting_small'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_small_6'.
	sequence 'random_ben_qwek_small_7':
		Run sequence 'int_seq_painting_small'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_small_7'.
	sequence 'random_ben_qwek_small_8':
		Run sequence 'int_seq_painting_small'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_small_8'.
	sequence 'random_ben_qwek_small_9':
		Run sequence 'int_seq_painting_small'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_small_9'.
	sequence 'random_ben_qwek_small_10':
		Run sequence 'int_seq_painting_small'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_small_10'.
	sequence 'random_ben_qwek_small_11':
		Run sequence 'int_seq_painting_small'.
		material_config 'units/payday2/props/shared_textures/random_ben_qwek_small_11'.
	sequence 'random_ray_small_1':
		Run sequence 'int_seq_painting_small'.
		material_config 'units/payday2/props/shared_textures/random_ray_small_1'.
	sequence 'random_ray_small_2':
		Run sequence 'int_seq_painting_small'.
		material_config 'units/payday2/props/shared_textures/random_ray_small_2'.
	sequence 'random_ray_small_3':
		Run sequence 'int_seq_painting_small'.
		material_config 'units/payday2/props/shared_textures/random_ray_small_3'.
	sequence 'random_ray_small_4':
		Run sequence 'int_seq_painting_small'.
		material_config 'units/payday2/props/shared_textures/random_ray_small_4'.
	sequence 'random_ray_small_5':
		Run sequence 'int_seq_painting_small'.
		material_config 'units/payday2/props/shared_textures/random_ray_small_5'.
	sequence 'random_ray_small_6':
		Run sequence 'int_seq_painting_small'.
		material_config 'units/payday2/props/shared_textures/random_ray_small_6'.
