unit:
	body 'body_window'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'seq_damage_window'.
		Upon receiving 2 bullet hits or 10 explosion damage or 20 melee damage, execute:
			Run sequence 'seq_break_window'.
	sequence 'seq_break_window':
		TRIGGER TIMES 1
		Disable body 'body_window'.
		Disable decal_mesh 'dm_glass_breakable'.
		Disable object 'g_glass_dmg'.
		effect 'effects/payday2/particles/window/mallcrasher_window_large':
			parent object( 'e_window' )
			position v()
		Play audio 'window_medium_shatter' at 'e_window'.
		Cause alert with 12 m radius.
	sequence 'seq_damage_window':
		TRIGGER TIMES 1
		Disable object 'g_g'.
		Enable object 'g_glass_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window' (alarm reason: 'glass').
