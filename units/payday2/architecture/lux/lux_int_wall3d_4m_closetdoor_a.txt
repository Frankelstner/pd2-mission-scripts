unit:
	sequence 'anim_door_close':
		Enable animation_group 'anim':
			from 0/30
			to 25/30
	sequence 'anim_door_open':
		Enable animation_group 'anim':
			from 25/30
			speed -1
			to 0/30
	sequence 'state_door_close':
		Enable animation_group 'anim':
			from 25/30
			speed 0
			to 25/30
	sequence 'state_door_open':
		Enable animation_group 'anim':
			from 0/30
			speed 0
			to 0/30
