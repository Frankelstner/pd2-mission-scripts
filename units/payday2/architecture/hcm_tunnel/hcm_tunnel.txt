unit:
	sequence 'action_explode_wall':
		Enable animation_group 'anim':
			from 0
			speed 0.8
		Enable object 'g_edge'.
		Disable body 'body_wall'.
		Enable body 'body_nav_walk'.
		Play audio 'wall_explosion_large' at 'e_effect'.
		effect 'effects/payday2/particles/explosions/c4_wall_explosion':
			parent object( 'e_effect' )
			position v()
