unit:
	allow_interaction = 0
	body 'hitbox'
		Upon receiving 50 saw damage, execute:
			Run sequence 'open_door_with_saw'.
	sequence 'open_door_with_saw':
		Disable interaction.
		Run sequence 'deactivate_door'.
		Enable animation_group 'anim':
			from 0/30
			speed 1
			to 20
		Play audio 'steel_door_open' at 'anim_door'.
		Disable object 'g_door'.
		Disable object 's_door'.
		Enable object 'g_door_drill'.
		Enable object 's_door_drill'.
		Hide graphic_group 'sawicongroup'.
		Hide graphic_group 'ghosticongroup'.
		Run sequence 'done_opened'.
		Disable body 'hitbox'.
		Run sequence 'done_opened'.
	sequence 'close_door':
		Enable animation_group 'anim':
			from 20/30
			speed -1
			to 0/30
		Play audio 'steel_door_open' at 'anim_door'.
	sequence 'open_door':
		Run sequence 'deactivate_door'.
		Run sequence 'done_opened'.
		Enable animation_group 'anim':
			from 0/30
			speed 1
			to 20
		Play audio 'steel_door_open' at 'anim_door'.
		Disable body 'hitbox'.
		Hide graphic_group 'sawicongroup'.
		Hide graphic_group 'ghosticongroup'.
		Disable interaction.
		allow_interaction = 1
	sequence 'int_seq_explosion':
		effect 'effects/particles/explosions/explosion_grenade':
			parent object( 'a_shp_charge' )
			position v()
		Play audio 'swat_explosion' at 'a_shp_charge'.
		Disable body 'body_door_whole'.
		Enable body 'body_door_broken'.
		Disable object 'g_door'.
		Disable object 's_door'.
		Enable object 'g_door_expl'.
		Enable object 's_door_expl'.
		Hide graphic_group 'sawicongroup'.
		Hide graphic_group 'ghosticongroup'.
		Call function: base.deactivate()
		Disable body 'hitbox'.
		Run sequence 'done_opened'.
		Disable interaction.
		allow_interaction = 1
	sequence 'activate_door':
		Call function: base.activate()
	sequence 'deactivate_door':
		Call function: base.deactivate()
		Disable body 'hitbox'.
	sequence 'explode_door':
		Run sequence 'int_seq_explosion'.
	sequence 'power_off':
		Call function: base.set_powered(False)
	sequence 'power_on':
		Call function: base.set_powered(True)
	sequence 'turn_off':
		Call function: base.set_on(False)
	sequence 'turn_on':
		Call function: base.set_on(True)
	sequence 'door_opened'.
	sequence 'door_closed'.
	sequence 'c4_placed'.
	sequence 'c4_completed'.
	sequence 'all_c4_placed'.
	sequence 'drill_placed'.
	sequence 'drill_jammed'.
	sequence 'drill_resumed'.
	sequence 'drill_power_off'.
	sequence 'drill_power_on'.
	sequence 'drill_completed'.
	sequence 'all_drill_placed'.
	sequence 'done_opened'.
	sequence 'interact'.
	sequence 'interaction_enabled':
		If allow_interaction == 0: Enable interaction.
		If allow_interaction == 0: Show graphic_group 'ghosticongroup'.
	sequence 'interaction_disabled':
		Disable interaction.
		Hide graphic_group 'ghosticongroup'.
	sequence 'state_door_open':
		Disable interaction.
		Enable animation_group 'anim':
			from 20/30
			speed 0
			to 20/30
		Run sequence 'done_opened'.
		Disable body 'hitbox'.
		Hide graphic_group 'sawicongroup'.
		Hide graphic_group 'ghosticongroup'.
	sequence 'state_door_close':
		Enable animation_group 'anim':
			from 0/30
			speed 0
			to 0/30
		Run sequence 'door_closed'.
		Enable body 'hitbox'.
		Show graphic_group 'sawicongroup'.
	sequence 'state_door_hide':
		Disable body 'body_door_whole'.
		Disable body 'body_door_broken'.
		Disable body 'hitbox'.
		Disable object 'g_door'.
		Disable object 's_door'.
		Disable object 'g_door_drill'.
		Disable object 's_door_drill'.
		Disable object 'g_door_expl'.
		Disable object 's_door_expl'.
		Hide graphic_group 'sawicongroup'.
		Hide graphic_group 'ghosticongroup'.
		Run sequence 'deactivate_door'.
		allow_interaction = 1
	sequence 'state_door_show':
		Enable body 'body_door_whole'.
		Disable body 'body_door_broken'.
		Enable body 'hitbox'.
		Show graphic_group 'sawicongroup'.
		Enable object 'g_door'.
		Enable object 's_door'.
		Disable object 'g_door_drill'.
		Disable object 's_door_drill'.
		Disable object 'g_door_expl'.
		Disable object 's_door_expl'.
		allow_interaction = 0
