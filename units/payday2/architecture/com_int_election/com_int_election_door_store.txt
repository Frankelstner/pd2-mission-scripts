unit:
	sequence 'state_door_open':
		Disable interaction.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_open_close':
			from 15/30
			speed 0
			to 15/30
		Run sequence 'done_opened'.
		Hide graphic_group 'icon'.
	sequence 'state_door_close':
		Enable interaction.
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_open_close':
			from 0/30
			speed 0
			to 0/30
		Show graphic_group 'icon'.
	sequence 'state_vis_hide':
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Disable body 'static_body'.
		Disable body 'body_door'.
		Disable body 'body_door2'.
		Disable body 'static_glass_01'.
		Disable body 'static_glass_02'.
		Disable body 'static_glass_03'.
		Disable body 'door_01_glass'.
		Disable body 'door_02_glass'.
		Hide graphic_group 'doors'.
	sequence 'state_vis_show':
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
		Enable body 'static_body'.
		Enable body 'body_door'.
		Enable body 'body_door2'.
		Enable body 'static_glass_01'.
		Enable body 'static_glass_02'.
		Enable body 'static_glass_03'.
		Enable body 'door_01_glass'.
		Enable body 'door_02_glass'.
		Show graphic_group 'doors'.
		Show graphic_group 'icon'.
	sequence 'anim_open_door':
		Enable animation_group 'anim_open_close':
			from 0/30
			to 15/30
		Play audio 'generic_door_metal_open' at 'a_door_01'.
		Play audio 'generic_door_metal_open' at 'a_door_02'.
		Hide graphic_group 'icon'.
	sequence 'anim_explosion_in':
		Play audio 'c4_explode_metal' at 'e_door_dst'.
		Enable animation_group 'anim_open_close':
			from 0/30
			speed 3
			to 15/30
		Hide graphic_group 'icon'.
	sequence 'anim_explosion_out':
		Play audio 'c4_explode_metal' at 'e_door_dst'.
		Enable animation_group 'anim_open_close':
			from 0/30
			speed 3
			to 15/30
		Hide graphic_group 'icon'.
	body 'body_hitbox_door_handle_in'
		Upon receiving 4 bullet hits, execute:
			Run sequence 'int_seq_bullet_hit_in'.
		Upon receiving 30 saw damage, execute:
			Run sequence 'int_seq_saw_in'.
		Upon receiving 10 explosion damage, execute:
			Run sequence 'int_seq_explosion_in'.
	body 'body_hitbox_door_handle_out'
		Upon receiving 4 bullet hits, execute:
			Run sequence 'int_seq_bullet_hit_out'.
		Upon receiving 30 saw damage, execute:
			Run sequence 'int_seq_saw_out'.
		Upon receiving 10 explosion damage, execute:
			Run sequence 'int_seq_explosion_out'.
	sequence 'int_seq_open':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
	sequence 'int_seq_breach_common':
		Disable interaction.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
	sequence 'int_seq_bullet_hit_in':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
	sequence 'int_seq_bullet_hit_out':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
	sequence 'int_seq_saw_in':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
	sequence 'int_seq_saw_out':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
	sequence 'int_seq_explosion_in':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_explosion_in'.
		Run sequence 'done_exploded'.
		Run sequence 'done_opened'.
	sequence 'int_seq_explosion_out':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_explosion_out'.
		Run sequence 'done_exploded'.
		Run sequence 'done_opened'.
	sequence 'interact':
		Run sequence 'int_seq_open'.
	sequence 'sobj_swat_breach_in':
		Run sequence 'int_seq_slam_door'. (DELAY 63/30)
		Run sequence 'done_swat_breach'.
	sequence 'sobj_swat_breach_out':
		Run sequence 'anim_explosion_out'. (DELAY 63/30)
		Run sequence 'done_swat_breach'.
		Run sequence 'done_opened'.
	sequence 'int_seq_slam_door':
		Enable animation_group 'anim_open_close':
			from 0/30
			speed 3
			to 15/30
		Play audio 'generic_door_metal_open' at 'a_door_01'.
		Play audio 'generic_door_metal_open' at 'a_door_02'.
		Run sequence 'int_seq_breach_common'.
		Run sequence 'done_opened'.
	sequence 'open_door':
		Run sequence 'int_seq_open'.
	sequence 'done_exploded'.
	sequence 'done_opened'.
	sequence 'done_swat_breach'.
	sequence 'crack_glass_01':
		Disable object 'g_glass_01'.
		Enable object 'g_glass_01_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_01' (alarm reason: 'glass').
	sequence 'destroy_glass_01':
		Disable body 'static_glass_01'.
		Disable object 'g_glass_01_dmg'.
		Disable decal_mesh 'dm_glass_01'.
		Play audio 'window_large_shatter' at 'e_glass_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_01')
			position v()
		Cause alert with 12 m radius.
	sequence 'crack_glass_02':
		Disable object 'g_glass_02'.
		Enable object 'g_glass_02_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_02' (alarm reason: 'glass').
	sequence 'destroy_glass_02':
		Disable body 'static_glass_02'.
		Disable object 'g_glass_02_dmg'.
		Disable decal_mesh 'dm_glass_02'.
		Play audio 'window_large_shatter' at 'e_glass_02'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_02')
			position v()
		Cause alert with 12 m radius.
	sequence 'crack_glass_03':
		Disable object 'g_glass_03'.
		Enable object 'g_glass_03_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_01' (alarm reason: 'glass').
	sequence 'destroy_glass_03':
		Disable body 'static_glass_03'.
		Disable object 'g_glass_03_dmg'.
		Disable decal_mesh 'dm_glass_03'.
		Play audio 'window_large_shatter' at 'e_glass_03'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_03')
			position v()
		Cause alert with 12 m radius.
	sequence 'crack_glass_door_01':
		Disable object 'g_glass_door_01'.
		Enable object 'g_glass_door_01_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_door_01' (alarm reason: 'glass').
	sequence 'destroy_glass_door_01':
		Disable body 'door_01_glass'.
		Disable object 'g_glass_door_01_dmg'.
		Disable decal_mesh 'dm_glass_door_01'.
		Play audio 'window_large_shatter' at 'e_glass_door_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_door_01')
			position v()
		Cause alert with 12 m radius.
	sequence 'crack_glass_door_02':
		Disable object 'g_glass_door_02'.
		Enable object 'g_glass_door_02_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_door_02' (alarm reason: 'glass').
	sequence 'destroy_glass_door_02':
		Disable body 'door_02_glass'.
		Disable object 'g_glass_door_02_dmg'.
		Disable decal_mesh 'dm_glass_door_02'.
		Play audio 'window_large_shatter' at 'e_glass_door_02'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_door_02')
			position v()
		Cause alert with 12 m radius.
	body 'static_glass_01'
		Upon receiving 4 bullet hits or 10 collision damage or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crack_glass_01'.
		Upon receiving 6 bullet hits or 10 collision damage or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_glass_01'.
	body 'static_glass_02'
		Upon receiving 4 bullet hits or 10 collision damage or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crack_glass_02'.
		Upon receiving 6 bullet hits or 10 collision damage or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_glass_02'.
	body 'static_glass_03'
		Upon receiving 4 bullet hits or 10 collision damage or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crack_glass_03'.
		Upon receiving 6 bullet hits or 10 collision damage or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_glass_03'.
	body 'door_01_glass'
		Upon receiving 4 bullet hits or 10 collision damage or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crack_glass_door_01'.
		Upon receiving 6 bullet hits or 10 collision damage or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_glass_door_01'.
	body 'door_02_glass'
		Upon receiving 4 bullet hits or 10 collision damage or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crack_glass_door_02'.
		Upon receiving 6 bullet hits or 10 collision damage or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_glass_door_02'.
