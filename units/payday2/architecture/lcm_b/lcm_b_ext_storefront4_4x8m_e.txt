unit:
	body 'body_glass1'
		Upon receiving 1 bullet hit or 20 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_glass1_shatter'.
		Upon receiving 2 bullet hits or 50 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_glass1_crack'.
	sequence 'int_seq_glass1_shatter':
		Disable object 'g_glass_1'.
		Enable object 'g_glass_1shatter'.
		Disable object 'g_glass_1crack'.
	sequence 'int_seq_glass1_crack':
		Disable body 'body_glass1'.
		Disable object 'g_glass_1shatter'.
		Enable object 'g_glass_1crack'.
		Play audio 'window_large_shatter' at 'e_e_1'.
		effect 'effects/payday2/particles/window/storefront_window_large':
			parent object('e_e_1')
			position v()
		Cause alert with 12 m radius.
	body 'body_glass2'
		Upon receiving 1 bullet hit or 20 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_glass2_shatter'.
		Upon receiving 2 bullet hits or 50 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_glass2_crack'.
	sequence 'int_seq_glass2_shatter':
		Disable object 'g_glass_2'.
		Enable object 'g_glass_2shatter'.
		Disable object 'g_glass_2crack'.
	sequence 'int_seq_glass2_crack':
		Disable body 'body_glass2'.
		Disable object 'g_glass_2shatter'.
		Enable object 'g_glass_2crack'.
		Play audio 'window_large_shatter' at 'e_e_2'.
		effect 'effects/payday2/particles/window/storefront_window_large':
			parent object('e_e_2')
			position v()
		Cause alert with 12 m radius.
	body 'body_glass3'
		Upon receiving 1 bullet hit or 20 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_glass3_shatter'.
		Upon receiving 2 bullet hits or 50 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_glass3_crack'.
	sequence 'int_seq_glass3_shatter':
		Disable object 'g_glass_3'.
		Enable object 'g_glass_3shatter'.
		Disable object 'g_glass_3crack'.
	sequence 'int_seq_glass3_crack':
		Disable body 'body_glass3'.
		Disable object 'g_glass_3shatter'.
		Enable object 'g_glass_3crack'.
		Play audio 'window_large_shatter' at 'e_e_3'.
		effect 'effects/payday2/particles/window/storefront_window_large':
			parent object('e_e_3')
			position v()
		Cause alert with 12 m radius.
	body 'body_glass4'
		Upon receiving 1 bullet hit or 20 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_glass4_shatter'.
		Upon receiving 2 bullet hits or 50 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_glass4_crack'.
	sequence 'int_seq_glass4_shatter':
		Disable object 'g_glass_4'.
		Enable object 'g_glass_4shatter'.
		Disable object 'g_glass_4crack'.
	sequence 'int_seq_glass4_crack':
		Disable body 'body_glass4'.
		Disable object 'g_glass_4shatter'.
		Enable object 'g_glass_4crack'.
		Play audio 'window_large_shatter' at 'e_e_4'.
		effect 'effects/payday2/particles/window/storefront_window_large':
			parent object('e_e_4')
			position v()
		Cause alert with 12 m radius.
	body 'body_glass5'
		Upon receiving 1 bullet hit or 20 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_glass5_shatter'.
		Upon receiving 2 bullet hits or 50 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_glass5_crack'.
	sequence 'int_seq_glass5_shatter':
		Disable object 'g_glass_5'.
		Enable object 'g_glass_5shatter'.
		Disable object 'g_glass_5crack'.
	sequence 'int_seq_glass5_crack':
		Disable body 'body_glass5'.
		Disable object 'g_glass_5shatter'.
		Enable object 'g_glass_5crack'.
		Play audio 'window_large_shatter' at 'e_e_5'.
		effect 'effects/payday2/particles/window/storefront_window_large':
			parent object('e_e_5')
			position v()
		Cause alert with 12 m radius.
	body 'body_glass6'
		Upon receiving 1 bullet hit or 20 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_glass6_shatter'.
		Upon receiving 2 bullet hits or 50 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_glass6_crack'.
	sequence 'int_seq_glass6_shatter':
		Disable object 'g_glass_6'.
		Enable object 'g_glass_6shatter'.
		Disable object 'g_glass_6crack'.
	sequence 'int_seq_glass6_crack':
		Disable body 'body_glass6'.
		Disable object 'g_glass_6shatter'.
		Enable object 'g_glass_6crack'.
		Play audio 'window_large_shatter' at 'e_e_6'.
		effect 'effects/payday2/particles/window/storefront_window_large':
			parent object('e_e_6')
			position v()
		Cause alert with 12 m radius.
	body 'body_glass7'
		Upon receiving 1 bullet hit or 20 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_glass7_shatter'.
		Upon receiving 2 bullet hits or 50 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_glass7_crack'.
	sequence 'int_seq_glass7_shatter':
		Disable object 'g_glass_7'.
		Enable object 'g_glass_7shatter'.
		Disable object 'g_glass_7crack'.
	sequence 'int_seq_glass7_crack':
		Disable body 'body_glass7'.
		Disable object 'g_glass_7shatter'.
		Enable object 'g_glass_7crack'.
		Play audio 'window_large_shatter' at 'e_e_7'.
		effect 'effects/payday2/particles/window/storefront_window_large':
			parent object('e_e_7')
			position v()
		Cause alert with 12 m radius.
