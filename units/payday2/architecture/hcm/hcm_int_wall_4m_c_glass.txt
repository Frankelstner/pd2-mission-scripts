unit:
	body 'body_window_left'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'seq_break_window_left'.
	sequence 'seq_break_window_left':
		TRIGGER TIMES 1
		Disable body 'body_window_left'.
		Disable decal_mesh 'g_window_left'.
		Disable object 'g_window_left'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_left' )
			position v()
		Play audio 'window_medium_shatter' at 'e_window_left'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_left' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_top'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'seq_break_window_top'.
	sequence 'seq_break_window_top':
		TRIGGER TIMES 1
		Disable body 'body_window_top'.
		Disable decal_mesh 'g_window_top'.
		Disable object 'g_window_top'.
		effect 'effects/payday2/particles/window/jewelry_counter_1':
			parent object( 'e_window_top' )
			position v()
		Play audio 'window_medium_shatter' at 'e_window_top'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_top' (alarm reason: 'glass').
		Cause alert with 12 m radius.
	body 'body_window_right'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'seq_break_window_right'.
	sequence 'seq_break_window_right':
		TRIGGER TIMES 1
		Disable body 'body_window_right'.
		Disable decal_mesh 'g_window_right'.
		Disable object 'g_window_right'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_right' )
			position v()
		Play audio 'window_medium_shatter' at 'e_window_right'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_window_right' (alarm reason: 'glass').
		Cause alert with 12 m radius.
