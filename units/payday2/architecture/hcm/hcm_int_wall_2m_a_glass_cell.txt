unit:
	sequence 'spawn_windows':
		EXECUTE ON STARTUP
		spawn_unit 'units/payday2/props/hcm_prop_int_spawned_glass/hcm_prop_int_spawned_glass':
			position object_pos('e_glass_01')
			rotation object_rot('e_glass_01')
		spawn_unit 'units/payday2/props/hcm_prop_int_spawned_glass/hcm_prop_int_spawned_glass':
			position object_pos('e_glass_02')
			rotation object_rot('e_glass_02')
		spawn_unit 'units/payday2/props/hcm_prop_int_spawned_glass/hcm_prop_int_spawned_glass':
			position object_pos('e_glass_03')
			rotation object_rot('e_glass_03')
		spawn_unit 'units/payday2/props/hcm_prop_int_spawned_glass/hcm_prop_int_spawned_glass':
			position object_pos('e_glass_04')
			rotation object_rot('e_glass_04')
		spawn_unit 'units/payday2/props/hcm_prop_int_spawned_glass/hcm_prop_int_spawned_glass':
			position object_pos('e_glass_05')
			rotation object_rot('e_glass_05')
		spawn_unit 'units/payday2/props/hcm_prop_int_spawned_glass/hcm_prop_int_spawned_glass':
			position object_pos('e_glass_06')
			rotation object_rot('e_glass_06')
		spawn_unit 'units/payday2/props/hcm_prop_int_spawned_glass/hcm_prop_int_spawned_glass':
			position object_pos('e_glass_07')
			rotation object_rot('e_glass_07')
		spawn_unit 'units/payday2/props/hcm_prop_int_spawned_glass/hcm_prop_int_spawned_glass':
			position object_pos('e_glass_08')
			rotation object_rot('e_glass_08')
		startup True
