unit:
	sequence 'state_interaction_enabled':
		Enable interaction.
		Enable object 'g_outline'.
	sequence 'state_interaction_disabled':
		Disable interaction.
		Disable object 'g_outline'.
	sequence 'show_c4_sawblade':
		Call function: base.activate()
		Enable object 'g_icon_saw'.
		Enable object 'g_icon_saw_lod01'.
	sequence 'hide_c4_sawblade':
		Call function: base.deactivate()
		Disable object 'g_icon_saw'.
		Disable object 'g_icon_saw_lod01'.
	sequence 'all_c4_placed'.
	sequence 'c4_placed'.
	sequence 'c4_completed'.
	sequence 'explode_door':
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'c4_icon_locator' )
			position v()
		Play audio 'c4_explode_wood' at 'a_fall'.
	sequence 'door_opened':
		Run sequence 'interact'.
	body 'static_body'
		Upon receiving 75 saw damage, execute:
			Run sequence 'interact'.
	sequence 'interact':
		Run sequence 'tree_fall_seq'.
	sequence 'tree_fall_seq':
		Disable body 'static_body'.
		Enable body 'body_stub'.
		Enable body 'body_tree'.
		Disable decal_mesh 'dm_pnau_tree_pine_02_d'.
		Enable decal_mesh 'g_stub'.
		Enable decal_mesh 'g_tree'.
		Disable object 'g_pnau_tree_pine_02_d'.
		Enable object 'g_stub'.
		Enable object 'g_tree'.
		Enable animation_group 'anim'.
		Disable object 'g_icon_saw'.
		Disable object 'g_icon_saw_lod01'.
		Disable interaction.
		Disable object 'g_outline'.
		Call function: base.deactivate()
		Play audio 'tree_fall' at 'a_fall'.
		effect 'effects/payday2/environment/leaf_winded_direction_burst': (DELAY 177/30)
			parent object( 'effect_leaf_01' )
			position v()
		effect 'effects/payday2/environment/leaf_winded_direction_burst': (DELAY 177/30)
			parent object( 'effect_leaf_02' )
			position v()
		effect 'effects/payday2/environment/leaf_winded_direction_burst': (DELAY 184/30)
			parent object( 'effect_leaf_03' )
			position v()
		effect 'effects/payday2/environment/leaf_winded_direction_burst': (DELAY 184/30)
			parent object( 'effect_leaf_04' )
			position v()
