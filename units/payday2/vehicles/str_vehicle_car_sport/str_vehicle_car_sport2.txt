unit:
	sequence 'crc_window_front':
		TRIGGER TIMES 1
		Disable object 'g_window_front'.
		Enable object 'g_window_front_dmg'.
	sequence 'sht_window_front':
		TRIGGER TIMES 1
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_front' )
			position v()
		Play audio 'window_small_shatter' at 'e_window_front'.
		Disable object 'g_window_front_dmg'.
		Disable body 'window_front'.
		Disable decal_mesh 'g_window_front'.
	body 'window_front'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crc_window_front'.
	sequence 'crc_window_back':
		TRIGGER TIMES 1
		Disable object 'g_window_back'.
		Enable object 'g_window_back_dmg'.
	sequence 'sht_window_back':
		TRIGGER TIMES 1
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_back' )
			position v()
		Play audio 'window_small_shatter' at 'e_window_back'.
		Disable object 'g_window_back_dmg'.
		Disable body 'window_back'.
		Disable decal_mesh 'g_window_back'.
	body 'window_back'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crc_window_back'.
	sequence 'crc_window_right':
		TRIGGER TIMES 1
		Disable object 'g_window_right'.
		Enable object 'g_window_right_dmg'.
	sequence 'sht_window_right':
		TRIGGER TIMES 1
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_right' )
			position v()
		Play audio 'window_small_shatter' at 'e_window_right'.
		Disable object 'g_window_right_dmg'.
		Disable body 'window_right'.
		Disable decal_mesh 'g_window_right'.
	body 'window_right'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crc_window_right'.
		Upon receiving 3 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'sht_window_right'.
	sequence 'crc_window_left':
		TRIGGER TIMES 1
		Disable object 'g_window_left'.
		Enable object 'g_window_left_dmg'.
	sequence 'sht_window_left':
		TRIGGER TIMES 1
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_left' )
			position v()
		Play audio 'window_small_shatter' at 'e_window_left'.
		Disable object 'g_window_left_dmg'.
		Disable body 'window_left'.
		Disable decal_mesh 'g_window_left'.
	body 'window_left'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crc_window_left'.
		Upon receiving 3 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'sht_window_left'.
