unit:
	sequence 'drive_forward':
		Disable interaction.
		Hide graphic_group 'icon'.
		Enable animation_group 'anim':
			from 0/30
			to 850/30
		Run sequence 'done_drive_forward'. (DELAY 850/30)
		Play audio 'truck_01_go' at 'snd'.
	sequence 'state_idle_sound_start':
		Play audio 'truck_01_idle' at 'snd'.
	sequence 'state_interaction_enabled':
		Enable interaction.
		Show graphic_group 'icon'.
	sequence 'state_interaction_disabled':
		Disable interaction.
		Hide graphic_group 'icon'.
	sequence 'state_vis_hide':
		Disable interaction.
		Hide graphic_group 'grp_truck'.
		Hide graphic_group 'icon'.
		Disable body 'body_anim_blocker'.
		Disable body 'body_blocker_mover'.
		Disable body 'body_body'.
		Disable body 'body_wheel_front_right'.
		Disable body 'body_wheel_front_left'.
		Disable body 'body_wheel_rear_right'.
		Disable body 'body_wheel_rear_left'.
		Disable body 'body_window_front'.
		Disable body 'body_window_left_front'.
		Disable body 'body_window_right_front'.
		Disable body 'body_window_left_middle'.
		Disable body 'body_window_right_middle'.
		Disable body 'back_door'.
		Disable body 'back_door_bottom'.
		Disable body 'body_blocker_bag'.
	sequence 'state_vis_show':
		Show graphic_group 'grp_truck'.
		Enable body 'body_anim_blocker'.
		Enable body 'body_blocker_mover'.
		Enable body 'body_body'.
		Enable body 'body_wheel_front_right'.
		Enable body 'body_wheel_front_left'.
		Enable body 'body_wheel_rear_right'.
		Enable body 'body_wheel_rear_left'.
		Enable body 'body_window_front'.
		Enable body 'body_window_left_front'.
		Enable body 'body_window_right_front'.
		Enable body 'body_window_left_middle'.
		Enable body 'body_window_right_middle'.
		Enable body 'back_door'.
		Enable body 'back_door_bottom'.
		Enable body 'body_blocker_bag'.
	sequence 'var_set_color_red':
		material_config 'units/payday2/vehicles/str_vehicle_truck_boxvan_anim_eday/mat_red'.
	sequence 'var_set_color_green':
		material_config 'units/payday2/vehicles/str_vehicle_truck_boxvan_anim_eday/mat_green'.
	sequence 'var_set_color_blue':
		material_config 'units/payday2/vehicles/str_vehicle_truck_boxvan_anim_eday/mat_blue'.
	sequence 'var_set_color_yellow':
		material_config 'units/payday2/vehicles/str_vehicle_truck_boxvan_anim_eday/mat_yellow'.
	sequence 'var_set_color_white':
		material_config 'units/payday2/vehicles/str_vehicle_truck_boxvan_anim_eday/mat_white'.
	sequence 'var_set_decal_1':
		Enable object 'g_decal_1_avalon'.
		Disable object 'g_decal_2_kranich'.
		Disable object 'g_decal_3_omni'.
		Disable object 'g_decal_4_hefty'.
	sequence 'var_set_decal_2':
		Disable object 'g_decal_1_avalon'.
		Enable object 'g_decal_2_kranich'.
		Disable object 'g_decal_3_omni'.
		Disable object 'g_decal_4_hefty'.
	sequence 'var_set_decal_3':
		Disable object 'g_decal_1_avalon'.
		Disable object 'g_decal_2_kranich'.
		Enable object 'g_decal_3_omni'.
		Disable object 'g_decal_4_hefty'.
	sequence 'var_set_decal_4':
		Disable object 'g_decal_1_avalon'.
		Disable object 'g_decal_2_kranich'.
		Disable object 'g_decal_3_omni'.
		Enable object 'g_decal_4_hefty'.
	sequence 'var_set_decal_none':
		Disable object 'g_decal_1_avalon'.
		Disable object 'g_decal_2_kranich'.
		Disable object 'g_decal_3_omni'.
		Disable object 'g_decal_4_hefty'.
	sequence 'drive_exit'.
	sequence 'set_position_1'.
	sequence 'set_position_2'.
	sequence 'set_position_3'.
	sequence 'set_position_4'.
	sequence 'set_position_5'.
	sequence 'set_position_6'.
	sequence 'anim_pos_1_to_pos_2'.
	sequence 'anim_pos_2_to_pos_3'.
	sequence 'anim_pos_3_to_pos_4'.
	sequence 'anim_pos_4_to_pos_5'.
	sequence 'anim_pos_5_to_pos_6'.
	sequence 'anim_pos_1_to_exit'.
	sequence 'anim_pos_2_to_exit'.
	sequence 'anim_pos_3_to_exit'.
	sequence 'anim_pos_4_to_exit'.
	sequence 'anim_pos_5_to_exit'.
	sequence 'anim_pos_6_to_exit'.
	sequence 'done_stopped_at_pos_2'.
	sequence 'done_stopped_at_pos_3'.
	sequence 'done_stopped_at_pos_4'.
	sequence 'done_stopped_at_pos_5'.
	sequence 'done_stopped_at_pos_6'.
	sequence 'done_pos_exit'.
	sequence 'done_drive_forward':
		Disable interaction.
		Stop audio 'truck_01_go' at 'snd'.
	sequence 'kill_window_front_01':
		TRIGGER TIMES 1
		Enable object 'g_window_front_dmg'.
		Disable object 'g_window_front'.
	body 'body_window_front'
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_front_01'.
	sequence 'kill_window_right_front_01':
		TRIGGER TIMES 1
		Enable object 'g_window_right_small_dmg'.
		Disable object 'g_window_right_small'.
	sequence 'kill_window_right_front_02':
		TRIGGER TIMES 1
		Disable object 'g_window_right_small_dmg'.
		Disable decal_mesh 'dm_window_right_small'.
		Play audio 'window_small_shatter' at 'a_glass_front_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_front_r' )
			position v()
		Disable body 'body_window_right_front'.
	body 'body_window_right_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_front_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_front_02'.
	sequence 'kill_window_left_front_01':
		TRIGGER TIMES 1
		Enable object 'g_window_left_small_dmg'.
		Disable object 'g_window_left_small'.
	sequence 'kill_window_left_front_02':
		TRIGGER TIMES 1
		Disable object 'g_window_left_small_dmg'.
		Disable decal_mesh 'dm_window_left_small'.
		Play audio 'window_small_shatter' at 'a_glass_front_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_front_l' )
			position v()
		Disable body 'body_window_left_front'.
	body 'body_window_left_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_front_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_front_02'.
	sequence 'kill_window_right_middle_01':
		TRIGGER TIMES 1
		Enable object 'g_window_right_big_dmg'.
		Disable object 'g_window_right_big'.
	sequence 'kill_window_right_middle_02':
		TRIGGER TIMES 1
		Disable object 'g_window_right_big_dmg'.
		Disable decal_mesh 'dm_window_right_big'.
		Play audio 'window_small_shatter' at 'a_glass_big_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_middle_r' )
			position v()
		Disable body 'body_window_right_middle'.
	body 'body_window_right_middle'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_middle_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_middle_02'.
	sequence 'kill_window_left_middle_01':
		TRIGGER TIMES 1
		Enable object 'g_window_left_big_dmg'.
		Disable object 'g_window_left_big'.
	sequence 'kill_window_left_middle_02':
		TRIGGER TIMES 1
		Disable object 'g_window_left_big_dmg'.
		Disable decal_mesh 'dm_window_left_big'.
		Play audio 'window_small_shatter' at 'a_glass_big_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_middle_l' )
			position v()
		Disable body 'body_window_left_middle'.
	body 'body_window_left_middle'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_middle_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_middle_02'.
	sequence 'open_door':
		Enable animation_group 'door_animation':
			end_time 100/30
			time 0/30
		Play audio 'box_van_rear_door_open' at 'anim_open'.
	sequence 'close_door':
		Enable animation_group 'door_animation':
			end_time 0/30
			speed -1
			time 100/30
	sequence 'interact':
		Hide graphic_group 'icon'.
