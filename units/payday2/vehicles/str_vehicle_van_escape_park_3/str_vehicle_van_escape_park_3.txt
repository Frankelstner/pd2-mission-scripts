unit:
	sequence 'mat_normal':
		material_config 'units/payday2/vehicles/anim_vehicle_van_player/anim_vehicle_van_player'.
	sequence 'mat_overkill':
		material_config 'units/pd2_dlc_overkill_pack/vehicles/vehicle_van_player/vehicle_van_player_overkill'.
	sequence 'mat_chill_brown':
		material_config 'units/payday2/vehicles/str_vehicle_van_fourstores/material_variations/str_vehicle_van_brown_chill'.
	sequence 'mat_chill_green':
		material_config 'units/payday2/vehicles/str_vehicle_van_fourstores/material_variations/str_vehicle_van_green_chill'.
	sequence 'mat_chill_grey':
		material_config 'units/payday2/vehicles/str_vehicle_van_fourstores/material_variations/str_vehicle_van_grey_chill'.
	sequence 'mat_chill_red':
		material_config 'units/payday2/vehicles/str_vehicle_van_fourstores/material_variations/str_vehicle_van_red_chill'.
	sequence 'mat_chill_white':
		material_config 'units/payday2/vehicles/str_vehicle_van_fourstores/material_variations/str_vehicle_van_white_chill'.
	sequence 'mat_chill_yellow':
		material_config 'units/payday2/vehicles/str_vehicle_van_fourstores/material_variations/str_vehicle_van_yellow_chill'.
	sequence 'mat_chill_icecream':
		material_config 'units/payday2/vehicles/str_vehicle_van_fourstores/material_variations/str_vehicle_van_icecream_chill'.
	sequence 'mat_chill_halloween':
		material_config 'units/payday2/vehicles/str_vehicle_van_fourstores/material_variations/str_vehicle_van_halloween_chill'.
	loot_bag = 0
	canvas_bag = 0
	sequence 'state_add_loot_bag':
		If loot_bag == 11: Run sequence 'int_seq_grow_loot_bag_12'.
		If loot_bag == 10: Run sequence 'int_seq_grow_loot_bag_11'.
		If loot_bag == 9: Run sequence 'int_seq_grow_loot_bag_10'.
		If loot_bag == 8: Run sequence 'int_seq_grow_loot_bag_09'.
		If loot_bag == 7: Run sequence 'int_seq_grow_loot_bag_08'.
		If loot_bag == 6: Run sequence 'int_seq_grow_loot_bag_07'.
		If loot_bag == 5: Run sequence 'int_seq_grow_loot_bag_06'.
		If loot_bag == 4: Run sequence 'int_seq_grow_loot_bag_05'.
		If loot_bag == 3: Run sequence 'int_seq_grow_loot_bag_04'.
		If loot_bag == 2: Run sequence 'int_seq_grow_loot_bag_03'.
		If loot_bag == 1: Run sequence 'int_seq_grow_loot_bag_02'.
		If loot_bag == 0: Run sequence 'int_seq_grow_loot_bag_01'.
	sequence 'state_add_canvas_bag':
		If canvas_bag == 8: Run sequence 'int_seq_grow_loot_canvas_09'.
		If canvas_bag == 7: Run sequence 'int_seq_grow_loot_canvas_08'.
		If canvas_bag == 6: Run sequence 'int_seq_grow_loot_canvas_07'.
		If canvas_bag == 5: Run sequence 'int_seq_grow_loot_canvas_06'.
		If canvas_bag == 4: Run sequence 'int_seq_grow_loot_canvas_05'.
		If canvas_bag == 3: Run sequence 'int_seq_grow_loot_canvas_04'.
		If canvas_bag == 2: Run sequence 'int_seq_grow_loot_canvas_03'.
		If canvas_bag == 1: Run sequence 'int_seq_grow_loot_canvas_02'.
		If canvas_bag == 0: Run sequence 'int_seq_grow_loot_canvas_01'.
	sequence 'int_seq_grow_loot_bag_01':
		Enable object 'g_lootbag_1'.
		Disable object 'g_lootbag_2'.
		Disable object 'g_lootbag_3'.
		Disable object 'g_lootbag_4'.
		Disable object 'g_lootbag_5'.
		Disable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 1
	sequence 'int_seq_grow_loot_bag_02':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Disable object 'g_lootbag_3'.
		Disable object 'g_lootbag_4'.
		Disable object 'g_lootbag_5'.
		Disable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 2
	sequence 'int_seq_grow_loot_bag_03':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Disable object 'g_lootbag_4'.
		Disable object 'g_lootbag_5'.
		Disable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 3
	sequence 'int_seq_grow_loot_bag_04':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Disable object 'g_lootbag_5'.
		Disable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 4
	sequence 'int_seq_grow_loot_bag_05':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Disable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 5
	sequence 'int_seq_grow_loot_bag_06':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 6
	sequence 'int_seq_grow_loot_bag_07':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 7
	sequence 'int_seq_grow_loot_bag_08':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 8
	sequence 'int_seq_grow_loot_bag_09':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 9
	sequence 'int_seq_grow_loot_bag_10':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Enable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 10
	sequence 'int_seq_grow_loot_bag_11':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Enable object 'g_lootbag_10'.
		Enable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 11
	sequence 'int_seq_grow_loot_bag_12':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Enable object 'g_lootbag_10'.
		Enable object 'g_lootbag_11'.
		Enable object 'g_lootbag_12'.
		loot_bag = 12
	sequence 'int_seq_grow_loot_canvas_01':
		Enable object 'g_canvasbag_1'.
		Disable object 'g_canvasbag_2'.
		Disable object 'g_canvasbag_3'.
		Disable object 'g_canvasbag_4'.
		Disable object 'g_canvasbag_5'.
		Disable object 'g_canvasbag_6'.
		Disable object 'g_canvasbag_7'.
		Disable object 'g_canvasbag_8'.
		Disable object 'g_canvasbag_9'.
		canvas_bag = 1
	sequence 'int_seq_grow_loot_canvas_02':
		Enable object 'g_canvasbag_1'.
		Enable object 'g_canvasbag_2'.
		Disable object 'g_canvasbag_3'.
		Disable object 'g_canvasbag_4'.
		Disable object 'g_canvasbag_5'.
		Disable object 'g_canvasbag_6'.
		Disable object 'g_canvasbag_7'.
		Disable object 'g_canvasbag_8'.
		Disable object 'g_canvasbag_9'.
		canvas_bag = 2
	sequence 'int_seq_grow_loot_canvas_03':
		Enable object 'g_canvasbag_1'.
		Enable object 'g_canvasbag_2'.
		Enable object 'g_canvasbag_3'.
		Disable object 'g_canvasbag_4'.
		Disable object 'g_canvasbag_5'.
		Disable object 'g_canvasbag_6'.
		Disable object 'g_canvasbag_7'.
		Disable object 'g_canvasbag_8'.
		Disable object 'g_canvasbag_9'.
		canvas_bag = 3
	sequence 'int_seq_grow_loot_canvas_04':
		Enable object 'g_canvasbag_1'.
		Enable object 'g_canvasbag_2'.
		Enable object 'g_canvasbag_3'.
		Enable object 'g_canvasbag_4'.
		Disable object 'g_canvasbag_5'.
		Disable object 'g_canvasbag_6'.
		Disable object 'g_canvasbag_7'.
		Disable object 'g_canvasbag_8'.
		Disable object 'g_canvasbag_9'.
		canvas_bag = 4
	sequence 'int_seq_grow_loot_canvas_05':
		Enable object 'g_canvasbag_1'.
		Enable object 'g_canvasbag_2'.
		Enable object 'g_canvasbag_3'.
		Enable object 'g_canvasbag_4'.
		Enable object 'g_canvasbag_5'.
		Disable object 'g_canvasbag_6'.
		Disable object 'g_canvasbag_7'.
		Disable object 'g_canvasbag_8'.
		Disable object 'g_canvasbag_9'.
		canvas_bag = 5
	sequence 'int_seq_grow_loot_canvas_06':
		Enable object 'g_canvasbag_1'.
		Enable object 'g_canvasbag_2'.
		Enable object 'g_canvasbag_3'.
		Enable object 'g_canvasbag_4'.
		Enable object 'g_canvasbag_5'.
		Enable object 'g_canvasbag_6'.
		Disable object 'g_canvasbag_7'.
		Disable object 'g_canvasbag_8'.
		Disable object 'g_canvasbag_9'.
		canvas_bag = 6
	sequence 'int_seq_grow_loot_canvas_07':
		Enable object 'g_canvasbag_1'.
		Enable object 'g_canvasbag_2'.
		Enable object 'g_canvasbag_3'.
		Enable object 'g_canvasbag_4'.
		Enable object 'g_canvasbag_5'.
		Enable object 'g_canvasbag_6'.
		Enable object 'g_canvasbag_7'.
		Disable object 'g_canvasbag_8'.
		Disable object 'g_canvasbag_9'.
		canvas_bag = 7
	sequence 'int_seq_grow_loot_canvas_08':
		Enable object 'g_canvasbag_1'.
		Enable object 'g_canvasbag_2'.
		Enable object 'g_canvasbag_3'.
		Enable object 'g_canvasbag_4'.
		Enable object 'g_canvasbag_5'.
		Enable object 'g_canvasbag_6'.
		Enable object 'g_canvasbag_7'.
		Enable object 'g_canvasbag_8'.
		Disable object 'g_canvasbag_9'.
		canvas_bag = 8
	sequence 'int_seq_grow_loot_canvas_09':
		Enable object 'g_canvasbag_1'.
		Enable object 'g_canvasbag_2'.
		Enable object 'g_canvasbag_3'.
		Enable object 'g_canvasbag_4'.
		Enable object 'g_canvasbag_5'.
		Enable object 'g_canvasbag_6'.
		Enable object 'g_canvasbag_7'.
		Enable object 'g_canvasbag_8'.
		Enable object 'g_canvasbag_9'.
		canvas_bag = 9
	sequence 'anim_van_arrive':
		Enable animation_group 'anim':
			from 0/30
			to 369/30
		Run sequence 'done_van_arrive'. (DELAY 369/30)
	sequence 'anim_door_side_left_open':
		Enable animation_group 'anim_door_side_left':
			from 0/30
			to 20/30
		Play audio 'van_side_door_open' at 'anim_door_side_left'.
	sequence 'anim_door_side_left_close':
		Enable animation_group 'anim_door_side_left':
			from 20/30
			speed -1
			to 0/30
		Play audio 'van_side_door_close' at 'anim_door_side_left'.
	sequence 'anim_door_side_right_open':
		Enable animation_group 'anim_door_side_right':
			from 0/30
			to 20/30
		Play audio 'van_side_door_open' at 'anim_door_side_right'.
	sequence 'anim_door_side_right_close':
		Enable animation_group 'anim_door_side_right':
			from 20/30
			speed -1
			to 0/30
		Play audio 'van_side_door_close' at 'anim_door_side_right'.
	sequence 'anim_door_rear_left_open':
		Enable animation_group 'anim_door_rear_left':
			from 0/30
			to 15/30
		Play audio 'van_rear_door_open' at 'anim_door_rear_left'.
	sequence 'anim_door_rear_left_close':
		Enable animation_group 'anim_door_rear_left':
			from 15/30
			speed -1
			to 0/30
		Play audio 'van_rear_door_close' at 'anim_door_rear_left'.
	sequence 'anim_door_rear_right_open':
		Enable animation_group 'anim_door_rear_right':
			from 0/30
			to 15/30
		Play audio 'van_rear_door_open' at 'anim_door_rear_right'.
	sequence 'anim_door_rear_right_close':
		Enable animation_group 'anim_door_rear_right':
			from 15/30
			speed -1
			to 0/30
		Play audio 'van_rear_door_close' at 'anim_door_rear_right'.
	sequence 'anim_doors_rear_open':
		Run sequence 'anim_door_rear_right_open'.
		Run sequence 'anim_door_rear_left_open'.
	sequence 'anim_doors_rear_close':
		Run sequence 'anim_door_rear_right_close'.
		Run sequence 'anim_door_rear_left_close'.
	sequence 'state_door_side_left_open':
		Enable animation_group 'anim_door_side_left':
			from 20/30
			speed 0
			to 20/30
	sequence 'state_door_side_left_close':
		Enable animation_group 'anim_door_side_left':
			from 0/30
			speed 0
			to 0/30
	sequence 'state_door_side_right_open':
		Enable animation_group 'anim_door_side_right':
			from 20/30
			speed 0
			to 20/30
	sequence 'state_door_side_right_close':
		Enable animation_group 'anim_door_side_right':
			from 0/30
			speed 0
			to 0/30
	sequence 'state_door_rear_left_open':
		Enable animation_group 'anim_door_rear_left':
			from 15/30
			speed 0
			to 15/30
	sequence 'state_door_rear_left_close':
		Enable animation_group 'anim_door_rear_left':
			from 0/30
			speed 0
			to 0/30
	sequence 'state_door_rear_right_open':
		Enable animation_group 'anim_door_rear_right':
			from 15/30
			speed 0
			to 15/30
	sequence 'state_door_rear_right_close':
		Enable animation_group 'anim_door_rear_right':
			from 0/30
			speed 0
			to 0/30
	sequence 'done_van_arrive':
		Disable body 'body_anim_blocker'.
	sequence 'int_seq_shatter_win_front_left_1':
		TRIGGER TIMES 1
		Disable object 'g_windows_front_left'.
		Enable object 'g_windows_front_left_dmg'.
	sequence 'int_seq_shatter_win_front_left_2':
		TRIGGER TIMES 1
		Disable body 'body_windows_front_left'.
		Disable object 'g_windows_front_left_dmg'.
		Disable decal_mesh 'dm_windows_front_left'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_windows_front_left' )
			position v()
		Play audio 'window_small_shatter' at 'e_windows_front_left'.
	sequence 'int_seq_shatter_win_front_right_1':
		TRIGGER TIMES 1
		Disable object 'g_windows_front_right'.
		Enable object 'g_windows_front_right_dmg'.
	sequence 'int_seq_shatter_win_front_right_2':
		TRIGGER TIMES 1
		Disable body 'body_windows_front_right'.
		Disable object 'g_windows_front_right_dmg'.
		Disable decal_mesh 'dm_windows_front_right'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_windows_front_right' )
			position v()
		Play audio 'window_small_shatter' at 'e_windows_front_right'.
	sequence 'int_seq_shatter_win_front':
		TRIGGER TIMES 1
		Disable object 'g_windows_front'.
		Enable object 'g_windows_front_dmg'.
	sequence 'int_seq_shatter_win_rear_left_1':
		TRIGGER TIMES 1
		Disable object 'g_windows_rear_left'.
		Enable object 'g_windows_rear_left_dmg'.
	sequence 'int_seq_shatter_win_rear_left_2':
		TRIGGER TIMES 1
		Disable body 'body_windows_rear_left'.
		Disable object 'g_windows_rear_left_dmg'.
		Disable decal_mesh 'dm_windows_rear_left'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_windows_rear_left' )
			position v()
		Play audio 'window_small_shatter' at 'e_windows_rear_left'.
	sequence 'int_seq_shatter_win_rear_right_1':
		TRIGGER TIMES 1
		Disable object 'g_windows_rear_right'.
		Enable object 'g_windows_rear_right_dmg'.
	sequence 'int_seq_shatter_win_rear_right_2':
		TRIGGER TIMES 1
		Disable body 'body_windows_rear_right'.
		Disable object 'g_windows_rear_right_dmg'.
		Disable decal_mesh 'dm_windows_rear_right'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_windows_rear_right' )
			position v()
		Play audio 'window_small_shatter' at 'e_windows_rear_right'.
	body 'body_windows_front_left'
		Upon receiving 1 bullet hit or 10 explosion damage or 2.5 saw damage or 5 melee damage, execute:
			Run sequence 'int_seq_shatter_win_front_left_1'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 saw damage or 10 melee damage, execute:
			Run sequence 'int_seq_shatter_win_front_left_2'.
	body 'body_windows_front_right'
		Upon receiving 1 bullet hit or 10 explosion damage or 2.5 saw damage or 5 melee damage, execute:
			Run sequence 'int_seq_shatter_win_front_right_1'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 saw damage or 10 melee damage, execute:
			Run sequence 'int_seq_shatter_win_front_right_2'.
	body 'body_windows_front'
		Upon receiving 5 bullet hits or 10 explosion damage or 12.5 saw damage or 50 melee damage, execute:
			Run sequence 'int_seq_shatter_win_front'.
	body 'body_windows_rear_left'
		Upon receiving 1 bullet hit or 10 explosion damage or 2.5 saw damage or 5 melee damage, execute:
			Run sequence 'int_seq_shatter_win_rear_left_1'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 saw damage or 10 melee damage, execute:
			Run sequence 'int_seq_shatter_win_rear_left_2'.
	body 'body_windows_rear_right'
		Upon receiving 1 bullet hit or 10 explosion damage or 2.5 saw damage or 5 melee damage, execute:
			Run sequence 'int_seq_shatter_win_rear_right_1'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 saw damage or 10 melee damage, execute:
			Run sequence 'int_seq_shatter_win_rear_right_2'.
