unit:
	sequence 'silver':
		material_config 'units/payday2/vehicles/str_vehicle_car_charger/str_vehicle_car_charger_silver'.
	sequence 'black':
		material_config 'units/payday2/vehicles/str_vehicle_car_charger/str_vehicle_car_charger_black'.
	sequence 'green':
		material_config 'units/payday2/vehicles/str_vehicle_car_charger/str_vehicle_car_charger_green'.
	sequence 'kill_window_front_01':
		TRIGGER TIMES 1
		Enable object 'g_window_front_dmg'.
		Disable object 'g_window_front'.
	body 'window_front'
		Upon receiving 2 bullet hits or 10 explosion damage or 20 melee damage, execute:
			Run sequence 'kill_window_front_01'.
	sequence 'kill_window_right_front_01':
		TRIGGER TIMES 1
		Enable object 'g_window_right_dmg'.
		Disable object 'g_window_right_front'.
	sequence 'kill_window_right_front_02':
		TRIGGER TIMES 1
		Disable object 'g_window_right_dmg'.
		Disable decal_mesh 'dm_window_right_front'.
		Play audio 'window_small_shatter' at 'e_window_right_front'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_right_front' )
			position v()
		Disable body 'window_right_front'.
	body 'window_right_front'
		Upon receiving 1 bullet hit or 10 explosion damage or 20 melee damage, execute:
			Run sequence 'kill_window_right_front_01'.
		Upon receiving 2 bullet hits or 10 explosion damage or 40 melee damage, execute:
			Run sequence 'kill_window_right_front_02'.
	sequence 'kill_window_right_rear_01':
		TRIGGER TIMES 1
		Enable object 'g_window_right_rear_dmg'.
		Disable object 'g_window_right_rear'.
	sequence 'kill_window_right_rear_02':
		TRIGGER TIMES 1
		Disable object 'g_window_right_rear_dmg'.
		Disable decal_mesh 'dm_window_right_rear'.
		Play audio 'window_small_shatter' at 'e_window_right_rear'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_right_rear' )
			position v()
		Disable body 'window_right_rear'.
	body 'window_right_rear'
		Upon receiving 1 bullet hit or 10 explosion damage or 20 melee damage, execute:
			Run sequence 'kill_window_right_rear_01'.
		Upon receiving 2 bullet hits or 10 explosion damage or 40 melee damage, execute:
			Run sequence 'kill_window_right_rear_02'.
	sequence 'kill_window_left_front_01':
		TRIGGER TIMES 1
		Enable object 'g_window_left_front_dmg'.
		Disable object 'g_window_left_front'.
	sequence 'kill_window_left_front_02':
		TRIGGER TIMES 1
		Disable object 'g_window_left_front_dmg'.
		Disable decal_mesh 'dm_window_left_front'.
		Play audio 'window_small_shatter' at 'e_window_left_front'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_left_front' )
			position v()
		Disable body 'window_left_front'.
	body 'window_left_front'
		Upon receiving 1 bullet hit or 10 explosion damage or 20 melee damage, execute:
			Run sequence 'kill_window_left_front_01'.
		Upon receiving 2 bullet hits or 10 explosion damage or 40 melee damage, execute:
			Run sequence 'kill_window_left_front_02'.
	sequence 'kill_window_left_rear_01':
		TRIGGER TIMES 1
		Enable object 'g_window_left_rear_dmg'.
		Disable object 'g_window_left_rear'.
	sequence 'kill_window_left_rear_02':
		TRIGGER TIMES 1
		Disable object 'g_window_left_rear_dmg'.
		Disable decal_mesh 'dm_window_left_rear'.
		Play audio 'window_small_shatter' at 'e_window_left_rear'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_left_rear' )
			position v()
		Disable body 'window_left_rear'.
	body 'window_left_rear'
		Upon receiving 1 bullet hit or 10 explosion damage or 20 melee damage, execute:
			Run sequence 'kill_window_left_rear_01'.
		Upon receiving 2 bullet hits or 10 explosion damage or 40 melee damage, execute:
			Run sequence 'kill_window_left_rear_02'.
	sequence 'kill_window_back_01':
		TRIGGER TIMES 1
		Enable object 'g_window_back_dmg'.
		Disable object 'g_window_back'.
	body 'window_back'
		Upon receiving 2 bullet hits or 10 explosion damage or 20 melee damage, execute:
			Run sequence 'kill_window_back_01'.
