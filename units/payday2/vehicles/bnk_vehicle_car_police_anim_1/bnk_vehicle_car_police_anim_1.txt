unit:
	sequence 'state_hide':
		Hide graphic_group 'grp_vehicle_whole'.
		Disable body 'body_body'.
		Disable body 'body_anim_blocker'.
		Disable body 'body_door_left'.
		Disable body 'body_door_right'.
		Disable body 'body_wheel_front_right'.
		Disable body 'body_wheel_front_left'.
		Disable body 'body_win_front'.
		Disable body 'body_win_left_front'.
		Disable body 'body_win_right_front'.
		Disable body 'body_win_left_rear'.
		Disable body 'body_win_right_rear'.
		Disable body 'body_win_back'.
		Disable body 'body_blocker_mover'.
		Disable decal_mesh 'dm_window_front'.
		Disable decal_mesh 'dm_door_left_window'.
		Disable decal_mesh 'dm_door_right_window'.
		Disable decal_mesh 'dm_window_rear_left'.
		Disable decal_mesh 'dm_window_rear_right'.
		Disable decal_mesh 'dm_window_back'.
		Disable decal_mesh 'dm_body'.
		Disable decal_mesh 'dm_wheel_front_left'.
		Disable decal_mesh 'dm_wheel_front_right'.
		Disable decal_mesh 'dm_door_left'.
		Disable decal_mesh 'dm_door_right'.
	sequence 'state_show':
		Show graphic_group 'grp_vehicle_whole'.
		Enable body 'body_body'.
		Enable body 'body_anim_blocker'.
		Enable body 'body_door_left'.
		Enable body 'body_door_right'.
		Enable body 'body_wheel_front_right'.
		Enable body 'body_wheel_front_left'.
		Enable body 'body_win_front'.
		Enable body 'body_win_left_front'.
		Enable body 'body_win_right_front'.
		Enable body 'body_win_left_rear'.
		Enable body 'body_win_right_rear'.
		Enable body 'body_win_back'.
		Enable body 'body_blocker_mover'.
		Enable decal_mesh 'dm_window_front'.
		Enable decal_mesh 'dm_door_left_window'.
		Enable decal_mesh 'dm_door_right_window'.
		Enable decal_mesh 'dm_window_rear_left'.
		Enable decal_mesh 'dm_window_rear_right'.
		Enable decal_mesh 'dm_window_back'.
		Enable decal_mesh 'dm_body'.
		Enable decal_mesh 'dm_wheel_front_left'.
		Enable decal_mesh 'dm_wheel_front_right'.
		Enable decal_mesh 'dm_door_left'.
		Enable decal_mesh 'dm_door_right'.
	sequence 'state_lights_siren_on':
		Enable object 'g_il_red'.
		Enable object 'g_il_blue'.
		Enable light 'li_light_blue'.
		Enable light 'li_light_red'.
		Enable animation_group 'show':
			from 0/30
			loop True
			speed 1.7
			to 33/30
	sequence 'state_lights_siren_off':
		Disable object 'g_il_red'.
		Disable object 'g_il_blue'.
		Disable light 'li_light_blue'.
		Disable light 'li_light_red'.
		Disable animation_group 'show'.
	sequence 'state_police_officers_off':
		Disable object 'g_police'.
	sequence 'state_police_officers_on':
		Enable object 'g_police'.
	sequence 'anim_police_responce':
		Enable animation_group 'anim':
			from 0/30
			to 516/30
		Run sequence 'state_lights_siren_on'.
		Run sequence 'done_police_responce'. (DELAY 495/30)
		Play audio 'car_police_anim_1' at 'snd'.
	sequence 'done_police_responce':
		Disable object 'g_police'.
		Disable body 'body_anim_blocker'.
	sequence 'int_seq_win_left_front':
		TRIGGER TIMES 1
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_front_left' )
			position v()
		Play audio 'window_small_shatter' at 'e_window_front_left'.
		Disable object 'g_door_left_window'.
		Disable object 'g_door_left_window_lod'.
		Disable body 'body_win_left_front'.
		Disable decal_mesh 'dm_door_left_window'.
	sequence 'int_seq_win_right_front':
		TRIGGER TIMES 1
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_front_right' )
			position v()
		Play audio 'window_small_shatter' at 'e_window_front_right'.
		Disable object 'g_door_right_window'.
		Disable object 'g_door_right_window_lod'.
		Disable body 'body_win_right_front'.
		Disable decal_mesh 'dm_door_right_window'.
	sequence 'int_seq_win_left_rear':
		TRIGGER TIMES 1
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_rear_left' )
			position v()
		Play audio 'window_small_shatter' at 'e_window_rear_left'.
		Disable object 'g_window_rear_left'.
		Disable object 'g_window_rear_left_lod'.
		Disable body 'body_win_left_rear'.
		Disable decal_mesh 'dm_window_rear_left'.
	sequence 'int_seq_win_right_rear':
		TRIGGER TIMES 1
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_rear_right' )
			position v()
		Play audio 'window_small_shatter' at 'e_window_rear_right'.
		Disable object 'g_window_rear_right'.
		Disable object 'g_window_rear_right_lod'.
		Disable body 'body_win_right_rear'.
		Disable decal_mesh 'dm_window_rear_right'.
	sequence 'int_seq_win_front':
		TRIGGER TIMES 1
		Disable object 'g_window_front'.
		Disable object 'g_window_front_lod'.
		Enable object 'g_window_front_dmg'.
	sequence 'int_seq_win_back':
		TRIGGER TIMES 1
		Disable object 'g_window_back'.
		Disable object 'g_window_back_lod'.
		Enable object 'g_window_back_dmg'.
	body 'body_win_left_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'int_seq_win_left_front'.
	body 'body_win_right_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'int_seq_win_right_front'.
	body 'body_win_left_rear'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'int_seq_win_left_rear'.
	body 'body_win_right_rear'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'int_seq_win_right_rear'.
	body 'body_win_front'
		Upon receiving 5 bullet hits or 10 explosion damage, execute:
			Run sequence 'int_seq_win_front'.
	body 'body_win_back'
		Upon receiving 5 bullet hits or 10 explosion damage, execute:
			Run sequence 'int_seq_win_back'.
