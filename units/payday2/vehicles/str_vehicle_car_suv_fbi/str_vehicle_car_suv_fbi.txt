unit:
	open = 0
	loot_bag = 0
	sequence 'state_show_driver':
		Enable object 'g_driver'.
	sequence 'state_hide_driver':
		Disable object 'g_driver'.
	sequence 'state_interaction_enabled':
		Enable interaction.
	sequence 'state_interaction_disabled':
		Disable interaction.
	sequence 'state_lights_siren_on':
		Disable object 'g_lights_off'.
		Enable object 'g_lights_on'.
		Disable object 'g_lights_illum'.
		Enable light 'li_light_blue'.
		Enable light 'li_light_red'.
		Enable animation_group 'lights':
			from 0/30
			loop True
			speed 1.7
			to 33/30
	sequence 'state_lights_siren_off':
		Enable object 'g_lights_off'.
		Disable object 'g_lights_on'.
		Disable object 'g_lights_illum'.
		Disable light 'li_light_blue'.
		Disable light 'li_light_red'.
		Disable animation_group 'lights'.
	sequence 'state_lights_on':
		Disable object 'g_lights_off'.
		Disable object 'g_lights_on'.
		Enable object 'g_lights_illum'.
	sequence 'state_lights_off':
		Enable object 'g_lights_off'.
		Disable object 'g_lights_on'.
		Disable object 'g_lights_illum'.
	sequence 'state_vis_show':
		Enable interaction.
		Enable body 'body_vehicle'.
		Enable body 'body_door_front_left'.
		Enable body 'body_door_front_right'.
		Enable body 'body_door_rear_left'.
		Enable body 'body_door_rear_right'.
		Enable body 'body_trunk'.
		Enable body 'body_tire_front_right'.
		Enable body 'body_tire_front_left'.
		Enable body 'body_tire_rear_right'.
		Enable body 'body_tire_rear_left'.
		Enable body 'body_win_front_left'.
		Enable body 'body_win_front_right'.
		Enable body 'body_win_rear_left'.
		Enable body 'body_win_rear_right'.
		Enable body 'body_win_rear2_left'.
		Enable body 'body_win_rear2_right'.
		Enable body 'body_win_front'.
		Enable body 'body_win_rear'.
		Enable decal_mesh 'dm_vehicle'.
		Enable decal_mesh 'dm_door_front_l'.
		Enable decal_mesh 'dm_door_front_r'.
		Enable decal_mesh 'dm_door_rear_l'.
		Enable decal_mesh 'dm_door_rear_r'.
		Enable decal_mesh 'dm_trunk'.
		Enable decal_mesh 'dm_tire_front_left'.
		Enable decal_mesh 'dm_tire_rear_left'.
		Enable decal_mesh 'dm_tire_front_right'.
		Enable decal_mesh 'dm_tire_rear_right'.
		Show graphic_group 'grp_vehicle'.
	sequence 'state_vis_hide':
		Disable interaction.
		Disable body 'body_vehicle'.
		Disable body 'body_door_front_left'.
		Disable body 'body_door_front_right'.
		Disable body 'body_door_rear_left'.
		Disable body 'body_door_rear_right'.
		Disable body 'body_trunk'.
		Disable body 'body_tire_front_right'.
		Disable body 'body_tire_front_left'.
		Disable body 'body_tire_rear_right'.
		Disable body 'body_tire_rear_left'.
		Disable body 'body_win_front_left'.
		Disable body 'body_win_front_right'.
		Disable body 'body_win_rear_left'.
		Disable body 'body_win_rear_right'.
		Disable body 'body_win_rear2_left'.
		Disable body 'body_win_rear2_right'.
		Disable body 'body_win_front'.
		Disable body 'body_win_rear'.
		Disable decal_mesh 'dm_vehicle'.
		Disable decal_mesh 'dm_door_front_l'.
		Disable decal_mesh 'dm_door_front_r'.
		Disable decal_mesh 'dm_door_rear_l'.
		Disable decal_mesh 'dm_door_rear_r'.
		Disable decal_mesh 'dm_trunk'.
		Disable decal_mesh 'dm_tire_front_left'.
		Disable decal_mesh 'dm_tire_rear_left'.
		Disable decal_mesh 'dm_tire_front_right'.
		Disable decal_mesh 'dm_tire_rear_right'.
		Hide graphic_group 'grp_vehicle'.
	sequence 'state_add_green_bag':
		Enable object 'g_green_bag'.
	sequence 'state_add_loot_bag':
		If loot_bag == 7: Run sequence 'int_seq_grow_loot_bag_08'.
		If loot_bag == 6: Run sequence 'int_seq_grow_loot_bag_07'.
		If loot_bag == 5: Run sequence 'int_seq_grow_loot_bag_06'.
		If loot_bag == 4: Run sequence 'int_seq_grow_loot_bag_05'.
		If loot_bag == 3: Run sequence 'int_seq_grow_loot_bag_04'.
		If loot_bag == 2: Run sequence 'int_seq_grow_loot_bag_03'.
		If loot_bag == 1: Run sequence 'int_seq_grow_loot_bag_02'.
		If loot_bag == 0: Run sequence 'int_seq_grow_loot_bag_01'.
	sequence 'int_seq_grow_loot_bag_01':
		Enable object 'g_bag_01'.
		Disable object 'g_bag_02'.
		Disable object 'g_bag_03'.
		Disable object 'g_bag_04'.
		Disable object 'g_bag_05'.
		Disable object 'g_bag_06'.
		Disable object 'g_bag_07'.
		Disable object 'g_bag_08'.
		loot_bag = 1
	sequence 'int_seq_grow_loot_bag_02':
		Enable object 'g_bag_01'.
		Enable object 'g_bag_02'.
		Disable object 'g_bag_03'.
		Disable object 'g_bag_04'.
		Disable object 'g_bag_05'.
		Disable object 'g_bag_06'.
		Disable object 'g_bag_07'.
		Disable object 'g_bag_08'.
		loot_bag = 2
	sequence 'int_seq_grow_loot_bag_03':
		Enable object 'g_bag_01'.
		Enable object 'g_bag_02'.
		Enable object 'g_bag_03'.
		Disable object 'g_bag_04'.
		Disable object 'g_bag_05'.
		Disable object 'g_bag_06'.
		Disable object 'g_bag_07'.
		Disable object 'g_bag_08'.
		loot_bag = 3
	sequence 'int_seq_grow_loot_bag_04':
		Enable object 'g_bag_01'.
		Enable object 'g_bag_02'.
		Enable object 'g_bag_03'.
		Enable object 'g_bag_04'.
		Disable object 'g_bag_05'.
		Disable object 'g_bag_06'.
		Disable object 'g_bag_07'.
		Disable object 'g_bag_08'.
		loot_bag = 4
	sequence 'int_seq_grow_loot_bag_05':
		Enable object 'g_bag_01'.
		Enable object 'g_bag_02'.
		Enable object 'g_bag_03'.
		Enable object 'g_bag_04'.
		Enable object 'g_bag_05'.
		Disable object 'g_bag_06'.
		Disable object 'g_bag_07'.
		Disable object 'g_bag_08'.
		loot_bag = 5
	sequence 'int_seq_grow_loot_bag_06':
		Enable object 'g_bag_01'.
		Enable object 'g_bag_02'.
		Enable object 'g_bag_03'.
		Enable object 'g_bag_04'.
		Enable object 'g_bag_05'.
		Enable object 'g_bag_06'.
		Disable object 'g_bag_07'.
		Disable object 'g_bag_08'.
		loot_bag = 6
	sequence 'int_seq_grow_loot_bag_07':
		Enable object 'g_bag_01'.
		Enable object 'g_bag_02'.
		Enable object 'g_bag_03'.
		Enable object 'g_bag_04'.
		Enable object 'g_bag_05'.
		Enable object 'g_bag_06'.
		Enable object 'g_bag_07'.
		Disable object 'g_bag_08'.
		loot_bag = 7
	sequence 'int_seq_grow_loot_bag_08':
		Enable object 'g_bag_01'.
		Enable object 'g_bag_02'.
		Enable object 'g_bag_03'.
		Enable object 'g_bag_04'.
		Enable object 'g_bag_05'.
		Enable object 'g_bag_06'.
		Enable object 'g_bag_07'.
		Enable object 'g_bag_08'.
		loot_bag = 8
	sequence 'anim_door_front_left_open':
		Enable animation_group 'door_front_left':
			from 0/30
			to 15/30
		Play audio 'car_door_old_open' at 'snd_door_l'.
	sequence 'anim_door_front_left_close':
		Enable animation_group 'door_front_left':
			from 15/30
			speed -1
			to 0/30
		Play audio 'car_door_old_close' at 'snd_door_l'. (DELAY 14/30)
	sequence 'anim_door_front_right_open':
		Enable animation_group 'door_front_right':
			from 0/30
			to 15/30
		Play audio 'car_door_old_open' at 'snd_door_r'.
	sequence 'anim_door_front_right_close':
		Enable animation_group 'door_front_right':
			from 15/30
			speed -1
			to 0/30
		Play audio 'car_door_old_close' at 'snd_door_r'. (DELAY 14/30)
	sequence 'anim_door_rear_left_open':
		Enable animation_group 'door_rear_left':
			from 0/30
			to 15/30
	sequence 'anim_door_rear_left_close':
		Enable animation_group 'door_rear_left':
			from 15/30
			speed -1
			to 0/30
	sequence 'anim_door_rear_right_open':
		Enable animation_group 'door_rear_right':
			from 0/30
			to 15/30
	sequence 'anim_door_rear_right_close':
		Enable animation_group 'door_rear_right':
			from 15/30
			speed -1
			to 0/30
	sequence 'anim_trunk_open':
		Enable animation_group 'trunk':
			from 0/30
			to 15/30
		Play audio 'open_car_trunk' at 'anim_trunk'.
		open = 1
	sequence 'anim_trunk_close':
		Enable animation_group 'trunk':
			from 15/30
			speed -1
			to 0/30
		open = 0
	sequence 'interact':
		If open == 1: Run sequence 'int_trunk_close'.
		If open == 0: Run sequence 'anim_trunk_open'.
		If open == 2: Run sequence 'anim_trunk_close'.
	sequence 'int_trunk_close':
		Enable animation_group 'trunk':
			from 15/30
			speed -1
			to 0/30
		open = 2
	body 'body_win_front_left'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_win_front_left_crc'.
		Upon receiving 2 bullet hits or 20 explosion damage or 20 melee damage, execute:
			Run sequence 'int_seq_win_front_left_sht'.
	sequence 'int_seq_win_front_left_crc':
		TRIGGER TIMES 1
		Disable object 'g_win_front_l'.
		Enable object 'g_win_front_l_dmg'.
		Play audio 'glass_crack' at 'e_win_front_l'.
	sequence 'int_seq_win_front_left_sht':
		TRIGGER TIMES 1
		Disable body 'body_win_front_left'.
		Disable object 'g_win_front_l'.
		Disable object 'g_win_front_l_dmg'.
		Play audio 'window_small_shatter' at 'e_win_front_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_win_front_l' )
			position v()
	body 'body_win_front_right'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_win_front_right_crc'.
		Upon receiving 2 bullet hits or 20 explosion damage or 20 melee damage, execute:
			Run sequence 'int_seq_win_front_right_sht'.
	sequence 'int_seq_win_front_right_crc':
		TRIGGER TIMES 1
		Disable object 'g_win_front_r'.
		Enable object 'g_win_front_r_dmg'.
		Play audio 'glass_crack' at 'e_win_front_r'.
	sequence 'int_seq_win_front_right_sht':
		TRIGGER TIMES 1
		Disable body 'body_win_front_right'.
		Disable object 'g_win_front_r'.
		Disable object 'g_win_front_r_dmg'.
		Play audio 'window_small_shatter' at 'e_win_front_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_win_front_r' )
			position v()
	body 'body_win_rear_left'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_win_rear_left_crc'.
		Upon receiving 2 bullet hits or 20 explosion damage or 20 melee damage, execute:
			Run sequence 'int_seq_win_rear_left_sht'.
	sequence 'int_seq_win_rear_left_crc':
		TRIGGER TIMES 1
		Disable object 'g_win_rear_l'.
		Enable object 'g_win_rear_l_dmg'.
		Play audio 'glass_crack' at 'e_win_rear_l'.
	sequence 'int_seq_win_rear_left_sht':
		TRIGGER TIMES 1
		Disable body 'body_win_rear_left'.
		Disable object 'g_win_rear_l'.
		Disable object 'g_win_rear_l_dmg'.
		Play audio 'window_small_shatter' at 'e_win_rear_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_win_rear_l' )
			position v()
	body 'body_win_rear_right'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_win_rear_right_crc'.
		Upon receiving 2 bullet hits or 20 explosion damage or 20 melee damage, execute:
			Run sequence 'int_seq_win_rear_right_sht'.
	sequence 'int_seq_win_rear_right_crc':
		TRIGGER TIMES 1
		Disable object 'g_win_rear_r'.
		Enable object 'g_win_rear_r_dmg'.
		Play audio 'glass_crack' at 'e_win_rear_r'.
	sequence 'int_seq_win_rear_right_sht':
		TRIGGER TIMES 1
		Disable body 'body_win_rear_right'.
		Disable object 'g_win_rear_r'.
		Disable object 'g_win_rear_r_dmg'.
		Play audio 'window_small_shatter' at 'e_win_rear_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_win_rear_r' )
			position v()
	body 'body_win_front'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_win_front_crc'.
	sequence 'int_seq_win_front_crc':
		TRIGGER TIMES 1
		Disable object 'g_win_front'.
		Enable object 'g_win_front_dmg'.
		Play audio 'glass_crack' at 'g_win_front'.
	body 'body_win_rear'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_win_rear_crc'.
	sequence 'int_seq_win_rear_crc':
		TRIGGER TIMES 1
		Disable object 'g_win_rear'.
		Enable object 'g_win_rear_dmg'.
		Play audio 'glass_crack' at 'g_win_rear'.
	body 'body_win_rear2_left'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_win_rear2_left_crc'.
		Upon receiving 2 bullet hits or 20 explosion damage or 20 melee damage, execute:
			Run sequence 'int_seq_win_rear2_left_sht'.
	sequence 'int_seq_win_rear2_left_crc':
		TRIGGER TIMES 1
		Disable object 'g_win_rear2_l'.
		Enable object 'g_win_rear2_l_dmg'.
		Play audio 'glass_crack' at 'e_win_rear2_l'.
	sequence 'int_seq_win_rear2_left_sht':
		TRIGGER TIMES 1
		Disable body 'body_win_rear2_left'.
		Disable object 'g_win_rear2_l'.
		Disable object 'g_win_rear2_l_dmg'.
		Play audio 'window_small_shatter' at 'e_win_rear2_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_win_rear2_l' )
			position v()
	body 'body_win_rear2_right'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_win_rear2_right_crc'.
		Upon receiving 2 bullet hits or 20 explosion damage or 20 melee damage, execute:
			Run sequence 'int_seq_win_rear2_right_sht'.
	sequence 'int_seq_win_rear2_right_crc':
		TRIGGER TIMES 1
		Disable object 'g_win_rear2_r'.
		Enable object 'g_win_rear2_r_dmg'.
		Play audio 'glass_crack' at 'e_win_rear2_r'.
	sequence 'int_seq_win_rear2_right_sht':
		TRIGGER TIMES 1
		Disable body 'body_win_rear2_right'.
		Disable object 'g_win_rear2_r'.
		Disable object 'g_win_rear2_r_dmg'.
		Play audio 'window_small_shatter' at 'e_win_rear2_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_win_rear2_r' )
			position v()
