unit:
	loot_bag = 0
	sequence 'state_add_loot_bag':
		If loot_bag == 19: Run sequence 'int_seq_grow_loot_bag_20'.
		If loot_bag == 18: Run sequence 'int_seq_grow_loot_bag_19'.
		If loot_bag == 17: Run sequence 'int_seq_grow_loot_bag_18'.
		If loot_bag == 16: Run sequence 'int_seq_grow_loot_bag_17'.
		If loot_bag == 15: Run sequence 'int_seq_grow_loot_bag_16'.
		If loot_bag == 14: Run sequence 'int_seq_grow_loot_bag_15'.
		If loot_bag == 13: Run sequence 'int_seq_grow_loot_bag_14'.
		If loot_bag == 12: Run sequence 'int_seq_grow_loot_bag_13'.
		If loot_bag == 11: Run sequence 'int_seq_grow_loot_bag_12'.
		If loot_bag == 10: Run sequence 'int_seq_grow_loot_bag_11'.
		If loot_bag == 9: Run sequence 'int_seq_grow_loot_bag_10'.
		If loot_bag == 8: Run sequence 'int_seq_grow_loot_bag_09'.
		If loot_bag == 7: Run sequence 'int_seq_grow_loot_bag_08'.
		If loot_bag == 6: Run sequence 'int_seq_grow_loot_bag_07'.
		If loot_bag == 5: Run sequence 'int_seq_grow_loot_bag_06'.
		If loot_bag == 4: Run sequence 'int_seq_grow_loot_bag_05'.
		If loot_bag == 3: Run sequence 'int_seq_grow_loot_bag_04'.
		If loot_bag == 2: Run sequence 'int_seq_grow_loot_bag_03'.
		If loot_bag == 1: Run sequence 'int_seq_grow_loot_bag_02'.
		If loot_bag == 0: Run sequence 'int_seq_grow_loot_bag_01'.
	sequence 'int_seq_grow_loot_bag_01':
		Enable object 'g_lootbag_1'.
		Disable object 'g_lootbag_2'.
		Disable object 'g_lootbag_3'.
		Disable object 'g_lootbag_4'.
		Disable object 'g_lootbag_5'.
		Disable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		Disable object 'g_lootbag_13'.
		Disable object 'g_lootbag_14'.
		Disable object 'g_lootbag_15'.
		Disable object 'g_lootbag_16'.
		Disable object 'g_lootbag_17'.
		Disable object 'g_lootbag_18'.
		Disable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 1
	sequence 'int_seq_grow_loot_bag_02':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Disable object 'g_lootbag_3'.
		Disable object 'g_lootbag_4'.
		Disable object 'g_lootbag_5'.
		Disable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		Disable object 'g_lootbag_13'.
		Disable object 'g_lootbag_14'.
		Disable object 'g_lootbag_15'.
		Disable object 'g_lootbag_16'.
		Disable object 'g_lootbag_17'.
		Disable object 'g_lootbag_18'.
		Disable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 2
	sequence 'int_seq_grow_loot_bag_03':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Disable object 'g_lootbag_4'.
		Disable object 'g_lootbag_5'.
		Disable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		Disable object 'g_lootbag_13'.
		Disable object 'g_lootbag_14'.
		Disable object 'g_lootbag_15'.
		Disable object 'g_lootbag_16'.
		Disable object 'g_lootbag_17'.
		Disable object 'g_lootbag_18'.
		Disable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 3
	sequence 'int_seq_grow_loot_bag_04':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Disable object 'g_lootbag_5'.
		Disable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		Disable object 'g_lootbag_13'.
		Disable object 'g_lootbag_14'.
		Disable object 'g_lootbag_15'.
		Disable object 'g_lootbag_16'.
		Disable object 'g_lootbag_17'.
		Disable object 'g_lootbag_18'.
		Disable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 4
	sequence 'int_seq_grow_loot_bag_05':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Disable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		Disable object 'g_lootbag_13'.
		Disable object 'g_lootbag_14'.
		Disable object 'g_lootbag_15'.
		Disable object 'g_lootbag_16'.
		Disable object 'g_lootbag_17'.
		Disable object 'g_lootbag_18'.
		Disable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 5
	sequence 'int_seq_grow_loot_bag_06':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		Disable object 'g_lootbag_13'.
		Disable object 'g_lootbag_14'.
		Disable object 'g_lootbag_15'.
		Disable object 'g_lootbag_16'.
		Disable object 'g_lootbag_17'.
		Disable object 'g_lootbag_18'.
		Disable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 6
	sequence 'int_seq_grow_loot_bag_07':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		Disable object 'g_lootbag_13'.
		Disable object 'g_lootbag_14'.
		Disable object 'g_lootbag_15'.
		Disable object 'g_lootbag_16'.
		Disable object 'g_lootbag_17'.
		Disable object 'g_lootbag_18'.
		Disable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 7
	sequence 'int_seq_grow_loot_bag_08':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		Disable object 'g_lootbag_13'.
		Disable object 'g_lootbag_14'.
		Disable object 'g_lootbag_15'.
		Disable object 'g_lootbag_16'.
		Disable object 'g_lootbag_17'.
		Disable object 'g_lootbag_18'.
		Disable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 8
	sequence 'int_seq_grow_loot_bag_09':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		Disable object 'g_lootbag_13'.
		Disable object 'g_lootbag_14'.
		Disable object 'g_lootbag_15'.
		Disable object 'g_lootbag_16'.
		Disable object 'g_lootbag_17'.
		Disable object 'g_lootbag_18'.
		Disable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 9
	sequence 'int_seq_grow_loot_bag_10':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Enable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		Disable object 'g_lootbag_13'.
		Disable object 'g_lootbag_14'.
		Disable object 'g_lootbag_15'.
		Disable object 'g_lootbag_16'.
		Disable object 'g_lootbag_17'.
		Disable object 'g_lootbag_18'.
		Disable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 10
	sequence 'int_seq_grow_loot_bag_11':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Enable object 'g_lootbag_10'.
		Enable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		Disable object 'g_lootbag_13'.
		Disable object 'g_lootbag_14'.
		Disable object 'g_lootbag_15'.
		Disable object 'g_lootbag_16'.
		Disable object 'g_lootbag_17'.
		Disable object 'g_lootbag_18'.
		Disable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 11
	sequence 'int_seq_grow_loot_bag_12':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Enable object 'g_lootbag_10'.
		Enable object 'g_lootbag_11'.
		Enable object 'g_lootbag_12'.
		Disable object 'g_lootbag_13'.
		Disable object 'g_lootbag_14'.
		Disable object 'g_lootbag_15'.
		Disable object 'g_lootbag_16'.
		Disable object 'g_lootbag_17'.
		Disable object 'g_lootbag_18'.
		Disable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 12
	sequence 'int_seq_grow_loot_bag_13':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Enable object 'g_lootbag_10'.
		Enable object 'g_lootbag_11'.
		Enable object 'g_lootbag_12'.
		Enable object 'g_lootbag_13'.
		Disable object 'g_lootbag_14'.
		Disable object 'g_lootbag_15'.
		Disable object 'g_lootbag_16'.
		Disable object 'g_lootbag_17'.
		Disable object 'g_lootbag_18'.
		Disable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 13
	sequence 'int_seq_grow_loot_bag_14':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Enable object 'g_lootbag_10'.
		Enable object 'g_lootbag_11'.
		Enable object 'g_lootbag_12'.
		Enable object 'g_lootbag_13'.
		Enable object 'g_lootbag_14'.
		Disable object 'g_lootbag_15'.
		Disable object 'g_lootbag_16'.
		Disable object 'g_lootbag_17'.
		Disable object 'g_lootbag_18'.
		Disable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 14
	sequence 'int_seq_grow_loot_bag_15':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Enable object 'g_lootbag_10'.
		Enable object 'g_lootbag_11'.
		Enable object 'g_lootbag_12'.
		Enable object 'g_lootbag_13'.
		Enable object 'g_lootbag_14'.
		Enable object 'g_lootbag_15'.
		Disable object 'g_lootbag_16'.
		Disable object 'g_lootbag_17'.
		Disable object 'g_lootbag_18'.
		Disable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 15
	sequence 'int_seq_grow_loot_bag_16':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Enable object 'g_lootbag_10'.
		Enable object 'g_lootbag_11'.
		Enable object 'g_lootbag_12'.
		Enable object 'g_lootbag_13'.
		Enable object 'g_lootbag_14'.
		Enable object 'g_lootbag_15'.
		Enable object 'g_lootbag_16'.
		Disable object 'g_lootbag_17'.
		Disable object 'g_lootbag_18'.
		Disable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 16
	sequence 'int_seq_grow_loot_bag_17':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Enable object 'g_lootbag_10'.
		Enable object 'g_lootbag_11'.
		Enable object 'g_lootbag_12'.
		Enable object 'g_lootbag_13'.
		Enable object 'g_lootbag_14'.
		Enable object 'g_lootbag_15'.
		Enable object 'g_lootbag_16'.
		Enable object 'g_lootbag_17'.
		Disable object 'g_lootbag_18'.
		Disable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 17
	sequence 'int_seq_grow_loot_bag_18':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Enable object 'g_lootbag_10'.
		Enable object 'g_lootbag_11'.
		Enable object 'g_lootbag_12'.
		Enable object 'g_lootbag_13'.
		Enable object 'g_lootbag_14'.
		Enable object 'g_lootbag_15'.
		Enable object 'g_lootbag_16'.
		Enable object 'g_lootbag_17'.
		Enable object 'g_lootbag_18'.
		Disable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 18
	sequence 'int_seq_grow_loot_bag_19':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Enable object 'g_lootbag_10'.
		Enable object 'g_lootbag_11'.
		Enable object 'g_lootbag_12'.
		Enable object 'g_lootbag_13'.
		Enable object 'g_lootbag_14'.
		Enable object 'g_lootbag_15'.
		Enable object 'g_lootbag_16'.
		Enable object 'g_lootbag_17'.
		Enable object 'g_lootbag_18'.
		Enable object 'g_lootbag_19'.
		Disable object 'g_lootbag_20'.
		loot_bag = 19
	sequence 'int_seq_grow_loot_bag_20':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Enable object 'g_lootbag_10'.
		Enable object 'g_lootbag_11'.
		Enable object 'g_lootbag_12'.
		Enable object 'g_lootbag_13'.
		Enable object 'g_lootbag_14'.
		Enable object 'g_lootbag_15'.
		Enable object 'g_lootbag_16'.
		Enable object 'g_lootbag_17'.
		Enable object 'g_lootbag_18'.
		Enable object 'g_lootbag_19'.
		Enable object 'g_lootbag_20'.
		loot_bag = 20
	sequence 'state_vis_hide_water_wave':
		Disable object 'g_water_wave'.
	sequence 'state_vis_show_water_wave':
		Enable object 'g_water_wave'.
	sequence 'goodbye':
		Enable animation_group 'hi_and_bye':
			end_time 150/30
			time 50/30
	sequence 'hi':
		Enable animation_group 'hi_and_bye':
			end_time 50/30
			time 0/30
	sequence 'done_comming_in_left'.
	sequence 'done_comming_in_front'.
	sequence 'done_comming_in_right'.
	sequence 'water_splasch':
		Enable effect_spawner 'water_splash_1'.
		Enable effect_spawner 'water_splash_2'.
	sequence 'water_splasch_accellearate':
		Enable effect_spawner 'water_splash_5'.
		Enable effect_spawner 'water_splash_6'.
		Enable effect_spawner 'water_splash_7'.
		Enable effect_spawner 'water_splash_8'.
		Enable effect_spawner 'water_splash_3'.
		Enable effect_spawner 'water_splash_4'.
	sequence 'no_water_splasch':
		Disable effect_spawner 'water_splash_5'.
		Disable effect_spawner 'water_splash_6'.
		Disable effect_spawner 'water_splash_7'.
		Disable effect_spawner 'water_splash_8'.
		Disable effect_spawner 'water_splash_3'.
		Disable effect_spawner 'water_splash_4'.
	sequence 'comming_in_left':
		Enable animation_group 'boat':
			end_time 650/30
			time 200/30
		Enable body 'static_body'.
		Run sequence 'water_splasch'. (DELAY 150/30)
		Run sequence 'no_water_splasch'.
		Run sequence 'hi'. (DELAY 430/30)
		Run sequence 'done_comming_in_left'. (DELAY 450/30)
		Play audio 'boat_arrive' at 'a_body'.
		Play audio 'boat_idle' at 'a_body'. (DELAY 430/30)
	sequence 'leaving_from_left':
		Enable animation_group 'boat':
			end_time 1100/30
			time 650/30
		Run sequence 'water_splasch'. (DELAY 50/30)
		Run sequence 'water_splasch_accellearate'. (DELAY 50/30)
		Play audio 'boat_away' at 'a_body'. (DELAY 50/30)
		Run sequence 'goodbye'.
	sequence 'comming_in_front':
		Enable animation_group 'boat':
			end_time 1550/30
			time 1100/30
		Enable body 'static_body'.
		Run sequence 'water_splasch'. (DELAY 150/30)
		Run sequence 'no_water_splasch'.
		Run sequence 'hi'. (DELAY 450/30)
		Run sequence 'done_comming_in_front'. (DELAY 450/30)
		Play audio 'boat_arrive' at 'a_body'.
		Play audio 'boat_idle' at 'a_body'. (DELAY 430/30)
	sequence 'leaving_from_front':
		Run sequence 'water_splasch'. (DELAY 65/30)
		Run sequence 'water_splasch_accellearate'. (DELAY 65/30)
		Run sequence 'goodbye'.
		Play audio 'boat_away' at 'a_body'. (DELAY 60/30)
		Enable animation_group 'boat': (DELAY 60/30)
			end_time 2000/30
			time 1550/30
	sequence 'comming_in_right':
		Enable animation_group 'boat': (DELAY 80/30)
			end_time 2450/30
			time 2000/30
		Enable body 'static_body'.
		Run sequence 'water_splasch'. (DELAY 150/30)
		Run sequence 'no_water_splasch'.
		Run sequence 'hi'. (DELAY 430/30)
		Run sequence 'done_comming_in_right'. (DELAY 450/30)
		Play audio 'boat_arrive' at 'a_body'.
		Play audio 'boat_idle' at 'a_body'. (DELAY 430/30)
	sequence 'leaving_from_right':
		Enable animation_group 'boat': (DELAY 70/30)
			end_time 2900/30
			time 2450/30
		Run sequence 'water_splasch'. (DELAY 75/30)
		Run sequence 'water_splasch_accellearate'. (DELAY 75/30)
		Play audio 'boat_away' at 'a_body'. (DELAY 70/30)
		Run sequence 'goodbye'.
	sequence 'hide':
		Enable animation_group 'boat':
			end_time 3170/30
			time 3150/30
		Disable body 'static_body'.
