unit:
	sequence 'activate':
		Enable animation_group 'anim':
			from 0/30
			to 75/30
		Play audio 'swatturret_mount' at 'es_explode'.
	sequence 'deactivate':
		Enable animation_group 'anim':
			from 75/30
			speed -1
			to 0/30
		Play audio 'swatturret_mount' at 'es_explode'.
	sequence 'repair_start_seq':
		Run sequence 'int_seq_sparks_enable'.
		Play audio 'swatturret_repair_loop' at 'es_explode'.
	sequence 'repair_complete_seq':
		Run sequence 'int_seq_sparks_disable'.
		Play audio 'swatturret_repair_stop' at 'es_explode'.
	sequence 'explode':
		TRIGGER TIMES 1
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('es_explode') ),100,800).
		effect 'effects/payday2/particles/explosions/sentry_explosion':
			parent object( 'es_explode' )
			position v()
		Play audio 'swatturret_repair_stop' at 'es_explode'.
		Play audio 'swatturret_destroy' at 'es_explode'.
		Run sequence 'int_seq_shield_detach'.
		Disable body 'body_weakspot'.
		Disable body 'body_base'.
		Disable body 'body_rotor'.
		Disable body 'body_rotor1'.
		Disable body 'body_rotor2'.
		Disable body 'body'.
		Disable body 'body_side_plating'.
		Disable object 'g_body'.
		Disable object 'g_gun'.
		Disable object 'g_gun_glass'.
		Disable object 'g_shield'.
		Disable object 'g_rotor'.
		Disable object 'g_rotor1'.
		Disable decal_mesh 'g_body'.
		Disable decal_mesh 'g_gun'.
		Disable decal_mesh 'g_shield'.
		Disable decal_mesh 'g_rotor'.
		Disable decal_mesh 'g_rotor1'.
		Run sequence 'state_light_off'.
		spawn_unit 'units/payday2/vehicles/gen_vehicle_turret/spawn_turret_debris':
			position object_pos('anim_y_gun')
			rotation object_rot('anim_y_gun')
		Disable effect_spawner 'es_small_damage'.
		Disable effect_spawner 'es_medium_damage'.
		Disable effect_spawner 'es_large_damage'.
		Disable effect_spawner 'es_aftersmoke'.
		Disable effect_spawner 'es_sparks_loop'.
	sequence 'normal_difficulty':
		material_config 'units/payday2/vehicles/gen_vehicle_turret/gen_vehicle_turret'.
	sequence 'hard_difficulty':
		material_config 'units/payday2/vehicles/gen_vehicle_turret/gen_vehicle_turret_hard'.
	sequence 'state_light_on':
		Enable effect_spawner 'es_light'.
		Enable light 'li_light'.
	sequence 'state_light_off':
		Disable effect_spawner 'es_light'.
		Disable light 'li_light'.
	sequence 'int_seq_small_damage':
		Enable effect_spawner 'es_small_damage'.
		Disable effect_spawner 'es_medium_damage'.
		Disable effect_spawner 'es_large_damage'.
	sequence 'int_seq_medium_damage':
		Disable effect_spawner 'es_small_damage'.
		Enable effect_spawner 'es_medium_damage'.
		Disable effect_spawner 'es_large_damage'.
	sequence 'int_seq_large_damage':
		Disable effect_spawner 'es_small_damage'.
		Disable effect_spawner 'es_medium_damage'.
		Enable effect_spawner 'es_large_damage'.
	sequence 'int_seq_no_damage':
		Disable effect_spawner 'es_small_damage'.
		Disable effect_spawner 'es_medium_damage'.
		Disable effect_spawner 'es_large_damage'.
	sequence 'int_seq_sparks_enable':
		Enable effect_spawner 'es_sparks_loop'.
	sequence 'int_seq_sparks_disable':
		Disable effect_spawner 'es_sparks_loop'.
	sequence 'int_seq_shield_detach':
		TRIGGER TIMES 1
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('es_explode') ),65,200).
		Disable body 'body_shields'.
		Enable body 'body_weakspot'.
		Disable object 'g_front_shield'.
		Disable decal_mesh 'g_front_shield'.
		spawn_unit 'units/payday2/vehicles/gen_vehicle_turret/spawn_shield_front':
			position object_pos('spawn_shield_front')
			rotation object_rot('spawn_shield_front')
		Disable object 'g_upper_shield'.
		Disable decal_mesh 'g_upper_shield'.
		spawn_unit 'units/payday2/vehicles/gen_vehicle_turret/spawn_shield_upper':
			position object_pos('spawn_shield_upper')
			rotation object_rot('spawn_shield_upper')
		Disable object 'g_front_shield_glass'.
		Disable decal_mesh 'g_front_shield_glass'.
		Debug text: 'Last bullets damage ammount = ' .. damage
	sequence 'int_seq_shield_attach':
		TRIGGER TIMES 1
		Enable body 'body_shields'.
		Disable body 'body_weakspot'.
		Enable object 'g_front_shield'.
		Enable decal_mesh 'g_front_shield'.
		Enable object 'g_upper_shield'.
		Enable decal_mesh 'g_upper_shield'.
		Enable object 'g_front_shield_glass'.
		Enable decal_mesh 'g_front_shield_glass'.
		Debug text: 'Last bullets damage ammount = ' .. damage
