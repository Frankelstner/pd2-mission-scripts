unit:
	sequence 'state_lights_on':
		Enable object 'g_cone_light'.
		Enable object 'g_front_lights'.
		Disable object 'g_glass_lights'.
		Enable light 'ls_left'.
		Enable light 'ls_right'.
	sequence 'state_lights_off':
		Disable object 'g_cone_light'.
		Disable object 'g_front_lights'.
		Enable object 'g_glass_lights'.
		Disable light 'ls_left'.
		Disable light 'ls_right'.
	sequence 'state_interaction_disabled':
		Disable interaction.
		Disable object 'g_contour'.
	sequence 'state_interaction_enabled':
		Enable interaction.
		Enable object 'g_contour'.
	sequence 'interact':
		Disable object 'g_contour'.
	sequence '__reset_animation__':
		Call function: base.move_to_stored_pos()
	sequence '__allow_sync_reset_animation__':
		Call function: base.allow_sync_stored_pos('true')
	sequence 'anim_car_winch_start':
		Call function: base.store_current_pos()
		animation_redirect 'car_winch_start'.
		Enable effect_spawner 'burn_out_left'. (DELAY 38/30)
		Enable effect_spawner 'burn_out_right'. (DELAY 38/30)
		Play audio 'winch_engine_on' at 'snd'.
		Play audio 'winch_skidding_on' at 'snd_skidding'.
	sequence 'anim_car_winch_release':
		animation_redirect 'car_winch_release'.
		Disable effect_spawner 'burn_out_left'.
		Disable effect_spawner 'burn_out_right'.
		Play audio 'winch_engine_off' at 'snd'.
		Play audio 'winch_skidding_off' at 'snd_skidding'.
		Run sequence 'done_car_winch'. (DELAY 110/30)
	sequence 'done_car_winch'.
	sequence 'anim_car_winch_stop':
		animation_redirect 'car_winch_stop'.
		Disable effect_spawner 'burn_out_left'.
		Disable effect_spawner 'burn_out_right'.
		Play audio 'winch_engine_off' at 'snd'.
		Play audio 'winch_skidding_off' at 'snd_skidding'.
	sequence 'anim_car_winch_resume':
		animation_redirect 'car_winch_loop'.
		Enable effect_spawner 'burn_out_left'.
		Enable effect_spawner 'burn_out_right'.
		Play audio 'winch_engine_on' at 'snd'.
		Play audio 'winch_skidding_on' at 'snd_skidding'.
	sequence 'queue_car_sffs':
		Call function: damage.add_queued_sequence('anim_car_sffs')
	sequence 'anim_car_sffs':
		animation_redirect 'car_sffs'.
		Call function: damage.add_queued_sequence('done_car_anim')
	sequence 'queue_car_sfls':
		Call function: damage.add_queued_sequence('anim_car_sfls')
	sequence 'anim_car_sfls':
		animation_redirect 'car_sfls'.
		Call function: damage.add_queued_sequence('done_car_anim')
	sequence 'queue_car_sfrs':
		Call function: damage.add_queued_sequence('anim_car_sfrs')
	sequence 'anim_car_sfrs':
		animation_redirect 'car_sfrs'.
		Call function: damage.add_queued_sequence('done_car_anim')
	sequence 'queue_car_hox_garage_1':
		Call function: damage.add_queued_sequence('anim_car_hox_garage_1')
	sequence 'anim_car_hox_garage_1':
		animation_redirect 'car_hox_garage_1'.
		Call function: damage.add_queued_sequence('done_car_anim')
	sequence 'queue_car_hox_garage_2':
		Call function: damage.add_queued_sequence('anim_car_hox_garage_2')
	sequence 'anim_car_hox_garage_2':
		animation_redirect 'car_hox_garage_2'.
		Call function: damage.add_queued_sequence('done_car_anim')
	sequence 'anim_full_to_stop_8m':
		animation_redirect 'car_straight_slow_full_to_stop_8m'.
		Run sequence 'done_car_anim'. (DELAY 190/30)
	sequence 'done_car_anim'.
	sequence 'orig'.
	sequence 'blue'.
	sequence 'black'.
	sequence 'kill_window_front_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_front_dmg'.
		Disable object 'g_glass_front'.
	body 'window_front'
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_front_01'.
	sequence 'kill_window_right_front_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_front_r_dmg'.
		Disable object 'g_glass_front_r'.
	sequence 'kill_window_right_front_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_front_r_dmg'.
		Disable decal_mesh 'dm_glass_front_r'.
		Play audio 'window_small_shatter' at 'a_glass_front_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_front_r' )
			position v()
		Disable body 'window_right_front'.
	body 'window_right_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_front_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_front_02'.
	sequence 'kill_window_left_front_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_front_l_dmg'.
		Disable object 'g_glass_front_l'.
	sequence 'kill_window_left_front_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_front_l_dmg'.
		Disable decal_mesh 'dm_glass_front_l'.
		Play audio 'window_small_shatter' at 'a_glass_front_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_front_l' )
			position v()
		Disable body 'window_left_front'.
	body 'window_left_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_front_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_front_02'.
	sequence 'kill_window_back_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_back_dmg'.
		Disable object 'g_glass_back'.
	body 'window_back'
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_back_01'.
	sequence 'kill_window_right_middle_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_middle_r_dmg'.
		Disable object 'g_glass_middle_r'.
	sequence 'kill_window_right_middle_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_middle_r_dmg'.
		Disable decal_mesh 'dm_glass_middle_r'.
		Play audio 'window_small_shatter' at 'a_glass_middle_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_middle_r' )
			position v()
		Disable body 'window_right_middle'.
	body 'window_right_middle'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_middle_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_middle_02'.
	sequence 'kill_window_left_middle_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_middle_l_dmg'.
		Disable object 'g_glass_middle_l'.
	sequence 'kill_window_left_middle_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_middle_l_dmg'.
		Disable decal_mesh 'dm_glass_middle_l'.
		Play audio 'window_small_shatter' at 'a_glass_middle_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_middle_l' )
			position v()
		Disable body 'window_left_middle'.
	body 'window_left_middle'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_middle_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_middle_02'.
