unit:
	sequence 'start_barge_once':
		Enable animation_group 'anim_grp0':
			from 0/2500
			speed 1
			to 2500/2500
	sequence 'start_barge_loop':
		Enable animation_group 'anim_grp0':
			speed 1
