unit:
	sequence 'anim_van_arrive':
		Enable animation_group 'anim':
			from 0/30
			to 635/30
		Run sequence 'done_van_arrive'. (DELAY 635/30)
	sequence 'anim_door_side_left_open':
		Enable animation_group 'anim_door_side_left':
			from 0/30
			to 20/30
		Play audio 'van_side_door_open' at 'anim_door_side_left'.
	sequence 'anim_door_side_left_close':
		Enable animation_group 'anim_door_side_left':
			from 20/30
			speed -1
			to 0/30
		Play audio 'van_side_door_close' at 'anim_door_side_left'.
	sequence 'anim_door_side_right_open':
		Enable animation_group 'anim_door_side_right':
			from 0/30
			to 20/30
		Play audio 'van_side_door_open' at 'anim_door_side_right'.
	sequence 'anim_door_side_right_close':
		Enable animation_group 'anim_door_side_right':
			from 20/30
			speed -1
			to 0/30
		Play audio 'van_side_door_close' at 'anim_door_side_right'.
	sequence 'anim_door_rear_left_open':
		Enable animation_group 'anim_door_rear_left':
			from 0/30
			to 15/30
		Play audio 'van_rear_door_open' at 'anim_door_rear_left'.
	sequence 'anim_door_rear_left_close':
		Enable animation_group 'anim_door_rear_left':
			from 15/30
			speed -1
			to 0/30
		Play audio 'van_rear_door_close' at 'anim_door_rear_left'.
	sequence 'anim_door_rear_right_open':
		Enable animation_group 'anim_door_rear_right':
			from 0/30
			to 15/30
		Play audio 'van_rear_door_open' at 'anim_door_rear_right'.
	sequence 'anim_door_rear_right_close':
		Enable animation_group 'anim_door_rear_right':
			from 15/30
			speed -1
			to 0/30
		Play audio 'van_rear_door_close' at 'anim_door_rear_right'.
	sequence 'anim_doors_rear_open':
		Run sequence 'anim_door_rear_right_open'.
		Run sequence 'anim_door_rear_left_open'.
	sequence 'anim_doors_rear_close':
		Run sequence 'anim_door_rear_right_close'.
		Run sequence 'anim_door_rear_left_close'.
	sequence 'state_door_side_left_open':
		Enable animation_group 'anim_door_side_left':
			from 20/30
			speed 0
			to 20/30
	sequence 'state_door_side_left_close':
		Enable animation_group 'anim_door_side_left':
			from 0/30
			speed 0
			to 0/30
	sequence 'state_door_side_right_open':
		Enable animation_group 'anim_door_side_right':
			from 20/30
			speed 0
			to 20/30
	sequence 'state_door_side_right_close':
		Enable animation_group 'anim_door_side_right':
			from 0/30
			speed 0
			to 0/30
	sequence 'state_door_rear_left_open':
		Enable animation_group 'anim_door_rear_left':
			from 15/30
			speed 0
			to 15/30
	sequence 'state_door_rear_left_close':
		Enable animation_group 'anim_door_rear_left':
			from 0/30
			speed 0
			to 0/30
	sequence 'state_door_rear_right_open':
		Enable animation_group 'anim_door_rear_right':
			from 15/30
			speed 0
			to 15/30
	sequence 'state_door_rear_right_close':
		Enable animation_group 'anim_door_rear_right':
			from 0/30
			speed 0
			to 0/30
	sequence 'state_vis_hide_driver':
		Disable object 'g_driver'.
	sequence 'state_vis_show_driver':
		Enable object 'g_driver'.
	sequence 'state_vis_tinted_windows':
		material_config 'units/payday2/vehicles/str_vehicle_van_anim_shouse/str_vehicle_van_anim_shouse_tinted_win'.
		Disable object 'g_driver'.
	sequence 'state_vis_untinted_windows':
		material_config 'units/payday2/vehicles/str_vehicle_van_anim_shouse/str_vehicle_van_anim_shouse'.
	sequence 'done_van_arrive':
		Disable body 'body_anim_blocker'.
	sequence 'int_seq_shatter_win_front_left_1':
		TRIGGER TIMES 1
		Disable object 'g_windows_front_left'.
		Enable object 'g_windows_front_left_dmg'.
	sequence 'int_seq_shatter_win_front_left_2':
		TRIGGER TIMES 1
		Disable body 'body_windows_front_left'.
		Disable object 'g_windows_front_left_dmg'.
		Disable decal_mesh 'dm_windows_front_left'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_windows_front_left' )
			position v()
		Play audio 'window_small_shatter' at 'e_windows_front_left'.
	sequence 'int_seq_shatter_win_front_right_1':
		TRIGGER TIMES 1
		Disable object 'g_windows_front_right'.
		Enable object 'g_windows_front_right_dmg'.
	sequence 'int_seq_shatter_win_front_right_2':
		TRIGGER TIMES 1
		Disable body 'body_windows_front_right'.
		Disable object 'g_windows_front_right_dmg'.
		Disable decal_mesh 'dm_windows_front_right'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_windows_front_right' )
			position v()
		Play audio 'window_small_shatter' at 'e_windows_front_right'.
	sequence 'int_seq_shatter_win_front':
		TRIGGER TIMES 1
		Disable object 'g_windows_front'.
		Enable object 'g_windows_front_dmg'.
	sequence 'int_seq_shatter_win_rear_left_1':
		TRIGGER TIMES 1
		Disable object 'g_windows_rear_left'.
		Enable object 'g_windows_rear_left_dmg'.
	sequence 'int_seq_shatter_win_rear_left_2':
		TRIGGER TIMES 1
		Disable body 'body_windows_rear_left'.
		Disable object 'g_windows_rear_left_dmg'.
		Disable decal_mesh 'dm_windows_rear_left'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_windows_rear_left' )
			position v()
		Play audio 'window_small_shatter' at 'e_windows_rear_left'.
	sequence 'int_seq_shatter_win_rear_right_1':
		TRIGGER TIMES 1
		Disable object 'g_windows_rear_right'.
		Enable object 'g_windows_rear_right_dmg'.
	sequence 'int_seq_shatter_win_rear_right_2':
		TRIGGER TIMES 1
		Disable body 'body_windows_rear_right'.
		Disable object 'g_windows_rear_right_dmg'.
		Disable decal_mesh 'dm_windows_rear_right'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_windows_rear_right' )
			position v()
		Play audio 'window_small_shatter' at 'e_windows_rear_right'.
	body 'body_windows_front_left'
		Upon receiving 1 bullet hit or 10 explosion damage or 2.5 saw damage or 5 melee damage, execute:
			Run sequence 'int_seq_shatter_win_front_left_1'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 saw damage or 10 melee damage, execute:
			Run sequence 'int_seq_shatter_win_front_left_2'.
	body 'body_windows_front_right'
		Upon receiving 1 bullet hit or 10 explosion damage or 2.5 saw damage or 5 melee damage, execute:
			Run sequence 'int_seq_shatter_win_front_right_1'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 saw damage or 10 melee damage, execute:
			Run sequence 'int_seq_shatter_win_front_right_2'.
	body 'body_windows_front'
		Upon receiving 5 bullet hits or 10 explosion damage or 12.5 saw damage or 50 melee damage, execute:
			Run sequence 'int_seq_shatter_win_front'.
	body 'body_windows_rear_left'
		Upon receiving 1 bullet hit or 10 explosion damage or 2.5 saw damage or 5 melee damage, execute:
			Run sequence 'int_seq_shatter_win_rear_left_1'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 saw damage or 10 melee damage, execute:
			Run sequence 'int_seq_shatter_win_rear_left_2'.
	body 'body_windows_rear_right'
		Upon receiving 1 bullet hit or 10 explosion damage or 2.5 saw damage or 5 melee damage, execute:
			Run sequence 'int_seq_shatter_win_rear_right_1'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 saw damage or 10 melee damage, execute:
			Run sequence 'int_seq_shatter_win_rear_right_2'.
