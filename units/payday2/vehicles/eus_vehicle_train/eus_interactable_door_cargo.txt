unit:
	sequence 'state_door_open':
		Enable animation_group 'anim_open_close':
			from 40/30
			speed 0
			to 40/30
	sequence 'state_door_close':
		Enable animation_group 'anim_open_close':
			from 0/30
			speed 0
			to 0/30
	sequence 'state_saw_enabled':
		Enable body 'body_hitbox'.
		Show graphic_group 'icon'.
	sequence 'state_saw_disabled':
		Disable body 'body_hitbox'.
		Hide graphic_group 'icon'.
	sequence 'anim_open_door':
		Enable animation_group 'anim_open_close':
			from 0/30
			to 35/30
		Play audio 'train_door_slide_open' at 'anim_open_close_01'.
		Play audio 'train_door_slide_open' at 'anim_open_close_02'.
		Disable body 'body_hitbox'.
		Hide graphic_group 'icon'.
	sequence 'int_seq_open':
		Run sequence 'deactivate_door'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
	sequence 'int_seq_keycard_panel_complete':
		Enable object 'g_control_panel'.
		Disable object 'g_control_panel_screen'.
		Enable object 'g_control_panel_screen_acces'.
		Enable object 'g_keycard'.
	body 'body_hitbox'
		Upon receiving 87.5 saw damage, execute:
			Run sequence 'int_seq_open'.
	sequence 'activate_door':
		Call function: base.activate()
	sequence 'deactivate_door':
		Call function: base.deactivate()
	sequence 'power_off':
		Call function: base.set_powered(False)
	sequence 'power_on':
		Call function: base.set_powered(True)
	sequence 'turn_off':
		Call function: base.set_on(False)
	sequence 'turn_on':
		Call function: base.set_on(True)
	sequence 'open_door':
		Run sequence 'int_seq_open'.
	sequence 'open_door_keycard':
		Run sequence 'int_seq_open'.
		Run sequence 'int_seq_keycard_panel_complete'.
	sequence 'open_door_ecm':
		Run sequence 'int_seq_open'.
		Enable object 'g_ecm'.
		Enable object 'g_glow_func1_green'.
		Enable object 'g_glow_func2_green'.
	sequence 'door_opened'.
	sequence 'door_closed'.
	sequence 'drill_placed'.
	sequence 'drill_jammed'.
	sequence 'drill_resumed'.
	sequence 'drill_power_off'.
	sequence 'drill_power_on'.
	sequence 'drill_completed'.
	sequence 'all_drill_placed'.
	sequence 'key_placed'.
	sequence 'key_completed'.
	sequence 'all_key_placed'.
	sequence 'ecm_placed'.
	sequence 'all_ecm_placed'.
	sequence 'ecm_completed'.
	sequence 'sobj_swat_breach':
		Run sequence 'int_seq_open'.
	sequence 'done_opened'.
