unit:
	loot_bag = 0
	sequence 'state_add_loot_bag':
		If loot_bag == 11: Run sequence 'int_seq_grow_loot_bag_12'.
		If loot_bag == 10: Run sequence 'int_seq_grow_loot_bag_11'.
		If loot_bag == 9: Run sequence 'int_seq_grow_loot_bag_10'.
		If loot_bag == 8: Run sequence 'int_seq_grow_loot_bag_09'.
		If loot_bag == 7: Run sequence 'int_seq_grow_loot_bag_08'.
		If loot_bag == 6: Run sequence 'int_seq_grow_loot_bag_07'.
		If loot_bag == 5: Run sequence 'int_seq_grow_loot_bag_06'.
		If loot_bag == 4: Run sequence 'int_seq_grow_loot_bag_05'.
		If loot_bag == 3: Run sequence 'int_seq_grow_loot_bag_04'.
		If loot_bag == 2: Run sequence 'int_seq_grow_loot_bag_03'.
		If loot_bag == 1: Run sequence 'int_seq_grow_loot_bag_02'.
		If loot_bag == 0: Run sequence 'int_seq_grow_loot_bag_01'.
	sequence 'state_clear_loot_bags':
		Disable object 'g_lootbag_1'.
		Disable object 'g_lootbag_2'.
		Disable object 'g_lootbag_3'.
		Disable object 'g_lootbag_4'.
		Disable object 'g_lootbag_5'.
		Disable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		Disable object 'g_lootbag_toothbrush'.
		loot_bag = 0
	sequence 'int_seq_grow_loot_bag_01':
		Enable object 'g_lootbag_1'.
		Disable object 'g_lootbag_2'.
		Disable object 'g_lootbag_3'.
		Disable object 'g_lootbag_4'.
		Disable object 'g_lootbag_5'.
		Disable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 1
	sequence 'int_seq_grow_loot_bag_02':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Disable object 'g_lootbag_3'.
		Disable object 'g_lootbag_4'.
		Disable object 'g_lootbag_5'.
		Disable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 2
	sequence 'int_seq_grow_loot_bag_03':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Disable object 'g_lootbag_4'.
		Disable object 'g_lootbag_5'.
		Disable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 3
	sequence 'int_seq_grow_loot_bag_04':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Disable object 'g_lootbag_5'.
		Disable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 4
	sequence 'int_seq_grow_loot_bag_05':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Disable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 5
	sequence 'int_seq_grow_loot_bag_06':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Disable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 6
	sequence 'int_seq_grow_loot_bag_07':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Disable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 7
	sequence 'int_seq_grow_loot_bag_08':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Disable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 8
	sequence 'int_seq_grow_loot_bag_09':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Disable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 9
	sequence 'int_seq_grow_loot_bag_10':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Enable object 'g_lootbag_10'.
		Disable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 10
	sequence 'int_seq_grow_loot_bag_11':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Enable object 'g_lootbag_10'.
		Enable object 'g_lootbag_11'.
		Disable object 'g_lootbag_12'.
		loot_bag = 11
	sequence 'int_seq_grow_loot_bag_12':
		Enable object 'g_lootbag_1'.
		Enable object 'g_lootbag_2'.
		Enable object 'g_lootbag_3'.
		Enable object 'g_lootbag_4'.
		Enable object 'g_lootbag_5'.
		Enable object 'g_lootbag_6'.
		Enable object 'g_lootbag_7'.
		Enable object 'g_lootbag_8'.
		Enable object 'g_lootbag_9'.
		Enable object 'g_lootbag_10'.
		Enable object 'g_lootbag_11'.
		Enable object 'g_lootbag_12'.
		loot_bag = 12
	sequence 'state_vis_black_loot_bags':
		material_config 'units/payday2/vehicles/str_vehicle_pickup_sportcab_anim_wdog/str_vehicle_pickup_sportcab_anim_wdog'.
	sequence 'state_vis_yellow_loot_bags':
		material_config 'units/payday2/vehicles/str_vehicle_pickup_sportcab_anim_wdog/str_vehicle_pickup_sportcab_anim_wdog_ammobag'.
	sequence 'state_vis_hide':
		Disable object 'g_wheel_front_right'.
		Disable object 'g_wheel_front_left'.
		Disable object 'g_wheel_rear'.
		Disable object 'g_str_vehicle_pickuptruck_sportcab'.
		Disable object 'g_glass_lights'.
		Disable object 'g_glass_back_dmg'.
		Disable object 'g_glass_back'.
		Disable object 'g_glass_front'.
		Disable object 'g_glass_front_dmg'.
		Disable object 'g_glass_middle_l'.
		Disable object 'g_glass_middle_l_dmg'.
		Disable object 'g_glass_middle_r'.
		Disable object 'g_glass_middle_r_dmg'.
		Disable object 'g_glass_front_l'.
		Disable object 'g_glass_front_l_dmg'.
		Disable object 'g_glass_front_r_dmg'.
		Disable object 'g_glass_front_r'.
		Disable object 's_pickup'.
		Disable object 's_wheel_front_right'.
		Disable object 's_wheel_front_left'.
		Disable object 's_wheel_rear'.
		Enable body 'body0'.
		Enable body 'body1'.
		Enable body 'body2'.
		Enable body 'body3'.
		Enable body 'window_front'.
		Enable body 'window_left_front'.
		Enable body 'window_right_front'.
		Enable body 'window_left_middle'.
		Enable body 'window_right_middle'.
		Enable body 'window_back'.
		Disable decal_mesh 'g_str_vehicle_pickuptruck_sportcab'.
		Disable decal_mesh 'g_glass_back'.
		Disable decal_mesh 'g_glass_front'.
		Disable decal_mesh 'g_glass_middle_l'.
		Disable decal_mesh 'g_glass_middle_r'.
		Disable decal_mesh 'g_glass_front_l'.
		Disable decal_mesh 'g_glass_front_r'.
		Run sequence 'state_clear_loot_bags'.
	sequence 'state_vis_show':
		Disable object 'g_glass_back_dmg'.
		Disable object 'g_glass_front_dmg'.
		Disable object 'g_glass_middle_l_dmg'.
		Disable object 'g_glass_middle_r_dmg'.
		Disable object 'g_glass_front_l_dmg'.
		Disable object 'g_glass_front_r_dmg'.
		Enable object 'g_wheel_front_right'.
		Enable object 'g_wheel_front_left'.
		Enable object 'g_wheel_rear'.
		Enable object 'g_str_vehicle_pickuptruck_sportcab'.
		Enable object 'g_glass_lights'.
		Enable object 'g_glass_back'.
		Enable object 'g_glass_front'.
		Enable object 'g_glass_middle_l'.
		Enable object 'g_glass_middle_r'.
		Enable object 'g_glass_front_l'.
		Enable object 'g_glass_front_r'.
		Enable object 's_pickup'.
		Enable object 's_wheel_front_right'.
		Enable object 's_wheel_front_left'.
		Enable object 's_wheel_rear'.
		Enable body 'body0'.
		Enable body 'body1'.
		Enable body 'body2'.
		Enable body 'body3'.
		Enable body 'window_front'.
		Enable body 'window_left_front'.
		Enable body 'window_right_front'.
		Enable body 'window_left_middle'.
		Enable body 'window_right_middle'.
		Enable body 'window_back'.
		Enable decal_mesh 'g_str_vehicle_pickuptruck_sportcab'.
		Enable decal_mesh 'g_glass_back'.
		Enable decal_mesh 'g_glass_front'.
		Enable decal_mesh 'g_glass_middle_l'.
		Enable decal_mesh 'g_glass_middle_r'.
		Enable decal_mesh 'g_glass_front_l'.
		Enable decal_mesh 'g_glass_front_r'.
		Run sequence 'state_clear_loot_bags'.
	sequence 'state_approach_2_out':
		Enable animation_group 'anim':
			from 1665/30
			speed 0
			to 1665/30
		Run sequence 'state_vis_hide'.
	sequence 'anim_approach_1_in':
		Enable animation_group 'anim':
			from 0/30
			to 552/30
		Play audio 'cocaine_car_approach_01_in' at 'snd_src'.
		Run sequence 'done_anim_approach_1_in'. (DELAY 552/30)
	sequence 'anim_approach_1_out':
		Enable animation_group 'anim':
			from 552/30
			to 891/30
		Play audio 'cocaine_car_approach_01_out' at 'snd_src'.
		Run sequence 'done_anim_approach_1_out'. (DELAY 339/30)
	sequence 'anim_approach_2_in':
		Enable animation_group 'anim':
			from 900/30
			to 1424/30
		Play audio 'cocaine_car_approach_02_in' at 'snd_src'.
		Run sequence 'done_anim_approach_2_in'. (DELAY 524/30)
	sequence 'anim_approach_2_out':
		Enable animation_group 'anim':
			from 1435/30
			to 1665/30
		Play audio 'cocaine_car_approach_02_out' at 'snd_src'.
		Run sequence 'done_anim_approach_2_out'. (DELAY 230/30)
	sequence 'anim_approach_2_out_hide':
		Enable animation_group 'anim':
			from 1435/30
			to 1665/30
		Play audio 'cocaine_car_approach_02_out' at 'snd_src'.
		Run sequence 'done_anim_approach_2_out'. (DELAY 230/30)
		Run sequence 'state_vis_hide'. (DELAY 230/30)
	sequence 'anim_approach_3_in':
		Enable animation_group 'anim':
			from 1675/30
			to 2426/30
		Play audio 'cocaine_car_approach_03_in' at 'snd_src'.
		Run sequence 'done_anim_approach_3_in'. (DELAY 751/30)
	sequence 'anim_approach_3_out':
		Enable animation_group 'anim':
			from 2435/30
			to 2999/30
		Play audio 'cocaine_car_approach_03_out' at 'snd_src'.
		Run sequence 'done_anim_approach_3_out'. (DELAY 564/30)
	sequence 'state_add_flat_toothbrush':
		Enable object 'g_lootbag_toothbrush'.
	sequence 'done_anim_approach_1_in':
		Play audio 'cocaine_car_approach_01_idle' at 'snd_src'.
	sequence 'done_anim_approach_1_out'.
	sequence 'done_anim_approach_2_in':
		Play audio 'cocaine_car_approach_02_idle' at 'snd_src'.
	sequence 'done_anim_approach_2_out'.
	sequence 'done_anim_approach_3_in':
		Play audio 'cocaine_car_approach_03_idle' at 'snd_src'.
	sequence 'done_anim_approach_3_out'.
	sequence 'kill_window_front_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_front_dmg'.
		Disable object 'g_glass_front'.
	body 'window_front'
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_front_01'.
	sequence 'kill_window_right_front_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_front_r_dmg'.
		Disable object 'g_glass_front_r'.
	sequence 'kill_window_right_front_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_front_r_dmg'.
		Play audio 'window_small_shatter' at 'a_glass_front_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_front_r' )
			position v()
		Disable body 'window_right_front'.
	body 'window_right_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_front_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_front_02'.
	sequence 'kill_window_left_front_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_front_l_dmg'.
		Disable object 'g_glass_front_l'.
	sequence 'kill_window_left_front_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_front_l_dmg'.
		Play audio 'window_small_shatter' at 'a_glass_front_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_front_l' )
			position v()
		Disable body 'window_left_front'.
	body 'window_left_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_front_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_front_02'.
	sequence 'kill_window_back_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_back_dmg'.
		Disable object 'g_glass_back'.
	body 'window_back'
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_back_01'.
	sequence 'kill_window_right_middle_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_middle_r_dmg'.
		Disable object 'g_glass_middle_r'.
	sequence 'kill_window_right_middle_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_middle_r_dmg'.
		Play audio 'window_small_shatter' at 'a_glass_middle_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_middle_r' )
			position v()
		Disable body 'window_right_middle'.
	body 'window_right_middle'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_middle_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_middle_02'.
	sequence 'kill_window_left_middle_01':
		TRIGGER TIMES 1
		Enable object 'g_glass_middle_l_dmg'.
		Disable object 'g_glass_middle_l'.
	sequence 'kill_window_left_middle_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_middle_l_dmg'.
		Play audio 'window_small_shatter' at 'a_glass_middle_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_middle_l' )
			position v()
		Disable body 'window_left_middle'.
	body 'window_left_middle'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_middle_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_middle_02'.
