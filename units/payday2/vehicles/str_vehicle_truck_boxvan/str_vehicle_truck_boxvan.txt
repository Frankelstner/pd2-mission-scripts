unit:
	sequence 'kill_window_front_01':
		TRIGGER TIMES 1
		Enable object 'g_window_front_dmg'.
		Disable object 'g_window_front'.
	body 'window_front'
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_front_01'.
	sequence 'kill_window_right_front_01':
		TRIGGER TIMES 1
		Enable object 'g_window_right_small_dmg'.
		Disable object 'g_window_right_small'.
	sequence 'kill_window_right_front_02':
		TRIGGER TIMES 1
		Disable object 'g_window_right_small_dmg'.
		Disable decal_mesh 'dm_window_right_small'.
		Play audio 'window_small_shatter' at 'a_glass_front_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_front_r' )
			position v()
		Disable body 'window_right_front'.
	body 'window_right_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_front_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_front_02'.
	sequence 'kill_window_left_front_01':
		TRIGGER TIMES 1
		Enable object 'g_window_left_small_dmg'.
		Disable object 'g_window_left_small'.
	sequence 'kill_window_left_front_02':
		TRIGGER TIMES 1
		Disable object 'g_window_left_small_dmg'.
		Disable decal_mesh 'dm_window_left_small'.
		Play audio 'window_small_shatter' at 'a_glass_front_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_front_l' )
			position v()
		Disable body 'window_left_front'.
	body 'window_left_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_front_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_front_02'.
	sequence 'kill_window_right_middle_01':
		TRIGGER TIMES 1
		Enable object 'g_window_right_big_dmg'.
		Disable object 'g_window_right_big'.
	sequence 'kill_window_right_middle_02':
		TRIGGER TIMES 1
		Disable object 'g_window_right_big_dmg'.
		Disable decal_mesh 'dm_window_right_big'.
		Play audio 'window_small_shatter' at 'a_glass_big_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_front_r' )
			position v()
		Disable body 'window_right_middle'.
	body 'window_right_middle'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_middle_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_middle_02'.
	sequence 'kill_window_left_middle_01':
		TRIGGER TIMES 1
		Enable object 'g_window_left_big_dmg'.
		Disable object 'g_window_left_big'.
	sequence 'kill_window_left_middle_02':
		TRIGGER TIMES 1
		Disable object 'g_window_left_big_dmg'.
		Disable decal_mesh 'dm_window_left_big'.
		Play audio 'window_small_shatter' at 'a_glass_big_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_front_l' )
			position v()
		Disable body 'window_left_middle'.
	body 'window_left_middle'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_middle_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_middle_02'.
	sequence 'open_door':
		Enable animation_group 'door_animation':
			end_time 75/30
			time 0/30
		Play audio 'box_van_rear_door_open' at 'anim_open'.
	sequence 'close_door':
		Enable animation_group 'door_animation':
			end_time 0/30
			speed -1
			time 63/30
