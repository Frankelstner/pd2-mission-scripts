unit:
	door = 0
	sequence 'open_door':
		Enable animation_group 'door_animation':
			end_time 100/30
			time 0/30
		Run sequence 'shot_15_false'.
		Run sequence 'shot_16_false'.
		Run sequence 'shot_17_false'.
		Run sequence 'shot_18_false'.
		Run sequence 'shot_19_false'.
		Run sequence 'shot_22_false'.
		Disable body 'hitbox_left'.
		Disable body 'hitbox_right'.
	sequence 'break_door':
		Enable animation_group 'door_broken':
			end_time 100/30
			time 0/30
		Run sequence 'shot_15_false'.
		Run sequence 'shot_16_false'.
		Run sequence 'shot_17_false'.
		Run sequence 'shot_18_false'.
		Run sequence 'shot_19_false'.
		Run sequence 'shot_22_false'.
		Disable body 'hitbox_left'.
		Disable body 'hitbox_right'.
	sequence 'enable_sawing':
		Show graphic_group 'sawicongroup'.
		Enable body 'floor_hitbox'.
	sequence 'done_sawing':
		Hide graphic_group 'sawicongroup'.
		Enable object 'g_floor_falling'.
		Enable object 'g_floor_hole'.
		Disable object 'g_floor_no_hole'.
		Run sequence 'floor_body'. (DELAY 0.6)
		Enable animation_group 'hole_falling':
			end_time 100/30
			time 0/30
	sequence 'floor_body':
		TRIGGER TIMES 1
		Disable body 'floor'.
	body 'floor_hitbox'
		Upon receiving 50 saw damage, execute:
			Run sequence 'done_sawing'.
	body 'hitbox_right'
		Upon receiving 3 bullet hits, execute:
			Run sequence 'kill_right'.
	body 'hitbox_left'
		Upon receiving 3 bullet hits, execute:
			Run sequence 'kill_left'.
	sequence 'kill_left':
		TRIGGER TIMES 1
		Run sequence 'left_part_hide'.
		door = vars.var_door + 1
		If door == 2: Run sequence 'break_door'.
	sequence 'kill_right':
		TRIGGER TIMES 1
		Run sequence 'right_part_hide'.
		door = vars.var_door + 1
		If door == 2: Run sequence 'break_door'.
	sequence 'kill_window_front_01':
		TRIGGER TIMES 1
		Enable object 'g_window_front_dmg'.
		Disable object 'g_window_front'.
	body 'window_front'
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_front_01'.
	sequence 'kill_window_right_front_01':
		TRIGGER TIMES 1
		Enable object 'g_window_right_small_dmg'.
		Disable object 'g_window_right_small'.
	sequence 'kill_window_right_front_02':
		TRIGGER TIMES 1
		Disable object 'g_window_right_small_dmg'.
		Disable decal_mesh 'dm_window_right_small'.
		Play audio 'window_small_shatter' at 'a_glass_front_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_front_r' )
			position v()
		Disable body 'window_right_front'.
	body 'window_right_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_front_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_front_02'.
	sequence 'kill_window_left_front_01':
		TRIGGER TIMES 1
		Enable object 'g_window_left_small_dmg'.
		Disable object 'g_window_left_small'.
	sequence 'kill_window_left_front_02':
		TRIGGER TIMES 1
		Disable object 'g_window_left_small_dmg'.
		Disable decal_mesh 'dm_window_left_small'.
		Play audio 'window_small_shatter' at 'a_glass_front_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_front_l' )
			position v()
		Disable body 'window_left_front'.
	body 'window_left_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_front_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_front_02'.
	sequence 'kill_window_right_middle_01':
		TRIGGER TIMES 1
		Enable object 'g_window_right_big_dmg'.
		Disable object 'g_window_right_big'.
	sequence 'kill_window_right_middle_02':
		TRIGGER TIMES 1
		Disable object 'g_window_right_big_dmg'.
		Disable decal_mesh 'dm_window_right_big'.
		Play audio 'window_small_shatter' at 'a_glass_big_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_big_r' )
			position v()
		Disable body 'window_right_middle'.
	body 'window_right_middle'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_middle_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_middle_02'.
	sequence 'kill_window_left_middle_01':
		TRIGGER TIMES 1
		Enable object 'g_window_left_big_dmg'.
		Disable object 'g_window_left_big'.
	sequence 'kill_window_left_middle_02':
		TRIGGER TIMES 1
		Disable object 'g_window_left_big_dmg'.
		Disable decal_mesh 'dm_window_left_big'.
		Play audio 'window_small_shatter' at 'a_glass_big_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_big_l' )
			position v()
		Disable body 'window_left_middle'.
	body 'window_left_middle'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_middle_01'.
		Upon receiving 4 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_middle_02'.
	sequence 'left_part_destroyed_by_ai':
		Run sequence 'shot_1'.
		Run sequence 'shot_2'. (DELAY 0.3)
		Run sequence 'shot_6'. (DELAY 0.6)
		Run sequence 'shot_20'. (DELAY 0.8)
		Run sequence 'shot_5'. (DELAY 1)
		Run sequence 'shot_22'. (DELAY 1.3)
		Run sequence 'shot_18'. (DELAY 1.6)
		Run sequence 'shot_19'. (DELAY 1.9)
		Run sequence 'shot_4'. (DELAY 2.3)
		Run sequence 'shot_21'. (DELAY 2.4)
		Run sequence 'shot_3'. (DELAY 2.6)
		Run sequence 'left_part_destroyed'. (DELAY 2.7)
	sequence 'left_part_autofire_by_ai':
		Run sequence 'shot_34'.
		Run sequence 'shot_33'. (DELAY 0.1)
		Run sequence 'shot_32'. (DELAY 0.2)
		Run sequence 'shot_31'. (DELAY 0.3)
		Run sequence 'shot_30'. (DELAY 0.4)
		Run sequence 'shot_29'. (DELAY 0.5)
	sequence 'left_part_hide':
		Disable object 'g_left'.
		Enable object 'g_left_broken'.
	sequence 'left_part_destroyed':
		Disable object 'g_left'.
		Enable object 'g_left_broken'.
		Play audio 'bullet_hit' at 'a_shot_21'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_21' )
			position v()
		Run sequence 'shot_20_false'.
		Run sequence 'shot_21_false'.
	sequence 'right_part_destroyed_by_ai':
		Run sequence 'shot_7'.
		Run sequence 'shot_8'. (DELAY 0.2)
		Run sequence 'shot_9'. (DELAY 0.4)
		Run sequence 'shot_13'. (DELAY 0.6)
		Run sequence 'shot_11'. (DELAY 1)
		Run sequence 'shot_12'. (DELAY 1.2)
		Run sequence 'shot_14'. (DELAY 1.4)
		Run sequence 'shot_17'. (DELAY 1.5)
		Run sequence 'shot_15'. (DELAY 1.6)
		Run sequence 'shot_16'. (DELAY 1.7)
		Run sequence 'shot_10'. (DELAY 1.9)
		Run sequence 'right_part_destroyed'. (DELAY 2)
	sequence 'right_part_autofire_by_ai':
		Run sequence 'shot_26'.
		Run sequence 'shot_27'. (DELAY 0.1)
		Run sequence 'shot_28'. (DELAY 0.2)
		Run sequence 'shot_25'. (DELAY 0.3)
		Run sequence 'shot_24'. (DELAY 0.4)
		Run sequence 'shot_23'. (DELAY 0.5)
	sequence 'right_part_hide':
		Disable object 'g_right'.
		Enable object 'g_right_broken'.
	sequence 'right_part_destroyed':
		Disable object 'g_right'.
		Enable object 'g_right_broken'.
		Play audio 'bullet_hit' at 'a_shot_13'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_13' )
			position v()
		Run sequence 'shot_13_false'.
		Run sequence 'shot_14_false'.
	sequence 'shot_1':
		TRIGGER TIMES 1
		Enable object 'g_ray_1'.
		Enable object 'g_decal_1'.
		Enable object 'g_sun_1'.
		Play audio 'bullet_hit' at 'a_shot_1' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_1' )
			position v()
	sequence 'shot_2':
		TRIGGER TIMES 1
		Enable object 'g_ray_2'.
		Enable object 'g_decal_2'.
		Enable object 'g_sun_2'.
		Play audio 'bullet_hit' at 'a_shot_2' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_2' )
			position v()
	sequence 'shot_3':
		TRIGGER TIMES 1
		Enable object 'g_ray_3'.
		Enable object 'g_decal_3'.
		Enable object 'g_sun_3'.
		Play audio 'bullet_hit' at 'a_shot_3' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_3' )
			position v()
	sequence 'shot_4':
		TRIGGER TIMES 1
		Enable object 'g_ray_4'.
		Enable object 'g_decal_4'.
		Enable object 'g_sun_4'.
		Play audio 'bullet_hit' at 'a_shot_4' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_4' )
			position v()
	sequence 'shot_5':
		TRIGGER TIMES 1
		Enable object 'g_ray_5'.
		Enable object 'g_decal_5'.
		Enable object 'g_sun_5'.
		Play audio 'bullet_hit' at 'a_shot_5' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_5' )
			position v()
	sequence 'shot_6':
		TRIGGER TIMES 1
		Enable object 'g_ray_6'.
		Enable object 'g_decal_6'.
		Enable object 'g_sun_6'.
		Play audio 'bullet_hit' at 'a_shot_6' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_6' )
			position v()
	sequence 'shot_7':
		TRIGGER TIMES 1
		Enable object 'g_ray_7'.
		Enable object 'g_decal_7'.
		Enable object 'g_sun_7'.
		Play audio 'bullet_hit' at 'a_shot_7' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_7' )
			position v()
	sequence 'shot_8':
		TRIGGER TIMES 1
		Enable object 'g_ray_8'.
		Enable object 'g_decal_8'.
		Enable object 'g_sun_8'.
		Play audio 'bullet_hit' at 'a_shot_8' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_8' )
			position v()
	sequence 'shot_9':
		TRIGGER TIMES 1
		Enable object 'g_ray_9'.
		Enable object 'g_decal_9'.
		Enable object 'g_sun_9'.
		Play audio 'bullet_hit' at 'a_shot_9' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_9' )
			position v()
	sequence 'shot_10':
		TRIGGER TIMES 1
		Enable object 'g_ray_10'.
		Enable object 'g_decal_10'.
		Enable object 'g_sun_10'.
		Play audio 'bullet_hit' at 'a_shot_10' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_10' )
			position v()
	sequence 'shot_11':
		TRIGGER TIMES 1
		Enable object 'g_ray_11'.
		Enable object 'g_decal_11'.
		Enable object 'g_sun_11'.
		Play audio 'bullet_hit' at 'a_shot_11' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_11' )
			position v()
	sequence 'shot_12':
		TRIGGER TIMES 1
		Enable object 'g_ray_12'.
		Enable object 'g_decal_12'.
		Enable object 'g_sun_12'.
		Play audio 'bullet_hit' at 'a_shot_12' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_12' )
			position v()
	sequence 'shot_13':
		TRIGGER TIMES 1
		Enable object 'g_ray_13'.
		Enable object 'g_decal_13'.
		Enable object 'g_sun_13'.
		Play audio 'bullet_hit' at 'a_shot_13' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_13' )
			position v()
	sequence 'shot_14':
		TRIGGER TIMES 1
		Enable object 'g_ray_14'.
		Enable object 'g_decal_14'.
		Enable object 'g_sun_14'.
		Play audio 'bullet_hit' at 'a_shot_14' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_14' )
			position v()
	sequence 'shot_15':
		TRIGGER TIMES 1
		Enable object 'g_ray_15'.
		Enable object 'g_decal_15'.
		Enable object 'g_sun_15'.
		Play audio 'bullet_hit' at 'a_shot_15' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_15' )
			position v()
	sequence 'shot_16':
		TRIGGER TIMES 1
		Enable object 'g_ray_16'.
		Enable object 'g_decal_16'.
		Enable object 'g_sun_16'.
		Play audio 'bullet_hit' at 'a_shot_16' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_16' )
			position v()
	sequence 'shot_17':
		TRIGGER TIMES 1
		Enable object 'g_ray_17'.
		Enable object 'g_decal_17'.
		Enable object 'g_sun_17'.
		Play audio 'bullet_hit' at 'a_shot_17' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_17' )
			position v()
	sequence 'shot_18':
		TRIGGER TIMES 1
		Enable object 'g_ray_18'.
		Enable object 'g_decal_18'.
		Enable object 'g_sun_18'.
		Play audio 'bullet_hit' at 'a_shot_18' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_18' )
			position v()
	sequence 'shot_19':
		TRIGGER TIMES 1
		Enable object 'g_ray_19'.
		Enable object 'g_decal_19'.
		Enable object 'g_sun_19'.
		Play audio 'bullet_hit' at 'a_shot_19' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_19' )
			position v()
	sequence 'shot_20':
		TRIGGER TIMES 1
		Enable object 'g_ray_20'.
		Enable object 'g_decal_20'.
		Enable object 'g_sun_20'.
		Play audio 'bullet_hit' at 'a_shot_20' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_20' )
			position v()
	sequence 'shot_21':
		TRIGGER TIMES 1
		Enable object 'g_ray_21'.
		Enable object 'g_decal_21'.
		Enable object 'g_sun_21'.
		Play audio 'bullet_hit' at 'a_shot_21' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_21' )
			position v()
	sequence 'shot_22':
		TRIGGER TIMES 1
		Enable object 'g_ray_22'.
		Enable object 'g_decal_22'.
		Enable object 'g_sun_22'.
		Play audio 'bullet_hit' at 'a_shot_22' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_22' )
			position v()
	sequence 'shot_23':
		TRIGGER TIMES 1
		Enable object 'g_ray_23'.
		Enable object 'g_decal_23'.
		Enable object 'g_sun_23'.
		Play audio 'bullet_hit' at 'a_shot_23' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_23' )
			position v()
	sequence 'shot_24':
		TRIGGER TIMES 1
		Enable object 'g_ray_24'.
		Enable object 'g_decal_24'.
		Enable object 'g_sun_24'.
		Play audio 'bullet_hit' at 'a_shot_24' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_24' )
			position v()
	sequence 'shot_25':
		TRIGGER TIMES 1
		Enable object 'g_ray_25'.
		Enable object 'g_decal_25'.
		Enable object 'g_sun_25'.
		Play audio 'bullet_hit' at 'a_shot_25' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_25' )
			position v()
	sequence 'shot_26':
		TRIGGER TIMES 1
		Enable object 'g_ray_26'.
		Enable object 'g_decal_26'.
		Enable object 'g_sun_26'.
		Play audio 'bullet_hit' at 'a_shot_26' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_26' )
			position v()
	sequence 'shot_27':
		TRIGGER TIMES 1
		Enable object 'g_ray_27'.
		Enable object 'g_decal_27'.
		Enable object 'g_sun_27'.
		Play audio 'bullet_hit' at 'a_shot_27' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_27' )
			position v()
	sequence 'shot_28':
		TRIGGER TIMES 1
		Enable object 'g_ray_28'.
		Enable object 'g_decal_28'.
		Enable object 'g_sun_28'.
		Play audio 'bullet_hit' at 'a_shot_28' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_28' )
			position v()
	sequence 'shot_29':
		TRIGGER TIMES 1
		Enable object 'g_ray_29'.
		Enable object 'g_decal_29'.
		Enable object 'g_sun_29'.
		Play audio 'bullet_hit' at 'a_shot_29' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_29' )
			position v()
	sequence 'shot_30':
		TRIGGER TIMES 1
		Enable object 'g_ray_30'.
		Enable object 'g_decal_30'.
		Enable object 'g_sun_30'.
		Play audio 'bullet_hit' at 'a_shot_30' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_30' )
			position v()
	sequence 'shot_31':
		TRIGGER TIMES 1
		Enable object 'g_ray_31'.
		Enable object 'g_decal_31'.
		Enable object 'g_sun_31'.
		Play audio 'bullet_hit' at 'a_shot_31' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_31' )
			position v()
	sequence 'shot_32':
		TRIGGER TIMES 1
		Enable object 'g_ray_32'.
		Enable object 'g_decal_32'.
		Enable object 'g_sun_32'.
		Play audio 'bullet_hit' at 'a_shot_32' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_32' )
			position v()
	sequence 'shot_33':
		TRIGGER TIMES 1
		Enable object 'g_ray_33'.
		Enable object 'g_decal_33'.
		Enable object 'g_sun_33'.
		Play audio 'bullet_hit' at 'a_shot_33' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_33' )
			position v()
	sequence 'shot_34':
		TRIGGER TIMES 1
		Enable object 'g_ray_34'.
		Enable object 'g_decal_34'.
		Enable object 'g_sun_34'.
		Play audio 'bullet_hit' at 'a_shot_34' with switch = 'materials sheet_metal'.
		effect 'effects/particles/bullet_hit/sheet_metal/bullet_hit_sheet_metal':
			parent object( 'a_shot_34' )
			position v()
	sequence 'shot_13_false':
		Disable object 'g_ray_13'.
		Disable object 'g_decal_13'.
		Disable object 'g_sun_13'.
	sequence 'shot_14_false':
		Disable object 'g_ray_14'.
		Disable object 'g_decal_14'.
		Disable object 'g_sun_14'.
	sequence 'shot_15_false':
		Disable object 'g_ray_15'.
		Disable object 'g_sun_15'.
	sequence 'shot_16_false':
		Disable object 'g_ray_16'.
		Disable object 'g_sun_16'.
	sequence 'shot_17_false':
		Disable object 'g_ray_17'.
		Disable object 'g_sun_17'.
	sequence 'shot_18_false':
		Disable object 'g_ray_18'.
		Disable object 'g_sun_18'.
	sequence 'shot_19_false':
		Disable object 'g_ray_19'.
		Disable object 'g_sun_19'.
	sequence 'shot_20_false':
		Disable object 'g_ray_20'.
		Disable object 'g_decal_20'.
		Disable object 'g_sun_20'.
	sequence 'shot_21_false':
		Disable object 'g_ray_21'.
		Disable object 'g_decal_21'.
		Disable object 'g_sun_21'.
	sequence 'shot_22_false':
		Disable object 'g_ray_22'.
		Disable object 'g_sun_22'.
	sequence 'state_hide':
		Disable object 'g_back_door'.
		Disable object 'g_body'.
		Disable object 'g_window_right_big'.
		Disable object 'g_window_left_small'.
		Disable object 'g_window_left_big'.
		Disable object 'g_window_right_small'.
		Disable object 'g_window_front'.
		Disable object 'g_pneumatic_tube_decal'.
		Disable object 'g_hinge'.
		Disable object 'g_left'.
		Disable object 'g_right'.
		Disable object 'g_floor_no_hole'.
		Disable object 's_shadow'.
		Disable object 's_upper'.
		Disable object 's_lift_door'.
		Disable decal_mesh 'dm_window_right_big'.
		Disable decal_mesh 'dm_window_left_small'.
		Disable decal_mesh 'dm_window_left_big'.
		Disable decal_mesh 'dm_window_right_small'.
		Disable decal_mesh 'dm_window_front'.
		Disable decal_mesh 'dm_rubber'.
		Disable decal_mesh 'dm_metal'.
		Disable decal_mesh 'dm_sheet_metal'.
		Disable body 'body0'.
		Disable body 'floor'.
		Disable body 'window_front'.
		Disable body 'window_left_front'.
		Disable body 'window_right_front'.
		Disable body 'window_left_middle'.
		Disable body 'window_right_middle'.
		Disable body 'hitbox_right'.
		Disable body 'hitbox_left'.
		Disable body 'upper_door'.
		Disable body 'big_door'.
		Disable body 'bag_blocker'.
	sequence 'state_show':
		Enable object 'g_back_door'.
		Enable object 'g_body'.
		Enable object 'g_window_right_big'.
		Enable object 'g_window_left_small'.
		Enable object 'g_window_left_big'.
		Enable object 'g_window_right_small'.
		Enable object 'g_window_front'.
		Enable object 'g_pneumatic_tube_decal'.
		Enable object 'g_hinge'.
		Enable object 'g_left'.
		Enable object 'g_right'.
		Enable object 'g_floor_no_hole'.
		Enable object 's_shadow'.
		Enable object 's_upper'.
		Enable object 's_lift_door'.
		Enable decal_mesh 'dm_window_right_big'.
		Enable decal_mesh 'dm_window_left_small'.
		Enable decal_mesh 'dm_window_left_big'.
		Enable decal_mesh 'dm_window_right_small'.
		Enable decal_mesh 'dm_window_front'.
		Enable decal_mesh 'dm_rubber'.
		Enable decal_mesh 'dm_metal'.
		Enable decal_mesh 'dm_sheet_metal'.
		Enable body 'body0'.
		Enable body 'floor'.
		Enable body 'window_front'.
		Enable body 'window_left_front'.
		Enable body 'window_right_front'.
		Enable body 'window_left_middle'.
		Enable body 'window_right_middle'.
		Enable body 'hitbox_right'.
		Enable body 'hitbox_left'.
		Enable body 'upper_door'.
		Enable body 'big_door'.
		Enable body 'bag_blocker'.
