unit:
	sequence 'money_wrap_1':
		Run sequence 'hide'.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'hide':
		TRIGGER TIMES 1
		Disable interaction.
		Disable body 'body0'.
		Disable object 'g_sapphire_shimmer'.
		Disable object 'g_sapphire_inside'.
		Disable object 'g_sapphire_pickup'.
		Disable object 'g_sapphire_glow'.
	sequence 'show_interact':
		TRIGGER TIMES 1
		Enable body 'body0'.
		Enable object 'g_sapphire_shimmer'. (DELAY 5)
		Enable object 'g_sapphire_inside'. (DELAY 5)
		Enable object 'g_sapphire_pickup'. (DELAY 5)
		Enable object 'g_sapphire_glow'. (DELAY 5)
		Enable interaction. (DELAY 5)
	sequence 'trigger_used':
		Disable object 'g_sapphire_shimmer'.
		Disable object 'g_sapphire_inside'.
		Disable object 'g_sapphire_pickup'.
		Disable object 'g_sapphire_glow'.
		Disable body 'body0'.
		Disable interaction.
	sequence 'interact':
		Enable body 'body0'.
		Enable object 'g_sapphire_shimmer'.
		Enable object 'g_sapphire_inside'.
		Enable object 'g_sapphire_pickup'.
		Disable object 'g_sapphire_glow'.
		Disable interaction.
		Run sequence 'trigger_used'.
