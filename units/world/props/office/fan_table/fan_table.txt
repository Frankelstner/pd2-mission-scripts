unit:
	sequence 'dark_version_turn_on':
		Enable object 'g_fan_top'.
		Enable object 'g_fan_base'.
		Disable object 'g_fan_top_white'.
		Disable object 'g_fan_base_white'.
		Run sequence 'turn_on'.
	sequence 'bright_version_turn_on':
		Disable object 'g_fan_top'.
		Disable object 'g_fan_base'.
		Enable object 'g_fan_top_white'.
		Enable object 'g_fan_base_white'.
		Run sequence 'turn_on'.
	sequence 'dark_version_turn_off':
		Enable object 'g_fan_top'.
		Enable object 'g_fan_base'.
		Disable object 'g_fan_top_white'.
		Disable object 'g_fan_base_white'.
		Run sequence 'turn_off'.
	sequence 'bright_version_turn_off':
		Disable object 'g_fan_top'.
		Disable object 'g_fan_base'.
		Enable object 'g_fan_top_white'.
		Enable object 'g_fan_base_white'.
		Run sequence 'turn_off'.
	sequence 'turn_on':
		Enable animation_group 'a_blades':
			loop True
		Enable animation_group 'a_top':
			loop True
		Enable animation_group 'a_paper':
			loop True
		Enable object 'g_fan_paper_01'.
		Enable object 'g_fan_paper_02'.
		Enable object 'g_fan_paper_03'.
	sequence 'turn_off':
		Disable animation_group 'a_blades':
			loop True
		Disable animation_group 'a_top':
			loop True
		Disable animation_group 'a_paper':
			loop True
		Disable object 'g_fan_paper_01'.
		Disable object 'g_fan_paper_02'.
		Disable object 'g_fan_paper_03'.
