unit:
	sequence 'kill_receptiondesk_glass':
		TRIGGER TIMES 1
		Disable object 'g_glass'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass'.
		Disable body 'receptiondesk_glass'.
	body 'receptiondesk_glass'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'kill_receptiondesk_glass'.
