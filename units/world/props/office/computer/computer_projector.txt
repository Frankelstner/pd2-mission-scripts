unit:
	sequence 'destroy_projector':
		effect 'effects/particles/dest/security_camera_dest':
			parent object( 'e_lens' )
			position v()
		Disable object 'g_computer_projector_lens'.
		Disable light 'li_lens'.
	body 'static_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_projector'.
