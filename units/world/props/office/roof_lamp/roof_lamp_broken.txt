unit:
	sequence 'default':
		Enable object 'g_roof_lamp'.
		Enable object 'g_roof_lamp_light'.
		Disable object 'g_roof_lamp_broken'.
		Disable object 'g_roof_lamp_light_broken'.
		Enable body 'whole_body'.
		Disable body 'broken_body'.
		Enable light 'lo_omni'.
		Disable animation_group 'flicker':
			speed 0
			time 1
	sequence 'light_on':
		Enable light 'lo_omni'.
		Enable object 'g_roof_lamp'.
		Enable object 'g_roof_lamp_light'.
		Disable object 'g_roof_lamp_broken'.
		Disable object 'g_roof_lamp_light_broken'.
		Enable body 'whole_body'.
		Disable body 'broken_body'.
		Disable animation_group 'flicker':
			speed 0
			time 0
	sequence 'light_off':
		Disable light 'lo_omni'.
		Enable object 'g_roof_lamp'.
		Enable object 'g_roof_lamp_light'.
		Disable object 'g_roof_lamp_broken'.
		Disable object 'g_roof_lamp_light_broken'.
		Enable body 'whole_body'.
		Disable body 'broken_body'.
		Disable animation_group 'flicker'.
	sequence 'light_flickering':
		Enable light 'lo_omni'.
		Enable object 'g_roof_lamp'.
		Enable object 'g_roof_lamp_light'.
		Disable object 'g_roof_lamp_broken'.
		Disable object 'g_roof_lamp_light_broken'.
		Enable body 'whole_body'.
		Disable body 'broken_body'.
		Enable animation_group 'flicker':
			loop True
			time 0
	sequence 'mesh_broken':
		Enable object 'g_roof_lamp_broken'.
		Enable object 'g_roof_lamp_light_broken'.
		Disable object 'g_roof_lamp'.
		Disable object 'g_roof_lamp_light'.
		Enable body 'broken_body'.
		Disable body 'whole_body'.
		Enable decal_mesh 'dm_roof_lamp_broken'.
		Disable decal_mesh 'dm_roof_lamp'.
	sequence 'mesh_whole':
		Disable object 'g_roof_lamp_broken'.
		Disable object 'g_roof_lamp_light_broken'.
		Enable object 'g_roof_lamp'.
		Enable object 'g_roof_lamp_light'.
		Disable body 'broken_body'.
		Enable body 'whole_body'.
	sequence 'destroy_light':
		TRIGGER TIMES 1
		effect 'effects/particles/dest/sparks_lamp_fluorescent_dest':
			parent object( 'e_light' )
			position v()
		Disable light 'lo_omni'.
	body 'whole_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_light'.
	body 'broken_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_light'.
