unit:
	sequence 'stop':
		Enable animation_group 'loop':
			speed 0
		effect 'effects/particles/dest/security_camera_dest':
			parent  'g_security_camera' 
		Play audio 'emitter_security_camera_explode' at ''.
		Call function: base.generate_cooldown(1)
		Enable animation_group 'dead'.
		Enable object 'g_security_camera_break'.
		Disable object 'g_security_camera'.
		Disable object 'g_lamp'.
		Disable decal_mesh 'dm_security_camera'.
		Enable decal_mesh 'dm_security_camera_break'.
		Enable body 'c_security_camera_break'.
		Disable body 'c_security_camera'.
	body 'c_security_camera'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'stop'.
