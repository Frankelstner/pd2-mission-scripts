unit:
	sequence 'static_1':
		TRIGGER TIMES 1
		Disable body 'static_1'.
		Enable body 'dynamic_1'.
		Call function World:play_physic_effect('core/physic_effects/sequencemanager_push',dest_unit:body('dynamic_1'),dest_unit:body('dynamic_1'):rotation():x()* 60,5).
	sequence 'static_2':
		TRIGGER TIMES 1
		Disable body 'static_2'.
		Enable body 'dynamic_2'.
		Call function World:play_physic_effect('core/physic_effects/sequencemanager_push',dest_unit:body('dynamic_2'),dest_unit:body('dynamic_2'):rotation():x()* -60,5).
	body 'static_1'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'static_1'.
	body 'static_2'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'static_2'.
