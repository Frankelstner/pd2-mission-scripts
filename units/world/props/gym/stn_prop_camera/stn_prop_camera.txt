unit:
	sequence 'state_vis_hide':
		Disable object 'g_stn_prop_camera'.
		Disable decal_mesh 'g_stn_prop_camera'.
		Disable interaction.
	sequence 'state_vis_show':
		Enable object 'g_stn_prop_camera'.
		Enable decal_mesh 'g_stn_prop_camera'.
	sequence 'state_materaial_cheeze':
		material_config 'units/world/props/gym/stn_prop_camera/stn_prop_camera_cheeze'.
		Enable interaction.
	sequence 'state_materaial_placed':
		material_config 'units/world/props/gym/stn_prop_camera/stn_prop_camera'.
	sequence 'state_materaial_outlined':
		material_config 'units/world/props/gym/stn_prop_camera/stn_prop_camera_outline'.
		Disable interaction.
	sequence 'interact'.
