unit:
	sequence 'damage':
		effect 'effects/particles/dest/security_camera_dest':
			parent  'e_fx1' 
		effect 'effects/particles/dest/security_camera_dest':
			parent  'e_fx2' 
		effect 'effects/particles/dest/security_camera_dest':
			parent  'e_fx3' 
		Play audio 'emitter_security_camera_explode' at 'rp_stn_cover_dispatch_central'.
	body 'static_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'damage'.
