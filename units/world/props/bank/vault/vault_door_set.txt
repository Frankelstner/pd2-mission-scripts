unit:
	sequence 'open_door':
		Enable animation_group 'open_door'.
		Play audio 'vault_door_left' at 'a_sound_source_left'.
		Play audio 'vault_door_right' at 'a_sound_source_right'.
	sequence 'close_door':
		Enable animation_group 'open_door':
			from 460/30
			speed -1
			to 0
