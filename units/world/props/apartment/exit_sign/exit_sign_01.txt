unit:
	sequence 'il_on':
		Enable object 'g_exit_sign_01'.
		Enable object 'g_exit_sign_base'.
		Enable object 'g_il'.
		Enable body 'glass'.
	sequence 'il_off':
		Enable object 'g_exit_sign_01'.
		Enable object 'g_exit_sign_base'.
		Disable object 'g_il'.
		Disable body 'glass'.
	sequence 'wallmount_il_on':
		Enable object 'g_exit_sign_01'.
		Disable object 'g_exit_sign_base'.
		Enable object 'g_il'.
		Enable body 'glass'.
	sequence 'wallmount_il_off':
		Enable object 'g_exit_sign_01'.
		Disable object 'g_exit_sign_base'.
		Disable object 'g_il'.
		Disable body 'glass'.
	sequence 'lights_out':
		TRIGGER TIMES 1
		Disable object 'g_il'.
		Disable body 'glass'.
	body 'glass'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'lights_out'.
