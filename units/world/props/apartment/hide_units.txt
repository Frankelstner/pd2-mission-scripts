unit:
	sequence 'g_bookshelf_a_var':
		Disable object 'g_bookshelf_a_var'.
		Disable body 'static_body'.
	sequence 'g_bedwalllamp':
		Disable object 'g_bedwalllamp'.
		Disable body 'static_body'.
	sequence 'g_ht_tv_plasma':
		Disable object 'g_ht_tv_plasma'.
		Disable body 'static_body'.
	sequence 'g_tv_table':
		Disable object 'g_tv_table'.
		Disable body 'static_body'.
	sequence 'g_a4paperpile_02':
		Disable object 'g_a4paperpile_02'.
		Disable body 'static_body'.
	sequence 'g_tea_pot':
		Disable object 'g_tea_pot'.
		Disable body 'static_body'.
	sequence 'g_wst_prop_int_painting_d':
		Disable object 'g_wst_prop_int_painting_d'.
		Disable body 'static_body'.
	sequence 'g_wst_prop_int_painting_c':
		Disable object 'g_wst_prop_int_painting_c'.
		Disable body 'static_body'.
	sequence 'g_shelf_wall_single_2m':
		Disable object 'g_shelf_wall_single_2m'.
		Disable body 'static_body'.
	sequence 'g_stn_prop_socket_wire':
		Disable object 'g_stn_prop_socket_wire'.
		Disable body 'static_body'.
	sequence 'g_wst_prop_int_chair':
		Disable object 'g_g'.
		Disable body 'body1'.
		Disable body 'body2'.
	sequence 'g_wst_prop_int_gen_table_medium':
		Disable object 'g_wst_prop_int_gen_table_medium'.
		Disable body 'static_body'.
	sequence 'g_counter':
		Disable object 'g_counter'.
		Disable body 'static_body'.
	sequence 'g_apartment_stove':
		Disable object 'g_apartment_stove'.
		Disable body 'static_body'.
	sequence 'g_kitchen_fan':
		Disable object 'g_kitchen_fan'.
		Disable body 'static_body'.
	sequence 'g_cupbord':
		Disable object 'g_cupbord'.
		Disable body 'static_body'.
	sequence 'g_man_prop_int_carpet_short':
		Disable object 'g_man_prop_int_carpet_short'.
		Disable body 'static_body'.
	sequence 'g_props_residential_carpet_04_4m':
		Disable object 'g_props_residential_carpet_04_4m'.
		Disable body 'static_body'.
	sequence 'g_props_residential_fridge_dirt':
		Disable object 'g_props_residential_fridge_dirt'.
		Disable body 'static_body'.
	sequence 'g_tv_lcd4':
		Disable object 'g_tv_lcd4'.
		Disable body 'static_body'.
