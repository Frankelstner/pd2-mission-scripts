unit:
	sequence 'default':
		Enable object 'g_props_showercurtain'.
		Disable object 'g_props_showercurtain_loose'.
		Enable body 'showercurtain_body'.
		Disable body 'showercurtain_loose_body'.
		Enable decal_mesh 'dm_showercurtain_thin_layer'.
		Enable decal_mesh 'dm_showercurtain_loose_thin_layer'.
		reset_editable_state True
	sequence 'Variation_1':
		Enable object 'g_props_showercurtain'.
		Disable object 'g_props_showercurtain_loose'.
		Enable body 'showercurtain_body'.
		Disable body 'showercurtain_loose_body'.
		Enable decal_mesh 'dm_showercurtain_thin_layer'.
		Disable decal_mesh 'dm_showercurtain_loose_thin_layer'.
	sequence 'Variation_2':
		Disable object 'g_props_showercurtain'.
		Enable object 'g_props_showercurtain_loose'.
		Disable body 'showercurtain_body'.
		Enable body 'showercurtain_loose_body'.
		Disable decal_mesh 'dm_showercurtain_thin_layer'.
		Enable decal_mesh 'dm_showercurtain_loose_thin_layer'.
	sequence 'All_On':
		Enable object 'g_props_showercurtain'.
		Enable object 'g_props_showercurtain_loose'.
		Enable body 'showercurtain_body'.
		Enable body 'showercurtain_loose_body'.
		Enable decal_mesh 'dm_showercurtain_thin_layer'.
		Enable decal_mesh 'dm_showercurtain_loose_thin_layer'.
