unit:
	sequence 'empty':
		Enable object 'g_frying_pan'.
		Disable object 'g_egg'.
		Disable object 'g_fried_leftovers'.
		Disable object 'g_bacon'.
	sequence 'egg':
		Enable object 'g_frying_pan'.
		Enable object 'g_egg'.
		Disable object 'g_fried_leftovers'.
		Disable object 'g_bacon'.
	sequence 'burgers':
		Enable object 'g_frying_pan'.
		Disable object 'g_egg'.
		Enable object 'g_burgers'.
		Disable object 'g_bacon'.
	sequence 'bacon':
		Enable object 'g_frying_pan'.
		Disable object 'g_egg'.
		Disable object 'g_burgers'.
		Enable object 'g_bacon'.
