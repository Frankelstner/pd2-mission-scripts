unit:
	sequence 'make_dynamic':
		Disable body 'dynamic_body'.
		Enable body 'static_body'.
		Disable body 'static_body'.
		Enable body 'dynamic_body'.
		slot:
			slot 11
		Set bullet damage to 0.
		Set melee damage to 0.
	body 'static_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'make_dynamic'.
	body 'dynamic_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'make_dynamic'.
