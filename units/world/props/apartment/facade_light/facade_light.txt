unit:
	sequence 'default':
		reset_editable_state True
	sequence 'on':
		Enable light 'lo_omni'.
		Disable animation_group 'flicker':
			speed 0
			time 0
		Stop audio 'emitter_flickering_light' at 'snd_src'.
		Play audio 'emitter_fluorescent_light' at 'snd_src'.
	sequence 'off':
		Disable light 'lo_omni'.
		Disable animation_group 'flicker'.
		Stop audio 'emitter_flickering_light' at 'snd_src'.
		Stop audio 'emitter_fluorescent_light' at 'snd_src'.
	sequence 'flickering':
		Enable light 'lo_omni'.
		Enable animation_group 'flicker':
			loop True
			time 0
		Stop audio 'emitter_fluorescent_light' at 'snd_src'.
		Play audio 'emitter_flickering_light' at 'snd_src'.
