unit:
	sequence 'blink_on':
		EXECUTE ON STARTUP
		Enable animation_group 'ag_blink':
			loop true 
			time 0
		startup True
	sequence 'blink_off':
		Enable animation_group 'ag_blink':
			loop False
			time 1.5
	sequence 'make_dynamic':
		Disable body 'radio_static_body'.
		Enable body 'radio_dynamic_body'.
		slot:
			slot 11
	body 'radio_static_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'make_dynamic'.
			Run sequence 'blink_off'.
