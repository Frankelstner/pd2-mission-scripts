unit:
	sequence 'startup_times':
		EXECUTE ON STARTUP
		Run sequence 'start_time_'..pick('1','2','3','4','5','6').
		startup True
	sequence 'start_time_1':
		Enable animation_group 'ag_turn_slow': (DELAY 10/30)
			loop True
	sequence 'start_time_2':
		Enable animation_group 'ag_turn_slow': (DELAY 20/30)
			loop True
	sequence 'start_time_3':
		Enable animation_group 'ag_turn_slow': (DELAY 30/30)
			loop True
	sequence 'start_time_4':
		Enable animation_group 'ag_turn_slow': (DELAY 40/30)
			loop True
	sequence 'start_time_5':
		Enable animation_group 'ag_turn_slow': (DELAY 50/30)
			loop True
	sequence 'start_time_6':
		Enable animation_group 'ag_turn_slow': (DELAY 60/30)
			loop True
	sequence 'turn_on_slow':
		Enable object 'g_wire_socket_slow'.
		Enable object 'g_whole_lamp_slow'.
		Disable object 'g_broken_lamp_slow'.
		Disable object 'g_wire_socket'.
		Disable object 'g_whole_lamp'.
		Disable object 'g_broken_lamp'.
		Disable object 'g_whole_lamp_unlit'.
		Disable object 'g_whole_slow_lamp_unlit'.
		Enable light 'lo_omni'.
	sequence 'turn_on_pendulum':
		Enable light 'lo_omni'.
		Disable object 'g_wire_socket_slow'.
		Disable object 'g_whole_lamp_slow'.
		Disable object 'g_broken_lamp_slow'.
		Enable object 'g_wire_socket'.
		Enable object 'g_whole_lamp'.
		Disable object 'g_broken_lamp'.
		Disable object 'g_whole_lamp_unlit'.
		Disable object 'g_whole_slow_lamp_unlit'.
	sequence 'turn_off_slow':
		Disable light 'lo_omni'.
		Enable object 'g_wire_socket_slow'.
		Disable object 'g_broken_lamp_slow'.
		Disable object 'g_wire_socket'.
		Disable object 'g_whole_lamp'.
		Disable object 'g_whole_lamp_slow'.
		Disable object 'g_broken_lamp'.
		Enable object 'g_whole_slow_lamp_unlit'.
		Disable object 'g_whole_lamp_unlit'.
	sequence 'turn_off_pendulum':
		Disable light 'lo_omni'.
		Disable object 'g_wire_socket_slow'.
		Disable object 'g_whole_lamp_slow'.
		Disable object 'g_broken_lamp_slow'.
		Enable object 'g_wire_socket'.
		Disable object 'g_whole_lamp'.
		Disable object 'g_broken_lamp'.
		Enable object 'g_whole_lamp_unlit'.
		Disable object 'g_whole_slow_lamp_unlit'.
	sequence 'broken_slow':
		Disable light 'lo_omni'.
		Enable object 'g_wire_socket_slow'.
		Disable object 'g_whole_lamp_slow'.
		Enable object 'g_broken_lamp_slow'.
		Disable object 'g_wire_socket'.
		Disable object 'g_whole_lamp'.
		Disable object 'g_broken_lamp'.
		Disable body 'light_bulb'.
		Disable object 'g_whole_lamp_unlit'.
		Disable object 'g_whole_slow_lamp_unlit'.
	sequence 'broken_pendulum':
		Disable light 'lo_omni'.
		Disable object 'g_wire_socket_slow'.
		Disable object 'g_whole_lamp_slow'.
		Disable object 'g_broken_lamp_slow'.
		Enable object 'g_wire_socket'.
		Disable object 'g_whole_lamp'.
		Enable object 'g_broken_lamp'.
		Disable body 'light_bulb'.
		Disable object 'g_whole_lamp_unlit'.
		Disable object 'g_whole_slow_lamp_unlit'.
	sequence 'kill_light_bulb':
		Disable light 'lo_omni'.
		Disable object 'g_wire_socket_slow'.
		Disable object 'g_whole_lamp_slow'.
		Disable object 'g_broken_lamp_slow'.
		Enable object 'g_wire_socket'.
		Disable object 'g_whole_lamp'.
		Enable object 'g_broken_lamp'.
		Disable body 'light_bulb'.
		Play audio 'light_bulb_smash' at 'c_lamp_mesh_mopp'.
		Disable object 'g_whole_lamp_unlit'.
		Disable object 'g_whole_slow_lamp_unlit'.
	sequence 'hide':
		Disable object 'g_ceiling_contact'.
		Disable light 'lo_omni'.
		Disable object 'g_wire_socket_slow'.
		Disable object 'g_whole_lamp_slow'.
		Disable object 'g_broken_lamp_slow'.
		Disable object 'g_wire_socket'.
		Disable object 'g_whole_lamp'.
		Disable object 'g_broken_lamp'.
		Disable object 'g_whole_lamp_unlit'.
		Disable object 'g_whole_slow_lamp_unlit'.
	body 'light_bulb'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'kill_light_bulb'.
