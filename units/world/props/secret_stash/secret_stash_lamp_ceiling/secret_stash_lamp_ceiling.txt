unit:
	sequence 'light_on':
		material_config 'units/world/props/secret_stash/secret_stash_lamp_ceiling/secret_stash_lamp_ceiling_on'.
	sequence 'light_off':
		material_config 'units/world/props/secret_stash/secret_stash_lamp_ceiling/secret_stash_lamp_ceiling_off'.
