unit:
	body 'window'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'kill_window_01'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'kill_window_02'.
	sequence 'kill_window_01':
		TRIGGER TIMES 1
		Disable object 'g_slaughterhouse_glass'.
		Enable object 'g_slaughterhouse_glass_dmg'.
	sequence 'kill_window_02':
		TRIGGER TIMES 1
		Disable decal_mesh 'g_slaughterhouse_glass'.
		Disable object 'g_slaughterhouse_glass'.
		Disable object 'g_slaughterhouse_glass_dmg'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_win_1' )
			position v()
		Play audio 'window_small_shatter' at 'e_win_1'.
		Disable body 'window'.
