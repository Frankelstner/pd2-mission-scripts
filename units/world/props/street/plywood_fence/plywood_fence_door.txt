unit:
	sequence 'open':
		Enable animation_group 'anim'.
		Play audio 'fence_gate_open' at 'a_hinge'.
	sequence 'open_90_deg':
		Enable animation_group 'anim':
			end_time 40/30
			speed 1
			time 0
		Play audio 'fence_gate_open' at 'a_hinge'.
	sequence 'open_fast':
		Enable animation_group 'anim':
			speed 2.5
		Play audio 'fence_gate_open' at 'a_hinge'.
	sequence 'close':
		Enable animation_group 'anim':
			end_time 0
			speed -1
			time 60/30
		Play audio 'fence_gate_close' at 'a_hinge'.
