unit:
	sequence 'kill_win_1_01':
		TRIGGER TIMES 1
		Disable object 'g_glass'.
		Enable object 'g_glass_dmg'.
		Play audio 'glass_crack' at 'e_win_1'.
	sequence 'kill_win_1_02':
		TRIGGER TIMES 1
		Disable body 'glass_breakable'.
		Disable object 'g_glass_dmg'.
		Enable object 'g_glass_broken'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_win_1' )
			position v()
		Play audio 'window_small_shatter' at 'e_win_1'.
	body 'glass_breakable'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'kill_win_1_01'.
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_win_1_02'.
