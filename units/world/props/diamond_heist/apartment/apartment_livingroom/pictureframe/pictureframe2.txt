unit:
	sequence 'make_dynamic':
		slot:
			slot 11
		Disable body 'frame_static_body'.
		Enable body 'frame_dynamic_body'.
	body 'frame_static_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'make_dynamic'.
