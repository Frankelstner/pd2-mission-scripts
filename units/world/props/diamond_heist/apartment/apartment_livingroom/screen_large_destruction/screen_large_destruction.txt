unit:
	sequence 'destroyed_01':
		TRIGGER TIMES 1
		Disable object 'g_set_01'.
		Enable object 'g_broken_01'.
		Enable effect_spawner 'es_e_spawner_01'.
		Disable body 'destroy_01'.
		Enable body 'static_body_broken_01'.
		Disable decal_mesh 'c_screen_large'.
		Enable decal_mesh 'c_broken_01'.
	sequence 'destroyed_02':
		TRIGGER TIMES 1
		Disable object 'g_set_02'.
		Enable object 'g_broken_02'.
		Enable effect_spawner 'es_e_spawner_02'.
		Disable body 'destroy_02'.
		Enable body 'static_body_broken_02'.
		Disable decal_mesh 'c_screen_large'.
		Enable decal_mesh 'c_broken_02'.
	sequence 'destroyed_03':
		TRIGGER TIMES 1
		Disable object 'g_set_03'.
		Enable object 'g_broken_03'.
		Enable effect_spawner 'es_e_spawner_03'.
		Disable body 'destroy_03'.
		Enable body 'static_body_broken_03'.
		Disable decal_mesh 'c_screen_large'.
		Enable decal_mesh 'c_broken_03'.
	sequence 'destroyed_04':
		TRIGGER TIMES 1
		Disable object 'g_set_04'.
		Enable object 'g_broken_04'.
		Enable effect_spawner 'es_e_spawner_04'.
		Disable body 'destroy_04'.
		Enable body 'static_body_broken_04'.
		Disable decal_mesh 'c_screen_large'.
		Enable decal_mesh 'c_broken_04'.
	sequence 'destroyed_05':
		TRIGGER TIMES 1
		Disable object 'g_set_05'.
		Enable object 'g_broken_05'.
		Enable effect_spawner 'es_e_spawner_05'.
		Disable body 'destroy_05'.
		Enable body 'static_body_broken_05'.
		Disable decal_mesh 'c_screen_large'.
		Enable decal_mesh 'c_broken_05'.
	sequence 'destroyed_06':
		TRIGGER TIMES 1
		Disable object 'g_set_06'.
		Enable object 'g_broken_06'.
		Enable effect_spawner 'es_e_spawner_06'.
		Disable body 'destroy_06'.
		Enable body 'static_body_broken_06'.
		Disable decal_mesh 'c_screen_large'.
		Enable decal_mesh 'c_broken_06'.
	sequence 'destroyed_07':
		TRIGGER TIMES 1
		Disable object 'g_set_07'.
		Enable object 'g_broken_07'.
		Enable effect_spawner 'es_e_spawner_07'.
		Disable body 'destroy_07'.
		Enable body 'static_body_broken_07'.
		Disable decal_mesh 'c_screen_large'.
		Enable decal_mesh 'c_broken_07'.
	sequence 'destroyed_08':
		TRIGGER TIMES 1
		Disable object 'g_set_08'.
		Enable object 'g_broken_08'.
		Enable effect_spawner 'es_e_spawner_08'.
		Disable body 'destroy_08'.
		Enable body 'static_body_broken_08'.
		Disable decal_mesh 'c_screen_large'.
		Enable decal_mesh 'c_broken_08'.
	body 'destroy_01'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroyed_01'.
	body 'destroy_02'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroyed_02'.
	body 'destroy_03'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroyed_03'.
	body 'destroy_04'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroyed_04'.
	body 'destroy_05'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroyed_05'.
	body 'destroy_06'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroyed_06'.
	body 'destroy_07'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroyed_07'.
	body 'destroy_08'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroyed_08'.
