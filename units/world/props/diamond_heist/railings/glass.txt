unit:
	body 'glass_1'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crack_all'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'kill_window_1'.
	body 'glass_2'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crack_all'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'kill_window_2'.
	body 'glass_3'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crack_all'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'kill_window_3'.
	sequence 'crack_all':
		TRIGGER TIMES 1
		Enable object 'g_glass_dmg_1'.
		Enable object 'g_glass_dmg_2'.
		Enable object 'g_glass_dmg_3'.
		Disable object 'g_glass'.
	sequence 'kill_window_1':
		TRIGGER TIMES 1
		Disable object 'g_glass_dmg_1'.
		Disable decal_mesh 'c_glass_1'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_1' )
			position v()
		Play audio 'window_small_shatter' at 'e_window_1'.
		Disable body 'glass_1'.
		Cause alert with 12 m radius.
	sequence 'kill_window_2':
		TRIGGER TIMES 1
		Disable object 'g_glass_dmg_2'.
		Disable decal_mesh 'c_glass_2'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_2' )
			position v()
		Play audio 'window_small_shatter' at 'e_window_2'.
		Disable body 'glass_2'.
		Cause alert with 12 m radius.
	sequence 'kill_window_3':
		TRIGGER TIMES 1
		Disable object 'g_glass_dmg_3'.
		Disable decal_mesh 'c_glass_3'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_3' )
			position v()
		Play audio 'window_small_shatter' at 'e_window_3'.
		Disable body 'glass_3'.
		Cause alert with 12 m radius.
