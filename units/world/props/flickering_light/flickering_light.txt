unit:
	sequence 'default':
		Disable animation_group 'flickering'.
		reset_editable_state True
	sequence 'flicker_1':
		Enable light 'lo_light_flicker'. (DELAY 0)
		Enable animation_group 'flickering': (DELAY 0.01)
			from 1/30
			loop True
			to 300/30
		Play audio 'emitter_flickering_light' at 'snd'.
	sequence 'flicker_2':
		Enable light 'lo_light_flicker'. (DELAY 0)
		Enable animation_group 'flickering': (DELAY 0.01)
			from 301/30
			loop True
			to 600/30
		Play audio 'emitter_flickering_light' at 'snd'.
	sequence 'flicker_3':
		Enable light 'lo_light_flicker'. (DELAY 0)
		Enable animation_group 'flickering': (DELAY 0.01)
			from 601/30
			loop True
			to 900/30
		Play audio 'emitter_flickering_light' at 'snd'.
	sequence 'light_off':
		Disable light 'lo_light_flicker'. (DELAY 0)
		Stop audio 'emitter_flickering_light' at 'snd'.
	sequence 'light_on':
		Enable light 'lo_light_flicker'. (DELAY 0)
		Stop audio 'emitter_flickering_light' at 'snd'.
