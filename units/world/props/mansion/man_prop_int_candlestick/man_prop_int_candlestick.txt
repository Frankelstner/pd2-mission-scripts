unit:
	sequence 'break':
		Disable body 'static_body'.
		Enable body 'dynamic_body'.
	body 'static_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break'.
