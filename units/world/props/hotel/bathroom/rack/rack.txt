unit:
	sequence 'default':
		Run sequence 'c_rack_3'.
		reset_editable_state True
	sequence 'g_rack_1a':
		Enable object 'g_rack_1a'.
		Disable object 'g_rack_2'.
		Disable object 'g_rack1b'.
		Disable object 'g_rack1b_metal'.
		Disable object 'g_rack_3'.
		Enable body 'g_rack_1a'.
		Disable body 'g_rack_2'.
		Disable body 'g_rack1b'.
		Disable body 'c_rack_3'.
		Disable decal_mesh 'g_rack_3'.
		Disable decal_mesh 'g_rack1b'.
		Disable decal_mesh 'g_rack1b_metal'.
		Disable decal_mesh 'g_rack_2'.
		Enable decal_mesh 'g_rack_1a'.
	sequence 'g_rack_2':
		Disable object 'g_rack_1a'.
		Enable object 'g_rack_2'.
		Disable object 'g_rack1b'.
		Disable object 'g_rack1b_metal'.
		Disable object 'g_rack_3'.
		Disable body 'g_rack_1a'.
		Enable body 'g_rack_2'.
		Disable body 'g_rack1b'.
		Disable body 'c_rack_3'.
		Disable decal_mesh 'g_rack_3'.
		Disable decal_mesh 'g_rack1b'.
		Disable decal_mesh 'g_rack1b_metal'.
		Enable decal_mesh 'g_rack_2'.
		Disable decal_mesh 'g_rack_1a'.
	sequence 'g_rack1b':
		Enable object 'g_rack1b_metal'.
		Disable object 'g_rack_1a'.
		Disable object 'g_rack_2'.
		Enable object 'g_rack1b'.
		Disable object 'g_rack_3'.
		Disable body 'g_rack_1a'.
		Disable body 'g_rack_2'.
		Enable body 'g_rack1b'.
		Enable body 'c_rack_3'.
		Disable decal_mesh 'g_rack_3'.
		Enable decal_mesh 'g_rack1b'.
		Disable decal_mesh 'g_rack1b_metal'.
		Disable decal_mesh 'g_rack_2'.
		Disable decal_mesh 'g_rack_1a'.
	sequence 'c_rack_3':
		Disable object 'g_rack1b_metal'.
		Disable object 'g_rack_1a'.
		Disable object 'g_rack_2'.
		Disable object 'g_rack1b'.
		Enable object 'g_rack_3'.
		Disable body 'g_rack_1a'.
		Disable body 'g_rack_2'.
		Disable body 'g_rack1b'.
		Enable body 'c_rack_3'.
		Enable decal_mesh 'g_rack_3'.
		Disable decal_mesh 'g_rack1b'.
		Disable decal_mesh 'g_rack1b_metal'.
		Disable decal_mesh 'g_rack_2'.
		Disable decal_mesh 'g_rack_1a'.
