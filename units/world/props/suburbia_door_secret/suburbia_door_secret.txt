unit:
	open = 0
	int = 0
	sequence 'interact':
		If open == 1: Run sequence 'int_seq_text_open'.
		If open == 0: Run sequence 'int_seq_text_close'.
		If open == 0: Run sequence 'int_seq_set_var_1'.
	sequence 'enable_interaction':
		Enable interaction.
		If int == 1: Run sequence 'int_seq_set_var_0'.
		If int == 1: filter = 'filter_fix2'
		If int == 1: int = 2
		If open == 1: filter = 'filter_open'
		If open == 1: int = 1
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'int_seq_set_var_0':
		If open == 1: filter = 'filter_open'
		If open == 1: open = 0
	sequence 'int_seq_set_var_1':
		If open == 0: filter = 'filter_close'
		If open == 0: open = 1
	sequence 'int_seq_text_close':
		Run sequence 'open'.
	sequence 'int_seq_text_open':
		Run sequence 'close'.
	sequence 'enable_contour':
		material_config 'units/world/props/suburbia_door_secret/suburbia_door_secret_contour'.
	sequence 'disable_contour':
		material_config 'units/world/props/suburbia_door_secret/suburbia_door_secret'.
	sequence 'open':
		Enable animation_group 'door':
			from 0/30
			speed 1
			to 132/30
		Play audio 'cft_bookcase' at 'rp_suburbia_door_secret'.
		Add attention/detection preset 'prop_ene_ntl' to 'int_location' (alarm reason: 'breaking_entering').
	sequence 'close':
		Enable animation_group 'door':
			from 132/30
			speed -1
			to 0/30
		Play audio 'cft_bookcase' at 'rp_suburbia_door_secret'.
		Remove attention/detection preset: 'prop_ene_ntl'
	sequence 'hide':
		Disable body 'body_1'.
		Disable decal_mesh 'dm_paper'.
		Disable decal_mesh 'dm_plastic'.
		Disable object 'g_mesh'.
		Disable object 'g_books'.
	sequence 'show':
		Enable body 'body_1'.
		Enable decal_mesh 'dm_paper'.
		Enable decal_mesh 'dm_plastic'.
		Enable object 'g_mesh'.
		Enable object 'g_books'.
