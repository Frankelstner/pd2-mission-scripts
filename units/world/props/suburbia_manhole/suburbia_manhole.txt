unit:
	sequence 'state_no_contours':
		material_config 'units/equipment/garden_tap_interactive/main_water_valve_suburbia'.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'hide_plate':
		Disable body 'plate_mover'.
		Disable body 'plate_bullet'.
		Disable object 'g_plate'.
	sequence 'open':
		TRIGGER TIMES 1
		Disable body 'plate_mover'.
		Enable animation_group 'open':
			from 0/30
			speed 1
			to 60/30
		Play audio 'cft_manhole_cover_slide' at 'a_plate'.
	sequence 'interact':
		Run sequence 'open'.
		Run sequence 'disable_interaction'.
