unit:
	sequence 'interactive_off':
		Enable object 'g_necklace'.
		Hide graphic_group 'grp_glow'.
		Disable interaction.
	sequence 'interactive_on':
		Enable object 'g_necklace'.
		Show graphic_group 'grp_glow'.
		Enable interaction.
	sequence 'default':
		Run sequence 'interactive_on'.
		reset_editable_state True
	sequence 'trigger_used':
		Disable object 'g_necklace'.
		Hide graphic_group 'grp_glow'.
		Disable interaction.
	sequence 'interact':
		Enable object 'g_necklace'.
		Hide graphic_group 'grp_glow'.
		Disable interaction.
		Run sequence 'trigger_used'.
	sequence 'money_wrap_1'.
