unit:
	sequence 'interactive_off':
		Enable body 'body0'.
		Enable object 'g_diamond_pickup'.
		Enable object 'g_diamond_pickup_lod1'.
		Enable object 'g_diamond_inside'.
		Enable object 'g_diamond_inside_lod1'.
		Enable object 'g_diamond_shimmer'.
		Enable object 'g_diamond_shimmer_lod1'.
		Enable object 'g_caustics'.
		Disable object 'g_diamond_pickup_glow'.
		Disable object 'g_diamond_pickup_glow_lod1'.
		Disable interaction.
	sequence 'interactive_on':
		Enable body 'body0'.
		Enable object 'g_diamond_pickup'.
		Enable object 'g_diamond_pickup_lod1'.
		Enable object 'g_diamond_inside'.
		Enable object 'g_diamond_inside_lod1'.
		Enable object 'g_diamond_shimmer'.
		Enable object 'g_diamond_shimmer_lod1'.
		Enable object 'g_caustics'.
		Enable object 'g_diamond_pickup_glow'.
		Enable object 'g_diamond_pickup_glow_lod1'.
		Enable interaction.
	sequence 'default':
		Run sequence 'interactive_on'.
		reset_editable_state True
	sequence 'trigger_used':
		Disable object 'g_diamond_pickup_glow'.
		Disable object 'g_diamond_pickup_glow_lod1'.
		Disable object 'g_diamond_pickup'.
		Disable object 'g_diamond_pickup_lod1'.
		Disable object 'g_diamond_inside'.
		Disable object 'g_diamond_inside_lod1'.
		Disable object 'g_diamond_shimmer'.
		Disable object 'g_diamond_shimmer_lod1'.
		Disable object 'g_caustics'.
		Disable body 'body0'.
		Disable interaction.
	sequence 'interact':
		Enable body 'body0'.
		Enable object 'g_diamond_pickup'.
		Enable object 'g_diamond_pickup_lod1'.
		Enable object 'g_diamond_inside'.
		Enable object 'g_diamond_inside_lod1'.
		Enable object 'g_diamond_shimmer'.
		Enable object 'g_diamond_shimmer_lod1'.
		Enable object 'g_caustics'.
		Disable object 'g_diamond_pickup_glow'.
		Disable object 'g_diamond_pickup_glow_lod1'.
		Disable interaction.
		Run sequence 'trigger_used'.
	sequence 'money_wrap_1'.
