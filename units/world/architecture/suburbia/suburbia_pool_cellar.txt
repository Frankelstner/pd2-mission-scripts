unit:
	sequence 'destroy':
		Disable body 'fill'.
		Enable body 'debris'.
		Disable decal_mesh 'dm_concrete_fill'.
		Enable decal_mesh 'dm_concrete_dest'.
		Disable object 'g_fill'.
		Enable object 'g_dest'.
		Disable object 's_fill'.
		Enable object 's_dest'.
	sequence 'heal':
		Enable body 'fill'.
		Disable body 'debris'.
		Enable decal_mesh 'dm_concrete_fill'.
		Disable decal_mesh 'dm_concrete_dest'.
		Enable object 'g_fill'.
		Disable object 'g_dest'.
		Enable object 's_fill'.
		Disable object 's_dest'.
