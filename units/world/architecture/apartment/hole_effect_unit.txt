unit:
	sequence 'off':
		Disable effect_spawner 'es_e_env'.
		Disable effect_spawner 'es_e_debris_029'.
		Disable effect_spawner 'es_e_debris_028'.
		Disable effect_spawner 'es_e_debris_027'.
		Disable effect_spawner 'es_e_debris_026'.
		Disable effect_spawner 'es_e_debris_025'.
		Disable effect_spawner 'es_e_debris_024'.
		Disable effect_spawner 'es_e_debris_023'.
		Disable effect_spawner 'es_e_debris_022'.
		Disable effect_spawner 'es_e_debris_021'.
		Disable effect_spawner 'es_e_debris_020'.
		Disable effect_spawner 'es_e_debris_019'.
		Disable effect_spawner 'es_e_debris_018'.
		Disable effect_spawner 'es_e_debris_017'.
		Disable effect_spawner 'es_e_debris_016'.
		Disable effect_spawner 'es_e_debris_015'.
		Disable effect_spawner 'es_e_debris_014'.
		Disable effect_spawner 'es_e_debris_013'.
		Disable effect_spawner 'es_e_debris_012'.
		Disable effect_spawner 'es_e_debris_011'.
		Disable effect_spawner 'es_e_debris_010'.
		Disable effect_spawner 'es_e_debris_09'.
		Disable effect_spawner 'es_e_debris_08'.
		Disable effect_spawner 'es_e_debris_07'.
		Disable effect_spawner 'es_e_debris_06'.
		Disable effect_spawner 'es_e_debris_05'.
		Disable effect_spawner 'es_e_debris_04'.
		Disable effect_spawner 'es_e_debris_03'.
		Disable effect_spawner 'es_e_debris_02'.
		Disable effect_spawner 'es_e_debris_01'.
		Disable effect_spawner 'es_e_fire'.
	sequence 'enable':
		Enable effect_spawner 'es_e_env'.
		Enable effect_spawner 'es_e_debris_029'. (DELAY 0.01)
		Enable effect_spawner 'es_e_debris_028'. (DELAY 0.13)
		Enable effect_spawner 'es_e_debris_027'. (DELAY 0.06)
		Enable effect_spawner 'es_e_debris_026'. (DELAY 0.1)
		Enable effect_spawner 'es_e_debris_025'. (DELAY 0.11)
		Enable effect_spawner 'es_e_debris_024'. (DELAY 0.01)
		Enable effect_spawner 'es_e_debris_023'. (DELAY 0.21)
		Enable effect_spawner 'es_e_debris_022'. (DELAY 0.01)
		Enable effect_spawner 'es_e_debris_021'. (DELAY 0.03)
		Enable effect_spawner 'es_e_debris_020'. (DELAY 0.11)
		Enable effect_spawner 'es_e_debris_019'. (DELAY 0.03)
		Enable effect_spawner 'es_e_debris_018'. (DELAY 0.07)
		Enable effect_spawner 'es_e_debris_017'. (DELAY 0.01)
		Enable effect_spawner 'es_e_debris_016'. (DELAY 0.2)
		Enable effect_spawner 'es_e_debris_015'. (DELAY 0.08)
		Enable effect_spawner 'es_e_debris_014'. (DELAY 0.01)
		Enable effect_spawner 'es_e_debris_013'. (DELAY 0.02)
		Enable effect_spawner 'es_e_debris_012'. (DELAY 0.21)
		Enable effect_spawner 'es_e_debris_011'. (DELAY 0.05)
		Enable effect_spawner 'es_e_debris_010'. (DELAY 0.03)
		Enable effect_spawner 'es_e_debris_09'. (DELAY 0.23)
		Enable effect_spawner 'es_e_debris_08'. (DELAY 0.1)
		Enable effect_spawner 'es_e_debris_07'. (DELAY 0.01)
		Enable effect_spawner 'es_e_debris_06'. (DELAY 0.02)
		Enable effect_spawner 'es_e_debris_05'. (DELAY 0.2)
		Enable effect_spawner 'es_e_debris_04'. (DELAY 0.01)
		Enable effect_spawner 'es_e_debris_03'. (DELAY 0.11)
		Enable effect_spawner 'es_e_debris_02'. (DELAY 0.03)
		Enable effect_spawner 'es_e_debris_01'. (DELAY 0.01)
		Enable effect_spawner 'es_e_fire'.
