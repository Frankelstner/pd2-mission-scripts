unit:
	sequence 'make_dynamic':
		Enable body 'dynamic_body'.
		Disable body 'static_body_player'.
		slot:
			slot 11
	body 'bullet'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'make_dynamic'.
