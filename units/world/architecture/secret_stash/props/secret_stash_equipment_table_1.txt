unit:
	monitor_broken_01 = 0
	monitor_broken_02 = 0
	monitor_broken_03 = 0
	monitor_broken_04 = 0
	monitor_broken_05 = 0
	monitor_broken_06 = 0
	sequence 'hide':
		Disable body 'static_body'.
		Disable decal_mesh 'dm_secret_stash_equipment_table_1_metal'.
		Disable decal_mesh 'dm_secret_stash_equipment_table_1_electric'.
		Disable decal_mesh 'dm_secret_stash_equipment_table_1_hardwood'.
		Disable object 'g_secret_stash_equipment_table_1'.
		Disable object 'g_secret_stash_equipment_table_1_alpha'.
		Disable object 'g_monitors_on'.
		Disable object 'g_monitors_off'.
		Disable object 'g_monitors_input'.
		Disable body 'static_monitor_small_01'.
		Disable body 'static_monitor_small_02'.
		Disable body 'static_monitor_small_03'.
		Disable body 'static_monitor_small_04'.
		Disable body 'static_monitor_small_05'.
		Disable body 'static_monitor_small_06'.
		Disable object 'g_monitors_small_off_01'.
		Disable object 'g_monitors_small_off_02'.
		Disable object 'g_monitors_small_off_03'.
		Disable object 'g_monitors_small_off_04'.
		Disable object 'g_monitors_small_off_05'.
		Disable object 'g_monitors_small_off_06'.
		Disable object 'g_monitors_small_on_01'.
		Disable object 'g_monitors_small_on_02'.
		Disable object 'g_monitors_small_on_03'.
		Disable object 'g_monitors_small_on_04'.
		Disable object 'g_monitors_small_on_05'.
		Disable object 'g_monitors_small_on_06'.
	sequence 'show':
		Enable body 'static_body'.
		Enable decal_mesh 'dm_secret_stash_equipment_table_1_metal'.
		Enable decal_mesh 'dm_secret_stash_equipment_table_1_electric'.
		Enable decal_mesh 'dm_secret_stash_equipment_table_1_hardwood'.
		Enable object 'g_secret_stash_equipment_table_1'.
		Enable object 'g_secret_stash_equipment_table_1_alpha'.
		Enable object 'g_monitors_on'.
		Disable object 'g_monitors_off'.
		Disable object 'g_monitors_input'.
		Enable body 'static_monitor_small_01'.
		Enable body 'static_monitor_small_02'.
		Enable body 'static_monitor_small_03'.
		Enable body 'static_monitor_small_04'.
		Enable body 'static_monitor_small_05'.
		Enable body 'static_monitor_small_06'.
		Disable object 'g_monitors_small_off_01'.
		Disable object 'g_monitors_small_off_02'.
		Disable object 'g_monitors_small_off_03'.
		Disable object 'g_monitors_small_off_04'.
		Disable object 'g_monitors_small_off_05'.
		Disable object 'g_monitors_small_off_06'.
		Enable object 'g_monitors_small_on_01'.
		Enable object 'g_monitors_small_on_02'.
		Enable object 'g_monitors_small_on_03'.
		Enable object 'g_monitors_small_on_04'.
		Enable object 'g_monitors_small_on_05'.
		Enable object 'g_monitors_small_on_06'.
	sequence 'monitors_off':
		Disable object 'g_monitors_on'.
		Enable object 'g_monitors_off'.
		If monitor_broken_01 == 0: Run sequence 'small_monitor_01_off'.
		If monitor_broken_02 == 0: Run sequence 'small_monitor_02_off'.
		If monitor_broken_03 == 0: Run sequence 'small_monitor_03_off'.
		If monitor_broken_04 == 0: Run sequence 'small_monitor_04_off'.
		If monitor_broken_05 == 0: Run sequence 'small_monitor_05_off'.
		If monitor_broken_06 == 0: Run sequence 'small_monitor_06_off'.
	sequence 'monitors_on':
		Enable object 'g_monitors_on'.
		Disable object 'g_monitors_off'.
		If monitor_broken_01 == 0: Run sequence 'small_monitor_01_on'.
		If monitor_broken_02 == 0: Run sequence 'small_monitor_02_on'.
		If monitor_broken_03 == 0: Run sequence 'small_monitor_03_on'.
		If monitor_broken_04 == 0: Run sequence 'small_monitor_04_on'.
		If monitor_broken_05 == 0: Run sequence 'small_monitor_05_on'.
		If monitor_broken_06 == 0: Run sequence 'small_monitor_06_on'.
	sequence 'small_monitor_01_on':
		Enable object 'g_monitors_small_on_01'.
		Disable object 'g_monitors_small_off_01'.
	sequence 'small_monitor_02_on':
		Enable object 'g_monitors_small_on_02'.
		Disable object 'g_monitors_small_off_02'.
	sequence 'small_monitor_03_on':
		Enable object 'g_monitors_small_on_03'.
		Disable object 'g_monitors_small_off_03'.
	sequence 'small_monitor_04_on':
		Enable object 'g_monitors_small_on_04'.
		Disable object 'g_monitors_small_off_04'.
	sequence 'small_monitor_05_on':
		Enable object 'g_monitors_small_on_05'.
		Disable object 'g_monitors_small_off_05'.
	sequence 'small_monitor_06_on':
		Enable object 'g_monitors_small_on_06'.
		Disable object 'g_monitors_small_off_06'.
	sequence 'small_monitor_01_off':
		Enable object 'g_monitors_small_off_01'.
		Disable object 'g_monitors_small_on_01'.
	sequence 'small_monitor_02_off':
		Enable object 'g_monitors_small_off_02'.
		Disable object 'g_monitors_small_on_02'.
	sequence 'small_monitor_03_off':
		Enable object 'g_monitors_small_off_03'.
		Disable object 'g_monitors_small_on_03'.
	sequence 'small_monitor_04_off':
		Enable object 'g_monitors_small_off_04'.
		Disable object 'g_monitors_small_on_04'.
	sequence 'small_monitor_05_off':
		Enable object 'g_monitors_small_off_05'.
		Disable object 'g_monitors_small_on_05'.
	sequence 'small_monitor_06_off':
		Enable object 'g_monitors_small_off_06'.
		Disable object 'g_monitors_small_on_06'.
	sequence 'scrolling_play':
		material_config 'units/world/architecture/secret_stash/props/secret_stash_equipment_table_scrolling'.
	sequence 'scrolling_pause':
		material_config 'units/world/architecture/secret_stash/props/secret_stash_equipment_table'.
	sequence 'input_on':
		Enable object 'g_monitors_input'.
	sequence 'input_off':
		EXECUTE ON STARTUP
		Disable object 'g_monitors_input'.
		startup True
	sequence 'break_monitor_01':
		Disable object 'g_monitors_small_off_01'.
		Disable object 'g_monitors_small_on_01'.
		Enable object 'g_monitors_small_dmg_01'.
		monitor_broken_01 = 1
	sequence 'break_monitor_02':
		Disable object 'g_monitors_small_off_02'.
		Disable object 'g_monitors_small_on_02'.
		Enable object 'g_monitors_small_dmg_02'.
		monitor_broken_02 = 1
	sequence 'break_monitor_03':
		Disable object 'g_monitors_small_off_03'.
		Disable object 'g_monitors_small_on_03'.
		Enable object 'g_monitors_small_dmg_03'.
		monitor_broken_03 = 1
	sequence 'break_monitor_04':
		Disable object 'g_monitors_small_off_04'.
		Disable object 'g_monitors_small_on_04'.
		Enable object 'g_monitors_small_dmg_04'.
		monitor_broken_04 = 1
	sequence 'break_monitor_05':
		Disable object 'g_monitors_small_off_05'.
		Disable object 'g_monitors_small_on_05'.
		Enable object 'g_monitors_small_dmg_05'.
		monitor_broken_05 = 1
	sequence 'break_monitor_06':
		Disable object 'g_monitors_small_off_06'.
		Disable object 'g_monitors_small_on_06'.
		Enable object 'g_monitors_small_dmg_06'.
		monitor_broken_06 = 1
	body 'static_monitor_small_01'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'break_monitor_01'.
	body 'static_monitor_small_02'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'break_monitor_02'.
	body 'static_monitor_small_03'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'break_monitor_03'.
	body 'static_monitor_small_04'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'break_monitor_04'.
	body 'static_monitor_small_05'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'break_monitor_05'.
	body 'static_monitor_small_06'
		Upon receiving 1 bullet hit or 10 explosion damage, execute:
			Run sequence 'break_monitor_06'.
