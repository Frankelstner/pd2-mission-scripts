unit:
	sequence 'all_destroy':
		TRIGGER TIMES 1
		Run sequence 'destroyed_pin_1'.
		Run sequence 'destroyed_pin_2'.
		Run sequence 'destroyed_pin_3'.
		Run sequence 'destroyed_pin_4'.
		Run sequence 'destroyed_pin_5'.
		Run sequence 'destroyed_pin_6'.
		Run sequence 'destroyed_pin_7'.
		Run sequence 'hide'.
	sequence 'hide':
		Disable object 'g_secret_stash_stairs_handrail_diagonal'.
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_1'.
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_2'.
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_3'.
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_4'.
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_5'.
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_6'.
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_7'.
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_broken_1'.
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_broken_2'.
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_broken_3'.
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_broken_4'.
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_broken_5'.
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_broken_6'.
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_broken_7'.
		Disable decal_mesh 'c_secret_stash_stairs_handrail_diagonal'.
		Disable decal_mesh 'g_secret_stash_stairs_handrail_diagonal_pin_broken_1'.
		Disable decal_mesh 'g_secret_stash_stairs_handrail_diagonal_pin_broken_2'.
		Disable decal_mesh 'g_secret_stash_stairs_handrail_diagonal_pin_broken_3'.
		Disable decal_mesh 'g_secret_stash_stairs_handrail_diagonal_pin_broken_4'.
		Disable decal_mesh 'g_secret_stash_stairs_handrail_diagonal_pin_broken_5'.
		Disable decal_mesh 'g_secret_stash_stairs_handrail_diagonal_pin_broken_6'.
		Disable decal_mesh 'g_secret_stash_stairs_handrail_diagonal_pin_broken_7'.
		Disable body 'static_body'.
	sequence 'destroyed_pin_1':
		TRIGGER TIMES 1
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_1'.
		Enable object 'g_secret_stash_stairs_handrail_diagonal_pin_broken_1'.
		Disable body 'destroy_pin_1'.
		Enable effect_spawner 'es_e_spawner_1'.
	sequence 'destroyed_pin_2':
		TRIGGER TIMES 1
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_2'.
		Enable object 'g_secret_stash_stairs_handrail_diagonal_pin_broken_2'.
		Disable body 'destroy_pin_2'.
		Enable effect_spawner 'es_e_spawner_2'.
	sequence 'destroyed_pin_3':
		TRIGGER TIMES 1
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_3'.
		Enable object 'g_secret_stash_stairs_handrail_diagonal_pin_broken_3'.
		Disable body 'destroy_pin_3'.
		Enable effect_spawner 'es_e_spawner_3'.
	sequence 'destroyed_pin_4':
		TRIGGER TIMES 1
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_4'.
		Enable object 'g_secret_stash_stairs_handrail_diagonal_pin_broken_4'.
		Disable body 'destroy_pin_4'.
		Enable effect_spawner 'es_e_spawner_4'.
	sequence 'destroyed_pin_5':
		TRIGGER TIMES 1
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_5'.
		Enable object 'g_secret_stash_stairs_handrail_diagonal_pin_broken_5'.
		Disable body 'destroy_pin_5'.
		Enable effect_spawner 'es_e_spawner_5'.
	sequence 'destroyed_pin_6':
		TRIGGER TIMES 1
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_6'.
		Enable object 'g_secret_stash_stairs_handrail_diagonal_pin_broken_6'.
		Disable body 'destroy_pin_6'.
		Enable effect_spawner 'es_e_spawner_6'.
	sequence 'destroyed_pin_7':
		TRIGGER TIMES 1
		Disable object 'g_secret_stash_stairs_handrail_diagonal_pin_7'.
		Enable object 'g_secret_stash_stairs_handrail_diagonal_pin_broken_7'.
		Disable body 'destroy_pin_7'.
		Enable effect_spawner 'es_e_spawner_7'.
	body 'destroy_pin_1'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroyed_pin_1'.
	body 'destroy_pin_2'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroyed_pin_2'.
	body 'destroy_pin_3'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroyed_pin_3'.
	body 'destroy_pin_4'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroyed_pin_4'.
	body 'destroy_pin_5'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroyed_pin_5'.
	body 'destroy_pin_6'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroyed_pin_6'.
	body 'destroy_pin_7'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroyed_pin_7'.
