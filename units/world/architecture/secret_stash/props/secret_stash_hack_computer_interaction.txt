unit:
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'interact':
		Disable interaction.
	sequence 'turn_on_the_computer_screen':
		Enable object 'g_computer_monitor_display'.
		Enable object 'g_star01'.
		Enable object 'g_star02'.
		Enable object 'g_star03'.
		Enable object 'g_star04'.
		Enable object 'g_star05'.
		Enable object 'g_star06'.
		Enable object 'g_star07'.
		Enable object 'g_popup01'.
		Enable object 'g_popup02'.
		Enable object 'g_popup02b'.
		Enable object 'g_popup02c'.
		Enable object 'g_popup03'.
		Enable object 'g_popup03b'.
		Enable object 'g_popup03c'.
		Enable object 'g_popup04'.
		Enable object 'g_popup04b'.
		Enable object 'g_popup04c'.
		Enable object 'g_popup04d'.
		Enable object 'g_popup05'.
		Enable object 'g_popup05b'.
		Call function: timer_gui.set_visible(False)
	sequence 'show':
		Disable interaction.
		Enable body 'static_body'.
		Enable object 'g_computer_monitor'.
		Enable object 'g_glass'.
		Enable decal_mesh 'dm_computer_monitor_plastic'.
		Call function: timer_gui.set_visible(False)
	sequence 'hide':
		Disable interaction.
		Disable body 'static_body'.
		Disable object 'g_computer_monitor'.
		Disable object 'g_computer_monitor_display'.
		Disable decal_mesh 'dm_computer_monitor_plastic'.
		Disable object 'g_glass'.
		Disable object 'g_star01'.
		Disable object 'g_star02'.
		Disable object 'g_star03'.
		Disable object 'g_star04'.
		Disable object 'g_star05'.
		Disable object 'g_star06'.
		Disable object 'g_star07'.
		Disable object 'g_popup01'.
		Disable object 'g_popup02'.
		Disable object 'g_popup02b'.
		Disable object 'g_popup02c'.
		Disable object 'g_popup03'.
		Disable object 'g_popup03b'.
		Disable object 'g_popup03c'.
		Disable object 'g_popup04'.
		Disable object 'g_popup04b'.
		Disable object 'g_popup04c'.
		Disable object 'g_popup04d'.
		Disable object 'g_popup05'.
		Disable object 'g_popup05b'.
		Call function: timer_gui.set_visible(False)
	sequence 'anim_startup':
		Enable animation_group 'ag_startup':
			from 0/30
			speed 1
			to 299/30
		Run sequence 'disable_interaction'.
	sequence 'anim_finish':
		Call function: timer_gui.set_visible(False)
		Enable animation_group 'ag_startup':
			from 300/30
			speed 1
			to 500/30
		Run sequence 'disable_interaction'.
	sequence 'start_1':
		Call function: timer_gui.start(1,90)
		Call function: timer_gui.set_visible(True)
	sequence 'start_2':
		Call function: timer_gui.start(2,90)
		Call function: timer_gui.set_visible(True)
	sequence 'start_3':
		Call function: timer_gui.start(3,100)
		Call function: timer_gui.set_visible(True)
	sequence 'start_1_60s':
		Call function: timer_gui.start(1,60)
		Call function: timer_gui.set_visible(True)
	sequence 'start_2_60s':
		Call function: timer_gui.start(2,60)
		Call function: timer_gui.set_visible(True)
	sequence 'start_3_60s':
		Call function: timer_gui.start(3,60)
		Call function: timer_gui.set_visible(True)
	sequence '5sec_1'.
	sequence '5sec_2'.
	sequence '5sec_3'.
	sequence 'done_1'.
	sequence 'done_2'.
	sequence 'done_3'.
	sequence 'power_off':
		Call function: timer_gui.set_powered(False)
		Disable object 'g_computer_monitor_display'.
		Disable object 'g_star01'.
		Disable object 'g_star02'.
		Disable object 'g_star03'.
		Disable object 'g_star04'.
		Disable object 'g_star05'.
		Disable object 'g_star06'.
		Disable object 'g_star07'.
		Disable object 'g_popup01'.
		Disable object 'g_popup02'.
		Disable object 'g_popup02b'.
		Disable object 'g_popup02c'.
		Disable object 'g_popup03'.
		Disable object 'g_popup03b'.
		Disable object 'g_popup03c'.
		Disable object 'g_popup04'.
		Disable object 'g_popup04b'.
		Disable object 'g_popup04c'.
		Disable object 'g_popup04d'.
		Disable object 'g_popup05'.
		Disable object 'g_popup05b'.
	sequence 'power_on':
		Enable object 'g_computer_monitor_display'.
		Enable object 'g_star01'.
		Enable object 'g_star02'.
		Enable object 'g_star03'.
		Enable object 'g_star04'.
		Enable object 'g_star05'.
		Enable object 'g_star06'.
		Enable object 'g_star07'.
		Enable object 'g_popup01'.
		Enable object 'g_popup02'.
		Enable object 'g_popup02b'.
		Enable object 'g_popup02c'.
		Enable object 'g_popup03'.
		Enable object 'g_popup03b'.
		Enable object 'g_popup03c'.
		Enable object 'g_popup04'.
		Enable object 'g_popup04b'.
		Enable object 'g_popup04c'.
		Enable object 'g_popup04d'.
		Enable object 'g_popup05'.
		Enable object 'g_popup05b'.
		Call function: timer_gui.set_powered(True)
