unit:
	sequence 'seq_var1':
		Enable body 'body_var1'.
		Disable body 'body_solid'.
		Disable body 'static_body'.
		Enable object 'g_secret_stash_hallway_to_stairs_240_var1'.
		Disable object 'g_secret_stash_hallway_to_stairs_240_solid'.
		Enable decal_mesh 'g_secret_stash_hallway_to_stairs_240_var1'.
		Disable decal_mesh 'g_secret_stash_hallway_to_stairs_240_solid'.
	sequence 'seq_solid':
		Disable body 'body_var1'.
		Enable body 'body_solid'.
		Disable body 'static_body'.
		Disable object 'g_secret_stash_hallway_to_stairs_240_var1'.
		Enable object 'g_secret_stash_hallway_to_stairs_240_solid'.
		Disable decal_mesh 'g_secret_stash_hallway_to_stairs_240_var1'.
		Enable decal_mesh 'g_secret_stash_hallway_to_stairs_240_solid'.
	body 'body_solid'
		Upon receiving 50 explosion damage, execute:
			Run sequence 'seq_var1'.
			Run sequence 'shot_with_m79'.
	sequence 'shot_with_m79':
		effect 'effects/particles/explosions/explosion_grenade_wall_destruction':
			parent object('e_explosion_1')
			position v()
		spawn_unit 'units/world/architecture/secret_stash/props/wall_destruction':
			position object_pos('e_explosion_1')
			rotation object_rot('e_explosion_1')
