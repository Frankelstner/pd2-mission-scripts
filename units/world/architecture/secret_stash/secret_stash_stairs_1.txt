unit:
	sequence 'toggle_box_on':
		Enable body 'limoblocker'.
	sequence 'toggle_box_off':
		Disable body 'limoblocker'.
