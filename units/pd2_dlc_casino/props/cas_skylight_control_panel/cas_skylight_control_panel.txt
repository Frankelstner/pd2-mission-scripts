unit:
	sequence 'state_1':
		Disable object 'g_off'.
		Enable object 'g_text_01'.
		Disable object 'g_text_02'.
		Disable object 'g_text_03'.
		Disable object 'g_text_04'.
		Disable interaction.
	sequence 'state_2':
		Disable object 'g_off'.
		Disable object 'g_text_01'.
		Enable object 'g_text_02'.
		Disable object 'g_text_03'.
		Disable object 'g_text_04'.
	sequence 'state_3':
		Disable object 'g_off'.
		Disable object 'g_text_01'.
		Disable object 'g_text_02'.
		Enable object 'g_text_03'.
		Disable object 'g_text_04'.
	sequence 'state_4':
		Disable object 'g_off'.
		Disable object 'g_text_01'.
		Disable object 'g_text_02'.
		Disable object 'g_text_03'.
		Enable object 'g_text_04'.
		Enable interaction.
	sequence 'off':
		Enable object 'g_off'.
		Disable object 'g_text_01'.
		Disable object 'g_text_02'.
		Disable object 'g_text_03'.
		Disable object 'g_text_04'.
	sequence 'interact':
		Play audio 'card_reader_first_card' at 'rp_cas_skylight_control_panel'.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
