unit:
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'hide':
		Disable interaction.
		Disable object 'g_g'.
	sequence 'show':
		Enable object 'g_g'.
	sequence 'interact':
		Run sequence 'hide'.
