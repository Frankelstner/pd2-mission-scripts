unit:
	body 'body_glass'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_glass_break'.
	sequence 'int_seq_glass_break':
		TRIGGER TIMES 1
		Disable body 'body_glass'.
		Disable object 'g_glass'.
		effect 'effects/payday2/particles/window/jewelry_counter_1':
			parent object( 'e_glass' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass'.
