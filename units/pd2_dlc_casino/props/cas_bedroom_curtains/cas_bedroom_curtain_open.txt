unit:
	sequence 'state_open':
		Hide graphic_group 'grp_closed'.
		Show graphic_group 'grp_open'.
		Enable body 'body_open'.
		Disable body 'body_closed'.
	sequence 'state_closed':
		Show graphic_group 'grp_closed'.
		Hide graphic_group 'grp_open'.
		Disable body 'body_open'.
		Enable body 'body_closed'.
