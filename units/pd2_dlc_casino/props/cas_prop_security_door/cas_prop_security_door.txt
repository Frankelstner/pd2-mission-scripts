unit:
	body 'body_lock'
		Upon receiving 37.5 saw damage, execute:
			Run sequence 'int_seq_saw_door'.
		Upon receiving 3 bullet hits, execute:
			Run sequence 'int_seq_shoot_door'.
	sequence 'explode_door':
		Run sequence 'int_seq_explode_door'.
	sequence 'int_seq_explode_door':
		Hide graphic_group 'grp_door'.
		Disable body 'body_anim_door'.
		Play audio 'c4_explode_wood' at 'e_pos_shards'.
		spawn_unit 'units/pd2_dlc_casino/props/cas_prop_security_door/spawn_door_shards':
			position object_pos('e_pos_shards')
			rotation object_rot('e_pos_shards')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('e_dest') ),130,7).
		Run sequence 'done_exploded'.
		Run sequence 'deactivate_door'.
	sequence 'int_seq_shoot_door':
		Hide graphic_group 'grp_door'.
		Show graphic_group 'grp_door_shot'.
		Run sequence 'seq_open_door'.
	sequence 'int_seq_saw_door':
		Hide graphic_group 'grp_door'.
		Show graphic_group 'grp_door_saw'.
		Run sequence 'seq_open_door'.
	sequence 'done_exploded'.
	sequence 'done_open'.
	sequence 'interact':
		Run sequence 'seq_open_door'.
	sequence 'seq_open_door':
		Enable animation_group 'grp_anim_door':
			from 0/30
			to 30/30
		Run sequence 'done_open'.
		Run sequence 'deactivate_door'.
	sequence 'seq_close_door':
		Enable animation_group 'grp_anim_door':
			from 30/30
			speed -1
			to 0/30
	sequence 'activate_door':
		Enable body 'body_lock'.
		Show graphic_group 'grp_icon'.
		Call function: base.activate()
		Enable interaction.
	sequence 'deactivate_door':
		Disable body 'body_lock'.
		Hide graphic_group 'grp_icon'.
		Call function: base.deactivate()
		Disable interaction.
	sequence 'door_opened'.
	sequence 'door_closed'.
	sequence 'c4_placed'.
	sequence 'c4_completed'.
	sequence 'all_c4_placed'.
