unit:
	color = red
	number = 0
	sequence 'set_color_red':
		color = 0
	sequence 'set_color_green':
		color = 1
	sequence 'set_color_blue':
		color = 2
	sequence 'set_number_00':
		number = 0
	sequence 'set_number_01':
		number = 1
	sequence 'set_number_02':
		number = 2
	sequence 'set_number_03':
		number = 3
	sequence 'set_number_04':
		number = 4
	sequence 'set_number_05':
		number = 5
	sequence 'set_number_06':
		number = 6
	sequence 'set_number_07':
		number = 7
	sequence 'set_number_08':
		number = 8
	sequence 'set_number_09':
		number = 9
	sequence 'check_red_number':
		If number == 00: Run sequence 'set_red_00'.
		If number == 01: Run sequence 'set_red_01'.
		If number == 02: Run sequence 'set_red_02'.
		If number == 03: Run sequence 'set_red_03'.
		If number == 04: Run sequence 'set_red_04'.
		If number == 05: Run sequence 'set_red_05'.
		If number == 06: Run sequence 'set_red_06'.
		If number == 07: Run sequence 'set_red_07'.
		If number == 08: Run sequence 'set_red_08'.
		If number == 09: Run sequence 'set_red_09'.
	sequence 'check_green_number':
		If number == 00: Run sequence 'set_green_00'.
		If number == 01: Run sequence 'set_green_01'.
		If number == 02: Run sequence 'set_green_02'.
		If number == 03: Run sequence 'set_green_03'.
		If number == 04: Run sequence 'set_green_04'.
		If number == 05: Run sequence 'set_green_05'.
		If number == 06: Run sequence 'set_green_06'.
		If number == 07: Run sequence 'set_green_07'.
		If number == 08: Run sequence 'set_green_08'.
		If number == 09: Run sequence 'set_green_09'.
	sequence 'check_blue_number':
		If number == 00: Run sequence 'set_blue_00'.
		If number == 01: Run sequence 'set_blue_01'.
		If number == 02: Run sequence 'set_blue_02'.
		If number == 03: Run sequence 'set_blue_03'.
		If number == 04: Run sequence 'set_blue_04'.
		If number == 05: Run sequence 'set_blue_05'.
		If number == 06: Run sequence 'set_blue_06'.
		If number == 07: Run sequence 'set_blue_07'.
		If number == 08: Run sequence 'set_blue_08'.
		If number == 09: Run sequence 'set_blue_09'.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'interact':
		If color == 0: Run sequence 'check_red_number'.
		If color == 1: Run sequence 'check_green_number'.
		If color == 2: Run sequence 'check_blue_number'.
		Disable object 'g_top_closed'.
		Enable object 'g_top_opened'.
	sequence 'set_red_00':
		Enable object 'g_number_red_00'.
	sequence 'set_red_01':
		Enable object 'g_number_red_01'.
	sequence 'set_red_02':
		Enable object 'g_number_red_02'.
	sequence 'set_red_03':
		Enable object 'g_number_red_03'.
	sequence 'set_red_04':
		Enable object 'g_number_red_04'.
	sequence 'set_red_05':
		Enable object 'g_number_red_05'.
	sequence 'set_red_06':
		Enable object 'g_number_red_06'.
	sequence 'set_red_07':
		Enable object 'g_number_red_07'.
	sequence 'set_red_08':
		Enable object 'g_number_red_08'.
	sequence 'set_red_09':
		Enable object 'g_number_red_09'.
	sequence 'set_green_00':
		Enable object 'g_number_green_00'.
	sequence 'set_green_01':
		Enable object 'g_number_green_01'.
	sequence 'set_green_02':
		Enable object 'g_number_green_02'.
	sequence 'set_green_03':
		Enable object 'g_number_green_03'.
	sequence 'set_green_04':
		Enable object 'g_number_green_04'.
	sequence 'set_green_05':
		Enable object 'g_number_green_05'.
	sequence 'set_green_06':
		Enable object 'g_number_green_06'.
	sequence 'set_green_07':
		Enable object 'g_number_green_07'.
	sequence 'set_green_08':
		Enable object 'g_number_green_08'.
	sequence 'set_green_09':
		Enable object 'g_number_green_09'.
	sequence 'set_blue_00':
		Enable object 'g_number_blue_00'.
	sequence 'set_blue_01':
		Enable object 'g_number_blue_01'.
	sequence 'set_blue_02':
		Enable object 'g_number_blue_02'.
	sequence 'set_blue_03':
		Enable object 'g_number_blue_03'.
	sequence 'set_blue_04':
		Enable object 'g_number_blue_04'.
	sequence 'set_blue_05':
		Enable object 'g_number_blue_05'.
	sequence 'set_blue_06':
		Enable object 'g_number_blue_06'.
	sequence 'set_blue_07':
		Enable object 'g_number_blue_07'.
	sequence 'set_blue_08':
		Enable object 'g_number_blue_08'.
	sequence 'set_blue_09':
		Enable object 'g_number_blue_09'.
