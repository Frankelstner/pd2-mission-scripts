unit:
	drain_speed = 1
	e3 = 0
	sequence 'show':
		Enable object 'g_watertank_holder_02'.
		Enable body 'body_a_main'.
	sequence 'hide':
		Disable object 'g_watertank_holder_02'.
		Disable object 'g_water_level_02'.
		Disable body 'body_a_main'.
		Disable body 'body_a_water_level'.
	sequence 'lowered_state':
		Enable animation_group 'anim':
			from -3000/30
			to -3000/30
	sequence 'hanging_state':
		Enable animation_group 'anim':
			from 0/30
			to 0/30
	sequence 'start_state':
		Enable animation_group 'anim':
			from -2985/30
			to -2985/30
	sequence 'PP_water_level_meter':
		Enable object 'g_water_level_02'.
		Disable body 'body_a_water_level'.
	sequence 'done_lower'.
	sequence 'done_transition'.
	sequence 'done_fly_in'.
	sequence 'slower_drain':
		drain_speed = 2
	sequence 'set_e3':
		e3 = 1
	sequence 'anim_transition':
		If e3 == 0: 
			Enable animation_group 'anim':
				from 0/30
				speed 1
				to 1800/30
		If e3 == 1: 
			Enable animation_group 'anim':
				from 0/30
				speed 2
				to 1800/30
		If e3 == 0: Run sequence 'done_transition'. (DELAY 1800/30)
		If e3 == 1: Run sequence 'done_transition'. (DELAY 900/30)
	sequence 'pause_animation':
		Enable animation_group 'anim':
			speed 0
	sequence 'resume_animation':
		If e3 == 0: 
			Enable animation_group 'anim':
				speed 1
				to 1800/30
		If e3 == 1: 
			Enable animation_group 'anim':
				speed 2
				to 1800/30
	sequence 'anim_lower':
		Enable animation_group 'anim':
			from -1000/30
			to 0/30
		Run sequence 'done_lower'. (DELAY 1000/30)
	sequence 'fly_in':
		If e3 == 0: 
			Enable animation_group 'anim':
				from -2985/30
				speed 1
				to -1033/30
		If e3 == 1: 
			Enable animation_group 'anim':
				from -2985/30
				speed 2
				to -1033/30
		If e3 == 0: Run sequence 'done_fly_in'. (DELAY 1952/30)
		If e3 == 1: Run sequence 'done_fly_in'. (DELAY 976/30)
	sequence 'water_level_meter_start':
		If drain_speed == 1: 
			Enable animation_group 'water_level_meter':
				from 0/30
				speed 1
				to 1800/30
		If drain_speed == 2: 
			Enable animation_group 'water_level_meter':
				from 0/30
				speed 0.5
				to 1800/30
	sequence 'water_level_meter_pause':
		Enable animation_group 'water_level_meter':
			speed 0
	sequence 'water_level_meter_resume':
		If drain_speed == 1: 
			Enable animation_group 'water_level_meter':
				speed 1
		If drain_speed == 2: 
			Enable animation_group 'water_level_meter':
				speed 0.5
	sequence 'water_level_meter_reset':
		Enable animation_group 'water_level_meter':
			from 0/30
			to 0/30
	sequence 'drill_rise':
		Enable animation_group 'anim':
			from 2000/30
			speed 0.25
			to 2200/30
