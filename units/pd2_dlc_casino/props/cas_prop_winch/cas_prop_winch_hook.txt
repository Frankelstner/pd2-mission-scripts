unit:
	sequence 'show':
		Enable object 'g_hook'.
		Enable decal_mesh 'dm_metal_hook'.
	sequence 'hide':
		Disable object 'g_hook'.
		Disable decal_mesh 'dm_metal_hook'.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'interact'.
	sequence 'load':
		Disable interaction.
		Run sequence 'hide'.
