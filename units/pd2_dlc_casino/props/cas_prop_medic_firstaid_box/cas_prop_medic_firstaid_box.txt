unit:
	sequence 'open_door':
		Enable interaction.
		Enable animation_group 'anim_door_open':
			from 0/30
			to 30/30
	sequence 'state_1':
		Disable decal_mesh 'dm_plastic_door_stage0'.
		Disable decal_mesh 'dm_plastic_door_stage1'.
		Disable decal_mesh 'dm_plastic_door_stage2'.
		Enable decal_mesh 'dm_plastic_door_stage3'.
		Disable decal_mesh 'dm_plastic_stage0'.
		Disable decal_mesh 'dm_plastic_stage1'.
		Disable decal_mesh 'dm_plastic_stage2'.
		Enable decal_mesh 'dm_plastic_stage3'.
		Disable object 'g_firstaid_stage0'.
		Disable object 'g_firstaid_stage1'.
		Disable object 'g_firstaid_stage2'.
		Enable object 'g_firstaid_stage3'.
		Disable interaction.
	sequence 'state_2':
		Disable decal_mesh 'dm_plastic_door_stage0'.
		Disable decal_mesh 'dm_plastic_door_stage1'.
		Enable decal_mesh 'dm_plastic_door_stage2'.
		Disable decal_mesh 'dm_plastic_door_stage2'.
		Disable decal_mesh 'dm_plastic_stage0'.
		Disable decal_mesh 'dm_plastic_stage1'.
		Enable decal_mesh 'dm_plastic_stage2'.
		Disable decal_mesh 'dm_plastic_stage2'.
		Disable object 'g_firstaid_stage0'.
		Disable object 'g_firstaid_stage1'.
		Enable object 'g_firstaid_stage2'.
	sequence 'state_3':
		Disable decal_mesh 'dm_plastic_door_stage0'.
		Enable decal_mesh 'dm_plastic_door_stage1'.
		Disable decal_mesh 'dm_plastic_door_stage2'.
		Disable decal_mesh 'dm_plastic_door_stage3'.
		Disable decal_mesh 'dm_plastic_stage0'.
		Enable decal_mesh 'dm_plastic_stage1'.
		Disable decal_mesh 'dm_plastic_stage2'.
		Disable decal_mesh 'dm_plastic_stage3'.
		Disable object 'g_firstaid_stage0'.
		Enable object 'g_firstaid_stage1'.
		Disable object 'g_firstaid_stage2'.
	sequence 'show':
		Show graphic_group 'grp_firstaid'.
		Enable decal_mesh 'dm_plastic_door_stage0'.
		Enable decal_mesh 'dm_plastic_stage0'.
		Enable body 'body_static'.
		Enable body 'body_door'.
	sequence 'hide':
		Disable interaction.
		Hide graphic_group 'grp_firstaid'.
		Disable decal_mesh 'dm_plastic_door_stage0'.
		Disable decal_mesh 'dm_plastic_stage0'.
		Disable body 'body_static'.
		Disable body 'body_door'.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
