unit:
	sequence 'show':
		Enable object 'g_wrench'.
	sequence 'hide':
		Disable object 'g_wrench'.
		Disable interaction.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'interact'.
	sequence 'load':
		Play audio 'pick_up_crowbar' at 'snd'.
