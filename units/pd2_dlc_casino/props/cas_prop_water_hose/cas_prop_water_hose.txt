unit:
	sequence 'small_water_hose':
		Enable object 'g_water_hose_small'.
		Disable object 'g_water_hose_big'.
	sequence 'vis_small_water_hose':
		Run sequence 'small_water_hose'.
	sequence 'big_water_hose':
		Disable object 'g_water_hose_small'.
		Enable object 'g_water_hose_big'.
	sequence 'vis_big_water_hose':
		Run sequence 'big_water_hose'.
	sequence 'hidden_water_hose':
		Disable object 'g_water_hose_small'.
		Disable object 'g_water_hose_big'.
	sequence 'hidden':
		Run sequence 'hidden_water_hose'.
