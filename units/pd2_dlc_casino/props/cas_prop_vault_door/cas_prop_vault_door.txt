unit:
	sequence 'state_door_open':
		Enable animation_group 'anim':
			from 0
			speed 0
			to 0
	sequence 'state_door_close':
		Enable animation_group 'anim':
			from 125/30
			speed 0
			to 125/30
	sequence 'anim_door_close':
		Enable animation_group 'anim':
			from 0
			to 125/30
	sequence 'anim_door_open':
		Enable animation_group 'anim':
			from 125/30
			speed -1
			to 0
	sequence 'state_vis_hide':
		Disable body 'body_frame'.
		Disable body 'body_door_01'.
		Disable body 'body_door_02'.
		Disable body 'body_door_03'.
		Disable body 'body_floor_01'.
		Disable body 'body_floor_02'.
		Disable decal_mesh 'dm_glass_unbreakable'.
		Disable decal_mesh 'dm_metal'.
		Disable decal_mesh 'dm_door_01_metal'.
		Disable decal_mesh 'dm_door_02_metal'.
		Disable decal_mesh 'dm_door_03_metal'.
		Disable decal_mesh 'dm_floor_01_metal'.
		Disable decal_mesh 'dm_floor_02_metal'.
		Hide graphic_group 'grp_door'.
	sequence 'state_vis_show':
		Enable body 'body_frame'.
		Enable body 'body_door_01'.
		Enable body 'body_door_02'.
		Enable body 'body_door_03'.
		Enable body 'body_floor_01'.
		Enable body 'body_floor_02'.
		Enable decal_mesh 'dm_glass_unbreakable'.
		Enable decal_mesh 'dm_metal'.
		Enable decal_mesh 'dm_door_01_metal'.
		Enable decal_mesh 'dm_door_02_metal'.
		Enable decal_mesh 'dm_door_03_metal'.
		Enable decal_mesh 'dm_floor_01_metal'.
		Enable decal_mesh 'dm_floor_02_metal'.
		Show graphic_group 'grp_door'.
