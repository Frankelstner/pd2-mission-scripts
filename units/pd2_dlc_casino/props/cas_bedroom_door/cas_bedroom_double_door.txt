unit:
	body 'body_lock'
		Upon receiving 37.5 saw damage, execute:
			Run sequence 'open_door'.
		Upon receiving 4 bullet hits, execute:
			Run sequence 'open_door'.
	sequence 'hard_skill_lock':
		Call function: interaction.set_tweak_data('pick_lock_hard')
		Enable body 'body_lock'.
		Show graphic_group 'grp_icon_skill'.
		Hide graphic_group 'grp_icon_no_skill'.
	sequence 'hard_no_skill_lock':
		Call function: interaction.set_tweak_data('pick_lock_hard_no_skill')
		Enable body 'body_lock'.
		Hide graphic_group 'grp_icon_skill'.
		Show graphic_group 'grp_icon_no_skill'.
	sequence 'explode_door':
		Run sequence 'seq_explode_in'.
	sequence 'seq_explode_in':
		Play audio 'c4_explode_wood' at 'a_shp_charge'.
		Run sequence 'dest_seq'.
		Run sequence 'done_exploded'.
		Run sequence 'done_open'.
		Run sequence 'deactivate_door'.
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'a_shp_charge' )
			position v()
	sequence 'dest_seq':
		Show graphic_group 'grp_door_dmg'.
		Hide graphic_group 'grp_door'.
		Disable body 'body_anim_door_left'.
		Disable body 'body_anim_door_right'.
		Disable decal_mesh 'dm_wood_01'.
		Disable decal_mesh 'dm_wood_02'.
		spawn_unit 'units/pd2_dlc_casino/props/cas_bedroom_door/double_door_debris':
			position object_pos('e_pos')
			rotation object_rot('e_pos')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('a_shp_charge') ),80,50).
	sequence 'done_exploded'.
	sequence 'done_open'.
	sequence 'close_door':
		Enable animation_group 'grp_anim_doors':
			from 30/30
			speed -1
			to 0/30
		Run sequence 'activate_door'.
	sequence 'open_door':
		Enable animation_group 'grp_anim_doors':
			from 0/30
			to 30/30
		Run sequence 'deactivate_door'.
		Run sequence 'done_open'.
	sequence 'interact':
		Run sequence 'open_door'.
	sequence 'activate_door':
		Enable body 'body_lock'.
		Show graphic_group 'grp_icon_skill'.
		Hide graphic_group 'grp_icon_no_skill'.
		Call function: base.activate()
		Enable interaction.
	sequence 'deactivate_door':
		Disable body 'body_lock'.
		Hide graphic_group 'grp_icon_skill'.
		Hide graphic_group 'grp_icon_no_skill'.
		Call function: base.deactivate()
		Disable interaction.
	sequence 'door_opened'.
	sequence 'door_closed'.
	sequence 'c4_placed'.
	sequence 'c4_completed'.
	sequence 'all_c4_placed'.
	sequence 'drill_placed'.
	sequence 'drill_jammed'.
	sequence 'drill_resumed'.
	sequence 'drill_power_off'.
	sequence 'drill_power_on'.
	sequence 'drill_completed'.
	sequence 'all_drill_placed'.
