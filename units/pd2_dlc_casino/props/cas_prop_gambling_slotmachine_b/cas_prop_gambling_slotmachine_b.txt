unit:
	win = 0
	win_all = 0
	destroyed = 0
	sequence 'state_interaction_enable':
		If destroyed == 0: Enable interaction.
	sequence 'state_interaction_disable':
		Disable interaction.
	sequence 'state_blinking':
		Call function: blink.set_state('blink','0.2')
	sequence 'state_not_blinking':
		Call function: blink.set_state('enable','0')
	sequence 'state_blinking_off':
		Call function: blink.set_state('disable','0')
	sequence 'lose_pick_one_randomly_after_interact':
		win = 0
		win_all = 0
	sequence 'win01_pick_one_randomly_after_interact':
		win = 1
		win_all = 1
	sequence 'win02_pick_one_randomly_after_interact':
		win = 2
		win_all = 1
	sequence 'win03_pick_one_randomly_after_interact':
		win = 3
		win_all = 1
	sequence 'win04_pick_one_randomly_after_interact':
		win = 4
		win_all = 1
	sequence 'win05_pick_one_randomly_after_interact':
		win = 5
		win_all = 1
	sequence 'win06_pick_one_randomly_after_interact':
		win = 6
		win_all = 1
	sequence 'win07_pick_one_randomly_after_interact':
		win = 7
		win_all = 1
	sequence 'win08_pick_one_randomly_after_interact':
		win = 8
		win_all = 1
	sequence 'win09_pick_one_randomly_after_interact':
		win = 9
		win_all = 1
	sequence 'win10_pick_one_randomly_after_interact':
		win = 10
		win_all = 1
	sequence 'win11_pick_one_randomly_after_interact':
		win = 11
		win_all = 1
	sequence 'win12_pick_one_randomly_after_interact':
		win = 12
		win_all = 1
	sequence 'state_starting_slot_position_01':
		Enable animation_group 'ag_align_a':
			from 10/30
			speed 0
			to 10/30
		Enable animation_group 'ag_align_b':
			from 20/30
			speed 0
			to 20/30
		Enable animation_group 'ag_align_c':
			from 90/30
			speed 0
			to 90/30
	sequence 'state_starting_slot_position_02':
		Enable animation_group 'ag_align_a':
			from 20/30
			speed 0
			to 20/30
		Enable animation_group 'ag_align_b':
			from 60/30
			speed 0
			to 60/30
		Enable animation_group 'ag_align_c':
			from 100/30
			speed 0
			to 100/30
	sequence 'state_starting_slot_position_03':
		Enable animation_group 'ag_align_a':
			from 10/30
			speed 0
			to 10/30
		Enable animation_group 'ag_align_b':
			from 50/30
			speed 0
			to 50/30
		Enable animation_group 'ag_align_c':
			from 120/30
			speed 0
			to 120/30
	sequence 'state_starting_slot_position_04':
		Enable animation_group 'ag_align_a':
			from 30/30
			speed 0
			to 30/30
		Enable animation_group 'ag_align_b':
			from 90/30
			speed 0
			to 90/30
		Enable animation_group 'ag_align_c':
			from 120/30
			speed 0
			to 120/30
	sequence 'state_starting_slot_position_05':
		Enable animation_group 'ag_align_a':
			from 50/30
			speed 0
			to 50/30
		Enable animation_group 'ag_align_b':
			from 90/30
			speed 0
			to 90/30
		Enable animation_group 'ag_align_c':
			from 110/30
			speed 0
			to 110/30
	sequence 'state_starting_slot_position_06':
		Enable animation_group 'ag_align_a':
			from 20/30
			speed 0
			to 20/30
		Enable animation_group 'ag_align_b':
			from 30/30
			speed 0
			to 30/30
		Enable animation_group 'ag_align_c':
			from 40/30
			speed 0
			to 40/30
	sequence 'state_starting_slot_position_07':
		Enable animation_group 'ag_align_a':
			from 30/30
			speed 0
			to 30/30
		Enable animation_group 'ag_align_b':
			from 110/30
			speed 0
			to 110/30
		Enable animation_group 'ag_align_c':
			from 120/30
			speed 0
			to 120/30
	sequence 'state_starting_slot_position_08':
		Enable animation_group 'ag_align_a':
			from 60/30
			speed 0
			to 60/30
		Enable animation_group 'ag_align_b':
			from 70/30
			speed 0
			to 70/30
		Enable animation_group 'ag_align_c':
			from 120/30
			speed 0
			to 120/30
	sequence 'state_starting_slot_position_09':
		Enable animation_group 'ag_align_a':
			from 10/30
			speed 0
			to 10/30
		Enable animation_group 'ag_align_b':
			from 30/30
			speed 0
			to 30/30
		Enable animation_group 'ag_align_c':
			from 80/30
			speed 0
			to 80/30
	sequence 'state_starting_slot_position_10':
		Enable animation_group 'ag_align_a':
			from 10/30
			speed 0
			to 10/30
		Enable animation_group 'ag_align_b':
			from 80/30
			speed 0
			to 80/30
		Enable animation_group 'ag_align_c':
			from 100/30
			speed 0
			to 100/30
	sequence 'state_starting_slot_position_11':
		Enable animation_group 'ag_align_a':
			from 40/30
			speed 0
			to 40/30
		Enable animation_group 'ag_align_b':
			from 80/30
			speed 0
			to 80/30
		Enable animation_group 'ag_align_c':
			from 90/30
			speed 0
			to 90/30
	sequence 'state_starting_slot_position_12':
		Enable animation_group 'ag_align_a':
			from 50/30
			speed 0
			to 50/30
		Enable animation_group 'ag_align_b':
			from 100/30
			speed 0
			to 100/30
		Enable animation_group 'ag_align_c':
			from 110/30
			speed 0
			to 110/30
	sequence 'state_starting_slot_position_13':
		Enable animation_group 'ag_align_a':
			from 90/30
			speed 0
			to 90/30
		Enable animation_group 'ag_align_b':
			from 110/30
			speed 0
			to 110/30
		Enable animation_group 'ag_align_c':
			from 120/30
			speed 0
			to 120/30
	sequence 'state_starting_slot_position_14':
		Enable animation_group 'ag_align_a':
			from 30/30
			speed 0
			to 30/30
		Enable animation_group 'ag_align_b':
			from 40/30
			speed 0
			to 40/30
		Enable animation_group 'ag_align_c':
			from 70/30
			speed 0
			to 70/30
	sequence 'state_starting_slot_position_15':
		Enable animation_group 'ag_align_a':
			from 20/30
			speed 0
			to 20/30
		Enable animation_group 'ag_align_b':
			from 60/30
			speed 0
			to 60/30
		Enable animation_group 'ag_align_c':
			from 80/30
			speed 0
			to 80/30
	sequence 'state_starting_slot_position_16':
		Enable animation_group 'ag_align_a':
			from 50/30
			speed 0
			to 50/30
		Enable animation_group 'ag_align_b':
			from 70/30
			speed 0
			to 70/30
		Enable animation_group 'ag_align_c':
			from 110/30
			speed 0
			to 110/30
	sequence 'state_starting_slot_position_17':
		Enable animation_group 'ag_align_a':
			from 20/30
			speed 0
			to 20/30
		Enable animation_group 'ag_align_b':
			from 30/30
			speed 0
			to 30/30
		Enable animation_group 'ag_align_c':
			from 80/30
			speed 0
			to 80/30
	sequence 'state_starting_slot_position_18':
		Enable animation_group 'ag_align_a':
			from 50/30
			speed 0
			to 50/30
		Enable animation_group 'ag_align_b':
			from 60/30
			speed 0
			to 60/30
		Enable animation_group 'ag_align_c':
			from 70/30
			speed 0
			to 70/30
	sequence 'state_starting_slot_position_19':
		Enable animation_group 'ag_align_a':
			from 10/30
			speed 0
			to 10/30
		Enable animation_group 'ag_align_b':
			from 70/30
			speed 0
			to 70/30
		Enable animation_group 'ag_align_c':
			from 100/30
			speed 0
			to 100/30
	sequence 'state_starting_slot_position_20':
		Enable animation_group 'ag_align_a':
			from 10/30
			speed 0
			to 10/30
		Enable animation_group 'ag_align_b':
			from 110/30
			speed 0
			to 110/30
		Enable animation_group 'ag_align_c':
			from 120/30
			speed 0
			to 120/30
	body 'body_hitbox'
		Upon receiving 1 bullet hit or 10 explosion damage or 50 melee damage, execute:
			Run sequence 'int_seq_break'.
	sequence 'int_seq_break':
		Disable interaction.
		Disable body 'body_hitbox'.
		Hide graphic_group 'grp_machine_whole'.
		Show graphic_group 'grp_machine_broken'.
		Disable animation_group 'ag_slotbars'.
		Disable animation_group 'ag_align_a'.
		Disable animation_group 'ag_align_b'.
		Disable animation_group 'ag_align_c'.
		Disable animation_group 'ag_lose'.
		effect 'effects/particles/dest/secret_stash_light_fixture_dest':
			parent object( 'e_sparks' )
			position v()
		effect 'effects/particles/dest/security_camera_dest':
			parent  'e_sparks' 
		Play audio 'emitter_security_camera_explode' at 'e_sparks'.
		Run sequence 'state_blinking_off'.
		destroyed = 1
		Stop audio 'slot_machine_rolling_loop' at 'snd_5'.
	sequence 'start_slots':
		Enable animation_group 'ag_slotbars':
			from 0
		If win == 0: 
			Enable animation_group 'ag_lose':
				from 0
		If win == 1: 
			Enable animation_group 'ag_align_a':
				to 0
		If win == 1: 
			Enable animation_group 'ag_align_b':
				to 0
		If win == 1: 
			Enable animation_group 'ag_align_c':
				to 0
		If win == 2: 
			Enable animation_group 'ag_align_a':
				to 10/30
		If win == 2: 
			Enable animation_group 'ag_align_b':
				to 10/30
		If win == 2: 
			Enable animation_group 'ag_align_c':
				to 10/30
		If win == 3: 
			Enable animation_group 'ag_align_a':
				to 20/30
		If win == 3: 
			Enable animation_group 'ag_align_b':
				to 20/30
		If win == 3: 
			Enable animation_group 'ag_align_c':
				to 20/30
		If win == 4: 
			Enable animation_group 'ag_align_a':
				to 30/30
		If win == 4: 
			Enable animation_group 'ag_align_b':
				to 30/30
		If win == 4: 
			Enable animation_group 'ag_align_c':
				to 30/30
		If win == 5: 
			Enable animation_group 'ag_align_a':
				to 40/30
		If win == 5: 
			Enable animation_group 'ag_align_b':
				to 40/30
		If win == 5: 
			Enable animation_group 'ag_align_c':
				to 40/30
		If win == 6: 
			Enable animation_group 'ag_align_a':
				to 50/30
		If win == 6: 
			Enable animation_group 'ag_align_b':
				to 50/30
		If win == 6: 
			Enable animation_group 'ag_align_c':
				to 50/30
		If win == 7: 
			Enable animation_group 'ag_align_a':
				to 60/30
		If win == 7: 
			Enable animation_group 'ag_align_b':
				to 60/30
		If win == 7: 
			Enable animation_group 'ag_align_c':
				to 60/30
		If win == 8: 
			Enable animation_group 'ag_align_a':
				to 70/30
		If win == 8: 
			Enable animation_group 'ag_align_b':
				to 70/30
		If win == 8: 
			Enable animation_group 'ag_align_c':
				to 70/30
		If win == 9: 
			Enable animation_group 'ag_align_a':
				to 80/30
		If win == 9: 
			Enable animation_group 'ag_align_b':
				to 80/30
		If win == 9: 
			Enable animation_group 'ag_align_c':
				to 80/30
		If win == 10: 
			Enable animation_group 'ag_align_a':
				to 90/30
		If win == 10: 
			Enable animation_group 'ag_align_b':
				to 90/30
		If win == 10: 
			Enable animation_group 'ag_align_c':
				to 90/30
		If win == 11: 
			Enable animation_group 'ag_align_a':
				to 100/30
		If win == 11: 
			Enable animation_group 'ag_align_b':
				to 100/30
		If win == 11: 
			Enable animation_group 'ag_align_c':
				to 100/30
		If win == 12: 
			Enable animation_group 'ag_align_a':
				to 120/30
		If win == 12: 
			Enable animation_group 'ag_align_b':
				to 120/30
		If win == 12: 
			Enable animation_group 'ag_align_c':
				to 120/30
	sequence 'sound_win':
		If destroyed == 0: Run sequence 'state_blinking'.
		If destroyed == 0: Play audio 'slot_machine_win' at 'snd_2'.
		If destroyed == 0: Play audio 'slot_machine_coins_drop' at 'snd_3'.
		Play audio 'slot_machine_coin_drop_end' at 'snd_3'. (DELAY 60/30)
	sequence 'sound_lose':
		If destroyed == 0: Play audio 'slot_machine_loose' at 'snd_4'.
		If destroyed == 0: Run sequence 'state_not_blinking'.
	sequence 'sound_win_check':
		If win_all == 1: Run sequence 'sound_win'.
		start_time = 10/30 (DELAY 10/30)
		win_all = 0 (DELAY 10/30)
	sequence 'sound_lose_check':
		If win == 0: Run sequence 'sound_lose'.
	sequence 'interact':
		Run sequence 'state_not_blinking'.
		Play audio 'slot_machine_rolling_loop' at 'snd_5'.
		Stop audio 'slot_machine_rolling_loop' at 'snd_5'. (DELAY 180/30)
		Run sequence 'sound_win_check'. (DELAY 181/30)
		Run sequence 'sound_lose_check'. (DELAY 181/30)
