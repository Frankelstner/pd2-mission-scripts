unit:
	sequence 'light_on':
		material_config 'units/pd2_dlc_casino/props/cas_prop_gambling_showgirl/cas_prop_gambling_showgirl_statue'.
		Enable object 'g_neon_il'.
	sequence 'light_off':
		material_config 'units/pd2_dlc_casino/props/cas_prop_gambling_showgirl/cas_prop_gambling_showgirl_statue_off'.
		Disable object 'g_neon_il'.
