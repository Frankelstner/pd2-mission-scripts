unit:
	sequence 'interaction_disable':
		Disable interaction.
		Hide graphic_group 'grp_icon'.
	sequence 'interaction_enable':
		Enable interaction.
		Show graphic_group 'grp_icon'.
	sequence 'interact':
		Hide graphic_group 'grp_icon'.
	sequence 'load'.
