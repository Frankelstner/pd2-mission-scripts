unit:
	sequence 'open_doors':
		Enable animation_group 'anim':
			from 0/30
			speed 1
			to 60/30
		Play audio 'elevator_doors_open' at 'rp_cas_elevator_door'.
		Run sequence 'done_door_animation'. (DELAY 60/30)
	sequence 'close_doors':
		Enable animation_group 'anim':
			from 60/30
			speed -1
			to 0/30
		Play audio 'elevator_doors_close' at 'rp_cas_elevator_door'.
		Run sequence 'done_door_animation'. (DELAY 60/30)
	sequence 'done_door_animation'.
