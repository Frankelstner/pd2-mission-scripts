unit:
	body 'static_body'
		Upon receiving 50 collision damage, execute:
			Run sequence 'spawn_debris'.
	sequence 'spawn_debris':
		spawn_unit 'units/pd2_dlc_casino/environment/cas_int/cas_int_skylight_main_hall/cas_int_skylight_main_hall_debris_01':
			position object_pos('debris_1')
			rotation object_rot('debris_1')
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'debris_1' )
			position v()
		Play audio 'window_medium_shatter' at 'snd'.
