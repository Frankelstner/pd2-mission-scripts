unit:
	sequence 'enable_interaction':
		Enable interaction.
		Show graphic_group 'icon_group'.
	sequence 'disable_interaction':
		Disable interaction.
		Hide graphic_group 'icon_group'.
	sequence 'interact':
		Run sequence 'anim_open_door'.
		Hide graphic_group 'icon_group'.
	sequence 'state_door_open':
		Enable animation_group 'anim_door':
			from 183/30
			speed 0
			to 183/30
		Run sequence 'done_opened'.
	sequence 'state_door_close':
		Enable animation_group 'anim_door':
			from 0/30
			speed 0
			to 0/30
	sequence 'state_vis_hide':
		Disable body 'body_frame'.
		Disable body 'body_door_1'.
		Disable body 'body_door_2'.
		Disable body 'body_glass_1_top'.
		Disable body 'body_glass_1_bottom'.
		Disable body 'body_mover_blocker_1'.
		Disable body 'body_glass_2_top'.
		Disable body 'body_glass_2_bottom'.
		Disable body 'body_mover_blocker_2'.
		Hide graphic_group 'doors'.
	sequence 'state_vis_show':
		Enable body 'body_frame'.
		Enable body 'body_door_1'.
		Enable body 'body_door_2'.
		Enable body 'body_glass_1_top'.
		Enable body 'body_glass_1_bottom'.
		Enable body 'body_mover_blocker_1'.
		Enable body 'body_glass_2_top'.
		Enable body 'body_glass_2_bottom'.
		Enable body 'body_mover_blocker_2'.
		Show graphic_group 'doors'.
	sequence 'anim_open_door':
		Enable animation_group 'anim_door':
			from 163/30
			to 194/30
		Play audio 'generic_door_metal_open' at 'anim_door_1'.
		Play audio 'generic_door_metal_open' at 'anim_door_2'.
		Run sequence 'done_opened'. (DELAY 31/30)
		Add attention/detection preset 'open_security_gate_ene_ntl' to 'rp_cas_securityroom_door' (alarm reason: 'breaking_entering').
	sequence 'remove_attention':
		Remove attention/detection preset: 'open_security_gate_ene_ntl'
	sequence 'anim_close_door':
		Enable animation_group 'anim_door':
			from 195/30
			to 214/30
	sequence 'anim_explosion_out':
		Play audio 'c4_explode_metal' at 'anim_frame'.
		Enable animation_group 'anim_door':
			from 163/30
			speed 3
			to 194/30
		Run sequence 'done_opened'.
	sequence 'anim_explosion_in':
		Play audio 'c4_explode_metal' at 'anim_frame'.
		Enable animation_group 'anim_door':
			from 163/30
			speed 3
			to 194/30
		Run sequence 'done_opened'.
	body 'body_glass_1_top'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 saw damage or 10 melee damage, execute:
			Run sequence 'int_seq_crack_win_1_top'.
		Upon receiving 3 bullet hits or 10 explosion damage or 7.5 saw damage or 20 melee damage, execute:
	body 'body_glass_1_bottom'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 saw damage or 10 melee damage, execute:
			Run sequence 'int_seq_crack_win_1_bottom'.
		Upon receiving 3 bullet hits or 10 explosion damage or 7.5 saw damage or 20 melee damage, execute:
	body 'body_glass_2_top'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 saw damage or 10 melee damage, execute:
			Run sequence 'int_seq_crack_win_2_top'.
		Upon receiving 3 bullet hits or 10 explosion damage or 7.5 saw damage or 20 melee damage, execute:
	body 'body_glass_2_bottom'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 saw damage or 10 melee damage, execute:
			Run sequence 'int_seq_crack_win_2_bottom'.
		Upon receiving 3 bullet hits or 10 explosion damage or 7.5 saw damage or 20 melee damage, execute:
	sequence 'int_seq_crack_win_1_top':
		TRIGGER TIMES 1
		Disable object 'g_glass_1_top'.
		Enable object 'g_glass_1_top_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_1_top' (alarm reason: 'glass').
	sequence 'int_seq_shatter_win_1_top':
		TRIGGER TIMES 1
		Disable body 'body_glass_1_top'.
		Disable object 'g_glass_1_top'.
		Disable object 'g_glass_1_top_dmg'.
		Disable decal_mesh 'dm_glass_1_top'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_1_top' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_1_top'.
	sequence 'int_seq_crack_win_1_bottom':
		TRIGGER TIMES 1
		Disable object 'g_glass_1_bottom'.
		Enable object 'g_glass_1_bottom_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_1_bottom' (alarm reason: 'glass').
	sequence 'int_seq_shatter_win_1_bottom':
		TRIGGER TIMES 1
		Disable body 'body_glass_1_bottom'.
		Disable object 'g_glass_1_bottom'.
		Disable object 'g_glass_1_bottom_dmg'.
		Disable decal_mesh 'dm_glass_1_bottom'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_1_bottom' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_1_bottom'.
	sequence 'int_seq_crack_win_2_top':
		TRIGGER TIMES 1
		Disable object 'g_glass_2_top'.
		Enable object 'g_glass_2_top_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_2_top' (alarm reason: 'glass').
	sequence 'int_seq_shatter_win_2_top':
		TRIGGER TIMES 1
		Disable body 'body_glass_2_top'.
		Disable object 'g_glass_2_top'.
		Disable object 'g_glass_2_top_dmg'.
		Disable decal_mesh 'dm_glass_2_top'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_2_top' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_2_top'.
	sequence 'int_seq_crack_win_2_bottom':
		TRIGGER TIMES 1
		Disable object 'g_glass_2_bottom'.
		Enable object 'g_glass_2_bottom_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_2_bottom' (alarm reason: 'glass').
	sequence 'int_seq_shatter_win_2_bottom':
		TRIGGER TIMES 1
		Disable body 'body_glass_2_bottom'.
		Disable object 'g_glass_2_bottom'.
		Disable object 'g_glass_2_bottom_dmg'.
		Disable decal_mesh 'dm_glass_2_bottom'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_2_bottom' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_2_bottom'.
	sequence 'int_seq_open':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Enable object 'g_door'.
	sequence 'int_seq_breach_common'.
	sequence 'int_seq_bullet_hit_in':
		effect 'effects/payday2/particles/wood/wood_door_shotgun_dmg_in':
			parent object( 'e_door_dst' )
			position v()
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Disable object 'g_door'.
	sequence 'int_seq_bullet_hit_out':
		effect 'effects/payday2/particles/wood/wood_door_shotgun_dmg_out':
			parent object( 'e_door_dst' )
			position v()
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Disable object 'g_door'.
	sequence 'int_seq_explosion_in':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_explosion_in'.
		Run sequence 'done_exploded'.
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'a_shp_charge_1' )
			position v()
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'a_shp_charge_2' )
			position v()
	sequence 'int_seq_explosion_out':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_explosion_out'.
		Run sequence 'done_exploded'.
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'a_shp_charge_1' )
			position v()
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'a_shp_charge_2' )
			position v()
	sequence 'sobj_swat_breach_in':
		Enable animation_group 'anim_door':
			from 0/30
			to 83/30
		Run sequence 'int_seq_slam_door'. (DELAY 89/30)
		Run sequence 'done_swat_breach'.
	sequence 'sobj_swat_breach_out':
		Enable animation_group 'anim_door':
			from 93/30
			to 153/30
		Run sequence 'anim_explosion_out'. (DELAY 90/30)
		Run sequence 'done_swat_breach'.
	sequence 'int_seq_slam_door':
		Enable animation_group 'anim_door':
			from 163/30
			speed 3
			to 194/30
		Play audio 'generic_door_metal_open' at 'anim_door_1'.
		Play audio 'generic_door_metal_open' at 'anim_door_2'.
		Run sequence 'int_seq_breach_common'.
		Run sequence 'done_opened'.
	sequence 'open_door':
		Run sequence 'int_seq_open'.
	sequence 'done_exploded'.
	sequence 'done_opened'.
	sequence 'done_swat_breach'.
	sequence 'explode_door':
		Run sequence 'int_seq_explosion_in'.
	sequence 'open_door':
		Run sequence 'int_seq_open'.
	sequence 'door_opened'.
	sequence 'door_closed'.
	sequence 'drill_placed'.
	sequence 'drill_jammed'.
	sequence 'drill_resumed'.
	sequence 'drill_power_off'.
	sequence 'drill_power_on'.
	sequence 'drill_completed'.
	sequence 'all_drill_placed'.
	sequence 'c4_placed'.
	sequence 'c4_completed'.
	sequence 'all_c4_placed'.
