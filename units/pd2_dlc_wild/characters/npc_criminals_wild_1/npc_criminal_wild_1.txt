unit:
	armor_state = 1
	body_replace = 0
	sequence 'int_seq_hide_all':
		Disable object 'g_body'.
		Disable object 'g_head'.
		Disable object 'g_hands'.
	sequence 'spawn_prop_raincoat':
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_glace/characters/glc_acc_raincoat/glc_acc_raincoat')
		Disable object 'g_vest_small'.
		Disable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'despawn_prop_raincoat':
		Call function: spawn_manager.remove_unit('char_mesh')
		If armor_state == 1: Run sequence 'var_model_01'.
		If armor_state == 2: Run sequence 'var_model_02'.
		If armor_state == 3: Run sequence 'var_model_03'.
		If armor_state == 4: Run sequence 'var_model_04'.
		If armor_state == 5: Run sequence 'var_model_05'.
		If armor_state == 6: Run sequence 'var_model_06'.
		If armor_state == 7: Run sequence 'var_model_07'.
	sequence 'spawn_prop_sneak_suit':
		body_replace = 1
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_dah/characters/dah_acc_stealth_suit_male_big/dah_acc_stealth_suit_male_big')
		Disable object 'g_body'.
		Disable object 'g_hands'.
		Disable object 'g_vest_small'.
		Disable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'despawn_prop_sneak_suit':
		Call function: spawn_manager.remove_unit('char_mesh')
		If armor_state == 1: Run sequence 'var_model_01'.
		If armor_state == 2: Run sequence 'var_model_02'.
		If armor_state == 3: Run sequence 'var_model_03'.
		If armor_state == 4: Run sequence 'var_model_04'.
		If armor_state == 5: Run sequence 'var_model_05'.
		If armor_state == 6: Run sequence 'var_model_06'.
		If armor_state == 7: Run sequence 'var_model_07'.
	sequence 'spawn_prop_murky_suit':
		body_replace = 1
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_vit/characters/vit_acc_murky_suit_male_big/vit_acc_murky_suit_male_big')
		Disable object 'g_body'. (DELAY 5/30)
		Disable object 'g_hands'. (DELAY 5/30)
		Disable object 'g_vest_small'. (DELAY 5/30)
		Disable object 'g_vest_body'. (DELAY 5/30)
		Disable object 'g_vest_shoulder'. (DELAY 5/30)
		Disable object 'g_vest_neck'. (DELAY 5/30)
		Disable object 'g_vest_thies'. (DELAY 5/30)
		Disable object 'g_vest_leg_arm'. (DELAY 5/30)
	sequence 'despawn_prop_murky_suit':
		Call function: spawn_manager.remove_unit('char_mesh')
		Enable object 'g_body'.
		Enable object 'g_hands'.
		If armor_state == 1: Run sequence 'var_model_01'.
		If armor_state == 2: Run sequence 'var_model_02'.
		If armor_state == 3: Run sequence 'var_model_03'.
		If armor_state == 4: Run sequence 'var_model_04'.
		If armor_state == 5: Run sequence 'var_model_05'.
		If armor_state == 6: Run sequence 'var_model_06'.
		If armor_state == 7: Run sequence 'var_model_07'.
	sequence 'spawn_prop_tux':
		body_replace = 1
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_sah/characters/npc_acc_criminal_rust_tux/npc_acc_criminal_rust_tux')
		Disable object 'g_body'.
		Disable object 'g_body_vest'.
		Disable object 'g_body_armor'.
		Disable object 'g_vest_small'.
		Disable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'despawn_prop_sneak_suit':
		Call function: spawn_manager.remove_unit('char_mesh')
		If armor_state == 1: Run sequence 'var_model_01'.
		If armor_state == 2: Run sequence 'var_model_02'.
		If armor_state == 3: Run sequence 'var_model_03'.
		If armor_state == 4: Run sequence 'var_model_04'.
		If armor_state == 5: Run sequence 'var_model_05'.
		If armor_state == 6: Run sequence 'var_model_06'.
		If armor_state == 7: Run sequence 'var_model_07'.
	sequence 'spawn_prop_scrub':
		body_replace = 1
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_nmh/characters/nmh_acc_scrubs_male_big/nmh_acc_scrubs_male_big')
		Disable object 'g_body'. (DELAY 5/30)
		Disable object 'g_hands'. (DELAY 5/30)
		Disable object 'g_vest_small'. (DELAY 5/30)
		Disable object 'g_vest_body'. (DELAY 5/30)
		Disable object 'g_vest_shoulder'. (DELAY 5/30)
		Disable object 'g_vest_neck'. (DELAY 5/30)
		Disable object 'g_vest_thies'. (DELAY 5/30)
		Disable object 'g_vest_leg_arm'. (DELAY 5/30)
	sequence 'spawn_prop_winter_suit':
		body_replace = 1
		Call function: spawn_manager.spawn_and_link_unit('_char_joint_names','char_mesh','units/pd2_dlc_wwh/characters/wwh_acc_stealth_suit_male_big/wwh_acc_stealth_suit_male_big')
		Disable object 'g_body'.
		Disable object 'g_hands'.
		Disable object 'g_vest_small'.
		Disable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
	sequence 'despawn_prop_winter_suit':
		Call function: spawn_manager.remove_unit('char_mesh')
		If armor_state == 1: Run sequence 'var_model_01'.
		If armor_state == 2: Run sequence 'var_model_02'.
		If armor_state == 3: Run sequence 'var_model_03'.
		If armor_state == 4: Run sequence 'var_model_04'.
		If armor_state == 5: Run sequence 'var_model_05'.
		If armor_state == 6: Run sequence 'var_model_06'.
		If armor_state == 7: Run sequence 'var_model_07'.
	sequence 'var_mtr_wild':
		Run sequence 'int_seq_hide_all'.
		If body_replace == 0: Enable object 'g_body'.
		Enable object 'g_hands'.
		Enable object 'g_head'.
	sequence 'var_model_01':
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Disable object 'g_vest_small'.
		Disable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
		armor_state = 1
	sequence 'var_model_02':
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Enable object 'g_vest_small'.
		Disable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
		armor_state = 2
	sequence 'var_model_03':
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Disable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
		armor_state = 3
	sequence 'var_model_04':
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Disable object 'g_vest_shoulder'.
		Enable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
		armor_state = 4
	sequence 'var_model_05':
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Enable object 'g_vest_shoulder'.
		Enable object 'g_vest_neck'.
		Disable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
		armor_state = 5
	sequence 'var_model_06':
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Enable object 'g_vest_shoulder'.
		Enable object 'g_vest_neck'.
		Enable object 'g_vest_thies'.
		Disable object 'g_vest_leg_arm'.
		armor_state = 6
	sequence 'var_model_07':
		Enable object 'g_body'.
		Enable object 'g_hands'.
		Disable object 'g_vest_small'.
		Enable object 'g_vest_body'.
		Enable object 'g_vest_shoulder'.
		Enable object 'g_vest_neck'.
		Enable object 'g_vest_thies'.
		Enable object 'g_vest_leg_arm'.
		armor_state = 7
	sequence 'preprocessor_dummy'.
