unit:
	hidden = 0
	sequence 'enable_interaction':
		If hidden == 0: 
			Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'hide':
		Disable interaction.
		Disable object 'g_bag'.
		Disable object 'g_goldbars'.
		Disable body 'body_static'.
		Disable decal_mesh 'dm_cloth'.
		Disable decal_mesh 'dm_metal'.
		hidden = 1
	sequence 'show':
		Enable interaction.
		Enable object 'g_bag'.
		Enable object 'g_goldbars'.
		Enable body 'body_static'.
		Enable decal_mesh 'dm_cloth'.
		Enable decal_mesh 'dm_metal'.
		hidden = 0
	sequence 'show_and_enable_interaction':
		Enable interaction.
		Enable object 'g_bag'.
		Enable object 'g_goldbars'.
		Enable body 'body_static'.
		Enable decal_mesh 'dm_cloth'.
		Enable decal_mesh 'dm_metal'.
		hidden = 0
	sequence 'interact':
		Run sequence 'hide'.
	sequence 'state_contour_enable':
		material_config 'units/payday2/pickups/gen_pku_gold/gen_pku_gold_contour'.
	sequence 'state_contour_disable':
		material_config 'units/pd2_dlc_uno/props/uno_interactable_goldbag_open/uno_interactable_goldbag_open'.
	sequence 'load'.
