unit:
	sequence 'anim_wheelchair':
		Play audio 'wheelchair_loop' at 'snd'.
		Play audio 'wheelchair_loop_stop' at 'snd'. (DELAY 1530/30)
		Enable animation_group 'anim_chair':
			from 0/30
			to 1530/30
		Run sequence 'done_hot_wheels'. (DELAY 1535/30)
	sequence 'anim_wheelchair_idle':
		Enable animation_group 'anim_chair':
			from 0/30
			to 0/30
	sequence 'hide':
		Disable object 'g_frame'.
		Disable object 'g_wheels'.
		Disable decal_mesh 'dm_metal'.
		Disable decal_mesh 'dm_seat'.
		Disable body 'body0'.
	sequence 'show':
		Enable object 'g_frame'.
		Enable object 'g_wheels'.
		Enable decal_mesh 'dm_metal'.
		Enable decal_mesh 'dm_seat'.
		Enable body 'body0'.
	sequence 'done_hot_wheels'.
