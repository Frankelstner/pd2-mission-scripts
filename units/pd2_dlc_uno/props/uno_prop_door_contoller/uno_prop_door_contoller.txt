unit:
	sequence 'hide':
		Disable object 'g_lever'.
		Disable object 'g_g'.
		Disable body 'body_static'.
		Disable body 'body_collision_01'.
	sequence 'show':
		Enable object 'g_lever'.
		Enable object 'g_g'.
		Enable body 'body_static'.
		Enable body 'body_collision_01'.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'interact':
		Enable animation_group 'anim_lever':
			from 0/30
			to 30/30
		Play audio 'uno_lever_pull' at 'rp_uno_prop_door_contoller'.
		Enable animation_group 'anim_lever': (DELAY 30/30)
			from 30/30
			speed -1
			to 0/30
		Play audio 'uno_lever_pull' at 'rp_uno_prop_door_contoller'. (DELAY 30/30)
