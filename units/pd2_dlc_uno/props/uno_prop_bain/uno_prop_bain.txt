unit:
	sequence 'anim_gun_fail':
		animation_redirect 'bain_die'.
		Enable effect_spawner 'blood_impact'. (DELAY 122/30)
		Enable effect_spawner 'blood_exit'. (DELAY 122/30)
	sequence 'anim_gun_win':
		animation_redirect 'bain_win'.
	sequence 'anim_gun_loop':
		animation_redirect 'bain_loop'.
	sequence 'show':
		Enable body 'body_collision_01'.
		Enable body 'body_collision_02'.
		Enable body 'body_collision_03'.
		Enable body 'body_collision_04'.
		Enable body 'body_collision_05'.
		Enable body 'body_collision_06'.
		Enable body 'body_collision_07'.
		Enable body 'body_collision_08'.
		Enable body 'body_collision_09'.
		Enable body 'body_collision_10'.
		Enable body 'body_collision_11'.
		Enable body 'body_collision_12'.
		Enable body 'body_collision_13'.
		Enable body 'body_collision_14'.
		Enable body 'body_collision_15'.
		Show graphic_group 'grp_bain'.
	sequence 'hide':
		Disable body 'body_collision_01'.
		Disable body 'body_collision_02'.
		Disable body 'body_collision_03'.
		Disable body 'body_collision_04'.
		Disable body 'body_collision_05'.
		Disable body 'body_collision_06'.
		Disable body 'body_collision_07'.
		Disable body 'body_collision_08'.
		Disable body 'body_collision_09'.
		Disable body 'body_collision_10'.
		Disable body 'body_collision_11'.
		Disable body 'body_collision_12'.
		Disable body 'body_collision_13'.
		Disable body 'body_collision_14'.
		Disable body 'body_collision_15'.
		Hide graphic_group 'grp_bain'.
