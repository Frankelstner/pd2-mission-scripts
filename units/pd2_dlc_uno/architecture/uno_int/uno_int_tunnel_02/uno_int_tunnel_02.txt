unit:
	sequence 'init':
		EXECUTE ON STARTUP
		material 'mat_rope':
			state 'stop'
		startup True
	sequence 'int_seq_anim_lever':
		Enable animation_group 'anim_lever':
			from 0/30
			to 30/30
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'interact':
		Run sequence 'int_seq_anim_lever'.
		Run sequence 'anim_shaft'.
		Play audio 'int_wooden_elevator_finish' at 'snd_03'.
	sequence 'state_shaft_done':
		Enable animation_group 'anim_shaft':
			from 1912/30
			to 1912/30
		Enable animation_group 'anim_counterweight':
			from 1912/30
			to 1912/30
	sequence 'state_shaft_start':
		Enable animation_group 'anim_shaft':
			from 1800/30
			to 0/30
		Enable animation_group 'anim_counterweight':
			from 1800/30
			to 0/30
	sequence 'anim_shaft':
		material 'mat_rope':
			state 'play 3'
			time 0
		Enable animation_group 'anim_shaft':
			from 0/30
			to 1800/30
		Enable animation_group 'anim_counterweight':
			from 0/30
			to 1800/30
		Play audio 'wooden_elevator_mechanics_loop' at 'snd_02'.
		Play audio 'wooden_elevator_loop_01' at 'snd_el_01'.
		Play audio 'wooden_elevator_loop_02' at 'snd_el_02'.
		Play audio 'wooden_elevator_stop_01' at 'snd_el_01'. (DELAY 1770/30)
		Play audio 'wooden_elevator_stop_02' at 'snd_el_02'. (DELAY 1770/30)
		Run sequence 'done_arrive'. (DELAY 1800/30)
	sequence 'anim_elevator':
		Enable animation_group 'anim_elevator':
			from 0/30
			to 1800/30
	sequence 'done_arrive'.
	sequence 'show_door':
		Enable object 'g_door'.
	sequence 'hide_door':
		Disable object 'g_door'.
	sequence 'hide_all':
		Disable object 'g_elevator'.
		Disable object 'g_lever'.
		Disable object 'g_wheel'.
		Disable object 'g_wheel2'.
		Disable object 'g_wheel3'.
		Disable object 'g_counterweight'.
		Disable object 'g_g'.
		Disable object 'g_cave'.
		Disable object 'g_machine'.
		Disable object 'g_rope'.
		Disable object 'g_rope1'.
		Disable object 'g_door'.
		Disable object 'g_lights'.
		Disable object 'g_lightbulb_elevator'.
		Disable object 's_s'.
		Disable light 'lightbulb_001'.
		Disable light 'lightbulb_002'.
		Disable light 'lightbulb_003'.
		Disable light 'lightbulb_004'.
		Disable light 'lightbulb_005'.
		Disable light 'lightbulb_006'.
		Disable light 'lightbulb_007'.
		Disable light 'lightbulb_008'.
		Disable light 'lightbulb_009'.
		Disable light 'lightbulb_010'.
		Disable light 'lightbulb_011'.
		Disable light 'lightbulb_012'.
		Disable light 'lightbulb_015'.
		Disable light 'omni_door'.
		Disable light 'omni_ambient_main'.
		Disable light 'omni_ambient_bulbs_01'.
		Disable light 'omni_ambient_bulbs_02'.
		Disable light 'omni_ambient_bulbs_03'.
		Disable light 'omni_ambient_bulbs_04'.
		Disable light 'omni_ambient_bulbs_05'.
		Disable light 'omni_spot_01'.
		Disable light 'omni_spot_02'.
		Disable light 'spotlight_spot_01'.
		Disable light 'spotlight_spot_02'.
		Disable light 'lightbulb_tunnel_001'.
		Disable light 'lightbulb_tunnel_002'.
		Disable light 'lightbulb_tunnel_003'.
		Disable light 'lightbulb_tunnel_004'.
		Disable light 'lightbulb_tunnel_005'.
		Disable light 'ambientlight_tunnel_001'.
		Disable light 'ambientlight_tunnel_002'.
		Disable light 'lightbulb_shaft_001'.
		Disable light 'lightbulb_shaft_002'.
		Disable light 'lightbulb_shaft_003'.
		Disable light 'lightbulb_shaft_004'.
		Disable light 'lightbulb_shaft_005'.
		Disable light 'lightbulb_shaft_006'.
		Disable light 'lightbulb_shaft_007'.
		Disable light 'lightbulb_shaft_008'.
		Disable light 'lightbulb_shaft_009'.
		Disable light 'lightbulb_shaft_010'.
		Disable light 'lightbulb_shaft_011'.
		Disable light 'lightbulb_shaft_012'.
		Disable light 'lightbulb_shaft_013'.
		Disable light 'lightbulb_shaft_014'.
		Disable light 'lightbulb_shaft_015'.
		Disable light 'lightbulb_shaft_016'.
		Disable light 'lightbulb_shaft_017'.
		Disable light 'lightbulb_shaft_018'.
		Disable light 'lightbulb_shaft_019'.
		Disable light 'lightbulb_shaft_020'.
		Disable light 'lightbulb_shaft_021'.
		Disable light 'lightbulb_shaft_022'.
		Disable light 'lightbulb_shaft_023'.
		Disable light 'lightbulb_shaft_024'.
		Disable light 'lightbulb_shaft_025'.
		Disable light 'lightbulb_shaft_026'.
		Disable light 'lightbulb_shaft_027'.
		Disable light 'lightbulb_shaft_028'.
		Disable effect_spawner 'candle_1'.
		Disable effect_spawner 'candle_2'.
		Disable effect_spawner 'candle_3'.
		Disable effect_spawner 'candle_4'.
		Disable effect_spawner 'indoor_lamp_0'.
		Disable effect_spawner 'indoor_lamp_1'.
		Disable effect_spawner 'indoor_lamp_2'.
		Disable effect_spawner 'indoor_lamp_3'.
		Disable effect_spawner 'indoor_lamp_4'.
		Disable effect_spawner 'indoor_lamp_5'.
		Disable effect_spawner 'indoor_lamp_6'.
		Disable effect_spawner 'indoor_lamp_7'.
		Disable effect_spawner 'indoor_lamp_8'.
		Disable effect_spawner 'indoor_lamp_9'.
		Disable effect_spawner 'indoor_lamp_10'.
		Disable effect_spawner 'indoor_lamp_11'.
		Disable effect_spawner 'indoor_lamp_12'.
		Disable effect_spawner 'indoor_lamp_13'.
		Disable effect_spawner 'indoor_lamp_14'.
		Disable effect_spawner 'indoor_lamp_15'.
		Disable effect_spawner 'indoor_lamp_16'.
		Disable effect_spawner 'indoor_lamp_17'.
		Disable effect_spawner 'indoor_lamp_18'.
		Disable effect_spawner 'indoor_lamp_19'.
		Disable effect_spawner 'indoor_lamp_20'.
		Disable effect_spawner 'indoor_lamp_21'.
		Disable effect_spawner 'indoor_lamp_22'.
		Disable effect_spawner 'indoor_lamp_23'.
		Disable effect_spawner 'indoor_lamp_24'.
		Disable effect_spawner 'indoor_lamp_25'.
		Disable effect_spawner 'indoor_lamp_26'.
		Disable effect_spawner 'indoor_lamp_27'.
		Disable effect_spawner 'indoor_lamp_28'.
		Disable effect_spawner 'indoor_lamp_29'.
		Disable effect_spawner 'indoor_lamp_30'.
		Disable effect_spawner 'indoor_lamp_31'.
		Disable effect_spawner 'indoor_lamp_32'.
		Disable effect_spawner 'indoor_lamp_33'.
		Disable effect_spawner 'indoor_lamp_34'.
		Disable effect_spawner 'indoor_lamp_35'.
		Disable effect_spawner 'indoor_lamp_36'.
		Disable effect_spawner 'indoor_lamp_37'.
		Disable effect_spawner 'indoor_lamp_38'.
		Disable effect_spawner 'indoor_lamp_39'.
		Disable effect_spawner 'indoor_lamp_40'.
		Disable effect_spawner 'indoor_lamp_41'.
		Disable effect_spawner 'indoor_lamp_42'.
		Disable effect_spawner 'indoor_lamp_43'.
		Disable effect_spawner 'indoor_lamp_44'.
		Disable effect_spawner 'indoor_lamp_45'.
		Disable effect_spawner 'indoor_lamp_50'.
		Disable light 'candle_light'.
		Disable body 'body_collision_01'.
		Disable body 'body_collision_05'.
		Disable body 'body_lever'.
		Disable body 'body_shaft'.
	sequence 'show_all':
		Enable object 'g_elevator'.
		Enable object 'g_lever'.
		Enable object 'g_wheel'.
		Enable object 'g_wheel2'.
		Enable object 'g_wheel3'.
		Enable object 'g_counterweight'.
		Enable object 'g_g'.
		Enable object 'g_cave'.
		Enable object 'g_machine'.
		Enable object 'g_rope'.
		Enable object 'g_rope1'.
		Enable object 'g_door'.
		Enable object 'g_lights'.
		Enable object 'g_lightbulb_elevator'.
		Enable object 's_s'.
		Enable light 'lightbulb_001'.
		Enable light 'lightbulb_002'.
		Enable light 'lightbulb_003'.
		Enable light 'lightbulb_004'.
		Enable light 'lightbulb_005'.
		Enable light 'lightbulb_006'.
		Enable light 'lightbulb_007'.
		Enable light 'lightbulb_008'.
		Enable light 'lightbulb_009'.
		Enable light 'lightbulb_010'.
		Enable light 'lightbulb_011'.
		Enable light 'lightbulb_012'.
		Enable light 'lightbulb_015'.
		Enable light 'omni_door'.
		Enable light 'omni_ambient_main'.
		Enable light 'omni_ambient_bulbs_01'.
		Enable light 'omni_ambient_bulbs_02'.
		Enable light 'omni_ambient_bulbs_03'.
		Enable light 'omni_ambient_bulbs_04'.
		Enable light 'omni_ambient_bulbs_05'.
		Enable light 'omni_spot_01'.
		Enable light 'omni_spot_02'.
		Enable light 'spotlight_spot_01'.
		Enable light 'spotlight_spot_02'.
		Enable light 'lightbulb_tunnel_001'.
		Enable light 'lightbulb_tunnel_002'.
		Enable light 'lightbulb_tunnel_003'.
		Enable light 'lightbulb_tunnel_004'.
		Enable light 'lightbulb_tunnel_005'.
		Enable light 'ambientlight_tunnel_001'.
		Enable light 'ambientlight_tunnel_002'.
		Enable light 'lightbulb_shaft_001'.
		Enable light 'lightbulb_shaft_002'.
		Enable light 'lightbulb_shaft_003'.
		Enable light 'lightbulb_shaft_004'.
		Enable light 'lightbulb_shaft_005'.
		Enable light 'lightbulb_shaft_006'.
		Enable light 'lightbulb_shaft_007'.
		Enable light 'lightbulb_shaft_008'.
		Enable light 'lightbulb_shaft_009'.
		Enable light 'lightbulb_shaft_010'.
		Enable light 'lightbulb_shaft_011'.
		Enable light 'lightbulb_shaft_012'.
		Enable light 'lightbulb_shaft_013'.
		Enable light 'lightbulb_shaft_014'.
		Enable light 'lightbulb_shaft_015'.
		Enable light 'lightbulb_shaft_016'.
		Enable light 'lightbulb_shaft_017'.
		Enable light 'lightbulb_shaft_018'.
		Enable light 'lightbulb_shaft_019'.
		Enable light 'lightbulb_shaft_020'.
		Enable light 'lightbulb_shaft_021'.
		Enable light 'lightbulb_shaft_022'.
		Enable light 'lightbulb_shaft_023'.
		Enable light 'lightbulb_shaft_024'.
		Enable light 'lightbulb_shaft_025'.
		Enable light 'lightbulb_shaft_026'.
		Enable light 'lightbulb_shaft_027'.
		Enable light 'lightbulb_shaft_028'.
		Enable effect_spawner 'candle_1'.
		Enable effect_spawner 'candle_2'.
		Enable effect_spawner 'candle_3'.
		Enable effect_spawner 'candle_4'.
		Enable effect_spawner 'indoor_lamp_0'.
		Enable effect_spawner 'indoor_lamp_1'.
		Enable effect_spawner 'indoor_lamp_2'.
		Enable effect_spawner 'indoor_lamp_3'.
		Enable effect_spawner 'indoor_lamp_4'.
		Enable effect_spawner 'indoor_lamp_5'.
		Enable effect_spawner 'indoor_lamp_6'.
		Enable effect_spawner 'indoor_lamp_7'.
		Enable effect_spawner 'indoor_lamp_8'.
		Enable effect_spawner 'indoor_lamp_9'.
		Enable effect_spawner 'indoor_lamp_10'.
		Enable effect_spawner 'indoor_lamp_11'.
		Enable effect_spawner 'indoor_lamp_12'.
		Enable effect_spawner 'indoor_lamp_13'.
		Enable effect_spawner 'indoor_lamp_14'.
		Enable effect_spawner 'indoor_lamp_15'.
		Enable effect_spawner 'indoor_lamp_16'.
		Enable effect_spawner 'indoor_lamp_17'.
		Enable effect_spawner 'indoor_lamp_18'.
		Enable effect_spawner 'indoor_lamp_19'.
		Enable effect_spawner 'indoor_lamp_20'.
		Enable effect_spawner 'indoor_lamp_21'.
		Enable effect_spawner 'indoor_lamp_22'.
		Enable effect_spawner 'indoor_lamp_23'.
		Enable effect_spawner 'indoor_lamp_24'.
		Enable effect_spawner 'indoor_lamp_25'.
		Enable effect_spawner 'indoor_lamp_26'.
		Enable effect_spawner 'indoor_lamp_27'.
		Enable effect_spawner 'indoor_lamp_28'.
		Enable effect_spawner 'indoor_lamp_29'.
		Enable effect_spawner 'indoor_lamp_30'.
		Enable effect_spawner 'indoor_lamp_31'.
		Enable effect_spawner 'indoor_lamp_32'.
		Enable effect_spawner 'indoor_lamp_33'.
		Enable effect_spawner 'indoor_lamp_34'.
		Enable effect_spawner 'indoor_lamp_35'.
		Enable effect_spawner 'indoor_lamp_36'.
		Enable effect_spawner 'indoor_lamp_37'.
		Enable effect_spawner 'indoor_lamp_38'.
		Enable effect_spawner 'indoor_lamp_39'.
		Enable effect_spawner 'indoor_lamp_40'.
		Enable effect_spawner 'indoor_lamp_41'.
		Enable effect_spawner 'indoor_lamp_42'.
		Enable effect_spawner 'indoor_lamp_43'.
		Enable effect_spawner 'indoor_lamp_44'.
		Enable effect_spawner 'indoor_lamp_45'.
		Enable effect_spawner 'indoor_lamp_50'.
		Enable light 'candle_light'.
		Enable body 'body_collision_01'.
		Enable body 'body_collision_05'.
		Enable body 'body_lever'.
		Enable body 'body_shaft'.
