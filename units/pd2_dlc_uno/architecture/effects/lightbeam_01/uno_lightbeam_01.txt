unit:
	sequence 'hide_debug_boxes':
		Disable object 'g_debug'.
	sequence 'show_debug_boxes':
		Enable object 'g_debug'.
	sequence 'show_lightay_effect':
		Disable object 'g_debug'.
		Enable effect_spawner 'light_beam_01'.
		Enable effect_spawner 'light_beam_02'.
		Enable effect_spawner 'light_beam_03'.
		Enable effect_spawner 'light_beam_04'.
		Enable effect_spawner 'light_beam_05'.
		Enable effect_spawner 'light_beam_06'.
		Enable effect_spawner 'light_beam_07'.
		Enable effect_spawner 'light_beam_08'.
		Enable effect_spawner 'light_beam_09'.
		Enable effect_spawner 'light_beam_10'.
		Enable effect_spawner 'light_beam_11'.
		Enable effect_spawner 'light_beam_12'.
		Enable effect_spawner 'light_beam_13'.
		Enable effect_spawner 'light_beam_14'.
		Enable effect_spawner 'light_beam_15'.
		Enable effect_spawner 'light_beam_16'.
