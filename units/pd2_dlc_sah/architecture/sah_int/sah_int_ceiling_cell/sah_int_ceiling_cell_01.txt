unit:
	sequence 'glass_shatter':
		TRIGGER TIMES 1
		Disable body 'glass_body'.
		effect 'effects/payday2/particles/window/window_break_1x1':
			parent object( 'e_glass_01' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_01'.
		Disable object 'g_glass_01'.
		Disable decal_mesh 'g_glass_01'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_01' (alarm reason: 'glass').
	body 'glass_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'glass_shatter'.
