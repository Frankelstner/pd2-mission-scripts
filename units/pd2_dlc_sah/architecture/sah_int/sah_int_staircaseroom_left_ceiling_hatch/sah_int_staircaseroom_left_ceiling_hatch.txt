unit:
	sequence 'open_hatch':
		Enable animation_group 'hatch':
			from 50/30
			speed 1
			to 50/30
	sequence 'close_hatch':
		Enable animation_group 'hatch':
			from 0/30
			speed 1
			to 0/30
	sequence 'anim_open_hatch':
		Enable animation_group 'hatch':
			from 0/30
			to 50/30
	sequence 'anim_close_hatch':
		Enable animation_group 'hatch':
			from 50/30
			speed -1
			to 0/30
