unit:
	sequence 'open_doors':
		Enable animation_group 'door':
			from 40/30
			speed 1
			to 40/30
	sequence 'close_doors':
		Enable animation_group 'door':
			from 0/30
			speed 1
			to 0/30
	sequence 'anim_open_door':
		Enable animation_group 'door':
			from 0/30
			to 50/30
		Play audio 'glass_door_open' at 'a_door'.
	sequence 'anim_close_door':
		Enable animation_group 'door':
			from 50/30
			speed -1
			to 0/30
		Play audio 'glass_door_open' at 'a_door'.
