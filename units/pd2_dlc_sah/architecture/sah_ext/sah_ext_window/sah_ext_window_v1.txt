unit:
	sequence 'crack_window_1':
		TRIGGER TIMES 1
		Disable object 'g_glass_1'.
		Enable object 'g_glass_dmg_1'.
		Cause alert with 12 m radius.
	sequence 'crack_window_2':
		TRIGGER TIMES 1
		Disable object 'g_glass_2'.
		Enable object 'g_glass_dmg_2'.
		Cause alert with 12 m radius.
	sequence 'crack_window_3':
		TRIGGER TIMES 1
		Disable object 'g_glass_3'.
		Enable object 'g_glass_dmg_3'.
		Cause alert with 12 m radius.
	sequence 'crack_window_4':
		TRIGGER TIMES 1
		Disable object 'g_glass_4'.
		Enable object 'g_glass_dmg_4'.
		Cause alert with 12 m radius.
	sequence 'break_window_1':
		TRIGGER TIMES 1
		Disable body 'static_glass_1'.
		Disable object 'g_glass_dmg_1'.
		Disable object 'g_glass_1'.
		Disable decal_mesh 'dm_glass_breakable_1'.
		Play audio 'window_medium_shatter' at 'e_glass_1'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_1')
			position v()
		Cause alert with 12 m radius.
	sequence 'break_window_2':
		TRIGGER TIMES 1
		Disable body 'static_glass_2'.
		Disable object 'g_glass_dmg_2'.
		Disable object 'g_glass_2'.
		Disable decal_mesh 'dm_glass_breakable_2'.
		Play audio 'window_medium_shatter' at 'e_glass_2'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_2')
			position v()
		Cause alert with 12 m radius.
	sequence 'break_window_3':
		TRIGGER TIMES 1
		Disable body 'static_glass_3'.
		Disable object 'g_glass_dmg_3'.
		Disable object 'g_glass_3'.
		Disable decal_mesh 'dm_glass_breakable_3'.
		Play audio 'window_medium_shatter' at 'e_glass_3'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_3')
			position v()
		Cause alert with 12 m radius.
	sequence 'break_window_4':
		TRIGGER TIMES 1
		Disable body 'static_glass_4'.
		Disable object 'g_glass_dmg_4'.
		Disable object 'g_glass_4'.
		Disable decal_mesh 'dm_glass_breakable_4'.
		Play audio 'window_medium_shatter' at 'e_glass_4'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_4')
			position v()
		Cause alert with 12 m radius.
	body 'static_glass_1'
		Upon receiving 2 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'crack_window_1'.
		Upon receiving 4 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'break_window_1'.
	body 'static_glass_2'
		Upon receiving 2 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'crack_window_2'.
		Upon receiving 4 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'break_window_2'.
	body 'static_glass_3'
		Upon receiving 2 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'crack_window_3'.
		Upon receiving 4 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'break_window_3'.
	body 'static_glass_4'
		Upon receiving 2 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'crack_window_4'.
		Upon receiving 4 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'break_window_4'.
