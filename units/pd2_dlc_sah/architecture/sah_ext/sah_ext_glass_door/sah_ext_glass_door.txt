unit:
	sequence 'init':
		EXECUTE ON STARTUP
		Play audio 'emitter_rain_on_window_indoor_sphere' at 'snd'.
		startup True
	sequence 'new_sound':
		TRIGGER TIMES 1
		Stop audio 'emitter_rain_on_window_indoor_sphere' at 'snd'.
		Play audio 'emitter_heavy_rain_asphalt' at 'snd'.
	sequence 'open_doors':
		Enable animation_group 'door':
			from 1/30
			speed 1
			to 1/30
	sequence 'close_doors':
		Enable animation_group 'door':
			from 0/30
			speed 1
			to 0/30
	sequence 'anim_open_door':
		Enable animation_group 'door':
			from 0/30
			to 30/30
	sequence 'anim_close_door':
		Enable animation_group 'door':
			from 30/30
			speed -1
			to 0/30
	sequence 'crack_window_1':
		TRIGGER TIMES 1
		Run sequence 'window_hurt'.
		Disable object 'g_window_glass_01'.
		Enable object 'g_window_glass_01_dmg'.
		Cause alert with 12 m radius.
	sequence 'crack_window_2':
		TRIGGER TIMES 1
		Run sequence 'window_hurt'.
		Disable object 'g_window_glass_02'.
		Enable object 'g_window_glass_02_dmg'.
		Cause alert with 12 m radius.
	sequence 'crack_window_3':
		TRIGGER TIMES 1
		Run sequence 'window_hurt'.
		Disable object 'g_window_glass_03'.
		Enable object 'g_window_glass_03_dmg'.
		Cause alert with 12 m radius.
	sequence 'crack_window_4':
		TRIGGER TIMES 1
		Run sequence 'window_hurt'.
		Disable object 'g_window_glass_04'.
		Enable object 'g_window_glass_04_dmg'.
		Cause alert with 12 m radius.
	sequence 'break_window_1':
		TRIGGER TIMES 1
		Enable body 'animated_door_frame_a'.
		Disable body 'animated_door_glass_a'.
		Disable object 'g_window_glass_01_dmg'.
		Disable object 'g_window_glass_01'.
		Play audio 'window_medium_shatter' at 'e_glass_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_01')
			position v()
		Cause alert with 12 m radius.
		Run sequence 'new_sound'.
	sequence 'break_window_2':
		TRIGGER TIMES 1
		Enable body 'animated_door_frame_b'.
		Disable body 'animated_door_glass_b'.
		Disable object 'g_window_glass_02_dmg'.
		Disable object 'g_window_glass_02'.
		Play audio 'window_medium_shatter' at 'e_glass_02'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_02')
			position v()
		Cause alert with 12 m radius.
		Run sequence 'new_sound'.
	sequence 'break_window_3':
		TRIGGER TIMES 1
		Disable body 'animated_glass_03'.
		Disable object 'g_window_glass_03_dmg'.
		Disable decal_mesh 'dm_glass_breakable_03'.
		Play audio 'window_medium_shatter' at 'e_glass_03'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_03')
			position v()
		Cause alert with 12 m radius.
		Run sequence 'new_sound'.
	sequence 'break_window_4':
		TRIGGER TIMES 1
		Disable body 'animated_glass_04'.
		Disable object 'g_window_glass_04_dmg'.
		Disable decal_mesh 'dm_glass_breakable_04'.
		Play audio 'window_medium_shatter' at 'e_glass_04'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_04')
			position v()
		Cause alert with 12 m radius.
		Run sequence 'new_sound'.
	sequence 'break_frame':
		TRIGGER TIMES 1
		Run sequence 'window_break'.
		Run sequence 'crack_window_1'.
		Run sequence 'crack_window_2'.
		Run sequence 'break_window_1'.
		Run sequence 'break_window_2'.
		Disable body 'animated_door_frame_a'.
		Disable body 'animated_door_frame_b'.
		Disable object 'g_frame_a'.
		Disable object 'g_frame_b'.
		Enable object 'g_frame_dmg_a'.
		Enable object 'g_frame_dmg_b'.
		Disable object 's_door_a_frame'.
		Disable object 's_door_b_frame'.
		spawn_unit 'units/pd2_indiana/architecture/mus_ext_courtyard/mus_ext_court_window_debris_a':
			position object_pos('e_glass_03')
			rotation object_rot('e_glass_03')
		Cause alert with 12 m radius.
		spawn_unit 'units/pd2_indiana/architecture/mus_ext_courtyard/mus_ext_court_window_debris_a':
			position object_pos('e_glass_04')
			rotation object_rot('e_glass_04')
		Cause alert with 12 m radius.
		Run sequence 'new_sound'.
	sequence 'break_window_in':
		Run sequence 'window_break'.
		Enable body 'body_door_01'.
		Enable body 'body_door_02'.
		Enable body 'body_door_03'.
		Enable body 'body_door_04'.
		Enable body 'body_door_05'.
		Enable body 'body_door_06'.
		Enable body 'body_door_07'.
		Enable body 'body_door_08'.
		Enable body 'body_door_09'.
		Enable body 'body_door_10'.
		Enable body 'body_door_11'.
		Enable body 'body_door_12'.
		Enable body 'body_door_13'.
		Disable body 'animated_door_frame_a'.
		Disable body 'animated_door_glass_a'.
		Disable body 'animated_door_a'.
		Disable body 'animated_door_frame_b'.
		Disable body 'animated_door_glass_b'.
		Disable body 'animated_door_b'.
		Enable body 'body_door_13'.
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_push_in')),200,20).
		Show graphic_group 'grp_door_broken'.
		Run sequence 'crack_window_1'.
		Run sequence 'crack_window_2'.
		Run sequence 'break_window_1'.
		Run sequence 'break_window_2'.
		Disable body 'animated_door_frame_a'.
		Disable body 'animated_door_frame_b'.
		Disable object 'g_frame_a'.
		Disable object 'g_frame_b'.
		Disable object 'g_frame_dmg_a'.
		Disable object 'g_frame_dmg_b'.
		Disable object 'g_door_a'.
		Disable object 'g_door_b'.
		spawn_unit 'units/pd2_indiana/architecture/mus_ext_courtyard/mus_ext_court_window_debris_a':
			position object_pos('e_glass_03')
			rotation object_rot('e_glass_03')
		Cause alert with 12 m radius.
		spawn_unit 'units/pd2_indiana/architecture/mus_ext_courtyard/mus_ext_court_window_debris_a':
			position object_pos('e_glass_04')
			rotation object_rot('e_glass_04')
		Cause alert with 12 m radius.
		Run sequence 'new_sound'.
	sequence 'break_window_out':
		Run sequence 'window_break'.
		Enable body 'body_door_01'.
		Enable body 'body_door_02'.
		Enable body 'body_door_03'.
		Enable body 'body_door_04'.
		Enable body 'body_door_05'.
		Enable body 'body_door_06'.
		Enable body 'body_door_07'.
		Enable body 'body_door_08'.
		Enable body 'body_door_09'.
		Enable body 'body_door_10'.
		Enable body 'body_door_11'.
		Enable body 'body_door_12'.
		Enable body 'body_door_13'.
		Disable body 'animated_door_frame_a'.
		Disable body 'animated_door_glass_a'.
		Disable body 'animated_door_a'.
		Disable body 'animated_door_frame_b'.
		Disable body 'animated_door_glass_b'.
		Disable body 'animated_door_b'.
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_push_out')),200,20).
		Show graphic_group 'grp_door_broken'.
		Run sequence 'crack_window_1'.
		Run sequence 'crack_window_2'.
		Run sequence 'break_window_1'.
		Run sequence 'break_window_2'.
		Disable body 'animated_door_frame_a'.
		Disable body 'animated_door_frame_b'.
		Disable object 'g_frame_a'.
		Disable object 'g_frame_b'.
		Disable object 'g_frame_dmg_a'.
		Disable object 'g_frame_dmg_b'.
		Disable object 'g_door_a'.
		Disable object 'g_door_b'.
		spawn_unit 'units/pd2_indiana/architecture/mus_ext_courtyard/mus_ext_court_window_debris_a':
			position object_pos('e_glass_03')
			rotation object_rot('e_glass_03')
		Cause alert with 12 m radius.
		spawn_unit 'units/pd2_indiana/architecture/mus_ext_courtyard/mus_ext_court_window_debris_a':
			position object_pos('e_glass_04')
			rotation object_rot('e_glass_04')
		Cause alert with 12 m radius.
		Run sequence 'new_sound'.
	sequence 'window_break':
		TRIGGER TIMES 1
	sequence 'window_hurt':
		TRIGGER TIMES 1
	body 'animated_door_glass_a'
		Upon receiving 2 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'crack_window_1'.
		Upon receiving 4 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'break_window_1'.
	body 'animated_door_frame_a'
		Upon receiving 10 explosion damage or 30 melee damage, execute:
			Run sequence 'break_frame'.
	body 'animated_door_glass_b'
		Upon receiving 2 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'crack_window_2'.
		Upon receiving 4 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'break_window_2'.
	body 'animated_door_frame_b'
		Upon receiving 10 explosion damage or 30 melee damage, execute:
			Run sequence 'break_frame'.
	body 'animated_glass_03'
		Upon receiving 2 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'crack_window_3'.
		Upon receiving 4 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'break_window_3'.
	body 'animated_glass_04'
		Upon receiving 2 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'crack_window_4'.
		Upon receiving 4 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'break_window_4'.
