unit:
	sequence 'turn_on':
		EXECUTE ON STARTUP
		Enable object 'g_light_on'.
		Disable object 'g_light_off'.
		Enable light 'ls_bulb'.
		startup True
	sequence 'turn_off':
		Disable object 'g_light_on'.
		Enable object 'g_light_off'.
		Disable light 'ls_bulb'.
