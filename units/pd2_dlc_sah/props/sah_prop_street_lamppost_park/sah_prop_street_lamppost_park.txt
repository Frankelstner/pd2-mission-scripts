unit:
	sequence 'break':
		Disable body 'body_lamp'.
		Disable effect_spawner 'street_light_flare_big'.
		Disable object 'g_glass'.
		Enable object 'g_glass_broken'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_flare' )
			position v()
		Play audio 'ceiling_lamp_break' at 'e_flare'.
	body 'body_lamp'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break'.
