unit:
	sequence 'state_on':
		Enable object 'g_screen_on'.
		Disable object 'g_screen_off'.
	sequence 'state_off':
		Disable object 'g_screen_on'.
		Enable object 'g_screen_off'.
