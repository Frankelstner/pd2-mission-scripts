unit:
	sequence 'start_swing':
		EXECUTE ON STARTUP
		Run sequence 'anim_swing_0'..pick('1','2','3').
		startup True
	sequence 'anim_swing_01':
		Enable animation_group 'anim_swing':
			from 0/30
			loop True
			speed 1
			to 130/30
	sequence 'anim_swing_02':
		Enable animation_group 'anim_swing':
			from 134/30
			loop True
			speed 1
			to 263/30
	sequence 'anim_swing_03':
		Enable animation_group 'anim_swing':
			from 270/30
			loop True
			speed 1
			to 400/30
	sequence 'int_seq_break':
		Disable object 'g_glass'.
		Disable decal_mesh 'dm_glass'.
		Enable effect_spawner 'fire'.
		Enable effect_spawner 'glass'.
		Play audio 'window_small_shatter' at 'e_effect'.
		Disable light 'ls_spotlight_01'.
		Disable light 'ls_spotlight_02'.
		Disable light 'ls_spotlight_03'.
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break'.
