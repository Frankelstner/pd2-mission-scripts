unit:
	sequence 'anim_door_open':
		Enable animation_group 'anim':
			from 0/30
			speed 1
			to 20/30
		Play audio 'magnetic_door_open' at 'a_door'.
	sequence 'anim_door_close':
		Enable animation_group 'anim':
			from 20/30
			speed -1
			to 0/30
