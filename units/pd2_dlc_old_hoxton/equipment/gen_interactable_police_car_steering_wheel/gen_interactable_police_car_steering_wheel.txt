unit:
	sequence 'state_interaction_disabled':
		Disable interaction.
	sequence 'state_interaction_enabled':
		Enable interaction.
	sequence 'interact'.
