unit:
	sequence 'play_animation':
		Enable animation_group 'anim':
			from 4000/30
			speed -2
			to 0/30
		Run sequence 'play_animation_loop'. (DELAY 4000/2/30)
	sequence 'play_animation_loop':
		Enable animation_group 'anim':
			from 8000/30
			loop True
			speed -2
			to 0/30
	sequence 'reset_animation':
		Enable animation_group 'anim':
			from 0/30
			to 0/30
