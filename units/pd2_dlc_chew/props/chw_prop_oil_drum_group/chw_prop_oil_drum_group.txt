unit:
	sequence 'anim_barrel_fall_off':
		Enable animation_group 'anim_barrels':
			from 0/30
			to 200/30
		Run sequence 'int_barrel_1_dyn'. (DELAY 20/30)
		Run sequence 'int_barrel_2_dyn'. (DELAY 160/30)
		Run sequence 'int_barrel_3_dyn'. (DELAY 130/30)
		Run sequence 'int_barrel_4_dyn'. (DELAY 100/30)
		Run sequence 'int_barrel_5_dyn'. (DELAY 85/30)
		Run sequence 'int_barrel_6_dyn'. (DELAY 60/30)
		Run sequence 'int_barrel_7_dyn'. (DELAY 45/30)
		Run sequence 'int_barrel_8_dyn'. (DELAY 70/30)
		Run sequence 'int_barrel_9_dyn'. (DELAY 90/30)
		Run sequence 'int_barrel_10_dyn'. (DELAY 110/30)
		Run sequence 'int_barrel_11_dyn'. (DELAY 150/30)
		Run sequence 'int_barrel_12_dyn'. (DELAY 30/30)
		Run sequence 'int_barrel_13_dyn'. (DELAY 70/30)
		Run sequence 'int_barrel_14_dyn'. (DELAY 90/30)
		Run sequence 'int_barrel_15_dyn'. (DELAY 116/30)
	sequence 'int_barrel_1_dyn':
		Disable body 'static_body1'.
		Disable decal_mesh 'dm_metal_barrel_01'.
		Hide graphic_group 'barrel_01'.
		spawn_unit 'units/pd2_dlc_chew/props/chw_prop_oil_drum_group/chw_prop_oil_drum_group_debri':
			position object_pos('anim_barrel')
			rotation object_rot('anim_barrel')
	sequence 'int_barrel_2_dyn':
		Disable body 'static_body2'.
		Disable decal_mesh 'dm_metal_barrel_02'.
		Hide graphic_group 'barrel_02'.
		spawn_unit 'units/pd2_dlc_chew/props/chw_prop_oil_drum_group/chw_prop_oil_drum_group_debri':
			position object_pos('anim_barrel1')
			rotation object_rot('anim_barrel1')
	sequence 'int_barrel_3_dyn':
		Disable body 'static_body3'.
		Disable decal_mesh 'dm_metal_barrel_03'.
		Hide graphic_group 'barrel_03'.
		spawn_unit 'units/pd2_dlc_chew/props/chw_prop_oil_drum_group/chw_prop_oil_drum_group_debri':
			position object_pos('anim_barrel2')
			rotation object_rot('anim_barrel2')
	sequence 'int_barrel_4_dyn':
		Disable body 'static_body4'.
		Disable decal_mesh 'dm_metal_barrel_04'.
		Hide graphic_group 'barrel_04'.
		spawn_unit 'units/pd2_dlc_chew/props/chw_prop_oil_drum_group/chw_prop_oil_drum_group_debri':
			position object_pos('anim_barrel3')
			rotation object_rot('anim_barrel3')
	sequence 'int_barrel_5_dyn':
		Disable body 'static_body5'.
		Disable decal_mesh 'dm_metal_barrel_05'.
		Hide graphic_group 'barrel_05'.
		spawn_unit 'units/pd2_dlc_chew/props/chw_prop_oil_drum_group/chw_prop_oil_drum_group_debri':
			position object_pos('anim_barrel4')
			rotation object_rot('anim_barrel4')
	sequence 'int_barrel_6_dyn':
		Disable body 'static_body6'.
		Disable decal_mesh 'dm_metal_barrel_06'.
		Hide graphic_group 'barrel_06'.
		spawn_unit 'units/pd2_dlc_chew/props/chw_prop_oil_drum_group/chw_prop_oil_drum_group_debri':
			position object_pos('anim_barrel5')
			rotation object_rot('anim_barrel5')
	sequence 'int_barrel_7_dyn':
		Disable body 'static_body7'.
		Disable decal_mesh 'dm_metal_barrel_07'.
		Hide graphic_group 'barrel_07'.
		spawn_unit 'units/pd2_dlc_chew/props/chw_prop_oil_drum_group/chw_prop_oil_drum_group_debri':
			position object_pos('anim_barrel6')
			rotation object_rot('anim_barrel6')
	sequence 'int_barrel_8_dyn':
		Disable body 'static_body8'.
		Disable decal_mesh 'dm_metal_barrel_08'.
		Hide graphic_group 'barrel_08'.
		spawn_unit 'units/pd2_dlc_chew/props/chw_prop_oil_drum_group/chw_prop_oil_drum_group_debri':
			position object_pos('anim_barrel7')
			rotation object_rot('anim_barrel7')
	sequence 'int_barrel_9_dyn':
		Disable body 'static_body9'.
		Disable decal_mesh 'dm_metal_barrel_09'.
		Hide graphic_group 'barrel_09'.
		spawn_unit 'units/pd2_dlc_chew/props/chw_prop_oil_drum_group/chw_prop_oil_drum_group_debri':
			position object_pos('anim_barrel8')
			rotation object_rot('anim_barrel8')
	sequence 'int_barrel_10_dyn':
		Disable body 'static_body10'.
		Disable decal_mesh 'dm_metal_barrel_10'.
		Hide graphic_group 'barrel_10'.
		spawn_unit 'units/pd2_dlc_chew/props/chw_prop_oil_drum_group/chw_prop_oil_drum_group_debri':
			position object_pos('anim_barrel9')
			rotation object_rot('anim_barrel9')
	sequence 'int_barrel_11_dyn':
		Disable body 'static_body11'.
		Disable decal_mesh 'dm_metal_barrel_11'.
		Hide graphic_group 'barrel_11'.
		spawn_unit 'units/pd2_dlc_chew/props/chw_prop_oil_drum_group/chw_prop_oil_drum_group_debri':
			position object_pos('anim_barrel10')
			rotation object_rot('anim_barrel10')
	sequence 'int_barrel_12_dyn':
		Disable body 'static_body12'.
		Disable decal_mesh 'dm_metal_barrel_15'.
		Hide graphic_group 'barrel_15'.
		spawn_unit 'units/pd2_dlc_chew/props/chw_prop_oil_drum_group/chw_prop_oil_drum_group_debri':
			position object_pos('anim_barrel11')
			rotation object_rot('anim_barrel11')
	sequence 'int_barrel_13_dyn':
		Disable body 'static_body13'.
		Disable decal_mesh 'dm_metal_barrel_12'.
		Hide graphic_group 'barrel_12'.
		spawn_unit 'units/pd2_dlc_chew/props/chw_prop_oil_drum_group/chw_prop_oil_drum_group_debri':
			position object_pos('anim_barrel12')
			rotation object_rot('anim_barrel12')
	sequence 'int_barrel_14_dyn':
		Disable body 'static_body14'.
		Disable decal_mesh 'dm_metal_barrel_13'.
		Hide graphic_group 'barrel_13'.
		spawn_unit 'units/pd2_dlc_chew/props/chw_prop_oil_drum_group/chw_prop_oil_drum_group_debri':
			position object_pos('anim_barrel13')
			rotation object_rot('anim_barrel13')
	sequence 'int_barrel_15_dyn':
		Disable body 'static_body15'.
		Disable decal_mesh 'dm_metal_barrel_14'.
		Hide graphic_group 'barrel_14'.
		spawn_unit 'units/pd2_dlc_chew/props/chw_prop_oil_drum_group/chw_prop_oil_drum_group_debri':
			position object_pos('anim_barrel14')
			rotation object_rot('anim_barrel14')
	sequence 'show':
		Show graphic_group 'barrel_01'.
		Show graphic_group 'barrel_02'.
		Show graphic_group 'barrel_03'.
		Show graphic_group 'barrel_04'.
		Show graphic_group 'barrel_05'.
		Show graphic_group 'barrel_06'.
		Show graphic_group 'barrel_07'.
		Show graphic_group 'barrel_08'.
		Show graphic_group 'barrel_09'.
		Show graphic_group 'barrel_10'.
		Show graphic_group 'barrel_11'.
		Show graphic_group 'barrel_12'.
		Show graphic_group 'barrel_13'.
		Show graphic_group 'barrel_14'.
		Show graphic_group 'barrel_15'.
		Enable decal_mesh 'dm_metal_barrel_01'.
		Enable decal_mesh 'dm_metal_barrel_02'.
		Enable decal_mesh 'dm_metal_barrel_03'.
		Enable decal_mesh 'dm_metal_barrel_04'.
		Enable decal_mesh 'dm_metal_barrel_05'.
		Enable decal_mesh 'dm_metal_barrel_06'.
		Enable decal_mesh 'dm_metal_barrel_07'.
		Enable decal_mesh 'dm_metal_barrel_08'.
		Enable decal_mesh 'dm_metal_barrel_09'.
		Enable decal_mesh 'dm_metal_barrel_10'.
		Enable decal_mesh 'dm_metal_barrel_11'.
		Enable decal_mesh 'dm_metal_barrel_12'.
		Enable decal_mesh 'dm_metal_barrel_13'.
		Enable decal_mesh 'dm_metal_barrel_14'.
		Enable decal_mesh 'dm_metal_barrel_15'.
		Enable body 'static_body1'.
		Enable body 'static_body2'.
		Enable body 'static_body3'.
		Enable body 'static_body4'.
		Enable body 'static_body5'.
		Enable body 'static_body6'.
		Enable body 'static_body7'.
		Enable body 'static_body8'.
		Enable body 'static_body9'.
		Enable body 'static_body10'.
		Enable body 'static_body11'.
		Enable body 'static_body12'.
		Enable body 'static_body13'.
		Enable body 'static_body14'.
		Enable body 'static_body15'.
	sequence 'hide':
		Hide graphic_group 'barrel_01'.
		Hide graphic_group 'barrel_02'.
		Hide graphic_group 'barrel_03'.
		Hide graphic_group 'barrel_04'.
		Hide graphic_group 'barrel_05'.
		Hide graphic_group 'barrel_06'.
		Hide graphic_group 'barrel_07'.
		Hide graphic_group 'barrel_08'.
		Hide graphic_group 'barrel_09'.
		Hide graphic_group 'barrel_10'.
		Hide graphic_group 'barrel_11'.
		Hide graphic_group 'barrel_12'.
		Hide graphic_group 'barrel_13'.
		Hide graphic_group 'barrel_14'.
		Hide graphic_group 'barrel_15'.
		Disable decal_mesh 'dm_metal_barrel_01'.
		Disable decal_mesh 'dm_metal_barrel_02'.
		Disable decal_mesh 'dm_metal_barrel_03'.
		Disable decal_mesh 'dm_metal_barrel_04'.
		Disable decal_mesh 'dm_metal_barrel_05'.
		Disable decal_mesh 'dm_metal_barrel_06'.
		Disable decal_mesh 'dm_metal_barrel_07'.
		Disable decal_mesh 'dm_metal_barrel_08'.
		Disable decal_mesh 'dm_metal_barrel_09'.
		Disable decal_mesh 'dm_metal_barrel_10'.
		Disable decal_mesh 'dm_metal_barrel_11'.
		Disable decal_mesh 'dm_metal_barrel_12'.
		Disable decal_mesh 'dm_metal_barrel_13'.
		Disable decal_mesh 'dm_metal_barrel_14'.
		Disable decal_mesh 'dm_metal_barrel_15'.
		Disable body 'static_body1'.
		Disable body 'static_body2'.
		Disable body 'static_body3'.
		Disable body 'static_body4'.
		Disable body 'static_body5'.
		Disable body 'static_body6'.
		Disable body 'static_body7'.
		Disable body 'static_body8'.
		Disable body 'static_body9'.
		Disable body 'static_body10'.
		Disable body 'static_body11'.
		Disable body 'static_body12'.
		Disable body 'static_body13'.
		Disable body 'static_body14'.
		Disable body 'static_body15'.
