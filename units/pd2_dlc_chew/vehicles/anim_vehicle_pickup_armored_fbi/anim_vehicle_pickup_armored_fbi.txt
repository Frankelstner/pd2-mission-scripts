unit:
	sequence 'state_vis_hide':
		Disable body 'body_swat'.
		Disable body 'body_grenade'.
		Disable body 'bag_blocker'.
		Disable body 'body0'.
		Disable body 'window_front'.
		Disable body 'window_left_front'.
		Disable body 'window_right_front'.
		Disable body 'window_left_middle'.
		Disable body 'window_right_middle'.
		Disable body 'window_back'.
		Hide graphic_group 'grp_vehicle'.
		Disable effect_spawner 'chw_car_smoke'.
		Disable effect_spawner 'chw_car_smoke1'.
	sequence 'state_vis_show':
		Enable body 'body_swat'.
		Enable body 'body_grenade'.
		Enable body 'bag_blocker'.
		Enable body 'body0'.
		Enable body 'window_front'.
		Enable body 'window_left_front'.
		Enable body 'window_right_front'.
		Enable body 'window_left_middle'.
		Enable body 'window_right_middle'.
		Enable body 'window_back'.
		Show graphic_group 'grp_vehicle'.
		Enable effect_spawner 'chw_car_smoke'.
		Enable effect_spawner 'chw_car_smoke1'.
	sequence 'state_collision_hide':
		Disable body 'body_swat'.
		Disable body 'body_grenade'.
		Disable body 'bag_blocker'.
		Disable body 'body0'.
		Disable body 'window_front'.
		Disable body 'window_left_front'.
		Disable body 'window_right_front'.
		Disable body 'window_left_middle'.
		Disable body 'window_right_middle'.
		Disable body 'window_back'.
	sequence 'state_collision_show':
		Enable body 'body_swat'.
		Enable body 'bag_blocker'.
		Enable body 'body0'.
		Enable body 'window_front'.
		Enable body 'window_left_front'.
		Enable body 'window_right_front'.
		Enable body 'window_left_middle'.
		Enable body 'window_right_middle'.
		Enable body 'window_back'.
	sequence 'state_lights_on':
		Enable object 'g_cone_light'.
		Enable object 'g_front_lights'.
		Disable object 'g_glass_lights'.
		Enable light 'ls_left'.
		Enable light 'ls_right'.
	sequence 'state_lights_off':
		Disable object 'g_cone_light'.
		Disable object 'g_front_lights'.
		Enable object 'g_glass_lights'.
		Disable light 'ls_left'.
		Disable light 'ls_right'.
	sequence '__reset_animation__':
		Call function: base.move_to_stored_pos()
		Disable object 'g_glass_front_dmg'.
		Enable object 'g_glass_front'.
		Enable object 'g_glass_middle_r'.
		Disable object 'g_glass_middle_r_dmg'.
		Enable object 'g_glass_front_l'.
		Disable object 'g_glass_front_l_dmg'.
		Enable object 'g_glass_front_r'.
		Disable object 'g_glass_front_r_dmg'.
	sequence '__allow_sync_reset_animation__':
		Call function: base.allow_sync_stored_pos('true')
	sequence 'anim_car_chw_arrive_1':
		Run sequence 'state_vis_show'.
		Call function: base.store_current_pos()
		animation_redirect 'car_chw_arrive_1'.
		animation_redirect 'car_chw_wheel_loop'.
		Play audio 'car_loop_chew' at 'snd'.
		Run sequence 'done_car_anim'. (DELAY 1000/30)
		Enable effect_spawner 'chw_car_smoke'. (DELAY 1000/30)
		Enable effect_spawner 'chw_car_smoke1'. (DELAY 1000/30)
	sequence 'anim_car_chw_leave_1':
		animation_redirect 'car_chw_leave_1'.
		Run sequence 'state_vis_hide'. (DELAY 1000/30)
		Run sequence 'done_car_anim'. (DELAY 1000/30)
		Play audio 'car_loop_chew_stop' at 'snd'. (DELAY 1000/30)
	sequence 'anim_car_chw_move_fwd_15m':
		animation_redirect 'car_chw_move_fwd_15m'.
		animation_redirect 'car_chw_wheel_loop'.
		Run sequence 'done_car_anim'. (DELAY 100/30)
	sequence 'anim_car_chw_move_bwd_15m':
		animation_redirect 'car_chw_move_bwd_15m'.
		animation_redirect 'car_chw_wheel_loop'.
		Run sequence 'done_car_anim'. (DELAY 100/30)
	sequence 'anim_car_chw_crash':
		Run sequence 'int_seq_car_chw_'..pick('crash_1','crash_2','crash_6','crash_5').
		Enable effect_spawner 'chw_car_smoke_cloud'.
	sequence 'done_car_anim'.
	sequence 'done_car_crash'.
	sequence 'int_seq_car_chw_crash_1':
		animation_redirect 'car_chw_crash_1'.
		Play audio 'car_crash_chew_anim_01' at 'snd'.
		Run sequence 'state_vis_hide'. (DELAY 800/30)
		Run sequence 'done_car_crash'. (DELAY 800/30)
	sequence 'int_seq_car_chw_crash_2':
		animation_redirect 'car_chw_crash_2'.
		Play audio 'car_crash_chew_anim_02' at 'snd'.
		Run sequence 'state_vis_hide'. (DELAY 800/30)
		Run sequence 'done_car_crash'. (DELAY 800/30)
	sequence 'int_seq_car_chw_crash_3':
		animation_redirect 'car_chw_crash_3'.
		Play audio 'car_crash_chew_anim_03' at 'snd'.
		Run sequence 'state_vis_hide'. (DELAY 800/30)
		Run sequence 'done_car_crash'. (DELAY 800/30)
	sequence 'int_seq_car_chw_crash_4':
		animation_redirect 'car_chw_crash_4'.
		Play audio 'car_crash_chew_anim_04' at 'snd'.
		Run sequence 'state_vis_hide'. (DELAY 800/30)
		Run sequence 'done_car_crash'. (DELAY 800/30)
	sequence 'int_seq_car_chw_crash_5':
		animation_redirect 'car_chw_crash_5'.
		Play audio 'car_crash_chew_anim_05' at 'snd'.
		Run sequence 'state_vis_hide'. (DELAY 800/30)
		Run sequence 'done_car_crash'. (DELAY 800/30)
	sequence 'int_seq_car_chw_crash_6':
		animation_redirect 'car_chw_crash_6'.
		Play audio 'car_crash_chew_anim_06' at 'snd'.
		Run sequence 'state_vis_hide'. (DELAY 800/30)
		Run sequence 'done_car_crash'. (DELAY 800/30)
	body 'body_swat'
		Upon receiving 100 damage, execute:
			Run sequence 'done_swat_dead'.
	sequence 'done_swat_dead':
		Disable body 'body_swat'.
		Set damage damage to 0.
		Set explosion damage to 0.
	sequence 'kill_window_front_01':
		Enable object 'g_glass_front_dmg'.
		Disable object 'g_glass_front'.
		Set bullet damage to 0.
		Set explosion damage to 0.
	body 'window_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_front_01'.
	sequence 'kill_window_right_front_01':
		Enable object 'g_glass_front_r_dmg'.
		Disable object 'g_glass_front_r'.
	sequence 'kill_window_right_front_02':
		Disable body 'window_right_front'.
		Set explosion damage to 0.
		Set bullet damage to 0.
		Disable object 'g_glass_front_r_dmg'.
		Disable object 'g_glass_front_r'.
		Disable decal_mesh 'dm_glass_front_r'.
		Play audio 'window_small_shatter' at 'a_glass_front_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_front_r' )
			position v()
	body 'window_right_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_front_02'.
	sequence 'kill_window_left_front_01':
		Enable object 'g_glass_front_l_dmg'.
		Disable object 'g_glass_front_l'.
	sequence 'kill_window_left_front_02':
		Disable body 'window_left_front'.
		Set bullet damage to 0.
		Set explosion damage to 0.
		Disable object 'g_glass_front_l_dmg'.
		Disable object 'g_glass_front_l'.
		Disable decal_mesh 'dm_glass_front_l'.
		Play audio 'window_small_shatter' at 'a_glass_front_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_front_l' )
			position v()
	body 'window_left_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_front_02'.
	sequence 'kill_window_right_middle_01':
		Enable object 'g_glass_middle_r_dmg'.
		Disable object 'g_glass_middle_r'.
	sequence 'kill_window_right_middle_02':
		Disable body 'window_right_middle'.
		Set explosion damage to 0.
		Set bullet damage to 0.
		Disable object 'g_glass_middle_r_dmg'.
		Disable object 'g_glass_middle_r'.
		Disable decal_mesh 'dm_glass_middle_r'.
		Play audio 'window_small_shatter' at 'a_glass_middle_r'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_middle_r' )
			position v()
	body 'window_right_middle'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_right_middle_02'.
	sequence 'kill_window_left_middle_01':
		Enable object 'g_glass_middle_l_dmg'.
		Disable object 'g_glass_middle_l'.
	sequence 'kill_window_left_middle_02':
		Disable body 'window_left_middle'.
		Set explosion damage to 0.
		Set bullet damage to 0.
		Disable object 'g_glass_middle_l_dmg'.
		Disable object 'g_glass_middle_l'.
		Disable decal_mesh 'dm_glass_middle_l'.
		Play audio 'window_small_shatter' at 'a_glass_middle_l'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'a_glass_middle_l' )
			position v()
	body 'window_left_middle'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'kill_window_left_middle_02'.
	sequence 'anim_spin_loop':
		Run sequence 'state_vis_show'.
		Call function: base.store_current_pos()
		animation_redirect 'spin_loop'.
		animation_redirect 'car_chw_wheel_loop'.
		Run sequence 'done_car_anim'. (DELAY 1000/30)
