unit:
	sequence 'light_on':
		material_config 'units/pd2_indiana/architecture/mus_int_skylight/mus_int_exibit_skylight_il'.
	sequence 'light_off':
		material_config 'units/pd2_indiana/architecture/mus_int_skylight/mus_int_exibit_skylight'.
	sequence 'break_01':
		Disable body 'glass_body_01'.
		Disable object 'g_glass_01'.
		Disable decal_mesh 'dm_glass_01'.
		Enable object 'g_glass_dmg_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_01' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_01'.
	body 'glass_body_01'
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_01'.
	sequence 'break_02':
		Disable body 'glass_body_02'.
		Disable object 'g_glass_02'.
		Disable decal_mesh 'dm_glass_02'.
		Enable object 'g_glass_dmg_02'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_02' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_02'.
	body 'glass_body_02'
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_02'.
	sequence 'break_03':
		Disable body 'glass_body_03'.
		Disable object 'g_glass_03'.
		Disable decal_mesh 'dm_glass_03'.
		Enable object 'g_glass_dmg_03'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_03' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_03'.
	body 'glass_body_03'
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_03'.
	sequence 'break_04':
		Disable body 'glass_body_04'.
		Disable object 'g_glass_04'.
		Disable decal_mesh 'dm_glass_04'.
		Enable object 'g_glass_dmg_04'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_04' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_04'.
	body 'glass_body_04'
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_04'.
	sequence 'break_05':
		Disable body 'glass_body_05'.
		Disable object 'g_glass_05'.
		Disable decal_mesh 'dm_glass_05'.
		Enable object 'g_glass_dmg_05'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_05' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_05'.
	body 'glass_body_05'
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_05'.
	sequence 'break_06':
		Disable body 'glass_body_06'.
		Disable object 'g_glass_06'.
		Disable decal_mesh 'dm_glass_06'.
		Enable object 'g_glass_dmg_06'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_06' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_06'.
	body 'glass_body_06'
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_06'.
	sequence 'break_07':
		Disable body 'glass_body_07'.
		Disable object 'g_glass_07'.
		Disable decal_mesh 'dm_glass_07'.
		Enable object 'g_glass_dmg_07'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_07' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_07'.
	body 'glass_body_07'
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_07'.
	sequence 'break_08':
		Disable body 'glass_body_08'.
		Disable object 'g_glass_08'.
		Disable decal_mesh 'dm_glass_08'.
		Enable object 'g_glass_dmg_08'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_08' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_08'.
	body 'glass_body_08'
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_08'.
	sequence 'break_09':
		Disable body 'glass_body_09'.
		Disable object 'g_glass_09'.
		Disable decal_mesh 'dm_glass_09'.
		Enable object 'g_glass_dmg_09'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_09' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_09'.
	body 'glass_body_09'
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_09'.
	sequence 'break_10':
		Disable body 'glass_body_10'.
		Disable object 'g_glass_10'.
		Disable decal_mesh 'dm_glass_10'.
		Enable object 'g_glass_dmg_10'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_10' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_10'.
	body 'glass_body_10'
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_10'.
	sequence 'break_11':
		Disable body 'glass_body_11'.
		Disable object 'g_glass_11'.
		Disable decal_mesh 'dm_glass_11'.
		Enable object 'g_glass_dmg_11'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_11' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_11'.
	body 'glass_body_11'
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_11'.
	sequence 'break_12':
		Disable body 'glass_body_12'.
		Disable object 'g_glass_12'.
		Disable decal_mesh 'dm_glass_12'.
		Enable object 'g_glass_dmg_12'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_12' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_12'.
	body 'glass_body_12'
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_12'.
