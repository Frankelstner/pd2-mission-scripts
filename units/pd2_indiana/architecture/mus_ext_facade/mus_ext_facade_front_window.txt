unit:
	sequence 'break_01':
		Disable body 'glass_body_01'.
		Disable object 'g_glass_01'.
		Disable decal_mesh 'dm_glass_breakable_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_01' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_01'.
		Cause alert with 12 m radius.
	body 'glass_body_01'
		Upon receiving 3 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_01'.
	sequence 'break_02':
		Disable body 'glass_body_02'.
		Disable object 'g_glass_02'.
		Disable decal_mesh 'dm_glass_breakable_02'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_02' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_02'.
		Cause alert with 12 m radius.
	body 'glass_body_02'
		Upon receiving 3 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_02'.
	sequence 'break_03':
		Disable body 'glass_body_03'.
		Disable object 'g_glass_03'.
		Disable decal_mesh 'dm_glass_breakable_03'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_03' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_03'.
		Cause alert with 12 m radius.
	body 'glass_body_03'
		Upon receiving 3 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_03'.
	sequence 'break_04':
		Disable body 'glass_body_04'.
		Disable object 'g_glass_04'.
		Disable decal_mesh 'dm_glass_breakable_04'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_04' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_04'.
		Cause alert with 12 m radius.
	body 'glass_body_04'
		Upon receiving 3 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_04'.
	sequence 'break_05':
		Disable body 'glass_body_05'.
		Disable object 'g_glass_05'.
		Disable decal_mesh 'dm_glass_breakable_05'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_05' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_05'.
		Cause alert with 12 m radius.
	body 'glass_body_05'
		Upon receiving 3 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_05'.
	sequence 'break_06':
		Disable body 'glass_body_06'.
		Disable object 'g_glass_06'.
		Disable decal_mesh 'dm_glass_breakable_06'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_06' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_06'.
		Cause alert with 12 m radius.
	body 'glass_body_06'
		Upon receiving 3 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_06'.
	sequence 'break_07':
		Disable body 'glass_body_07'.
		Disable object 'g_glass_07'.
		Disable decal_mesh 'dm_glass_breakable_07'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_07' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_07'.
		Cause alert with 12 m radius.
	body 'glass_body_07'
		Upon receiving 3 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break_07'.
