unit:
	sequence 'crack_window_1':
		TRIGGER TIMES 1
		Disable object 'g_windows_01'.
		Enable object 'g_windows_01_dmg'.
		Cause alert with 12 m radius.
	sequence 'crack_window_2':
		TRIGGER TIMES 1
		Disable object 'g_windows_02'.
		Enable object 'g_windows_02_dmg'.
		Cause alert with 12 m radius.
	sequence 'crack_window_3':
		TRIGGER TIMES 1
		Disable object 'g_windows_03'.
		Enable object 'g_windows_03_dmg'.
		Cause alert with 12 m radius.
	sequence 'crack_window_4':
		TRIGGER TIMES 1
		Disable object 'g_windows_04'.
		Enable object 'g_windows_04_dmg'.
		Cause alert with 12 m radius.
	sequence 'break_window_1':
		TRIGGER TIMES 1
		Disable body 'static_glass_01'.
		Disable object 'g_windows_01_dmg'.
		Disable decal_mesh 'dm_glass_breakable_01'.
		Play audio 'window_large_shatter' at 'e_glass_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_01')
			position v()
		Cause alert with 12 m radius.
	sequence 'break_window_2':
		TRIGGER TIMES 1
		Disable body 'static_glass_02'.
		Disable object 'g_windows_02_dmg'.
		Disable decal_mesh 'dm_glass_breakable_02'.
		Play audio 'window_large_shatter' at 'e_glass_02'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_02')
			position v()
		Cause alert with 12 m radius.
	sequence 'break_window_3':
		TRIGGER TIMES 1
		Disable body 'static_glass_03'.
		Disable object 'g_windows_03_dmg'.
		Disable decal_mesh 'dm_glass_breakable_03'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_03')
			position v()
		Play audio 'window_large_shatter' at 'e_glass_03'.
		Cause alert with 12 m radius.
	sequence 'break_window_4':
		TRIGGER TIMES 1
		Disable body 'static_glass_04'.
		Disable object 'g_windows_04'.
		Disable object 'g_windows_04_dmg'.
		Disable decal_mesh 'dm_glass_breakable_04'.
		Play audio 'window_large_shatter' at 'e_glass_04'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_04')
			position v()
		Cause alert with 12 m radius.
	sequence 'break_frame':
		TRIGGER TIMES 1
		Run sequence 'crack_window_3'.
		Run sequence 'break_window_3'.
		Disable body 'static_frame_melee'.
		Disable object 'g_frame'.
		Enable object 'g_frame_dmg'.
		spawn_unit 'units/pd2_indiana/architecture/mus_ext_courtyard/mus_ext_court_window_debris_a':
			position object_pos('e_glass_03')
			rotation object_rot('e_glass_03')
		Cause alert with 12 m radius.
	body 'static_glass_01'
		Upon receiving 2 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'crack_window_1'.
		Upon receiving 4 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'break_window_1'.
	body 'static_glass_02'
		Upon receiving 2 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'crack_window_2'.
		Upon receiving 4 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'break_window_2'.
	body 'static_glass_03'
		Upon receiving 2 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'crack_window_3'.
		Upon receiving 4 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'break_window_3'.
	body 'static_glass_04'
		Upon receiving 2 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'crack_window_4'.
		Upon receiving 4 bullet hits or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'break_window_4'.
