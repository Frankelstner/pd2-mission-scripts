unit:
	sequence 'state_interaction_enabled':
		Enable interaction.
		Show graphic_group 'icon'.
	sequence 'state_interaction_disabled':
		Disable interaction.
		Hide graphic_group 'icon'.
	sequence 'interact':
		Add attention/detection preset 'prop_state_civ_ene_ntl' to 'interact' (alarm reason: 'fire').
		Hide graphic_group 'icon'.
		Play audio 'thermite_gate_burn' at 'snd'.
		effect 'effects/payday2/environment/parts/flashing_light':
			parent object( 'rp_mus_interactable_grate_small' )
			position v()
		effect 'effects/payday2/environment/small_thermal_burner':
			parent object( 'e_thrm_1' )
			position v()
		effect 'effects/payday2/environment/small_thermal_burner': (DELAY 10/30)
			parent object( 'e_thrm_2' )
			position v()
		effect 'effects/payday2/environment/small_thermal_burner': (DELAY 20/30)
			parent object( 'e_thrm_3' )
			position v()
		effect 'effects/payday2/environment/small_thermal_burner': (DELAY 30/30)
			parent object( 'e_thrm_4' )
			position v()
		effect 'effects/payday2/environment/small_thermal_burner': (DELAY 40/30)
			parent object( 'e_thrm_5' )
			position v()
		effect 'effects/payday2/environment/small_thermal_burner': (DELAY 50/30)
			parent object( 'e_thrm_6' )
			position v()
		effect 'effects/payday2/environment/small_thermal_burner': (DELAY 60/30)
			parent object( 'e_thrm_7' )
			position v()
		effect 'effects/payday2/environment/small_thermal_burner': (DELAY 70/30)
			parent object( 'e_thrm_8' )
			position v()
		effect 'effects/payday2/environment/small_thermal_burner': (DELAY 80/30)
			parent object( 'e_thrm_9' )
			position v()
		effect 'effects/payday2/environment/small_thermal_burner': (DELAY 90/30)
			parent object( 'e_thrm_10' )
			position v()
		Disable body 'body_static'. (DELAY 300/30)
		Disable object 'g_g'. (DELAY 300/30)
		Run sequence 'done_thermite_burn'. (DELAY 300/30)
		Remove attention/detection preset: 'prop_state_civ_ene_ntl' (DELAY 300/30)
		spawn_unit 'units/pd2_indiana/equipment/mus_interactable_grate_small/spawn_grate_small_debris': (DELAY 300/30)
			position object_pos('rp_mus_interactable_grate_small')
			rotation object_rot('rp_mus_interactable_grate_small')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('rp_mus_interactable_grate_small') ),200,100). (DELAY 302/30)
	sequence 'done_thermite_burn'.
