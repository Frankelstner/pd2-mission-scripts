unit:
	sequence 'state_door_open':
		Disable interaction.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_door':
			from 40/30
			speed 0
			to 40/30
	sequence 'state_door_close':
		Enable interaction.
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_door':
			from 0/30
			speed 0
			to 0/30
	sequence 'state_vis_door_hide':
		Disable interaction.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Disable body 'body_frame'.
		Disable body 'body_door'.
		Hide graphic_group 'grp_doors'.
		Run sequence 'deactivate_door'.
	sequence 'state_vis_door_show':
		Enable interaction.
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
		Enable body 'body_frame'.
		Enable body 'body_door'.
		Show graphic_group 'grp_doors'.
	sequence 'anim_open_door':
		Enable animation_group 'anim_door':
			from 0/30
			to 20/30
		Play audio 'generic_door_metal_open' at 'anim_door'.
	sequence 'anim_close_door':
		Enable interaction.
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_door':
			from 20/30
			speed -1
			to 0/30
		Run sequence 'activate_door'. (DELAY 20/30)
	body 'body_hitbox_door_handle_in'
		Upon receiving 225 saw damage, execute:
			Run sequence 'int_seq_saw_in'.
	body 'body_hitbox_door_handle_out'
		Upon receiving 225 saw damage, execute:
			Run sequence 'int_seq_saw_out'.
	sequence 'int_seq_open':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
	sequence 'int_seq_breach_common':
		Disable interaction.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Run sequence 'deactivate_door'.
	sequence 'int_seq_saw_in':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
	sequence 'int_seq_saw_out':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
	sequence 'int_seq_explosion_in':
		Run sequence 'int_seq_breach_common'.
		Play audio 'c4_explode_metal' at 'a_shp_charge_1'.
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'a_shp_charge_1' )
			position v()
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'a_shp_charge_2' )
			position v()
		spawn_unit 'units/pd2_indiana/equipment/mus_interactable_door_reinforced/spawn_debris_door_reinforced':
			position object_pos('rp_mus_interactable_door_reinforced_2')
			rotation object_rot('rp_mus_interactable_door_reinforced_2')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('a_push_in') ),200,400).
		Run sequence 'done_exploded'.
		Disable body 'body_door'.
		Disable object 'g_door1'.
		Enable object 'g_door_expload'.
		Run sequence 'done_opened'.
	sequence 'int_seq_explosion_out':
		Run sequence 'int_seq_breach_common'.
		Play audio 'c4_explode_metal' at 'a_shp_charge_1'.
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'a_shp_charge_1' )
			position v()
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'a_shp_charge_2' )
			position v()
		spawn_unit 'units/pd2_indiana/equipment/mus_interactable_door_reinforced/spawn_debris_door_reinforced':
			position object_pos('rp_mus_interactable_door_reinforced_2')
			rotation object_rot('rp_mus_interactable_door_reinforced_2')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('a_push_out') ),200,400).
		Run sequence 'done_exploded'.
		Disable body 'body_door'.
		Disable object 'g_door1'.
		Enable object 'g_door_expload'.
		Run sequence 'done_opened'.
	sequence 'interact':
		Run sequence 'int_seq_open'.
	sequence 'activate_door':
		Call function: base.activate()
		Show graphic_group 'grp_icon'.
		Enable interaction.
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
	sequence 'deactivate_door':
		Call function: base.deactivate()
		Hide graphic_group 'grp_icon'.
		Disable interaction.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
	sequence 'explode_door':
		Run sequence 'int_seq_explosion_in'.
	sequence 'power_off':
		Call function: base.set_powered(False)
	sequence 'power_on':
		Call function: base.set_powered(True)
	sequence 'turn_off':
		Call function: base.set_on(False)
	sequence 'turn_on':
		Call function: base.set_on(True)
	sequence 'door_opened'.
	sequence 'door_closed'.
	sequence 'drill_placed'.
	sequence 'drill_jammed'.
	sequence 'drill_resumed'.
	sequence 'drill_power_off'.
	sequence 'drill_power_on'.
	sequence 'drill_completed'.
	sequence 'all_drill_placed'.
	sequence 'c4_placed'.
	sequence 'c4_completed'.
	sequence 'all_c4_placed'.
	sequence 'open_door':
		Run sequence 'int_seq_open'.
		Hide graphic_group 'grp_icon'.
	sequence 'done_exploded'.
	sequence 'done_opened'.
