unit:
	sequence 'bullet_damage':
		Show graphic_group 'grp_damage'.
		Disable object 'g_body_lod0'.
		Disable object 'g_body_lod1'.
		Enable object 'g_debri_body_lod0'.
		Enable object 'g_debri_body_lod1'.
		Enable decal_mesh 'dm_dmg_stone'.
		Disable decal_mesh 'dm_stone'.
		Disable body 'body_static_clean'.
		Enable body 'body_static_dmg'.
		Enable body 'body_dmg_01_static'.
		Enable body 'body_dmg_02_static'.
		Enable body 'body_dmg_03_static'.
		Enable body 'body_dmg_04_static'.
		Enable body 'body_dmg_05_static'.
		Enable body 'body_dmg_06_static'.
		Enable body 'body_dmg_07_static'.
		Enable body 'body_dmg_08_static'.
		Enable body 'body_dmg_09_static'.
		Enable body 'body_dmg_10_static'.
		Enable body 'body_dmg_11_static'.
	sequence 'grenade_damage':
		Show graphic_group 'grp_damage'.
		Disable object 'g_body_lod0'.
		Disable object 'g_body_lod1'.
		Enable object 'g_debri_body_lod0'.
		Enable object 'g_debri_body_lod1'.
		Enable decal_mesh 'dm_dmg_stone'.
		Disable decal_mesh 'dm_stone'.
		Disable body 'body_static_clean'.
		Enable body 'body_static_dmg'.
		Run sequence 'dmg_01_break'.
		Run sequence 'dmg_02_break'.
		Run sequence 'dmg_03_break'.
		Run sequence 'dmg_04_break'.
		Run sequence 'dmg_05_break'.
		Run sequence 'dmg_06_break'.
		Run sequence 'dmg_07_break'.
		Run sequence 'dmg_08_break'.
		Run sequence 'dmg_09_break'.
		Run sequence 'dmg_10_break'.
		Run sequence 'dmg_11_break'.
		Run sequence 'run_sound'.
	sequence 'run_sound':
		Play audio 'statue_break' at 'sound'.
		Play audio 'statue_break_debris' at 'sound'.
	sequence 'dmg_01_break':
		TRIGGER TIMES 1
		Disable body 'body_dmg_01_static'.
		Disable object 'g_debri_001_lod0'.
		Disable object 'g_debri_001_lod1'.
		spawn_unit 'units/pd2_indiana/props/mus_prop_kariatyde_pillar_statue/spawn_piece_01':
			position object_pos('e_pivot_01')
			rotation object_rot('e_pivot_01')
		effect 'effects/payday2/particles/destruction/des_statue_man':
			parent object( 'e_pivot_01' )
			position v()
	sequence 'dmg_02_break':
		TRIGGER TIMES 1
		Disable body 'body_dmg_02_static'.
		Disable object 'g_debris_002_lod0'.
		Disable object 'g_debris_002_lod1'.
		spawn_unit 'units/pd2_indiana/props/mus_prop_kariatyde_pillar_statue/spawn_piece_02':
			position object_pos('e_pivot_02')
			rotation object_rot('e_pivot_02')
		effect 'effects/payday2/particles/destruction/des_statue_man':
			parent object( 'e_pivot_02' )
			position v()
	sequence 'dmg_03_break':
		TRIGGER TIMES 1
		Disable body 'body_dmg_03_static'.
		Disable object 'g_debri_003_lod0'.
		Disable object 'g_debri_003_lod1'.
		spawn_unit 'units/pd2_indiana/props/mus_prop_kariatyde_pillar_statue/spawn_piece_03':
			position object_pos('e_pivot_03')
			rotation object_rot('e_pivot_03')
		effect 'effects/payday2/particles/destruction/des_statue_man':
			parent object( 'e_pivot_03' )
			position v()
	sequence 'dmg_04_break':
		TRIGGER TIMES 1
		Disable body 'body_dmg_04_static'.
		Disable object 'g_debri_004_lod0'.
		Disable object 'g_debri_004_lod1'.
		spawn_unit 'units/pd2_indiana/props/mus_prop_kariatyde_pillar_statue/spawn_piece_04':
			position object_pos('e_pivot_04')
			rotation object_rot('e_pivot_04')
		effect 'effects/payday2/particles/destruction/des_statue_man':
			parent object( 'e_pivot_04' )
			position v()
	sequence 'dmg_05_break':
		TRIGGER TIMES 1
		Disable body 'body_dmg_05_static'.
		Disable object 'g_debri_005_lod0'.
		Disable object 'g_debri_005_lod1'.
		spawn_unit 'units/pd2_indiana/props/mus_prop_kariatyde_pillar_statue/spawn_piece_05':
			position object_pos('e_pivot_05')
			rotation object_rot('e_pivot_05')
		effect 'effects/payday2/particles/destruction/des_statue_man':
			parent object( 'e_pivot_05' )
			position v()
	sequence 'dmg_06_break':
		TRIGGER TIMES 1
		Disable body 'body_dmg_06_static'.
		Disable object 'g_debri_006_lod0'.
		Disable object 'g_debri_006_lod1'.
		spawn_unit 'units/pd2_indiana/props/mus_prop_kariatyde_pillar_statue/spawn_piece_06':
			position object_pos('e_pivot_06')
			rotation object_rot('e_pivot_06')
		effect 'effects/payday2/particles/destruction/des_statue_man':
			parent object( 'e_pivot_06' )
			position v()
	sequence 'dmg_07_break':
		TRIGGER TIMES 1
		Disable body 'body_dmg_07_static'.
		Disable object 'g_debri_007_lod0'.
		Disable object 'g_debri_007_lod1'.
		spawn_unit 'units/pd2_indiana/props/mus_prop_kariatyde_pillar_statue/spawn_piece_06':
			position object_pos('e_pivot_06')
			rotation object_rot('e_pivot_06')
		effect 'effects/payday2/particles/destruction/des_statue_man':
			parent object( 'e_pivot_07' )
			position v()
	sequence 'dmg_08_break':
		TRIGGER TIMES 1
		Disable body 'body_dmg_08_static'.
		Disable object 'g_debri_008_lod0'.
		Disable object 'g_debri_008_lod1'.
		spawn_unit 'units/pd2_indiana/props/mus_prop_kariatyde_pillar_statue/spawn_piece_08':
			position object_pos('e_pivot_08')
			rotation object_rot('e_pivot_08')
		effect 'effects/payday2/particles/destruction/des_statue_man':
			parent object( 'e_pivot_08' )
			position v()
	sequence 'dmg_09_break':
		TRIGGER TIMES 1
		Disable body 'body_dmg_09_static'.
		Disable object 'g_debri_009_lod0'.
		Disable object 'g_debri_009_lod1'.
		spawn_unit 'units/pd2_indiana/props/mus_prop_kariatyde_pillar_statue/spawn_piece_09':
			position object_pos('e_pivot_09')
			rotation object_rot('e_pivot_09')
		effect 'effects/payday2/particles/destruction/des_statue_man':
			parent object( 'e_pivot_09' )
			position v()
	sequence 'dmg_10_break':
		TRIGGER TIMES 1
		Disable body 'body_dmg_10_static'.
		Disable object 'g_debri_010_lod0'.
		Disable object 'g_debri_010_lod1'.
		spawn_unit 'units/pd2_indiana/props/mus_prop_kariatyde_pillar_statue/spawn_piece_10':
			position object_pos('e_pivot_010')
			rotation object_rot('e_pivot_010')
		effect 'effects/payday2/particles/destruction/des_statue_man':
			parent object( 'e_pivot_010' )
			position v()
	sequence 'dmg_11_break':
		TRIGGER TIMES 1
		Disable body 'body_dmg_11_static'.
		Disable object 'g_debri_011_lod0'.
		Disable object 'g_debri_011_lod1'.
		spawn_unit 'units/pd2_indiana/props/mus_prop_kariatyde_pillar_statue/spawn_piece_11':
			position object_pos('e_pivot_011')
			rotation object_rot('e_pivot_011')
		effect 'effects/payday2/particles/destruction/des_statue_man':
			parent object( 'e_pivot_011' )
			position v()
	body 'body_static_clean'
		Upon receiving 1 bullet hit or 10 melee damage, execute:
			Run sequence 'bullet_damage'.
		Upon receiving 10 explosion damage, execute:
			Run sequence 'grenade_damage'.
			Run sequence 'run_sound'.
	body 'body_dmg_01_static'
		Upon receiving 1 bullet hit or 10 collision damage or 20 explosion damage or 10 melee damage, execute:
			Run sequence 'dmg_01_break'.
			Run sequence 'run_sound'.
	body 'body_dmg_02_static'
		Upon receiving 1 bullet hit or 10 collision damage or 20 explosion damage or 10 melee damage, execute:
			Run sequence 'dmg_02_break'.
			Run sequence 'run_sound'.
	body 'body_dmg_03_static'
		Upon receiving 1 bullet hit or 10 collision damage or 20 explosion damage or 10 melee damage, execute:
			Run sequence 'dmg_03_break'.
			Run sequence 'run_sound'.
	body 'body_dmg_04_static'
		Upon receiving 1 bullet hit or 10 collision damage or 20 explosion damage or 10 melee damage, execute:
			Run sequence 'dmg_04_break'.
			Run sequence 'run_sound'.
	body 'body_dmg_05_static'
		Upon receiving 1 bullet hit or 10 collision damage or 20 explosion damage or 10 melee damage, execute:
			Run sequence 'dmg_05_break'.
			Run sequence 'run_sound'.
	body 'body_dmg_06_static'
		Upon receiving 1 bullet hit or 10 collision damage or 20 explosion damage or 10 melee damage, execute:
			Run sequence 'dmg_06_break'.
			Run sequence 'run_sound'.
	body 'body_dmg_07_static'
		Upon receiving 1 bullet hit or 10 collision damage or 20 explosion damage or 10 melee damage, execute:
			Run sequence 'dmg_07_break'.
			Run sequence 'run_sound'.
	body 'body_dmg_08_static'
		Upon receiving 1 bullet hit or 10 collision damage or 20 explosion damage or 10 melee damage, execute:
			Run sequence 'dmg_08_break'.
			Run sequence 'run_sound'.
	body 'body_dmg_09_static'
		Upon receiving 1 bullet hit or 10 collision damage or 20 explosion damage or 10 melee damage, execute:
			Run sequence 'dmg_09_break'.
			Run sequence 'run_sound'.
	body 'body_dmg_10_static'
		Upon receiving 1 bullet hit or 10 collision damage or 20 explosion damage or 10 melee damage, execute:
			Run sequence 'dmg_10_break'.
			Run sequence 'run_sound'.
	body 'body_dmg_11_static'
		Upon receiving 1 bullet hit or 10 collision damage or 20 explosion damage or 10 melee damage, execute:
			Run sequence 'dmg_11_break'.
			Run sequence 'run_sound'.
