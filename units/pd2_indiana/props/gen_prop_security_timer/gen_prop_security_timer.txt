unit:
	sequence 'done_hit'.
	sequence 'state_vis_show':
		Enable body 'body0'.
		Enable decal_mesh 'dm_plastic'.
		Enable object 'g_g'.
		Call function: digital_gui.set_visible(True)
	sequence 'state_vis_hide':
		Disable body 'body0'.
		Disable decal_mesh 'dm_plastic'.
		Disable object 'g_g'.
		Call function: digital_gui.set_visible(False)
	sequence 'red_on_black':
		Call function: digital_gui.set_color_type('red')
		Call function: digital_gui.set_bg_color_type()
	sequence 'green_on_black':
		Call function: digital_gui.set_color_type('green')
		Call function: digital_gui.set_bg_color_type()
	sequence 'blue_on_black':
		Call function: digital_gui.set_color_type('blue')
		Call function: digital_gui.set_bg_color_type()
	sequence 'yellow_on_black':
		Call function: digital_gui.set_color_type('yellow')
		Call function: digital_gui.set_bg_color_type()
	sequence 'orange_on_black':
		Call function: digital_gui.set_color_type('orange')
		Call function: digital_gui.set_bg_color_type()
	sequence 'light_red_on_black':
		Call function: digital_gui.set_color_type('light_red')
		Call function: digital_gui.set_bg_color_type()
	sequence 'light_green_on_black':
		Call function: digital_gui.set_color_type('light_green')
		Call function: digital_gui.set_bg_color_type()
	sequence 'light_blue_on_black':
		Call function: digital_gui.set_color_type('light_blue')
		Call function: digital_gui.set_bg_color_type()
	sequence 'light_yellow_on_black':
		Call function: digital_gui.set_color_type('light_yellow')
		Call function: digital_gui.set_bg_color_type()
	sequence 'light_orange_on_black':
		Call function: digital_gui.set_color_type('light_orange')
		Call function: digital_gui.set_bg_color_type()
	sequence 'black_on_light_red':
		Call function: digital_gui.set_color_type('black')
		Call function: digital_gui.set_bg_color_type('light_red')
	sequence 'black_on_light_green':
		Call function: digital_gui.set_color_type('black')
		Call function: digital_gui.set_bg_color_type('light_green')
	sequence 'black_on_light_blue':
		Call function: digital_gui.set_color_type('black')
		Call function: digital_gui.set_bg_color_type('light_blue')
	sequence 'black_on_light_yellow':
		Call function: digital_gui.set_color_type('black')
		Call function: digital_gui.set_bg_color_type('light_yellow')
	sequence 'black_on_light_orange':
		Call function: digital_gui.set_color_type('black')
		Call function: digital_gui.set_bg_color_type('light_orange')
	sequence 'number_increase':
		Call function: digital_gui.number_increase()
	sequence 'number_decrease':
		Call function: digital_gui.number_decrease()
	sequence 'timer_start_count_up':
		Call function: digital_gui.timer_start_count_up()
	sequence 'timer_start_count_down':
		Call function: digital_gui.timer_start_count_down()
	sequence 'timer_pause':
		Call function: digital_gui.timer_pause()
	sequence 'timer_resume':
		Call function: digital_gui.timer_resume()
	sequence 'timer_reach_zero'.
	body 'body0'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_dmg_1'.
	sequence 'int_seq_dmg_1':
		Enable object 'g_g'.
