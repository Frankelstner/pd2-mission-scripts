unit:
	sequence 'bust1':
		TRIGGER TIMES 1
		Run sequence 'bust_break1'.
	sequence 'bust_dynamic1':
		TRIGGER TIMES 1
		Disable body 'bust'.
		Enable body 'bust_dynamic'.
	sequence 'bust_break1':
		TRIGGER TIMES 1
		Disable body 'bust'.
		Disable body 'bust_static'.
		Enable body 'bust_dynamic'.
		Enable body 'bust_dmg_1'.
		Enable body 'bust_dmg_2'.
		Enable body 'bust_dmg_3'.
		Enable body 'bust_dmg_4'.
		Enable body 'bust_dmg_5'.
		effect 'effects/payday2/particles/destruction/dest_statue_bust':
			parent object( 'e_pos' )
			position v()
		Disable object 'g_aristotle_base'.
		Disable decal_mesh 'dm_head'.
		Enable object 'g_dmg_head_01'.
		Enable object 'g_dmg_head_02'.
		Enable object 'g_dmg_torso_01'.
		Enable object 'g_dmg_torso_02'.
		Enable object 'g_dmg_mouth_01'.
		Enable object 'g_dmg_chin_01'.
		Play audio 'pot_large_shatter' at 'e_pos'.
		Call function World:play_physic_effect('physic_effects/push_sphere_unit',dest_unit,10,4.25,pos,dir).
	body 'bust'
		Upon receiving 3 bullet hits or 10 collision damage or 30 explosion damage or 10 melee damage, execute:
			Run sequence 'bust1'.
	body 'bust_dynamic'
		Upon receiving 1 bullet hit or 10 collision damage or 20 explosion damage or 10 melee damage, execute:
			Run sequence 'bust_break1'.
