unit:
	sequence 'state_stop':
		TRIGGER TIMES 1
		Enable animation_group 'animation_grp_fan_slow':
			from 0/30
			to 60/30
		Disable animation_group 'animation_grp_fan':
			from 0/30
			to 60/30
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'anim_fan' )
			position v()
		Play audio 'emitter_security_camera_explode' at 'anim_fan'.
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'state_stop'.
	body 'body_animated'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'state_stop'.
