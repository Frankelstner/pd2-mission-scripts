unit:
	sequence 'state_destroy_01':
		TRIGGER TIMES 1
		Disable object 'g_lamp_clean_01'.
		Enable object 'g_lamp_dmg_01'.
		Play audio 'light_bulb_smash' at 'e_effect_01'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_effect_01' )
			position v()
	sequence 'state_destroy_02':
		TRIGGER TIMES 1
		Disable object 'g_lamp_clean_02'.
		Enable object 'g_lamp_dmg_02'.
		Play audio 'light_bulb_smash' at 'e_effect_02'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_effect_02' )
			position v()
	sequence 'state_destroy_03':
		TRIGGER TIMES 1
		Disable object 'g_lamp_clean_03'.
		Enable object 'g_lamp_dmg_03'.
		Play audio 'light_bulb_smash' at 'e_effect_03'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_effect_03' )
			position v()
	sequence 'state_destroy_04':
		TRIGGER TIMES 1
		Disable object 'g_lamp_clean_04'.
		Enable object 'g_lamp_dmg_04'.
		Play audio 'light_bulb_smash' at 'e_effect_04'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_effect_04' )
			position v()
	sequence 'state_destroy_05':
		TRIGGER TIMES 1
		Disable object 'g_lamp_clean_05'.
		Enable object 'g_lamp_dmg_05'.
		Play audio 'light_bulb_smash' at 'e_effect_05'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_effect_05' )
			position v()
