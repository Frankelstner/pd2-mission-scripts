unit:
	sequence 'door_open':
		Enable animation_group 'animation_gate':
			from 0/30
			to 60/30
		Play audio 'security_gate_up' at 'anim_gate'.
