unit:
	sequence 'glass_shatter_01':
		TRIGGER TIMES 1
		Disable body 'glass_body_01'.
		Disable object 'g_glass'.
		Disable decal_mesh 'dm_glass_breakable'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_01' )
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_02' )
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_03' )
			position v()
		Play audio 'window_small_shatter' at 'e_glass_01'.
	body 'glass_body_01'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'glass_shatter_01'.
