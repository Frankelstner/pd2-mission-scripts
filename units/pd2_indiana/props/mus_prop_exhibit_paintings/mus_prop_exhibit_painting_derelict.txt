unit:
	sequence 'make_dynamic':
		Disable interaction.
		Disable body 'static_body'.
		Enable body 'dynamic_body'.
		Call function World:play_physic_effect('physic_effects/push_sphere_unit',dest_unit,75,0.25,pos,dir).
	body 'static_body'
		Upon receiving 2 bullet hits or 10 melee damage, execute:
			Run sequence 'make_dynamic'.
		Upon receiving 10 explosion damage, execute:
			Run sequence 'make_dynamic'.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'interact'.
	sequence 'load'.
