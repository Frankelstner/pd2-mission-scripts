unit:
	sequence 'state_damaged'.
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'state_damaged'.
