unit:
	sequence 'explode':
		Disable interaction.
		Enable object 'g_base'.
		Hide graphic_group 'grp_urns'.
		Disable body 'static_body'.
		effect 'effects/payday2/particles/destruction/dest_statue_bust':
			parent object( 'e_e' )
			position v()
		Disable decal_mesh 'dm_marble'.
		Play audio 'pot_large_shatter' at 'e_e'.
	body 'static_body'
		Upon receiving 2 bullet hits or 10 collision damage or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'explode'.
	sequence 'state_interaction_enable':
		Enable interaction.
	sequence 'state_interaction_disable':
		Disable interaction.
	sequence 'interact'.
	sequence 'load'.
