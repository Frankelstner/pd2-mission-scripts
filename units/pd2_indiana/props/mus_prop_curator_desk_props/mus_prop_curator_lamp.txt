unit:
	sequence 'state_damage':
		Disable object 'g_lamp_clean'.
		Enable object 'g_lamp_damage'.
		Disable body 'body_damage'.
		effect 'effects/particles/dest/security_camera_dest':
			parent object( 'e_effect' )
			position v()
		Play audio 'light_bulb_smash' at 'e_effect'.
		Disable light 'li_light'.
	body 'body_damage'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'state_damage'.
