unit:
	sequence 'anim_pedestal_lower':
		Disable interaction.
		Enable animation_group 'anim':
			from 0
			speed 1
			to 30/30
	sequence 'anim_pedestal_raise':
		Enable animation_group 'anim':
			from 30/30
			speed -1
			to 0
	sequence 'state_interaction_enable':
		Enable interaction.
	sequence 'state_interaction_disable':
		Disable interaction.
	sequence 'hud_set_take_diamond':
		Call function: interaction.set_tweak_data('mus_take_diamond')
	sequence 'hud_set_open_display':
		Call function: interaction.set_tweak_data('mus_hold_open_display')
	sequence 'action_take_diamond':
		Disable body 'body_diamond'.
		Disable object 'g_diamond'.
		Disable object 'g_caustics'.
	sequence 'interact':
		Disable body 'body_glass'.
		Disable object 'g_glass'.
		Disable decal_mesh 'dm_glass'.
