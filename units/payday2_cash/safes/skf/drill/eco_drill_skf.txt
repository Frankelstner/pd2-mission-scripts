unit:
	sequence 'anim_start_drilling':
		Enable animation_group 'ag_start_drill':
			loop True
		effect 'effects/payday2/economy/particles/equipment/eco_drill_sparks':
			parent object( 'e_drill_particles' )
			position v()
			store_id_list_var 'sparks_id_list'
		Play audio 'cash_drill' at 'sound_drill_effect'.
	sequence 'anim_push_through':
		Enable animation_group 'ag_drill_move_in':
			from 0
			speed 1
			to 10/30
		stop_effect: (DELAY 20/30)
			id_list_var 'sparks_id_list'
			instant False
		Disable animation_group 'ag_start_drill'. (DELAY 45/30)
		Run sequence 'anim_fall_off'. (DELAY 69/30)
		Stop audio 'cash_drill' at 'sound_drill_effect'.
	sequence 'anim_fall_off':
		Enable animation_group 'ag_drill_falling':
			from 60/30
			to 75/30
		Run sequence 'state_vis_hide'. (DELAY 20/30)
	sequence 'state_vis_hide':
		Disable object 'g_base'.
		Disable object 'g_drill'.
		Disable object 'g_drill_drill'.
	sequence 'state_vis_show':
		Enable object 'g_base'.
		Enable object 'g_drill'.
		Enable object 'g_drill_drill'.
