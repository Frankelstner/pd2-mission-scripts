unit:
	trigger 'create_safe_result'.
	sequence 'anim_open_01':
		Run sequence 'seq_glow_01'. (DELAY 15/30)
		Run sequence 'anim_explode_safe'. (DELAY 15/30)
		Play audio 'cash_safe_open_01' at 'e_effect_01'.
	sequence 'anim_open_02':
		Run sequence 'seq_glow_02'. (DELAY 15/30)
		Run sequence 'anim_explode_safe'. (DELAY 15/30)
		Play audio 'cash_safe_open_02' at 'e_effect_01'.
	sequence 'anim_open_03':
		Run sequence 'seq_glow_03'. (DELAY 15/30)
		Run sequence 'anim_explode_safe'. (DELAY 15/30)
		Play audio 'cash_safe_open_03' at 'e_effect_01'.
	sequence 'anim_open_04':
		Run sequence 'seq_glow_04'. (DELAY 15/30)
		Run sequence 'anim_explode_safe'. (DELAY 15/30)
		Play audio 'cash_safe_open_04' at 'e_effect_01'.
	sequence 'anim_open_05':
		Run sequence 'seq_glow_05'. (DELAY 15/30)
		Run sequence 'anim_explode_safe'. (DELAY 15/30)
		Play audio 'cash_safe_open_05' at 'e_effect_01'.
	sequence 'anim_explode_door_01':
		Enable animation_group 'grp_anim_door': (DELAY 54/30)
			from 60/30
			speed 1
			to 112/30
	sequence 'anim_explode_door_02':
		Enable animation_group 'grp_anim_door': (DELAY 54/30)
			from 113/30
			speed 1
			to 200/30
	sequence 'anim_explode_door_03':
		Enable animation_group 'grp_anim_door': (DELAY 54/30)
			from 201/30
			speed 1
			to 238/30
	sequence 'anim_explode_door_04':
		Enable animation_group 'grp_anim_door': (DELAY 54/30)
			from 239/30
			speed 1
			to 274/30
	sequence 'anim_explode_door_05':
		Enable animation_group 'grp_anim_door': (DELAY 54/30)
			from 275/30
			speed 1
			to 314/30
	sequence 'anim_explode_door_06':
		Enable animation_group 'grp_anim_door': (DELAY 54/30)
			from 315/30
			speed 1
			to 348/30
	sequence 'anim_explode_safe':
		Enable animation_group 'grp_anim_dest': (DELAY 54/30)
			from 60/30
			speed 1
			to 97/30
		Enable animation_group 'grp_anim_door_dest': (DELAY 54/30)
			from 60/30
			speed 1
			to 63/30
		Run sequence 'anim_explode_door_'..pick('01','02','03','04','05','06').
	sequence 'state_closed_door':
		Enable animation_group 'grp_anim_dest':
			from 0/30
			to 0/30
		Enable animation_group 'grp_anim_door':
			from 0/30
			to 0/30
		Enable animation_group 'grp_anim_door_dest':
			from 0/30
			to 0/30
	sequence 'seq_glow_01':
		Enable object 'g_glow_01'.
		Enable animation_group 'grp_anim_glow':
			from 60/30
			speed -1
			to 0/30
		Enable animation_group 'grp_anim_dest': (DELAY 55/30)
			from 80/30
			speed 3
			to 93/30
		Disable object 'g_glow_01'. (DELAY 55/30)
		effect 'effects/payday2/economy/particles/explosions/safe_eco_explosion_01':
			parent object( 'e_effect_01' )
			position v()
			store_id_list_var 'explosion_id_list'
		trigger 'create_safe_result'. (DELAY 53/30)
		stop_effect: (DELAY 5)
			id_list_var 'explosion_id_list'
			instant False
	sequence 'seq_glow_02':
		Enable object 'g_glow_02'.
		Enable animation_group 'grp_anim_glow':
			from 60/30
			speed -1
			to 0/30
		Enable animation_group 'grp_anim_dest': (DELAY 55/30)
			from 80/30
			speed 3
			to 93/30
		Disable object 'g_glow_02'. (DELAY 55/30)
		effect 'effects/payday2/economy/particles/explosions/safe_eco_explosion_02':
			parent object( 'e_effect_01' )
			position v()
			store_id_list_var 'explosion_id_list'
		trigger 'create_safe_result'. (DELAY 53/30)
		stop_effect: (DELAY 5)
			id_list_var 'explosion_id_list'
			instant False
	sequence 'seq_glow_03':
		Enable object 'g_glow_03'.
		Enable animation_group 'grp_anim_glow':
			from 60/30
			speed -1
			to 0/30
		Enable animation_group 'grp_anim_dest': (DELAY 55/30)
			from 80/30
			speed 3
			to 93/30
		Disable object 'g_glow_03'. (DELAY 55/30)
		effect 'effects/payday2/economy/particles/explosions/safe_eco_explosion_03':
			parent object( 'e_effect_01' )
			position v()
			store_id_list_var 'explosion_id_list'
		trigger 'create_safe_result'. (DELAY 53/30)
		stop_effect: (DELAY 5)
			id_list_var 'explosion_id_list'
			instant False
	sequence 'seq_glow_04':
		Enable object 'g_glow_04'.
		Enable animation_group 'grp_anim_glow':
			from 60/30
			speed -1
			to 0/30
		Enable animation_group 'grp_anim_dest': (DELAY 55/30)
			from 80/30
			speed 3
			to 93/30
		Disable object 'g_glow_04'. (DELAY 55/30)
		effect 'effects/payday2/economy/particles/explosions/safe_eco_explosion_04':
			parent object( 'e_effect_01' )
			position v()
			store_id_list_var 'explosion_id_list'
		trigger 'create_safe_result'. (DELAY 53/30)
		stop_effect: (DELAY 5)
			id_list_var 'explosion_id_list'
			instant False
	sequence 'seq_glow_05':
		Enable object 'g_glow_05'.
		Enable animation_group 'grp_anim_glow':
			from 60/30
			speed -1
			to 0/30
		Enable animation_group 'grp_anim_dest': (DELAY 55/30)
			from 80/30
			speed 3
			to 93/30
		Disable object 'g_glow_05'. (DELAY 55/30)
		effect 'effects/payday2/economy/particles/explosions/safe_eco_explosion_05':
			parent object( 'e_effect_01' )
			position v()
			store_id_list_var 'explosion_id_list'
		trigger 'create_safe_result'. (DELAY 53/30)
		stop_effect: (DELAY 5)
			id_list_var 'explosion_id_list'
			instant False
