unit:
	breached = 0
	sequence 'state_prop':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_handle_collision_in'.
		Disable body 'body_handle_collision_out'.
	sequence 'state_open_in':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_handle_collision_in'.
		Disable body 'body_handle_collision_out'.
		Enable animation_group 'anim_open_door':
			from 50/30
			to 50/30
	sequence 'state_open':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_handle_collision_in'.
		Disable body 'body_handle_collision_out'.
		Enable animation_group 'anim_open_door':
			from 101/30
			to 101/30
	sequence 'state_close':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_handle_collision_in'.
		Disable body 'body_handle_collision_out'.
		Enable animation_group 'anim_open_door':
			from 0/30
			to 0/30
	sequence 'enable_interaction':
		Enable interaction.
		Show graphic_group 'grp_icons'.
		Enable body 'body_handle_collision_in'.
		Enable body 'body_handle_collision_out'.
	sequence 'disable_interaction':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
	sequence 'open_in':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_handle_collision_in'.
		Disable body 'body_handle_collision_out'.
		Enable animation_group 'anim_open_door':
			from 0/30
			speed 1
			to 50/30
	sequence 'open':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_handle_collision_in'.
		Disable body 'body_handle_collision_out'.
		Enable animation_group 'anim_open_door':
			from 51/30
			speed 1
			to 101/30
	sequence 'close':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_handle_collision_in'.
		Disable body 'body_handle_collision_out'.
		Enable animation_group 'anim_open_door':
			from 101/30
			speed -1
			to 51/30
	sequence 'close_in':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Enable animation_group 'anim_open_door':
			from 50/30
			speed -1
			to 0/30
		Disable body 'body_handle_collision_in'.
		Disable body 'body_handle_collision_out'.
	sequence 'interact':
		Run sequence 'open'.
		Hide graphic_group 'grp_icons'.
	sequence 'int_seq_saw_in':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Run sequence 'open'.
		Disable object 'g_door_left'.
		Disable object 'g_door_right'.
		Enable object 'g_door_left_saw'.
		Enable object 'g_door_right_saw'.
		effect 'effects/payday2/particles/wood/wood_door_shotgun_dmg_in':
			parent object( 'e_effect_in' )
			position v()
		Disable body 'body_handle_collision_in'.
		Disable body 'body_handle_collision_out'.
	sequence 'int_seq_saw_out':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Run sequence 'open_in'.
		Disable object 'g_door_left'.
		Disable object 'g_door_right'.
		Enable object 'g_door_left_saw'.
		Enable object 'g_door_right_saw'.
		effect 'effects/payday2/particles/wood/wood_door_shotgun_dmg_out':
			parent object( 'e_effect_out' )
			position v()
		Disable body 'body_handle_collision_in'.
		Disable body 'body_handle_collision_out'.
	sequence 'kick_in':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_handle_collision_in'.
		Disable body 'body_handle_collision_out'.
		Enable animation_group 'anim_open_door':
			from 102/30
			speed 1
			to 117/30
	sequence 'kick_out':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_handle_collision_in'.
		Disable body 'body_handle_collision_out'.
		Enable animation_group 'anim_open_door':
			from 118/30
			speed 1
			to 133/30
	sequence 'explode_in':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_handle_collision_in'.
		Disable body 'body_handle_collision_out'.
		Disable body 'body_door_left'.
		Disable body 'body_door_right'.
		Disable object 'g_door_left'.
		Disable object 'g_door_right'.
		spawn_unit 'units/pd2_dlc_vit/props/vit_prop_interactable_door_wooden/debris/spawn_prop_door_debris_02':
			inherit_destroy True
			position object_pos('rp_vit_prop_interactable_door_wooden_2m')
			rotation object_rot('rp_vit_prop_interactable_door_wooden_2m')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_push_in')),300,25).
	sequence 'explode_out':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_handle_collision_in'.
		Disable body 'body_handle_collision_out'.
		Disable body 'body_door_left'.
		Disable body 'body_door_right'.
		Disable object 'g_door_left'.
		Disable object 'g_door_right'.
		spawn_unit 'units/pd2_dlc_vit/props/vit_prop_interactable_door_wooden/debris/spawn_prop_door_debris_02':
			inherit_destroy True
			position object_pos('rp_vit_prop_interactable_door_wooden_2m')
			rotation object_rot('rp_vit_prop_interactable_door_wooden_2m')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_push_out')),300,25).
	body 'body_handle_collision_in'
		Upon receiving 2.5 saw damage, execute:
			If breached == 0: Run sequence 'int_seq_saw_in'.
		Upon receiving 10 explosion damage, execute:
			If breached == 0: Run sequence 'explode_in'.
	body 'body_handle_collision_out'
		Upon receiving 2.5 saw damage, execute:
			If breached == 0: Run sequence 'int_seq_saw_out'.
		Upon receiving 10 explosion damage, execute:
			If breached == 0: Run sequence 'explode_out'.
