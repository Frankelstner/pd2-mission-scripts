unit:
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'interact':
		Disable object 'g_prop_oval_office_picture_01_lod0'.
		Disable body 'body_static'.
		Enable decal_mesh 'dm_cloth'.
		Enable decal_mesh 'dm_metal'.
		Hide graphic_group 'grp_outline'.
	sequence 'make_shootable':
		Disable body 'body_static'.
		Enable body 'body_shootable'.
	sequence 'make_dynamic':
		Disable body 'body_shootable'.
		Enable body 'body_dynamic'.
		slot:
			slot 18
	sequence 'show':
		Enable object 'g_prop_oval_office_picture_01_lod0'.
		Show graphic_group 'grp_outline'.
	sequence 'hide':
		Disable object 'g_prop_oval_office_picture_01_lod0'.
		Hide graphic_group 'grp_outline'.
	body 'body_shootable'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'make_dynamic'.
