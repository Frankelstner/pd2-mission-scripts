unit:
	sequence 'break':
		Disable object 'g_prop_oval_office_window_02_small'.
		Enable object 'g_prop_oval_office_window_02_small_broken'.
		Disable body 'body_glass'.
		Disable decal_mesh 'dm_glass_breakable'.
		effect 'effects/payday2/particles/wood/wood_debris_large':
			position object_pos('e_glass_break_01')
			rotation object_rot('e_glass_break_01')
		effect 'effects/payday2/particles/wood/wood_debris_large':
			position object_pos('e_glass_break_02')
			rotation object_rot('e_glass_break_02')
		effect 'effects/payday2/particles/window/des_window_medium':
			position object_pos('e_glass_break_01')
			rotation object_rot('e_glass_break_01')
		effect 'effects/payday2/particles/window/des_window_medium': (DELAY 2/30)
			position object_pos('e_glass_break_02')
			rotation object_rot('e_glass_break_02')
		Play audio 'window_medium_shatter' at 'e_glass_break_01'.
		Play audio 'window_medium_shatter' at 'e_glass_break_02'.
		Cause alert with 12 m radius.
	body 'body_glass'
		Upon receiving 3 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break'.
