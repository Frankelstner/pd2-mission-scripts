unit:
	sequence 'break':
		Disable body 'body_glass'.
		Enable body 'body_broken'.
		Disable object 'g_prop_oval_office_window_01_large'.
		Enable object 'g_prop_oval_office_window_01_large_broken'.
		Disable decal_mesh 'dm_wood'.
		Enable decal_mesh 'dm_wood_broken'.
		Disable decal_mesh 'dm_glass_breakable'.
		Disable decal_mesh 'dm_glass_unbreakable'.
		effect 'effects/payday2/particles/wood/wood_debris_large':
			position object_pos('e_glass_break_02')
			rotation object_rot('e_glass_break_02')
		effect 'effects/payday2/particles/wood/wood_debris_large':
			position object_pos('e_glass_break_01')
			rotation object_rot('e_glass_break_01')
		effect 'effects/payday2/particles/window/des_window_medium':
			position object_pos('e_glass_break_01')
			rotation object_rot('e_glass_break_01')
		effect 'effects/payday2/particles/window/des_window_medium': (DELAY 2/30)
			position object_pos('e_glass_break_02')
			rotation object_rot('e_glass_break_02')
		Cause alert with 12 m radius.
	body 'body_glass'
		Upon receiving 3 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break'.
