unit:
	sequence 'break_glass_01':
		Play audio 'glass_crack' at 'rp_picture_01'.
		Disable object 'g_1_glass'.
		Enable object 'g_1_dmg'.
	sequence 'make_dynamic_01':
		slot:
			slot 11
		Disable body 'static_body_01'.
		Enable body 'dynamic_body_01'.
		Play audio 'painting_break' at 'snd'.
		Call function World:play_physic_effect('core/physic_effects/sequencemanager_push',rp_picture_01,).
	sequence 'break_glass_02':
		Play audio 'glass_crack' at 'rp_picture_02'.
		Disable object 'g_2_glass'.
		Enable object 'g_2_dmg'.
	sequence 'make_dynamic_02':
		slot:
			slot 11
		Disable body 'static_body_02'.
		Enable body 'dynamic_body_02'.
		Play audio 'painting_break' at 'snd'.
		Call function World:play_physic_effect('core/physic_effects/sequencemanager_push',rp_picture_02,).
	sequence 'break_glass_03':
		Play audio 'glass_crack' at 'rp_picture_03'.
		Disable object 'g_3_glass'.
		Enable object 'g_3_dmg'.
	sequence 'make_dynamic_03':
		slot:
			slot 11
		Disable body 'static_body_03'.
		Enable body 'dynamic_body_03'.
		Play audio 'painting_break' at 'snd'.
		Call function World:play_physic_effect('core/physic_effects/sequencemanager_push',rp_picture_03,).
	sequence 'break_glass_04':
		Play audio 'glass_crack' at 'rp_picture_04'.
		Disable object 'g_4_glass'.
		Enable object 'g_4_dmg'.
	sequence 'make_dynamic_04':
		slot:
			slot 11
		Disable body 'static_body_04'.
		Enable body 'dynamic_body_04'.
		Play audio 'painting_break' at 'snd'.
		Call function World:play_physic_effect('core/physic_effects/sequencemanager_push',rp_picture_04,).
	body 'static_body_01'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'break_glass_01'.
		Upon receiving 2 bullet hits or 10 melee damage, execute:
			Run sequence 'make_dynamic_01'.
		Upon receiving 10 explosion damage, execute:
			Run sequence 'break_glass_01'.
			Run sequence 'make_dynamic_01'.
	body 'static_body_02'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'break_glass_02'.
		Upon receiving 2 bullet hits or 10 melee damage, execute:
			Run sequence 'make_dynamic_02'.
		Upon receiving 10 explosion damage, execute:
			Run sequence 'break_glass_02'.
			Run sequence 'make_dynamic_02'.
	body 'static_body_03'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'break_glass_03'.
		Upon receiving 2 bullet hits or 10 melee damage, execute:
			Run sequence 'make_dynamic_03'.
		Upon receiving 10 explosion damage, execute:
			Run sequence 'break_glass_03'.
			Run sequence 'make_dynamic_03'.
	body 'static_body_04'
		Upon receiving 1 bullet hit or 5 melee damage, execute:
			Run sequence 'break_glass_04'.
		Upon receiving 2 bullet hits or 10 melee damage, execute:
			Run sequence 'make_dynamic_04'.
		Upon receiving 10 explosion damage, execute:
			Run sequence 'break_glass_04'.
			Run sequence 'make_dynamic_04'.
