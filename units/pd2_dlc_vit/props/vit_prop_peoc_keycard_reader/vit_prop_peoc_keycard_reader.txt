unit:
	sequence 'set_use_presidential_key':
		Call function: interaction.set_tweak_data('vit_keycard_use')
	sequence 'light_default':
		Disable object 'g_greenscreen'.
		Disable object 'g_redscreen'.
	sequence 'light_green':
		Enable object 'g_greenscreen'.
		Disable object 'g_redscreen'.
		Enable object 'g_keycard'.
	sequence 'light_red':
		Disable object 'g_greenscreen'.
		Enable object 'g_redscreen'.
	sequence 'hide':
		Disable object 'g_lights'.
		Disable object 'g_greenscreen'.
		Disable object 'g_redscreen'.
		Hide graphic_group 'grp_lod'.
		Disable object 'g_keycard'.
		Disable body 'body_static'.
		Disable decal_mesh 'dm_steel'.
		Disable interaction.
	sequence 'show':
		Enable object 'g_lights'.
		Disable object 'g_greenscreen'.
		Disable object 'g_redscreen'.
		Disable object 'g_keycard'.
		Show graphic_group 'grp_lod'.
		Enable body 'body_static'.
		Enable decal_mesh 'dm_steel'.
	sequence 'interact_enable':
		Enable interaction.
	sequence 'interact_disable':
		Disable interaction.
	sequence 'interact':
		Run sequence 'light_green'.
		Run sequence 'interact_disable'.
		Play audio 'card_reader_first_card' at 'interact'.
