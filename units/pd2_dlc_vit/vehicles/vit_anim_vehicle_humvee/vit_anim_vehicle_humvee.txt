unit:
	sequence '__reset_animation__':
		Call function: base.move_to_stored_pos()
	sequence '__allow_sync_reset_animation__':
		Call function: base.allow_sync_stored_pos('true')
	sequence 'anim_humvee_crash':
		Play audio 'vit_anim_car_wall' at 'snd'.
		animation_redirect 'humvee_crash'.
		Call function: base.store_current_pos()
		Run sequence 'done_car_anim'. (DELAY 296/30)
	sequence 'anim_humvee_drive_in':
		Play audio 'vit_anim_humvee_drive_in' at 'snd'.
		animation_redirect 'humvee_drive_in'.
		Call function: base.store_current_pos()
		Run sequence 'done_car_anim'. (DELAY 344/30)
	sequence 'show_turret_crate':
		Enable body 'body_lid_01'.
		Enable body 'body_lid_02'.
		Enable body 'body_static'.
		Show graphic_group 'grp_body'.
		Show graphic_group 'grp_lid_01'.
		Show graphic_group 'grp_lid_02'.
		Enable decal_mesh 'dm_steel_01'.
		Enable decal_mesh 'dm_steel_02'.
		Enable decal_mesh 'dm_steel_03'.
		Enable decal_mesh 'dm_steel_04'.
		Enable decal_mesh 'dm_steel_05'.
		Enable decal_mesh 'dm_foam_01'.
		Enable decal_mesh 'dm_foam_02'.
		Enable decal_mesh 'dm_foam_03'.
	sequence 'hide_turret_crate':
		Disable body 'body_lid_01'.
		Disable body 'body_lid_02'.
		Disable body 'body_static'.
		Hide graphic_group 'grp_body'.
		Hide graphic_group 'grp_lid_01'.
		Hide graphic_group 'grp_lid_02'.
		Disable decal_mesh 'dm_steel_01'.
		Disable decal_mesh 'dm_steel_02'.
		Disable decal_mesh 'dm_steel_03'.
		Disable decal_mesh 'dm_steel_04'.
		Disable decal_mesh 'dm_steel_05'.
		Disable decal_mesh 'dm_foam_01'.
		Disable decal_mesh 'dm_foam_02'.
		Disable decal_mesh 'dm_foam_03'.
	sequence 'turret_spawn':
		Call function: base.spawn_module('units/pd2_dlc_bph/vehicles/bph_turret/bph_turret','spawn_turret','turret1')
	sequence 'turret_activate':
		Enable animation_group 'anim':
			from 0
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('e_effect')),200,200). (DELAY 5/30)
		Enable effect_spawner 'smoke'.
		Play audio 'c4_explode_wood' at 'e_effect'. (DELAY 5/30)
		Enable body 'body_lid_dyn_01'. (DELAY 5/30)
		Disable body 'body_lid_01'. (DELAY 5/30)
		Enable body 'body_lid_dyn_02'. (DELAY 5/30)
		Disable body 'body_lid_02'. (DELAY 5/30)
		Enable animation_group 'anim':
			from 0/30
			to 10/30
		Call function: base.run_module_function('turret1','base','activate_as_module','combatant','crate_turret_module') (DELAY 8/30)
	sequence 'done_car_anim'.
