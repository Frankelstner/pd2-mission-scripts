unit:
	breached = 0
	sequence 'state_prop':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
	sequence 'state_open_in':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_open_door':
			from 30/30
			to 30/30
	sequence 'state_open':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_open_door':
			from 30/30
			to 30/30
	sequence 'state_close':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_open_door':
			from 0/30
			to 0/30
	sequence 'enable_interaction':
		Enable interaction.
		Show graphic_group 'grp_icons'.
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
	sequence 'disable_interaction':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
	sequence 'open_in':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_open_door':
			from 0/30
			speed 1
			to 30/30
	sequence 'open':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_open_door':
			from 0/30
			speed 1
			to 30/30
	sequence 'close':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_open_door':
			from 30/30
			speed -1
			to 0/30
	sequence 'close_in':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Enable animation_group 'anim_open_door':
			from 30/30
			speed -1
			to 0/30
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
	sequence 'interact':
		Run sequence 'open'.
		Hide graphic_group 'grp_icons'.
	sequence 'int_seq_saw_in':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Run sequence 'open'.
		Enable object 'g_door_frame'.
		Hide graphic_group 'grp_door'.
		Enable object 'g_saw_dmg'.
		effect 'effects/payday2/particles/wood/wood_door_shotgun_dmg_in':
			parent object( 'e_effect_in' )
			position v()
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
	sequence 'int_seq_saw_out':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Run sequence 'open_in'.
		Enable object 'g_door_frame'.
		Enable object 'g_saw_dmg'.
		Hide graphic_group 'grp_door'.
		effect 'effects/payday2/particles/wood/wood_door_shotgun_dmg_out':
			parent object( 'e_effect_out' )
			position v()
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
	sequence 'kick_in':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_kick':
			from 0/30
			speed 1
			to 12/30
		Disable body 'body_door'. (DELAY 12/30)
		Disable body 'body_door2'. (DELAY 12/30)
	sequence 'kick_out':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim_kick':
			from 13/30
			speed 1
			to 26/30
		Disable body 'body_door'. (DELAY 13/30)
		Disable body 'body_door2'. (DELAY 13/30)
	sequence 'explode_in':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Hide graphic_group 'grp_door'.
		Disable object 's_s'.
		Disable object 's_s2'.
		Run sequence 'disable_decal_meshes'.
		Run sequence 'disable_collisions'.
		spawn_unit 'units/pd2_dlc_vit/equipment/vit_interactable_door_wooden_white/debris/spawn_prop_door_white_debris_02':
			inherit_destroy True
			position object_pos('rp_vit_interactable_door_wooden_white_double')
			rotation object_rot('rp_vit_interactable_door_wooden_white_double')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('c_push_in')),300,25).
	sequence 'explode_out':
		Disable interaction.
		Hide graphic_group 'grp_icons'.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Hide graphic_group 'grp_door'.
		Disable object 's_s'.
		Disable object 's_s2'.
		Run sequence 'disable_decal_meshes'.
		Run sequence 'disable_collisions'.
		spawn_unit 'units/pd2_dlc_vit/equipment/vit_interactable_door_wooden_white/debris/spawn_prop_door_white_debris_02':
			inherit_destroy True
			position object_pos('rp_vit_interactable_door_wooden_white_double')
			rotation object_rot('rp_vit_interactable_door_wooden_white_double')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object(Idstring('c_push_out')),300,25).
	sequence 'disable_decal_meshes':
		Disable decal_mesh 'dm_wood_door'.
	sequence 'disable_collisions':
		Disable body 'body_door'.
		Disable body 'body_door2'.
		Disable body 'body_frame'.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
	body 'body_hitbox_door_handle_in'
		Upon receiving 2.5 saw damage, execute:
			If breached == 0: Run sequence 'int_seq_saw_in'.
		Upon receiving 10 explosion damage, execute:
			If breached == 0: Run sequence 'explode_in'.
	body 'body_hitbox_door_handle_out'
		Upon receiving 2.5 saw damage, execute:
			If breached == 0: Run sequence 'int_seq_saw_out'.
		Upon receiving 10 explosion damage, execute:
			If breached == 0: Run sequence 'explode_out'.
