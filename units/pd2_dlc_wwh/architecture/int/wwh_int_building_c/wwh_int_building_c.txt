unit:
	sequence 'open':
		Disable body 'c_random'.
		Enable body 'c_always'.
		Enable object 'g_floor'.
		Enable object 'g_roof'.
		Enable object 'g_metal_beams'.
		Disable object 'g_random_door'.
		Enable object 'g_walls'.
		Enable object 'g_walls_2'.
		Disable decal_mesh 'dm_sheet_metal_random'.
	sequence 'close':
		Enable body 'c_random'.
		Enable body 'c_always'.
		Enable object 'g_floor'.
		Enable object 'g_roof'.
		Enable object 'g_metal_beams'.
		Enable object 'g_random_door'.
		Enable object 'g_walls'.
		Enable object 'g_walls_2'.
		Enable decal_mesh 'dm_sheet_metal_random'.
