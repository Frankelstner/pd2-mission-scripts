unit:
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'interact'.
	sequence 'anim_fall_01':
		Enable animation_group 'anim_fall':
			from 0/30
			to 80/30
		Play audio 'bar_remove_stairs_finished_01' at 'snd'.
	sequence 'anim_fall_02':
		Enable animation_group 'anim_fall':
			from 81/30
			to 189/30
		Play audio 'bar_remove_stairs_finished_02' at 'snd'.
