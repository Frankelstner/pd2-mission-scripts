unit:
	sequence 'hide':
		Disable object 'g_first_01_a_c'.
		Disable object 's_first_01_a_c'.
		Disable object 'g_first_01_a_c_dropout'.
		Disable object 's_first_01_a_c_dropout'.
	sequence 'show_connected':
		Enable object 'g_first_01_a_c'.
		Enable body 'body_static'.
		Disable body 'body_static_dropout'.
		Enable object 's_first_01_a_c'.
		Disable object 'g_first_01_a_c_dropout'.
		Disable object 's_first_01_a_c_dropout'.
	sequence 'show_unconnected':
		Disable body 'body_static'.
		Enable body 'body_static_dropout'.
		Disable object 'g_first_01_a_c'.
		Disable object 's_first_01_a_c'.
		Enable object 'g_first_01_a_c_dropout'.
		Enable object 's_first_01_a_c_dropout'.
	sequence 'hide_transparent':
		Disable object 'g_first_01_a_c_transparent'.
		Disable object 'g_first_01_a_c_transparent_dropout'.
	sequence 'show_transparent_unconnected':
		Disable object 'g_first_01_a_c_transparent'.
		Enable object 'g_first_01_a_c_transparent_dropout'.
	sequence 'show_transparent_connected':
		Enable object 'g_first_01_a_c_transparent'.
		Disable object 'g_first_01_a_c_transparent_dropout'.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'interact':
		Run sequence 'hide_transparent'.
		Run sequence 'show_connected'.
		Run sequence 'disable_interaction'.
