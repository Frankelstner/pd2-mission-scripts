unit:
	spawn_bool = 1
	bag = 0
	sequence 'int_seq_state_1_lootbag':
		Enable object 'g_bag_01'.
		bag = 1
	sequence 'int_seq_state_2_lootbags':
		Enable object 'g_bag_02'.
		bag = 2
	sequence 'int_seq_state_3_lootbags':
		Enable object 'g_bag_03'.
		bag = 3
	sequence 'int_seq_state_4_lootbags':
		Enable object 'g_bag_04'.
		bag = 4
	sequence 'int_seq_state_5_lootbags':
		Enable object 'g_bag_05'.
		bag = 5
	sequence 'int_seq_state_6_lootbags':
		Enable object 'g_bag_06'.
		bag = 6
	sequence 'int_seq_state_7_lootbags':
		Enable object 'g_bag_07'.
		bag = 7
	sequence 'int_seq_state_8_lootbags':
		Enable object 'g_bag_08'.
		bag = 8
	sequence 'state_no_bags':
		Disable object 'g_bag_01'.
		Disable object 'g_bag_02'.
		Disable object 'g_bag_03'.
		Disable object 'g_bag_04'.
		Disable object 'g_bag_05'.
		Disable object 'g_bag_06'.
		Disable object 'g_bag_07'.
		Disable object 'g_bag_08'.
		bag = 0
	sequence 'action_add_lootbag':
		If bag == 7: Run sequence 'int_seq_state_8_lootbags'.
		If bag == 6: Run sequence 'int_seq_state_7_lootbags'.
		If bag == 5: Run sequence 'int_seq_state_6_lootbags'.
		If bag == 4: Run sequence 'int_seq_state_5_lootbags'.
		If bag == 3: Run sequence 'int_seq_state_4_lootbags'.
		If bag == 2: Run sequence 'int_seq_state_3_lootbags'.
		If bag == 1: Run sequence 'int_seq_state_2_lootbags'.
		If bag == 0: Run sequence 'int_seq_state_1_lootbag'.
	sequence 'var_set_color_white':
		Enable object 'g_1'.
		Disable object 'g_2'.
		Disable object 'g_3'.
		Disable object 'g_4'.
		Disable object 'g_5'.
	sequence 'var_set_color_red':
		Disable object 'g_1'.
		Enable object 'g_2'.
		Disable object 'g_3'.
		Disable object 'g_4'.
		Disable object 'g_5'.
	sequence 'var_set_color_green':
		Disable object 'g_1'.
		Disable object 'g_2'.
		Enable object 'g_3'.
		Disable object 'g_4'.
		Disable object 'g_5'.
	sequence 'var_set_color_blue':
		Disable object 'g_1'.
		Disable object 'g_2'.
		Disable object 'g_3'.
		Enable object 'g_4'.
		Disable object 'g_5'.
	sequence 'var_set_color_yellow':
		Disable object 'g_1'.
		Disable object 'g_2'.
		Disable object 'g_3'.
		Disable object 'g_4'.
		Enable object 'g_5'.
	sequence 'state_all_door_open':
		Run sequence 'state_back_door_open'.
		Run sequence 'state_front_door_open'.
	sequence 'state_all_door_closed':
		Run sequence 'state_back_door_closed'.
		Run sequence 'state_front_door_closed'.
	sequence 'state_back_door_open':
		Enable animation_group 'anim_door_back':
			from 60/30
			speed 0
			to 60/30
	sequence 'state_back_door_closed':
		Enable animation_group 'anim_door_back':
			from 0/30
			speed 0
			to 0/30
	sequence 'state_front_door_open':
		Enable animation_group 'anim_door_front':
			from 60/30
			speed 0
			to 60/30
	sequence 'state_front_door_closed':
		Enable animation_group 'anim_door_front':
			from 0/30
			speed 0
			to 0/30
	sequence 'state_vis_hide':
		Hide graphic_group 'grp_container'.
		Disable body 'body_container'.
		Disable body 'body_door_back_right'.
		Disable body 'body_door_back_left'.
		Disable body 'body_door_front_left'.
		Disable body 'body_door_front_right'.
		Disable decal_mesh 'dm_metal'.
		Disable decal_mesh 'dm_lr_door'.
		Disable decal_mesh 'dm_rr_door'.
		Disable decal_mesh 'dm_lf_door'.
		Disable decal_mesh 'dm_rf_door'.
	sequence 'state_vis_show':
		Show graphic_group 'grp_container'.
		Enable body 'body_container'.
		Enable body 'body_door_back_right'.
		Enable body 'body_door_back_left'.
		Enable body 'body_door_front_left'.
		Enable body 'body_door_front_right'.
		Enable decal_mesh 'dm_metal'.
		Enable decal_mesh 'dm_lr_door'.
		Enable decal_mesh 'dm_rr_door'.
		Enable decal_mesh 'dm_lf_door'.
		Enable decal_mesh 'dm_rf_door'.
	sequence 'anim_back_door_open':
		Enable animation_group 'anim_door_back':
			from 0/30
			to 60/30
		Play audio 'container_open' at 'lr_door'.
	sequence 'anim_back_door_close':
		Enable animation_group 'anim_door_back':
			from 60/30
			speed -1
			to 0/30
		Play audio 'container_close' at 'lr_door'.
		Play audio 'container_close' at 'rr_door'.
	sequence 'anim_front_door_open':
		Enable animation_group 'anim_door_front':
			from 0/30
			to 60/30
		Play audio 'container_open' at 'lf_door'.
	sequence 'anim_front_door_close':
		Enable animation_group 'anim_door_front':
			from 60/30
			speed -1
			to 0/30
		Play audio 'container_close' at 'lf_door'.
		Play audio 'container_close' at 'rf_door'.
