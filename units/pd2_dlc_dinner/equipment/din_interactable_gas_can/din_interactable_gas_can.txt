unit:
	sequence 'hide':
		Disable interaction.
		Disable decal_mesh 'g_thermite'.
		Disable object 'g_thermite'.
		Disable object 'g_gas_decal'.
	sequence 'show_interactable':
		Enable interaction.
		Enable decal_mesh 'g_thermite'.
		Enable object 'g_thermite'.
		Disable object 'g_gas_decal'.
		material_config 'units/equipment/gas/gas_can_interact'.
	sequence 'show':
		Enable interaction.
		Enable decal_mesh 'g_thermite'.
		Enable object 'g_thermite'.
		Enable object 'g_gas_decal'.
		material_config 'units/equipment/thermite/thermite_01'.
	sequence 'switch':
		Disable interaction.
		material_config 'units/equipment/thermite/thermite_01'. (DELAY 0.1)
		Enable object 'g_thermite'. (DELAY 0.1)
		Disable object 'g_thermite_decal'. (DELAY 0.1)
		Enable object 'g_gas_decal'. (DELAY 0.1)
	sequence 'interact':
		Run sequence 'switch'.
		Play audio 'gas_tank_put_down' at 'rp_thermite'.
