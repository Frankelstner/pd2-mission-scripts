unit:
	sequence 'warp_blue':
		Disable object 'g_warp_ladder'.
		Disable object 'g_warp_jump'.
		Disable object 'g_warp_fatal'.
		Enable object 'g_warp_blue'.
		Disable object 'g_warp_blue_direction'.
		Disable object 'g_warp_blue_intense'.
		Disable object 'g_warp_orange'.
		Disable object 'g_warp_green'.
		Disable object 'g_warp_grey'.
	sequence 'warp_blue_intense':
		Disable object 'g_warp_ladder'.
		Disable object 'g_warp_jump'.
		Disable object 'g_warp_fatal'.
		Disable object 'g_warp_blue'.
		Enable object 'g_warp_blue_direction'.
		Enable object 'g_warp_blue_intense'.
		Disable object 'g_warp_orange'.
		Disable object 'g_warp_green'.
		Disable object 'g_warp_grey'.
	sequence 'warp_orange':
		Disable object 'g_warp_ladder'.
		Disable object 'g_warp_jump'.
		Disable object 'g_warp_fatal'.
		Disable object 'g_warp_blue'.
		Disable object 'g_warp_blue_direction'.
		Disable object 'g_warp_blue_intense'.
		Enable object 'g_warp_orange'.
		Disable object 'g_warp_green'.
		Disable object 'g_warp_grey'.
	sequence 'warp_green':
		Disable object 'g_warp_ladder'.
		Disable object 'g_warp_jump'.
		Disable object 'g_warp_fatal'.
		Disable object 'g_warp_blue'.
		Disable object 'g_warp_blue_direction'.
		Disable object 'g_warp_blue_intense'.
		Disable object 'g_warp_orange'.
		Enable object 'g_warp_green'.
		Disable object 'g_warp_grey'.
	sequence 'warp_jump':
		Disable object 'g_warp_ladder'.
		Enable object 'g_warp_jump'.
		Disable object 'g_warp_fatal'.
		Disable object 'g_warp_blue'.
		Disable object 'g_warp_blue_direction'.
		Disable object 'g_warp_blue_intense'.
		Enable object 'g_warp_orange'.
		Disable object 'g_warp_green'.
		Disable object 'g_warp_grey'.
	sequence 'warp_ladder':
		Enable object 'g_warp_ladder'.
		Disable object 'g_warp_jump'.
		Disable object 'g_warp_fatal'.
		Disable object 'g_warp_blue'.
		Disable object 'g_warp_blue_direction'.
		Disable object 'g_warp_blue_intense'.
		Enable object 'g_warp_orange'.
		Disable object 'g_warp_green'.
		Disable object 'g_warp_grey'.
	sequence 'warp_fatal':
		Disable object 'g_warp_ladder'.
		Disable object 'g_warp_jump'.
		Enable object 'g_warp_fatal'.
		Disable object 'g_warp_blue'.
		Disable object 'g_warp_blue_direction'.
		Enable object 'g_warp_blue_intense'.
		Disable object 'g_warp_orange'.
		Disable object 'g_warp_green'.
		Disable object 'g_warp_grey'.
	sequence 'warp_direction':
		Disable object 'g_warp_ladder'.
		Disable object 'g_warp_jump'.
		Disable object 'g_warp_fatal'.
		Disable object 'g_warp_blue'.
		Enable object 'g_warp_blue_direction'.
		Disable object 'g_warp_blue_intense'.
		Disable object 'g_warp_orange'.
		Disable object 'g_warp_green'.
		Disable object 'g_warp_grey'.
