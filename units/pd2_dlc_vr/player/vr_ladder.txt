unit:
	sequence 'ladder_up':
		Disable object 'g_ladder_down'.
		Enable object 'g_ladder_up'.
		Disable object 'g_warp_ladder_idle'.
		Disable object 'g_ladder_area_idle'.
		Disable object 'g_ladder_up_idle'.
		Disable object 'g_ladder_down_idle'.
	sequence 'ladder_down':
		Enable object 'g_ladder_down'.
		Disable object 'g_ladder_up'.
		Disable object 'g_warp_ladder_idle'.
		Disable object 'g_ladder_area_idle'.
		Disable object 'g_ladder_up_idle'.
		Disable object 'g_ladder_down_idle'.
	sequence 'ladder_up_idle':
		Disable object 'g_ladder_down'.
		Disable object 'g_ladder_up'.
		Enable object 'g_warp_ladder_idle'.
		Enable object 'g_ladder_area_idle'.
		Enable object 'g_ladder_up_idle'.
		Disable object 'g_ladder_down_idle'.
	sequence 'ladder_down_idle':
		Disable object 'g_ladder_down'.
		Disable object 'g_ladder_up'.
		Enable object 'g_warp_ladder_idle'.
		Enable object 'g_ladder_area_idle'.
		Disable object 'g_ladder_up_idle'.
		Enable object 'g_ladder_down_idle'.
	sequence 'ladder_hide':
		Disable object 'g_ladder_down'.
		Disable object 'g_ladder_up'.
		Disable object 'g_warp_ladder_idle'.
		Disable object 'g_ladder_area_idle'.
		Disable object 'g_ladder_up_idle'.
		Disable object 'g_ladder_down_idle'.
