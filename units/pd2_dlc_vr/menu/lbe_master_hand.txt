unit:
	sequence 'right':
		Enable object 'g_right_on'.
		Disable object 'g_right_off'.
		Disable object 'g_left_on'.
		Enable object 'g_left_off'.
	sequence 'left':
		Disable object 'g_right_on'.
		Enable object 'g_right_off'.
		Enable object 'g_left_on'.
		Disable object 'g_left_off'.
	sequence 'none':
		Disable object 'g_right_on'.
		Enable object 'g_right_off'.
		Disable object 'g_left_on'.
		Enable object 'g_left_off'.
