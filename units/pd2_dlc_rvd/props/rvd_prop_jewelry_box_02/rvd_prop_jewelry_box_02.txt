unit:
	sequence 'state_show':
		Enable body 'body_static'.
		Enable object 'g_g'.
		Enable object 'g_pillow'.
		Enable object 'g_diamonds'.
	sequence 'state_hide':
		Disable interaction.
		Disable body 'body_static'.
		Disable object 'g_g'.
		Disable object 'g_pillow'.
		Disable object 'g_diamonds'.
		Show graphic_group 'grp_outline'.
	sequence 'enable_interaction':
		Enable interaction.
		Show graphic_group 'grp_outline'.
	sequence 'disable_interaction':
		Disable interaction.
		Hide graphic_group 'grp_outline'.
	sequence 'enabled':
		Enable interaction.
	sequence 'load'.
