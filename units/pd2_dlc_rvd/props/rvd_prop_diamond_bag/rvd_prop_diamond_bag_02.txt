unit:
	sequence 'interact':
		Disable interaction.
		Disable object 'g_g'.
		Hide graphic_group 'grp_outline'.
	sequence 'enable_interaction':
		Enable interaction.
		Show graphic_group 'grp_outline'.
	sequence 'disable_interaction':
		Disable interaction.
		Hide graphic_group 'grp_outline'.
	sequence 'hide':
		Disable interaction.
		Disable object 'g_g'.
		Disable body 'body_static'.
		Hide graphic_group 'grp_outline'.
	sequence 'show':
		Enable object 'g_g'.
		Enable body 'body_static'.
	sequence 'show_and_enable_interaction':
		Enable interaction.
		Enable object 'g_g'.
		Enable body 'body_static'.
		Show graphic_group 'grp_outline'.
	sequence 'load'.
