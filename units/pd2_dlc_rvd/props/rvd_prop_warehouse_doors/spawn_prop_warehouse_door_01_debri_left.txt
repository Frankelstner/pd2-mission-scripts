unit:
	sequence 'int_seq_impact_sound_plank_01':
		Play audio 'wood_plank_impact' at 'g_plank_01'.
	sequence 'int_seq_impact_sound_plank_02':
		Play audio 'wood_plank_impact' at 'g_plank_02'.
	sequence 'int_seq_impact_sound_plank_03':
		Play audio 'wood_plank_impact' at 'g_plank_03'.
	sequence 'int_seq_impact_sound_plank_04':
		Play audio 'wood_plank_impact' at 'g_plank_04'.
	sequence 'int_seq_impact_sound_plank_05':
		Play audio 'wood_plank_impact' at 'g_plank_05'.
	sequence 'int_seq_impact_sound_plank_06':
		Play audio 'wood_plank_impact' at 'g_plank_06'.
	body 'body_plank_01'
		Upon receiving 1 collision damage, execute:
			Run sequence 'int_seq_impact_sound_plank_01'.
	body 'body_plank_02'
		Upon receiving 1 collision damage, execute:
			Run sequence 'int_seq_impact_sound_plank_02'.
	body 'body_plank_03'
		Upon receiving 1 collision damage, execute:
			Run sequence 'int_seq_impact_sound_plank_03'.
	body 'body_plank_04'
		Upon receiving 1 collision damage, execute:
			Run sequence 'int_seq_impact_sound_plank_04'.
	body 'body_plank_05'
		Upon receiving 1 collision damage, execute:
			Run sequence 'int_seq_impact_sound_plank_05'.
	body 'body_plank_06'
		Upon receiving 1 collision damage, execute:
			Run sequence 'int_seq_impact_sound_plank_06'.
