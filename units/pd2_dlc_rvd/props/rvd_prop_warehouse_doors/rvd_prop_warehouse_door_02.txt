unit:
	sequence 'anim_open_door':
		Enable animation_group 'door':
			from 0/30
			to 30/30
		Run sequence 'done_opened'.
	sequence 'anim_close_door':
		Enable animation_group 'door':
			from 31/30
			to 54/30
		Run sequence 'done_closed'.
	sequence 'state_open_door':
		Enable animation_group 'door':
			from 30/30
			to 30/30
	sequence 'state_door_close':
		Enable animation_group 'door':
			from 54/30
			to 54/30
	sequence 'anim_door_kick':
		Enable animation_group 'door':
			from 55/30
			to 128/30
		Run sequence 'done_swat_breach'.
	body 'body_window_01'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_shatter_window_01'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_window_01'.
	sequence 'int_seq_shatter_window_01':
		Disable object 'g_glass_01'.
		Enable object 'g_glass_01_broken'.
	sequence 'int_seq_break_window_01':
		Disable body 'body_window_01'.
		Disable object 'g_glass_01'.
		Disable object 'g_glass_01_broken'.
		Play audio 'window_small_shatter' at 'e_glass_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_01')
			position v()
	body 'body_window_02'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_shatter_window_02'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_window_02'.
	sequence 'int_seq_shatter_window_02':
		Disable object 'g_glass_02'.
		Enable object 'g_glass_02_broken'.
	sequence 'int_seq_break_window_02':
		Disable body 'body_window_02'.
		Disable object 'g_glass_02'.
		Disable object 'g_glass_02_broken'.
		Play audio 'window_small_shatter' at 'e_glass_02'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_02')
			position v()
	body 'body_window_03'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_shatter_window_03'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_window_03'.
	sequence 'int_seq_shatter_window_03':
		Disable object 'g_glass_03'.
		Enable object 'g_glass_03_broken'.
	sequence 'int_seq_break_window_03':
		Disable body 'body_window_03'.
		Disable object 'g_glass_03'.
		Disable object 'g_glass_03_broken'.
		Play audio 'window_small_shatter' at 'e_glass_03'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_03')
			position v()
	body 'body_window_04'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_shatter_window_04'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_window_04'.
	sequence 'int_seq_shatter_window_04':
		Disable object 'g_glass_04'.
		Enable object 'g_glass_04_broken'.
	sequence 'int_seq_break_window_04':
		Disable body 'body_window_04'.
		Disable object 'g_glass_04'.
		Disable object 'g_glass_04_broken'.
		Play audio 'window_small_shatter' at 'e_glass_04'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_04')
			position v()
	body 'body_window_05'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_shatter_window_05'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_window_05'.
	sequence 'int_seq_shatter_window_05':
		Disable object 'g_glass_05'.
		Enable object 'g_glass_05_broken'.
	sequence 'int_seq_break_window_05':
		Disable body 'body_window_05'.
		Disable object 'g_glass_05'.
		Disable object 'g_glass_05_broken'.
		Play audio 'window_small_shatter' at 'e_glass_05'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_05')
			position v()
	body 'body_window_06'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_shatter_window_06'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_window_06'.
	sequence 'int_seq_shatter_window_06':
		Disable object 'g_glass_06'.
		Enable object 'g_glass_06_broken'.
	sequence 'int_seq_break_window_06':
		Disable body 'body_window_06'.
		Disable object 'g_glass_06'.
		Disable object 'g_glass_06_broken'.
		Play audio 'window_small_shatter' at 'e_glass_06'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_06')
			position v()
	body 'body_window_07'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_shatter_window_07'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_window_07'.
	sequence 'int_seq_shatter_window_07':
		Disable object 'g_glass_07'.
		Enable object 'g_glass_07_broken'.
	sequence 'int_seq_break_window_07':
		Disable body 'body_window_07'.
		Disable object 'g_glass_07'.
		Disable object 'g_glass_07_broken'.
		Play audio 'window_small_shatter' at 'e_glass_07'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_07')
			position v()
	sequence 'anim_car_crash_door':
		Enable animation_group 'debris':
			from 0/30
			speed 1
			to 275/30
		Run sequence 'int_seq_car_crash_door'. (DELAY 275/30)
	sequence 'int_seq_car_crash_door':
		Enable animation_group 'debris':
			from 275/30
			speed 3
			to 555/30
		effect 'effects/particles/dest/wall_dest_rvd':
			parent object( 'e_effect_01' )
			position v()
		Play audio 'rvd_warehouse_door_02_impact' at 'g_frame'.
		Enable body 'body_static_broken'.
		Disable body 'body_static'.
		Disable body 'anim_door'.
		Disable object 'g_frame'.
		Disable object 'g_door'.
		Enable object 'g_frame_broken'.
		Enable object 'g_debris_01'.
		Enable object 'g_debris_02'.
		Play audio 'rvd_warehouse_door_02_door_piece' at 'snd_debris_02'.
		Enable object 'g_debris_03'.
		Enable object 'g_debris_04'.
		Play audio 'rvd_warehouse_door_02_square_piece' at 'snd_debris_04'.
		Enable object 'g_debris_05'.
		Enable object 'g_door_debris'.
		Play audio 'rvd_warehouse_door_02_plank_piece' at 'snd_debris_06'.
		Play audio 'rvd_warehouse_door_02_big_piece' at 'snd_debris_07'.
		Enable object 's_frame_broken'.
		Disable object 's_frame'.
		Enable decal_mesh 'dm_wood_frame_broken'.
		Disable decal_mesh 'dm_wood_frame'.
	sequence 'done_opened'.
	sequence 'done_closed'.
	sequence 'done_swat_breach'.
