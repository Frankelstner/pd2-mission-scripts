unit:
	sequence 'state_vis_hide':
		Hide graphic_group 'grp_vehicle_whole'.
		Disable body 'body_body'.
		Disable body 'body_anim_blocker'.
		Disable body 'body_door_left'.
		Disable body 'body_door_right'.
		Disable body 'body_wheel_front_right'.
		Disable body 'body_wheel_front_left'.
		Disable body 'body_win_front'.
		Disable body 'body_win_left_front'.
		Disable body 'body_win_right_front'.
		Disable body 'body_win_left_rear'.
		Disable body 'body_win_right_rear'.
		Disable body 'body_win_back'.
		Disable body 'body_blocker_mover'.
		Disable decal_mesh 'dm_window_front'.
		Disable decal_mesh 'dm_door_left_window'.
		Disable decal_mesh 'dm_door_right_window'.
		Disable decal_mesh 'dm_window_rear_left'.
		Disable decal_mesh 'dm_window_rear_right'.
		Disable decal_mesh 'dm_window_back'.
		Disable decal_mesh 'dm_body'.
		Disable decal_mesh 'dm_wheel_front_left'.
		Disable decal_mesh 'dm_wheel_front_right'.
		Disable decal_mesh 'dm_door_left'.
		Disable decal_mesh 'dm_door_right'.
		Disable body 'mover_blocker'.
	sequence 'state_vis_show':
		Show graphic_group 'grp_vehicle_whole'.
		Enable body 'body_body'.
		Enable body 'body_anim_blocker'.
		Enable body 'body_door_left'.
		Enable body 'body_door_right'.
		Enable body 'body_wheel_front_right'.
		Enable body 'body_wheel_front_left'.
		Enable body 'body_win_front'.
		Enable body 'body_win_left_front'.
		Enable body 'body_win_right_front'.
		Enable body 'body_win_left_rear'.
		Enable body 'body_win_right_rear'.
		Enable body 'body_win_back'.
		Disable body 'body_blocker_mover'.
		Enable decal_mesh 'dm_window_front'.
		Enable decal_mesh 'dm_door_left_window'.
		Enable decal_mesh 'dm_door_right_window'.
		Enable decal_mesh 'dm_window_rear_left'.
		Enable decal_mesh 'dm_window_rear_right'.
		Enable decal_mesh 'dm_window_back'.
		Enable decal_mesh 'dm_body'.
		Enable decal_mesh 'dm_wheel_front_left'.
		Enable decal_mesh 'dm_wheel_front_right'.
		Enable decal_mesh 'dm_door_left'.
		Enable decal_mesh 'dm_door_right'.
		Enable body 'mover_blocker'.
	sequence 'state_anim_blocker_disable':
		Disable body 'body_anim_blocker'.
	sequence 'state_anim_blocker_enable':
		Enable body 'body_anim_blocker'.
	sequence 'state_lights_siren_on':
		Enable object 'g_il_red'.
		Enable object 'g_il_blue'.
		Enable light 'li_light_blue'.
		Enable light 'li_light_red'.
		Enable animation_group 'show':
			from 0/30
			loop True
			speed 1.7
			to 33/30
	sequence 'state_lights_siren_off':
		Disable object 'g_il_red'.
		Disable object 'g_il_blue'.
		Disable light 'li_light_blue'.
		Disable light 'li_light_red'.
		Disable animation_group 'show'.
	sequence 'state_police_officers_off':
		Disable object 'g_police_la_driver_1'.
		Disable object 'g_police_la_driver_2'.
		Disable object 'g_police_la_driver_3'.
		Disable object 'g_police_la_driver_4'.
		Disable object 'g_police_la_passenger_1'.
		Disable object 'g_police_la_passenger_2'.
		Disable object 'g_police_la_passenger_3'.
		Disable object 'g_police_la_passenger_4'.
		Disable object 'g_police_la_back_left_1'.
		Disable object 'g_police_la_back_left_2'.
		Disable object 'g_police_la_back_left_3'.
		Disable object 'g_police_la_back_left_4'.
		Disable object 'g_police_la_back_right_1'.
		Disable object 'g_police_la_back_right_2'.
		Disable object 'g_police_la_back_right_3'.
		Disable object 'g_police_la_back_right_4'.
	sequence 'state_police_officers_on':
		Run sequence 'state_police_1_driver_on'.
	sequence 'state_police_1_driver_on':
		Enable object 'g_police_la_driver_1'.
		Disable object 'g_police_la_driver_2'.
		Disable object 'g_police_la_driver_3'.
		Disable object 'g_police_la_driver_4'.
	sequence 'state_police_1_driver_off':
		Disable object 'g_police_la_driver_1'.
	sequence 'state_police_2_driver_on':
		Disable object 'g_police_la_driver_1'.
		Enable object 'g_police_la_driver_2'.
		Disable object 'g_police_la_driver_3'.
		Disable object 'g_police_la_driver_4'.
	sequence 'state_police_2_driver_off':
		Disable object 'g_police_la_driver_2'.
	sequence 'state_police_3_driver_on':
		Disable object 'g_police_la_driver_1'.
		Disable object 'g_police_la_driver_2'.
		Enable object 'g_police_la_driver_3'.
		Disable object 'g_police_la_driver_4'.
	sequence 'state_police_3_driver_off':
		Disable object 'g_police_la_driver_3'.
	sequence 'state_police_4_driver_on':
		Disable object 'g_police_la_driver_1'.
		Disable object 'g_police_la_driver_2'.
		Disable object 'g_police_la_driver_3'.
		Enable object 'g_police_la_driver_4'.
	sequence 'state_police_4_driver_off':
		Disable object 'g_police_la_driver_4'.
	sequence 'state_police_1_passenger_on':
		Enable object 'g_police_la_passenger_1'.
		Disable object 'g_police_la_passenger_2'.
		Disable object 'g_police_la_passenger_3'.
		Disable object 'g_police_la_passenger_4'.
	sequence 'state_police_1_passenger_off':
		Disable object 'g_police_la_passenger_1'.
	sequence 'state_police_2_passenger_on':
		Disable object 'g_police_la_passenger_1'.
		Enable object 'g_police_la_passenger_2'.
		Disable object 'g_police_la_passenger_3'.
		Disable object 'g_police_la_passenger_4'.
	sequence 'state_police_2_passenger_off':
		Disable object 'g_police_la_passenger_2'.
	sequence 'state_police_3_passenger_on':
		Disable object 'g_police_la_passenger_1'.
		Disable object 'g_police_la_passenger_2'.
		Enable object 'g_police_la_passenger_3'.
		Disable object 'g_police_la_passenger_4'.
	sequence 'state_police_3_passenger_off':
		Disable object 'g_police_la_passenger_3'.
	sequence 'state_police_4_passenger_on':
		Disable object 'g_police_la_passenger_1'.
		Disable object 'g_police_la_passenger_2'.
		Disable object 'g_police_la_passenger_3'.
		Enable object 'g_police_la_passenger_4'.
	sequence 'state_police_4_passenger_off':
		Disable object 'g_police_la_passenger_4'.
	sequence 'state_police_1_back_left_on':
		Enable object 'g_police_la_back_left_1'.
		Disable object 'g_police_la_back_left_2'.
		Disable object 'g_police_la_back_left_3'.
		Disable object 'g_police_la_back_left_4'.
	sequence 'state_police_1_back_left_off':
		Disable object 'g_police_la_back_left_1'.
	sequence 'state_police_2_back_left_on':
		Disable object 'g_police_la_back_left_1'.
		Enable object 'g_police_la_back_left_2'.
		Disable object 'g_police_la_back_left_3'.
		Disable object 'g_police_la_back_left_4'.
	sequence 'state_police_2_back_left_off':
		Disable object 'g_police_la_back_left_2'.
	sequence 'state_police_3_back_left_on':
		Disable object 'g_police_la_back_left_1'.
		Disable object 'g_police_la_back_left_2'.
		Enable object 'g_police_la_back_left_3'.
		Disable object 'g_police_la_back_left_4'.
	sequence 'state_police_3_back_left_off':
		Disable object 'g_police_la_back_left_3'.
	sequence 'state_police_4_back_left_on':
		Disable object 'g_police_la_back_left_1'.
		Disable object 'g_police_la_back_left_2'.
		Disable object 'g_police_la_back_left_3'.
		Enable object 'g_police_la_back_left_4'.
	sequence 'state_police_4_back_left_off':
		Disable object 'g_police_la_back_left_4'.
	sequence 'state_police_1_back_right_on':
		Enable object 'g_police_la_back_right_1'.
		Disable object 'g_police_la_back_right_2'.
		Disable object 'g_police_la_back_right_3'.
		Disable object 'g_police_la_back_right_4'.
	sequence 'state_police_1_back_right_off':
		Disable object 'g_police_la_back_right_1'.
	sequence 'state_police_2_back_right_on':
		Disable object 'g_police_la_back_right_1'.
		Enable object 'g_police_la_back_right_2'.
		Disable object 'g_police_la_back_right_3'.
		Disable object 'g_police_la_back_right_4'.
	sequence 'state_police_2_back_right_off':
		Disable object 'g_police_la_back_right_2'.
	sequence 'state_police_3_back_right_on':
		Disable object 'g_police_la_back_right_1'.
		Disable object 'g_police_la_back_right_2'.
		Enable object 'g_police_la_back_right_3'.
		Disable object 'g_police_la_back_right_4'.
	sequence 'state_police_3_back_right_off':
		Disable object 'g_police_la_back_right_3'.
	sequence 'state_police_4_back_right_on':
		Disable object 'g_police_la_back_right_1'.
		Disable object 'g_police_la_back_right_2'.
		Disable object 'g_police_la_back_right_3'.
		Enable object 'g_police_la_back_right_4'.
	sequence 'state_police_4_back_right_off':
		Disable object 'g_police_la_back_right_4'.
	sequence '__reset_animation__':
		Call function: base.move_to_stored_pos()
		Run sequence 'state_lights_siren_off'.
		Enable body 'body_anim_blocker'.
		Enable animation_group 'anim_door_right':
			from 0
			to 0
		Enable animation_group 'anim_door_left':
			from 0
			to 0
	sequence '__allow_sync_reset_animation__':
		Call function: base.allow_sync_stored_pos('true')
	sequence 'anim_door_left_open':
		Enable animation_group 'anim_door_left'.
	sequence 'anim_door_right_open':
		Enable animation_group 'anim_door_right'.
	sequence 'anim_door_left_close':
		Enable animation_group 'anim_door_left':
			from 10/30
			speed -1
			to 0
	sequence 'anim_door_left_close_half':
		Enable animation_group 'anim_door_left':
			from 10/30
			speed -1
			to 6.5/30
	sequence 'anim_door_right_close':
		Enable animation_group 'anim_door_right':
			from 10/30
			speed -1
			to 0
	sequence 'anim_mus_arrive_1':
		Call function: base.store_current_pos()
		animation_redirect 'car_mus_arrive_1'.
		Run sequence 'state_lights_siren_on'.
		Play audio 'cop_car_01' at 'cop_siren'.
		Run sequence 'done_anim_police_responce'. (DELAY 610/30)
		Enable animation_group 'anim_door_right'. (DELAY 610/30)
		Enable animation_group 'anim_door_left'. (DELAY 610/30)
	sequence 'anim_mus_arrive_2':
		Call function: base.store_current_pos()
		animation_redirect 'car_mus_arrive_2'.
		Run sequence 'state_lights_siren_on'.
		Play audio 'cop_car_02' at 'cop_siren'.
		Run sequence 'done_anim_police_responce'. (DELAY 735/30)
		Enable animation_group 'anim_door_right'. (DELAY 725/30)
		Enable animation_group 'anim_door_left'. (DELAY 725/30)
	sequence 'anim_hlm_arrive_1':
		Call function: base.store_current_pos()
		animation_redirect 'car_hlm_arrive_1'.
		Run sequence 'state_lights_siren_on'.
		Play audio 'cop_car_01' at 'cop_siren'.
		Run sequence 'done_anim_police_responce'. (DELAY 540/30)
		Enable animation_group 'anim_door_right'. (DELAY 540/30)
		Enable animation_group 'anim_door_left'. (DELAY 540/30)
	sequence 'anim_hlm_arrive_2':
		Call function: base.store_current_pos()
		animation_redirect 'car_hlm_arrive_2'.
		Run sequence 'state_lights_siren_on'.
		Play audio 'cop_car_01' at 'cop_siren'.
		Run sequence 'done_anim_police_responce'. (DELAY 485/30)
		Enable animation_group 'anim_door_right'. (DELAY 485/30)
		Enable animation_group 'anim_door_left'. (DELAY 485/30)
	sequence 'anim_hlm_arrive_3':
		Call function: base.store_current_pos()
		animation_redirect 'car_hlm_arrive_3'.
		Run sequence 'state_lights_siren_on'.
		Play audio 'cop_car_01' at 'cop_siren'.
		Run sequence 'done_anim_police_responce'. (DELAY 345/30)
		Enable animation_group 'anim_door_right'. (DELAY 345/30)
		Enable animation_group 'anim_door_left'. (DELAY 345/30)
	sequence 'anim_hlm_arrive_4':
		Call function: base.store_current_pos()
		animation_redirect 'car_hlm_arrive_4'.
		Run sequence 'state_lights_siren_on'.
		Play audio 'cop_car_01' at 'cop_siren'.
		Run sequence 'done_anim_police_responce'. (DELAY 345/30)
		Enable animation_group 'anim_door_right'. (DELAY 345/30)
		Enable animation_group 'anim_door_left'. (DELAY 345/30)
	sequence 'anim_car_hox_move_away':
		Call function: base.store_current_pos()
		animation_redirect 'car_hox_move_away'.
		Play audio 'cop_car_moveaway' at 'snd'.
		Disable body 'mover_blocker'.
		Run sequence 'done_anim_police_responce'. (DELAY 530/30)
	sequence 'anim_car_hox_intro_skidding':
		Call function: base.store_current_pos()
		animation_redirect 'car_hox_intro_skidding'.
		Play audio 'cop_car_skidding' at 'snd'.
		Run sequence 'done_anim_police_responce'. (DELAY 125/30)
	sequence 'anim_pal_arrive_1':
		Call function: base.store_current_pos()
		animation_redirect 'car_pal_arrive_1'.
		Run sequence 'state_lights_siren_on'.
		Play audio 'cop_car_01' at 'cop_siren'.
		Run sequence 'done_anim_police_responce'. (DELAY 569/30)
		Enable animation_group 'anim_door_right'. (DELAY 569/30)
		Enable animation_group 'anim_door_left'. (DELAY 569/30)
	sequence 'anim_pal_arrive_2':
		Call function: base.store_current_pos()
		animation_redirect 'car_pal_arrive_2'.
		Run sequence 'state_lights_siren_on'.
		Play audio 'cop_car_01' at 'cop_siren'.
		Run sequence 'done_anim_police_responce'. (DELAY 556/30)
		Enable animation_group 'anim_door_right'. (DELAY 556/30)
		Enable animation_group 'anim_door_left'. (DELAY 556/30)
	sequence 'anim_bor_arrive_1':
		Call function: base.store_current_pos()
		animation_redirect 'car_bor_arrive_1'.
		Run sequence 'state_lights_siren_on'.
		Play audio 'cop_car_01' at 'cop_siren'.
		Run sequence 'done_anim_police_responce'. (DELAY 395/30)
		Enable animation_group 'anim_door_right'. (DELAY 395/30)
		Enable animation_group 'anim_door_left'. (DELAY 395/30)
	sequence 'anim_bor_arrive_2':
		Call function: base.store_current_pos()
		animation_redirect 'car_bor_arrive_2'.
		Run sequence 'state_lights_siren_on'.
		Play audio 'cop_car_01' at 'cop_siren'.
		Run sequence 'done_anim_police_responce'. (DELAY 310/30)
		Enable animation_group 'anim_door_right'. (DELAY 310/30)
		Enable animation_group 'anim_door_left'. (DELAY 310/30)
		Play audio 'police_car_born_arrive_01' at 'snd'.
	sequence 'anim_gen_whizz_by_160m':
		Call function: base.store_current_pos()
		animation_redirect 'car_gen_whizz_by_160m'.
		Run sequence 'state_lights_siren_on'.
		Run sequence 'state_police_officers_on'.
		Run sequence 'state_anim_blocker_enable'.
		Play audio 'cop_car_01' at 'cop_siren'.
		Run sequence 'done_anim_police_responce'. (DELAY 181/30)
	sequence 'anim_car_rvd_arrive_01':
		Call function: base.store_current_pos()
		animation_redirect 'car_rvd_arrive_01'.
		Run sequence 'state_lights_siren_on'.
		Run sequence 'state_police_officers_on'.
		Run sequence 'state_anim_blocker_enable'.
		Play audio 'car_police_rvd_01' at 'cop_siren'.
		Run sequence 'done_anim_police_responce'. (DELAY 400/30)
	sequence 'anim_car_rvd_arrive_02':
		Call function: base.store_current_pos()
		animation_redirect 'car_rvd_arrive_02'.
		Run sequence 'state_lights_siren_on'.
		Run sequence 'state_police_officers_on'.
		Run sequence 'state_anim_blocker_enable'.
		Play audio 'car_police_rvd_02' at 'cop_siren'.
		Run sequence 'done_anim_police_responce'. (DELAY 310/30)
	sequence 'anim_car_rvd_arrive_03':
		Call function: base.store_current_pos()
		animation_redirect 'car_rvd_arrive_03'.
		Run sequence 'state_lights_siren_on'.
		Run sequence 'state_police_officers_on'.
		Run sequence 'state_anim_blocker_enable'.
		Play audio 'car_police_rvd_03' at 'cop_siren'.
		Run sequence 'done_anim_police_responce'. (DELAY 350/30)
	sequence 'anim_car_rvd_arrive_04':
		Call function: base.store_current_pos()
		animation_redirect 'car_rvd_arrive_04'.
		Run sequence 'state_lights_siren_on'.
		Run sequence 'state_police_officers_on'.
		Run sequence 'state_anim_blocker_enable'.
		Play audio 'cop_car_01' at 'cop_siren'.
		Run sequence 'done_anim_police_responce'. (DELAY 300/30)
	sequence 'anim_car_rvd_arrive_05':
		Call function: base.store_current_pos()
		animation_redirect 'car_rvd_arrive_05'.
		Run sequence 'state_lights_siren_on'.
		Run sequence 'state_police_officers_on'.
		Run sequence 'state_anim_blocker_enable'.
		Play audio 'cop_car_01' at 'cop_siren'.
		Run sequence 'done_anim_police_responce'. (DELAY 240/30)
	sequence 'anim_car_rvd_crash_02':
		Call function: base.store_current_pos()
		animation_redirect 'car_rvd_crash_01'.
		Run sequence 'state_lights_siren_on'.
		Run sequence 'state_police_officers_on'.
		Run sequence 'state_anim_blocker_enable'.
		Play audio 'car_police_rvd_crash_01' at 'snd'.
		Run sequence 'done_anim_police_responce'. (DELAY 490/30)
	sequence 'anim_car_rvd_crash_01':
		Call function: base.store_current_pos()
		animation_redirect 'car_rvd_crash_02'.
		Run sequence 'state_lights_siren_on'.
		Run sequence 'state_police_officers_on'.
		Run sequence 'state_anim_blocker_enable'.
		Play audio 'car_police_rvd_crash_02' at 'snd'.
		Run sequence 'done_anim_police_responce'. (DELAY 600/30)
	sequence 'anim_car_police_rvd_day2_arrive_01':
		Call function: base.store_current_pos()
		animation_redirect 'car_police_rvd_day2_arrive_01'.
		Run sequence 'state_lights_siren_on'.
		Run sequence 'state_police_officers_on'.
		Run sequence 'state_anim_blocker_enable'.
		Play audio 'car_police_rvd_day2_01' at 'cop_siren'. (DELAY 50/30)
		Run sequence 'done_anim_police_responce'. (DELAY 225/30)
	sequence 'anim_car_police_rvd_day2_arrive_02':
		Call function: base.store_current_pos()
		animation_redirect 'car_police_rvd_day2_arrive_02'.
		Run sequence 'state_lights_siren_on'.
		Run sequence 'state_police_officers_on'.
		Run sequence 'state_anim_blocker_enable'.
		Play audio 'car_police_rvd_day2_02' at 'cop_siren'. (DELAY 80/30)
		Run sequence 'done_anim_police_responce'. (DELAY 300/30)
	sequence 'anim_car_police_rvd_day2_arrive_03':
		Call function: base.store_current_pos()
		animation_redirect 'car_police_rvd_day2_arrive_03'.
		Run sequence 'state_lights_siren_on'.
		Run sequence 'state_police_officers_on'.
		Run sequence 'state_anim_blocker_enable'.
		Play audio 'car_police_rvd_day2_03' at 'cop_siren'. (DELAY 70/30)
		Run sequence 'done_anim_police_responce'. (DELAY 305/30)
	sequence 'anim_car_police_rvd_day2_arrive_04':
		Call function: base.store_current_pos()
		animation_redirect 'car_police_rvd_day2_arrive_04'.
		Run sequence 'state_lights_siren_on'.
		Run sequence 'state_police_officers_on'.
		Run sequence 'state_anim_blocker_enable'.
		Play audio 'cop_car_01' at 'cop_siren'.
		Run sequence 'done_anim_police_responce'. (DELAY 320/30)
	sequence 'anim_car_police_rvd_day2_arrive_05':
		Call function: base.store_current_pos()
		animation_redirect 'car_police_rvd_day2_arrive_05'.
		Run sequence 'state_lights_siren_on'.
		Run sequence 'state_police_officers_on'.
		Run sequence 'state_anim_blocker_enable'.
		Play audio 'car_police_rvd_day2_05' at 'cop_siren'.
		Run sequence 'done_anim_police_responce'. (DELAY 220/30)
	sequence 'done_anim_police_responce':
		Disable body 'body_anim_blocker'.
	sequence 'int_seq_win_left_front':
		TRIGGER TIMES 1
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_front_left' )
			position v()
		Play audio 'window_small_shatter' at 'e_window_front_left'.
		Disable object 'g_door_left_window'.
		Disable object 'g_door_left_window_lod'.
		Disable body 'body_win_left_front'.
		Disable decal_mesh 'dm_door_left_window'.
	sequence 'int_seq_win_right_front':
		TRIGGER TIMES 1
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_front_right' )
			position v()
		Play audio 'window_small_shatter' at 'e_window_front_right'.
		Disable object 'g_door_right_window'.
		Disable object 'g_door_right_window_lod'.
		Disable body 'body_win_right_front'.
		Disable decal_mesh 'dm_door_right_window'.
	sequence 'int_seq_win_left_rear':
		TRIGGER TIMES 1
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_rear_left' )
			position v()
		Play audio 'window_small_shatter' at 'e_window_rear_left'.
		Disable object 'g_window_rear_left'.
		Disable object 'g_window_rear_left_lod'.
		Disable body 'body_win_left_rear'.
		Disable decal_mesh 'dm_window_rear_left'.
	sequence 'int_seq_win_right_rear':
		TRIGGER TIMES 1
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_window_rear_right' )
			position v()
		Play audio 'window_small_shatter' at 'e_window_rear_right'.
		Disable object 'g_window_rear_right'.
		Disable object 'g_window_rear_right_lod'.
		Disable body 'body_win_right_rear'.
		Disable decal_mesh 'dm_window_rear_right'.
	sequence 'int_seq_win_front':
		TRIGGER TIMES 1
		Disable object 'g_window_front'.
		Disable object 'g_window_front_lod'.
		Enable object 'g_window_front_dmg'.
	sequence 'int_seq_win_back':
		TRIGGER TIMES 1
		Disable object 'g_window_back'.
		Disable object 'g_window_back_lod'.
		Enable object 'g_window_back_dmg'.
	body 'body_win_left_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'int_seq_win_left_front'.
	body 'body_win_right_front'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'int_seq_win_right_front'.
	body 'body_win_left_rear'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'int_seq_win_left_rear'.
	body 'body_win_right_rear'
		Upon receiving 2 bullet hits or 10 explosion damage, execute:
			Run sequence 'int_seq_win_right_rear'.
	body 'body_win_front'
		Upon receiving 5 bullet hits or 10 explosion damage, execute:
			Run sequence 'int_seq_win_front'.
	body 'body_win_back'
		Upon receiving 5 bullet hits or 10 explosion damage, execute:
			Run sequence 'int_seq_win_back'.
