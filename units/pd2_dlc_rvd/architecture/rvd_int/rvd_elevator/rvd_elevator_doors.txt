unit:
	sequence 'open_doors':
		Enable animation_group 'anim_door_left':
			from 0/30
			speed 1
			to 30/30
		Enable animation_group 'anim_door_right':
			from 0/30
			speed 1
			to 30/30
		Play audio 'bar_elevator_crowbar_open_finish' at 'interact'.
	sequence 'close_doors':
		Enable animation_group 'anim_door_left':
			from 31/30
			speed 1
			to 60/30
		Enable animation_group 'anim_door_right':
			from 31/30
			speed 1
			to 60/30
	sequence 'enable_interaction':
		Enable interaction.
		Show graphic_group 'icons'.
	sequence 'disable_interaction':
		Disable interaction.
		Hide graphic_group 'icons'.
	sequence 'interact':
		Run sequence 'open_doors'.
