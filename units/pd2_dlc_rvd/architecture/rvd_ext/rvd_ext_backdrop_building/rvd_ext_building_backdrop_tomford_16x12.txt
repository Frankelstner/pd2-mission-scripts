unit:
	sequence 'destroy_01':
		TRIGGER TIMES 1
		Disable object 'g_glass_05'.
		Enable object 'g_glass_05_shattered'.
		Play audio 'glass_crack' at 'e_glass_01'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_01' (alarm reason: 'glass').
	sequence 'destroy_02':
		TRIGGER TIMES 1
		Disable object 'g_glass_05'.
		Disable object 'g_glass_05_shattered'.
		Play audio 'window_large_shatter' at 'e_glass_01'.
		effect 'effects/payday2/particles/window/mallcrasher_window_large':
			parent object('e_glass_01')
			position v()
		Disable body 'window1'.
		Cause alert with 12 m radius.
	sequence 'destroy_03':
		TRIGGER TIMES 1
		Disable object 'g_glass_06'.
		Enable object 'g_glass_06_shattered'.
		Play audio 'glass_crack' at 'e_glass_02'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_02' (alarm reason: 'glass').
	sequence 'destroy_04':
		TRIGGER TIMES 1
		Disable object 'g_glass_06'.
		Disable object 'g_glass_06_shattered'.
		Play audio 'window_large_shatter' at 'e_glass_02'.
		effect 'effects/payday2/particles/window/mallcrasher_window_large':
			parent object('e_glass_02')
			position v()
		Disable body 'window2'.
		Cause alert with 12 m radius.
	body 'window1'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_01'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_02'.
	body 'window2'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_03'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_04'.
