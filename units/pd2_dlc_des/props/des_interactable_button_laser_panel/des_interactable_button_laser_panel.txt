unit:
	sequence 'int_seq_anim_button':
		Enable animation_group 'anim_button':
			from 0/30
			to 30/30
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'interact':
		Run sequence 'int_seq_anim_button'.
