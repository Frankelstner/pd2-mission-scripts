unit:
	sequence 'show':
		Show graphic_group 'grp_box'.
		Show graphic_group 'grp_crowbar'.
		Enable decal_mesh 'dm_wood'.
		Enable decal_mesh 'dm_wood_01'.
		Enable body 'body_static'.
		Enable body 'body_collision_01'.
	sequence 'hide':
		Hide graphic_group 'grp_box'.
		Hide graphic_group 'grp_crowbar'.
		Disable decal_mesh 'dm_wood'.
		Disable decal_mesh 'dm_wood_01'.
		Disable body 'body_static'.
		Disable body 'body_collision_01'.
	sequence 'anim_open_lid':
		Enable animation_group 'anim_lid':
			from 0/30
			to 35/30
	sequence 'interact':
		Hide graphic_group 'grp_crowbar'.
		Run sequence 'anim_open_lid'.
	sequence 'enable_interaction':
		Enable interaction.
		Show graphic_group 'grp_crowbar'.
	sequence 'disable_interaction':
		Disable interaction.
		Hide graphic_group 'grp_crowbar'.
