unit:
	sequence 'spawn_bag_loot':
		spawn_unit 'units/pd2_dlc_des/props/des_prop_conveyor_belt/spawn_animated_bag_anim_02':
			position object_pos('rp_des_prop_conveyor_belt_02')
			rotation object_rot('rp_des_prop_conveyor_belt_02')
	sequence 'spawn_bag_special':
		spawn_unit 'units/pd2_dlc_des/props/des_prop_conveyor_belt/spawn_animated_bag_special_anim_02':
			position object_pos('rp_des_prop_conveyor_belt_02')
			rotation object_rot('rp_des_prop_conveyor_belt_02')
	sequence 'spawn_bag_tool':
		spawn_unit 'units/pd2_dlc_des/props/des_prop_conveyor_belt/spawn_animated_bag_tool_anim_02':
			position object_pos('rp_des_prop_conveyor_belt_02')
			rotation object_rot('rp_des_prop_conveyor_belt_02')
