unit:
	sequence 'break_window_01':
		Run sequence 'int_seq_break_window_01'.
	body 'body_glass_01'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_crack_window_01'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_window_01'.
	sequence 'int_seq_crack_window_01':
		TRIGGER TIMES 1
		Disable object 'g_glass_01'.
		Enable object 'g_glass_01_cracked'.
		Play audio 'glass_crack' at 'e_glass_01'.
	sequence 'int_seq_break_window_01':
		TRIGGER TIMES 1
		Disable object 'g_glass_01'.
		Disable object 'g_glass_01_cracked'.
		Disable body 'body_glass_01'.
		Disable decal_mesh 'dm_glass_breakable_01'.
		effect 'effects/payday2/particles/window/storefront_window_large':
			parent object( 'e_glass_01' )
			position v()
		Play audio 'window_medium_shatter' at 'e_glass_01'.
