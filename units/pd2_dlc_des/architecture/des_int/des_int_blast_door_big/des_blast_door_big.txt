unit:
	sequence 'anim_door_open':
		Enable animation_group 'open':
			from 0/30
			to 400/30
		Play audio 'des_big_door_anim_open_L' at 'snd_left'.
		Play audio 'des_big_door_anim_open_R' at 'snd_right'.
	sequence 'anim_door_close':
		Enable animation_group 'open':
			from 400/30
			to 510/30
		Play audio 'des_big_door_anim_close_L' at 'snd_left'.
		Play audio 'des_big_door_anim_close_R' at 'snd_right'.
	sequence 'state_door_open':
		Enable animation_group 'open':
			from 400/30
			to 400/30
	sequence 'state_door_closed':
		Enable animation_group 'open':
			from 0/30
			to 0/30
