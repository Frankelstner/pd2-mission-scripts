unit:
	sequence 'state_light_off':
		Disable animation_group 'illum'.
		Disable light 'li_light1'.
		Disable light 'li_light2'.
	sequence 'state_light_green':
		Enable animation_group 'illum':
			from 0/30
			speed 0
			to 4/30
		Enable light 'li_light1'.
		Enable light 'li_light2'.
	sequence 'state_light_red':
		Enable animation_group 'illum':
			from 5/30
			speed 0
			to 5/30
		Enable light 'li_light1'.
		Enable light 'li_light2'.
	sequence 'state_light_red_blink':
		Enable animation_group 'illum':
			from 5/30
			loop True
			speed 2
			to 120/30
		Enable light 'li_light1'.
		Enable light 'li_light2'.
	sequence 'anim_door_open':
		Enable animation_group 'open':
			from 0/30
			speed 1
			to 340/30
		Play audio 'des_small_door_open' at 'rp_des_int_blast_door_small'.
	sequence 'anim_door_close':
		Enable animation_group 'open':
			from 340/30
			speed -1
			to 0/30
		Play audio 'des_small_door_close' at 'rp_des_int_blast_door_small'.
