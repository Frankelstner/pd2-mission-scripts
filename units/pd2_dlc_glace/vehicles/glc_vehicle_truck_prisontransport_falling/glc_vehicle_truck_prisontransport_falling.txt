unit:
	sequence 'anim_open_rear_doors':
		Enable animation_group 'anim_open_doors':
			from 0/30
			to 15/30
		Play audio 'bar_prison_transport_door_open_finished' at 'snd_doors'.
	sequence 'state_crashed':
		Enable animation_group 'anim_car':
			from 2/30
			to 2/30
	sequence 'state_show':
		Show graphic_group 'g_g'.
		Show graphic_group 'door_f_l'.
		Show graphic_group 'door_f_r'.
		Show graphic_group 'door_r_r'.
		Show graphic_group 'door_r_l'.
		Enable object 's_s'.
		Enable object 's_door_f_left'.
		Enable object 's_door_f_right'.
		Enable object 's_door_r_left'.
		Enable object 's_door_r_right'.
		Enable decal_mesh 'dm_cloth_stuffed'.
		Enable decal_mesh 'dm_glass_unbreakable_door_f_left'.
		Enable decal_mesh 'dm_glass_unbreakable_door_f_right'.
		Enable decal_mesh 'dm_glass_unbreakable_door_r_right'.
		Enable decal_mesh 'dm_glass_unbreakable_door_r_left'.
		Enable decal_mesh 'dm_glass_unbreakable'.
		Enable decal_mesh 'dm_metal'.
		Enable decal_mesh 'dm_metal_door_f_left'.
		Enable decal_mesh 'dm_metal_door_f_right'.
		Enable decal_mesh 'dm_metal_door_r_right'.
		Enable decal_mesh 'dm_metal_door_r_left'.
		Enable decal_mesh 'dm_plastic'.
		Enable decal_mesh 'dm_plastic_door_f_left'.
		Enable decal_mesh 'dm_plastic_door_f_right'.
		Enable decal_mesh 'dm_rubber'.
		Enable body 'body_animated'.
		Enable body 'body_collision_01'.
		Enable body 'body_collision_02'.
		Enable body 'body_collision_03'.
		Enable body 'body_collision_04'.
	sequence 'state_hide':
		Hide graphic_group 'g_g'.
		Hide graphic_group 'door_f_l'.
		Hide graphic_group 'door_f_r'.
		Hide graphic_group 'door_r_r'.
		Hide graphic_group 'door_r_l'.
		Disable object 's_s'.
		Disable object 's_door_f_left'.
		Disable object 's_door_f_right'.
		Disable object 's_door_r_left'.
		Disable object 's_door_r_right'.
		Disable decal_mesh 'dm_cloth_stuffed'.
		Disable decal_mesh 'dm_glass_unbreakable_door_f_left'.
		Disable decal_mesh 'dm_glass_unbreakable_door_f_right'.
		Disable decal_mesh 'dm_glass_unbreakable_door_r_right'.
		Disable decal_mesh 'dm_glass_unbreakable_door_r_left'.
		Disable decal_mesh 'dm_glass_unbreakable'.
		Disable decal_mesh 'dm_metal'.
		Disable decal_mesh 'dm_metal_door_f_left'.
		Disable decal_mesh 'dm_metal_door_f_right'.
		Disable decal_mesh 'dm_metal_door_r_right'.
		Disable decal_mesh 'dm_metal_door_r_left'.
		Disable decal_mesh 'dm_plastic'.
		Disable decal_mesh 'dm_plastic_door_f_left'.
		Disable decal_mesh 'dm_plastic_door_f_right'.
		Disable decal_mesh 'dm_rubber'.
		Disable body 'body_animated'.
		Disable body 'body_collision_01'.
		Disable body 'body_collision_02'.
		Disable body 'body_collision_03'.
		Disable body 'body_collision_04'.
	sequence 'anim_slide_01':
		Enable animation_group 'anim_car':
			from 2/30
			to 150/30
		Play audio 'prison_transport_sliding' at 'src_slide'.
		Enable effect_spawner 'es_e_sparks_01'.
		Enable effect_spawner 'es_e_sparks_02'.
		Disable effect_spawner 'es_e_sparks_01'. (DELAY 148/30)
		Disable effect_spawner 'es_e_sparks_02'. (DELAY 148/30)
		Run sequence 'done_anim_slide_01'. (DELAY 148/30)
	sequence 'done_anim_slide_01'.
	sequence 'anim_slide_02':
		Enable animation_group 'anim_car':
			from 150/30
			to 325/30
		Play audio 'prison_transport_falling' at 'src_slide'.
		Play audio 'prison_transport_falling_rubble' at 'src_rubble'.
		Play audio 'friend_car_impact_water' at 'src_slide'. (DELAY 160/30)
		Enable effect_spawner 'es_e_sparks_01'. (DELAY 35/30)
		Enable effect_spawner 'es_e_sparks_03'. (DELAY 35/30)
		Disable effect_spawner 'es_e_sparks_01'. (DELAY 70/30)
		Disable effect_spawner 'es_e_sparks_03'. (DELAY 70/30)
	sequence 'hide_doors':
		Hide graphic_group 'door_r_r'.
		Hide graphic_group 'door_r_l'.
		Disable body 'body_collision_03'.
		Disable body 'body_collision_04'.
		Disable decal_mesh 'dm_glass_unbreakable_door_r_right'.
		Disable decal_mesh 'dm_glass_unbreakable_door_r_left'.
		Disable decal_mesh 'dm_metal_door_r_right'.
		Disable decal_mesh 'dm_metal_door_r_left'.
	sequence 'interact'.
	sequence 'activate_door':
		Call function: base.activate()
	sequence 'deactivate_door':
		Call function: base.deactivate()
	sequence 'c4_placed'.
	sequence 'all_c4_placed'.
	sequence 'c4_completed'.
	sequence 'door_opened'.
	sequence 'explode_door':
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'e_effect' )
			position v()
		Play audio 'c4_explode_metal' at 'e_effect'.
		spawn_unit 'units/pd2_dlc_glace/vehicles/glc_vehicle_truck_prisontransport/spawn_right_door':
			position object_pos('rp_glc_vehicle_truck_prisontransport_falling')
			rotation object_rot('rp_glc_vehicle_truck_prisontransport_falling')
		spawn_unit 'units/pd2_dlc_glace/vehicles/glc_vehicle_truck_prisontransport/spawn_left_door':
			position object_pos('rp_glc_vehicle_truck_prisontransport_falling')
			rotation object_rot('rp_glc_vehicle_truck_prisontransport_falling')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('e_effect') ),100,20).
		Run sequence 'hide_doors'.
