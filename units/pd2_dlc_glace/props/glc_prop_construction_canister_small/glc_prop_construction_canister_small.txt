unit:
	sequence 'show':
		Show graphic_group 'canister'.
		Enable decal_mesh 'dm_metal'.
		Enable body 'body_static'.
	sequence 'hide':
		Hide graphic_group 'canister'.
		Disable decal_mesh 'dm_metal'.
		Disable body 'body_static'.
	sequence 'destroy':
		Run sequence 'hide'.
		Call function: base.detonate(object_pos( 'e_effect' ),700,250,5)
		spawn_unit 'units/pd2_dlc_glace/props/glc_prop_construction_canister_small/spawn_prop_construction_canister_small_debris':
			position object_pos('rp_glc_prop_construction_canister_small')
			rotation rot(0, 0, 0)
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('e_effect') ),100,50).
		effect 'effects/particles/explosions/explosion_grenade':
			parent object( 'e_effect' )
			position v()
	body 'body_static'
		Upon receiving 100 damage, execute:
			Run sequence 'destroy'.
