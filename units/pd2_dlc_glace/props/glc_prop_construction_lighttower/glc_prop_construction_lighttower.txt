unit:
	sequence 'show':
		Enable body 'body_static'.
		Enable body 'body_lamp_01'.
		Enable body 'body_lamp_02'.
		Enable body 'body_lamp_03'.
		Enable body 'body_lamp_04'.
		Disable body 'body_collision_dyn_01'.
		Enable body 'body_collision_01'.
		Disable body 'body_collision_dyn_02'.
		Enable body 'body_collision_02'.
		Disable body 'body_collision_dyn_03'.
		Enable body 'body_collision_03'.
		Enable body 'body_collision_04'.
		Disable body 'body_collision_dyn_04'.
		Enable body 'body_collision_05'.
		Disable body 'body_collision_dyn_05'.
		Show graphic_group 'g_lamp'.
		Show graphic_group 'g_lamp_1'.
		Show graphic_group 'g_lamp_2'.
		Show graphic_group 'g_lamp_3'.
		Show graphic_group 'g_lamp_4'.
		Hide graphic_group 'g_dmg_lamp_1'.
		Hide graphic_group 'g_dmg_lamp_2'.
		Hide graphic_group 'g_dmg_lamp_3'.
		Hide graphic_group 'g_dmg_lamp_4'.
		Disable decal_mesh 'dm_glass_breakable'.
		Enable decal_mesh 'dm_metal'.
		Enable object 'g_lamp_1_il'.
		Enable object 'g_lamp_2_il'.
		Enable object 'g_lamp_3_il'.
		Enable object 'g_lamp_4_il'.
		Enable light 'ls_spot_01'.
		Enable light 'ls_spot_02'.
		Enable light 'ls_spot_03'.
		Enable light 'ls_spot_04'.
	sequence 'hide':
		Disable body 'body_static'.
		Disable body 'body_lamp_01'.
		Disable body 'body_lamp_02'.
		Disable body 'body_lamp_03'.
		Disable body 'body_lamp_04'.
		Disable body 'body_collision_dyn_01'.
		Disable body 'body_collision_01'.
		Disable body 'body_collision_dyn_02'.
		Disable body 'body_collision_02'.
		Disable body 'body_collision_dyn_03'.
		Disable body 'body_collision_03'.
		Disable body 'body_collision_04'.
		Disable body 'body_collision_dyn_04'.
		Disable body 'body_collision_05'.
		Disable body 'body_collision_dyn_05'.
		Hide graphic_group 'g_lamp'.
		Hide graphic_group 'g_lamp_1'.
		Hide graphic_group 'g_lamp_2'.
		Hide graphic_group 'g_lamp_3'.
		Hide graphic_group 'g_lamp_4'.
		Hide graphic_group 'g_dmg_lamp_1'.
		Hide graphic_group 'g_dmg_lamp_2'.
		Hide graphic_group 'g_dmg_lamp_3'.
		Hide graphic_group 'g_dmg_lamp_4'.
		Disable decal_mesh 'dm_glass_breakable'.
		Disable decal_mesh 'dm_metal'.
		Disable object 'g_lamp_1_il'.
		Disable object 'g_lamp_2_il'.
		Disable object 'g_lamp_3_il'.
		Disable object 'g_lamp_4_il'.
		Disable light 'ls_spot_01'.
		Disable light 'ls_spot_02'.
		Disable light 'ls_spot_03'.
		Disable light 'ls_spot_04'.
	sequence 'destroy_1':
		Disable body 'body_collision_04'.
		Enable body 'body_collision_dyn_04'.
		Hide graphic_group 'g_lamp_1'.
		Show graphic_group 'g_dmg_lamp_1'.
		Disable light 'ls_spot_01'.
		Disable object 'g_lamp_1_il'.
	sequence 'destroy_2':
		Disable body 'body_collision_05'.
		Enable body 'body_collision_dyn_05'.
		Hide graphic_group 'g_lamp_2'.
		Show graphic_group 'g_dmg_lamp_2'.
		Disable light 'ls_spot_02'.
		Disable object 'g_lamp_2_il'.
	sequence 'destroy_3':
		Enable body 'body_collision_dyn_01'.
		Disable body 'body_collision_01'.
		Hide graphic_group 'g_lamp_3'.
		Show graphic_group 'g_dmg_lamp_3'.
		Disable light 'ls_spot_03'.
		Disable object 'g_lamp_3_il'.
	sequence 'destroy_4':
		Enable body 'body_collision_dyn_02'.
		Disable body 'body_collision_02'.
		Enable body 'body_collision_dyn_03'.
		Disable body 'body_collision_03'.
		Hide graphic_group 'g_lamp_4'.
		Show graphic_group 'g_dmg_lamp_4'.
		Disable light 'ls_spot_04'.
		Disable object 'g_lamp_4_il'.
	body 'body_lamp_01'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_2'.
	body 'body_lamp_02'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_1'.
	body 'body_lamp_03'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_3'.
	body 'body_lamp_04'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy_4'.
