unit:
	sequence 'show':
		Disable body 'body_collision_dyn_01'.
		Enable body 'body_collision_01'.
		Disable body 'body_collision_dyn_02'.
		Enable body 'body_collision_02'.
		Disable body 'body_collision_dyn_03'.
		Enable body 'body_collision_03'.
		Enable body 'body_collision_04'.
		Disable body 'body_collision_dyn_04'.
		Enable body 'body_collision_05'.
		Disable body 'body_collision_dyn_05'.
		Enable body 'body_collision_06'.
		Disable body 'body_collision_dyn_06'.
		Enable body 'body_collision_07'.
		Disable body 'body_collision_dyn_07'.
		Show graphic_group 'g_lamp'.
		Hide graphic_group 'g_lamp_broken'.
		Enable decal_mesh 'dm_glass_breakable'.
		Enable decal_mesh 'dm_metal'.
		Enable object 'g_il'.
		Enable light 'ls_spot'.
		Enable light 'ls_point'.
		Disable decal_mesh 'dm_glass_breakable'.
		Enable object 'g_cone_light'.
	sequence 'hide':
		Disable body 'body_collision_dyn_01'.
		Disable body 'body_collision_01'.
		Disable body 'body_collision_dyn_02'.
		Disable body 'body_collision_02'.
		Disable body 'body_collision_dyn_03'.
		Disable body 'body_collision_03'.
		Disable body 'body_collision_04'.
		Disable body 'body_collision_dyn_04'.
		Disable body 'body_collision_05'.
		Disable body 'body_collision_dyn_05'.
		Disable body 'body_collision_06'.
		Disable body 'body_collision_dyn_06'.
		Disable body 'body_collision_07'.
		Disable body 'body_collision_dyn_07'.
		Hide graphic_group 'g_lamp'.
		Hide graphic_group 'g_lamp_broken'.
		Disable decal_mesh 'dm_glass_breakable'.
		Disable decal_mesh 'dm_metal'.
		Disable object 'g_il'.
		Disable light 'ls_spot'.
		Disable light 'ls_point'.
		Enable decal_mesh 'dm_glass_breakable'.
		Disable object 'g_cone_light'.
	sequence 'destroy':
		Enable body 'body_collision_dyn_01'.
		Disable body 'body_collision_01'.
		Enable body 'body_collision_dyn_02'.
		Disable body 'body_collision_02'.
		Enable body 'body_collision_dyn_03'.
		Disable body 'body_collision_03'.
		Disable body 'body_collision_04'.
		Enable body 'body_collision_dyn_04'.
		Disable body 'body_collision_05'.
		Enable body 'body_collision_dyn_05'.
		Disable body 'body_collision_06'.
		Enable body 'body_collision_dyn_06'.
		Disable body 'body_collision_07'.
		Enable body 'body_collision_dyn_07'.
		Hide graphic_group 'g_lamp'.
		Show graphic_group 'g_lamp_broken'.
		Disable object 'g_il'.
		Disable light 'ls_spot'.
		Disable light 'ls_point'.
		Disable decal_mesh 'dm_glass_breakable'.
		Disable object 'g_cone_light'.
		Play audio 'light_bulb_smash' at 'snd'.
	body 'body_lamp'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'destroy'.
