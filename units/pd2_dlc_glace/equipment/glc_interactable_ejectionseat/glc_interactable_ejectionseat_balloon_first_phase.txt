unit:
	sequence 'anim_start_up':
		Enable animation_group 'anim_balloon':
			from 0/30
			speed 0.2
			to 28/30
		Show graphic_group 'balloon_group'.
		Play audio 'electric_winch_stop' at 'snd_rope'.
		Play audio 'electric_winch_up' at 'snd_rope'. (DELAY 1/30)
	sequence 'anim_start_down':
		Enable animation_group 'anim_balloon':
			from 28/30
			speed -0.2
			to 0/30
		Show graphic_group 'balloon_group'.
		Play audio 'electric_winch_stop' at 'snd_rope'.
		Play audio 'electric_winch_down' at 'snd_rope'. (DELAY 1/30)
	sequence 'play_sound_winch_stop':
		Play audio 'electric_winch_stop' at 'snd_rope'.
	sequence 'anim_continue_up':
		Enable animation_group 'anim_balloon':
			speed 0.2
			to 28/30
		Show graphic_group 'balloon_group'.
		Play audio 'electric_winch_stop' at 'snd_rope'.
		Play audio 'electric_winch_up' at 'snd_rope'. (DELAY 1/30)
	sequence 'anim_continue_down':
		Enable animation_group 'anim_balloon':
			speed -0.2
			to 0/30
		Show graphic_group 'balloon_group'.
		Play audio 'electric_winch_stop' at 'snd_rope'.
		Play audio 'electric_winch_down' at 'snd_rope'. (DELAY 1/30)
	sequence 'hide_balloon':
		Hide graphic_group 'balloon_group'.
	sequence 'show_balloon':
		Show graphic_group 'balloon_group'.
