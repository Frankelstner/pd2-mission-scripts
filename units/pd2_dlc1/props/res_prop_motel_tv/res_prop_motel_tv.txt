unit:
	sequence 'break':
		Play audio 'light_bulb_smash' at 'e_2'.
		Disable object 'g_g'.
		Enable object 'g_dmg'.
		Disable decal_mesh 'dm_glass'.
		effect 'effects/payday2/particles/window/car_window_small':
			parent  'e_1' 
			position v()
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent  'e_2' 
	body 'static_body'
		Upon receiving 2 bullet hits or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'break'.
