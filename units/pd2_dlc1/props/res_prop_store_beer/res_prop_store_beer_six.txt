unit:
	sequence 'destroy':
		TRIGGER TIMES 1
		effect 'effects/particles/dest/brown_bottle_dest':
			parent  'g_g_lod0' 
		Play audio 'pot_large_shatter' at 'g_g_lod0'.
		Hide graphic_group 'bottles_six'.
		Disable body 'body_static'.
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy'.
