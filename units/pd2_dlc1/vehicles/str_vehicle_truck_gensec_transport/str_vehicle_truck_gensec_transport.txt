unit:
	sequence 'spawn_loot_gold_1':
		If exploded == 0: 
			spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_gold':
				position object_pos('sp_spawn_1')
				rotation object_rot('sp_spawn_1')
		spawn_1 = 1
	sequence 'spawn_loot_gold_2':
		If exploded == 0: 
			spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_gold':
				position object_pos('sp_spawn_2')
				rotation object_rot('sp_spawn_2')
		spawn_2 = 1
	sequence 'spawn_loot_gold_3':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_gold':
			position object_pos('sp_spawn_3')
			rotation object_rot('sp_spawn_3')
		spawn_3 = 1
	sequence 'spawn_loot_gold_4':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_gold':
			position object_pos('sp_spawn_4')
			rotation object_rot('sp_spawn_4')
		spawn_4 = 1
	sequence 'spawn_loot_gold_5':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_gold':
			position object_pos('sp_spawn_5')
			rotation object_rot('sp_spawn_5')
		spawn_5 = 1
	sequence 'spawn_loot_gold_6':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_gold':
			position object_pos('sp_spawn_6')
			rotation object_rot('sp_spawn_6')
		spawn_6 = 1
	sequence 'spawn_loot_gold_7':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_gold':
			position object_pos('sp_spawn_7')
			rotation object_rot('sp_spawn_7')
		spawn_7 = 1
	sequence 'spawn_loot_gold_8':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_gold':
			position object_pos('sp_spawn_8')
			rotation object_rot('sp_spawn_8')
		spawn_8 = 1
	sequence 'spawn_loot_gold_9':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_gold':
			position object_pos('sp_spawn_9')
			rotation object_rot('sp_spawn_9')
		spawn_9 = 1
	sequence 'spawn_loot_money_1':
		If exploded == 0: 
			spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_money':
				position object_pos('sp_spawn_1')
				rotation object_rot('sp_spawn_1')
		spawn_1 = 1
		If exploded == 1: Run sequence 'exploded_money'.
	sequence 'spawn_loot_money_2':
		If exploded == 0: 
			spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_money':
				position object_pos('sp_spawn_2')
				rotation object_rot('sp_spawn_2')
		spawn_2 = 1
		If exploded == 1: Run sequence 'exploded_money'.
	sequence 'spawn_loot_money_3':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_money':
			position object_pos('sp_spawn_3')
			rotation object_rot('sp_spawn_3')
		spawn_3 = 1
		If exploded == 1: Run sequence 'exploded_money'.
	sequence 'spawn_loot_money_4':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_money':
			position object_pos('sp_spawn_4')
			rotation object_rot('sp_spawn_4')
		spawn_4 = 1
	sequence 'spawn_loot_money_5':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_money':
			position object_pos('sp_spawn_5')
			rotation object_rot('sp_spawn_5')
		spawn_5 = 1
	sequence 'spawn_loot_money_6':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_money':
			position object_pos('sp_spawn_6')
			rotation object_rot('sp_spawn_6')
		spawn_6 = 1
	sequence 'spawn_loot_money_7':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_money':
			position object_pos('sp_spawn_7')
			rotation object_rot('sp_spawn_7')
		spawn_7 = 1
	sequence 'spawn_loot_money_8':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_money':
			position object_pos('sp_spawn_8')
			rotation object_rot('sp_spawn_8')
		spawn_8 = 1
	sequence 'spawn_loot_money_9':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_money':
			position object_pos('sp_spawn_9')
			rotation object_rot('sp_spawn_9')
		spawn_9 = 1
	sequence 'spawn_loot_art_1':
		If exploded == 0: 
			spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_jewelry':
				position object_pos('sp_spawn_1')
				rotation object_rot('sp_spawn_1')
		If exploded == 0: filter = 'filter_not_exploded'
		If exploded == 0: spawn_1 = 1
	sequence 'spawn_loot_art_2':
		If exploded == 0: 
			spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_jewelry':
				position object_pos('sp_spawn_2')
				rotation object_rot('sp_spawn_2')
		If exploded == 0: filter = 'filter_not_exploded'
		If exploded == 0: spawn_2 = 1
	sequence 'spawn_loot_art_3':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_jewelry':
			position object_pos('sp_spawn_3')
			rotation object_rot('sp_spawn_3')
		spawn_3 = 1
	sequence 'spawn_loot_art_4':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_jewelry':
			position object_pos('sp_spawn_4')
			rotation object_rot('sp_spawn_4')
		spawn_4 = 1
	sequence 'spawn_loot_art_5':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_jewelry':
			position object_pos('sp_spawn_5')
			rotation object_rot('sp_spawn_5')
		spawn_5 = 1
	sequence 'spawn_loot_art_6':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_jewelry':
			position object_pos('sp_spawn_6')
			rotation object_rot('sp_spawn_6')
		spawn_6 = 1
	sequence 'spawn_loot_art_7':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_jewelry':
			position object_pos('sp_spawn_7')
			rotation object_rot('sp_spawn_7')
		spawn_7 = 1
	sequence 'spawn_loot_art_8':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_jewelry':
			position object_pos('sp_spawn_8')
			rotation object_rot('sp_spawn_8')
		spawn_8 = 1
	sequence 'spawn_loot_art_9':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_deposit/spawn_jewelry':
			position object_pos('sp_spawn_9')
			rotation object_rot('sp_spawn_9')
		spawn_9 = 1
	sequence 'spawn_loot_empty_1':
		extra = 50*rand()
		If intel_1 == 1 AND exploded == 0: Run sequence 'int_seq_intel_1'.
		If intel_1 == 0 AND exploded == 0: Run sequence 'int_seq_trash_1'.
	sequence 'spawn_loot_empty_2':
		extra = 50*rand()
		If intel_2 == 1 AND exploded == 0: Run sequence 'int_seq_intel_2'.
		If intel_2 == 0 AND exploded == 0: Run sequence 'int_seq_trash_2'.
	sequence 'spawn_loot_empty_3':
		extra = 50*rand()
		If intel_3 == 1: Run sequence 'int_seq_intel_3'.
		If intel_3 == 0: Run sequence 'int_seq_trash_3'.
	sequence 'spawn_loot_empty_4':
		extra = 50*rand()
		If intel_4 == 1: Run sequence 'int_seq_intel_4'.
		If intel_4 == 0: Run sequence 'int_seq_trash_4'.
	sequence 'spawn_loot_empty_5':
		extra = 50*rand()
		If intel_5 == 1: Run sequence 'int_seq_intel_5'.
		If intel_5 == 0: Run sequence 'int_seq_trash_5'.
	sequence 'spawn_loot_empty_6':
		extra = 50*rand()
		If intel_6 == 1: Run sequence 'int_seq_intel_6'.
		If intel_6 == 0: Run sequence 'int_seq_trash_6'.
	sequence 'spawn_loot_empty_7':
		extra = 50*rand()
		If intel_7 == 1: Run sequence 'int_seq_intel_7'.
		If intel_7 == 0: Run sequence 'int_seq_trash_7'.
	sequence 'spawn_loot_empty_8':
		extra = 50*rand()
		If intel_8 == 1: Run sequence 'int_seq_intel_8'.
		If intel_8 == 0: Run sequence 'int_seq_trash_8'.
	sequence 'spawn_loot_empty_9':
		extra = 50*rand()
		If intel_9 == 1: Run sequence 'int_seq_intel_9'.
		If intel_9 == 0: Run sequence 'int_seq_trash_9'.
	sequence 'spawn_loot_german_folder_1':
		spawn_unit 'units/pd2_dlc_jfr/pickups/spawn_german_folder/spawn_german_folder':
			position object_pos('sp_spawn_1')
			rotation object_rot('sp_spawn_1')
		spawn_1 = 1
	sequence 'spawn_loot_german_folder_2':
		spawn_unit 'units/pd2_dlc_jfr/pickups/spawn_german_folder/spawn_german_folder':
			position object_pos('sp_spawn_2')
			rotation object_rot('sp_spawn_2')
		spawn_2 = 1
	sequence 'spawn_loot_german_folder_3':
		spawn_unit 'units/pd2_dlc_jfr/pickups/spawn_german_folder/spawn_german_folder':
			position object_pos('sp_spawn_3')
			rotation object_rot('sp_spawn_3')
		spawn_3 = 1
	sequence 'spawn_loot_german_folder_4':
		spawn_unit 'units/pd2_dlc_jfr/pickups/spawn_german_folder/spawn_german_folder':
			position object_pos('sp_spawn_4')
			rotation object_rot('sp_spawn_4')
		spawn_4 = 1
	sequence 'spawn_loot_german_folder_5':
		spawn_unit 'units/pd2_dlc_jfr/pickups/spawn_german_folder/spawn_german_folder':
			position object_pos('sp_spawn_5')
			rotation object_rot('sp_spawn_5')
		spawn_5 = 1
	sequence 'spawn_loot_german_folder_6':
		spawn_unit 'units/pd2_dlc_jfr/pickups/spawn_german_folder/spawn_german_folder':
			position object_pos('sp_spawn_6')
			rotation object_rot('sp_spawn_6')
		spawn_6 = 1
	sequence 'spawn_loot_german_folder_7':
		spawn_unit 'units/pd2_dlc_jfr/pickups/spawn_german_folder/spawn_german_folder':
			position object_pos('sp_spawn_7')
			rotation object_rot('sp_spawn_7')
		spawn_7 = 1
	sequence 'spawn_loot_german_folder_8':
		spawn_unit 'units/pd2_dlc_jfr/pickups/spawn_german_folder/spawn_german_folder':
			position object_pos('sp_spawn_8')
			rotation object_rot('sp_spawn_8')
		spawn_8 = 1
	sequence 'spawn_loot_german_folder_9':
		spawn_unit 'units/pd2_dlc_jfr/pickups/spawn_german_folder/spawn_german_folder':
			position object_pos('sp_spawn_9')
			rotation object_rot('sp_spawn_9')
		spawn_9 = 1
	sequence 'int_seq_intel_1':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/str_vehicle_truck_gensec_transport_deposit_box_intel':
			position object_pos('sp_spawn_1')
			rotation object_rot('sp_spawn_1')
	sequence 'int_seq_empty_1':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/str_vehicle_truck_gensec_transport_deposit_box':
			position object_pos('sp_spawn_1')
			rotation object_rot('sp_spawn_1')
	sequence 'int_seq_intel_2':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/str_vehicle_truck_gensec_transport_deposit_box_intel':
			position object_pos('sp_spawn_2')
			rotation object_rot('sp_spawn_2')
	sequence 'int_seq_empty_2':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/str_vehicle_truck_gensec_transport_deposit_box':
			position object_pos('sp_spawn_2')
			rotation object_rot('sp_spawn_2')
	sequence 'int_seq_intel_3':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/str_vehicle_truck_gensec_transport_deposit_box_intel':
			position object_pos('sp_spawn_3')
			rotation object_rot('sp_spawn_3')
	sequence 'int_seq_empty_3':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/str_vehicle_truck_gensec_transport_deposit_box':
			position object_pos('sp_spawn_3')
			rotation object_rot('sp_spawn_3')
	sequence 'int_seq_intel_4':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/str_vehicle_truck_gensec_transport_deposit_box_intel':
			position object_pos('sp_spawn_4')
			rotation object_rot('sp_spawn_4')
	sequence 'int_seq_empty_4':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/str_vehicle_truck_gensec_transport_deposit_box':
			position object_pos('sp_spawn_4')
			rotation object_rot('sp_spawn_4')
	sequence 'int_seq_intel_5':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/str_vehicle_truck_gensec_transport_deposit_box_intel':
			position object_pos('sp_spawn_5')
			rotation object_rot('sp_spawn_5')
	sequence 'int_seq_empty_5':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/str_vehicle_truck_gensec_transport_deposit_box':
			position object_pos('sp_spawn_5')
			rotation object_rot('sp_spawn_5')
	sequence 'int_seq_intel_6':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/str_vehicle_truck_gensec_transport_deposit_box_intel':
			position object_pos('sp_spawn_6')
			rotation object_rot('sp_spawn_6')
	sequence 'int_seq_empty_6':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/str_vehicle_truck_gensec_transport_deposit_box':
			position object_pos('sp_spawn_6')
			rotation object_rot('sp_spawn_6')
	sequence 'int_seq_intel_7':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/str_vehicle_truck_gensec_transport_deposit_box_intel':
			position object_pos('sp_spawn_7')
			rotation object_rot('sp_spawn_7')
	sequence 'int_seq_empty_7':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/str_vehicle_truck_gensec_transport_deposit_box':
			position object_pos('sp_spawn_7')
			rotation object_rot('sp_spawn_7')
	sequence 'int_seq_intel_8':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/str_vehicle_truck_gensec_transport_deposit_box_intel':
			position object_pos('sp_spawn_8')
			rotation object_rot('sp_spawn_8')
	sequence 'int_seq_empty_8':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/str_vehicle_truck_gensec_transport_deposit_box':
			position object_pos('sp_spawn_8')
			rotation object_rot('sp_spawn_8')
	sequence 'int_seq_intel_9':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/str_vehicle_truck_gensec_transport_deposit_box_intel':
			position object_pos('sp_spawn_9')
			rotation object_rot('sp_spawn_9')
	sequence 'int_seq_empty_9':
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/str_vehicle_truck_gensec_transport_deposit_box':
			position object_pos('sp_spawn_9')
			rotation object_rot('sp_spawn_9')
	sequence 'int_seq_trash_1':
		If extra <= 1: Run sequence 'spawn_loot_german_folder_1'.
		If extra > 1: Run sequence 'int_seq_empty_1'.
	sequence 'int_seq_trash_2':
		If extra <= 1: Run sequence 'spawn_loot_german_folder_2'.
		If extra > 1: Run sequence 'int_seq_empty_2'.
	sequence 'int_seq_trash_3':
		If extra <= 1: Run sequence 'spawn_loot_german_folder_3'.
		If extra > 1: Run sequence 'int_seq_empty_3'.
	sequence 'int_seq_trash_4':
		If extra <= 1: Run sequence 'spawn_loot_german_folder_4'.
		If extra > 1: Run sequence 'int_seq_empty_4'.
	sequence 'int_seq_trash_5':
		If extra <= 1: Run sequence 'spawn_loot_german_folder_5'.
		If extra > 1: Run sequence 'int_seq_empty_5'.
	sequence 'int_seq_trash_6':
		If extra <= 1: Run sequence 'spawn_loot_german_folder_6'.
		If extra > 1: Run sequence 'int_seq_empty_6'.
	sequence 'int_seq_trash_7':
		If extra <= 1: Run sequence 'spawn_loot_german_folder_7'.
		If extra > 1: Run sequence 'int_seq_empty_7'.
	sequence 'int_seq_trash_8':
		If extra <= 1: Run sequence 'spawn_loot_german_folder_8'.
		If extra > 1: Run sequence 'int_seq_empty_8'.
	sequence 'int_seq_trash_9':
		If extra <= 1: Run sequence 'spawn_loot_german_folder_9'.
		If extra > 1: Run sequence 'int_seq_empty_9'.
	extra = 50
	type = 0
	amount = 0
	trucks = 0
	order = 0
	spawn_1 = 0
	spawn_2 = 0
	spawn_3 = 0
	spawn_4 = 0
	spawn_5 = 0
	spawn_6 = 0
	spawn_7 = 0
	spawn_8 = 0
	spawn_9 = 0
	exploded = 0
	intel = 0
	intel_1 = 0
	intel_2 = 0
	intel_3 = 0
	intel_4 = 0
	intel_5 = 0
	intel_6 = 0
	intel_7 = 0
	intel_8 = 0
	intel_9 = 0
	sequence 'set_type_gold':
		type = 1
	sequence 'set_type_money':
		type = 2
	sequence 'set_type_art':
		type = 3
	sequence 'set_type_nothing':
		type = 4
	sequence 'set_order':
		Run sequence 'set_order_'..pick('1','2','3','4').
	sequence 'set_order_1':
		order = 1
	sequence 'set_order_2':
		order = 2
	sequence 'set_order_3':
		order = 3
	sequence 'set_order_4':
		order = 4
	sequence 'set_exploded':
		exploded = 1
	sequence 'set_intel':
		Run sequence 'set_intel_'..pick('1','2','3','4','5','6','7','8','9').
		intel = 1
	sequence 'set_intel_1':
		intel_1 = 1
	sequence 'set_intel_2':
		intel_2 = 1
	sequence 'set_intel_3':
		intel_3 = 1
	sequence 'set_intel_4':
		intel_4 = 1
	sequence 'set_intel_5':
		intel_5 = 1
	sequence 'set_intel_6':
		intel_6 = 1
	sequence 'set_intel_7':
		intel_7 = 1
	sequence 'set_intel_8':
		intel_8 = 1
	sequence 'set_intel_9':
		intel_9 = 1
	sequence 'spawn':
		TRIGGER TIMES 1
		Run sequence 'set_order'.
		If type == 0: Run sequence 'spawn_'..pick('gold','money','art','german_folder').
		If type == 1: Run sequence 'spawn_gold'.
		If type == 2: Run sequence 'spawn_money'.
		If type == 3: Run sequence 'spawn_art'.
		If type == 4: Run sequence 'spawn_nothing'.
	sequence 'spawn_explosion':
		TRIGGER TIMES 1
		Run sequence 'set_order'.
		Run sequence 'set_exploded'.
		If type == 0: Run sequence 'spawn_'..pick('gold','money','art','german_folder').
		If type == 1: Run sequence 'spawn_gold'.
		If type == 2: Run sequence 'spawn_money'.
		If type == 3: Run sequence 'spawn_art'.
	sequence 'spawn_gold':
		If trucks == 0 AND amount == 0: Run sequence 'set_amount_'..pick('3','3').
		If trucks == 1 AND amount == 0: Run sequence 'set_amount_'..pick('4','4','5','5','6').
		If trucks == 2 AND amount == 0: Run sequence 'set_amount_'..pick('3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','4','4','4','4').
		If trucks == 3 AND amount == 0: Run sequence 'set_amount_'..pick('3','3').
		If amount == 0 AND trucks == 4: Run sequence 'set_amount_'..pick('3','3').
		If amount > 0: Run sequence 'spawn_loot_gold_1_order'.
		If amount > 1: Run sequence 'spawn_loot_gold_2_order'.
		If amount > 2: Run sequence 'spawn_loot_gold_3_order'.
		If amount > 3: Run sequence 'spawn_loot_gold_4_order'.
		If amount > 4: Run sequence 'spawn_loot_gold_5_order'.
		If amount > 5: Run sequence 'spawn_loot_gold_6_order'.
		If amount > 6: Run sequence 'spawn_loot_gold_7_order'.
		If amount > 7: Run sequence 'spawn_loot_gold_8_order'.
		If amount > 8: Run sequence 'spawn_loot_gold_9_order'.
		If spawn_1 == 0: Run sequence 'spawn_loot_empty_1'.
		If spawn_2 == 0: Run sequence 'spawn_loot_empty_2'.
		If spawn_3 == 0: Run sequence 'spawn_loot_empty_3'.
		If spawn_4 == 0: Run sequence 'spawn_loot_empty_4'.
		If spawn_5 == 0: Run sequence 'spawn_loot_empty_5'.
		If spawn_6 == 0: Run sequence 'spawn_loot_empty_6'.
		If spawn_7 == 0: Run sequence 'spawn_loot_empty_7'.
		If spawn_8 == 0: Run sequence 'spawn_loot_empty_8'.
		If spawn_9 == 0: Run sequence 'spawn_loot_empty_9'.
	sequence 'spawn_money':
		If trucks == 0 AND amount == 0: Run sequence 'set_amount_'..pick('3','3').
		If trucks == 1 AND amount == 0: Run sequence 'set_amount_'..pick('3','4','4','5','5','6','6','7','7').
		If trucks == 2 AND amount == 0: Run sequence 'set_amount_'..pick('3','3','3','4','4','4','4','4','5','5','5','5','5','5','5','6','6','6').
		If trucks == 3 AND amount == 0: Run sequence 'set_amount_'..pick('3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','3','4','4','4','4','5').
		If amount == 0 AND trucks == 4: Run sequence 'set_amount_'..pick('3','3','3','3','3','3','3','3','3','3','4','4','4','4').
		If amount > 0: Run sequence 'spawn_loot_money_1_order'.
		If amount > 1: Run sequence 'spawn_loot_money_2_order'.
		If amount > 2: Run sequence 'spawn_loot_money_3_order'.
		If amount > 3: Run sequence 'spawn_loot_money_4_order'.
		If amount > 4: Run sequence 'spawn_loot_money_5_order'.
		If amount > 5: Run sequence 'spawn_loot_money_6_order'.
		If amount > 6: Run sequence 'spawn_loot_money_7_order'.
		If amount > 7: Run sequence 'spawn_loot_money_8_order'.
		If amount > 8: Run sequence 'spawn_loot_money_9_order'.
		If spawn_1 == 0: Run sequence 'spawn_loot_empty_1'.
		If spawn_2 == 0: Run sequence 'spawn_loot_empty_2'.
		If spawn_3 == 0: Run sequence 'spawn_loot_empty_3'.
		If spawn_4 == 0: Run sequence 'spawn_loot_empty_4'.
		If spawn_5 == 0: Run sequence 'spawn_loot_empty_5'.
		If spawn_6 == 0: Run sequence 'spawn_loot_empty_6'.
		If spawn_7 == 0: Run sequence 'spawn_loot_empty_7'.
		If spawn_8 == 0: Run sequence 'spawn_loot_empty_8'.
		If spawn_9 == 0: Run sequence 'spawn_loot_empty_9'.
	sequence 'spawn_art':
		If trucks == 0 AND amount == 0: Run sequence 'set_amount_'..pick('3','3').
		If trucks == 1 AND amount == 0: Run sequence 'set_amount_'..pick('5','5','6','6','7','7').
		If trucks == 2 AND amount == 0: Run sequence 'set_amount_'..pick('5','6').
		If trucks == 3 AND amount == 0: Run sequence 'set_amount_'..pick('4','5','6').
		If amount == 0 AND trucks == 4: Run sequence 'set_amount_'..pick('3','4').
		If amount > 0: Run sequence 'spawn_loot_art_1_order'.
		If amount > 1: Run sequence 'spawn_loot_art_2_order'.
		If amount > 2: Run sequence 'spawn_loot_art_3_order'.
		If amount > 3: Run sequence 'spawn_loot_art_4_order'.
		If amount > 4: Run sequence 'spawn_loot_art_5_order'.
		If amount > 5: Run sequence 'spawn_loot_art_6_order'.
		If amount > 6: Run sequence 'spawn_loot_art_7_order'.
		If amount > 7: Run sequence 'spawn_loot_art_8_order'.
		If amount > 8: Run sequence 'spawn_loot_art_9_order'.
		If spawn_1 == 0: Run sequence 'spawn_loot_empty_1'.
		If spawn_2 == 0: Run sequence 'spawn_loot_empty_2'.
		If spawn_3 == 0: Run sequence 'spawn_loot_empty_3'.
		If spawn_4 == 0: Run sequence 'spawn_loot_empty_4'.
		If spawn_5 == 0: Run sequence 'spawn_loot_empty_5'.
		If spawn_6 == 0: Run sequence 'spawn_loot_empty_6'.
		If spawn_7 == 0: Run sequence 'spawn_loot_empty_7'.
		If spawn_8 == 0: Run sequence 'spawn_loot_empty_8'.
		If spawn_9 == 0: Run sequence 'spawn_loot_empty_9'.
	sequence 'spawn_nothing':
		If spawn_1 == 0: Run sequence 'spawn_loot_empty_1'.
		If spawn_2 == 0: Run sequence 'spawn_loot_empty_2'.
		If spawn_3 == 0: Run sequence 'spawn_loot_empty_3'.
		If spawn_4 == 0: Run sequence 'spawn_loot_empty_4'.
		If spawn_5 == 0: Run sequence 'spawn_loot_empty_5'.
		If spawn_6 == 0: Run sequence 'spawn_loot_empty_6'.
		If spawn_7 == 0: Run sequence 'spawn_loot_empty_7'.
		If spawn_8 == 0: Run sequence 'spawn_loot_empty_8'.
		If spawn_9 == 0: Run sequence 'spawn_loot_empty_9'.
	sequence 'spawn_loot_gold_1_order':
		If order == 1: Run sequence 'spawn_loot_gold_3'.
		If order == 2: Run sequence 'spawn_loot_gold_5'.
		If order == 3: Run sequence 'spawn_loot_gold_2'.
		If order == 4: Run sequence 'spawn_loot_gold_7'.
	sequence 'spawn_loot_gold_2_order':
		If order == 1: Run sequence 'spawn_loot_gold_4'.
		If order == 2: Run sequence 'spawn_loot_gold_3'.
		If order == 3: Run sequence 'spawn_loot_gold_6'.
		If order == 4: Run sequence 'spawn_loot_gold_2'.
	sequence 'spawn_loot_gold_3_order':
		If order == 1: Run sequence 'spawn_loot_gold_8'.
		If order == 2: Run sequence 'spawn_loot_gold_1'.
		If order == 3: Run sequence 'spawn_loot_gold_4'.
		If order == 4: Run sequence 'spawn_loot_gold_3'.
	sequence 'spawn_loot_gold_4_order':
		If order == 1: Run sequence 'spawn_loot_gold_1'.
		If order == 2: Run sequence 'spawn_loot_gold_8'.
		If order == 3: Run sequence 'spawn_loot_gold_7'.
		If order == 4: Run sequence 'spawn_loot_gold_8'.
	sequence 'spawn_loot_gold_5_order':
		If order == 1: Run sequence 'spawn_loot_gold_7'.
		If order == 2: Run sequence 'spawn_loot_gold_7'.
		If order == 3: Run sequence 'spawn_loot_gold_1'.
		If order == 4: Run sequence 'spawn_loot_gold_4'.
	sequence 'spawn_loot_gold_6_order':
		If order == 1: Run sequence 'spawn_loot_gold_5'.
		If order == 2: Run sequence 'spawn_loot_gold_4'.
		If order == 3: Run sequence 'spawn_loot_gold_3'.
		If order == 4: Run sequence 'spawn_loot_gold_5'.
	sequence 'spawn_loot_gold_7_order':
		If order == 1: Run sequence 'spawn_loot_gold_2'.
		If order == 2: Run sequence 'spawn_loot_gold_6'.
		If order == 3: Run sequence 'spawn_loot_gold_8'.
		If order == 4: Run sequence 'spawn_loot_gold_1'.
	sequence 'spawn_loot_gold_8_order':
		If order == 1: Run sequence 'spawn_loot_gold_6'.
		If order == 2: Run sequence 'spawn_loot_gold_2'.
		If order == 3: Run sequence 'spawn_loot_gold_5'.
		If order == 4: Run sequence 'spawn_loot_gold_6'.
	sequence 'spawn_loot_gold_9_order':
		If order == 1: Run sequence 'spawn_loot_gold_9'.
		If order == 2: Run sequence 'spawn_loot_gold_9'.
		If order == 3: Run sequence 'spawn_loot_gold_9'.
		If order == 4: Run sequence 'spawn_loot_gold_9'.
	sequence 'spawn_loot_money_1_order':
		If order == 1: Run sequence 'spawn_loot_money_3'.
		If order == 2: Run sequence 'spawn_loot_money_5'.
		If order == 3: Run sequence 'spawn_loot_money_2'.
		If order == 4: Run sequence 'spawn_loot_money_7'.
	sequence 'spawn_loot_money_2_order':
		If order == 1: Run sequence 'spawn_loot_money_4'.
		If order == 2: Run sequence 'spawn_loot_money_3'.
		If order == 3: Run sequence 'spawn_loot_money_6'.
		If order == 4: Run sequence 'spawn_loot_money_3'.
	sequence 'spawn_loot_money_3_order':
		If order == 1: Run sequence 'spawn_loot_money_8'.
		If order == 2: Run sequence 'spawn_loot_money_1'.
		If order == 3: Run sequence 'spawn_loot_money_4'.
		If order == 4: Run sequence 'spawn_loot_money_2'.
	sequence 'spawn_loot_money_4_order':
		If order == 1: Run sequence 'spawn_loot_money_2'.
		If order == 2: Run sequence 'spawn_loot_money_6'.
		If order == 3: Run sequence 'spawn_loot_money_8'.
		If order == 4: Run sequence 'spawn_loot_money_1'.
	sequence 'spawn_loot_money_5_order':
		If order == 1: Run sequence 'spawn_loot_money_7'.
		If order == 2: Run sequence 'spawn_loot_money_7'.
		If order == 3: Run sequence 'spawn_loot_money_1'.
		If order == 4: Run sequence 'spawn_loot_money_4'.
	sequence 'spawn_loot_money_6_order':
		If order == 1: Run sequence 'spawn_loot_money_5'.
		If order == 2: Run sequence 'spawn_loot_money_4'.
		If order == 3: Run sequence 'spawn_loot_money_3'.
		If order == 4: Run sequence 'spawn_loot_money_5'.
	sequence 'spawn_loot_money_7_order':
		If order == 1: Run sequence 'spawn_loot_money_1'.
		If order == 2: Run sequence 'spawn_loot_money_8'.
		If order == 3: Run sequence 'spawn_loot_money_7'.
		If order == 4: Run sequence 'spawn_loot_money_8'.
	sequence 'spawn_loot_money_8_order':
		If order == 1: Run sequence 'spawn_loot_money_6'.
		If order == 2: Run sequence 'spawn_loot_money_2'.
		If order == 3: Run sequence 'spawn_loot_money_5'.
		If order == 4: Run sequence 'spawn_loot_money_6'.
	sequence 'spawn_loot_money_9_order':
		If order == 1: Run sequence 'spawn_loot_money_9'.
		If order == 2: Run sequence 'spawn_loot_money_9'.
		If order == 3: Run sequence 'spawn_loot_money_9'.
		If order == 4: Run sequence 'spawn_loot_money_9'.
	sequence 'spawn_loot_art_1_order':
		If order == 1: Run sequence 'spawn_loot_art_3'.
		If order == 2: Run sequence 'spawn_loot_art_5'.
		If order == 3: Run sequence 'spawn_loot_art_2'.
		If order == 4: Run sequence 'spawn_loot_art_7'.
	sequence 'spawn_loot_art_2_order':
		If order == 1: Run sequence 'spawn_loot_art_8'.
		If order == 2: Run sequence 'spawn_loot_art_1'.
		If order == 3: Run sequence 'spawn_loot_art_4'.
		If order == 4: Run sequence 'spawn_loot_art_3'.
	sequence 'spawn_loot_art_3_order':
		If order == 1: Run sequence 'spawn_loot_art_4'.
		If order == 2: Run sequence 'spawn_loot_art_3'.
		If order == 3: Run sequence 'spawn_loot_art_6'.
		If order == 4: Run sequence 'spawn_loot_art_2'.
	sequence 'spawn_loot_art_4_order':
		If order == 1: Run sequence 'spawn_loot_art_1'.
		If order == 2: Run sequence 'spawn_loot_art_8'.
		If order == 3: Run sequence 'spawn_loot_art_7'.
		If order == 4: Run sequence 'spawn_loot_art_8'.
	sequence 'spawn_loot_art_5_order':
		If order == 1: Run sequence 'spawn_loot_art_7'.
		If order == 2: Run sequence 'spawn_loot_art_7'.
		If order == 3: Run sequence 'spawn_loot_art_1'.
		If order == 4: Run sequence 'spawn_loot_art_4'.
	sequence 'spawn_loot_art_6_order':
		If order == 1: Run sequence 'spawn_loot_art_5'.
		If order == 2: Run sequence 'spawn_loot_art_4'.
		If order == 3: Run sequence 'spawn_loot_art_3'.
		If order == 4: Run sequence 'spawn_loot_art_5'.
	sequence 'spawn_loot_art_7_order':
		If order == 1: Run sequence 'spawn_loot_art_2'.
		If order == 2: Run sequence 'spawn_loot_art_6'.
		If order == 3: Run sequence 'spawn_loot_art_8'.
		If order == 4: Run sequence 'spawn_loot_art_1'.
	sequence 'spawn_loot_art_8_order':
		If order == 1: Run sequence 'spawn_loot_art_6'.
		If order == 2: Run sequence 'spawn_loot_art_2'.
		If order == 3: Run sequence 'spawn_loot_art_5'.
		If order == 4: Run sequence 'spawn_loot_art_6'.
	sequence 'spawn_loot_art_9_order':
		If order == 1: Run sequence 'spawn_loot_art_9'.
		If order == 2: Run sequence 'spawn_loot_art_9'.
		If order == 3: Run sequence 'spawn_loot_art_9'.
		If order == 4: Run sequence 'spawn_loot_art_9'.
	sequence 'set_amount_1':
		amount = 1
	sequence 'set_amount_2':
		amount = 2
	sequence 'set_amount_3':
		amount = 3
	sequence 'set_amount_4':
		amount = 4
	sequence 'set_amount_5':
		amount = 5
	sequence 'set_amount_6':
		amount = 6
	sequence 'set_amount_7':
		amount = 7
	sequence 'set_amount_8':
		amount = 8
	sequence 'set_amount_9':
		amount = 9
	sequence 'set_trucks_1':
		trucks = 1
	sequence 'set_trucks_2':
		trucks = 2
	sequence 'set_trucks_3':
		trucks = 3
	sequence 'set_trucks_4':
		trucks = 4
	sequence 'open_front_doors':
		Enable animation_group 'front_doors':
			end_time 30/30
			time 0/30
		Play audio 'money_transport_front_door_open' at 'a_front_door_right'.
		Play audio 'money_transport_front_door_open' at 'a_front_door_left'.
	sequence 'open_middle_door_left':
		Enable animation_group 'middle_door_left':
			end_time 30/30
			time 0/30
		Play audio 'money_transport_side_door_open' at 'a_middle_door_left'.
	sequence 'open_middle_door_right':
		Enable animation_group 'middle_door_right':
			end_time 30/30
			time 0/30
		Play audio 'money_transport_side_door_open' at 'a_middle_door_right'.
	sequence 'open_back_doors':
		Enable light 'lo_omni_1'.
		Enable light 'lo_omni_2'.
		Enable animation_group 'back_doors':
			end_time 30/30
			time 0/30
		Play audio 'money_transport_rear_door_open' at 'a_back_door_right'.
		Play audio 'money_transport_rear_door_open' at 'a_back_door_left'.
		Disable body 'body_saw_hitbox'.
		Run sequence 'deactivate_door'.
		Run sequence 'done_opened'.
	sequence 'close_front_doors':
		Enable animation_group 'front_doors':
			end_time 0/30
			speed -1
			time 30/30
	sequence 'close_middle_door_left':
		Enable animation_group 'middle_door_left':
			end_time 0/30
			speed -1
			time 30/30
	sequence 'close_middle_door_right':
		Enable animation_group 'middle_door_right':
			end_time 0/30
			speed -1
			time 30/30
	sequence 'close_back_doors':
		Enable animation_group 'back_doors':
			end_time 0/30
			speed -1
			time 30/30
	sequence 'state_hide':
		Disable body 'static_body'.
		Disable body 'body_back_door_right'.
		Disable body 'body_back_door_left'.
		Disable body 'body_middle_door_right'.
		Disable body 'body_middle_door_left'.
		Disable body 'body_front_door_right'.
		Disable body 'body_front_door_left'.
		Disable body 'body_saw_hitbox'.
		Disable body 'body_blocker_mover'.
		Disable body 'bag_blocker'.
		Hide graphic_group 'grp_truck'.
	sequence 'state_show':
		Enable body 'static_body'.
		Enable body 'body_back_door_right'.
		Enable body 'body_back_door_left'.
		Enable body 'body_middle_door_right'.
		Enable body 'body_middle_door_left'.
		Enable body 'body_front_door_right'.
		Enable body 'body_front_door_left'.
		Enable body 'body_saw_hitbox'.
		Enable body 'body_blocker_mover'.
		Enable body 'bag_blocker'.
		Show graphic_group 'grp_truck'.
	sequence 'int_seq_explosion':
		Disable body 'body_saw_hitbox'.
		Run sequence 'deactivate_door'.
		Disable body 'body_back_door_right'.
		Disable body 'body_back_door_left'.
		Disable decal_mesh 'dm_glass_back_door_right'.
		Disable decal_mesh 'dm_back_door_right_sheet_metal'.
		Disable decal_mesh 'dm_glass_back_door_left'.
		Disable decal_mesh 'dm_back_door_left_sheet_metal'.
		Disable object 'g_decals_right_back_door'.
		Disable object 'g_back_door_left'.
		Disable object 's_back_door_left'.
		Disable object 'g_back_door_right'.
		Disable object 's_back_door_right'.
		Disable object 'g_glass_back_door_right'.
		Disable object 'g_glass_back_door_left'.
		spawn_unit 'units/pd2_dlc1/vehicles/str_vehicle_truck_gensec_transport/spawn_gensec_doors/spawn_gensec_doors':
			position object_pos('spawn_doors')
			rotation object_rot('spawn_doors')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('physics_effect') ),100,20).
		effect 'effects/payday2/environment/arm_truck_vehicle_smoke':
			parent object( 'smoke' )
			position v()
		effect 'effects/particles/explosions/explosion_grenade':
			parent object( 'a_shp_charge' )
			position v()
		effect 'effects/particles/dest/smoke_pocket_puff':
			parent object( 'a_shp_charge' )
			position v()
		Play audio 'c4_explode_transport_truck_rear_door' at 'a_shp_charge'.
		Run sequence 'done_opened'.
	sequence 'activate_door':
		Call function: base.activate()
	sequence 'deactivate_door':
		Call function: base.deactivate()
	sequence 'explode_door':
		Run sequence 'int_seq_explosion'.
	sequence 'power_off':
		Call function: base.set_powered(False)
	sequence 'power_on':
		Call function: base.set_powered(True)
	sequence 'turn_off':
		Call function: base.set_on(False)
	sequence 'turn_on':
		Call function: base.set_on(True)
	sequence 'door_opened'.
	sequence 'door_closed'.
	sequence 'drill_placed'.
	sequence 'drill_jammed'.
	sequence 'drill_resumed'.
	sequence 'drill_power_off'.
	sequence 'drill_power_on'.
	sequence 'drill_completed':
		Run sequence 'spawn'.
	sequence 'all_drill_placed'.
	sequence 'c4_placed'.
	sequence 'c4_completed':
		Run sequence 'spawn_explosion'.
	sequence 'all_c4_placed'.
	sequence 'open_door':
		Run sequence 'open_back_doors'.
	sequence 'done_opened':
		Run sequence 'open_middle_door_left'.
		Run sequence 'open_middle_door_right'.
	sequence 'exploded_money':
		TRIGGER TIMES 1
		effect 'effects/payday2/environment/arm_money_truck_exp':
			parent object( 'a_shp_charge' )
			position v()
