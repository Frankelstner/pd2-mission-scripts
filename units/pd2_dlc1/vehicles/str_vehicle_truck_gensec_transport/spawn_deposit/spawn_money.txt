unit:
	sequence 'int_seq_open':
		TRIGGER TIMES 1
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_special_money':
			position object_pos('sp_spawn1')
			rotation object_rot('sp_spawn1')
		Hide graphic_group 'drillicongroup'.
		Disable interaction.
		Enable animation_group 'anim'.
		Disable body 'body_door'.
		Disable decal_mesh 'dm_metal_door'.
		Play audio 'deposit_slide_open' at 'jt_5'.
	sequence 'interact':
		Run sequence 'int_seq_open'.
	body 'body_door'
		Upon receiving 37.5 saw damage, execute:
			Run sequence 'int_seq_open'.
