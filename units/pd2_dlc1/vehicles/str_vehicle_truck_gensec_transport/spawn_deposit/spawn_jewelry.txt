unit:
	sequence 'int_seq_open':
		TRIGGER TIMES 1
		Run sequence 'spawn_loot_jewelry_'..pick('1','2','3','4').
		Hide graphic_group 'drillicongroup'.
		Disable interaction.
		Enable animation_group 'anim'.
		Disable body 'body_door'.
		Disable decal_mesh 'dm_metal_door'.
		Play audio 'deposit_slide_open' at 'jt_5'.
	sequence 'interact':
		Run sequence 'int_seq_open'.
	body 'body_door'
		Upon receiving 37.5 saw damage, execute:
			Run sequence 'int_seq_open'.
	sequence 'spawn_loot_jewelry_1':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/com_prop_jewelry_box_01':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	sequence 'spawn_loot_jewelry_2':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/com_prop_jewelry_box_02':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	sequence 'spawn_loot_jewelry_3':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/com_prop_jewelry_box_03':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
	sequence 'spawn_loot_jewelry_4':
		spawn_unit 'units/payday2/props/com_prop_jewelry_jewels/com_prop_jewelry_box_04':
			position object_pos('sp_spawn')
			rotation object_rot('sp_spawn')
