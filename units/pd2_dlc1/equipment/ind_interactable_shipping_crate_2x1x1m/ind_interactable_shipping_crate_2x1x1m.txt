unit:
	open = 0
	sequence 'state_requires_crowbar':
		Call function: interaction.set_tweak_data('crate_loot_crowbar')
	sequence 'state_interaction_enabled':
		Enable interaction.
	sequence 'state_interaction_disabled':
		Disable interaction.
	sequence 'state_vis_show':
		Enable interaction.
		Enable body 'body_static'.
		Enable body 'body_static_open'.
		Enable body 'body_mover_blocker'.
		Enable decal_mesh 'g_g'.
		Disable decal_mesh 'g_g_open'.
		Enable object 'g_g'.
		Enable object 's_s'.
		Disable object 'g_g_open'.
		Disable object 's_s_open'.
		Enable set_proximity 'proximity_check'.
	sequence 'state_vis_hide':
		Disable interaction.
		Disable body 'body_static'.
		Disable body 'body_static_open'.
		Disable body 'body_mover_blocker'.
		Disable decal_mesh 'g_g'.
		Disable decal_mesh 'g_g_open'.
		Disable object 'g_g'.
		Disable object 's_s'.
		Disable object 'g_g_open'.
		Disable object 's_s_open'.
	sequence 'interact':
		If open == 1: Run sequence 'int_seq_close'.
		If open == 0: Run sequence 'int_seq_open'.
	sequence 'int_seq_open':
		Disable body 'body_static'.
		Enable body 'body_static_open'.
		Disable decal_mesh 'g_g'.
		Enable decal_mesh 'g_g_open'.
		Disable object 'g_g'.
		Disable object 's_s'.
		Enable object 'g_g_open'.
		Enable object 's_s_open'.
		Play audio 'open_crate_end' at 'spawn'.
		Call function: interaction.set_tweak_data('crate_loot_close')
		open = 1
	sequence 'int_seq_close':
		Enable body 'body_static'.
		Disable body 'body_static_open'.
		Enable decal_mesh 'g_g'.
		Disable decal_mesh 'g_g_open'.
		Enable object 'g_g'.
		Enable object 's_s'.
		Disable object 'g_g_open'.
		Disable object 's_s_open'.
		Play audio 'close_crate_end' at 'spawn'.
		Call function: interaction.set_tweak_data('crate_loot')
	proximities:
		Disable proximity 'proximity_check':
			within:
				Run sequence 'done_proximity'.
				max_activations 1
				range 200
			interval 1
			ref_object 'attention'
			type 'players'
	sequence 'done_proximity'.
