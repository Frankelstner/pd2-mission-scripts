unit:
	sequence 'state_door_open':
		Disable interaction.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim':
			from 40/30
			speed 0
			to 40/30
		Run sequence 'done_opened'.
		Hide graphic_group 'icon'.
	sequence 'state_door_close':
		Enable interaction.
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
		Enable animation_group 'anim':
			from 0/30
			speed 0
			to 0/30
		Show graphic_group 'icon'.
	sequence 'state_vis_hide':
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Disable body 'body_frame'.
		Disable body 'body_door'.
		Hide graphic_group 'doors'.
		Hide graphic_group 'icon'.
	sequence 'state_vis_show':
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
		Enable body 'body_frame'.
		Enable body 'body_door'.
		Show graphic_group 'doors'.
		Show graphic_group 'icon'.
	sequence 'activate':
		Enable interaction.
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
		Show graphic_group 'icon'.
	sequence 'deactivate':
		Disable interaction.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Hide graphic_group 'icon'.
	sequence 'anim_open_door':
		Disable interaction.
		Enable animation_group 'anim':
			from 0/30
			to 35/30
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
		Play audio 'generic_door_wood_open' at 'hinge'.
		Hide graphic_group 'icon'.
	sequence 'anim_close_door':
		Enable interaction.
		Enable animation_group 'anim':
			from 35/30
			speed -1
			to 0/30
		Enable body 'body_hitbox_door_handle_in'.
		Enable body 'body_hitbox_door_handle_out'.
		Play audio 'generic_door_wood_open' at 'hinge'.
		Show graphic_group 'icon'.
	body 'body_hitbox_door_handle_in'
		Upon receiving 4 bullet hits, execute:
			Run sequence 'int_seq_bullet_hit_in'.
		Upon receiving 30 saw damage, execute:
			Run sequence 'int_seq_saw_in'.
		Upon receiving 10 explosion damage, execute:
			Run sequence 'int_seq_explosion_in'.
	body 'body_hitbox_door_handle_out'
		Upon receiving 4 bullet hits, execute:
			Run sequence 'int_seq_bullet_hit_out'.
		Upon receiving 30 saw damage, execute:
			Run sequence 'int_seq_saw_out'.
		Upon receiving 10 explosion damage, execute:
			Run sequence 'int_seq_explosion_out'.
	sequence 'int_seq_open':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Enable object 'g_door'.
		Disable object 'g_shot_dmg'.
		Disable object 'g_saw_dmg'.
	sequence 'int_seq_breach_common':
		Disable interaction.
		Disable body 'body_hitbox_door_handle_in'.
		Disable body 'body_hitbox_door_handle_out'.
	sequence 'int_seq_bullet_hit_in':
		effect 'effects/payday2/particles/wood/wood_door_shotgun_dmg_in':
			parent object( 'e_door_dst' )
			position v()
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Disable object 'g_door'.
		Enable object 'g_shot_dmg'.
		Disable object 'g_saw_dmg'.
	sequence 'int_seq_bullet_hit_out':
		effect 'effects/payday2/particles/wood/wood_door_shotgun_dmg_out':
			parent object( 'e_door_dst' )
			position v()
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Disable object 'g_door'.
		Enable object 'g_shot_dmg'.
		Disable object 'g_saw_dmg'.
	sequence 'int_seq_saw_in':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Disable object 'g_door'.
		Disable object 'g_shot_dmg'.
		Enable object 'g_saw_dmg'.
	sequence 'int_seq_saw_out':
		Run sequence 'int_seq_breach_common'.
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
		Disable object 'g_door'.
		Disable object 'g_shot_dmg'.
		Enable object 'g_saw_dmg'.
	sequence 'interact':
		Run sequence 'int_seq_open'.
	sequence 'sobj_swat_breach_in':
		Run sequence 'int_seq_slam_door'. (DELAY 63/30)
		Run sequence 'done_swat_breach'.
	sequence 'int_seq_slam_door':
		Enable animation_group 'anim':
			from 0/30
			speed 3
			to 35/30
		Play audio 'generic_door_wood_open' at 'hinge'.
		Run sequence 'int_seq_breach_common'.
		Run sequence 'done_opened'.
		Hide graphic_group 'icon'.
	sequence 'sound_explosion_in':
		TRIGGER TIMES 1
		Play audio 'c4_explode_wood' at 'door'.
		Hide graphic_group 'icon'.
	sequence 'int_seq_explosion_in':
		TRIGGER TIMES 1
		Run sequence 'int_seq_breach_common'.
		Run sequence 'sound_explosion_in'.
		Run sequence 'done_exploded'.
		Run sequence 'done_opened'.
		Disable body 'body_door'.
		Disable decal_mesh 'dm_wood_door'.
		Hide graphic_group 'icon'.
		Disable object 'g_door'.
		Enable object 'g_door_explode_dmg'.
		Run sequence 'int_seq_spawn_debris'.
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('c_push_in') ),120,40).
	sequence 'int_seq_explosion_out':
		TRIGGER TIMES 1
		Run sequence 'int_seq_breach_common'.
		Run sequence 'sound_explosion_in'.
		Run sequence 'done_exploded'.
		Run sequence 'done_opened'.
		Disable body 'body_door'.
		Disable decal_mesh 'dm_wood_door'.
		Hide graphic_group 'icon'.
		Disable object 'g_door'.
		Enable object 'g_door_explode_dmg'.
		Run sequence 'int_seq_spawn_debris'.
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('c_push_out') ),120,60).
	sequence 'open_door':
		Run sequence 'int_seq_open'.
	sequence 'done_exploded'.
	sequence 'done_opened'.
	sequence 'done_swat_breach'.
	sequence 'int_seq_spawn_debris':
		TRIGGER TIMES 1
		spawn_unit 'units/pd2_dlc1/equipment/gen_interactable_door_wooden_c/spawn_debris_door_wooden_c_v2_dirty':
			position object_pos('rp_gen_interactable_door_wooden_c_v2_dirty')
			rotation object_rot('rp_gen_interactable_door_wooden_c_v2_dirty')
