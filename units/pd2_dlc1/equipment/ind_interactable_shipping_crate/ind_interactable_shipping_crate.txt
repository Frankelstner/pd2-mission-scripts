unit:
	loot_type = 0
	open = 0
	sequence 'state_requires_crowbar':
		Call function: interaction.set_tweak_data('crate_loot_crowbar')
	sequence 'state_interaction_enabled':
		Enable interaction.
	sequence 'state_interaction_disabled':
		Disable interaction.
	sequence 'state_vis_show':
		Enable body 'body_static'.
		Enable body 'body_static_open'.
		Enable body 'body_mover_blocker'.
		Enable decal_mesh 'dm_wood'.
		Enable decal_mesh 'dm_metal'.
		Disable decal_mesh 'dm_wood_open'.
		Disable decal_mesh 'dm_metal_open'.
		Enable object 'g_g'.
		Enable object 's_s'.
		Disable object 'g_g_open'.
		Disable object 's_s_open'.
		Enable set_proximity 'proximity_check'.
	sequence 'state_vis_hide':
		Disable body 'body_static'.
		Disable body 'body_static_open'.
		Disable body 'body_mover_blocker'.
		Disable decal_mesh 'dm_wood'.
		Disable decal_mesh 'dm_metal'.
		Disable decal_mesh 'dm_wood_open'.
		Disable decal_mesh 'dm_metal_open'.
		Disable object 'g_g'.
		Disable object 's_s'.
		Disable object 'g_g_open'.
		Disable object 's_s_open'.
		Disable object 'g_unlocked'.
	sequence 'state_show_logo_1_w':
		Enable object 'g_decal_w'.
		Disable object 'g_decal_e'.
		Disable object 'g_decal_n'.
		Disable object 'g_decal_s'.
	sequence 'state_show_logo_1_e':
		Disable object 'g_decal_w'.
		Enable object 'g_decal_e'.
		Disable object 'g_decal_n'.
		Disable object 'g_decal_s'.
	sequence 'state_show_logo_1_n':
		Disable object 'g_decal_w'.
		Disable object 'g_decal_e'.
		Enable object 'g_decal_n'.
		Disable object 'g_decal_s'.
	sequence 'state_show_logo_1_s':
		Disable object 'g_decal_w'.
		Disable object 'g_decal_e'.
		Disable object 'g_decal_n'.
		Enable object 'g_decal_s'.
	sequence 'set_loot_type_a':
		loot_type = 1
	sequence 'set_loot_type_b':
		loot_type = 2
	sequence 'set_loot_type_c':
		loot_type = 3
	sequence 'set_loot_type_d':
		loot_type = 4
	sequence 'set_loot_type_empty':
		loot_type = 0
	sequence 'interact':
		If open == 1: Run sequence 'int_seq_close'.
		If open == 0: Run sequence 'int_seq_open'.
	sequence 'int_seq_open':
		Disable body 'body_static'.
		Enable body 'body_static_open'.
		Disable decal_mesh 'dm_wood'.
		Disable decal_mesh 'dm_metal'.
		Enable decal_mesh 'dm_wood_open'.
		Enable decal_mesh 'dm_metal_open'.
		Disable object 'g_g'.
		Disable object 's_s'.
		Enable object 'g_g_open'.
		Enable object 's_s_open'.
		Disable object 'g_unlocked'.
		If loot_type == 1: 
			spawn_unit 'units/payday2/pickups/gen_pku_diamonds/gen_pku_diamonds':
				position object_pos('spawn')
				rotation object_rot('spawn')
		If loot_type == 2: 
			spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_special_money':
				position object_pos('spawn')
				rotation object_rot('spawn')
		If loot_type == 3: 
			spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_special_gold':
				position object_pos('spawn')
				rotation object_rot('spawn')
		If loot_type == 4: 
			spawn_unit 'units/payday2/pickups/gen_pku_toolbag/gen_pku_toolbag':
				position object_pos('spawn')
				rotation object_rot('spawn')
		Play audio 'open_crate_end' at 'spawn'.
		Call function: interaction.set_tweak_data('crate_loot_close')
		open = 1
	sequence 'int_seq_close':
		Enable body 'body_static'.
		Disable body 'body_static_open'.
		Enable decal_mesh 'dm_wood'.
		Enable decal_mesh 'dm_metal'.
		Disable decal_mesh 'dm_wood_open'.
		Disable decal_mesh 'dm_metal_open'.
		Disable object 'g_g'.
		Enable object 's_s'.
		Disable object 'g_g_open'.
		Disable object 's_s_open'.
		Enable object 'g_unlocked'.
		Play audio 'close_crate_end' at 'spawn'.
		Call function: interaction.set_tweak_data('crate_loot')
	proximities:
		Disable proximity 'proximity_check':
			within:
				Run sequence 'done_proximity'.
				max_activations 1
				range 200
			interval 1
			ref_object 'attention'
			type 'players'
	sequence 'done_proximity'.
