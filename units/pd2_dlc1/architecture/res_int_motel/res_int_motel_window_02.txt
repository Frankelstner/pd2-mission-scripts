unit:
	sequence 'damage':
		TRIGGER TIMES 1
		Disable object 'g_glass'.
		Enable object 'g_glass_dmg'.
		Play audio 'glass_crack' at 'e_01'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_01' (alarm reason: 'glass').
	sequence 'destroy':
		TRIGGER TIMES 1
		Disable object 'g_glass'.
		Disable object 'g_glass_dmg'.
		Enable object 'g_g_dmg'.
		Disable object 'g_g'.
		Disable object 's_s'.
		Enable object 's_s_dmg'.
		Play audio 'window_large_shatter' at 'e_01'.
		Disable decal_mesh 'dm_glass_breakable'.
		effect 'effects/payday2/particles/window/storefront_window_large':
			parent object( 'e_01' )
			position v()
		Cause alert with 12 m radius.
		Disable body 'glass_body'.
		Enable body 'physics'.
	body 'glass_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'damage'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy'.
