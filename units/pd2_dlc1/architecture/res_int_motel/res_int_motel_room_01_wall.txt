unit:
	sequence 'single_wall':
		Run sequence 'clear'.
		Enable body 'static_normal'.
		Enable object 'g_wall_normal_single'.
	sequence 'double_wall':
		Run sequence 'clear'.
		Enable body 'static_normal'.
		Enable object 'g_wall_normal_double'.
	sequence 'tunnel':
		Run sequence 'clear'.
		Enable body 'static_tunnel'.
		Enable body 'nav_tunnel'.
		Enable object 'g_wall_tunnel'.
	sequence 'clear':
		Disable body 'static_normal'.
		Disable body 'static_tunnel'.
		Disable body 'nav_tunnel'.
		Disable object 'g_wall_normal_single'.
		Disable object 'g_wall_normal_double'.
		Disable object 'g_wall_tunnel'.
