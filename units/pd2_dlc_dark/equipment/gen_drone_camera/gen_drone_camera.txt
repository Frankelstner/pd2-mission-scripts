unit:
	camera_charge_del = 10
	def_led_color = 2
	take_off_len = 150/30
	curr_anim_id = 0
	curr_anim_len = 0
	broken = 0
	led_color = 0
	deathwish = 0
	sequence 'anim_land':
		If curr_anim_id == 1: Run sequence 'int_seq_fly_right_6m_land'.
		If curr_anim_id == 2: Run sequence 'int_seq_fly_right_12m_land'.
		If curr_anim_id == 3: Run sequence 'int_seq_fly_left_6m_land'.
		If curr_anim_id == 4: Run sequence 'int_seq_fly_left_12m_land'.
		If curr_anim_id == 5: Run sequence 'int_seq_fly_forward_6m_land'.
		If curr_anim_id == 6: Run sequence 'int_seq_fly_forward_12m_land'.
		If curr_anim_id == 7: Run sequence 'int_seq_fly_backward_6m_land'.
		If curr_anim_id == 8: Run sequence 'int_seq_fly_backward_12m_land'.
		Run sequence 'int_seq_disable_hitbox'. (DELAY vars.curr_anim_len*0.8)
		Play audio 'drone_loop_stop' at 'snd'. (DELAY vars.curr_anim_len-4)
	sequence 'anim_fly_right_6m':
		curr_anim_id = 1
		curr_anim_len = 350/30
		led_color = vars.def_led_color
		Run sequence 'anim_take_off'.
		If broken == 1: animation_redirect 'broken_loop'. (DELAY vars.take_off_len)
		If broken == 0: animation_redirect 'hover_loop'. (DELAY vars.take_off_len)
		animation_redirect 'fly_right_6m'. (DELAY vars.take_off_len)
		If deathwish == 0: Run sequence 'int_seq_enable_hitbox'. (DELAY vars.take_off_len)
		Run sequence 'done_anim'. (DELAY vars.curr_anim_len+vars.take_off_len)
	sequence 'int_seq_fly_right_6m_land':
		curr_anim_len = 350/30
		animation_redirect 'fly_right_6m_land'.
		animation_redirect 'hover_stop'. (DELAY vars.curr_anim_len-1.5)
		animation_redirect 'propeller_stop'. (DELAY vars.curr_anim_len-1.5)
		Run sequence 'int_seq_blades_solid'. (DELAY vars.curr_anim_len+1.5)
		Run sequence 'done_landed'. (DELAY vars.curr_anim_len)
	sequence 'anim_fly_right_12m':
		curr_anim_id = 2
		curr_anim_len = 550/30
		led_color = vars.def_led_color
		Run sequence 'anim_take_off'.
		If broken == 1: animation_redirect 'broken_loop'. (DELAY vars.take_off_len)
		If broken == 0: animation_redirect 'hover_loop'. (DELAY vars.take_off_len)
		animation_redirect 'fly_right_12m'. (DELAY vars.take_off_len)
		If deathwish == 0: Run sequence 'int_seq_enable_hitbox'. (DELAY vars.take_off_len)
		Run sequence 'done_anim'. (DELAY vars.curr_anim_len+vars.take_off_len)
	sequence 'int_seq_fly_right_12m_land':
		curr_anim_len = 550/30
		animation_redirect 'fly_right_12m_land'.
		animation_redirect 'hover_stop'. (DELAY vars.curr_anim_len-1.5)
		animation_redirect 'propeller_stop'. (DELAY vars.curr_anim_len-1.5)
		Run sequence 'int_seq_blades_solid'. (DELAY vars.curr_anim_len+1.5)
		Run sequence 'done_landed'. (DELAY vars.curr_anim_len)
	sequence 'anim_fly_left_6m':
		curr_anim_id = 3
		curr_anim_len = 350/30
		led_color = vars.def_led_color
		Run sequence 'anim_take_off'.
		If broken == 1: animation_redirect 'broken_loop'. (DELAY vars.take_off_len)
		If broken == 0: animation_redirect 'hover_loop'. (DELAY vars.take_off_len)
		animation_redirect 'fly_left_6m'. (DELAY vars.take_off_len)
		If deathwish == 0: Run sequence 'int_seq_enable_hitbox'. (DELAY vars.take_off_len)
		Run sequence 'done_anim'. (DELAY vars.curr_anim_len+vars.take_off_len)
	sequence 'int_seq_fly_left_6m_land':
		curr_anim_len = 350/30
		animation_redirect 'fly_left_6m_land'.
		animation_redirect 'hover_stop'. (DELAY vars.curr_anim_len-1.5)
		animation_redirect 'propeller_stop'. (DELAY vars.curr_anim_len-1.5)
		Run sequence 'int_seq_blades_solid'. (DELAY vars.curr_anim_len+1.5)
		Run sequence 'done_landed'. (DELAY vars.curr_anim_len)
	sequence 'anim_fly_left_12m':
		curr_anim_id = 4
		curr_anim_len = 550/30
		led_color = vars.def_led_color
		Run sequence 'anim_take_off'.
		If broken == 1: animation_redirect 'broken_loop'. (DELAY vars.take_off_len)
		If broken == 0: animation_redirect 'hover_loop'. (DELAY vars.take_off_len)
		animation_redirect 'fly_left_12m'. (DELAY vars.take_off_len)
		If deathwish == 0: Run sequence 'int_seq_enable_hitbox'. (DELAY vars.take_off_len)
		Run sequence 'done_anim'. (DELAY vars.curr_anim_len+vars.take_off_len)
	sequence 'int_seq_fly_left_12m_land':
		curr_anim_len = 550/30
		animation_redirect 'fly_left_12m_land'.
		animation_redirect 'hover_stop'. (DELAY vars.curr_anim_len-1.5)
		animation_redirect 'propeller_stop'. (DELAY vars.curr_anim_len-1.5)
		Run sequence 'int_seq_blades_solid'. (DELAY vars.curr_anim_len+1.5)
		Run sequence 'done_landed'. (DELAY vars.curr_anim_len)
	sequence 'anim_fly_forward_6m':
		curr_anim_id = 5
		curr_anim_len = 350/30
		led_color = vars.def_led_color
		Run sequence 'anim_take_off'.
		If broken == 1: animation_redirect 'broken_loop'. (DELAY vars.take_off_len)
		If broken == 0: animation_redirect 'hover_loop'. (DELAY vars.take_off_len)
		animation_redirect 'fly_forward_6m'. (DELAY vars.take_off_len)
		If deathwish == 0: Run sequence 'int_seq_enable_hitbox'. (DELAY vars.take_off_len)
		Run sequence 'done_anim'. (DELAY vars.curr_anim_len+vars.take_off_len)
	sequence 'int_seq_fly_forward_6m_land':
		curr_anim_len = 350/30
		animation_redirect 'fly_forward_6m_land'.
		animation_redirect 'hover_stop'. (DELAY vars.curr_anim_len-1.5)
		animation_redirect 'propeller_stop'. (DELAY vars.curr_anim_len-1.5)
		Run sequence 'int_seq_blades_solid'. (DELAY vars.curr_anim_len+1.5)
		Run sequence 'done_landed'. (DELAY vars.curr_anim_len)
	sequence 'anim_fly_forward_12m':
		curr_anim_id = 6
		curr_anim_len = 550/30
		led_color = vars.def_led_color
		Run sequence 'anim_take_off'.
		If broken == 1: animation_redirect 'broken_loop'. (DELAY vars.take_off_len)
		If broken == 0: animation_redirect 'hover_loop'. (DELAY vars.take_off_len)
		animation_redirect 'fly_forward_12m'. (DELAY vars.take_off_len)
		If deathwish == 0: Run sequence 'int_seq_enable_hitbox'. (DELAY vars.take_off_len)
		Run sequence 'done_anim'. (DELAY vars.curr_anim_len+vars.take_off_len)
	sequence 'int_seq_fly_forward_12m_land':
		curr_anim_len = 550/30
		animation_redirect 'fly_forward_12m_land'.
		animation_redirect 'hover_stop'. (DELAY vars.curr_anim_len-1.5)
		animation_redirect 'propeller_stop'. (DELAY vars.curr_anim_len-1.5)
		Run sequence 'int_seq_blades_solid'. (DELAY vars.curr_anim_len+1.5)
		Run sequence 'done_landed'. (DELAY vars.curr_anim_len)
	sequence 'anim_fly_backward_6m':
		curr_anim_id = 7
		curr_anim_len = 350/30
		led_color = vars.def_led_color
		Run sequence 'anim_take_off'.
		If broken == 1: animation_redirect 'broken_loop'. (DELAY vars.take_off_len)
		If broken == 0: animation_redirect 'hover_loop'. (DELAY vars.take_off_len)
		animation_redirect 'fly_backward_6m'. (DELAY vars.take_off_len)
		If deathwish == 0: Run sequence 'int_seq_enable_hitbox'. (DELAY vars.take_off_len)
		Run sequence 'done_anim'. (DELAY vars.curr_anim_len+vars.take_off_len)
	sequence 'int_seq_fly_backward_6m_land':
		curr_anim_len = 350/30
		animation_redirect 'fly_backward_6m_land'.
		animation_redirect 'hover_stop'. (DELAY vars.curr_anim_len-1.5)
		animation_redirect 'propeller_stop'. (DELAY vars.curr_anim_len-1.5)
		Run sequence 'int_seq_blades_solid'. (DELAY vars.curr_anim_len+1.5)
		Run sequence 'done_landed'. (DELAY vars.curr_anim_len)
	sequence 'anim_fly_backward_12m':
		curr_anim_id = 8
		curr_anim_len = 550/30
		led_color = vars.def_led_color
		Run sequence 'anim_take_off'.
		If broken == 1: animation_redirect 'broken_loop'. (DELAY vars.take_off_len)
		If broken == 0: animation_redirect 'hover_loop'. (DELAY vars.take_off_len)
		animation_redirect 'fly_backward_12m'. (DELAY vars.take_off_len)
		If deathwish == 0: Run sequence 'int_seq_enable_hitbox'. (DELAY vars.take_off_len)
		Run sequence 'done_anim'. (DELAY vars.curr_anim_len+vars.take_off_len)
	sequence 'int_seq_fly_backward_12m_land':
		curr_anim_len = 550/30
		animation_redirect 'fly_backward_12m_land'.
		animation_redirect 'hover_stop'. (DELAY vars.curr_anim_len-1.5)
		animation_redirect 'propeller_stop'. (DELAY vars.curr_anim_len-1.5)
		Run sequence 'int_seq_blades_solid'. (DELAY vars.curr_anim_len+1.5)
		Run sequence 'done_landed'. (DELAY vars.curr_anim_len)
	sequence 'anim_take_off':
		If broken == 1: Run sequence 'int_seq_start_broken'.
		Call function: base.set_access_camera_enabled(True) (DELAY vars.take_off_len)
		Run sequence 'int_seq_blades_trans'. (DELAY vars.take_off_len*0.9)
		animation_redirect 'take_off'.
		Play audio 'drone_loop_start' at 'snd'.
	sequence 'state_camera_charging':
		Run sequence 'int_seq_led_blink_off'.
		Run sequence 'int_seq_spot_off'.
		led_color = 0
		Run sequence 'int_seq_led_blink'.
		Run sequence 'int_seq_spot_on'.
		Play audio 'drone_camera_charge' at 'CameraPitch'.
		Run sequence 'done_camera_charged'. (DELAY vars.camera_charge_del)
	sequence 'state_camera_active':
		Run sequence 'int_seq_led_blink_off'.
		Run sequence 'int_seq_spot_off'.
		led_color = vars.def_led_color
		Run sequence 'int_seq_led_blink'.
		Run sequence 'int_seq_spot_on'.
	sequence 'state_camera_deactivate':
		Run sequence 'int_seq_spot_off'.
		Run sequence 'int_seq_led_blink_off'.
		Play audio 'drone_camera_deactivate' at 'CameraPitch'.
	sequence 'state_ecm_on':
		Run sequence 'int_seq_led_blink_off'.
		Run sequence 'int_seq_spot_off'.
		led_color = 1
		Run sequence 'int_seq_led_blink'.
		Run sequence 'int_seq_spot_on'.
		Play audio 'drone_camera_malfunction_loop' at 'snd'.
	sequence 'state_ecm_off':
		Run sequence 'state_camera_active'.
		Play audio 'drone_camera_malfunction_stop' at 'snd'.
	sequence 'stop':
		broken = 1
		Run sequence 'int_seq_led_blink_off'.
		Run sequence 'int_seq_spot_off'.
		led_color = 0
		Run sequence 'int_seq_led_blink'.
		Run sequence 'int_seq_spot_on'.
		Enable effect_spawner 'es_sparks'.
		Add attention/detection preset 'broken_cam_ene_ntl' (alarm reason: 'broken_cam').
		Call function: base.generate_cooldown(1)
		animation_redirect 'broken_loop'.
		Play audio 'emitter_security_camera_explode' at 'CameraPitch'.
	sequence 'state_deathwish':
		Disable object 'g_body'.
		Enable object 'g_body_dw'.
		Run sequence 'int_seq_disable_hitbox'.
		deathwish = 1
	sequence 'done_anim'.
	sequence 'done_landed':
		Call function: base.set_access_camera_enabled(False)
		Play audio 'drone_camera_malfunction_stop' at 'snd'.
		Remove attention/detection preset: 'broken_cam_ene_ntl'
	sequence 'done_camera_charged'.
	sequence 'int_seq_start_broken':
		Run sequence 'int_seq_led_blink_off'.
		led_color = 0
		Run sequence 'int_seq_led_blink'.
		Add attention/detection preset 'broken_cam_ene_ntl' (alarm reason: 'broken_cam').
	sequence 'int_seq_enable_hitbox':
		Disable body 'body_static'.
		Enable body 'body_hitbox'.
	sequence 'int_seq_disable_hitbox':
		Enable body 'body_static'.
		Disable body 'body_hitbox'.
	sequence 'int_seq_blades_solid':
		Disable object 'g_blade_1'.
		Disable object 'g_blade_2'.
		Disable object 'g_blade_3'.
		Disable object 'g_blade_4'.
		Enable object 'g_blade_solid_1'.
		Enable object 'g_blade_solid_2'.
		Enable object 'g_blade_solid_3'.
		Enable object 'g_blade_solid_4'.
	sequence 'int_seq_blades_trans':
		Enable object 'g_blade_1'.
		Enable object 'g_blade_2'.
		Enable object 'g_blade_3'.
		Enable object 'g_blade_4'.
		Disable object 'g_blade_solid_1'.
		Disable object 'g_blade_solid_2'.
		Disable object 'g_blade_solid_3'.
		Disable object 'g_blade_solid_4'.
	sequence 'int_seq_led_blink':
		If led_color == 1: Enable object 'g_led_01'.
		If led_color == 1: Enable effect_spawner 'es_lights_blue_01'.
		If led_color == 1: Enable effect_spawner 'es_lights_blue_02'.
		If led_color == 1: Enable effect_spawner 'es_lights_blue_03'.
		If led_color == 1: Enable effect_spawner 'es_lights_blue_04'.
		If led_color == 2: Enable object 'g_led_02'.
		If led_color == 2: Enable effect_spawner 'es_lights_white_01'.
		If led_color == 2: Enable effect_spawner 'es_lights_white_02'.
		If led_color == 2: Enable effect_spawner 'es_lights_white_03'.
		If led_color == 2: Enable effect_spawner 'es_lights_white_04'.
		If led_color == 0: Enable object 'g_led_03'.
		If led_color == 0: Enable effect_spawner 'es_lights_red_01'.
		If led_color == 0: Enable effect_spawner 'es_lights_red_02'.
		If led_color == 0: Enable effect_spawner 'es_lights_red_03'.
		If led_color == 0: Enable effect_spawner 'es_lights_red_04'.
	sequence 'int_seq_led_blink_off':
		Disable object 'g_led_01'.
		Disable effect_spawner 'es_lights_blue_01'.
		Disable effect_spawner 'es_lights_blue_02'.
		Disable effect_spawner 'es_lights_blue_03'.
		Disable effect_spawner 'es_lights_blue_04'.
		Disable object 'g_led_02'.
		Disable effect_spawner 'es_lights_white_01'.
		Disable effect_spawner 'es_lights_white_02'.
		Disable effect_spawner 'es_lights_white_03'.
		Disable effect_spawner 'es_lights_white_04'.
		Disable object 'g_led_03'.
		Disable effect_spawner 'es_lights_red_01'.
		Disable effect_spawner 'es_lights_red_02'.
		Disable effect_spawner 'es_lights_red_03'.
		Disable effect_spawner 'es_lights_red_04'.
	sequence 'int_seq_spot_on':
		Enable object 'g_light_cone'.
		If led_color == 0: Enable light 'li_light_red'.
		If led_color == 1: Enable light 'li_light_blue'.
		If led_color == 2: Enable light 'li_light_white'.
	sequence 'int_seq_spot_off':
		Disable object 'g_light_cone'.
		Disable light 'li_light_red'.
		Disable light 'li_light_blue'.
		Disable light 'li_light_white'.
	body 'body_hitbox'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'stop'.
