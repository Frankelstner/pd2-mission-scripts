unit:
	sequence 'state_hide':
		Disable interaction.
		Disable body 'body_closed'.
		Disable body 'body_open'.
		Disable decal_mesh 'g_weapon_case'.
		Disable decal_mesh 'dm_medium_open'.
		Disable decal_mesh 'dm_foam'.
		Hide graphic_group 'grp_wpn'.
	sequence 'state_show':
		Enable interaction.
		Enable body 'body_closed'.
		Disable body 'body_open'.
		Enable decal_mesh 'g_weapon_case'.
		Disable decal_mesh 'dm_medium_open'.
		Disable decal_mesh 'dm_foam'.
		Show graphic_group 'grp_wpn'.
	sequence 'interact':
		spawn_unit 'units/pd2_dlc_dark/equipment/drk_interactable_weapon_case_2x1/spawn_scar':
			position object_pos('rp_drk_interactable_weapon_case_2x1')
			rotation object_rot('rp_drk_interactable_weapon_case_2x1')
		Play audio 'weapon_box_open' at 'anim_lock'.
		Enable animation_group 'anim'.
		Disable body 'body_closed'.
		Enable body 'body_open'.
		Disable decal_mesh 'g_weapon_case'.
		Enable decal_mesh 'dm_medium_open'.
		Enable decal_mesh 'dm_foam'.
