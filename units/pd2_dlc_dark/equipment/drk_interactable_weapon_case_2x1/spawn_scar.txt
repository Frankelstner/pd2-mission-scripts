unit:
	sequence 'enable_interaction':
		EXECUTE ON STARTUP
		Enable interaction.
		startup True
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'interact'.
	sequence 'load'.
