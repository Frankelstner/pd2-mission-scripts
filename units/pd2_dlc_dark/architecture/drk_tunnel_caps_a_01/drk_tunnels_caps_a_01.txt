unit:
	sequence 'hide_cap_01':
		Disable object 'g_door_a_01'.
		Enable object 'g_frame_a_01'.
		Disable decal_mesh 'dm_concrete_01'.
		Enable decal_mesh 'dm_metal_01'.
		Disable body 'col_01'.
	sequence 'hide_cap_02':
		Disable object 'g_door_a_02'.
		Enable object 'g_frame_a_02'.
		Disable decal_mesh 'dm_concrete_02'.
		Enable decal_mesh 'dm_metal_02'.
		Disable body 'col_02'.
	sequence 'hide_cap_03':
		Disable object 'g_door_a_03'.
		Enable object 'g_frame_a_03'.
		Disable decal_mesh 'dm_concrete_03'.
		Enable decal_mesh 'dm_metal_03'.
		Disable body 'col_03'.
	sequence 'show_all':
		Enable object 'g_door_a_01'.
		Enable object 'g_door_a_02'.
		Enable object 'g_door_a_03'.
		Disable object 'g_frame_a_01'.
		Disable object 'g_frame_a_02'.
		Disable object 'g_frame_a_03'.
		Enable decal_mesh 'dm_concrete_01'.
		Enable decal_mesh 'dm_concrete_02'.
		Enable decal_mesh 'dm_concrete_03'.
		Disable decal_mesh 'dm_metal_01'.
		Disable decal_mesh 'dm_metal_02'.
		Disable decal_mesh 'dm_metal_03'.
		Enable body 'col_01'.
		Enable body 'col_02'.
		Enable body 'col_03'.
