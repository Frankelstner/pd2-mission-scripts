unit:
	sequence 'interaction_enabled':
		Enable interaction.
	sequence 'interaction_disabled':
		Disable interaction.
	sequence 'hide':
		Disable object 'g_L_brackets'.
		Disable object 'g_grilles'.
		Disable object 's_s'.
		Disable decal_mesh 'dm_metal'.
		Disable body 'body_collision_01'.
		Enable body 'body_collision_01_editor'.
	sequence 'show':
		Enable object 'g_L_brackets'.
		Enable object 'g_grilles'.
		Enable object 's_s'.
		Enable decal_mesh 'dm_metal'.
		Enable body 'body_collision_01'.
		Disable body 'body_collision_01_editor'.
	sequence 'open_vent_left':
		Enable animation_group 'anim_open':
			from 0/30
			to 64/30
	sequence 'open_vent_right':
		Enable animation_group 'anim_open':
			from 65/30
			to 129/30
	sequence 'open_vent_right_short':
		Enable animation_group 'anim_open':
			from 181/30
			to 212/30
	sequence 'open_vent_down':
		Enable animation_group 'anim_open':
			from 130/30
			to 179/30
	sequence 'reset':
		Enable animation_group 'anim_open':
			from 0/30
			to 0/30
	sequence 'close_vent_down':
		Enable animation_group 'anim_open':
			from 179/30
			speed -1
			to 130/30
	sequence 'interact'.
