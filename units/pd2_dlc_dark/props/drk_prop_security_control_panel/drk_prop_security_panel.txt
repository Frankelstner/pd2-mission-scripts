unit:
	sequence 'state_interaction_enabled':
		Enable interaction.
	sequence 'state_interaction_disabled':
		Disable interaction.
	sequence 'state_vis_show':
		Enable body 'body_static'.
		Enable decal_mesh 'dm_metal'.
		Show graphic_group 'grp_geo'.
		Call function: blink.set_state('disable','0')
	sequence 'state_vis_hide':
		Disable interaction.
		Disable body 'body_static'.
		Disable decal_mesh 'dm_metal'.
		Hide graphic_group 'grp_geo'.
		Call function: blink.set_state('disable','0')
	sequence 'state_doors_locked':
		Enable object 'g_g_screen_locked'.
		Disable object 'g_g_screen_unlocked'.
		Call function: blink.set_state('disable','0')
	sequence 'state_doors_unlocked':
		Disable object 'g_g_screen_locked'.
		Enable object 'g_g_screen_unlocked'.
		Call function: blink.set_state('disable','0')
	sequence 'state_doors_jammed':
		Disable interaction.
		Disable object 'g_g_screen_locked'.
		Disable object 'g_g_screen_unlocked'.
		Call function: blink.set_state('cycle','0.75')
	sequence 'interact'.
