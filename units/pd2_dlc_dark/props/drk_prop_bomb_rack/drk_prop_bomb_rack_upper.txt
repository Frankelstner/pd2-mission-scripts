unit:
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'hide':
		Disable object 'g_g'.
		Disable object 'g_belts_tight'.
		Disable object 'g_belts_loose'.
		Disable object 'g_bomb'.
		Disable object 's_s'.
		Disable object 'g_shadow_bomb'.
		Disable object 'g_shadow_rack_upper'.
		Disable body 'body_static_bomb'.
		Disable body 'body_static'.
		Disable interaction.
		Disable decal_mesh 'dm_metal'.
	sequence 'show':
		Enable object 'g_g'.
		Enable object 'g_belts_tight'.
		Disable object 'g_belts_loose'.
		Enable object 'g_bomb'.
		Enable object 's_s'.
		Enable object 'g_shadow_bomb'.
		Enable object 'g_shadow_rack_upper'.
		Enable body 'body_static_bomb'.
		Enable body 'body_static'.
		Enable decal_mesh 'dm_metal'.
		Enable interaction.
	sequence 'belts_tight':
		Enable object 'g_belts_tight'.
		Disable object 'g_belts_loose'.
	sequence 'belts_loose':
		Disable object 'g_belts_tight'.
		Enable object 'g_belts_loose'.
	sequence 'interact':
		Run sequence 'belts_loose'.
		Disable object 'g_bomb'.
		Disable object 'g_shadow_bomb'.
		Disable interaction.
		Disable body 'body_static_bomb'.
	sequence 'load'.
