unit:
	sequence 'light_on':
		Enable light 'li_desk_lamp'.
		material_config 'units/pd2_dlc_cage/props/are_prop_office_lamp_desk/are_prop_office_lamp_desk_on'.
	sequence 'light_off':
		Disable light 'li_desk_lamp'.
		material_config 'units/pd2_dlc_cage/props/are_prop_office_lamp_desk/are_prop_office_lamp_desk_off'.
	sequence 'break_int':
		TRIGGER TIMES 1
		Run sequence 'light_off'.
		Play audio 'light_bulb_smash' at 'e_effect'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_effect' )
			position v()
	sequence 'make_dynamic':
		Disable body 'body_static'.
		Enable body 'body_dynamic'.
		slot:
			slot 11
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_int'.
			Run sequence 'make_dynamic'.
