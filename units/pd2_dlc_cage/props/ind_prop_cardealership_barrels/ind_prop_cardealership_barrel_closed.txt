unit:
	body 'body_static'
		Upon receiving 1 bullet hit or 1 collision damage or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_make_dynamic'.
	body 'body_dynamic_01'
		Upon receiving 10 collision damage, execute:
			Run sequence 'int_seq_break'.
			Run sequence 'int_seq_car_hit'.
			Run sequence 'int_seq_reset_endurance'.
	sequence 'int_seq_make_dynamic':
		slot:
			slot 11
		Disable body 'body_static'.
		Enable body 'body_dynamic_01'.
	sequence 'int_seq_break':
		TRIGGER TIMES 1
		Disable object 'g_barrel_lod0'.
		Disable object 'g_barrel_lod1'.
		Enable object 'g_barrel_dest_lod0'.
		Enable object 'g_barrel_dest_lod1'.
	sequence 'int_seq_car_hit':
		Play audio 'car_hit_oil_drum' at 'e_effect'.
	sequence 'int_seq_reset_endurance':
		Set collision damage to 0.
	sequence 'car_destructable':
		Run sequence 'int_seq_make_dynamic'.
