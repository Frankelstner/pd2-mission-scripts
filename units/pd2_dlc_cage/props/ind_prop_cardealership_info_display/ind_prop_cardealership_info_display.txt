unit:
	sequence 'state_break':
		TRIGGER TIMES 1
		Show graphic_group 'grp_dmg'.
		Hide graphic_group 'grp_clean'.
		Disable object 'g_glow'.
		Disable decal_mesh 'dm_glass_breakable'.
		effect 'effects/payday2/particles/explosions/sparks/sparks':
			parent object( 'effect' )
			position v()
		Play audio 'window_small_shatter' at 'effect'.
	body 'body_dmg'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'state_break'.
