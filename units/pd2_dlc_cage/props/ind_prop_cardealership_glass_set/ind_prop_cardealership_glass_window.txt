unit:
	sequence 'destroy_01':
		TRIGGER TIMES 1
		Disable object 'g_glass'.
		Enable object 'g_glass_dmg'.
		Play audio 'glass_crack' at 'e_effect1'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_effect1' (alarm reason: 'glass').
	sequence 'destroy_02':
		TRIGGER TIMES 1
		Disable object 'g_glass'.
		Disable object 'g_glass_dmg'.
		Play audio 'window_medium_shatter' at 'e_effect1'.
		Disable decal_mesh 'dm_glass'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_effect1' )
			position v()
		Enable object 'g_glass_shattered'.
		Disable body 'window1'.
		Cause alert with 12 m radius.
	body 'window1'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_01'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_02'.
