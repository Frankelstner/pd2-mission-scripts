unit:
	sequence 'make_dynamic':
		TRIGGER TIMES 1
		Disable body 'static_body'.
		Enable body 'dynamic_body'.
		slot:
			slot 11
		Play audio 'cone_hit_large' at 'e_pow'.
	sequence 'car_destructable':
		TRIGGER TIMES 1
		Run sequence 'make_dynamic'.
	body 'static_body'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'make_dynamic'.
