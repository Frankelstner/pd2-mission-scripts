unit:
	sequence 'state_open':
		Enable animation_group 'anim_grp_window':
			from 0/30
			to 30/30
	sequence 'break_window_01':
		Disable object 'g_glass_01'.
		Disable body 'body_window_01'.
		effect 'effects/payday2/particles/window/office_divider_glass':
			parent object( 'e_01' )
			position v()
		Play audio 'window_small_shatter' at 'e_01'.
	sequence 'break_window_02':
		Disable object 'g_glass_02'.
		Disable body 'body_window_02'.
		effect 'effects/payday2/particles/window/office_divider_glass':
			parent object( 'e_02' )
			position v()
		Play audio 'window_small_shatter' at 'e_02'.
	sequence 'break_window_03':
		Disable object 'g_glass_03'.
		Disable body 'body_anim_window'.
		effect 'effects/payday2/particles/window/office_divider_glass':
			parent object( 'e_03' )
			position v()
		Play audio 'window_small_shatter' at 'e_03'.
	sequence 'interact_enabled':
		Enable interaction.
		Show graphic_group 'grp_icon'.
	sequence 'interact_disabled':
		Disable interaction.
		Hide graphic_group 'grp_icon'.
	sequence 'interact':
		Run sequence 'state_open'.
		Run sequence 'interact_disabled'.
	body 'body_window_01'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_window_01'.
	body 'body_window_02'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_window_02'.
	body 'body_anim_window'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_window_03'.
