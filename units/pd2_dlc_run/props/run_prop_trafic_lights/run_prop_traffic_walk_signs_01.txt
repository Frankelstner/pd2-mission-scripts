unit:
	sequence 'lights':
		Enable object 'g_on_g'.
		Disable object 'g_off_g'.
		Enable object 'g_on_g'. (DELAY 10)
		Disable object 'g_off_g'. (DELAY 10)
		Disable object 'g_on_g'. (DELAY 12)
		Enable object 'g_off_g'. (DELAY 12)
		Disable object 'g_on_g'. (DELAY 22)
		Enable object 'g_off_g'. (DELAY 22)
		Run sequence 'lights'. (DELAY 24)
