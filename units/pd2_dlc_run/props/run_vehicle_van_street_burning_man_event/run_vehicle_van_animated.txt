unit:
	sequence 'getaway_anim':
		Enable animation_group 'getaway':
			from 323/30
			to 705/30
		Play audio 'run_van_escape' at 'a_animation_nod'.
	sequence 'play_sound_idle':
		Play audio 'van_escape_idle' at 'a_animation_nod'.
	sequence 'show':
		Show graphic_group 'grp_body'.
		Show graphic_group 'grp_door_left'.
		Show graphic_group 'grp_door_right'.
		Show graphic_group 'grp_wheel_FL'.
		Show graphic_group 'grp_wheel_FR'.
		Show graphic_group 'grp_wheel_RR'.
		Enable object 's_s_door_right'.
		Enable object 's_s_door_left'.
		Enable object 's_s_body'.
		Enable object 's_s_wheelFR'.
		Enable object 's_s_wheelFL'.
		Enable object 's_s_wheelR'.
		Enable decal_mesh 'dm_glass_unbreakable_body'.
		Enable decal_mesh 'dm_glass_unbreakable_door_left'.
		Enable decal_mesh 'dm_glass_unbreakable_door_right'.
		Enable decal_mesh 'dm_metal_body'.
		Enable decal_mesh 'dm_metal_door_left'.
		Enable decal_mesh 'dm_metal_door_right'.
		Enable decal_mesh 'dm_metal_wheelFL'.
		Enable decal_mesh 'dm_metal_wheelFR'.
		Enable decal_mesh 'dm_metal_wheelR'.
		Enable decal_mesh 'dm_plastic_body'.
		Enable decal_mesh 'dm_plastic_door_right'.
		Enable decal_mesh 'dm_rubber_wheelFL'.
		Enable decal_mesh 'dm_rubber_wheelFR'.
		Enable decal_mesh 'dm_rubber_wheelR'.
		Enable body 'body_collision_01'.
		Enable body 'body_collision_02'.
		Enable body 'body_collision_03'.
		Enable body 'body_collision_04'.
		Enable body 'body_collision_05'.
		Enable body 'body_collision_06'.
		Enable body 'body_collision_07'.
	sequence 'hide':
		Hide graphic_group 'grp_body'.
		Hide graphic_group 'grp_door_left'.
		Hide graphic_group 'grp_door_right'.
		Hide graphic_group 'grp_wheel_FL'.
		Hide graphic_group 'grp_wheel_FR'.
		Hide graphic_group 'grp_wheel_RR'.
		Disable object 's_s_door_right'.
		Disable object 's_s_door_left'.
		Disable object 's_s_body'.
		Disable object 's_s_wheelFR'.
		Disable object 's_s_wheelFL'.
		Disable object 's_s_wheelR'.
		Disable decal_mesh 'dm_glass_unbreakable_body'.
		Disable decal_mesh 'dm_glass_unbreakable_door_left'.
		Disable decal_mesh 'dm_glass_unbreakable_door_right'.
		Disable decal_mesh 'dm_metal_body'.
		Disable decal_mesh 'dm_metal_door_left'.
		Disable decal_mesh 'dm_metal_door_right'.
		Disable decal_mesh 'dm_metal_wheelFL'.
		Disable decal_mesh 'dm_metal_wheelFR'.
		Disable decal_mesh 'dm_metal_wheelR'.
		Disable decal_mesh 'dm_plastic_body'.
		Disable decal_mesh 'dm_plastic_door_right'.
		Disable decal_mesh 'dm_rubber_wheelFL'.
		Disable decal_mesh 'dm_rubber_wheelFR'.
		Disable decal_mesh 'dm_rubber_wheelR'.
		Disable body 'body_collision_01'.
		Disable body 'body_collision_02'.
		Disable body 'body_collision_03'.
		Disable body 'body_collision_04'.
		Disable body 'body_collision_05'.
		Disable body 'body_collision_06'.
		Disable body 'body_collision_07'.
	sequence 'close_rear_doors':
		Enable animation_group 'doors':
			from 0/30
			speed 1
			to 25/30
	sequence 'open_rear_doors':
		Enable animation_group 'doors':
			from 25/30
			speed -1
			to 0/30
