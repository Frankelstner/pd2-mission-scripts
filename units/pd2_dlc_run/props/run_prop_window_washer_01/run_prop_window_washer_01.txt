unit:
	sequence 'default':
		Hide graphic_group 'grp_short'.
		Show graphic_group 'grp_middle'.
		Hide graphic_group 'grp_long'.
		Disable body 'short':
			visibility False
		Enable body 'middle':
			visibility True
		Disable body 'long':
			visibility False
		Disable object 's_s':
			shadow_caster False
		Enable object 's_s1':
			shadow_caster True
		Disable object 's_s2':
			shadow_caster False
		reset_editable_state True
	sequence 'short':
		Show graphic_group 'grp_short'.
		Hide graphic_group 'grp_middle'.
		Hide graphic_group 'grp_long'.
		Enable body 'short':
			visibility True
		Disable body 'middle':
			visibility False
		Disable body 'long':
			visibility False
		Enable object 's_s':
			shadow_caster False
		Disable object 's_s1':
			shadow_caster False
		Disable object 's_s2':
			shadow_caster True
	sequence 'middle':
		Hide graphic_group 'grp_short'.
		Show graphic_group 'grp_middle'.
		Hide graphic_group 'grp_long'.
		Disable body 'short':
			visibility False
		Enable body 'middle':
			visibility True
		Disable body 'long':
			visibility False
		Disable object 's_s':
			shadow_caster False
		Enable object 's_s1':
			shadow_caster True
		Disable object 's_s2':
			shadow_caster False
	sequence 'long':
		Hide graphic_group 'grp_short'.
		Hide graphic_group 'grp_middle'.
		Show graphic_group 'grp_long'.
		Disable body 'short':
			visibility False
		Disable body 'middle':
			visibility False
		Enable body 'long':
			visibility True
		Disable object 's_s':
			shadow_caster True
		Disable object 's_s1':
			shadow_caster False
		Enable object 's_s2':
			shadow_caster False
