unit:
	sequence 'anim_turtle_idle':
		EXECUTE ON STARTUP
		animation_redirect 'anim_turtle_idle'.
		Play audio 'turtle_upright_idle' at 'rp_run_props_trophies_turtle'.
		startup True
	sequence 'hide':
		Disable object 'g_turtle'.
		Play audio 'turtle_upright_idle_stop' at 'rp_run_props_trophies_turtle'.
		Disable interaction.
	sequence 'show':
		Enable object 'g_turtle'.
		Play audio 'turtle_upright_idle' at 'rp_run_props_trophies_turtle'.
		Enable interaction.
	sequence 'interact':
		Disable object 'g_turtle'.
		Play audio 'turtle_upright_idle_stop' at 'rp_run_props_trophies_turtle'.
	sequence 'load'.
