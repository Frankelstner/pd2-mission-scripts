unit:
	sequence 'anim_clouds':
		Enable animation_group 'ag_clouds':
			loop True
	sequence 'anim_clouds_stop':
		Enable animation_group 'ag_clouds':
			loop True
			speed 0
		Enable animation_group 'ag_clouds_stop':
			from 0/30
			speed 1
			to 360/30
	sequence 'show':
		Enable object 'g_g1'.
		Enable object 'g_g2'.
	sequence 'hide':
		Disable object 'g_g1'.
		Disable object 'g_g2'.
