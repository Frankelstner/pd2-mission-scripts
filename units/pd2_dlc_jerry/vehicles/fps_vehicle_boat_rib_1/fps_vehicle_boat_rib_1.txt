unit:
	sequence 'local_driving_enter'.
	sequence 'local_driving_exit'.
	sequence 'not_driving':
		Enable interaction.
		Enable body 'body_static_vehicle'.
		Disable body 'body_chassis'.
		Disable animation_group 'anim_propeller'.
		Disable effect_spawner 'water_engine'.
		slot:
			slot 39
	sequence 'driving':
		Enable interaction.
		Disable body 'body_static_vehicle'.
		Enable body 'body_chassis'.
		Enable animation_group 'anim_propeller'.
		Enable effect_spawner 'water_engine'.
		slot:
			slot 39
	sequence 'state_interaction_enabled':
		Enable interaction.
	sequence 'state_interaction_disabled':
		Disable interaction.
	sequence 'state_vis_icon_entry_enabled':
		Show graphic_group 'grp_icon_entry'.
	sequence 'state_vis_icon_entry_disabled':
		Hide graphic_group 'grp_icon_entry'.
	sequence 'state_vis_icon_loot_enabled':
		Show graphic_group 'grp_icon_loot'.
	sequence 'state_vis_icon_loot_disabled':
		Hide graphic_group 'grp_icon_loot'.
	sequence 'state_vis_icon_repair_enabled':
		Show graphic_group 'grp_icon_repair'.
	sequence 'state_vis_icon_repair_disabled':
		Hide graphic_group 'grp_icon_repair'.
	sequence 'int_seq_repaired':
		Disable effect_spawner 'es_smoke_full'.
		Disable effect_spawner 'es_smoke_med'.
	sequence 'int_seq_med_damaged':
		Enable effect_spawner 'es_smoke_med'.
		Disable effect_spawner 'es_smoke_full'.
	sequence 'int_seq_full_damaged':
		Disable effect_spawner 'es_smoke_med'.
		Enable effect_spawner 'es_smoke_full'.
