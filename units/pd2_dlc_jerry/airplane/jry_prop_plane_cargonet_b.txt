unit:
	sequence 'net_down':
		Enable animation_group 'anim_net':
			from 0/30
			to 29/30
		Disable decal_mesh 'dm_cloth'.
		Disable body 'body_static'. (DELAY 29/30)
		Play audio 'cargo_net_down' at 'snd_locator'.
	sequence 'net_up':
		Enable animation_group 'anim_net':
			from 29/30
			speed -1
			to 0/30
		Enable decal_mesh 'dm_cloth'.
		Enable body 'body_static'.
		Play audio 'cargo_net_down' at 'snd_locator'.
