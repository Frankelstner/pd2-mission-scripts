unit:
	sequence 'state_interaction_enable':
		Disable object 'g_contour'.
		Enable object 'g_intpanel_lod0'.
		Enable object 'g_intpanel_lod1'.
		Enable object 'g_intpanel_lod2'.
		Enable interaction.
	sequence 'state_interaction_disable':
		Disable interaction.
	sequence 'state_interaction_enable_contour':
		Enable object 'g_contour'.
		Disable object 'g_intpanel_lod0'.
		Disable object 'g_intpanel_lod1'.
		Disable object 'g_intpanel_lod2'.
		Enable interaction.
	sequence 'interact'.
