unit:
	sequence 'open_door':
		Enable animation_group 'anim_door':
			from 0/30
			to 30/30
	sequence 'open_door_kick':
		Enable animation_group 'anim_door':
			from 31/30
			to 44/30
		Disable body 'body_collision_01'. (DELAY 13/30)
		Enable body 'body_collision_02'. (DELAY 13/30)
