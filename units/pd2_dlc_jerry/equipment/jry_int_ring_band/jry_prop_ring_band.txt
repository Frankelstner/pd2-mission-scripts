unit:
	sequence 'state_interaction_enabled':
		Enable interaction.
	sequence 'state_interaction_disabled':
		Disable interaction.
	sequence 'state_vis_hide':
		Disable interaction.
		Hide graphic_group 'grp_ring'.
	sequence 'state_vis_show':
		Show graphic_group 'grp_ring'.
	sequence 'interact':
		Run sequence 'state_vis_hide'.
