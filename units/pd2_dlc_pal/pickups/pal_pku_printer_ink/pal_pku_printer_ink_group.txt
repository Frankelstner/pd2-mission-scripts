unit:
	sequence 'state_vis_hide':
		Hide graphic_group 'graphics_group'.
		Disable interaction.
	sequence 'state_vis_show':
		Show graphic_group 'graphics_group'.
	sequence 'state_interaction_disabled':
		Disable interaction.
	sequence 'state_interaction_enabled':
		Enable interaction.
	sequence 'interact':
		Run sequence 'state_interaction_disabled'.
		Run sequence 'state_vis_hide'.
	sequence 'load':
		Play audio 'plastic_jar_pick_up_put_down' at 'rp_pal_pku_printer_ink_group'.
