unit:
	sequence 'state_interaction_enabled':
		Enable interaction.
		Enable object 'g_stack_huge_outline'.
	sequence 'state_interaction_disabled':
		Disable interaction.
		Disable object 'g_stack_huge_outline'.
	sequence 'state_vis_hide':
		Disable interaction.
		Disable object 'g_stack_huge'.
		Disable decal_mesh 'dm_stack_huge'.
		Disable body 'body0'.
	sequence 'state_vis_show':
		Enable object 'g_stack_huge'.
		Enable decal_mesh 'dm_stack_huge'.
		Enable body 'body0'.
	sequence 'interact':
		Run sequence 'state_vis_hide'.
		effect 'effects/payday2/particles/grab/grab_money':
			parent object( 'rp_gen_pku_money' )
			position v()
	sequence 'load'.
