unit:
	sequence 'state_interaction_enabled':
		Enable interaction.
	sequence 'state_interaction_disabled':
		Disable interaction.
	sequence 'state_vis_hide':
		Disable interaction.
		Hide graphic_group 'graphics_group'.
		Disable decal_mesh 'g_g'.
		Disable body 'body_static'.
	sequence 'state_vis_show':
		Show graphic_group 'graphics_group'.
		Enable decal_mesh 'g_g'.
		Enable body 'body_static'.
	sequence 'interact'.
	sequence 'load':
		Play audio 'pickup_paperroll' at 'rp_pal_pku_printer_paper'.
