unit:
	sequence 'state_hide':
		Disable body 'body_static'.
		Disable body 'body_collision_01'.
		Disable decal_mesh 'dm_glass_01'.
		Disable decal_mesh 'dm_glass_02'.
		Disable decal_mesh 'dm_metal_02'.
		Hide graphic_group 'g_g_grp'.
		Hide graphic_group 'counter_01_grp'.
		Hide graphic_group 'counter_02_grp'.
		Hide graphic_group 'counter_03_grp'.
		Hide graphic_group 'counter_04_grp'.
		Hide graphic_group 'counter_05_grp'.
		Hide graphic_group 'g_lid_open_grp'.
		Hide graphic_group 'g_lid_closed_grp'.
		Hide graphic_group 'outline_lever'.
		Hide graphic_group 'outline_keycard'.
		Disable object 'g_display'.
		Disable object 'g_keycard_01'.
		Disable object 'g_keycard_02'.
		Disable object 'g_keycard_03'.
		Call function: digital_gui.set_visible(False)
	sequence 'state_show':
		Enable body 'body_static'.
		Enable body 'body_collision_01'.
		Enable decal_mesh 'dm_glass_01'.
		Enable decal_mesh 'dm_glass_02'.
		Enable decal_mesh 'dm_metal_02'.
		Show graphic_group 'g_g_grp'.
		Show graphic_group 'counter_01_grp'.
		Show graphic_group 'counter_02_grp'.
		Show graphic_group 'counter_03_grp'.
		Show graphic_group 'counter_04_grp'.
		Show graphic_group 'counter_05_grp'.
		Show graphic_group 'g_lid_open_grp'.
		Show graphic_group 'g_lid_closed_grp'.
		Show graphic_group 'outline_lever'.
		Show graphic_group 'outline_keycard'.
		Enable object 'g_display'.
		Disable object 'g_keycard_01'.
		Disable object 'g_keycard_02'.
		Disable object 'g_keycard_03'.
		Call function: digital_gui.set_visible(True)
	sequence 'state_show_outline_lever':
		Show graphic_group 'outline_lever'.
		Call function: interaction.set_tweak_data('no_interact')
		Enable interaction.
	sequence 'state_hide_outline_lever':
		Hide graphic_group 'outline_lever'.
	sequence 'anim_open_lid_keycard_01':
		Enable animation_group 'anim_lid_keycard_03':
			from 0/30
			to 6/30
		Enable animation_group 'anim_keycard_01':
			from 0/30
			to 8/30
		Enable object 'g_keycard_01'.
		Play audio 'hlp_controlswitch_keycards_open' at 'snd_keycard_03'.
	sequence 'state_hide_keycard_01':
		Enable animation_group 'anim_lid_keycard_03':
			from 0/30
			to 0/30
		Enable animation_group 'anim_keycard_01':
			from 0/30
			to 0/30
		Disable object 'g_keycard_01'.
	sequence 'anim_open_lid_keycard_02':
		Enable animation_group 'anim_lid_keycard_02':
			from 0/30
			to 6/30
		Enable animation_group 'anim_keycard_02':
			from 0/30
			to 8/30
		Enable object 'g_keycard_02'.
		Play audio 'hlp_controlswitch_keycards_open' at 'snd_keycard_02'.
	sequence 'state_hide_keycard_02':
		Enable animation_group 'anim_lid_keycard_02':
			from 0/30
			to 0/30
		Enable animation_group 'anim_keycard_02':
			from 0/30
			to 0/30
		Disable object 'g_keycard_02'.
	sequence 'anim_open_lid_keycard_03':
		Enable animation_group 'anim_lid_keycard_01':
			from 0/30
			to 6/30
		Enable animation_group 'anim_keycard_03':
			from 0/30
			to 8/30
		Enable object 'g_keycard_03'.
		Play audio 'hlp_controlswitch_keycards_open' at 'snd_keycard_01'.
	sequence 'state_hide_keycard_03':
		Enable animation_group 'anim_lid_keycard_01':
			from 0/30
			to 0/30
		Enable animation_group 'anim_keycard_03':
			from 0/30
			to 0/30
		Disable object 'g_keycard_03'.
	sequence 'anim_pull_lever':
		Enable animation_group 'anim_lid_switch':
			from 0/30
			to 45/30
		Play audio 'hlp_controlswitch_lever' at 'snd_lever'.
	sequence 'anim_open_lever_lid':
		Enable animation_group 'anim_lever_lid':
			from 0/30
			to 12/30
		Play audio 'hlp_controlswitch_lever_lid' at 'snd_lever_lid'.
	sequence 'anim_close_lever_lid':
		Enable animation_group 'anim_lever_lid':
			from 12/30
			speed -1
			to 0/30
		Play audio 'hlp_controlswitch_lever_lid' at 'snd_lever_lid'.
	sequence 'anim_open_lid':
		Show graphic_group 'g_lid_open_grp'. (DELAY 50/30)
		Hide graphic_group 'g_lid_closed_grp'. (DELAY 50/30)
		Enable animation_group 'anim_lid':
			from 0/30
			to 50/30
		Play audio 'hlp_controlswitch_open' at 'snd_lid'.
	sequence 'anim_close_lid':
		Show graphic_group 'g_lid_open_grp'. (DELAY 50/30)
		Hide graphic_group 'g_lid_closed_grp'. (DELAY 50/30)
		Enable animation_group 'anim_lid':
			from 50/30
			speed -1
			to 0/30
		Play audio 'hlp_controlswitch_open' at 'snd_lid'.
	sequence 'state_show_number_01':
		Show graphic_group 'counter_01_grp'.
		Hide graphic_group 'counter_02_grp'.
		Hide graphic_group 'counter_03_grp'.
		Hide graphic_group 'counter_04_grp'.
		Hide graphic_group 'counter_05_grp'.
	sequence 'state_show_number_02':
		Hide graphic_group 'counter_01_grp'.
		Show graphic_group 'counter_02_grp'.
		Hide graphic_group 'counter_03_grp'.
		Hide graphic_group 'counter_04_grp'.
		Hide graphic_group 'counter_05_grp'.
	sequence 'state_show_number_03':
		Hide graphic_group 'counter_01_grp'.
		Hide graphic_group 'counter_02_grp'.
		Show graphic_group 'counter_03_grp'.
		Hide graphic_group 'counter_04_grp'.
		Hide graphic_group 'counter_05_grp'.
	sequence 'state_show_number_04':
		Hide graphic_group 'counter_01_grp'.
		Hide graphic_group 'counter_02_grp'.
		Hide graphic_group 'counter_03_grp'.
		Show graphic_group 'counter_04_grp'.
		Hide graphic_group 'counter_05_grp'.
	sequence 'state_show_number_05':
		Hide graphic_group 'counter_01_grp'.
		Hide graphic_group 'counter_02_grp'.
		Hide graphic_group 'counter_03_grp'.
		Hide graphic_group 'counter_04_grp'.
		Show graphic_group 'counter_05_grp'.
	sequence 'state_reset_numbers':
		Hide graphic_group 'counter_01_grp'.
		Hide graphic_group 'counter_02_grp'.
		Hide graphic_group 'counter_03_grp'.
		Hide graphic_group 'counter_04_grp'.
		Hide graphic_group 'counter_05_grp'.
	sequence 'interact':
		Run sequence 'enable_interaction'.
	sequence 'enable_interaction':
		Call function: interaction.set_tweak_data('hold_insert_keycard_hlp')
		Enable interaction.
		Show graphic_group 'outline_keycard'.
	sequence 'disable_interaction':
		Call function: interaction.set_tweak_data('hold_insert_keycard_hlp')
		Disable interaction.
		Hide graphic_group 'outline_keycard'.
	sequence 'red_on_black':
		Call function: digital_gui.set_color_type('red')
		Call function: digital_gui.set_bg_color_type()
	sequence 'green_on_black':
		Call function: digital_gui.set_color_type('green')
		Call function: digital_gui.set_bg_color_type()
	sequence 'blue_on_black':
		Call function: digital_gui.set_color_type('blue')
		Call function: digital_gui.set_bg_color_type()
	sequence 'yellow_on_black':
		Call function: digital_gui.set_color_type('yellow')
		Call function: digital_gui.set_bg_color_type()
	sequence 'orange_on_black':
		Call function: digital_gui.set_color_type('orange')
		Call function: digital_gui.set_bg_color_type()
	sequence 'light_red_on_black':
		Call function: digital_gui.set_color_type('light_red')
		Call function: digital_gui.set_bg_color_type()
	sequence 'light_green_on_black':
		Call function: digital_gui.set_color_type('light_green')
		Call function: digital_gui.set_bg_color_type()
	sequence 'light_blue_on_black':
		Call function: digital_gui.set_color_type('light_blue')
		Call function: digital_gui.set_bg_color_type()
	sequence 'light_yellow_on_black':
		Call function: digital_gui.set_color_type('light_yellow')
		Call function: digital_gui.set_bg_color_type()
	sequence 'light_orange_on_black':
		Call function: digital_gui.set_color_type('light_orange')
		Call function: digital_gui.set_bg_color_type()
	sequence 'black_on_light_red':
		Call function: digital_gui.set_color_type('black')
		Call function: digital_gui.set_bg_color_type('light_red')
	sequence 'black_on_light_green':
		Call function: digital_gui.set_color_type('black')
		Call function: digital_gui.set_bg_color_type('light_green')
	sequence 'black_on_light_blue':
		Call function: digital_gui.set_color_type('black')
		Call function: digital_gui.set_bg_color_type('light_blue')
	sequence 'black_on_light_yellow':
		Call function: digital_gui.set_color_type('black')
		Call function: digital_gui.set_bg_color_type('light_yellow')
	sequence 'black_on_light_orange':
		Call function: digital_gui.set_color_type('black')
		Call function: digital_gui.set_bg_color_type('light_orange')
	sequence 'number_increase':
		Call function: digital_gui.number_increase()
	sequence 'number_decrease':
		Call function: digital_gui.number_decrease()
	sequence 'timer_start_count_up':
		Call function: digital_gui.timer_start_count_up()
	sequence 'timer_start_count_down':
		Call function: digital_gui.timer_start_count_down()
	sequence 'timer_pause':
		Call function: digital_gui.timer_pause()
	sequence 'timer_resume':
		Call function: digital_gui.timer_resume()
	sequence 'timer_reach_zero'.
