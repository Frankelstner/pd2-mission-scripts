unit:
	sequence 'make_dynamic':
		Disable body 'body_static'.
		Disable object 'g_lamp'.
		Disable decal_mesh 'dm_glass'.
		Enable body 'body_dmg_static'.
		Enable body 'dynamic_lampshard_01'.
		Enable body 'dynamic_lampshard_02'.
		Enable body 'dynamic_lampshard_03'.
		Show graphic_group 'dmg_lamp_grp'.
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('e_effect') ),12,8).
	sequence 'light_on':
		Enable light 'li_wall_lamp'.
		Enable effect_spawner 'indoor_lamp_small'.
		material_config 'units/pd2_mcmansion/props/mcm_prop_bathroom_lamp/mcm_prop_bathroom_lamp_on'.
	sequence 'light_off':
		Disable light 'li_wall_lamp'.
		Disable effect_spawner 'indoor_lamp_small'.
		material_config 'units/pd2_mcmansion/props/mcm_prop_bathroom_lamp/mcm_prop_bathroom_lamp_off'.
	sequence 'break_int':
		Run sequence 'light_off'.
		Play audio 'light_bulb_smash' at 'e_effect'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_effect' )
			position v()
		Run sequence 'make_dynamic'.
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_int'.
