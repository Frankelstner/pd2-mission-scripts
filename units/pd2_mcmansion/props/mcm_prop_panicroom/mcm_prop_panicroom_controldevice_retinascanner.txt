unit:
	sequence 'light_default':
		Disable object 'g_greenscreen'.
		Disable object 'g_redscreen'.
	sequence 'light_green':
		Enable object 'g_greenscreen'.
		Disable object 'g_redscreen'.
	sequence 'light_red':
		Disable object 'g_greenscreen'.
		Enable object 'g_redscreen'.
	sequence 'hide':
		Disable object 'g_lights'.
		Disable object 'g_greenscreen'.
		Hide graphic_group 'grp_lod'.
		Disable body 'body_static'.
		Disable decal_mesh 'dm_steel'.
	sequence 'show':
		Enable object 'g_lights'.
		Enable object 'g_greenscreen'.
		Show graphic_group 'grp_lod'.
		Enable body 'body_static'.
		Enable decal_mesh 'dm_steel'.
