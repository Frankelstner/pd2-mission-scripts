unit:
	sequence 'light_on':
		Enable light 'li_ceiling_lamp'.
		Enable effect_spawner 'indoor_lamp_small_01'.
		Enable effect_spawner 'indoor_lamp_small_02'.
		Enable effect_spawner 'indoor_lamp_small_03'.
		Enable effect_spawner 'indoor_lamp_small_04'.
		material_config 'units/pd2_mcmansion/props/mcm_prop_dining_lamp_ceiling/mcm_prop_dining_lamp_ceiling_on'.
	sequence 'light_off':
		Disable light 'li_ceiling_lamp'.
		Disable effect_spawner 'indoor_lamp_small_01'.
		Disable effect_spawner 'indoor_lamp_small_02'.
		Disable effect_spawner 'indoor_lamp_small_03'.
		Disable effect_spawner 'indoor_lamp_small_04'.
		material_config 'units/pd2_mcmansion/props/mcm_prop_dining_lamp_ceiling/mcm_prop_dining_lamp_ceiling_off'.
	sequence 'break_int'.
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_int'.
