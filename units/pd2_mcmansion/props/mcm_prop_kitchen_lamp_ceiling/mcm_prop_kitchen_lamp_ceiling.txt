unit:
	sequence 'break'.
	sequence 'light_off':
		Disable light 'lo_omni'.
		Disable effect_spawner 'indoor_lamp'.
	sequence 'light_on':
		Enable light 'lo_omni'.
		Enable effect_spawner 'indoor_lamp'.
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break'.
