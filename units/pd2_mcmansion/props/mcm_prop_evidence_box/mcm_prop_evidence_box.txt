unit:
	sequence 'show':
		Enable object 'g_g'.
		Enable object 's_s'.
		Enable body 'body_static'.
	sequence 'hide':
		Disable object 'g_g'.
		Disable object 's_s'.
		Disable body 'body_static'.
		Run sequence 'interact_disable'.
	sequence 'interact_enable':
		Enable interaction.
	sequence 'interact_disable':
		Disable interaction.
	sequence 'interact':
		Run sequence 'hide'.
		Disable interaction.
	sequence 'load'.
