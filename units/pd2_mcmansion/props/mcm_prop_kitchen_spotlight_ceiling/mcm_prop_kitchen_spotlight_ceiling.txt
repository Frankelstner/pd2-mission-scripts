unit:
	sequence 'light_on':
		Enable light 'li_small'.
		Enable object 'g_il'.
		Enable effect_spawner 'small_lamp_glow'.
	sequence 'light_off':
		Disable light 'li_small'.
		Disable object 'g_il'.
		Disable effect_spawner 'small_lamp_glow'.
	sequence 'break_int':
		TRIGGER TIMES 1
		Run sequence 'light_off'.
		Play audio 'light_bulb_smash' at 'e_effect'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_effect' )
			position v()
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_int'.
