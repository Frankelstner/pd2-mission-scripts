unit:
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 10 melee damage, execute:
			Run sequence 'int_seq_bullet_hit'.
	sequence 'light_on':
		Enable light 'li_hanging_lamp'.
		Enable effect_spawner 'indoor_lamp_small'.
		material_config 'units/pd2_mcmansion/props/mcm_prop_hallway_wall_lamp/mcm_prop_hallway_wall_lamp_on'.
	sequence 'light_off':
		Disable light 'li_hanging_lamp'.
		Disable effect_spawner 'indoor_lamp_small'.
		material_config 'units/pd2_mcmansion/props/mcm_prop_hallway_wall_lamp/mcm_prop_hallway_wall_lamp_off'.
	sequence 'int_seq_bullet_hit':
		TRIGGER TIMES 1
