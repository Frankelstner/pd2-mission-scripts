unit:
	sequence 'enable_special':
		random = 0
	random = 1
	extra = 100
	sequence 'spawn_loot_crap_a':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_crap_a':
			position object_pos('sp_spawn1')
			rotation object_rot('sp_spawn1')
	sequence 'spawn_loot_crap_b':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_crap_b':
			position object_pos('sp_spawn1')
			rotation object_rot('sp_spawn1')
	sequence 'spawn_loot_crap_c':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_crap_c':
			position object_pos('sp_spawn1')
			rotation object_rot('sp_spawn1')
	sequence 'spawn_loot_crap_d':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_crap_d':
			position object_pos('sp_spawn1')
			rotation object_rot('sp_spawn1')
	sequence 'spawn_loot_crap_e':
		If extra > 3: 
			spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_crap_e':
				position object_pos('sp_spawn1')
				rotation object_rot('sp_spawn1')
		If extra <= 3: Run sequence 'spawn_german_folder'.
	sequence 'spawn_loot_value_a':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_value_a':
			position object_pos('sp_spawn1')
			rotation object_rot('sp_spawn1')
	sequence 'spawn_loot_value_b':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_value_b':
			position object_pos('sp_spawn1')
			rotation object_rot('sp_spawn1')
	sequence 'spawn_loot_value_c':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_value_c':
			position object_pos('sp_spawn1')
			rotation object_rot('sp_spawn1')
	sequence 'spawn_loot_value_d':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_value_d':
			position object_pos('sp_spawn1')
			rotation object_rot('sp_spawn1')
	sequence 'spawn_loot_value_e':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_value_e':
			position object_pos('sp_spawn1')
			rotation object_rot('sp_spawn1')
	sequence 'spawn_loot_empty'.
	sequence 'spawn_special_money':
		spawn_unit 'units/payday2/props/bnk_prop_vault_loot/bnk_prop_vault_loot_special_money':
			position object_pos('sp_spawn1')
			rotation object_rot('sp_spawn1')
	sequence 'spawn_german_folder':
		spawn_unit 'units/pd2_dlc_jfr/pickups/pku_german_folder/pku_german_folder':
			position object_pos('sp_spawn1')
			rotation object_rot('sp_spawn1')
	sequence 'int_seq_open':
		TRIGGER TIMES 1
		extra = 100*rand()
		Hide graphic_group 'drillicongroup'.
		Disable interaction.
		Enable animation_group 'anim'.
		Disable body 'body_door'.
		Disable decal_mesh 'dm_metal_door'.
		Play audio 'deposit_slide_open' at 'jt_5'.
		If random == 1: Run sequence 'spawn_loot_'..pick('crap_a','crap_b','crap_c','crap_d','crap_e','value_a','value_b','value_c','value_d','value_e','value_a','value_b','value_c','value_d','value_e','value_a','value_b','value_c','value_d','value_e','value_a','value_b','value_c','value_d','value_e','empty').
		If random == 0: Run sequence 'spawn_special_money'.
	sequence 'interact':
		Run sequence 'int_seq_open'.
	body 'body_door'
		Upon receiving 37.5 saw damage, execute:
			Run sequence 'int_seq_open'.
