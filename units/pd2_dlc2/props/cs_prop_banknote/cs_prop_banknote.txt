unit:
	hidden = 0
	sequence 'enable_interaction':
		If hidden == 0: 
			Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'state_vis_hidden':
		Disable interaction.
		Disable body 'static_body'.
		Disable object 'g_g'.
		hidden = 1
	sequence 'state_vis_show':
		Enable body 'static_body'.
		Enable object 'g_g'.
		hidden = 0
	sequence 'interact':
		Run sequence 'state_vis_hidden'.
