unit:
	body 'static_body'
		Upon receiving 4 bullet hits or 10 melee damage, execute:
			Run sequence 'int_seq_bullet_hit'.
		Upon receiving 10 explosion damage, execute:
			Run sequence 'int_seq_explosion'.
	body 'leafs_body'
		Upon receiving 10 melee damage, execute:
			Run sequence 'int_seq_bullet_hit'.
	sequence 'int_seq_bullet_hit':
		Disable body 'static_body'.
		Disable body 'leafs_body'.
		Disable decal_mesh 'dm_marble'.
		Disable object 'g_g_lod_0'.
		Disable object 'g_g_lod_1'.
		Play audio 'pot_large_shatter' at 'e_pos'.
		spawn_unit 'units/pd2_dlc2/props/bnk_prop_lobby_plant_dracaenafragrans/spawn_debris_plant_dracaenafragrans_c':
			position object_pos('rp_bnk_prop_lobby_plant_dracaenafragrans_c')
			rotation object_rot('rp_bnk_prop_lobby_plant_dracaenafragrans_c')
		spawn_unit 'units/pd2_dlc2/props/bnk_prop_lobby_plant_dracaenafragrans/spawn_debris_pot_dracaenafragrans_c':
			position object_pos('rp_bnk_prop_lobby_plant_dracaenafragrans_c')
			rotation object_rot('rp_bnk_prop_lobby_plant_dracaenafragrans_c')
	sequence 'int_seq_explosion':
		Disable body 'static_body'.
		Disable body 'leafs_body'.
		Disable decal_mesh 'dm_marble'.
		Disable object 'g_g_lod_0'.
		Disable object 'g_g_lod_1'.
		Play audio 'pot_large_shatter' at 'e_pos'.
		effect 'effects/payday2/particles/destruction/dest_plant_matter':
			parent object( 'e_pos' )
			position v()
		spawn_unit 'units/pd2_dlc2/props/bnk_prop_lobby_plant_dracaenafragrans/spawn_debris_plant_dracaenafragrans_c':
			position object_pos('rp_bnk_prop_lobby_plant_dracaenafragrans_c')
			rotation object_rot('rp_bnk_prop_lobby_plant_dracaenafragrans_c')
		spawn_unit 'units/pd2_dlc2/props/bnk_prop_lobby_plant_dracaenafragrans/spawn_debris_pot_dracaenafragrans_c':
			position object_pos('rp_bnk_prop_lobby_plant_dracaenafragrans_c')
			rotation object_rot('rp_bnk_prop_lobby_plant_dracaenafragrans_c')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('e_pos') ),100,40).
