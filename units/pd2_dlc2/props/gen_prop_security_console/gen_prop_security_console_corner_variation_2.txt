unit:
	broken_01 = 0
	broken_02 = 0
	broken_03 = 0
	broken_04 = 0
	sequence 'state_power_off':
		If broken_01 == 0: Stop audio 'tv_screen_small_hum' at 'snd_01'.
		If broken_02 == 0: Stop audio 'tv_screen_small_hum' at 'snd_02'.
		If broken_03 == 0: Stop audio 'tv_screen_small_hum' at 'snd_03'.
		Stop audio 'tv_screen_small_hum' at 'snd_04'.
		If broken_01 == 0: Disable object 'g_screen_off_01'.
		If broken_02 == 0: Disable object 'g_screen_off_02'.
		If broken_03 == 0: Disable object 'g_screen_off_03'.
		If broken_01 == 0: Disable object 'g_screen_01'.
		If broken_02 == 0: Disable object 'g_screen_02'.
		If broken_03 == 0: Disable object 'g_screen_03'.
	sequence 'state_power_on':
		If broken_01 == 0: Play audio 'tv_screen_small_hum' at 'snd_01'.
		If broken_02 == 0: Play audio 'tv_screen_small_hum' at 'snd_02'.
		If broken_03 == 0: Play audio 'tv_screen_small_hum' at 'snd_03'.
		Play audio 'tv_screen_small_hum' at 'snd_04'.
		If broken_01 == 0: Enable object 'g_screen_off_01'.
		If broken_02 == 0: Enable object 'g_screen_off_02'.
		If broken_03 == 0: Enable object 'g_screen_off_03'.
		If broken_01 == 0: Enable object 'g_screen_01'.
		If broken_02 == 0: Enable object 'g_screen_02'.
		If broken_03 == 0: Enable object 'g_screen_03'.
	sequence 'int_seq_hum_sound':
		EXECUTE ON STARTUP
		Play audio 'tv_screen_small_hum' at 'snd_01'.
		Play audio 'tv_screen_small_hum' at 'snd_02'.
		Play audio 'tv_screen_small_hum' at 'snd_03'.
		Play audio 'tv_screen_small_hum' at 'snd_04'.
		startup True
	sequence 'int_seq_crack_screen_01':
		Disable object 'g_screen_01'.
		Enable object 'g_screen_cracked_01'.
		Disable object 'g_screen_broken_01'.
		Disable body 'body_monitor_static_01'.
		Enable body 'body_monitor_dynamic_01'.
		Enable effect_spawner 'glass_01'.
		Stop audio 'tv_screen_small_hum' at 'snd_01'.
		broken_01 = 1
	sequence 'int_seq_break_screen_01':
		TRIGGER TIMES 1
		Disable object 'g_screen_01'.
		Disable object 'g_screen_cracked_01'.
		Enable object 'g_screen_broken_01'.
		Enable effect_spawner 'spark_01'.
		broken_01 = 1
	sequence 'int_seq_crack_screen_02':
		Disable object 'g_screen_02'.
		Enable object 'g_screen_cracked_02'.
		Disable object 'g_screen_broken_02'.
		Disable body 'body_monitor_static_02'.
		Enable body 'body_monitor_dynamic_02'.
		Enable effect_spawner 'glass_02'.
		Stop audio 'tv_screen_small_hum' at 'snd_02'.
		broken_02 = 1
	sequence 'int_seq_break_screen_02':
		TRIGGER TIMES 1
		Disable object 'g_screen_02'.
		Disable object 'g_screen_cracked_02'.
		Enable object 'g_screen_broken_02'.
		Enable effect_spawner 'spark_02'.
		broken_02 = 1
	sequence 'int_seq_crack_screen_03':
		Disable object 'g_screen_03'.
		Enable object 'g_screen_cracked_03'.
		Disable object 'g_screen_broken_03'.
		Disable body 'body_monitor_static_03'.
		Enable body 'body_monitor_dynamic_03'.
		Enable effect_spawner 'glass_03'.
		Stop audio 'tv_screen_small_hum' at 'snd_03'.
		broken_03 = 1
	sequence 'int_seq_break_screen_03':
		TRIGGER TIMES 1
		Disable object 'g_screen_03'.
		Disable object 'g_screen_cracked_03'.
		Enable object 'g_screen_broken_03'.
		Enable effect_spawner 'spark_03'.
		broken_03 = 1
	sequence 'int_seq_break_keyboard_01':
		TRIGGER TIMES 1
		Disable body 'body_keyboard_static_01'.
		Enable body 'body_keyboard_dynamic_01'.
	sequence 'int_seq_break_chair':
		TRIGGER TIMES 1
		Disable body 'body_chair_static'.
		Enable body 'body_chair_dynamic'.
	sequence 'int_seq_break_laptop':
		TRIGGER TIMES 1
		Disable body 'body_monitor_static_04'.
		Enable body 'body_monitor_dynamic_04'.
	body 'body_chair_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_chair'.
	body 'body_monitor_static_01'
		Upon receiving 1 bullet hit or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'int_seq_crack_screen_01'.
		Upon receiving 2 bullet hits or 10 explosion damage or 40 melee damage, execute:
			Run sequence 'int_seq_break_screen_01'.
	body 'body_monitor_dynamic_01'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_screen_01'.
	body 'body_monitor_static_02'
		Upon receiving 1 bullet hit or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'int_seq_crack_screen_02'.
		Upon receiving 2 bullet hits or 10 explosion damage or 40 melee damage, execute:
			Run sequence 'int_seq_break_screen_02'.
	body 'body_monitor_dynamic_02'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_screen_02'.
	body 'body_monitor_static_03'
		Upon receiving 1 bullet hit or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'int_seq_crack_screen_03'.
		Upon receiving 2 bullet hits or 10 explosion damage or 40 melee damage, execute:
			Run sequence 'int_seq_break_screen_03'.
	body 'body_monitor_dynamic_03'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_screen_03'.
	body 'body_monitor_static_04'
		Upon receiving 1 bullet hit or 10 explosion damage or 30 melee damage, execute:
			Run sequence 'int_seq_break_laptop'.
	body 'body_keyboard_static_01'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break_keyboard_01'.
