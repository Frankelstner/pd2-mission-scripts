unit:
	sequence 'int_seq_hum':
		EXECUTE ON STARTUP
		Play audio 'computer_screen_large_hum' at 'snd_01'.
		startup True
	sequence 'state_off':
		Disable object 'g_screens'.
	sequence 'state_on':
		Run sequence 'change_01'.
		Enable object 'g_screens'.
	sequence 'change_01':
		material_config 'units/pd2_dlc2/props/gen_prop_security_console/gen_prop_security_monitor_hanging_01'.
		Run sequence 'change_02'. (DELAY 300/30)
	sequence 'change_02':
		material_config 'units/pd2_dlc2/props/gen_prop_security_console/gen_prop_security_monitor_hanging_02'.
		Run sequence 'change_01'. (DELAY 300/30)
