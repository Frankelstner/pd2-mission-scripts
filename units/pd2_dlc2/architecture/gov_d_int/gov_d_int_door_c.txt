unit:
	sequence 'state_interact_enable':
		Enable interaction.
		Show graphic_group 'icon'.
	sequence 'state_interact_disable':
		Disable interaction.
		Hide graphic_group 'icon'.
	sequence 'state_door_open':
		Disable interaction.
		Enable animation_group 'anim_grp0':
			from 40/30
			speed 0
			to 40/30
		Hide graphic_group 'icon'.
		Run sequence 'deactivate_door'.
	sequence 'state_door_close':
		Enable interaction.
		Enable animation_group 'anim_grp0':
			from 0/30
			speed 0
			to 0/30
		Show graphic_group 'icon'.
	sequence 'state_door_hide':
		Disable interaction.
		Disable body 'animated_body'.
		Disable object 'g_door_c'.
		Hide graphic_group 'icon'.
		Run sequence 'deactivate_door'.
	sequence 'state_door_show':
		Enable interaction.
		Enable body 'animated_body'.
		Enable object 'g_door_c'.
		Show graphic_group 'icon'.
	sequence 'anim_open_door':
		Disable interaction.
		Enable animation_group 'anim_grp0':
			from 0/30
			to 35/30
		Play audio 'generic_door_wood_open' at 'a_door_c'.
		Hide graphic_group 'icon'.
		Run sequence 'deactivate_door'.
	sequence 'anim_close_door':
		Enable interaction. (DELAY 35/30)
		Enable animation_group 'anim_grp0':
			from 35/30
			speed -1
			to 0/30
		Show graphic_group 'icon'. (DELAY 35/30)
	sequence 'interact':
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
	sequence 'activate_door':
		Call function: base.activate()
		Show graphic_group 'icon'.
		Enable interaction.
	sequence 'deactivate_door':
		Call function: base.deactivate()
		Hide graphic_group 'icon'.
		Disable interaction.
	sequence 'explode_door':
		Disable interaction.
		Play audio 'c4_explode_metal' at 'a_push'.
		Disable body 'animated_body'.
		spawn_unit 'units/pd2_dlc2/architecture/gov_d_int/spawn_debris_gov_d_int_door_a':
			position object_pos('rp_gov_d_int_door_c')
			rotation object_rot('rp_gov_d_int_door_c')
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('a_push') ),200,400).
		Run sequence 'deactivate_door'.
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'a_c4_placement' )
			position v()
		Run sequence 'done_exploded'.
		Disable object 'g_door_c'.
		Disable interaction.
		Run sequence 'done_opened'.
	sequence 'power_off':
		Call function: base.set_powered(False)
	sequence 'power_on':
		Call function: base.set_powered(True)
	sequence 'turn_off':
		Call function: base.set_on(False)
	sequence 'turn_on':
		Call function: base.set_on(True)
	sequence 'door_opened'.
	sequence 'door_closed'.
	sequence 'c4_placed'.
	sequence 'c4_completed'.
	sequence 'all_c4_placed'.
	sequence 'open_door':
		Run sequence 'anim_open_door'.
		Run sequence 'done_opened'.
	sequence 'done_opened'.
	sequence 'done_exploded'.
