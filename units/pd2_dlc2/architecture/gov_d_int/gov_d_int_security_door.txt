unit:
	sequence 'anim_open_door':
		Disable body 'door_body'.
		Enable animation_group 'anim_grp0':
			from 0/30
			to 17/30
		Play audio 'shutter_door_up_02' at 'a_1'.
	sequence 'anim_open_door_last_segment':
		Enable animation_group 'anim_grp0':
			from 0/30
			to 8/30
	sequence 'anim_close_door_last_segment':
		Enable animation_group 'anim_grp0':
			from 8/30
			speed -1
			to 0/30
	sequence 'anim_close_door':
		Enable body 'door_body'.
		Enable animation_group 'anim_grp0':
			from 17/30
			speed -1
			to 0/30
		Play audio 'shutter_door_up_02' at 'a_1'.
	sequence 'state_open_door':
		Disable body 'door_body'.
		Enable animation_group 'anim_grp0':
			from 17/30
			to 17/30
