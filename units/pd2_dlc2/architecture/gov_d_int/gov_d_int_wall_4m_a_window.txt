unit:
	body 'glass_01_body'
		Upon receiving 4 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'seq_break_glass_01'.
	sequence 'seq_break_glass_01':
		TRIGGER TIMES 1
		Disable body 'glass_01_body'.
		Disable decal_mesh 'g_glass_01'.
		Disable object 'g_glass_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_01' )
			position v()
		Play audio 'window_medium_shatter' at 'e_glass_01'.
		Cause alert with 12 m radius.
	body 'glass_02_body'
		Upon receiving 4 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'seq_break_glass_02'.
	sequence 'seq_break_glass_02':
		TRIGGER TIMES 1
		Disable body 'glass_02_body'.
		Disable decal_mesh 'g_glass_02'.
		Disable object 'g_glass_02'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_02' )
			position v()
		Play audio 'window_medium_shatter' at 'e_glass_02'.
		Cause alert with 12 m radius.
