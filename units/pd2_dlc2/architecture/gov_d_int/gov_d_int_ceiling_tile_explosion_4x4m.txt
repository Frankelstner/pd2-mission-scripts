unit:
	sequence 'explode':
		Enable object 'g_exploded'.
		Disable decal_mesh 'dm_plaster'.
		Disable object 'g_g'.
		Disable body 'static_body_1'.
		spawn_unit 'units/pd2_dlc2/architecture/gov_d_int/gov_d_int_ceiling_tile_debris':
			position object_pos('e_explosion')
			rotation object_rot('e_explosion')
			run_sequence 'explode'
	sequence 'whole':
		Enable body 'static_body_1'.
		Disable object 'g_exploded'.
		Enable object 'g_g'.
