unit:
	sequence 'seq_explode':
		Disable body 'hole_body'.
		Enable object 'g_explosion'.
		Disable object 'g_g'.
		Disable body 'glass_04_body'.
		Disable decal_mesh 'g_glass_04'.
		Disable object 'g_glass_04'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_04' )
			position v()
		Play audio 'window_medium_shatter' at 'e_glass_04'.
		Disable body 'glass_03_body'.
		Disable decal_mesh 'g_glass_03'.
		Disable object 'g_glass_03'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_03' )
			position v()
		Play audio 'window_medium_shatter' at 'e_glass_03'.
		Disable body 'glass_02_body'.
		Disable decal_mesh 'g_glass_02'.
		Disable object 'g_glass_02'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_02' )
			position v()
		Play audio 'window_medium_shatter' at 'e_glass_02'.
		Disable body 'glass_01_body'.
		Disable decal_mesh 'g_glass_01'.
		Disable object 'g_glass_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_01' )
			position v()
		Play audio 'window_medium_shatter' at 'e_glass_01'.
		Cause alert with 12 m radius.
	body 'glass_01_body'
		Upon receiving 4 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'seq_break_glass_01'.
	sequence 'seq_break_glass_01':
		TRIGGER TIMES 1
		Disable body 'glass_01_body'.
		Disable decal_mesh 'g_glass_01'.
		Disable object 'g_glass_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_01' )
			position v()
		Play audio 'window_medium_shatter' at 'e_glass_01'.
		Cause alert with 12 m radius.
	body 'glass_02_body'
		Upon receiving 4 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'seq_break_glass_02'.
	sequence 'seq_break_glass_02':
		TRIGGER TIMES 1
		Disable body 'glass_02_body'.
		Disable decal_mesh 'g_glass_02'.
		Disable object 'g_glass_02'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_02' )
			position v()
		Play audio 'window_medium_shatter' at 'e_glass_02'.
		Cause alert with 12 m radius.
	body 'glass_03_body'
		Upon receiving 4 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'seq_break_glass_03'.
	sequence 'seq_break_glass_03':
		TRIGGER TIMES 1
		Disable body 'glass_03_body'.
		Disable decal_mesh 'g_glass_03'.
		Disable object 'g_glass_03'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_03' )
			position v()
		Play audio 'window_medium_shatter' at 'e_glass_03'.
		Cause alert with 12 m radius.
	body 'glass_04_body'
		Upon receiving 4 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'seq_break_glass_04'.
	sequence 'seq_break_glass_04':
		TRIGGER TIMES 1
		Disable body 'glass_04_body'.
		Disable decal_mesh 'g_glass_04'.
		Disable object 'g_glass_04'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_04' )
			position v()
		Play audio 'window_medium_shatter' at 'e_glass_04'.
		Cause alert with 12 m radius.
