unit:
	sequence 'remove_pole_01':
		TRIGGER TIMES 1
		Disable body 'pole_01'.
		Disable object 'g_pole_01'.
		Disable object 's_01'.
		Disable decal_mesh 'g_pole_01'.
		Enable object 'g_pole_01_dmg'.
		effect 'effects/payday2/particles/destruction/dest_statue_bust':
			parent object('e_effect_01')
			position v()
		spawn_unit 'units/pd2_dlc2/architecture/gov_c_int/gov_c_int_balustrade_debris':
			position object_pos('e_effect_01')
			rotation object_rot('e_effect_01')
			run_sequence 'explode'
	sequence 'remove_pole_02':
		TRIGGER TIMES 1
		Disable body 'pole_02'.
		Disable object 'g_pole_02'.
		Disable object 's_02'.
		Disable decal_mesh 'g_pole_02'.
		Enable object 'g_pole_02_dmg'.
		effect 'effects/payday2/particles/destruction/dest_statue_bust':
			parent object('e_effect_02')
			position v()
		spawn_unit 'units/pd2_dlc2/architecture/gov_c_int/gov_c_int_balustrade_debris':
			position object_pos('e_effect_02')
			rotation object_rot('e_effect_02')
			run_sequence 'explode'
	sequence 'remove_pole_03':
		TRIGGER TIMES 1
		Disable body 'pole_03'.
		Disable object 'g_pole_03'.
		Disable object 's_03'.
		Disable decal_mesh 'g_pole_03'.
		Enable object 'g_pole_03_dmg'.
		effect 'effects/payday2/particles/destruction/dest_statue_bust':
			parent object('e_effect_03')
			position v()
		spawn_unit 'units/pd2_dlc2/architecture/gov_c_int/gov_c_int_balustrade_debris':
			position object_pos('e_effect_03')
			rotation object_rot('e_effect_03')
			run_sequence 'explode'
	sequence 'remove_pole_04':
		TRIGGER TIMES 1
		Disable body 'pole_04'.
		Disable object 'g_pole_04'.
		Disable object 's_05'.
		Disable decal_mesh 'g_pole_04'.
		Enable object 'g_pole_04_dmg'.
		effect 'effects/payday2/particles/destruction/dest_statue_bust':
			parent object('e_effect_04')
			position v()
		spawn_unit 'units/pd2_dlc2/architecture/gov_c_int/gov_c_int_balustrade_debris':
			position object_pos('e_effect_04')
			rotation object_rot('e_effect_04')
			run_sequence 'explode'
	sequence 'remove_pole_05':
		TRIGGER TIMES 1
		Disable body 'pole_05'.
		Disable object 'g_pole_05'.
		Disable object 's_05'.
		Disable decal_mesh 'g_pole_05'.
		Enable object 'g_pole_05_dmg'.
		effect 'effects/payday2/particles/destruction/dest_statue_bust':
			parent object('e_effect_05')
			position v()
		spawn_unit 'units/pd2_dlc2/architecture/gov_c_int/gov_c_int_balustrade_debris':
			position object_pos('e_effect_05')
			rotation object_rot('e_effect_05')
			run_sequence 'explode'
	sequence 'remove_pole_06':
		TRIGGER TIMES 1
		Disable body 'pole_06'.
		Disable object 'g_pole_06'.
		Disable object 's_06'.
		Disable decal_mesh 'g_pole_06'.
		Enable object 'g_pole_06_dmg'.
		effect 'effects/payday2/particles/destruction/dest_statue_bust':
			parent object('e_effect_06')
			position v()
		spawn_unit 'units/pd2_dlc2/architecture/gov_c_int/gov_c_int_balustrade_debris':
			position object_pos('e_effect_06')
			rotation object_rot('e_effect_06')
			run_sequence 'explode'
	sequence 'remove_pole_07':
		TRIGGER TIMES 1
		Disable body 'pole_07'.
		Disable object 'g_pole_07'.
		Disable object 's_07'.
		Disable decal_mesh 'g_pole_07'.
		Enable object 'g_pole_07_dmg'.
		effect 'effects/payday2/particles/destruction/dest_statue_bust':
			parent object('e_effect_07')
			position v()
		spawn_unit 'units/pd2_dlc2/architecture/gov_c_int/gov_c_int_balustrade_debris':
			position object_pos('e_effect_07')
			rotation object_rot('e_effect_07')
			run_sequence 'explode'
	sequence 'remove_pole_08':
		TRIGGER TIMES 1
		Disable body 'pole_08'.
		Disable object 'g_pole_08'.
		Disable object 's_08'.
		Disable decal_mesh 'g_pole_08'.
		Enable object 'g_pole_08_dmg'.
		effect 'effects/payday2/particles/destruction/dest_statue_bust':
			parent object('e_effect_08')
			position v()
		spawn_unit 'units/pd2_dlc2/architecture/gov_c_int/gov_c_int_balustrade_debris':
			position object_pos('e_effect_08')
			rotation object_rot('e_effect_08')
			run_sequence 'explode'
	sequence 'remove_pole_09':
		TRIGGER TIMES 1
		Disable body 'pole_09'.
		Disable object 'g_pole_09'.
		Disable object 's_09'.
		Disable decal_mesh 'g_pole_09'.
		Enable object 'g_pole_09_dmg'.
		effect 'effects/payday2/particles/destruction/dest_statue_bust':
			parent object('e_effect_09')
			position v()
		spawn_unit 'units/pd2_dlc2/architecture/gov_c_int/gov_c_int_balustrade_debris':
			position object_pos('e_effect_09')
			rotation object_rot('e_effect_09')
			run_sequence 'explode'
	sequence 'remove_pole_10':
		TRIGGER TIMES 1
		Disable body 'pole_10'.
		Disable object 'g_pole_10'.
		Disable object 's_10'.
		Disable decal_mesh 'g_pole_10'.
		Enable object 'g_pole_10_dmg'.
		effect 'effects/payday2/particles/destruction/dest_statue_bust':
			parent object('e_effect_10')
			position v()
		spawn_unit 'units/pd2_dlc2/architecture/gov_c_int/gov_c_int_balustrade_debris':
			position object_pos('e_effect_10')
			rotation object_rot('e_effect_10')
			run_sequence 'explode'
	sequence 'remove_pole_11':
		TRIGGER TIMES 1
		Disable body 'pole_11'.
		Disable object 'g_pole_11'.
		Disable object 's_11'.
		Disable decal_mesh 'g_pole_11'.
		Enable object 'g_pole_11_dmg'.
		effect 'effects/payday2/particles/destruction/dest_statue_bust':
			parent object('e_effect_11')
			position v()
		spawn_unit 'units/pd2_dlc2/architecture/gov_c_int/gov_c_int_balustrade_debris':
			position object_pos('e_effect_11')
			rotation object_rot('e_effect_11')
			run_sequence 'explode'
	body 'pole_01'
		Upon receiving 4 bullet hits or 250 explosion damage, execute:
			Run sequence 'remove_pole_01'.
	body 'pole_02'
		Upon receiving 4 bullet hits or 250 explosion damage, execute:
			Run sequence 'remove_pole_02'.
	body 'pole_03'
		Upon receiving 4 bullet hits or 250 explosion damage, execute:
			Run sequence 'remove_pole_03'.
	body 'pole_04'
		Upon receiving 4 bullet hits or 250 explosion damage, execute:
			Run sequence 'remove_pole_04'.
	body 'pole_05'
		Upon receiving 4 bullet hits or 250 explosion damage, execute:
			Run sequence 'remove_pole_05'.
	body 'pole_06'
		Upon receiving 4 bullet hits or 250 explosion damage, execute:
			Run sequence 'remove_pole_06'.
	body 'pole_07'
		Upon receiving 4 bullet hits or 250 explosion damage, execute:
			Run sequence 'remove_pole_07'.
	body 'pole_08'
		Upon receiving 4 bullet hits or 250 explosion damage, execute:
			Run sequence 'remove_pole_08'.
	body 'pole_09'
		Upon receiving 4 bullet hits or 250 explosion damage, execute:
			Run sequence 'remove_pole_09'.
	body 'pole_10'
		Upon receiving 4 bullet hits or 250 explosion damage, execute:
			Run sequence 'remove_pole_10'.
	body 'pole_11'
		Upon receiving 4 bullet hits or 250 explosion damage, execute:
			Run sequence 'remove_pole_11'.
