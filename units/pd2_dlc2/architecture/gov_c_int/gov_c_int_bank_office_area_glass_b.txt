unit:
	sequence 'destroy_glass_01':
		TRIGGER TIMES 1
		Disable body 'glass_01'.
		Disable object 'g_glass_01'.
		Disable decal_mesh 'g_glass_01'.
		Play audio 'window_large_shatter' at 'e_glass_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_01' )
			position v()
		Cause alert with 12 m radius.
	sequence 'destroy_glass_02':
		TRIGGER TIMES 1
		Disable body 'glass_02'.
		Disable object 'g_glass_02'.
		Disable decal_mesh 'g_glass_02'.
		Play audio 'window_large_shatter' at 'e_glass_02'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_02' )
			position v()
		Cause alert with 12 m radius.
	sequence 'destroy_glass_03':
		TRIGGER TIMES 1
		Disable body 'glass_03'.
		Disable object 'g_glass_03'.
		Disable decal_mesh 'g_glass_03'.
		Play audio 'window_large_shatter' at 'e_glass_03'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_03' )
			position v()
		Cause alert with 12 m radius.
	sequence 'destroy_glass_04':
		TRIGGER TIMES 1
		Disable body 'glass_04'.
		Disable object 'g_glass_04'.
		Disable decal_mesh 'g_glass_04'.
		Play audio 'window_large_shatter' at 'e_glass_04'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_04' )
			position v()
		Cause alert with 12 m radius.
	body 'glass_01'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_glass_01'.
	body 'glass_02'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_glass_02'.
	body 'glass_03'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_glass_03'.
	body 'glass_04'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_glass_04'.
