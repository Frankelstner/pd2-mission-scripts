unit:
	sequence 'explode':
		Call function World:play_physic_effect('physic_effects/push_sphere_unit',dest_unit,750,0.2,pos,dir).
	sequence 'destroy_01':
		TRIGGER TIMES 1
		Disable object 'g_1'.
		Disable object 's_1'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect_01')
			position v()
	sequence 'destroy_02':
		TRIGGER TIMES 1
		Disable object 'g_2'.
		Disable object 's_2'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect_02')
			position v()
	sequence 'destroy_03':
		TRIGGER TIMES 1
		Disable object 'g_3'.
		Disable object 's_3'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect_03')
			position v()
	sequence 'destroy_04':
		TRIGGER TIMES 1
		Disable object 'g_4'.
		Disable object 's_4'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect_04')
			position v()
	sequence 'destroy_05':
		TRIGGER TIMES 1
		Disable object 'g_5'.
		Disable object 's_5'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect_05')
			position v()
	sequence 'destroy_06':
		TRIGGER TIMES 1
		Disable object 'g_6'.
		Disable object 's_6'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect_06')
			position v()
	sequence 'destroy_07':
		TRIGGER TIMES 1
		Disable object 'g_7'.
		Disable object 's_7'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect_07')
			position v()
	sequence 'destroy_08':
		TRIGGER TIMES 1
		Disable object 'g_8'.
		Disable object 's_8'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_effect_08')
			position v()
	body 'debris_body_01'
		Upon receiving 1 bullet hit or 10 collision damage or 40 explosion damage, execute:
			Run sequence 'destroy_01'.
	body 'debris_body_02'
		Upon receiving 1 bullet hit or 10 collision damage or 40 explosion damage, execute:
			Run sequence 'destroy_02'.
	body 'debris_body_03'
		Upon receiving 1 bullet hit or 10 collision damage or 40 explosion damage, execute:
			Run sequence 'destroy_03'.
	body 'debris_body_04'
		Upon receiving 1 bullet hit or 10 collision damage or 40 explosion damage, execute:
			Run sequence 'destroy_04'.
	body 'debris_body_05'
		Upon receiving 1 bullet hit or 10 collision damage or 40 explosion damage, execute:
			Run sequence 'destroy_05'.
	body 'debris_body_06'
		Upon receiving 1 bullet hit or 10 collision damage or 40 explosion damage, execute:
			Run sequence 'destroy_06'.
	body 'debris_body_07'
		Upon receiving 1 bullet hit or 10 collision damage or 40 explosion damage, execute:
			Run sequence 'destroy_07'.
	body 'debris_body_08'
		Upon receiving 1 bullet hit or 10 collision damage or 40 explosion damage, execute:
			Run sequence 'destroy_08'.
