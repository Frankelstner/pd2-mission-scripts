unit:
	sequence 'destroy_glass':
		TRIGGER TIMES 1
		Disable body 'static_body'.
		Disable object 'g_g'.
		Enable object 'g_dmg_edges'.
		Disable decal_mesh 'dm_glass'.
		Play audio 'window_large_shatter' at 'e_glass'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass')
			position v()
		spawn_unit 'units/pd2_dlc2/architecture/gov_c_ext/gov_c_ext_window_debris':
			position object_pos('e_glass')
			rotation object_rot('e_glass')
			run_sequence 'explode'
		Cause alert with 12 m radius.
	body 'static_body'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_glass'.
