unit:
	sequence 'bridge_show':
		Enable object 'g_bridge'.
		Enable body 'body_bridge'.
		Enable decal_mesh 'dm_metal_01'.
		Enable decal_mesh 'dm_metal_02'.
	sequence 'bridge_hide':
		Disable object 'g_bridge'.
		Disable body 'body_bridge'.
		Disable decal_mesh 'dm_metal_01'.
		Disable decal_mesh 'dm_metal_02'.
