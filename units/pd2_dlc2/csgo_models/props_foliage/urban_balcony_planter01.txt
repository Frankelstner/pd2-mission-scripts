unit:
	sequence 'break1':
		Disable body 'body1'.
		Enable body 'body1_debris'.
		Disable body 'body2'.
		Enable body 'body2_debris'.
		Disable body 'body3'.
		Enable body 'body3_debris'.
		Disable decal_mesh 'g_urban_balcony_planter01_0'.
		Enable decal_mesh 'g_dmg_1'.
		Enable decal_mesh 'g_dmg_2'.
		Enable decal_mesh 'g_dmg_3'.
		effect 'effects/payday2/particles/destruction/dest_plant_matter':
			parent object( 'e_1' )
			position v()
		effect 'effects/payday2/particles/destruction/dest_plant_matter':
			parent object( 'e_2' )
			position v()
		Disable object 'g_urban_balcony_planter01_0'.
		Disable object 'g_urban_balcony_planter01_1'.
		Enable object 'g_dmg_1'.
		Enable object 'g_p_dmg_1'.
		Enable object 'g_dmg_2'.
		Enable object 'g_p_dmg_2'.
		Enable object 'g_dmg_3'.
		Enable object 'g_p_dmg_3'.
		Play audio 'pot_large_shatter' at 'e_1'.
		slot:
			slot 18
	sequence 'break2':
		Disable body 'body1'.
		Enable body 'body1_debris'.
		Disable body 'body2'.
		Enable body 'body2_debris'.
		Disable body 'body3'.
		Enable body 'body3_debris'.
		Disable decal_mesh 'g_urban_balcony_planter01_0'.
		Enable decal_mesh 'g_dmg_1'.
		Enable decal_mesh 'g_dmg_2'.
		Enable decal_mesh 'g_dmg_3'.
		effect 'effects/payday2/particles/destruction/dest_plant_matter':
			parent object( 'e_1' )
			position v()
		effect 'effects/payday2/particles/destruction/dest_plant_matter':
			parent object( 'e_2' )
			position v()
		effect 'effects/payday2/particles/destruction/dest_plant_matter':
			parent object( 'e_3' )
			position v()
		Disable object 'g_urban_balcony_planter01_0'.
		Disable object 'g_urban_balcony_planter01_1'.
		Enable object 'g_dmg_1'.
		Enable object 'g_p_dmg_1'.
		Enable object 'g_dmg_2'.
		Enable object 'g_p_dmg_2'.
		Enable object 'g_dmg_3'.
		Enable object 'g_p_dmg_3'.
		Play audio 'pot_large_shatter' at 'e_2'.
		slot:
			slot 18
	sequence 'break3':
		Disable body 'body1'.
		Enable body 'body1_debris'.
		Disable body 'body2'.
		Enable body 'body2_debris'.
		Disable body 'body3'.
		Enable body 'body3_debris'.
		Disable decal_mesh 'g_urban_balcony_planter01_0'.
		Enable decal_mesh 'g_dmg_1'.
		Enable decal_mesh 'g_dmg_2'.
		Enable decal_mesh 'g_dmg_3'.
		effect 'effects/payday2/particles/destruction/dest_plant_matter':
			parent object( 'e_2' )
			position v()
		effect 'effects/payday2/particles/destruction/dest_plant_matter':
			parent object( 'e_3' )
			position v()
		Disable object 'g_urban_balcony_planter01_0'.
		Disable object 'g_urban_balcony_planter01_1'.
		Enable object 'g_dmg_1'.
		Enable object 'g_p_dmg_1'.
		Enable object 'g_dmg_2'.
		Enable object 'g_p_dmg_2'.
		Enable object 'g_dmg_3'.
		Enable object 'g_p_dmg_3'.
		Play audio 'pot_large_shatter' at 'e_3'.
		slot:
			slot 18
	body 'body1'
		Upon receiving 2 bullet hits or 40 explosion damage, execute:
			Run sequence 'break1'.
	body 'body2'
		Upon receiving 2 bullet hits or 40 explosion damage, execute:
			Run sequence 'break2'.
	body 'body3'
		Upon receiving 2 bullet hits or 40 explosion damage, execute:
			Run sequence 'break3'.
