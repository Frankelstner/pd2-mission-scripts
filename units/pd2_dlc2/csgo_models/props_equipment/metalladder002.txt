unit:
	sequence 'done_hit'.
	body 'static_body'
		Upon receiving 60 explosion damage, execute:
			Run sequence 'int_seq_dmg_1'.
	sequence 'int_seq_dmg_1':
		Disable object 'g_metalladder002'.
		Enable object 'g_dmg'.
		Run sequence 'done_hit'.
	sequence 'make_indestructible':
		Enable body 'indestructible_body'.
		Disable body 'static_body'.
