unit:
	front = 0
	left = 0
	right = 0
	sequence 'crack_front':
		front = 1
		Disable object 'g_window_1'.
		Enable object 'g_window_dmg_1'.
		Disable decal_mesh 'g_window_1'.
		Enable decal_mesh 'g_window_dmg_1'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_locator_1' (alarm reason: 'glass').
	sequence 'crack_left':
		left = 1
		Disable object 'g_window_2'.
		Enable object 'g_window_dmg_2'.
		Disable decal_mesh 'g_window_2'.
		Enable decal_mesh 'g_window_dmg_2'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_locator_2' (alarm reason: 'glass').
	sequence 'crack_right':
		right = 1
		Disable object 'g_window_3'.
		Enable object 'g_window_dmg_3'.
		Disable decal_mesh 'g_window_3'.
		Enable decal_mesh 'g_window_dmg_3'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_locator_3' (alarm reason: 'glass').
	sequence 'shatter_front':
		Disable object 'g_window_1'.
		Disable object 'g_window_dmg_1'.
		Disable body 'window_front_body'.
		Disable decal_mesh 'g_window_1'.
		Disable decal_mesh 'g_window_dmg_1'.
		Play audio 'window_medium_shatter' at 'e_locator_1'.
		effect 'effects/payday2/particles/window/storefront_window_large':
			parent object('e_locator_1')
			position v()
		Cause alert with 12 m radius.
	sequence 'shatter_left':
		Disable object 'g_window_2'.
		Disable object 'g_window_dmg_2'.
		Disable body 'window_left_body'.
		Disable decal_mesh 'g_window_2'.
		Disable decal_mesh 'g_window_dmg_2'.
		Play audio 'window_small_shatter' at 'e_locator_2'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_locator_2')
			position v()
		Cause alert with 12 m radius.
	sequence 'shatter_right':
		Disable object 'g_window_3'.
		Disable object 'g_window_dmg_3'.
		Disable decal_mesh 'g_window_3'.
		Disable decal_mesh 'g_window_dmg_3'.
		Disable body 'window_right_body'.
		Play audio 'window_small_shatter' at 'e_locator_3'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_locator_3')
			position v()
		Cause alert with 12 m radius.
	body 'window_front_body'
		Upon receiving 1 bullet hit or 30 explosion damage, execute:
			If front == 0: Run sequence 'crack_front'.
		Upon receiving 2 bullet hits or 30 explosion damage or 10 melee damage, execute:
			If front == 1: Run sequence 'shatter_front'.
	body 'window_left_body'
		Upon receiving 1 bullet hit or 30 explosion damage, execute:
			If left == 0: Run sequence 'crack_left'.
		Upon receiving 2 bullet hits or 30 explosion damage or 10 melee damage, execute:
			If left == 1: Run sequence 'shatter_left'.
	body 'window_right_body'
		Upon receiving 1 bullet hit or 30 explosion damage, execute:
			If right == 0: Run sequence 'crack_right'.
		Upon receiving 2 bullet hits or 30 explosion damage or 10 melee damage, execute:
			If right == 1: Run sequence 'shatter_right'.
