unit:
	top = 0
	sequence 'crack_window':
		top = 1
		Disable object 'g_glass'.
		Enable object 'g_glass_dmg'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass' (alarm reason: 'glass').
	sequence 'shatter_window':
		TRIGGER TIMES 1
		Disable object 'g_glass'.
		Disable object 'g_glass_dmg'.
		Disable body 'static_window'.
		Disable decal_mesh 'dm_glass_breakable'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass')
			position v()
		Play audio 'window_medium_shatter' at 'e_glass'.
		Cause alert with 12 m radius.
	body 'static_window'
		Upon receiving 1 bullet hit or 30 explosion damage, execute:
			If top == 0: Run sequence 'crack_window'.
		Upon receiving 2 bullet hits or 30 explosion damage or 10 melee damage, execute:
			If top == 1: Run sequence 'shatter_window'.
