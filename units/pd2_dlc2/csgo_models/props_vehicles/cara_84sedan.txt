unit:
	open = 0
	sequence 'state_interaction_enabled':
		Enable interaction.
	sequence 'state_interaction_disabled':
		Disable interaction.
	sequence 'orig':
		material_config 'units/pd2_dlc2/csgo_models/props_vehicles/cara_84sedan'.
	sequence 'blue':
		material_config 'units/pd2_dlc2/csgo_models/props_vehicles/cara_84sedan_blue'.
	sequence 'brown':
		material_config 'units/pd2_dlc2/csgo_models/props_vehicles/cara_84sedan_brown'.
	sequence 'green':
		material_config 'units/pd2_dlc2/csgo_models/props_vehicles/cara_84sedan_green'.
	sequence 'lightgreen':
		material_config 'units/pd2_dlc2/csgo_models/props_vehicles/cara_84sedan_lightgreen'.
	sequence 'anim_trunk_open':
		Enable animation_group 'trunk':
			from 0/30
			to 15/30
		Play audio 'open_car_trunk' at 'a_anim_trunk'.
		open = 1
	sequence 'anim_trunk_close':
		Enable animation_group 'trunk':
			from 15/30
			speed -1
			to 0/30
		open = 0
	sequence 'interact':
		If open == 1: Run sequence 'int_trunk_close'.
		If open == 0: Run sequence 'anim_trunk_open'.
		If open == 2: Run sequence 'anim_trunk_close'.
	sequence 'int_trunk_close':
		Enable animation_group 'trunk':
			from 15/30
			speed -1
			to 0/30
		open = 2
