unit:
	sequence 'orig':
		material_config 'units/pd2_dlc2/csgo_models/props_vehicles/cara_95sedan_wrecked'.
	sequence 'blue':
		material_config 'units/pd2_dlc2/csgo_models/props_vehicles/cara_95sedan_wrecked_blue'.
	sequence 'brown':
		material_config 'units/pd2_dlc2/csgo_models/props_vehicles/cara_95sedan_wrecked_brown'.
	sequence 'green':
		material_config 'units/pd2_dlc2/csgo_models/props_vehicles/cara_95sedan_wrecked_green'.
	sequence 'lightgreen':
		material_config 'units/pd2_dlc2/csgo_models/props_vehicles/cara_95sedan_wrecked_lightgreen'.
