unit:
	sequence 'crack_01':
		Disable object 'g_skylight_broken_whole_1'.
		Enable object 'g_skylight_broken_cracked_1'.
		Disable decal_mesh 'g_skylight_broken_whole_1'.
		Enable decal_mesh 'g_skylight_broken_cracked_1'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_10' (alarm reason: 'glass').
	sequence 'crack_02':
		Disable object 'g_skylight_broken_whole_2'.
		Enable object 'g_skylight_broken_cracked_2'.
		Disable decal_mesh 'g_skylight_broken_whole_2'.
		Enable decal_mesh 'g_skylight_broken_cracked_2'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_02' (alarm reason: 'glass').
	sequence 'crack_03':
		Disable object 'g_skylight_broken_whole_3'.
		Enable object 'g_skylight_broken_cracked_3'.
		Disable decal_mesh 'g_skylight_broken_whole_3'.
		Enable decal_mesh 'g_skylight_broken_cracked_3'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_08' (alarm reason: 'glass').
	sequence 'crack_04':
		Disable object 'g_skylight_broken_whole_4'.
		Enable object 'g_skylight_broken_cracked_4'.
		Disable decal_mesh 'g_skylight_broken_whole_4'.
		Enable decal_mesh 'g_skylight_broken_cracked_4'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_06' (alarm reason: 'glass').
	sequence 'crack_05':
		Disable object 'g_skylight_broken_whole_5'.
		Enable object 'g_skylight_broken_cracked_5'.
		Disable decal_mesh 'g_skylight_broken_whole_5'.
		Enable decal_mesh 'g_skylight_broken_cracked_5'.
		Add attention/detection preset 'prop_ene_ntl' to 'e_glass_04' (alarm reason: 'glass').
	sequence 'shatter':
		TRIGGER TIMES 1
		Disable body '01_body'.
		Disable body '02_body'.
		Disable body '03_body'.
		Disable body '04_body'.
		Disable body '05_body'.
		Enable body 'static_broken'.
		Enable object 'g_skylight_broken_large_0'.
		Enable object 'g_skylight_broken_large_1'.
		Disable object 'g_skylight_broken_whole_0'.
		Disable object 'g_skylight_broken_whole_1'.
		Disable object 'g_skylight_broken_cracked_1'.
		Disable object 'g_skylight_broken_whole_2'.
		Disable object 'g_skylight_broken_cracked_2'.
		Disable object 'g_skylight_broken_whole_3'.
		Disable object 'g_skylight_broken_cracked_3'.
		Disable object 'g_skylight_broken_whole_4'.
		Disable object 'g_skylight_broken_cracked_4'.
		Disable object 'g_skylight_broken_whole_5'.
		Disable object 'g_skylight_broken_cracked_5'.
		Enable decal_mesh 'g_skylight_broken_large_0'.
		Enable decal_mesh 'g_skylight_broken_large_1'.
		Disable decal_mesh 'g_skylight_broken_whole_0'.
		Disable decal_mesh 'g_skylight_broken_whole_1'.
		Disable decal_mesh 'g_skylight_broken_cracked_1'.
		Disable decal_mesh 'g_skylight_broken_whole_2'.
		Disable decal_mesh 'g_skylight_broken_cracked_2'.
		Disable decal_mesh 'g_skylight_broken_whole_3'.
		Disable decal_mesh 'g_skylight_broken_cracked_3'.
		Disable decal_mesh 'g_skylight_broken_whole_4'.
		Disable decal_mesh 'g_skylight_broken_cracked_4'.
		Disable decal_mesh 'g_skylight_broken_whole_5'.
		Disable decal_mesh 'g_skylight_broken_cracked_5'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_01' )
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_02' )
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_03' )
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_04' )
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_05' )
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_06' )
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_07' )
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_08' )
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_09' )
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_10' )
			position v()
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_glass_11' )
			position v()
		Play audio 'window_large_shatter' at 'e_glass_10'.
		Cause alert with 12 m radius.
	body 'static_body'
		Upon receiving 10 explosion damage, execute:
			Run sequence 'shatter'.
	body '01_body'
		Upon receiving 1 bullet hit, execute:
			Run sequence 'crack_01'.
		Upon receiving 2 bullet hits or 10 melee damage, execute:
			Run sequence 'shatter'.
	body '02_body'
		Upon receiving 1 bullet hit, execute:
			Run sequence 'crack_02'.
		Upon receiving 2 bullet hits or 10 melee damage, execute:
			Run sequence 'shatter'.
	body '03_body'
		Upon receiving 1 bullet hit, execute:
			Run sequence 'crack_03'.
		Upon receiving 2 bullet hits or 10 melee damage, execute:
			Run sequence 'shatter'.
	body '04_body'
		Upon receiving 1 bullet hit, execute:
			Run sequence 'crack_04'.
		Upon receiving 2 bullet hits or 10 melee damage, execute:
			Run sequence 'shatter'.
	body '05_body'
		Upon receiving 1 bullet hit, execute:
			Run sequence 'crack_05'.
		Upon receiving 2 bullet hits or 10 melee damage, execute:
			Run sequence 'shatter'.
