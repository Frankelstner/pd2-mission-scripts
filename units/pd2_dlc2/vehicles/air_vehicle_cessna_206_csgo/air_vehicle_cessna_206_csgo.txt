unit:
	int = 0
	sequence 'state_vis_show':
		Show graphic_group 'cessna'.
	sequence 'state_vis_hide':
		Hide graphic_group 'cessna'.
	sequence 'state_interaction_enabled':
		Enable interaction.
		Enable object 'g_outline'.
	sequence 'state_interaction_disabled':
		Disable interaction.
		Disable object 'g_outline'.
	sequence 'state_reset_cage':
		int = 0
		Run sequence 'state_interaction_disabled'.
		Enable animation_group 'anim_plane':
			from 0
			speed 0
		Enable animation_group 'anim_cage':
			from 0
			speed 0
		Disable body 'body_bag_blocker_bottom'.
		Disable body 'body_cage_bottom'.
		Disable decal_mesh 'g_cage_bottom'.
		Disable object 'g_cage_bottom'.
		Disable body 'body_bag_blocker'.
		Disable body 'body_cage'.
		Disable decal_mesh 'g_cage_top'.
		Disable object 'g_cage_top'.
		Disable body 'balloon_box'.
		Disable decal_mesh 'g_balloon_box'.
		Disable decal_mesh 'g_balloon_box_lid'.
		Disable object 'g_balloon_box'.
		Disable object 'g_balloon_box_lid'.
		Disable object 'g_string_static'.
		Disable object 'g_cage_wire'.
		Disable object 'g_canister'.
		Disable object 'g_balloon'.
	sequence 'state_build_cage':
		If int == 2: Run sequence 'int_seq_show_part_3'.
		If int == 1: Run sequence 'int_seq_show_part_2'.
		If int == 0: Run sequence 'int_seq_show_part_1'.
		Play audio 'saw_impact_medium' at 'rp_cessna'.
	sequence 'anim_plane_miss':
		Run sequence 'int_plane_show'.
		Enable animation_group 'anim_plane':
			from 45/30
		Play audio 'airplane_loop' at 'snd'.
		Run sequence 'done_cage_miss'. (DELAY 205/30)
		Run sequence 'int_plane_hide'. (DELAY 455/30)
		Play audio 'airplane_fade_out' at 'snd'. (DELAY 455/30)
		Run sequence 'done_animation'. (DELAY 455/30)
	sequence 'anim_plane_crash':
		Run sequence 'int_plane_show'.
		Play audio 'airplane_loop' at 'snd'.
		Enable animation_group 'anim_plane':
			from 45/30
		effect 'effects/particles/dest/engine_smoke': (DELAY 250/30)
			parent object( 'a_body' )
			position v()
		Play audio 'airplane_break_loop' at 'snd'. (DELAY 250/30)
		effect 'effects/payday2/particles/explosions/plane_explosion': (DELAY 355/30)
			parent object( 'a_body' )
			position v()
		Play audio 'airplane_explode' at 'snd'. (DELAY 355/30)
		Run sequence 'done_cage_crash'. (DELAY 205/30)
		Run sequence 'int_plane_hide'. (DELAY 355/30)
	sequence 'anim_plane_catch':
		Disable interaction.
		Run sequence 'int_plane_show'.
		Play audio 'airplane_loop' at 'snd'.
		Enable animation_group 'anim_plane':
			from 45/30
		Disable animation_group 'anim_cage': (DELAY 204/30)
			from 0/30
		Enable animation_group 'anim_cage': (DELAY 205/30)
			from 250/30
		Play audio 'wire_hit_plane' at 'ct_ring'. (DELAY 205/30)
		Play audio 'cage_liftoff' at 'rp_cessna'. (DELAY 205/30)
		Run sequence 'state_cage_collision_hide'. (DELAY 200/30)
		Run sequence 'done_cage_catch'. (DELAY 170/30)
		Run sequence 'int_plane_hide'. (DELAY 455/30)
		Play audio 'airplane_fade_out' at 'snd'. (DELAY 455/30)
		Run sequence 'done_animation'. (DELAY 455/30)
	sequence 'interact':
		Enable body 'balloon_box_lid'.
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('e_force') ),18,3).
		effect 'effects/payday2/particles/explosions/smoke_explosion/smoke':
			parent object( 'e_smoke' )
			position v()
		Enable object 'g_balloon'.
		Disable object 'g_string_static'.
		Disable object 'g_balloon_box_lid'.
		Enable animation_group 'anim_cage':
			from 0/30
			to 15/30
		Enable animation_group 'anim_cage': (DELAY 15/30)
			from 20/30
			loop True
			speed 0.1
			to 40/30
		Play audio 'balloon_release' at 'interact'.
	sequence 'int_seq_show_part_1':
		Enable body 'body_bag_blocker_bottom'.
		Enable body 'body_cage_bottom'.
		Enable decal_mesh 'g_cage_bottom'.
		Enable object 'g_cage_bottom'.
		int = 1
	sequence 'int_seq_show_part_2':
		Enable body 'body_bag_blocker'.
		Enable body 'body_cage'.
		Enable decal_mesh 'g_cage_top'.
		Enable object 'g_cage_top'.
		int = 2
	sequence 'int_seq_show_part_3':
		Enable body 'balloon_box'.
		Enable decal_mesh 'g_balloon_box'.
		Enable decal_mesh 'g_balloon_box_lid'.
		Enable object 'g_balloon_box'.
		Enable object 'g_balloon_box_lid'.
		Enable object 'g_string_static'.
		Enable object 'g_cage_wire'.
		Enable object 'g_canister'.
	sequence 'int_plane_show':
		Show graphic_group 'cessna'.
	sequence 'int_plane_hide':
		Hide graphic_group 'cessna'.
	sequence 'state_cage_collision_hide':
		Disable body 'body_bag_blocker_bottom'.
		Disable body 'body_bag_blocker'.
		Disable decal_mesh 'g_balloon_box'.
		Disable decal_mesh 'g_balloon_box_lid'.
		Disable decal_mesh 'g_cage_bottom'.
		Disable decal_mesh 'g_cage_top'.
		Disable body 'body_cage'.
		Disable body 'balloon_box'.
		Disable body 'balloon_box_lid'.
		Disable body 'body_cage_bottom'.
	sequence 'done_cage_catch'.
	sequence 'done_cage_miss'.
	sequence 'done_cage_crash'.
	sequence 'done_animation'.
