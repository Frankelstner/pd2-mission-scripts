unit:
	sequence 'state_int_enabled':
		Enable interaction.
		Show graphic_group 'grp_icon'.
	sequence 'state_int_disable':
		Disable interaction.
		Hide graphic_group 'grp_icon'.
	sequence 'state_vis_hide':
		Disable interaction.
		Disable object 'g_shelf_01_boxes'.
		Disable object 'g_shelf_01_boxes_dmg'.
		Hide graphic_group 'grp_icon'.
	sequence 'state_vis_unhide':
		Enable object 'g_shelf_01_boxes'.
		Disable object 'g_shelf_01_boxes_dmg'.
	sequence 'interact':
		Disable object 'g_shelf_01_boxes'.
		Enable object 'g_shelf_01_boxes_dmg'.
		Hide graphic_group 'grp_icon'.
		Play audio 'bar_gift_box_open_end' at 'rp_are_supply_shelf_01_boxes'.
