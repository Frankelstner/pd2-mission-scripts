unit:
	sequence 'destroy':
		TRIGGER TIMES 1
		Disable object 'g_g'.
		Play audio 'window_medium_shatter' at 'e_01'.
		effect 'effects/payday2/particles/window/storefront_window_large':
			parent object( 'e_01' )
			position v()
		Cause alert with 12 m radius.
		Disable body 'body_static'.
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy'.
