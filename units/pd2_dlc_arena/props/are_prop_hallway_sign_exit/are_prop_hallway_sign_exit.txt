unit:
	body 'body_sign'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'int_seq_break'.
	sequence 'int_seq_break':
		TRIGGER TIMES 1
		Disable object 'g_g'.
		Disable decal_mesh 'dm_metal'.
		effect 'effects/payday2/particles/explosions/sparks/flashbang_spark':
			parent object( 'e_effect1' )
			position v()
		Play audio 'ceiling_lamp_break' at 'e_effect1'.
		Hide graphic_group 'grp_whole'.
		Show graphic_group 'grp_broken'.
