unit:
	sequence 'light_on_01':
		Enable light 'li_spotlight_01'.
		Show graphic_group 'lamp_grp_01'.
		Hide graphic_group 'lamp_grp_off_01'.
		Enable object 'g_glow_01'.
	sequence 'light_off_01':
		Disable light 'li_spotlight_01'.
		Hide graphic_group 'lamp_grp_01'.
		Show graphic_group 'lamp_grp_off_01'.
		Disable object 'g_glow_01'.
	sequence 'break_int_01':
		TRIGGER TIMES 1
		Disable light 'li_spotlight_01'.
		Hide graphic_group 'lamp_grp_off_01'.
		Hide graphic_group 'lamp_grp_01'.
		Show graphic_group 'lamp_dmg_grp_01'.
		Disable object 'g_glow_01'.
		Play audio 'light_bulb_smash' at 'e_effect_01'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_effect_01' )
			position v()
	body 'body_hitbox_01'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_int_01'.
	sequence 'light_on_02':
		Enable light 'li_spotlight_02'.
		Show graphic_group 'lamp_grp_02'.
		Hide graphic_group 'lamp_grp_off_02'.
		Enable object 'g_glow_02'.
	sequence 'light_off_02':
		Disable light 'li_spotlight_02'.
		Hide graphic_group 'lamp_grp_02'.
		Show graphic_group 'lamp_grp_off_02'.
		Disable object 'g_glow_02'.
	sequence 'break_int_02':
		TRIGGER TIMES 1
		Disable light 'li_spotlight_02'.
		Hide graphic_group 'lamp_grp_off_02'.
		Hide graphic_group 'lamp_grp_02'.
		Show graphic_group 'lamp_dmg_grp_02'.
		Disable object 'g_glow_02'.
		Play audio 'light_bulb_smash' at 'e_effect_02'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_effect_02' )
			position v()
	body 'body_hitbox_02'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_int_02'.
	sequence 'light_on_03':
		Enable light 'li_spotlight_03'.
		Show graphic_group 'lamp_grp_03'.
		Hide graphic_group 'lamp_grp_off_03'.
		Enable object 'g_glow_03'.
	sequence 'light_off_03':
		Disable light 'li_spotlight_03'.
		Hide graphic_group 'lamp_grp_03'.
		Show graphic_group 'lamp_grp_off_03'.
		Disable object 'g_glow_03'.
	sequence 'break_int_03':
		TRIGGER TIMES 1
		Disable light 'li_spotlight_03'.
		Hide graphic_group 'lamp_grp_off_03'.
		Hide graphic_group 'lamp_grp_03'.
		Show graphic_group 'lamp_dmg_grp_03'.
		Disable object 'g_glow_03'.
		Play audio 'light_bulb_smash' at 'e_effect_03'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_effect_03' )
			position v()
	body 'body_hitbox_03'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_int_03'.
	sequence 'light_on_04':
		Enable light 'li_spotlight_04'.
		Show graphic_group 'lamp_grp_04'.
		Hide graphic_group 'lamp_grp_off_04'.
		Enable object 'g_glow_04'.
	sequence 'light_off_04':
		Disable light 'li_spotlight_04'.
		Hide graphic_group 'lamp_grp_04'.
		Show graphic_group 'lamp_grp_off_04'.
		Disable object 'g_glow_04'.
	sequence 'break_int_04':
		TRIGGER TIMES 1
		Disable light 'li_spotlight_04'.
		Hide graphic_group 'lamp_grp_off_04'.
		Hide graphic_group 'lamp_grp_04'.
		Show graphic_group 'lamp_dmg_grp_04'.
		Disable object 'g_glow_04'.
		Play audio 'light_bulb_smash' at 'e_effect_04'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_effect_04' )
			position v()
	body 'body_hitbox_04'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_int_04'.
	sequence 'light_on_05':
		Enable light 'li_spotlight_05'.
		Show graphic_group 'lamp_grp_05'.
		Show graphic_group 'lamp_grp_off_05'.
		Disable object 'g_glow_04'.
	sequence 'light_off_05':
		Disable light 'li_spotlight_05'.
		Hide graphic_group 'lamp_grp_05'.
		Show graphic_group 'lamp_grp_off_05'.
		Disable object 'g_glow_05'.
	sequence 'break_int_05':
		TRIGGER TIMES 1
		Disable light 'li_spotlight_05'.
		Hide graphic_group 'lamp_grp_off_05'.
		Hide graphic_group 'lamp_grp_05'.
		Show graphic_group 'lamp_dmg_grp_05'.
		Disable object 'g_glow_05'.
		Play audio 'light_bulb_smash' at 'e_effect_05'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_effect_05' )
			position v()
	body 'body_hitbox_05'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_int_05'.
	sequence 'light_on_06':
		Enable light 'li_spotlight_06'.
		Show graphic_group 'lamp_grp_06'.
		Hide graphic_group 'lamp_grp_off_06'.
		Enable object 'g_glow_06'.
	sequence 'light_off_06':
		Disable light 'li_spotlight_06'.
		Hide graphic_group 'lamp_grp_06'.
		Show graphic_group 'lamp_grp_off_06'.
		Disable object 'g_glow_06'.
	sequence 'break_int_06':
		TRIGGER TIMES 1
		Disable light 'li_spotlight_06'.
		Hide graphic_group 'lamp_grp_off_06'.
		Hide graphic_group 'lamp_grp_06'.
		Show graphic_group 'lamp_dmg_grp_06'.
		Disable object 'g_glow_06'.
		Play audio 'light_bulb_smash' at 'e_effect_06'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_effect_06' )
			position v()
	body 'body_hitbox_06'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_int_06'.
