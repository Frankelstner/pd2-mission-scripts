unit:
	sequence 'light_on':
		Enable light 'li_spotlight'.
		Show graphic_group 'lamp_grp'.
		Hide graphic_group 'lamp_off_grp'.
		Enable object 'g_glow'.
	sequence 'light_off':
		Disable light 'li_spotlight'.
		Hide graphic_group 'lamp_grp'.
		Show graphic_group 'lamp_off_grp'.
		Disable object 'g_glow'.
	sequence 'break_int':
		TRIGGER TIMES 1
		Disable light 'li_spotlight'.
		Hide graphic_group 'lamp_off_grp'.
		Hide graphic_group 'lamp_grp'.
		Show graphic_group 'lamp_dmg_grp'.
		Disable object 'g_glow'.
		Play audio 'light_bulb_smash' at 'e_effect'.
		effect 'effects/particles/dest/sparks_lamp_dest':
			parent object( 'e_effect' )
			position v()
	body 'body_static'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_int'.
