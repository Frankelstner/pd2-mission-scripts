unit:
	body 'body_lock'
		Upon receiving 37.5 saw damage, execute:
			Run sequence 'open_door'.
	sequence 'explode_door':
		Run sequence 'seq_explode_in'.
	sequence 'seq_explode_in':
		Enable animation_group 'grp_anim_doors':
			from 70/30
			to 78/30
		Play audio 'c4_explode_metal' at 'a_shp_charge_1'.
		Run sequence 'done_exploded'.
		Run sequence 'done_open'.
		Run sequence 'deactivate_door'.
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'a_shp_charge_1' )
			position v()
		effect 'effects/payday2/particles/explosions/shapecharger_explosion':
			parent object( 'a_shp_charge_2' )
			position v()
	sequence 'done_exploded'.
	sequence 'done_open'.
	sequence 'close_door':
		Enable animation_group 'grp_anim_doors':
			from 30/30
			speed -1
			to 0/30
		Run sequence 'activate_door'.
	sequence 'open_door':
		Enable animation_group 'grp_anim_doors':
			from 0/30
			to 30/30
		Run sequence 'deactivate_door'.
		Run sequence 'done_open'.
	sequence 'kick_open':
		Enable animation_group 'grp_anim_doors':
			from 0/30
			speed 4
			to 30/30
		Play audio 'metal_door_push_01' at 'a_shp_charge_1'.
		Run sequence 'deactivate_door'.
		Run sequence 'done_open'.
	sequence 'interact':
		Run sequence 'open_door'.
	sequence 'activate_door':
		Enable body 'body_lock'.
		Show graphic_group 'grp_saw_icon'.
		Call function: base.activate()
	sequence 'deactivate_door':
		Disable body 'body_lock'.
		Hide graphic_group 'grp_saw_icon'.
		Call function: base.deactivate()
	sequence 'door_opened'.
	sequence 'door_closed'.
	sequence 'c4_placed'.
	sequence 'c4_completed'.
	sequence 'all_c4_placed'.
	sequence 'drill_placed'.
	sequence 'drill_jammed'.
	sequence 'drill_resumed'.
	sequence 'drill_power_off'.
	sequence 'drill_power_on'.
	sequence 'drill_completed'.
	sequence 'all_drill_placed'.
