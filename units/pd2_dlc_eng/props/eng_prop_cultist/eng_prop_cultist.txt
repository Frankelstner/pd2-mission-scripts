unit:
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'interact':
		Run sequence 'hide'.
	sequence 'hide':
		Hide graphic_group 'grp_eng_trophy'.
		Disable body 'body_static'.
		Disable interaction.
	sequence 'show':
		Show graphic_group 'grp_eng_trophy'.
		Enable body 'body_static'.
