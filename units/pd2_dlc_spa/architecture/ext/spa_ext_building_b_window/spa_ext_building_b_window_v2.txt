unit:
	window_01_broken = 0
	window_02_broken = 0
	window_03_broken = 0
	window_04_broken = 0
	window_05_broken = 0
	window_06_broken = 0
	window_07_broken = 0
	window_08_broken = 0
	window_09_broken = 0
	sequence 'crack_window_01':
		Disable object 'g_glass_01'.
		Enable object 'g_glass_dmg_01'.
		Cause alert with 12 m radius.
	sequence 'crack_window_02':
		Disable object 'g_glass_02'.
		Enable object 'g_glass_dmg_02'.
		Cause alert with 12 m radius.
	sequence 'crack_window_03':
		Disable object 'g_glass_03'.
		Enable object 'g_glass_dmg_03'.
		Cause alert with 12 m radius.
	sequence 'crack_window_04':
		Disable object 'g_glass_04'.
		Enable object 'g_glass_dmg_04'.
		Cause alert with 12 m radius.
	sequence 'crack_window_05':
		Disable object 'g_glass_05'.
		Enable object 'g_glass_dmg_05'.
		Cause alert with 12 m radius.
	sequence 'crack_window_06':
		Disable object 'g_glass_06'.
		Enable object 'g_glass_dmg_06'.
		Cause alert with 12 m radius.
	sequence 'crack_window_07':
		Disable object 'g_glass_07'.
		Enable object 'g_glass_dmg_07'.
		Cause alert with 12 m radius.
	sequence 'crack_window_08':
		Disable object 'g_glass_08'.
		Enable object 'g_glass_dmg_08'.
		Cause alert with 12 m radius.
	sequence 'crack_window_09':
		Disable object 'g_glass_09'.
		Enable object 'g_glass_dmg_09'.
		Cause alert with 12 m radius.
	sequence 'break_window_01':
		Disable body 'static_glass_01'.
		Disable object 'g_glass_dmg_01'.
		Disable decal_mesh 'dm_glass_breakable_01'.
		Play audio 'window_small_shatter' at 'e_glass_01'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_01')
			position v()
		Cause alert with 12 m radius.
		window_01_broken = 1
	sequence 'break_window_02':
		Disable body 'static_glass_02'.
		Disable object 'g_glass_dmg_02'.
		Disable decal_mesh 'dm_glass_breakable_02'.
		Play audio 'window_small_shatter' at 'e_glass_02'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_02')
			position v()
		Cause alert with 12 m radius.
		window_02_broken = 1
	sequence 'break_window_03':
		Disable body 'static_glass_03'.
		Disable object 'g_glass_dmg_03'.
		Disable decal_mesh 'dm_glass_breakable_03'.
		Play audio 'window_small_shatter' at 'e_glass_03'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_03')
			position v()
		Cause alert with 12 m radius.
		window_03_broken = 1
	sequence 'break_window_04':
		Disable body 'static_glass_04'.
		Disable object 'g_glass_dmg_04'.
		Disable decal_mesh 'dm_glass_breakable_04'.
		Play audio 'window_small_shatter' at 'e_glass_04'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_04')
			position v()
		Cause alert with 12 m radius.
		window_04_broken = 1
	sequence 'break_window_05':
		Disable body 'static_glass_05'.
		Disable object 'g_glass_dmg_05'.
		Disable decal_mesh 'dm_glass_breakable_05'.
		Play audio 'window_small_shatter' at 'e_glass_05'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_05')
			position v()
		Cause alert with 12 m radius.
		window_05_broken = 1
	sequence 'break_window_06':
		Disable body 'static_glass_06'.
		Disable object 'g_glass_dmg_06'.
		Disable decal_mesh 'dm_glass_breakable_06'.
		Play audio 'window_small_shatter' at 'e_glass_06'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_06')
			position v()
		Cause alert with 12 m radius.
		window_06_broken = 1
	sequence 'break_window_07':
		Disable body 'static_glass_07'.
		Disable object 'g_glass_dmg_07'.
		Disable decal_mesh 'dm_glass_breakable_07'.
		Play audio 'window_small_shatter' at 'e_glass_07'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_07')
			position v()
		Cause alert with 12 m radius.
		window_07_broken = 1
	sequence 'break_window_08':
		Disable body 'static_glass_08'.
		Disable object 'g_glass_dmg_08'.
		Disable decal_mesh 'dm_glass_breakable_08'.
		Play audio 'window_small_shatter' at 'e_glass_08'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_08')
			position v()
		Cause alert with 12 m radius.
		window_08_broken = 1
	sequence 'break_window_09':
		Disable body 'static_glass_09'.
		Disable object 'g_glass_dmg_09'.
		Disable decal_mesh 'dm_glass_breakable_09'.
		Play audio 'window_small_shatter' at 'e_glass_09'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object('e_glass_09')
			position v()
		Cause alert with 12 m radius.
		window_09_broken = 1
	body 'static_glass_01'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crack_window_01'.
		Upon receiving 4 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_window_01'.
	body 'static_glass_02'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crack_window_02'.
		Upon receiving 4 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_window_02'.
	body 'static_glass_03'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crack_window_03'.
		Upon receiving 4 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_window_03'.
	body 'static_glass_04'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crack_window_04'.
		Upon receiving 4 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_window_04'.
	body 'static_glass_05'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crack_window_05'.
		Upon receiving 4 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_window_05'.
	body 'static_glass_06'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crack_window_06'.
		Upon receiving 4 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_window_06'.
	body 'static_glass_07'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crack_window_07'.
		Upon receiving 4 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_window_07'.
	body 'static_glass_08'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crack_window_08'.
		Upon receiving 4 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_window_08'.
	body 'static_glass_09'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'crack_window_09'.
		Upon receiving 4 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'break_window_09'.
	body 'body_static_wood'
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'destroy_all'.
	sequence 'destroy_all':
		If window_01_broken == 0: Run sequence 'break_window_01'.
		If window_02_broken == 0: Run sequence 'break_window_02'.
		If window_03_broken == 0: Run sequence 'break_window_03'.
		If window_04_broken == 0: Run sequence 'break_window_04'.
		If window_05_broken == 0: Run sequence 'break_window_05'.
		If window_06_broken == 0: Run sequence 'break_window_06'.
		If window_07_broken == 0: Run sequence 'break_window_07'.
		If window_08_broken == 0: Run sequence 'break_window_08'.
		If window_09_broken == 0: Run sequence 'break_window_09'.
		Hide graphic_group 'glass_dmg'.
		Call function World:play_physic_effect('physic_effects/push_sphere',dest_unit:get_object( Idstring('e_glass') ),50,3).
		spawn_unit 'units/pd2_dlc_spa/architecture/ext/spa_ext_building_b_window/spa_spawn_window_debris_v2':
			position object_pos('rp_spa_ext_building_b_window_v2')
			rotation object_rot('rp_spa_ext_building_b_window_v2')
		Disable body 'body_static_wood'.
		Show graphic_group 'frame_broken'.
		Hide graphic_group 'window'.
		Hide graphic_group 'glass'.
		Hide graphic_group 'whole_shadow'.
