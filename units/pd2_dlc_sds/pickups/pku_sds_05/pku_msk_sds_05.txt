unit:
	sequence 'show_enable_interaction':
		Enable interaction.
		Enable object 'g_contour'.
		Enable object 'g_op'.
		Enable object 'g_g'.
		Enable object 'g_frame'.
		Enable body 'static_body'.
		Run sequence 'state_normal_material'.
	sequence 'hide_disable_interaction':
		Disable interaction.
		Disable object 'g_op'.
		Disable object 'g_g'.
		Disable object 'g_frame'.
		Disable body 'static_body'.
	sequence 'show_no_interaction':
		Enable object 'g_op'.
		Enable object 'g_g'.
		Enable object 'g_frame'.
		Enable body 'static_body'.
		Run sequence 'state_normal_material'.
		Disable interaction.
	sequence 'state_black_material':
		material_config 'units/pd2_dlc_sds/pickups/pku_sds_05/msk_sds_black_05'.
	sequence 'state_normal_material':
		material_config 'units/pd2_dlc_sds/pickups/pku_sds_05/msk_sds_05'.
	sequence 'interact':
		Run sequence 'hide_disable_interaction'.
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
