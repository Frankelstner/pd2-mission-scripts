unit:
	sequence 'make_dynamic':
		TRIGGER TIMES 1
		Disable body 'static_body'.
		Enable body 'dynamic_body'.
		slot:
			slot 18
