unit:
	sequence 'kill_office_railing_end_2_window_01':
		TRIGGER TIMES 1
		Enable object 'g_office_railing_end_2_window_dmg'.
		Disable object 'g_office_railing_end_2_window'.
	sequence 'kill_office_railing_end_2_window_02':
		TRIGGER TIMES 1
		Disable object 'g_office_railing_end_2_window_dmg'.
		Enable object 'g_office_railing_end_2_window_cracked'.
		Disable decal_mesh 'dm_glass'.
		effect 'effects/payday2/particles/window/storefront_window_small':
			parent object( 'e_office_railing_end_2_window' )
			position v()
		Play audio 'window_small_shatter' at 'e_office_railing_end_2_window'.
		Disable body 'office_railing_end_2_window'.
	body 'office_railing_end_2_window'
		Upon receiving 1 bullet hit or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'kill_office_railing_end_2_window_01'.
		Upon receiving 2 bullet hits or 10 explosion damage or 5 melee damage, execute:
			Run sequence 'kill_office_railing_end_2_window_02'.
