unit:
	sequence 'floor_1':
		Enable animation_group 'a_arrow':
			from 70/30
			loop False
			to 70/30
	sequence 'floor_3':
		Enable animation_group 'a_arrow':
			from 0
			loop False
			to 0
	sequence 'floor 1-2':
		Enable animation_group 'a_arrow':
			from 70/30
			loop False
			to 130/30
	sequence 'floor 3-2':
		Enable animation_group 'a_arrow':
			from 0
			loop False
			to 60/30
