unit:
	sequence 'hide_thermite_hole':
		Disable object 'g_thermite_hole'.
		Disable object 'g_vtxc'.
		Disable object 'g_glow'.
		Enable object 'g_fill'.
		Enable object 'g_ceiling_whole_vtxc'.
		Enable object 'g_ceiling_whole'.
		Enable decal_mesh 'dm_concrete_whole'.
		Disable body 'hole_body'.
		Enable body 'fill_body'.
		Disable decal_mesh 'dm_carpet_hole'.
		Enable decal_mesh 'dm_carpet_fill'.
	sequence 'show_thermite_hole':
		Enable object 'g_thermite_hole'.
		Enable object 'g_vtxc'.
		Enable object 'g_glow'.
		Disable object 'g_fill'.
		Disable object 'g_ceiling_whole_vtxc'.
		Disable object 'g_ceiling_whole'.
		Disable decal_mesh 'dm_concrete_whole'.
		Enable body 'hole_body'.
		Disable body 'fill_body'.
		Enable decal_mesh 'dm_concrete'.
		Enable decal_mesh 'dm_carpet_hole'.
		Disable decal_mesh 'dm_carpet_fill'.
