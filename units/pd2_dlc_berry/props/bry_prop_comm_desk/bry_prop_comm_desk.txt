unit:
	sequence 'state_cancelling_countdown':
		Enable object 'g_screen_cancelling_countdown'.
		Disable object 'g_screen_countdown_cancelled'.
		Disable object 'g_screen_neutral'.
	sequence 'state_restart_countdown':
		Disable object 'g_screen_cancelling_countdown'.
		Enable object 'g_screen_countdown_cancelled'.
		Disable object 'g_screen_neutral'.
	sequence 'state_neutral':
		Enable object 'g_screen_neutral'.
		Disable object 'g_screen_countdown_cancelled'.
		Disable object 'g_screen_cancelling_countdown'.
