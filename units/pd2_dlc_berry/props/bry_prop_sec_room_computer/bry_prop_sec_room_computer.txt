unit:
	sequence 'state_clearance':
		Enable object 'g_screen_clearance'.
		Disable object 'g_screen_approved'.
		Disable object 'g_screen_desk'.
	sequence 'state_approved':
		Enable object 'g_screen_approved'.
		Disable object 'g_screen_clearance'.
		Disable object 'g_screen_desk'.
	sequence 'state_desktop':
		Enable object 'g_screen_desk'.
		Disable object 'g_screen_clearance'.
		Disable object 'g_screen_approved'.
