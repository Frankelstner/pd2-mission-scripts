unit:
	sequence 'default':
		Enable object 'g_grill_ver1_lod0'.
		Enable object 'g_grill_ver1_lod1'.
		Disable object 'g_grill_ver2_lod0'.
		Disable object 'g_grill_ver2_lod1'.
		reset_editable_state True
	sequence 'grill_v2':
		Enable object 'g_grill_ver2_lod0'.
		Enable object 'g_grill_ver2_lod1'.
		Disable object 'g_grill_ver1_lod0'.
		Disable object 'g_grill_ver1_lod1'.
	sequence 'hide_ribbon':
		Disable object 'g_ribbon'.
	sequence 'show_ribbon':
		Enable object 'g_ribbon'.
	sequence 'play_fan_animation':
		Enable animation_group 'anim_fan_group':
			from 0/30
			loop True
			speed 1
			to 30/30
		Enable animation_group 'anim_ribbon_group':
			from 0/30
			loop True
			speed 1
			to 19/30
	sequence 'play_fan_animation_fast':
		Enable animation_group 'anim_fan_group':
			from 0/30
			loop True
			speed 2
			to 30/30
		Enable animation_group 'anim_ribbon_group':
			from 0/30
			loop True
			speed 2
			to 19/30
	sequence 'play_fan_animation_slow':
		Enable animation_group 'anim_fan_group':
			from 0/30
			loop True
			speed 0.2
			to 30/30
		Enable animation_group 'anim_ribbon_group':
			from 0/30
			loop True
			speed 0.2
			to 19/30
	sequence 'state_fan_still':
		Enable animation_group 'anim_fan_group':
			from 0/30
			to 0/30
		Enable animation_group 'anim_ribbon_group':
			from 0/30
			to 0/30
