unit:
	sequence 'enable_interaction':
		Enable interaction.
		Call function: interaction.set_tweak_data('hold_breaching_detonator')
		Enable object 'g_frame_outline'.
		Disable object 'g_frame'.
	sequence 'enable_rearm':
		Enable interaction.
		Call function: interaction.set_tweak_data('hold_breaching_detonator_rearm')
		Enable object 'g_frame_outline'.
		Enable object 'g_frame'.
	sequence 'hide':
		Disable object 'g_frame_outline'.
		Disable object 'g_frame'.
		Disable body 'body_static'.
		Disable interaction.
	sequence 'done_interaction':
		Disable interaction.
		Disable object 'g_frame_outline'.
		Enable object 'g_frame'.
	sequence 'interact':
		Run sequence 'done_interaction'.
