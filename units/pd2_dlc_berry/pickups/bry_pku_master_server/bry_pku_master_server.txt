unit:
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'hide':
		Disable interaction.
		Disable object 'g_circuit'.
		Disable decal_mesh 'g_circuit'.
		Disable body 'body_circuit'.
	sequence 'show':
		Enable object 'g_circuit'.
		Enable decal_mesh 'g_circuit'.
		Enable body 'body_circuit'.
	sequence 'show_and_enable_interaction':
		Enable interaction.
		Enable object 'g_circuit'.
		Enable decal_mesh 'g_circuit'.
		Enable body 'body_circuit'.
	sequence 'state_contour_enable':
		material_config 'units/pd2_dlc_berry/pickups/bry_pku_master_server/bry_pku_master_server'.
	sequence 'state_contour_disable':
		material_config 'units/pd2_dlc_berry/pickups/bry_pku_master_server/bry_pku_master_server_no_contour'.
	sequence 'interact':
		Run sequence 'hide'.
	sequence 'load'.
