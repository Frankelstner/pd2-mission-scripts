unit:
	sequence 'state_interaction_enabled':
		Enable interaction.
	sequence 'state_interaction_disabled':
		Disable interaction.
	sequence 'state_vis_hide':
		Disable interaction.
		Disable body 'body_static'.
		Disable body 'body_static_2'.
		Disable object 'g_light_green'.
		Disable object 'g_light_green_2'.
		Disable object 'g_light_red'.
		Disable object 'g_light_red_2'.
		Hide graphic_group 'grp'.
	sequence 'state_vis_show':
		Enable body 'body_static'.
		Enable body 'body_static_2'.
		Show graphic_group 'grp'.
	sequence 'red_light':
		Enable object 'g_light_red'.
		Enable object 'g_light_red_2'.
		Disable object 'g_light_green'.
		Disable object 'g_light_green_2'.
	sequence 'green_light':
		Enable object 'g_light_green'.
		Enable object 'g_light_green_2'.
		Disable object 'g_light_red'.
		Disable object 'g_light_red_2'.
	sequence 'lights_off':
		Disable object 'g_light_green'.
		Disable object 'g_light_green_2'.
		Disable object 'g_light_red'.
		Disable object 'g_light_red_2'.
	sequence 'interact':
		Run sequence 'state_vis_hide'.
	sequence 'load'.
