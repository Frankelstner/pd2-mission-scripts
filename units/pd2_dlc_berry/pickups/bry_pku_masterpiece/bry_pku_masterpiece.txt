unit:
	sequence 'enable_interaction':
		Enable interaction.
	sequence 'disable_interaction':
		Disable interaction.
	sequence 'hide':
		Disable interaction.
		Hide graphic_group 'grp_painting'.
		Disable decal_mesh 'dm_wood'.
		Disable decal_mesh 'dm_cloth'.
		Disable body 'body_static'.
	sequence 'show':
		Show graphic_group 'grp_painting'.
		Enable decal_mesh 'dm_wood'.
		Enable decal_mesh 'dm_cloth'.
		Enable body 'body_static'.
	sequence 'show_and_enable_interaction':
		Enable interaction.
		Run sequence 'show'.
	sequence 'interact'.
	sequence 'load'.
