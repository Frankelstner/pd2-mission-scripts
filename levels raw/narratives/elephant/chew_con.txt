ID range vs continent name:
	100000: world
	300000: collision
	400000: terrain

instances
	levels/instances/unique/chew/chew_train_car/001
		continent world
		index_size 2000
		position 0.0, -4400.0, 0.0
		rotation 0.0, 0.0, 0.0, -1.0
		script default
		start_index 0
	levels/instances/unique/chew/chew_train_car/002
		continent world
		index_size 2000
		position 0.0, -2200.0, 0.0
		rotation 0.0, 0.0, 0.0, -1.0
		script default
		start_index 2000
	levels/instances/unique/chew/chew_train_car/003
		continent world
		index_size 2000
		position 0.0, 0.0, 0.0
		rotation 0.0, 0.0, 0.0, -1.0
		script default
		start_index 4000
	levels/instances/unique/chew/chew_train_car/004
		continent world
		index_size 2000
		position 0.0, 4400.0, 0.0
		rotation 1.14049e-08, 0.0, -1.14049e-08, -1.0
		script default
		start_index 6000
	levels/instances/unique/chew/chew_train_car/005
		continent world
		index_size 2000
		position 0.0, 6600.0, 0.0
		rotation 0.0, 0.0, 0.0, -1.0
		script default
		start_index 8000
	levels/instances/unique/chew/chew_train_car/006
		continent world
		index_size 2000
		position 0.0, 8800.0, 0.0
		rotation 0.0, 0.0, 0.0, -1.0
		script default
		start_index 10000
	levels/instances/unique/chew/chew_tall_train/001
		continent world
		index_size 1500
		position -5.01816e-05, 2200.0, -5.01816e-05
		rotation 1.14049e-08, 0.0, -1.14049e-08, -1.0
		script default
		start_index 20000
	levels/instances/unique/chew/chew_tall_train/002
		continent world
		index_size 1500
		position -0.000250908, 11000.0, -0.000250908
		rotation 1.14049e-08, 0.0, -1.14049e-08, -1.0
		script default
		start_index 12000
	levels/instances/unique/chew/chew_heli_dropoff/001
		continent world
		index_size 250
		mission_placed True
		position 3000.0, -3400.0, 1675.0
		rotation 0.0, 0.0, 1.0, -7.45058e-08
		script default
		start_index 13500
	levels/instances/unique/chew/chew_heli_dropoff/002
		continent world
		index_size 250
		mission_placed True
		position 3000.0, -1000.0, 1675.0
		rotation 0.0, 0.0, 1.0, -7.45058e-08
		script default
		start_index 13750
	levels/instances/unique/chew/chew_heli_dropoff/003
		continent world
		index_size 250
		mission_placed True
		position 3000.0, 1000.0, 1675.0
		rotation 0.0, 0.0, 1.0, -7.45058e-08
		script default
		start_index 14000
	levels/instances/unique/chew/chew_heli_dropoff/004
		continent world
		index_size 250
		mission_placed True
		position 3000.0, 5400.0, 1675.0
		rotation 0.0, 0.0, 1.0, -7.45058e-08
		script default
		start_index 14250
	levels/instances/unique/chew/chew_heli_dropoff/005
		continent world
		index_size 250
		mission_placed True
		position 3000.0, 7800.0, 1675.0
		rotation 0.0, 0.0, 1.0, -7.45058e-08
		script default
		start_index 14500
	levels/instances/unique/chew/chew_heli_dropoff/006
		continent world
		index_size 250
		mission_placed True
		position 3000.0, 10200.0, 1675.0
		rotation 0.0, 0.0, 1.0, -7.45058e-08
		script default
		start_index 14750
	levels/instances/unique/chew/chew_heli_sniper/001
		continent world
		index_size 200
		position 2200.0, 400.0, -213.147
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 15000
	levels/instances/unique/chew/chew_pursuit_car/002
		continent world
		index_size 250
		position 1450.0, 975.0, -200.241
		rotation 0.0, 0.0, 1.0, 0.0
		script default
		start_index 15450
	levels/instances/unique/chew/chew_pursuit_car/003
		continent world
		index_size 250
		position 1450.0, 5525.0, -200.241
		rotation 0.0, 0.0, 1.0, 0.0
		script default
		start_index 15700
	levels/instances/unique/chew/chew_pursuit_car/004
		continent world
		index_size 250
		position 1450.0, 7600.0, -200.241
		rotation 0.0, 0.0, 1.0, 0.0
		script default
		start_index 15950
	levels/instances/unique/chew/chew_drum_stacks/002
		continent world
		index_size 150
		position 0.0, -2200.0, 0.0
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 16600
	levels/instances/unique/chew/chew_drum_stacks/003
		continent world
		index_size 150
		position 0.0, 0.0, 0.0
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 16750
	levels/instances/unique/chew/chew_drum_stacks/004
		continent world
		index_size 150
		position 0.0, 4400.0, 0.0
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 16900
	levels/instances/unique/chew/chew_drum_stacks/005
		continent world
		index_size 150
		position 0.0, 6600.0, 0.0
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 17050
	levels/instances/unique/chew/chew_drum_stacks/006
		continent world
		index_size 150
		position 0.0, 8800.0, 0.0
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 16450
	levels/instances/unique/chew/chew_weld_gate/002
		continent world
		index_size 150
		position 0.0, -2200.0, 0.0
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 18100
	levels/instances/unique/chew/chew_weld_gate/003
		continent world
		index_size 150
		position 0.0, 0.0, 0.0
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 17950
	levels/instances/unique/chew/chew_weld_gate/004
		continent world
		index_size 150
		position 0.0, 4400.0, 0.0
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 18250
	levels/instances/unique/chew/chew_weld_gate/005
		continent world
		index_size 150
		position 0.0, 6600.0, 0.0
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 18400
	levels/instances/unique/chew/chew_weld_gate/006
		continent world
		index_size 150
		position 0.0, 8800.0, 0.0
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 18550
	levels/instances/shared/obj_link/001
		continent world
		index_size 150
		position 4400.0, 8400.0, 6595.22
		rotation 0.0, 0.0, 0.707107, 0.707107
		script default
		start_index 18700
	levels/instances/shared/obj_link/002
		continent world
		index_size 150
		position 4400.0, 8800.0, 6595.22
		rotation 0.0, 0.0, 0.707107, 0.707107
		script default
		start_index 18850
	levels/instances/shared/obj_link/003
		continent world
		index_size 150
		position 4400.0, 9200.0, 6595.22
		rotation 0.0, 0.0, 0.707107, 0.707107
		script default
		start_index 19000
	levels/instances/shared/obj_link/004
		continent world
		index_size 150
		position 4400.0, 9600.0, 6595.22
		rotation 0.0, 0.0, 0.707107, 0.707107
		script default
		start_index 19150
	levels/instances/shared/obj_link/005
		continent world
		index_size 150
		position 4400.0, 10000.0, 6595.22
		rotation 0.0, 0.0, 0.707107, 0.707107
		script default
		start_index 19300
	levels/instances/unique/chew/chew_smuggle_crate/001
		continent world
		index_size 120
		position 7.0, 3110.0, 0.0
		rotation 0.0, 0.0, 1.0, 0.0
		script default
		start_index 19450
	levels/instances/unique/chew/chew_smuggle_crate/002
		continent world
		index_size 120
		position -211.932, 3512.65, 0.0
		rotation 0.0, 0.0, 0.737278, -0.67559
		script default
		start_index 19570
	levels/instances/unique/chew/chew_smuggle_crate/007
		continent world
		index_size 120
		position -66.663, 2529.84, 0.0
		rotation 0.0, 0.0, -0.92388, 0.382684
		script default
		start_index 21740
	levels/instances/unique/chew/chew_smuggle_crate/008
		continent world
		index_size 120
		position 202.087, 2797.43, 0.0
		rotation 0.0, 0.0, 0.642788, 0.766044
		script default
		start_index 21860
	levels/instances/unique/chew/chew_smuggle_crate/009
		continent world
		index_size 120
		position 205.989, 2456.74, 0.0
		rotation 0.0, 0.0, 0.67559, 0.737277
		script default
		start_index 21980
	levels/instances/unique/chew/chew_smuggle_crate/010
		continent world
		index_size 120
		position 199.0, 2633.0, 0.0
		rotation 0.0, 0.0, 0.748956, 0.66262
		script default
		start_index 22100
	levels/instances/unique/chew/chew_smuggle_crate/013
		continent world
		index_size 120
		position -202.0, 3939.0, 0.0
		rotation 0.0, 0.0, 0.707107, -0.707107
		script default
		start_index 22460
	levels/instances/unique/chew/chew_smuggle_crate/014
		continent world
		index_size 120
		position -213.0, 3703.0, 0.0
		rotation 0.0, 0.0, 0.707107, -0.707107
		script default
		start_index 22580
	levels/instances/unique/chew/chew_smuggle_crate/016
		continent world
		index_size 120
		position 94.0, 3844.0, 0.0
		rotation 0.0, 0.0, -0.707107, -0.707107
		script default
		start_index 22820
	levels/instances/unique/chew/chew_heli_sniper/002
		continent world
		index_size 200
		position 5100.0, 8200.0, -213.147
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 23180
	levels/instances/unique/chew/chew_heli_sniper/003
		continent world
		index_size 200
		position -4400.0, 400.0, -213.147
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 23380
	levels/instances/unique/chew/chew_heli_sniper/004
		continent world
		index_size 200
		position -1900.0, 7600.0, -213.147
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 23580
	levels/instances/unique/chew/chew_swat_van/001
		continent world
		index_size 150
		mission_placed True
		position 1500.0, -87400.0, -195.0
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 17200
	levels/instances/unique/chew/chew_pursuit_car/007
		continent world
		index_size 250
		position 2650.0, 8080.0, -200.241
		rotation 0.0, 0.0, 1.0, 0.0
		script default
		start_index 17600
	levels/instances/unique/chew/chew_pursuit_car/008
		continent world
		index_size 250
		position 2650.0, 5980.0, -200.241
		rotation 0.0, 0.0, 1.0, 0.0
		script default
		start_index 23780
	levels/instances/unique/chew/chew_pursuit_car/009
		continent world
		index_size 250
		position 2650.0, 2280.0, -200.241
		rotation 0.0, 0.0, 1.0, 0.0
		script default
		start_index 24030
	levels/instances/unique/chew/chew_ladder_blocker/001
		continent world
		index_size 300
		position 0.0, -2200.0, 0.0
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 24280
	levels/instances/unique/chew/chew_ladder_blocker/002
		continent world
		index_size 300
		position 0.0, 0.0, 0.0
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 24580
	levels/instances/unique/chew/chew_ladder_blocker/003
		continent world
		index_size 300
		position 0.0, 4400.0, 0.0
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 24880
	levels/instances/unique/chew/chew_ladder_blocker/004
		continent world
		index_size 300
		position 0.0, 6600.0, 0.0
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 25180
	levels/instances/unique/chew/chew_ladder_blocker/005
		continent world
		index_size 300
		position 0.0, 8800.0, 0.0
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 25480
statics
	100396 units/dev_tools/level_tools/ai_coverpoint/001 (100.0, 3500.0, 698.518)
	101017 units/dev_tools/level_tools/ai_coverpoint/002 (-100.0, 3500.0, 698.518)
	101021 units/dev_tools/level_tools/ai_coverpoint/003 (100.0, 2900.0, 698.518)
	101022 units/dev_tools/level_tools/ai_coverpoint/004 (-100.0, 2900.0, 698.518)
	101026 units/dev_tools/level_tools/ai_coverpoint/005 (-200.0, 2300.0, 698.535)
	101027 units/dev_tools/level_tools/ai_coverpoint/006 (200.0, 2300.0, 698.535)
	101031 units/dev_tools/level_tools/ai_coverpoint/007 (-200.0, 4100.0, 698.535)
	101032 units/dev_tools/level_tools/ai_coverpoint/008 (200.0, 4100.0, 698.535)
	101036 units/dev_tools/level_tools/ai_coverpoint/009 (200.0, 12900.0, 698.535)
	101037 units/dev_tools/level_tools/ai_coverpoint/010 (-200.0, 12900.0, 698.535)
	101041 units/dev_tools/level_tools/ai_coverpoint/011 (100.0, 12300.0, 698.518)
	101042 units/dev_tools/level_tools/ai_coverpoint/012 (-100.0, 12300.0, 698.518)
	101046 units/dev_tools/level_tools/ai_coverpoint/013 (-100.0, 11700.0, 698.518)
	101047 units/dev_tools/level_tools/ai_coverpoint/014 (100.0, 11700.0, 698.518)
	101051 units/dev_tools/level_tools/ai_coverpoint/015 (-200.0, 11100.0, 698.535)
	101137 units/dev_tools/level_tools/ai_coverpoint/016 (200.0, 11100.0, 698.535)
	100393 units/dev_tools/level_tools/dev_collision_1x1m/001 (-71.0, 12796.0, 52.5485)
	100394 units/dev_tools/level_tools/dev_collision_1x1m/002 (-144.0, 12796.0, 52.5485)
	100365 units/dev_tools/level_tools/dev_door_blocker_1x1x3/004 (-136.0, 12201.0, -0.000272854)
	100364 units/dev_tools/level_tools/dev_nav_blocker_1x3m/001 (150.0, 12232.0, 0.982314)
	100366 units/dev_tools/level_tools/dev_nav_blocker_1x3m/002 (50.0, 12232.0, 0.982314)
	100367 units/dev_tools/level_tools/dev_nav_blocker_1x3m/003 (-22.0, 12232.0, 0.982314)
	100642 units/dev_tools/level_tools/dev_nav_blocker_1x3m/004 (-243.0, 12232.0, 0.982314)
	100721 units/payday2/equipment/gen_interactable_weapon_case_2x1_z_axis/001 (-188.845, 12737.8, 78.9823)
		disable_on_ai_graph True
		disable_shadows True
	100722 units/payday2/equipment/gen_interactable_weapon_case_2x1_z_axis/002 (134.691, 12778.6, 78.9823)
		disable_on_ai_graph True
		disable_shadows True
	100729 units/payday2/equipment/gen_interactable_weapon_case_2x1_z_axis/003 (156.191, 12541.4, 0.9823)
		disable_on_ai_graph True
		disable_shadows True
	100730 units/payday2/equipment/gen_interactable_weapon_case_2x1_z_axis/004 (-150.547, 12372.0, 0.9823)
		disable_on_ai_graph True
		disable_shadows True
	100731 units/payday2/equipment/gen_interactable_weapon_case_2x1_z_axis/005 (210.191, 12638.0, 0.9823)
		disable_on_ai_graph True
		disable_shadows True
	100732 units/payday2/equipment/gen_interactable_weapon_case_2x1_z_axis/006 (-196.744, 12491.4, 0.9823)
		disable_on_ai_graph True
		disable_shadows True
	100733 units/payday2/equipment/gen_interactable_weapon_case_2x1_z_axis/007 (-200.688, 12604.4, 0.9823)
		disable_on_ai_graph True
		disable_shadows True
	100624 units/payday2/props/gen_interactable_panel_keycard/001 (-167.0, 12213.0, 105.982)
		mesh_variation state_5
	100606 units/payday2/props/gen_prop_plywood/001 (6.0, 11483.0, 351.255)
	100610 units/payday2/props/gen_prop_plywood/002 (67.0, 12213.0, 348.255)
	100716 units/payday2/props/gen_prop_plywood/003 (232.0, 12234.0, 0.255005)
	100757 units/payday2/props/gen_prop_plywood/004 (240.0, 3433.0, 1.255)
	100609 units/payday2/props/gen_prop_plywood_v2/002 (-221.0, 12581.0, 348.255)
	100608 units/payday2/props/gen_prop_plywood_v3/001 (-236.0, 11775.0, 348.255)
	100338 units/payday2/props/gen_prop_square_goal_marker_8x15/001 (142.0, -4000.0, -4.0)
	100589 units/payday2/props/set/ind_prop_warehouse_box_stack_4/001 (-196.0, 3700.0, 345.818)
	100603 units/payday2/props/set/ind_prop_warehouse_box_stack_4/002 (56.0, 3565.0, 345.818)
	100611 units/payday2/props/set/ind_prop_warehouse_box_stack_4/003 (-199.0, 12365.0, 352.818)
	100612 units/payday2/props/set/ind_prop_warehouse_box_stack_4/004 (-86.0, 12365.0, 347.818)
	100622 units/payday2/props/set/ind_prop_warehouse_box_stack_4/005 (-199.0, 12607.0, 348.818)
	100456 units/payday2/props/set/ind_prop_warehouse_box_stack_4/006 (-64.0, 11612.0, -0.182007)
	100750 units/payday2/props/set/ind_prop_warehouse_box_stack_4/007 (26.0, 3311.0, 1.81799)
	100755 units/payday2/props/set/ind_prop_warehouse_box_stack_4/008 (95.0, 3550.0, -4.18201)
	100913 units/payday2/props/set/ind_prop_warehouse_box_stack_4/009 (-211.0, 2666.0, 19.818)
	100342 units/payday2/props/set/ind_prop_warehouse_box_stack_4/010 (-200.0, 2832.0, 0.818001)
	100588 units/payday2/props/set/ind_prop_warehouse_box_stack_8/001 (-163.0, 3836.0, 347.255)
	100592 units/payday2/props/set/ind_prop_warehouse_box_stack_8/002 (-38.4282, 3702.39, 347.255)
	100615 units/payday2/props/set/ind_prop_warehouse_box_stack_8/003 (-131.428, 11343.4, 347.255)
	100616 units/payday2/props/set/ind_prop_warehouse_box_stack_8/004 (-164.65, 11670.6, 370.255)
	100618 units/payday2/props/set/ind_prop_warehouse_box_stack_8/005 (141.572, 11643.4, 350.255)
	100752 units/payday2/props/set/ind_prop_warehouse_box_stack_8/006 (165.08, 3695.74, 0.255005)
	100917 units/payday2/props/set/ind_prop_warehouse_box_stack_8/007 (-118.0, 2696.0, 16.212)
	100939 units/payday2/props/set/ind_prop_warehouse_box_stack_8/009 (169.877, 2491.88, 366.212)
	100944 units/payday2/props/set/ind_prop_warehouse_box_stack_8/010 (-180.123, 2727.88, 347.212)
	100617 units/payday2/props/set/ind_prop_warehouse_pallet_blue/002 (-163.624, 11669.5, 352.755)
	100758 units/payday2/props/set/ind_prop_warehouse_pallet_blue/003 (197.376, 4077.5, 0.755005)
	100900 units/payday2/props/set/ind_prop_warehouse_pallet_blue/004 (197.376, 2727.5, 350.755)
	100905 units/payday2/props/set/ind_prop_warehouse_pallet_blue/005 (-202.624, 2690.5, 0.755005)
	100912 units/payday2/props/set/ind_prop_warehouse_pallet_blue/006 (-99.6236, 2690.5, 0.755005)
	100935 units/payday2/props/set/ind_prop_warehouse_pallet_blue/007 (164.376, 2473.5, 350.755)
	100940 units/payday2/props/set/ind_prop_warehouse_pallet_blue/008 (164.376, 2576.5, 350.755)
	100590 units/payday2/props/set/ind_prop_warehouse_pallet_stack_a/001 (151.0, 3685.0, 347.255)
	100607 units/payday2/props/set/ind_prop_warehouse_pallet_stack_a/002 (179.0, 12500.0, 347.255)
	100614 units/payday2/props/set/ind_prop_warehouse_pallet_stack_a/003 (-166.0, 12700.0, 347.255)
	100538 units/payday2/props/set/ind_prop_warehouse_pallet_stack_a/005 (0.0, 3211.0, 1.255)
	100591 units/payday2/props/set/ind_prop_warehouse_pallet_stack_e/001 (-162.0, 3534.0, 347.255)
	100604 units/payday2/props/set/ind_prop_warehouse_pallet_stack_e/002 (-2.99998, 11504.0, 0.255005)
	100605 units/payday2/props/set/ind_prop_warehouse_pallet_stack_e/003 (-178.0, 11545.0, 349.255)
	100751 units/payday2/props/set/ind_prop_warehouse_pallet_stack_e/004 (188.0, 3534.0, 1.255)
	100930 units/payday2/props/set/ind_prop_warehouse_pallet_stack_e/005 (-162.0, 2934.0, 347.255)
	100934 units/payday2/props/set/ind_prop_warehouse_pallet_stack_e/006 (-162.0, 2834.0, 347.255)
	100945 units/pd2_dlc_chew/architecture/chw_occluder_01/001 (0.0, 0.0, 0.0)
	100537 units/pd2_dlc_chew/props/chw_prop_cargocrate_a/001 (-143.0, 2539.0, 347.752)
	100668 units/pd2_dlc_chew/props/chw_prop_cargocrate_a/002 (-124.0, -3877.0, 8.3078)
	100669 units/pd2_dlc_chew/props/chw_prop_cargocrate_a/003 (-124.0, -4077.0, 8.3078)
	100672 units/pd2_dlc_chew/props/chw_prop_cargocrate_a/004 (-153.0, -3093.0, 8.3078)
	100918 units/pd2_dlc_chew/props/chw_prop_cargocrate_b/001 (136.0, 3884.0, 348.493)
	100671 units/pd2_dlc_chew/props/chw_prop_cargocrate_b/002 (-152.0, -3300.0, 8.3078)
	100665 units/pd2_dlc_chew/props/chw_prop_cargocrate_b/003 (179.0, -2800.0, 8.3078)
	100539 units/pd2_dlc_chew/props/chw_prop_cargocrate_b/004 (26.0, 12684.0, 348.493)
	100459 units/pd2_dlc_chew/props/chw_prop_drone_crate/001 (-35.0, 12857.0, 77.7478)
		mesh_variation close_crate
	100389 units/pd2_dlc_chew/props/chw_prop_shipping_box_pallet/001 (-218.215, 12370.7, 0.982307)
	100736 units/pd2_dlc_chew/props/chw_prop_shipping_box_pallet/002 (-210.215, 11276.7, 0.982307)
	100737 units/pd2_dlc_chew/props/chw_prop_shipping_box_pallet/003 (-210.215, 11276.7, 74.9823)
	100715 units/pd2_dlc_chew/props/chw_prop_shipping_box_pallet/004 (-213.0, 12381.0, 74.9823)
	100738 units/pd2_dlc_chew/props/chw_prop_shipping_box_pallet/005 (-128.215, 11276.7, 0.982307)
	100666 units/pd2_dlc_chew/props/chw_prop_shipping_box_pallet/006 (-6.215, -3144.3, 0.982307)
	100667 units/pd2_dlc_chew/props/chw_prop_shipping_box_pallet/007 (-6.215, -3272.3, 0.982307)
	100670 units/pd2_dlc_chew/props/chw_prop_shipping_box_pallet/008 (-6.215, -3735.3, 0.982307)
	100673 units/pd2_dlc_chew/props/chw_prop_shipping_box_pallet/009 (-135.215, -3727.3, 0.982307)
	100674 units/pd2_dlc_chew/props/chw_prop_shipping_box_pallet/010 (-135.215, -3727.3, 75.9823)
	100401 units/pd2_dlc_chew/props/chw_prop_train_coupling_cables/001 (0.0, 2100.0, -75.0)
	100402 units/pd2_dlc_chew/props/chw_prop_train_coupling_cables/002 (0.0, -100.0, -75.0)
	100405 units/pd2_dlc_chew/props/chw_prop_train_coupling_cables/003 (0.0, -2300.0, -75.0)
	100406 units/pd2_dlc_chew/props/chw_prop_train_coupling_cables/004 (0.0, 4300.0, -75.0)
	100407 units/pd2_dlc_chew/props/chw_prop_train_coupling_cables/005 (0.0, 6500.0, -75.0)
	100408 units/pd2_dlc_chew/props/chw_prop_train_coupling_cables/006 (0.0, 8700.0, -75.0)
	100413 units/pd2_dlc_chew/props/chw_prop_train_coupling_cables/007 (0.0, 10900.0, -75.0)
	100414 units/pd2_dlc_chew/props/chw_prop_train_locomotive_hi_res/001 (-0.000281632, 14747.0, -132.788)
	100641 units/pd2_dlc_chew/props/chw_security_gate/001 (-132.0, 12213.0, 0.982314)
		disable_on_ai_graph True
	101043 units/pd2_dlc_chew/vehicles/anim_vehicle_pickup_armored_fbi/002 (1450.0, 100991.0, -191.674)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 0.65
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_left
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
		2
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 0.65
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_right
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
	101044 units/pd2_dlc_chew/vehicles/anim_vehicle_pickup_armored_fbi/003 (1450.0, 105541.0, -191.674)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 0.65
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_left
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
		2
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 0.65
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_right
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
	101045 units/pd2_dlc_chew/vehicles/anim_vehicle_pickup_armored_fbi/004 (1450.0, 107616.0, -191.674)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 0.65
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_left
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
		2
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 0.65
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_right
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
	101048 units/pd2_dlc_chew/vehicles/anim_vehicle_pickup_armored_fbi/007 (2650.0, 108096.0, -191.674)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 0.65
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_left
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
		2
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 0.65
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_right
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
	101049 units/pd2_dlc_chew/vehicles/anim_vehicle_pickup_armored_fbi/008 (2650.0, 105996.0, -191.674)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 0.65
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_left
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
		2
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 0.65
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_right
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
	101050 units/pd2_dlc_chew/vehicles/anim_vehicle_pickup_armored_fbi/009 (2650.0, 102296.0, -191.674)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 0.65
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_left
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
		2
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 0.65
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_right
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
	100289 units/pd2_dlc_chew/vehicles/chw_vehicle_helicopter_blackhawk/001 (370.0, -3428.0, 189.847)
	100457 units/pd2_dlc_cro/train_prop_cargo_bomb/ind_case/001 (136.0, 12775.0, 0.982305)
	100458 units/pd2_dlc_cro/train_prop_cargo_bomb/ind_case/002 (-181.0, 12739.0, 0.982305)
	100723 units/pd2_dlc_cro/train_prop_cargo_bomb/ind_case/003 (-43.0, 12838.0, 0.982305)
	100724 units/pd2_dlc_cro/train_prop_cargo_bomb/ind_case/004 (182.0, 12937.0, -12.0177)
	100725 units/pd2_dlc_cro/train_prop_cargo_bomb/ind_case/005 (182.0, 12937.0, 68.9823)
	100726 units/pd2_dlc_cro/train_prop_cargo_bomb/ind_case/006 (-10.0, 12937.0, -12.0177)
	100727 units/pd2_dlc_cro/train_prop_cargo_bomb/ind_case/007 (-10.0, 12937.0, 68.9823)
	100551 units/pd2_dlc_cro/train_prop_cargo_bomb/ind_case/008 (-172.0, 12937.0, -12.0177)
	100714 units/pd2_dlc_cro/train_prop_cargo_bomb/ind_case/009 (-172.0, 12930.0, 68.9823)
	101052 units/pd2_dlc_drive/vehicles/helicopter_cops_turret/001 (-29261.0, -109655.0, 1437.02)
	100619 units/pd2_indiana/props/mus_prop_exhibit_crate/001 (200.0, 11322.0, 347.446)
	100620 units/pd2_indiana/props/mus_prop_exhibit_crate/002 (94.0, 11349.0, 347.446)
	100621 units/pd2_indiana/props/mus_prop_exhibit_crate/003 (177.0, 12612.0, 347.446)
	100717 units/pd2_indiana/props/mus_prop_exhibit_crate/012 (202.0, 11091.0, -2.55399)
	100720 units/pd2_indiana/props/mus_prop_exhibit_crate/013 (202.0, 11091.0, 97.446)
	100739 units/pd2_indiana/props/mus_prop_exhibit_crate/014 (51.0, 12136.0, -2.55399)
	100756 units/pd2_indiana/props/mus_prop_exhibit_crate/015 (204.0, 4075.0, 18.446)
	100765 units/pd2_indiana/props/mus_prop_exhibit_crate/016 (204.0, 4075.0, 118.446)
	100719 units/pd2_indiana/props/mus_prop_exhibit_crate_lid/001 (25.0, 12328.0, 0.982315)
		disable_on_ai_graph True
	100735 units/pd2_indiana/props/mus_prop_exhibit_crate_lid/002 (230.056, 11658.7, 71.9823)
	100754 units/pd2_indiana/props/mus_prop_exhibit_crate_lid/003 (146.204, 3940.27, -0.0177002)
	100923 units/pd2_indiana/props/mus_prop_exhibit_crate_lid/004 (-213.796, 2610.27, -0.0177002)
	100925 units/pd2_indiana/props/mus_prop_exhibit_crate_lid/005 (-170.754, 2771.75, 124.54)
	100718 units/pd2_indiana/props/mus_prop_exhibit_crate_open/001 (102.481, 12479.6, 0.982314)
	100734 units/pd2_indiana/props/mus_prop_exhibit_crate_open/002 (157.956, 11777.3, 0.982314)
	100753 units/pd2_indiana/props/mus_prop_exhibit_crate_open/003 (144.104, 3913.87, 0.982314)
	100922 units/pd2_indiana/props/mus_prop_exhibit_crate_open/004 (-99.0673, 2273.94, 0.982308)
	100929 units/pd2_indiana/props/mus_prop_exhibit_crate_open/005 (-148.327, 2382.01, 0.982333)
	300050 units/dev_tools/level_tools/dev_bag_collision_8x32m/001 (-458.0, 11021.0, 691.651)
	300051 units/dev_tools/level_tools/dev_bag_collision_8x32m/002 (-458.0, 4203.0, 691.651)
	300052 units/dev_tools/level_tools/dev_bag_collision_8x32m/003 (-458.0, 2219.0, 691.651)
	300057 units/dev_tools/level_tools/dev_bag_collision_8x3m/005 (-325.0, 11800.0, 341.871)
	300056 units/dev_tools/level_tools/dev_bag_collision_8x3m/006 (-325.0, 11800.0, 341.871)
	300058 units/dev_tools/level_tools/dev_bag_collision_8x3m/007 (-325.0, 11500.0, 341.871)
	300059 units/dev_tools/level_tools/dev_bag_collision_8x3m/008 (-325.0, 11225.0, 341.871)
	300060 units/dev_tools/level_tools/dev_bag_collision_8x3m/009 (-325.0, 12800.0, 341.871)
	300061 units/dev_tools/level_tools/dev_bag_collision_8x3m/010 (-325.0, 12500.0, 341.871)
	300062 units/dev_tools/level_tools/dev_bag_collision_8x3m/011 (-325.0, 12800.0, 341.871)
	300063 units/dev_tools/level_tools/dev_bag_collision_8x3m/012 (-325.0, 12225.0, 341.871)
	300064 units/dev_tools/level_tools/dev_bag_collision_8x3m/013 (-325.0, 3000.0, 341.871)
	300065 units/dev_tools/level_tools/dev_bag_collision_8x3m/014 (-325.0, 3000.0, 341.871)
	300066 units/dev_tools/level_tools/dev_bag_collision_8x3m/015 (-325.0, 2700.0, 341.871)
	300067 units/dev_tools/level_tools/dev_bag_collision_8x3m/016 (-325.0, 2425.0, 341.871)
	300068 units/dev_tools/level_tools/dev_bag_collision_8x3m/017 (-325.0, 4000.0, 341.871)
	300069 units/dev_tools/level_tools/dev_bag_collision_8x3m/018 (-325.0, 4000.0, 341.871)
	300070 units/dev_tools/level_tools/dev_bag_collision_8x3m/019 (-325.0, 3700.0, 341.871)
	300071 units/dev_tools/level_tools/dev_bag_collision_8x3m/020 (-325.0, 3425.0, 341.871)
	300053 units/dev_tools/level_tools/dev_collision_4x3m/003 (282.0, -3700.0, -148.667)
	300054 units/dev_tools/level_tools/dev_collision_4x3m/004 (282.0, -3700.0, 451.333)
	300055 units/dev_tools/level_tools/dev_collision_4x3m/005 (282.0, -3700.0, 151.333)
	300000 units/dev_tools/level_tools/dev_collision_8x32m/003 (-318.0, 12200.0, -189.677)
	300001 units/dev_tools/level_tools/dev_collision_8x32m/004 (-318.0, 9000.0, -189.677)
	300002 units/dev_tools/level_tools/dev_collision_8x32m/005 (-318.0, 5800.0, -189.677)
	300003 units/dev_tools/level_tools/dev_collision_8x32m/006 (-318.0, 2600.0, -189.677)
	300004 units/dev_tools/level_tools/dev_collision_8x32m/007 (-318.0, -114.0, -189.677)
	300005 units/dev_tools/level_tools/dev_collision_8x32m/008 (-318.0, -3303.0, -189.677)
	300006 units/dev_tools/level_tools/dev_collision_8x32m/009 (282.0, -168.0, -189.677)
	300007 units/dev_tools/level_tools/dev_collision_8x32m/010 (282.0, -3618.0, -189.677)
	300008 units/dev_tools/level_tools/dev_collision_8x32m/011 (282.0, 2600.0, -189.677)
	300009 units/dev_tools/level_tools/dev_collision_8x32m/012 (282.0, 5800.0, -189.677)
	300010 units/dev_tools/level_tools/dev_collision_8x32m/013 (282.0, 9000.0, -189.677)
	300011 units/dev_tools/level_tools/dev_collision_8x32m/014 (282.0, 12200.0, -189.677)
	300012 units/dev_tools/level_tools/dev_collision_8x32m/015 (-418.0, -600.0, -0.677299)
	300013 units/dev_tools/level_tools/dev_collision_8x32m/016 (-418.0, -4462.0, -202.677)
	300014 units/dev_tools/level_tools/dev_collision_8x32m/017 (-418.0, 2600.0, -0.677299)
	300015 units/dev_tools/level_tools/dev_collision_8x32m/018 (-418.0, 5800.0, -0.677299)
	300016 units/dev_tools/level_tools/dev_collision_8x32m/019 (-418.0, 9000.0, -0.677299)
	300017 units/dev_tools/level_tools/dev_collision_8x32m/020 (-418.0, 12200.0, -0.677299)
	300018 units/dev_tools/level_tools/dev_collision_8x32m/021 (282.0, 13020.0, -189.677)
	300019 units/dev_tools/level_tools/dev_collision_8x32m/022 (484.0, -4003.0, -202.677)
	300020 units/dev_tools/level_tools/dev_collision_8x32m/023 (-318.0, 15400.0, -189.677)
	300021 units/dev_tools/level_tools/dev_collision_8x32m/024 (282.0, 15400.0, -189.677)
	300022 units/dev_tools/level_tools/dev_collision_8x32m/025 (282.0, 15400.0, 610.323)
	300023 units/dev_tools/level_tools/dev_collision_8x32m/026 (-318.0, 15400.0, 610.323)
	300024 units/dev_tools/level_tools/dev_collision_8x32m/027 (282.0, 12200.0, 610.323)
	300025 units/dev_tools/level_tools/dev_collision_8x32m/028 (-318.0, 12200.0, 610.323)
	300026 units/dev_tools/level_tools/dev_collision_8x32m/029 (-318.0, 9000.0, 610.323)
	300027 units/dev_tools/level_tools/dev_collision_8x32m/030 (-318.0, 5800.0, 610.323)
	300028 units/dev_tools/level_tools/dev_collision_8x32m/031 (-318.0, 2600.0, 610.323)
	300029 units/dev_tools/level_tools/dev_collision_8x32m/032 (-318.0, -114.0, 610.323)
	300030 units/dev_tools/level_tools/dev_collision_8x32m/033 (-318.0, -3303.0, 610.323)
	300031 units/dev_tools/level_tools/dev_collision_8x32m/034 (282.0, -3800.0, 610.323)
	300032 units/dev_tools/level_tools/dev_collision_8x32m/035 (282.0, -600.0, 610.323)
	300033 units/dev_tools/level_tools/dev_collision_8x32m/036 (282.0, 2600.0, 610.323)
	300034 units/dev_tools/level_tools/dev_collision_8x32m/037 (282.0, 5800.0, 610.323)
	300035 units/dev_tools/level_tools/dev_collision_8x32m/038 (282.0, 9000.0, 610.323)
	300036 units/dev_tools/level_tools/dev_collision_8x32m/039 (282.0, 15400.0, 1410.32)
	300037 units/dev_tools/level_tools/dev_collision_8x32m/040 (-318.0, 15400.0, 1410.32)
	300038 units/dev_tools/level_tools/dev_collision_8x32m/041 (282.0, 12200.0, 1410.32)
	300039 units/dev_tools/level_tools/dev_collision_8x32m/042 (-318.0, 12200.0, 1410.32)
	300040 units/dev_tools/level_tools/dev_collision_8x32m/043 (-318.0, 9000.0, 1410.32)
	300041 units/dev_tools/level_tools/dev_collision_8x32m/044 (-318.0, 5800.0, 1410.32)
	300042 units/dev_tools/level_tools/dev_collision_8x32m/045 (-318.0, 2600.0, 1410.32)
	300043 units/dev_tools/level_tools/dev_collision_8x32m/046 (-318.0, -114.0, 1410.32)
	300044 units/dev_tools/level_tools/dev_collision_8x32m/047 (-318.0, -3303.0, 1410.32)
	300045 units/dev_tools/level_tools/dev_collision_8x32m/048 (282.0, -3800.0, 1410.32)
	300046 units/dev_tools/level_tools/dev_collision_8x32m/049 (282.0, -600.0, 1410.32)
	300047 units/dev_tools/level_tools/dev_collision_8x32m/050 (282.0, 2600.0, 1410.32)
	300048 units/dev_tools/level_tools/dev_collision_8x32m/051 (282.0, 5800.0, 1410.32)
	300049 units/dev_tools/level_tools/dev_collision_8x32m/052 (282.0, 9000.0, 1410.32)
	300072 units/payday2/architecture/mkp_int_floor_2x2m_a/006 (-275.0, 3800.0, 675.0)
	300074 units/payday2/architecture/mkp_int_floor_2x2m_a/007 (-275.0, 4000.0, 675.0)
	300075 units/payday2/architecture/mkp_int_floor_2x2m_a/008 (-275.0, 3400.0, 675.0)
	300076 units/payday2/architecture/mkp_int_floor_2x2m_a/009 (-275.0, 3600.0, 675.0)
	300078 units/payday2/architecture/mkp_int_floor_2x2m_a/010 (-275.0, 3000.0, 675.0)
	300079 units/payday2/architecture/mkp_int_floor_2x2m_a/011 (-275.0, 3200.0, 675.0)
	300081 units/payday2/architecture/mkp_int_floor_2x2m_a/012 (-275.0, 2600.0, 675.0)
	300082 units/payday2/architecture/mkp_int_floor_2x2m_a/013 (-275.0, 2800.0, 675.0)
	300084 units/payday2/architecture/mkp_int_floor_2x2m_a/014 (-275.0, 2200.0, 675.0)
	300085 units/payday2/architecture/mkp_int_floor_2x2m_a/015 (-275.0, 2400.0, 675.0)
	300087 units/payday2/architecture/mkp_int_floor_2x2m_a/016 (-275.0, 11000.0, 675.0)
	300088 units/payday2/architecture/mkp_int_floor_2x2m_a/017 (-275.0, 11200.0, 675.0)
	300090 units/payday2/architecture/mkp_int_floor_2x2m_a/018 (-275.0, 11400.0, 675.0)
	300091 units/payday2/architecture/mkp_int_floor_2x2m_a/019 (-275.0, 11600.0, 675.0)
	300093 units/payday2/architecture/mkp_int_floor_2x2m_a/020 (-275.0, 11800.0, 675.0)
	300094 units/payday2/architecture/mkp_int_floor_2x2m_a/021 (-275.0, 12000.0, 675.0)
	300096 units/payday2/architecture/mkp_int_floor_2x2m_a/022 (-275.0, 12200.0, 675.0)
	300097 units/payday2/architecture/mkp_int_floor_2x2m_a/023 (-275.0, 12400.0, 675.0)
	300099 units/payday2/architecture/mkp_int_floor_2x2m_a/024 (-275.0, 12600.0, 675.0)
	300100 units/payday2/architecture/mkp_int_floor_2x2m_a/025 (-275.0, 12800.0, 675.0)
	300073 units/payday2/architecture/mkp_int_floor_4x4m_a/002 (-125.0, 3800.0, 675.0)
	300077 units/payday2/architecture/mkp_int_floor_4x4m_a/003 (-125.0, 3400.0, 675.0)
	300080 units/payday2/architecture/mkp_int_floor_4x4m_a/004 (-125.0, 3000.0, 675.0)
	300083 units/payday2/architecture/mkp_int_floor_4x4m_a/005 (-125.0, 2600.0, 675.0)
	300086 units/payday2/architecture/mkp_int_floor_4x4m_a/006 (-125.0, 2200.0, 675.0)
	300089 units/payday2/architecture/mkp_int_floor_4x4m_a/007 (-125.0, 11000.0, 675.0)
	300092 units/payday2/architecture/mkp_int_floor_4x4m_a/008 (-125.0, 11400.0, 675.0)
	300095 units/payday2/architecture/mkp_int_floor_4x4m_a/009 (-125.0, 11800.0, 675.0)
	300098 units/payday2/architecture/mkp_int_floor_4x4m_a/010 (-125.0, 12200.0, 675.0)
	300101 units/payday2/architecture/mkp_int_floor_4x4m_a/011 (-125.0, 12600.0, 675.0)
	400048 units/pd2_dlc_chew/effects/chw_wind_tunnel/002 (0.0, 4200.0, -165.788)
	400003 units/pd2_dlc_chew/nature/chw_train_terrain/003 (2700.0, -0.000793457, -200.0)
		disable_on_ai_graph True
	400004 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/021 (-120.04, -21600.0, -175.0)
	400005 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/022 (-120.05, -26600.0, -175.0)
	400006 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/023 (-120.05, -31600.0, -175.0)
	400007 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/024 (-120.06, -36600.0, -175.0)
	400008 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/025 (-120.07, -41600.0, -175.0)
	400009 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/026 (-120.08, -46600.0, -175.0)
	400010 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/027 (-120.08, -51600.0, -175.0)
	400011 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/028 (-120.09, -56600.0, -175.0)
	400012 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/029 (-120.1, -61600.0, -175.0)
	400013 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/030 (-120.11, -66600.0, -175.0)
	400014 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/031 (-120.04, -21600.0, -175.0)
	400015 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/032 (-120.05, -26600.0, -175.0)
	400016 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/033 (-120.05, -31600.0, -175.0)
	400017 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/034 (-120.06, -36600.0, -175.0)
	400018 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/035 (-120.07, -41600.0, -175.0)
	400019 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/036 (-120.08, -46600.0, -175.0)
	400020 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/037 (-120.08, -51600.0, -175.0)
	400021 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/038 (-120.09, -56600.0, -175.0)
	400022 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/039 (-120.1, -61600.0, -175.0)
	400023 units/pd2_dlc_chew/nature/foliage/chw_veg_desert_bushes/040 (-120.11, -66600.0, -175.0)
	400024 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/020 (-120.04, -21600.0, -175.0)
	400025 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/022 (-120.05, -26600.0, -175.0)
	400026 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/023 (-120.05, -31600.0, -175.0)
	400027 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/024 (-120.06, -36600.0, -175.0)
	400028 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/025 (-120.07, -41600.0, -175.0)
	400029 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/026 (-120.08, -46600.0, -175.0)
	400030 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/027 (-120.08, -51600.0, -175.0)
	400031 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/028 (-120.09, -56600.0, -175.0)
	400032 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/029 (-120.1, -61600.0, -175.0)
	400033 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/030 (-120.11, -66600.0, -175.0)
	400034 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/031 (-120.04, -21600.0, -175.0)
	400035 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/032 (-120.05, -26600.0, -175.0)
	400036 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/033 (-120.05, -31600.0, -175.0)
	400037 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/034 (-120.06, -36600.0, -175.0)
	400038 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/035 (-120.07, -41600.0, -175.0)
	400039 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/036 (-120.08, -46600.0, -175.0)
	400040 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/037 (-120.08, -51600.0, -175.0)
	400041 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/038 (-120.09, -56600.0, -175.0)
	400042 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/039 (-120.1, -61600.0, -175.0)
	400043 units/pd2_dlc_chew/nature/rocks/chw_desert_rocks/040 (-120.11, -66600.0, -175.0)
	400044 units/pd2_dlc_chew/nature/rocks/chw_train_props_rocks/001 (2700.0, -0.000793457, -200.0)
	400045 units/pd2_dlc_chew/nature/rocks/chw_train_props_rocks/002 (2700.0, -0.000793457, -200.0)
	400001 units/pd2_dlc_chew/nature/terrain/chw_train_props_01/003 (2700.0, -0.000793457, -200.0)
	400002 units/pd2_dlc_chew/nature/terrain/chw_train_props_01/004 (2700.0, -0.000793457, -200.0)
	400046 units/pd2_dlc_chew/nature/terrain/chw_train_props_02/004 (2700.0, -0.000793457, -200.0)
	400047 units/pd2_dlc_chew/nature/terrain/chw_train_props_02/005 (2700.0, -0.000793457, -200.0)
