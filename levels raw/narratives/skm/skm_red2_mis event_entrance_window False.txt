﻿´SO_AI_search002´ ElementSpecialObjective 101041
	TRIGGER TIMES 1
	SO_access 12252
	action_duration_max 0
	action_duration_min 0
	ai_group enemies
	align_position False
	align_rotation False
	attitude engage
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis 7
	interrupt_dmg 0
	interrupt_objective False
	interval 1
	is_navigation_link False
	needs_pos_rsrv True
	path_haste walk
	path_stance cbt
	path_style precise
	patrol_path so_event_entrance_window
	pose crouch
	position -2875.0, 1050.0, 19.9997
	repeatable False
	rotation 0.0, 0.0, 0.707107, -0.707107
	scan True
	search_distance 0
	search_position -2923.62, 1140.1, 5.0039
	so_action AI_search
	trigger_on none
	use_instigator True
