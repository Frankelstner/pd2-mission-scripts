﻿´start_helicopter_cafeteria_2´ MissionScriptElement 101913
	on_executed
		´flyin´ (delay 0)
		´start_crackin´ (delay 26)
		´point_debug_120´ (delay 0)
´flyin´ ElementUnitSequence 101979
	position -1100.0, -1600.0, 1700.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/vehicles/helicopter/ranger/helicopter_cops/001 (-530.0, -1952.0, 457.0)
			notify_unit_sequence swat
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/vehicles/helicopter/ranger/helicopter_cops/001 (-530.0, -1952.0, 457.0)
			notify_unit_sequence flyin_right_hover
			time 0
´start_crackin´ MissionScriptElement 101983
	on_executed
		´spawn_dudes´ (delay 10)
		´env_effect_play_013´ (delay 0)
´spawn_dudes´ MissionScriptElement 101984
	BASE DELAY 7
	on_executed
		´flyout´ (delay 16)
		´hide_heli´ (delay 35)
		´open_door´ (delay 0)
		´ai_spawn_enemy_017´ (delay 1)
		´ai_spawn_enemy_016´ (delay 2)
		´ai_spawn_enemy_019´ (delay 5)
		´ai_spawn_enemy_018´ (delay 6)
		´func_execute_in_other_mission_014´ (delay 0)
		´break_win9´ (delay 3)
´flyout´ ElementUnitSequence 101985
	position -1000.0, -2200.0, 1700.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/vehicles/helicopter/ranger/helicopter_cops/001 (-530.0, -1952.0, 457.0)
			notify_unit_sequence hover_flyout_back
			time 0
	on_executed
		´env_effect_stop_009´ (delay 0)
´break_win9´ ElementUnitSequence 101986
	position -1300.0, -1900.0, 1700.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_red/props/windows/red_prop_building_window_03/001 (-584.989, -2409.96, 925.001)
			notify_unit_sequence destroy_02_win9
			time 0
	on_executed
		´point_teammate_comment_chopper´ (delay 0)
		´point_debug_122´ (delay 0)
´point_teammate_comment_chopper´ ElementTeammateComment 103490
	close_to_element True
	comment p41
	position -1400.0, -2100.0, 475.0
	radius 1000
	rotation 0.0, 0.0, -0.707107, -0.707107
	use_instigator False
´hide_heli´ ElementUnitSequence 102006
	position -1000.0, -2400.0, 1700.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/vehicles/helicopter/ranger/helicopter_cops/001 (-530.0, -1952.0, 457.0)
			notify_unit_sequence hidden
			time 0
´point_debug_120´ ElementDebug 101865
	as_subtitle False
	debug_string start_helicopter_cafeteria
	show_instigator False
´ai_spawn_enemy_016´ ElementSpawnEnemyDummy 102382
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_tazer_1/ene_tazer_1
	force_pickup none
	interval 5
	participate_to_group_ai False
	position -900.0, -1650.0, 475.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	spawn_action e_sp_down_17m_var2
	team default
	voice 0
	on_executed
		´point_special_objective_001´ (delay 0)
´env_effect_play_013´ ElementPlayEffect 102383
	base_time 0
	effect effects/particles/dest/bridge_heli_smoke
	max_amount 0
	position -900.0, -1700.0, 1601.11
	random_time 0
	rotation 0.0, 0.0, 0.0, -1.0
	screen_space False
´env_effect_stop_009´ ElementStopEffect 102402
	elements
		1 ´env_effect_play_013´
	operation fade_kill
	position -1100.0, -2200.0, 1700.0
	rotation 0.0, 0.0, 0.0, -1.0
´ai_spawn_enemy_017´ ElementSpawnEnemyDummy 103535
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_fbi_swat_2/ene_fbi_swat_2
	force_pickup none
	interval 5
	participate_to_group_ai False
	position -811.0, -1626.0, 475.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	spawn_action e_sp_down_17m_var2
	team default
	voice 0
	on_executed
		´point_special_objective_001´ (delay 0)
´open_door´ ElementUnitSequence 106875
	position -1100.0, -2000.0, 1700.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/vehicles/helicopter/ranger/helicopter_cops/001 (-530.0, -1952.0, 457.0)
			notify_unit_sequence open_door_right
			time 0
´point_special_objective_001´ ElementSpecialObjective 106876
	SO_access 0
	action_duration_max 0
	action_duration_min 0
	ai_group enemies
	align_position False
	align_rotation False
	allow_followup_self False
	attitude engage
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis 0
	interrupt_dmg 1
	interrupt_objective False
	interval -1
	needs_pos_rsrv False
	path_haste run
	path_stance cbt
	path_style destination
	patrol_path none
	pose none
	position -850.0, -1500.0, 750.0
	rotation 0.0, 0.0, 0.0, -1.0
	scan False
	search_distance 0
	search_position -850.0, -1500.0, 500.0
	so_action AI_hunt
	trigger_on none
	use_instigator True
´ai_spawn_enemy_018´ ElementSpawnEnemyDummy 101967
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_tazer_1/ene_tazer_1
	force_pickup none
	interval 5
	participate_to_group_ai False
	position -812.0, -1577.0, 475.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	spawn_action e_sp_down_17m_var2
	team default
	voice 0
	on_executed
		´point_special_objective_001´ (delay 0)
´ai_spawn_enemy_019´ ElementSpawnEnemyDummy 106855
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_fbi_swat_2/ene_fbi_swat_2
	force_pickup none
	interval 5
	participate_to_group_ai False
	position -900.0, -1592.0, 475.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	spawn_action e_sp_down_17m_var2
	team default
	voice 0
	on_executed
		´point_special_objective_001´ (delay 0)
´point_debug_122´ ElementDebug 106860
	as_subtitle False
	debug_string shatter
	show_instigator False
´func_execute_in_other_mission_014´ ElementExecuteInOtherMission 106864
	position -900.0, -2000.0, 1700.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´enable_spawn_skylight´ (delay 0)
´ai_enemy_trigger_015´ ElementEnemyDummyTrigger 101875
	TRIGGER TIMES 1
	elements
		1 ´ai_spawn_enemy_017´
	event spawn
	position -1100.0, -1600.0, 475.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´break_win9´ (delay 1)
