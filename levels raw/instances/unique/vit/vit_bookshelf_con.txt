ID range vs continent name:
	100000: world

statics
	100077 units/pd2_dlc_arena/props/are_prop_security_button/001 (-38.0, 24.0, 117.0)
		disable_on_ai_graph True
		hide_on_projection_light True
	100078 units/pd2_dlc_arena/props/are_prop_security_button/002 (55.0, 24.0, 114.0)
		disable_on_ai_graph True
		hide_on_projection_light True
	100079 units/pd2_dlc_arena/props/are_prop_security_button/003 (-65.0, 24.0, 158.0)
		disable_on_ai_graph True
		hide_on_projection_light True
	100080 units/pd2_dlc_arena/props/are_prop_security_button/004 (74.0, 24.0, 159.0)
		disable_on_ai_graph True
		hide_on_projection_light True
	100034 units/pd2_dlc_arena/props/are_prop_security_button/005 (-49.0, 24.0, 197.0)
		disable_on_ai_graph True
		hide_on_projection_light True
	100035 units/pd2_dlc_arena/props/are_prop_security_button/006 (61.0, 24.0, 195.0)
		disable_on_ai_graph True
		hide_on_projection_light True
	100008 units/pd2_dlc_vit/architecture/vit_int/trims/vit_int_wall_book_shelf_1_m/001 (0.0, 0.0, 0.0)
		disable_on_ai_graph True
		hide_on_projection_light True
	100090 units/pd2_dlc_vit/architecture/vit_int/trims/vit_int_wall_book_shelf_2_m/001 (-100.0, 0.0, 0.0)
		disable_on_ai_graph True
		hide_on_projection_light True
	100098 units/pd2_dlc_vit/equipment/vit_interactable_books_01/001 (-53.0, 20.0, 96.0)
		disable_on_ai_graph True
		hide_on_projection_light True
		mesh_variation disable_interaction
	100012 units/pd2_dlc_vit/equipment/vit_interactable_books_01/002 (-45.0, 19.0, 181.0)
		disable_on_ai_graph True
		hide_on_projection_light True
		mesh_variation disable_interaction
	100009 units/pd2_dlc_vit/equipment/vit_interactable_books_02/001 (64.0, 17.0, 96.0)
		disable_on_ai_graph True
		hide_on_projection_light True
		mesh_variation disable_interaction
	100013 units/pd2_dlc_vit/equipment/vit_interactable_books_02/002 (62.0, 22.0, 181.0)
		disable_on_ai_graph True
		hide_on_projection_light True
		mesh_variation disable_interaction
	100010 units/pd2_dlc_vit/equipment/vit_interactable_books_03/001 (75.0, 20.0, 138.0)
		disable_on_ai_graph True
		hide_on_projection_light True
		mesh_variation disable_interaction
	100011 units/pd2_dlc_vit/equipment/vit_interactable_books_04/001 (-48.0, 18.0, 139.0)
		disable_on_ai_graph True
		hide_on_projection_light True
		mesh_variation disable_interaction
