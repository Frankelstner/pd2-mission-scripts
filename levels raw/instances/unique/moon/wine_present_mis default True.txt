﻿´test´ MissionScriptElement 100002
	EXECUTE ON STARTUP
	DISABLED
	on_executed
		´test_spawn_player´ (delay 0)
		´test_delay´ (delay 1)
´test_spawn_player´ ElementPlayerSpawner 100003
	position 0.0, -200.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	state standard
´test_delay´ MissionScriptElement 100004
	on_executed
		´show_box´ (delay 0)
		´enable_interaction´ (delay 1)
		´toggle´ (delay 0)
´setup´ MissionScriptElement 100005
	EXECUTE ON STARTUP
	BASE DELAY 0.5
	on_executed
		´seq_hider´ (delay 0)
´seq_hider´ ElementUnitSequence 100006
	position 800.0, 200.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/props/gifts/com_prop_christmas_gift_multi/001 (0.0, 0.0, 0.0)
			notify_unit_sequence state_vis_hide
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_moon/props/moo_props_store_boxes_expensive_wine/001 (-21.0, -5.0, 9.0)
			notify_unit_sequence hide_disable_interaction
			time 0
	on_executed
		´disabler´ (delay 0)
´input_show_box´ ElementInstanceInput 100007
	event show_box
	on_executed
		´show_box´ (delay 0)
´show_box´ ElementUnitSequence 100008
	position 900.0, 200.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/props/gifts/com_prop_christmas_gift_multi/001 (0.0, 0.0, 0.0)
			notify_unit_sequence state_vis_show
			time 0
´enable_interaction´ ElementUnitSequence 100009
	position 1000.0, 200.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/props/gifts/com_prop_christmas_gift_multi/001 (0.0, 0.0, 0.0)
			notify_unit_sequence state_interaction_enable_no_lid
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/props/gifts/com_prop_christmas_gift_multi/001 (0.0, 0.0, 0.0)
			notify_unit_sequence state_outline_enabled
			time 0
´input_enable_interaction´ ElementInstanceInput 100010
	event enable_interaction
	on_executed
		´enable_interaction´ (delay 0)
´WP_wine´ ElementWaypoint 100011
	icon pd2_generic_interact
	only_in_civilian False
	position 0.0, 0.0, 50.0
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´opened_box´ ElementUnitSequenceTrigger 100012
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/props/gifts/com_prop_christmas_gift_multi/001 (0.0, 0.0, 0.0)
	on_executed
		´show_wine´ (delay 0) DISABLED
		´random_trash´ (delay 0)
´show_wine´ ElementUnitSequence 100013
	DISABLED
	position 1100.0, -100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_moon/props/moo_props_store_boxes_expensive_wine/001 (-21.0, -5.0, 9.0)
			notify_unit_sequence show_enable_interaction
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_moon/props/moo_props_store_boxes_expensive_wine/001 (-21.0, -5.0, 9.0)
			notify_unit_sequence show_outline
			time 0
	on_executed
		´WP_wine´ (delay 0)
		´output_found_wine´ (delay 0)
´took_wine´ ElementUnitSequenceTrigger 100014
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/pd2_dlc_moon/props/moo_props_store_boxes_expensive_wine/001 (-21.0, -5.0, 9.0)
	on_executed
		´remove_WP_wine´ (delay 0)
		´output_took_wine´ (delay 0)
		´disable_WP_wine´ (delay 0)
´remove_WP_wine´ ElementOperator 100015
	elements
		1 ´WP_wine´
	operation remove
´output_took_wine´ ElementInstanceOutput 100016
	event took_wine
´input_enable_wine´ ElementInstanceInput 100017
	event enable_wine
	on_executed
		´toggle´ (delay 0)
´toggle´ ElementToggle 100018
	elements
		1 ´show_wine´ DISABLED
		2 ´random_trash´
	set_trigger_times -1
	toggle toggle
	on_executed
		´point_debug_001´ (delay 0)
´input_disable_interaction´ ElementInstanceInput 100019
	event disable_interaction
	on_executed
		´disable_interaction´ (delay 0)
´disable_interaction´ ElementUnitSequence 100020
	position 1200.0, 200.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/props/gifts/com_prop_christmas_gift_multi/001 (0.0, 0.0, 0.0)
			notify_unit_sequence state_interact_disable
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/props/gifts/com_prop_christmas_gift_multi/001 (0.0, 0.0, 0.0)
			notify_unit_sequence state_outline_disabled
			time 0
´output_found_wine´ ElementInstanceOutput 100021
	event found_wine
´point_debug_001´ ElementDebug 100022
	as_subtitle False
	debug_string from instance: input_enable_wine
	show_instigator False
´disable_WP_wine´ ElementToggle 100023
	elements
		1 ´WP_wine´
	set_trigger_times -1
	toggle off
´disabler´ ElementDisableUnit 100024
	position 800.0, 100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/world/props/bottle_3/001 (-18.0, -11.0, 6.0)
		2 units/world/props/bottle_3/002 (-7.0, -11.0, 6.0)
		3 units/world/props/bottle_3/003 (4.0, -11.0, 6.0)
		4 units/world/props/bottle_3/004 (15.0, -10.0, 6.0)
		5 units/world/props/bottle_5/001 (-25.0, -20.0, 7.0)
		6 units/world/props/bottle_5/002 (-15.0, -20.0, 7.0)
		7 units/world/props/bottle_5/003 (-5.0, -19.0, 7.0)
		8 units/world/props/bottle_5/004 (5.0, -19.0, 7.0)
		9 units/world/props/bottle_5/005 (15.0, -19.0, 7.0)
		10 units/world/props/bottle_5/006 (25.0, -18.0, 7.0)
		11 units/world/props/bottle_7/001 (-17.0, -14.0, 7.0)
		12 units/world/props/bottle_7/002 (-4.0, -13.0, 7.0)
		13 units/world/props/bottle_7/003 (11.0, -11.0, 7.0)
´random_trash´ ElementRandom 100029
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´func_enable_unit_001´ (delay 0)
		´func_enable_unit_002´ (delay 0)
		´func_enable_unit_003´ (delay 0)
´func_enable_unit_001´ ElementEnableUnit 100030
	position 900.0, -400.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/world/props/bottle_3/001 (-18.0, -11.0, 6.0)
		2 units/world/props/bottle_3/002 (-7.0, -11.0, 6.0)
		3 units/world/props/bottle_3/003 (4.0, -11.0, 6.0)
		4 units/world/props/bottle_3/004 (15.0, -10.0, 6.0)
´func_enable_unit_002´ ElementEnableUnit 100031
	position 1000.0, -400.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/world/props/bottle_5/001 (-25.0, -20.0, 7.0)
		2 units/world/props/bottle_5/002 (-15.0, -20.0, 7.0)
		3 units/world/props/bottle_5/003 (-5.0, -19.0, 7.0)
		4 units/world/props/bottle_5/004 (5.0, -19.0, 7.0)
		5 units/world/props/bottle_5/005 (15.0, -19.0, 7.0)
		6 units/world/props/bottle_5/006 (25.0, -18.0, 7.0)
´func_enable_unit_003´ ElementEnableUnit 100032
	position 1100.0, -400.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/world/props/bottle_7/001 (-17.0, -14.0, 7.0)
		2 units/world/props/bottle_7/002 (-4.0, -13.0, 7.0)
		3 units/world/props/bottle_7/003 (11.0, -11.0, 7.0)
