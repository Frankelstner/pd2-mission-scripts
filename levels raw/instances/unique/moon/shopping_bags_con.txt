ID range vs continent name:
	100000: world

statics
	100024 units/payday2/props/com_prop_barista_coffee_bag_a/content1 (-14.0, -3.0, 0.0)
		disable_on_ai_graph True
		hide_on_projection_light True
	100025 units/payday2/props/com_prop_barista_coffee_bag_a/content1 (10.0, -1.0, 20.0)
		disable_on_ai_graph True
		hide_on_projection_light True
	100028 units/payday2/props/com_prop_barista_coffee_bag_b/content1 (9.0, -6.0, 0.0)
		disable_on_ai_graph True
		hide_on_projection_light True
	100027 units/payday2/props/com_prop_barista_coffee_bag_c/content1 (16.0, 3.0, 0.0)
		disable_on_ai_graph True
		hide_on_projection_light True
	100026 units/payday2/props/com_prop_barista_coffee_bag_c/content1 (3.0, 2.0, 0.0)
		disable_on_ai_graph True
		hide_on_projection_light True
	100029 units/payday2/props/com_prop_jewelry_bag/jewelry_shoebox_single_3/content2 (7.0, -7.0, 21.0)
		disable_on_ai_graph True
		hide_on_projection_light True
	100032 units/payday2/props/com_prop_store_wines_champagne/content3 (-1.0, 4.76837e-07, 1.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100031 units/payday2/props/com_prop_store_wines_champagne/content3 (-20.0, 2.0, 1.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100034 units/payday2/props/com_prop_store_wines_champagne/content3 (23.0, 3.0, 1.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100000 units/pd2_dlc_moon/props/moo_prop_shopping_bag/001 (0.0, 0.0, 0.0)
		disable_on_ai_graph True
		hide_on_projection_light True
	100013 units/pd2_dlc_moon/props/moo_props_store_boxes_tin_boy/tin_boy_fake (20.0, -4.0, 10.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100010 units/pd2_dlc_moon/props/moo_props_store_boxes_tin_boy/tin_boy_real (20.0, -4.0, 30.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100036 units/world/props/plushies/toy_teddy_small/content3 (-9.0, -5.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
