﻿´logic_link_001´ MissionScriptElement 100015
	EXECUTE ON STARTUP
	TRIGGER TIMES 1
	on_executed
		´hide´ (delay 1)
		´logic_link_002´ (delay 2)
´spikes´ MissionScriptElement 100016
	TRIGGER TIMES 1
	on_executed
		´spikes_2´ (delay 0)
		´func_enable_unit_001´ (delay 0)
		´func_sequence_014´ (delay 0)
		´gen_block´ (delay 0)
´police_car´ MissionScriptElement 100017
	TRIGGER TIMES 1
	on_executed
		´police_car_2´ (delay 0)
		´func_sequence_001´ (delay 0)
		´func_nav_obstacle_002´ (delay 0)
		´cops´ (delay 0)
´swat_car´ MissionScriptElement 100018
	TRIGGER TIMES 1
	on_executed
		´swat_car_2´ (delay 0)
		´func_sequence_002´ (delay 0)
		´func_nav_obstacle_003´ (delay 0)
		´swat´ (delay 0)
´bollards´ MissionScriptElement 100019
	TRIGGER TIMES 1
	on_executed
		´bollards_2´ (delay 0)
		´func_enable_unit_002´ (delay 0)
		´func_nav_obstacle_004´ (delay 0)
		´cops´ (delay 0)
		´gen_block´ (delay 0)
´cover_hole´ MissionScriptElement 100020
	TRIGGER TIMES 1
	on_executed
		´cover_hole_2´ (delay 0)
		´func_enable_unit_003´ (delay 0)
		´func_sequence_004´ (delay 0)
		´gen_block´ (delay 0)
		´func_instance_output_002´ (delay 0)
´event_arrived´ ElementInstanceInput 100021
	TRIGGER TIMES 1
	event arrived
	on_executed
		´logic_operator_001´ (delay 0)
		´logic_link_004´ (delay 0.1)
´spikes_2´ ElementCounter 100022
	counter_target 2
	digital_gui_unit_ids
	on_executed
		´allow_interaction´ (delay 0)
		´point_waypoint_003´ (delay 0)
		´point_waypoint_004´ (delay 0)
		´func_objective_002´ (delay 0)
´police_car_2´ ElementCounter 100023
	counter_target 2
	digital_gui_unit_ids
	on_executed
		´func_sequence_006´ (delay 0)
		´point_waypoint_008´ (delay 0)
		´func_objective_003´ (delay 0)
´swat_car_2´ ElementCounter 100024
	counter_target 2
	digital_gui_unit_ids
	on_executed
		´func_sequence_007´ (delay 0)
		´point_waypoint_005´ (delay 0)
		´func_objective_004´ (delay 0)
´bollards_2´ ElementCounter 100025
	counter_target 2
	digital_gui_unit_ids
	on_executed
		´func_sequence_005´ (delay 0)
		´point_waypoint_006´ (delay 0)
		´func_objective_005´ (delay 0)
´cover_hole_2´ ElementCounter 100026
	counter_target 2
	digital_gui_unit_ids
	on_executed
		´func_sequence_003´ (delay 0)
		´point_waypoint_007´ (delay 0) DISABLED
		´func_objective_006´ (delay 0)
		´func_instance_output_001´ (delay 0)
´allow_interaction´ ElementUnitSequence 100027
	position 200.0, 1300.0, 300.0
	rotation 0.0, 0.0, 0.707107, -0.707107
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_road_spikes/001 (-700.0, 1300.0, -16.9991)
			notify_unit_sequence state_interaction_enabled
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_road_spikes/002 (-1000.0, 1350.0, -16.9991)
			notify_unit_sequence state_interaction_enabled
			time 0
´event_done´ ElementInstanceOutput 100028
	event done
´done_moved002´ ElementUnitSequenceTrigger 100029
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/equipment/gen_interactable_road_spikes/002 (-1000.0, 1350.0, -16.9991)
	on_executed
		´both_moved´ (delay 0)
		´logic_operator_004´ (delay 0)
´done_moved001´ ElementUnitSequenceTrigger 100030
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/equipment/gen_interactable_road_spikes/001 (-700.0, 1300.0, -16.9991)
	on_executed
		´both_moved´ (delay 0)
		´logic_operator_005´ (delay 0)
´both_moved´ ElementCounter 100031
	counter_target 2
	digital_gui_unit_ids
	on_executed
		´logic_link_003´ (delay 0)
		´gen_unblock´ (delay 0)
´hide´ ElementDisableUnit 100036
	position 400.0, 300.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/pd2_dlc2/architecture/str_ext_road_c_straight_a_16x16m_hole/001 (-1600.0, 1600.0, 0.0)
		2 units/payday2/props/str_prop_street_cone/001 (-619.0, 930.0, -19.9997)
		3 units/payday2/props/str_prop_street_cone/002 (-967.0, 893.0, -19.9997)
		4 units/payday2/props/str_prop_street_cone/003 (-1082.0, 995.0, -19.9997)
		5 units/payday2/props/str_prop_street_cone/004 (-555.0, 863.0, -19.9997)
		6 units/payday2/props/str_prop_street_cone/005 (-550.0, 1450.0, -19.9997)
		7 units/payday2/props/str_prop_street_cone/006 (-600.0, 1600.0, -19.9997)
		8 units/payday2/props/str_prop_street_cone/007 (-1050.0, 1450.0, -19.9996)
	on_executed
		´hide_2´ (delay 0)
´hide_2´ ElementUnitSequence 100037
	position 500.0, 300.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small/001 (-779.488, 1495.68, 138.0)
			notify_unit_sequence state_hide
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small/001 (-779.488, 1495.68, 138.0)
			notify_unit_sequence deactivate
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/props/str_prop_street_crashbarrier/001 (-500.0, 1350.0, -19.9991)
			notify_unit_sequence state_hide
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/payday2/props/str_prop_street_crashbarrier/002 (-800.0, 1350.0, -19.9991)
			notify_unit_sequence state_hide
			time 0
		5
			id 5
			name run_sequence
			notify_unit_id units/payday2/props/str_prop_street_crashbarrier/003 (-1100.0, 1350.0, -19.9991)
			notify_unit_sequence state_hide
			time 0
		6
			id 6
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_van_swat/001 (-775.0, 1600.0, -23.9991)
			notify_unit_sequence state_vis_hide
			time 0
		7
			id 7
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_car_police/001 (-825.0, 1600.0, -21.9991)
			notify_unit_sequence state_vis_hide
			time 0
		8
			id 8
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_road_spikes/001 (-700.0, 1300.0, -16.9991)
			notify_unit_sequence state_vis_hide
			time 0
		9
			id 9
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_road_spikes/002 (-1000.0, 1350.0, -16.9991)
			notify_unit_sequence state_vis_hide
			time 0
		10
			id 10
			name run_sequence
			notify_unit_id units/payday2/props/str_prop_street_electric_box_hackable/001 (-250.0, 1450.0, 0.0)
			notify_unit_sequence state_hide
			time 0
		11
			id 11
			name run_sequence
			notify_unit_id units/pd2_dlc2/architecture/ext_road_c/str_construction_metal_sheet_interactable/001 (-126.0, 2824.0, 6.00003)
			notify_unit_sequence state_hide
			time 0
´func_enable_unit_001´ ElementEnableUnit 100038
	position 200.0, 800.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
´func_sequence_001´ ElementUnitSequence 100039
	position 300.0, 800.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_car_police/001 (-825.0, 1600.0, -21.9991)
			notify_unit_sequence state_vis_show
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_car_police/001 (-825.0, 1600.0, -21.9991)
			notify_unit_sequence state_police_officers_off
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_car_police/001 (-825.0, 1600.0, -21.9991)
			notify_unit_sequence state_lights_siren_on
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_car_police/001 (-825.0, 1600.0, -21.9991)
			notify_unit_sequence anim_door_left_open
			time 0
´func_sequence_002´ ElementUnitSequence 100040
	position 400.0, 800.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_van_swat/001 (-775.0, 1600.0, -23.9991)
			notify_unit_sequence state_vis_show
			time 0
´func_enable_unit_002´ ElementEnableUnit 100041
	position 500.0, 900.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
´func_enable_unit_003´ ElementEnableUnit 100042
	position 600.0, 800.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/pd2_dlc2/architecture/str_ext_road_c_straight_a_16x16m_hole/001 (-1600.0, 1600.0, 0.0)
		2 units/payday2/props/str_prop_street_cone/001 (-619.0, 930.0, -19.9997)
		3 units/payday2/props/str_prop_street_cone/002 (-967.0, 893.0, -19.9997)
		4 units/payday2/props/str_prop_street_cone/003 (-1082.0, 995.0, -19.9997)
		5 units/payday2/props/str_prop_street_cone/004 (-555.0, 863.0, -19.9997)
		6 units/payday2/props/str_prop_street_cone/005 (-550.0, 1450.0, -19.9997)
		7 units/payday2/props/str_prop_street_cone/006 (-600.0, 1600.0, -19.9997)
		8 units/payday2/props/str_prop_street_cone/007 (-1050.0, 1450.0, -19.9996)
	on_executed
		´func_disable_unit_003´ (delay 0)
´func_disable_unit_003´ ElementDisableUnit 100043
	position 600.0, 900.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/pd2_dlc2/architecture/str_ext_road_c_straight_a_16x16m/002 (0.0, 0.0, 0.0)
´func_sequence_003´ ElementUnitSequence 100045
	position 600.0, 1300.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/dev_tools/dev_interaction_use_bridge/001 (-800.0, 1200.0, -19.9997)
			notify_unit_sequence enable_interaction
			time 0
´func_sequence_004´ ElementUnitSequence 100047
	position 600.0, 1000.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
´func_sequence_trigger_001´ ElementUnitSequenceTrigger 100048
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/dev_tools/dev_interaction_use_bridge/001 (-800.0, 1200.0, -19.9997)
	on_executed
		´logic_link_003´ (delay 0)
		´func_sequence_011´ (delay 0)
		´gen_unblock´ (delay 0)
´func_sequence_005´ ElementUnitSequence 100050
	position 500.0, 1300.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/dev_tools/invisible_interaction_open/001 (-275.0, 1450.0, 100.0)
			notify_unit_sequence interaction_enable
			time 0
´func_sequence_trigger_002´ ElementUnitSequenceTrigger 100051
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/props/str_prop_street_electric_box_hackable/001 (-250.0, 1450.0, 0.0)
	on_executed
		´logic_link_003´ (delay 0)
		´func_sequence_013´ (delay 0)
		´gen_unblock´ (delay 0)
´func_sequence_006´ ElementUnitSequence 100054
	position 300.0, 1300.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_old_hoxton/equipment/gen_interactable_police_car_steering_wheel/001 (-845.091, 1538.75, 53.6942)
			notify_unit_sequence state_interaction_enabled
			time 0
´func_sequence_trigger_003´ ElementUnitSequenceTrigger 100055
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/pd2_dlc_old_hoxton/equipment/gen_interactable_police_car_steering_wheel/001 (-845.091, 1538.75, 53.6942)
	on_executed
		´func_sequence_009´ (delay 0)
		´logic_link_003´ (delay 10)
´func_sequence_007´ ElementUnitSequence 100059
	position 400.0, 1300.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small/001 (-779.488, 1495.68, 138.0)
			notify_unit_sequence state_show
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small/001 (-779.488, 1495.68, 138.0)
			notify_unit_sequence activate
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small/001 (-779.488, 1495.68, 138.0)
			notify_unit_sequence set_hack_time_state_3
			time 0
´func_sequence_trigger_004´ ElementUnitSequenceTrigger 100060
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence timer_done
			unit_id units/payday2/equipment/gen_interactable_drill_small/001 (-779.488, 1495.68, 138.0)
	on_executed
		´func_sequence_008´ (delay 0)
´func_sequence_008´ ElementUnitSequence 100061
	position 400.0, 1500.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_van_swat/001 (-775.0, 1600.0, -23.9991)
			notify_unit_sequence anim_door_driver_open
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small/001 (-779.488, 1495.68, 138.0)
			notify_unit_sequence state_hide
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/pd2_dlc_old_hoxton/equipment/gen_interactable_swat_van_steering_wheel/001 (-854.301, 1506.87, 145.909)
			notify_unit_sequence state_interaction_enabled
			time 0
´func_sequence_trigger_005´ ElementUnitSequenceTrigger 100062
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/pd2_dlc_old_hoxton/equipment/gen_interactable_swat_van_steering_wheel/001 (-854.301, 1506.87, 145.909)
	on_executed
		´func_sequence_010´ (delay 0)
		´logic_link_003´ (delay 10)
		´logic_operator_003´ (delay 0)
´func_sequence_009´ ElementUnitSequence 100000
	position 300.0, 1500.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_car_police/001 (-825.0, 1600.0, -21.9991)
			notify_unit_sequence anim_car_hox_move_away
			time 0
	on_executed
		´func_nav_obstacle_006´ (delay 0)
´func_sequence_010´ ElementUnitSequence 100056
	position 400.0, 1700.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_van_swat/001 (-775.0, 1600.0, -23.9991)
			notify_unit_sequence anim_hox_move_away
			time 0
	on_executed
		´func_nav_obstacle_005´ (delay 0)
´func_sequence_trigger_006´ ElementUnitSequenceTrigger 100063
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence load
			unit_id units/pd2_dlc2/architecture/ext_road_c/str_construction_metal_sheet_interactable/001 (-126.0, 2824.0, 6.00003)
	on_executed
		´logic_operator_002´ (delay 0)
´func_nav_obstacle_002´ ElementNavObstacle 100066
	obstacle_list
		1
			guis_id 1
			obj_name 14937479a9b15b82
			unit_id units/payday2/vehicles/anim_vehicle_car_police/001 (-825.0, 1600.0, -21.9991)
		2
			guis_id 2
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/001 (-1300.0, 1200.0, 0.0)
		3
			guis_id 3
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/005 (-1300.0, 800.0, 0.0)
		4
			guis_id 4
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/006 (-1300.0, 400.0, 0.0)
	operation add
	position 300.0, 1100.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
´func_nav_obstacle_003´ ElementNavObstacle 100067
	obstacle_list
		1
			guis_id 1
			obj_name 59928d12a65b6e0f
			unit_id units/payday2/vehicles/anim_vehicle_van_swat/001 (-775.0, 1600.0, -23.9991)
		2
			guis_id 2
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/001 (-1300.0, 1200.0, 0.0)
		3
			guis_id 3
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/005 (-1300.0, 800.0, 0.0)
		4
			guis_id 4
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/006 (-1300.0, 400.0, 0.0)
	operation add
	position 400.0, 1100.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
´func_nav_obstacle_004´ ElementNavObstacle 100068
	obstacle_list
	operation add
	position 500.0, 1100.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´func_sequence_012´ (delay 1)
´func_nav_obstacle_005´ ElementNavObstacle 100069
	obstacle_list
		1
			guis_id 1
			obj_name 59928d12a65b6e0f
			unit_id units/payday2/vehicles/anim_vehicle_van_swat/001 (-775.0, 1600.0, -23.9991)
	operation remove
	position 400.0, 1800.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
´func_nav_obstacle_006´ ElementNavObstacle 100070
	obstacle_list
		1
			guis_id 1
			obj_name 14937479a9b15b82
			unit_id units/payday2/vehicles/anim_vehicle_car_police/001 (-825.0, 1600.0, -21.9991)
	operation remove
	position 300.0, 1600.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
´logic_counter_006´ ElementCounter 100014
	counter_target 2
	digital_gui_unit_ids
	on_executed
		´spikes´ (delay 0)
´logic_counter_007´ ElementCounter 100071
	counter_target 2
	digital_gui_unit_ids
	on_executed
		´police_car´ (delay 0)
´logic_counter_008´ ElementCounter 100072
	counter_target 2
	digital_gui_unit_ids
	on_executed
		´swat_car´ (delay 0)
´logic_counter_009´ ElementCounter 100073
	counter_target 2
	digital_gui_unit_ids
	on_executed
		´bollards´ (delay 0)
´logic_counter_010´ ElementCounter 100074
	counter_target 2
	digital_gui_unit_ids
	on_executed
		´cover_hole´ (delay 0)
´logic_link_002´ MissionScriptElement 100075
	on_executed
		´logic_counter_006´ (delay 0)
		´logic_counter_007´ (delay 0)
		´logic_counter_008´ (delay 0)
		´logic_counter_009´ (delay 0)
		´logic_counter_010´ (delay 0)
´func_instance_input_001´ ElementInstanceInput 100076
	event block_1
	on_executed
		´logic_counter_006´ (delay 0)
´func_instance_input_002´ ElementInstanceInput 100077
	event block_2
	on_executed
		´logic_counter_007´ (delay 0)
´func_instance_input_003´ ElementInstanceInput 100078
	event block_3
	on_executed
		´logic_counter_008´ (delay 0)
´func_instance_input_004´ ElementInstanceInput 100079
	event block_4
	on_executed
		´logic_counter_009´ (delay 0)
´func_instance_input_005´ ElementInstanceInput 100080
	event block_5
	on_executed
		´logic_counter_010´ (delay 0)
´logic_link_003´ MissionScriptElement 100082
	on_executed
		´logic_operator_001´ (delay 0)
		´point_waypoint_001´ (delay 0.1)
		´event_done001´ (delay 0)
		´event_done´ (delay 0)
		´give_blocked_done_exp´ (delay 0)
´point_waypoint_001´ ElementWaypoint 100085
	icon pd2_talk
	only_in_civilian False
	position -800.0, 825.0, 125.0
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´logic_operator_001´ ElementOperator 100086
	elements
		1 ´point_waypoint_001´
		2 ´point_waypoint_003´
		3 ´point_waypoint_004´
		4 ´point_waypoint_005´
		5 ´point_waypoint_006´
		6 ´point_waypoint_007´ DISABLED
		7 ´point_waypoint_010´
		8 ´point_waypoint_009´
		9 ´point_waypoint_008´
	operation remove
´point_waypoint_003´ ElementWaypoint 100088
	icon pd2_generic_look
	only_in_civilian False
	position -511.0, 1360.0, 83.5882
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´point_waypoint_004´ ElementWaypoint 100089
	icon pd2_generic_look
	only_in_civilian False
	position -1168.0, 1348.0, 57.6279
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´point_waypoint_005´ ElementWaypoint 100090
	icon pd2_drill
	only_in_civilian False
	position -771.0, 1495.0, 128.695
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´point_waypoint_006´ ElementWaypoint 100091
	icon pd2_wirecutter
	only_in_civilian False
	position -300.0, 1450.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´point_waypoint_007´ ElementWaypoint 100092
	DISABLED
	icon pd2_generic_look
	only_in_civilian False
	position -100.0, 2800.0, 25.0001
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´cop001´ ElementSpawnEnemyDummy 100094
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_cop_1/ene_cop_1
	force_pickup none
	interval 5
	participate_to_group_ai False
	position -770.0, 2009.0, -19.9991
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	spawn_action none
	team default
	voice 0
	on_executed
		´sniper_so002´ (delay 0)
´cop002´ ElementSpawnEnemyDummy 100093
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_cop_3/ene_cop_3
	force_pickup none
	interval 5
	participate_to_group_ai False
	position -900.0, 1900.0, 0.0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	spawn_action none
	team default
	voice 0
	on_executed
		´sniper_so001´ (delay 0)
´cops´ MissionScriptElement 100095
	on_executed
		´cop001´ (delay 0)
		´cop002´ (delay 0)
´sniper_so001´ ElementSpecialObjective 100096
	SO_access 15584
	action_duration_max 0
	action_duration_min 0
	ai_group none
	align_position True
	align_rotation True
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis 0
	interrupt_dmg 1
	interval -1
	needs_pos_rsrv True
	path_haste run
	path_stance cbt
	path_style none
	patrol_path none
	pose none
	position -950.0, 1825.0, -1.52588e-05
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	scan True
	search_distance 0
	search_position -1050.0, 1950.0, -19.9991
	so_action AI_sniper
	trigger_on none
	use_instigator True
´sniper_so002´ ElementSpecialObjective 100097
	SO_access 15584
	action_duration_max 0
	action_duration_min 0
	ai_group none
	align_position True
	align_rotation True
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis 0
	interrupt_dmg 1
	interval -1
	needs_pos_rsrv True
	path_haste run
	path_stance cbt
	path_style none
	patrol_path none
	pose none
	position -775.0, 1950.0, -19.9991
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	scan True
	search_distance 0
	search_position -1050.0, 1950.0, -19.9991
	so_action AI_sniper
	trigger_on none
	use_instigator True
´sniper_so004´ ElementSpecialObjective 100099
	SO_access 15584
	action_duration_max 0
	action_duration_min 0
	ai_group none
	align_position True
	align_rotation True
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis 0
	interrupt_dmg 1
	interval -1
	needs_pos_rsrv True
	path_haste run
	path_stance cbt
	path_style none
	patrol_path none
	pose none
	position -700.0, 2050.0, -1.52588e-05
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	scan True
	search_distance 0
	search_position -1050.0, 1950.0, -19.9991
	so_action AI_sniper
	trigger_on none
	use_instigator True
´sniper_so005´ ElementSpecialObjective 100100
	SO_access 15584
	action_duration_max 0
	action_duration_min 0
	ai_group none
	align_position True
	align_rotation True
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis 0
	interrupt_dmg 1
	interval -1
	needs_pos_rsrv True
	path_haste run
	path_stance cbt
	path_style none
	patrol_path none
	pose none
	position -1000.0, 2000.0, 0.0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	scan True
	search_distance 0
	search_position -1050.0, 1950.0, -19.9991
	so_action AI_sniper
	trigger_on none
	use_instigator True
´swat001´ ElementSpawnEnemyDummy 100101
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_swat_1/ene_swat_1
	force_pickup none
	interval 5
	participate_to_group_ai False
	position -950.0, 1950.0, 0.0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	spawn_action none
	team default
	voice 0
	on_executed
		´sniper_so001´ (delay 0)
´swat002´ ElementSpawnEnemyDummy 100102
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_swat_1/ene_swat_1
	force_pickup none
	interval 5
	participate_to_group_ai False
	position -1000.0, 2100.0, 3.05176e-05
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	spawn_action none
	team default
	voice 0
	on_executed
		´sniper_so005´ (delay 0)
´swat003´ ElementSpawnEnemyDummy 100103
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_swat_1/ene_swat_1
	force_pickup none
	interval 5
	participate_to_group_ai False
	position -700.0, 2100.0, 0.0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	spawn_action none
	team default
	voice 0
	on_executed
		´sniper_so004´ (delay 0)
´swat´ MissionScriptElement 100098
	on_executed
		´swat002´ (delay 0)
		´swat001´ (delay 0)
		´swat003´ (delay 0)
´func_sequence_012´ ElementUnitSequence 100107
	position 500.0, 800.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/props/str_prop_street_crashbarrier/001 (-500.0, 1350.0, -19.9991)
			notify_unit_sequence state_show
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/props/str_prop_street_crashbarrier/002 (-800.0, 1350.0, -19.9991)
			notify_unit_sequence state_show
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/props/str_prop_street_crashbarrier/003 (-1100.0, 1350.0, -19.9991)
			notify_unit_sequence state_show
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/payday2/props/str_prop_street_crashbarrier/001 (-500.0, 1350.0, -19.9991)
			notify_unit_sequence anim_lid_open
			time 0
		5
			id 5
			name run_sequence
			notify_unit_id units/payday2/props/str_prop_street_crashbarrier/002 (-800.0, 1350.0, -19.9991)
			notify_unit_sequence anim_lid_open
			time 0
		6
			id 6
			name run_sequence
			notify_unit_id units/payday2/props/str_prop_street_crashbarrier/003 (-1100.0, 1350.0, -19.9991)
			notify_unit_sequence anim_lid_open
			time 0
		7
			id 7
			name run_sequence
			notify_unit_id units/payday2/props/str_prop_street_electric_box_hackable/001 (-250.0, 1450.0, 0.0)
			notify_unit_sequence state_show
			time 0
´func_sequence_013´ ElementUnitSequence 100052
	position 500.0, 1700.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/props/str_prop_street_crashbarrier/001 (-500.0, 1350.0, -19.9991)
			notify_unit_sequence anim_lid_close
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/props/str_prop_street_crashbarrier/002 (-800.0, 1350.0, -19.9991)
			notify_unit_sequence anim_lid_close
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/props/str_prop_street_crashbarrier/003 (-1100.0, 1350.0, -19.9991)
			notify_unit_sequence anim_lid_close
			time 0
´event_done001´ ElementInstanceOutput 100108
	event unblocked
´func_sequence_014´ ElementUnitSequence 100109
	position 200.0, 900.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_road_spikes/001 (-700.0, 1300.0, -16.9991)
			notify_unit_sequence state_vis_show
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_road_spikes/002 (-1000.0, 1350.0, -16.9991)
			notify_unit_sequence state_vis_show
			time 0
´func_sequence_trigger_008´ ElementUnitSequenceTrigger 100005
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/dev_tools/invisible_interaction_open/001 (-275.0, 1450.0, 100.0)
	on_executed
		´func_sequence_015´ (delay 0)
´func_sequence_015´ ElementUnitSequence 100006
	position 500.0, 1500.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/props/str_prop_street_electric_box_hackable/001 (-250.0, 1450.0, 0.0)
			notify_unit_sequence open_door
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/props/str_prop_street_electric_box_hackable/001 (-250.0, 1450.0, 0.0)
			notify_unit_sequence interact_enabled
			time 0
´point_waypoint_008´ ElementWaypoint 100001
	icon pd2_generic_look
	only_in_civilian False
	position -830.0, 1550.0, 67.5357
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´point_waypoint_009´ ElementWaypoint 100033
	icon pd2_generic_look
	only_in_civilian False
	position -841.0, 1505.0, 150.398
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´point_waypoint_010´ ElementWaypoint 100049
	icon pd2_generic_look
	only_in_civilian False
	position -788.0, 1214.0, -4.9997
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´logic_operator_002´ ElementOperator 100087
	elements
		1 ´point_waypoint_007´ DISABLED
	operation remove
	on_executed
		´point_waypoint_010´ (delay 0)
´logic_operator_003´ ElementOperator 100110
	elements
		1 ´point_waypoint_005´
	operation remove
	on_executed
		´point_waypoint_009´ (delay 0)
´logic_operator_004´ ElementOperator 100111
	elements
		1 ´point_waypoint_004´
	operation remove
´logic_operator_005´ ElementOperator 100112
	elements
		1 ´point_waypoint_003´
	operation remove
´logic_link_004´ MissionScriptElement 100113
	TRIGGER TIMES 1
	on_executed
		´spikes_2´ (delay 0)
		´police_car_2´ (delay 0)
		´swat_car_2´ (delay 0)
		´bollards_2´ (delay 0)
		´cover_hole_2´ (delay 0)
´func_instance_input_006´ ElementInstanceInput 100114
	event interacted
	on_executed
		´logic_operator_001´ (delay 0)
´func_objective_002´ ElementObjective 100065
	amount 0
	objective heist_hox1_11
	position 200.0, 1900.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	state complete_and_activate
	sub_objective none
	on_executed
		´func_dialogue_001´ (delay 0)
´func_objective_003´ ElementObjective 100084
	amount 0
	objective heist_hox1_12
	position 300.0, 1900.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	state complete_and_activate
	sub_objective none
	on_executed
		´func_dialogue_002´ (delay 0)
´func_objective_004´ ElementObjective 100115
	amount 0
	objective heist_hox1_13
	position 400.0, 1900.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	state complete_and_activate
	sub_objective none
	on_executed
		´func_dialogue_003´ (delay 0)
´func_objective_005´ ElementObjective 100116
	amount 0
	objective heist_hox1_14
	position 500.0, 1900.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	state complete_and_activate
	sub_objective none
	on_executed
		´func_dialogue_004´ (delay 0)
´func_objective_006´ ElementObjective 100117
	amount 0
	objective heist_hox1_15
	position 600.0, 1900.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	state complete_and_activate
	sub_objective none
	on_executed
		´func_dialogue_005´ (delay 0)
´func_sequence_011´ ElementUnitSequence 100002
	position 600.0, 1500.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc2/architecture/str_ext_road_c_straight_a_16x16m_hole/001 (-1600.0, 1600.0, 0.0)
			notify_unit_sequence bridge_show
			time 0
´func_dialogue_001´ ElementDialogue 100044
	dialogue Play_pln_hb1_07
	execute_on_executed_when_done False
	use_position False
´func_dialogue_002´ ElementDialogue 100046
	dialogue Play_pln_hb1_04
	execute_on_executed_when_done False
	use_position False
´func_dialogue_003´ ElementDialogue 100057
	dialogue Play_pln_hb1_05
	execute_on_executed_when_done False
	use_position False
´func_dialogue_004´ ElementDialogue 100118
	dialogue Play_pln_hb1_08
	execute_on_executed_when_done False
	use_position False
´func_dialogue_005´ ElementDialogue 100119
	dialogue Play_pln_hb1_06
	execute_on_executed_when_done False
	use_position False
´gen_block´ ElementNavObstacle 100124
	obstacle_list
		1
			guis_id 1
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/003 (-550.0, 1200.0, 0.0)
		2
			guis_id 2
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/004 (-800.0, 1200.0, 1.49012e-05)
		3
			guis_id 3
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/002 (-1050.0, 1200.0, 0.0)
		4
			guis_id 4
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/001 (-1300.0, 1200.0, 0.0)
	operation add
	position 100.0, 1000.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
´gen_unblock´ ElementNavObstacle 100125
	obstacle_list
		1
			guis_id 1
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/003 (-550.0, 1200.0, 0.0)
		2
			guis_id 2
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/004 (-800.0, 1200.0, 1.49012e-05)
		3
			guis_id 3
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/002 (-1050.0, 1200.0, 0.0)
		4
			guis_id 4
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/001 (-1300.0, 1200.0, 0.0)
	operation remove
	position 0.0, 1000.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
´func_instance_output_001´ ElementInstanceOutput 100133
	event enable_planks
´func_instance_output_002´ ElementInstanceOutput 100132
	event planks
´func_instance_input_007´ ElementInstanceInput 100134
	event picked_up_planks
	on_executed
		´logic_operator_002´ (delay 0)
´give_blocked_done_exp´ ElementExperience 100135
	amount 200
