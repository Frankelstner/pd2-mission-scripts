ID range vs continent name:
	100000: world

statics
	100000 units/payday2/vehicles/anim_vehicle_van_swat/002 (0.0, 0.0, 0.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 1.0
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_left
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
		2
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 1.0
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_right
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
		3
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 1.0
			enabled False
			falloff_exponent 1
			far_range 1000
			multiplier identity
			name ls_t_left
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
		4
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 1.0
			enabled False
			falloff_exponent 1
			far_range 1000
			multiplier identity
			name ls_t_right
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
