﻿´point_special_objective_001´ ElementSpecialObjective 100090
	SO_access 262140
	action_duration_max 0
	action_duration_min 0
	ai_group none
	align_position True
	align_rotation True
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis 7
	interrupt_dmg 0
	interval 3
	is_navigation_link True
	needs_pos_rsrv True
	path_haste none
	path_stance none
	path_style none
	patrol_path none
	pose none
	position 100.0, 1225.0, 0.0
	rotation 0.0, 0.0, 1.0, -4.17233e-07
	scan True
	search_distance 0
	search_position 105.103, 774.838, 0.0
	so_action e_nl_up_2_25_fwd_2_5_dwn_2_25m
	trigger_on none
´point_special_objective_002´ ElementSpecialObjective 100012
	SO_access 262140
	action_duration_max 0
	action_duration_min 0
	ai_group none
	align_position True
	align_rotation True
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis 7
	interrupt_dmg 0
	interval 3
	is_navigation_link True
	needs_pos_rsrv True
	path_haste none
	path_stance none
	path_style none
	patrol_path none
	pose none
	position -100.0, 1225.0, 0.0
	rotation 0.0, 0.0, 1.0, -4.17233e-07
	scan True
	search_distance 0
	search_position -100.818, 757.97, 0.0
	so_action e_nl_up_2_25_fwd_2_5_dwn_2_25m
	trigger_on none
´point_special_objective_003´ ElementSpecialObjective 100013
	SO_access 262140
	action_duration_max 0
	action_duration_min 0
	ai_group none
	align_position True
	align_rotation True
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis 7
	interrupt_dmg 0
	interval 3
	is_navigation_link True
	needs_pos_rsrv True
	path_haste none
	path_stance none
	path_style none
	patrol_path none
	pose none
	position -100.001, 1900.0, 0.0
	rotation 0.0, 0.0, 1.0, -4.17233e-07
	scan True
	search_distance 0
	search_position -98.1866, 1462.24, 0.0
	so_action e_nl_up_2_25_fwd_2_5_dwn_2_25m
	trigger_on none
´point_special_objective_004´ ElementSpecialObjective 100014
	SO_access 262140
	action_duration_max 0
	action_duration_min 0
	ai_group none
	align_position True
	align_rotation True
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis 7
	interrupt_dmg 0
	interval 3
	is_navigation_link True
	needs_pos_rsrv True
	path_haste none
	path_stance none
	path_style none
	patrol_path none
	pose none
	position 99.9995, 1900.0, 0.0
	rotation 0.0, 0.0, 1.0, -4.17233e-07
	scan True
	search_distance 0
	search_position 97.2243, 1466.57, 0.0
	so_action e_nl_up_2_25_fwd_2_5_dwn_2_25m
	trigger_on none
´released_strap_001´ MissionScriptElement 100015
	on_executed
		´logic_operator_001´ (delay 0)
		´release_barrels001´ (delay 0)
		´point_play_sound_001´ (delay 0)
		´disable_blocker_collsiion_001´ (delay 3)
´released_strap_002´ MissionScriptElement 100016
	on_executed
		´disable_blocker_collsiion_002´ (delay 3)
		´logic_operator_002´ (delay 0)
		´release_barrels003´ (delay 0)
		´point_play_sound_002´ (delay 0)
´disable_blocker_collsiion_001´ ElementDisableUnit 100023
	position 600.0, 900.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/dev_tools/level_tools/dev_collision_8x32m/001 (350.0, 875.0, 0.0)
	on_executed
		´remove_blocker_obstacle_001´ (delay 0)
´disable_blocker_collsiion_002´ ElementDisableUnit 100024
	position 600.0, 1500.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/dev_tools/level_tools/dev_collision_8x32m/002 (350.0, 1550.0, 0.0)
	on_executed
		´remove_blocker_obstacle_003´ (delay 0)
´startup´ MissionScriptElement 100025
	EXECUTE ON STARTUP
	BASE DELAY 0.35
	on_executed
		´disable_all´ (delay 0)
´disable_all´ ElementDisableUnit 100026
	position 1200.0, 800.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/dev_tools/level_tools/dev_collision_8x32m/001 (350.0, 875.0, 0.0)
		2 units/dev_tools/level_tools/dev_collision_8x32m/002 (350.0, 1550.0, 0.0)
		3 units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch/003 (-26.0, 999.0, -143.0)
		4 units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch/005 (20.0, 1686.0, -143.0)
		5 units/payday2/props/gen_prop_crate_wood_a/002 (94.9729, 251.793, 0.0)
		6 units/world/props/street/cementbags_cover/001 (151.0, 777.0, 0.0)
		7 units/world/props/street/cementbags_cover/002 (-174.0, 1464.0, 0.0)
	on_executed
		´func_sequence_001´ (delay 0)
´set_blocker´ ElementInstanceInput 100027
	BASE DELAY 1
	TRIGGER TIMES 1
	event set_blocker
	on_executed
		´show_all´ (delay 0)
		´enable_interactions_and_areas´ (delay 0)
´show_all´ ElementEnableUnit 100028
	position 1200.0, 1000.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/dev_tools/level_tools/dev_collision_8x32m/001 (350.0, 875.0, 0.0)
		2 units/dev_tools/level_tools/dev_collision_8x32m/002 (350.0, 1550.0, 0.0)
		3 units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch/003 (-26.0, 999.0, -143.0)
		4 units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch/005 (20.0, 1686.0, -143.0)
		5 units/payday2/props/gen_prop_crate_wood_a/002 (94.9729, 251.793, 0.0)
		6 units/world/props/street/cementbags_cover/001 (151.0, 777.0, 0.0)
		7 units/world/props/street/cementbags_cover/002 (-174.0, 1464.0, 0.0)
	on_executed
		´add_blocker_obstacle_001´ (delay 1)
´add_blocker_obstacle_001´ ElementNavObstacle 100029
	obstacle_list
		1
			guis_id 1
			obj_name e6badfa70bf035f8
			unit_id units/world/props/street/cementbags_cover/001 (151.0, 777.0, 0.0)
		2
			guis_id 2
			obj_name 4407a5565a4566dd
			unit_id units/dev_tools/level_tools/dev_door_blocker_1x4x3/001 (150.0, 975.0, -9.42454)
	operation add
	position 1100.0, 1000.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´navlinks´ (delay 0)
		´add_blocker_obstacle_002´ (delay 0)
´add_blocker_obstacle_002´ ElementNavObstacle 100091
	obstacle_list
		1
			guis_id 1
			obj_name e6badfa70bf035f8
			unit_id units/world/props/street/cementbags_cover/002 (-174.0, 1464.0, 0.0)
		2
			guis_id 4
			obj_name 812ebc0cff9242d3
			unit_id units/dev_tools/level_tools/dev_door_blocker_3x4x3/001 (-178.0, 1851.0, -39.728)
	operation add
	position 1000.0, 1000.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´add_barrels_obstacle´ (delay 0)
´remove_blocker_obstacle_001´ ElementNavObstacle 100092
	obstacle_list
		1
			guis_id 1
			obj_name 4407a5565a4566dd
			unit_id units/dev_tools/level_tools/dev_door_blocker_1x4x3/001 (150.0, 975.0, -9.42454)
	operation remove
	position 700.0, 900.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
´add_barrels_obstacle´ ElementNavObstacle 100096
	obstacle_list
		1
			guis_id 1
			obj_name 8489b7474b87cd6e
			unit_id units/dev_tools/level_tools/dev_door_blocker_1x2x3/002 (145.0, 350.0, 0.0)
	operation add
	position 1000.0, 1100.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
´navlinks´ MissionScriptElement 100007
	on_executed
		´point_special_objective_002´ (delay 0)
		´point_special_objective_001´ (delay 0)
		´point_special_objective_004´ (delay 0)
		´point_special_objective_003´ (delay 0)
´enable_wp_area_001´ ElementAreaTrigger 100097
	DISABLED
	TRIGGER TIMES 1
	amount 1
	depth 500
	height 500
	instigator player
	interval 0.1
	position 0.0, 700.0, 100.0
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 500
	on_executed
		´point_waypoint_001´ (delay 0)
		´func_sequence_003´ (delay 0)
´enable_wp_area_002´ ElementAreaTrigger 100098
	DISABLED
	TRIGGER TIMES 1
	amount 1
	depth 500
	height 500
	instigator player
	interval 0.1
	position 0.0, 1300.0, 100.0
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 500
	on_executed
		´point_waypoint_002´ (delay 0)
		´func_sequence_004´ (delay 0)
´point_waypoint_001´ ElementWaypoint 100099
	icon pd2_generic_interact
	only_in_civilian False
	position -200.0, 850.0, 50.0
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´point_waypoint_002´ ElementWaypoint 100100
	icon pd2_generic_interact
	only_in_civilian False
	position 200.0, 1525.0, 50.0
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´enable_interactions_and_areas´ ElementToggle 100101
	elements
		1 ´enable_wp_area_001´ DISABLED
		2 ´enable_wp_area_002´ DISABLED
	set_trigger_times -1
	toggle on
	on_executed
		´func_sequence_002´ (delay 0)
´logic_operator_001´ ElementOperator 100102
	elements
		1 ´point_waypoint_001´
		2 ´point_special_objective_001´
		3 ´point_special_objective_002´
	operation remove
	on_executed
		´enable_kill_area_001´ (delay 0)
´logic_operator_002´ ElementOperator 100103
	elements
		1 ´point_waypoint_002´
		2 ´point_special_objective_004´
		3 ´point_special_objective_003´
	operation remove
	on_executed
		´enable_kill_area_002´ (delay 0)
´cleared_drums´ ElementInstanceOutput 100104
	event cleared_drums
´kill_area_001´ ElementAreaTrigger 100105
	DISABLED
	TRIGGER TIMES 1
	amount 1
	depth 700
	height 10000
	instigator player
	interval 0.1
	position 7.62939e-06, 1000.0, 100.0
	radius 250
	rotation 0.0, 0.0, -0.707107, -0.707107
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 200
´kill_area_002´ ElementAreaTrigger 100106
	DISABLED
	TRIGGER TIMES 1
	amount 1
	depth 700
	height 10000
	instigator player
	interval 0.1
	position -7.28369e-05, 1675.0, 100.0
	radius 250
	rotation 0.0, 0.0, -0.707107, -0.707107
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 200
´enable_kill_area_001´ ElementToggle 100107
	elements
		1 ´kill_area_001´ DISABLED
	set_trigger_times -1
	toggle on
	on_executed
		´disable_kill_area_001´ (delay 0.5)
´disable_kill_area_001´ ElementToggle 100108
	elements
		1 ´kill_area_001´ DISABLED
	set_trigger_times -1
	toggle off
´disable_kill_area_002´ ElementToggle 100109
	elements
		1 ´kill_area_002´ DISABLED
	set_trigger_times -1
	toggle off
´enable_kill_area_002´ ElementToggle 100110
	elements
		1 ´kill_area_002´ DISABLED
	set_trigger_times -1
	toggle on
	on_executed
		´disable_kill_area_002´ (delay 0.5)
´point_spawn_player_001´ ElementPlayerSpawner 100111
	EXECUTE ON STARTUP
	DISABLED
	position -100.0, 300.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	state standard
	on_executed
		´set_blocker´ (delay 1)
´release_barrels001´ ElementUnitSequence 100115
	position 600.0, 800.0, 102.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_chew/props/chw_prop_oil_drum_group/001 (-227.0, 999.0, 3.0)
			notify_unit_sequence anim_barrel_fall_off
			time 0
	on_executed
		´release_barrels002´ (delay 0.05-0.1)
´release_barrels002´ ElementUnitSequence 100033
	position 700.0, 800.0, 102.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_chew/props/chw_prop_oil_drum_group/002 (-227.0, 1124.0, 3.0)
			notify_unit_sequence anim_barrel_fall_off
			time 0
´release_barrels003´ ElementUnitSequence 100034
	position 600.0, 1400.0, 102.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_chew/props/chw_prop_oil_drum_group/003 (227.0, 1683.0, 3.0)
			notify_unit_sequence anim_barrel_fall_off
			time 0
	on_executed
		´release_barrels004´ (delay 0.05-0.1)
´release_barrels004´ ElementUnitSequence 100035
	position 700.0, 1400.0, 102.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_chew/props/chw_prop_oil_drum_group/004 (227.0, 1560.0, 3.0)
			notify_unit_sequence anim_barrel_fall_off
			time 0
´func_sequence_001´ ElementUnitSequence 100017
	position 1200.0, 900.0, 102.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_chew/props/chw_prop_oil_drum_group/001 (-227.0, 999.0, 3.0)
			notify_unit_sequence hide
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_chew/props/chw_prop_oil_drum_group/002 (-227.0, 1124.0, 3.0)
			notify_unit_sequence hide
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/pd2_dlc_chew/props/chw_prop_oil_drum_group/003 (227.0, 1683.0, 3.0)
			notify_unit_sequence hide
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/pd2_dlc_chew/props/chw_prop_oil_drum_group/004 (227.0, 1560.0, 3.0)
			notify_unit_sequence hide
			time 0
		5
			id 5
			name run_sequence
			notify_unit_id units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch/001 (24.0, 1000.0, -143.0)
			notify_unit_sequence state_vis_hide
			time 0
		6
			id 6
			name run_sequence
			notify_unit_id units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch_2/001 (-25.0, 1685.0, -143.0)
			notify_unit_sequence state_vis_hide
			time 0
´func_sequence_002´ ElementUnitSequence 100018
	position 1200.0, 1200.0, 102.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_chew/props/chw_prop_oil_drum_group/001 (-227.0, 999.0, 3.0)
			notify_unit_sequence show
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_chew/props/chw_prop_oil_drum_group/002 (-227.0, 1124.0, 3.0)
			notify_unit_sequence show
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/pd2_dlc_chew/props/chw_prop_oil_drum_group/003 (227.0, 1683.0, 3.0)
			notify_unit_sequence show
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/pd2_dlc_chew/props/chw_prop_oil_drum_group/004 (227.0, 1560.0, 3.0)
			notify_unit_sequence show
			time 0
		5
			id 5
			name run_sequence
			notify_unit_id units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch/001 (24.0, 1000.0, -143.0)
			notify_unit_sequence state_vis_show
			time 0
		6
			id 6
			name run_sequence
			notify_unit_id units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch_2/001 (-25.0, 1685.0, -143.0)
			notify_unit_sequence state_vis_show
			time 0
´interacted´ ElementUnitSequenceTrigger 100010
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch/001 (24.0, 1000.0, -143.0)
	on_executed
		´released_strap_001´ (delay 0)
		´point_teammate_comment_001´ (delay 0)
´interacted001´ ElementUnitSequenceTrigger 100011
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch_2/001 (-25.0, 1685.0, -143.0)
	on_executed
		´released_strap_002´ (delay 0)
		´hurry´ (delay 0)
´func_sequence_003´ ElementUnitSequence 100037
	position 100.0, 700.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch/001 (24.0, 1000.0, -143.0)
			notify_unit_sequence state_interaction_enabled
			time 0
	on_executed
		´func_dialogue_001´ (delay 0)
´func_sequence_004´ ElementUnitSequence 100041
	position 100.0, 1300.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch_2/001 (-25.0, 1685.0, -143.0)
			notify_unit_sequence state_interaction_enabled
			time 0
´point_play_sound_001´ ElementPlaySound 100044
	append_prefix False
	elements
	position -4.0, 1000.0, 207.617
	rotation 0.0, 0.0, 0.0, -1.0
	sound_event anim_barrel_fall_off
	use_instigator False
´point_play_sound_002´ ElementPlaySound 100045
	append_prefix False
	elements
	position -4.0, 1700.0, 207.617
	rotation 0.0, 0.0, 0.0, -1.0
	sound_event anim_barrel_fall_off
	use_instigator False
´func_dialogue_001´ ElementDialogue 100000
	dialogue Play_pln_chw_19
	execute_on_executed_when_done False
	use_position False
´point_teammate_comment_001´ ElementTeammateComment 100022
	close_to_element False
	comment g12
	position 400.0, 800.0, 102.5
	radius 0
	rotation 0.0, 0.0, 0.0, -1.0
	use_instigator True
´hurry´ ElementTeammateComment 100038
	close_to_element False
	comment g09
	position 400.0, 1600.0, 102.5
	radius 0
	rotation 0.0, 0.0, 0.0, -1.0
	use_instigator True
´remove_blocker_obstacle_003´ ElementNavObstacle 100020
	obstacle_list
		1
			guis_id 2
			obj_name 812ebc0cff9242d3
			unit_id units/dev_tools/level_tools/dev_door_blocker_3x4x3/001 (-178.0, 1851.0, -39.728)
	operation remove
	position 700.0, 1500.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
