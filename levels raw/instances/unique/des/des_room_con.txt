ID range vs continent name:
	100000: world

statics
	100071 core/units/light_omni/001 (-2100.0, 1621.01, 71.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.384314, 0.839216, 0.933333
			enabled True
			falloff_exponent 1
			far_range 200
			multiplier identity
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100073 core/units/light_spot/001 (-1768.5, 1214.64, 55.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.552941, 0.752941, 0.862745
			enabled True
			falloff_exponent 1
			far_range 300
			multiplier reddot
			name ls_spot
			near_range 80
			spot_angle_end 45
			spot_angle_start 43
	100000 units/payday2/architecture/mall/com_ext_floor_a/001 (-2590.5, 950.64, 1.0)
	100020 units/payday2/props/air_prop_office_binder_pack_4/001 (-1906.5, 977.64, 139.0)
	100019 units/payday2/props/air_prop_office_binder_pack_5/001 (-1976.5, 982.64, 138.0)
	100007 units/payday2/props/air_prop_terminal_coffecup/001 (-2464.11, 1346.55, 86.0)
	100008 units/payday2/props/air_prop_terminal_coffecup/002 (-2452.08, 1450.84, 86.0)
	100009 units/payday2/props/air_prop_terminal_coffecup/003 (-2460.55, 1568.42, 86.0)
	100003 units/payday2/props/bnk_prop_office_filofax/001 (-2347.5, 1687.64, 88.0)
	100050 units/payday2/props/bnk_prop_office_filofax/002 (-2445.09, 1316.61, 87.0)
	100053 units/payday2/props/gen_prop_security_safe_2x05/001 (-2496.0, 1085.0, 0.0)
	100054 units/payday2/props/gen_prop_security_safe_2x05/002 (-2496.0, 1136.0, 0.0)
	100036 units/payday2/props/off_prop_appliance_computer/001 (-2056.0, 1329.0, 1.0)
	100031 units/payday2/props/off_prop_generic_shelf_wire_a/001 (-2171.5, 988.64, -1.0)
	100021 units/payday2/props/paperstacks/off_prop_officehigh_paper_stack_01/001 (-1844.5, 977.64, 136.0)
	100045 units/payday2/props/servers/gen_prop_security_server_hightech_open/001 (-1966.0, 1681.01, 0.0)
	100046 units/payday2/props/servers/gen_prop_security_server_hightech_open/002 (-2029.0, 1681.01, 0.0)
	100047 units/payday2/props/servers/gen_prop_security_server_hightech_open/003 (-1903.0, 1681.01, 0.0)
	100049 units/payday2/props/servers/gen_prop_security_server_hightech_open/004 (-1840.0, 1681.01, 0.0)
	100044 units/payday2/props/servers/gen_prop_security_server_rack/001 (-2236.0, 1679.01, 0.0)
	100017 units/payday2/props/set/ind_prop_warehouse_box_f/001 (-2196.5, 988.64, 199.0)
	100029 units/payday2/props/set/ind_prop_warehouse_box_plastic_a/001 (-2216.85, 995.027, 87.0)
	100030 units/payday2/props/set/ind_prop_warehouse_box_plastic_a/002 (-2216.85, 995.027, 146.0)
	100028 units/payday2/props/set/ind_prop_warehouse_box_plastic_c/001 (-2209.5, 988.64, 25.0)
	100033 units/payday2/props/shadow_mullplan/mullplan_vehicle_small_light_02/001 (-1970.0, 963.999, 1.0)
	100034 units/payday2/props/shadow_mullplan/mullplan_vehicle_small_light_02/002 (-2082.0, 1408.0, 1.0)
	100043 units/payday2/props/shadow_mullplan/mullplan_vehicle_small_light_02/003 (-2176.0, 1708.0, 1.0)
	100048 units/payday2/props/shadow_mullplan/mullplan_vehicle_small_light_02/004 (-1965.0, 1708.0, 1.0)
	100018 units/payday2/props/stn_prop_office_filecabinet_b_02/001 (-1968.5, 1016.64, -2.0)
	100022 units/payday2/props/stn_prop_office_filecabinet_b_02/002 (-1927.5, 1016.64, -2.0)
	100023 units/payday2/props/stn_prop_office_filecabinet_b_02/003 (-1807.5, 1016.64, -2.0)
	100025 units/payday2/props/stn_prop_office_filecabinet_b_02/004 (-1886.5, 1016.64, -2.0)
	100027 units/payday2/props/stn_prop_office_filecabinet_b_02/005 (-2046.5, 1016.64, -2.0)
	100024 units/payday2/props/stn_prop_office_filecabinet_c_02/001 (-1847.5, 1016.64, -2.0)
	100026 units/payday2/props/stn_prop_office_filecabinet_c_02/002 (-2008.5, 1016.64, -2.0)
	100005 units/pd2_dlc2/props/gen_prop_security_console_corner/001 (-2315.5, 1754.64, -2.0)
	100001 units/pd2_dlc2/props/gen_prop_security_console_straight/001 (-2515.5, 1354.64, -2.0)
	100052 units/pd2_dlc2/props/gen_prop_security_console_straight/002 (-2515.5, 1154.64, -2.0)
	100002 units/pd2_dlc2/props/gen_prop_security_console_straight_variation_2/001 (-2507.5, 1354.64, -2.0)
		mesh_variation state_power_off
	100004 units/pd2_dlc2/props/gen_prop_security_console_straight_variation_2/002 (-2508.91, 1604.14, -2.0)
		mesh_variation state_power_off
	100051 units/pd2_dlc2/props/gen_prop_security_console_straight_variation_2/003 (-2507.5, 1154.64, -2.0)
		mesh_variation state_power_off
	100032 units/pd2_dlc_arena/architecture/room_boxes/are_supply_shelf_01_boxes/001 (-2152.5, 988.64, -1.0)
	100040 units/pd2_dlc_arena/props/set/are_prop_office_desk_straight/001 (-2066.5, 1407.64, -2.0)
	100041 units/pd2_dlc_berry/props/servers/bry_prop_master_server_03/001 (-2100.0, 1721.01, 0.0)
	100042 units/pd2_dlc_berry/props/servers/bry_prop_server_02/001 (-2171.0, 1681.01, 0.0)
	100038 units/pd2_dlc_cage/props/are_prop_office_lamp_desk/001 (-2086.3, 1483.64, 73.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 1.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier match
			name li_desk_lamp
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100037 units/pd2_dlc_casino/props/cas_prop_security_computer/001 (-2068.0, 1405.0, 73.0)
	100011 units/pd2_dlc_chill/props/details/chl_ext_prop_cable_straight_01_4m/001 (-2053.0, 1360.0, 321.0)
	100012 units/pd2_dlc_chill/props/details/chl_ext_prop_cable_straight_01_4m/002 (-2053.0, 1560.0, 321.0)
	100013 units/pd2_dlc_chill/props/details/chl_ext_prop_cable_straight_01_4m/003 (-2247.0, 1564.0, 321.0)
	100014 units/pd2_dlc_chill/props/details/chl_ext_prop_cable_straight_01_4m/004 (-2247.0, 1364.0, 321.0)
	100015 units/pd2_dlc_chill/props/details/chl_ext_prop_cable_straight_01_4m/005 (-2247.0, 1164.0, 321.0)
	100016 units/pd2_dlc_chill/props/details/chl_ext_prop_cable_straight_01_4m/006 (-2053.0, 1160.0, 321.0)
	100035 units/pd2_dlc_dah/props/dah_prop_chair_c/001 (-1998.5, 1394.64, -2.0)
	100061 units/pd2_dlc_des/architecture/des_int/des_int_tunnel_window/002 (-1770.0, 1386.0, 0.0)
	100057 units/pd2_dlc_des/props/des_prop_long_lamp/001 (-2149.5, 1160.64, 326.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.462745, 0.67451, 0.937255
			enabled True
			falloff_exponent 4
			far_range 450
			multiplier none
			name ls_light
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100058 units/pd2_dlc_des/props/des_prop_long_lamp/002 (-2149.5, 1360.64, 326.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.462745, 0.67451, 0.937255
			enabled True
			falloff_exponent 4
			far_range 450
			multiplier none
			name ls_light
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100059 units/pd2_dlc_des/props/des_prop_long_lamp/003 (-2149.5, 1560.64, 326.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.462745, 0.67451, 0.937255
			enabled True
			falloff_exponent 4
			far_range 450
			multiplier none
			name ls_light
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100114 units/pd2_dlc_des/props/des_prop_movie_screen/001 (-2515.0, 1424.0, 300.0)
	100006 units/pd2_dlc_holly/mansion/props/lxa_prop_office_trashbin_b/001 (-2437.0, 1720.0, 1.0)
	100039 units/pd2_dlc_holly/mansion/props/lxa_prop_office_trashbin_b/002 (-2064.0, 1519.0, 1.0)
	100055 units/pd2_dlc_holly/mansion/props/lxa_prop_office_trashbin_b/003 (-2450.0, 1142.0, 1.0)
	100056 units/world/props/bank/security_door_02/001 (-1815.5, 1000.64, 1.0)
		mesh_variation open_door
	100060 units/world/props/bank/security_door_02/002 (-1815.5, 1400.64, 1.0)
		mesh_variation open_door
