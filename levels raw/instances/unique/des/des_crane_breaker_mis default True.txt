﻿´disable_all´ ElementUnitSequence 100000
	EXECUTE ON STARTUP
	position 100.0, 100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/world/props/suburbia_circuitbreaker/002 (0.0, 0.0, 126.0)
			notify_unit_sequence disable_interaction
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/world/props/suburbia_circuitbreaker/002 (0.0, 0.0, 126.0)
			notify_unit_sequence disable_glow
			time 0
´enable_breaker´ ElementInstanceInput 100003
	event enable_breaker
	on_executed
		´logic_operator_002´ (delay 15)
		´enable_position001´ (delay 0)
´enable_position´ ElementToggle 100004
	elements
		1 ´position´ DISABLED
	set_trigger_times 1
	toggle on
	on_executed
		´enable_breaker_2´ (delay 0)
´enable_breaker_2´ ElementToggle 100005
	elements
		1 ´intrrupt_action´ DISABLED
	set_trigger_times 1
	toggle on
	on_executed
		´position´ (delay 1) DISABLED
´position´ ElementSpecialObjective 100006
	DISABLED
	TRIGGER TIMES 1
	SO_access 65012216
	action_duration_max 0
	action_duration_min 0
	ai_group enemies
	align_position True
	align_rotation True
	attitude none
	base_chance 1
	chance_inc 0
	followup_elements
		1 ´intrrupt_action´ DISABLED
	forced False
	interaction_voice none
	interrupt_dis 0
	interrupt_dmg 1
	interrupt_objective False
	interval 0
	is_navigation_link False
	needs_pos_rsrv True
	path_haste run
	path_stance cbt
	path_style destination
	patrol_path none
	pose none
	position 31.7842, 188.197, 0.0
	rotation 0.0, 0.0, 1.0, -7.45058e-07
	scan True
	search_distance 1000
	search_position 0.0, 100.0, 0.0
	so_action none
	trigger_on none
´kicked´ ElementSpecialObjectiveTrigger 100001
	elements
		1 ´intrrupt_action´ DISABLED
	event complete
	position 100.0, 500.0, 2.5
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´interrupt_do_once´ (delay 0)
		´interrupt´ (delay 0)
		´logic_toggle_001´ (delay 1)
´interrupt_do_once´ ElementUnitSequence 100007
	TRIGGER TIMES 1
	position 100.0, 600.0, 2.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/world/props/suburbia_circuitbreaker/002 (0.0, 0.0, 126.0)
			notify_unit_sequence open_box
			time 0
´interrupt´ ElementUnitSequence 100008
	position 100.0, 700.0, 2.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/world/props/suburbia_circuitbreaker/002 (0.0, 0.0, 126.0)
			notify_unit_sequence enable_glow
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/world/props/suburbia_circuitbreaker/002 (0.0, 0.0, 126.0)
			notify_unit_sequence enable_interaction
			time 0
´turned_off´ ElementUnitSequenceTrigger 100009
	sequence_list
		1
			guis_id 1
			sequence turn_off
			unit_id units/world/props/suburbia_circuitbreaker/002 (0.0, 0.0, 126.0)
	on_executed
		´breaker_off´ (delay 0)
		´wp_interact´ (delay 0)
´breaker_off´ ElementInstanceOutput 100011
	event breaker_off
´breaker_on´ ElementInstanceOutput 100010
	event breaker_on
´interacted´ ElementUnitSequenceTrigger 100012
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/world/props/suburbia_circuitbreaker/002 (0.0, 0.0, 126.0)
	on_executed
		´restored´ (delay 0)
´restored´ ElementUnitSequence 100013
	position 0.0, 600.0, 2.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/world/props/suburbia_circuitbreaker/002 (0.0, 0.0, 126.0)
			notify_unit_sequence disable_glow
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/world/props/suburbia_circuitbreaker/002 (0.0, 0.0, 126.0)
			notify_unit_sequence turn_on
			time 0
	on_executed
		´remove_wp´ (delay 0)
		´logic_operator_002´ (delay 15)
´wp_interact´ ElementWaypoint 100014
	icon pd2_generic_interact
	only_in_civilian False
	only_on_instigator False
	position 0.0, 31.0, 125.5
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´remove_wp´ ElementOperator 100015
	elements
		1 ´wp_interact´
	operation remove
	on_executed
		´breaker_on´ (delay 0)
´point_debug_001´ ElementDebug 100019
	as_subtitle False
	debug_string fail
	show_instigator False
´intrrupt_action´ ElementSpecialObjective 100002
	DISABLED
	TRIGGER TIMES 1
	SO_access 65012216
	action_duration_max 0
	action_duration_min 0
	ai_group enemies
	align_position True
	align_rotation True
	attitude none
	base_chance 1
	chance_inc 0
	followup_elements
		1 ´intrrupt_action´ DISABLED
	forced False
	interaction_voice none
	interrupt_dis 0
	interrupt_dmg 1
	interrupt_objective False
	interval 0
	is_navigation_link False
	needs_pos_rsrv True
	path_haste run
	path_stance cbt
	path_style destination
	patrol_path none
	pose none
	position -68.2158, 188.197, 0.0
	rotation 0.0, 0.0, 1.0, -7.15256e-07
	scan True
	search_distance 0
	search_position 83.2748, 253.82, 2.5
	so_action e_so_container_kick
	trigger_on none
	use_instigator True
´disable_breaker´ ElementInstanceInput 100016
	event disable_breaker
	on_executed
		´disable_all_2´ (delay 0)
´disable_all_2´ ElementToggle 100017
	elements
		1 ´position´ DISABLED
		2 ´intrrupt_action´ DISABLED
		3 ´interrupt´
		4 ´breaker_off´
	set_trigger_times 1
	toggle off
	on_executed
		´logic_operator_001´ (delay 0)
´logic_toggle_001´ ElementToggle 100021
	elements
		1 ´intrrupt_action´ DISABLED
	set_trigger_times -1
	toggle off
´kicked001´ ElementSpecialObjectiveTrigger 100020
	elements
		1 ´intrrupt_action´ DISABLED
		2 ´position´ DISABLED
	event fail
	position 300.0, 700.0, 2.5
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´logic_operator_002´ (delay 0)
´logic_operator_001´ ElementOperator 100026
	elements
		1 ´position´ DISABLED
		2 ´intrrupt_action´ DISABLED
	operation remove
´logic_operator_002´ ElementOperator 100027
	elements
		1 ´position´ DISABLED
		2 ´intrrupt_action´ DISABLED
	operation remove
	on_executed
		´enable_position´ (delay 0)
´enable_position001´ ElementToggle 100028
	elements
		1 ´interrupt´
		2 ´breaker_off´
	set_trigger_times -1
	toggle on
