ID range vs continent name:
	100000: world

statics
	100084 core/units/light_omni/002 (-1.0, 0.0, 133.5)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.960784, 0.819608, 0.662745
			enabled True
			falloff_exponent 1
			far_range 250
			multiplier identity
			name lo_omni
			near_range 250
			spot_angle_end -1
			spot_angle_start -1
	100004 units/dev_tools/editable_text_short/001 (-427.828, 363.222, -335.27)
		align left
		alpha 1
		blend_mode normal
		font fonts/font_medium_shadow_mf
		font_color 1.0, 0.0509804, 0.121569
		font_size 1.34
		render_template diffuse_vc_decal
		shape
			1 0
			2 0
			3 1
			4 1
		text +
		vertical center
		word_wrap False
		wrap False
	100002 units/dev_tools/editable_text_short/A002 (-428.566, 352.674, -338.302)
		align left
		alpha 1
		blend_mode normal
		font fonts/font_medium_shadow_mf
		font_color 1.0, 0.0509804, 0.121569
		font_size 1.34
		render_template diffuse_vc_decal
		shape
			1 0
			2 0
			3 1
			4 1
		text A
		vertical center
		word_wrap False
		wrap False
	100008 units/dev_tools/editable_text_short/A003 (-426.889, 376.647, -331.412)
		align left
		alpha 1
		blend_mode normal
		font fonts/font_medium_shadow_mf
		font_color 1.0, 0.0509804, 0.121569
		font_size 1.34
		render_template diffuse_vc_decal
		shape
			1 0
			2 0
			3 1
			4 1
		text A
		vertical center
		word_wrap False
		wrap False
	100038 units/dev_tools/editable_text_short/B002 (-428.566, 352.674, -338.302)
		align left
		alpha 1
		blend_mode normal
		font fonts/font_medium_shadow_mf
		font_color 1.0, 0.0509804, 0.121569
		font_size 1.34
		render_template diffuse_vc_decal
		shape
			1 0
			2 0
			3 1
			4 1
		text B
		vertical center
		word_wrap False
		wrap False
	100039 units/dev_tools/editable_text_short/B003 (-426.889, 376.647, -331.412)
		align left
		alpha 1
		blend_mode normal
		font fonts/font_medium_shadow_mf
		font_color 1.0, 0.0509804, 0.121569
		font_size 1.34
		render_template diffuse_vc_decal
		shape
			1 0
			2 0
			3 1
			4 1
		text B
		vertical center
		word_wrap False
		wrap False
	100047 units/dev_tools/editable_text_short/C002 (-428.566, 352.674, -338.302)
		align left
		alpha 1
		blend_mode normal
		font fonts/font_medium_shadow_mf
		font_color 1.0, 0.0509804, 0.121569
		font_size 1.34
		render_template diffuse_vc_decal
		shape
			1 0
			2 0
			3 1
			4 1
		text C
		vertical center
		word_wrap False
		wrap False
	100044 units/dev_tools/editable_text_short/D002 (-428.566, 352.674, -338.302)
		align left
		alpha 1
		blend_mode normal
		font fonts/font_medium_shadow_mf
		font_color 1.0, 0.0509804, 0.121569
		font_size 1.34
		render_template diffuse_vc_decal
		shape
			1 0
			2 0
			3 1
			4 1
		text D
		vertical center
		word_wrap False
		wrap False
	100045 units/dev_tools/editable_text_short/D003 (-426.889, 376.647, -331.412)
		align left
		alpha 1
		blend_mode normal
		font fonts/font_medium_shadow_mf
		font_color 1.0, 0.0509804, 0.121569
		font_size 1.34
		render_template diffuse_vc_decal
		shape
			1 0
			2 0
			3 1
			4 1
		text D
		vertical center
		word_wrap False
		wrap False
	100042 units/dev_tools/editable_text_short/c003 (-426.889, 376.647, -331.412)
		align left
		alpha 1
		blend_mode normal
		font fonts/font_medium_shadow_mf
		font_color 1.0, 0.0509804, 0.121569
		font_size 1.34
		render_template diffuse_vc_decal
		shape
			1 0
			2 0
			3 1
			4 1
		text C
		vertical center
		word_wrap False
		wrap False
	100009 units/pd2_dlc_des/props/des_prop_chemset_01/001 (33.0, -45.0, 95.0)
	100018 units/pd2_dlc_des/props/des_prop_chemset_02/001 (34.0, 68.0, 95.0)
	100019 units/pd2_dlc_des/props/des_prop_chemset_03/001 (-50.0, 80.0, 100.0)
	100068 units/pd2_dlc_des/props/des_prop_chemset_04/001 (-40.0, -81.0, 95.2296)
	100195 units/pd2_dlc_des/props/des_prop_mix_bowl/001 (-44.2014, -4.59046, 95.2296)
		mesh_variation disable_interaction
	100196 units/pd2_dlc_des/props/prop_mix_bowl/des_interactable_mix_bowl/001 (-44.2014, -4.59046, 95.2296)
		mesh_variation disable_interaction
	100006 units/pd2_dlc_mad/props/shelf_cupboard_desk/mad_prop_forensic_desk_v2/001 (-90.0, 125.0, 0.0)
	100007 units/pd2_dlc_mad/props/shelf_cupboard_desk/mad_prop_forensic_desk_v2/002 (92.0, -125.0, 0.0)
