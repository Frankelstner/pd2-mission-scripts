ID range vs continent name:
	100000: world

statics
	100023 units/pd2_dlc_dah/props/dah_prop_wiring_c/001 (116.0, 1.0, -16.0)
		disable_shadows True
		hide_on_projection_light True
	100024 units/pd2_dlc_dah/props/dah_prop_wiring_c/002 (116.0, 1.0, -16.0)
		disable_shadows True
		hide_on_projection_light True
	100025 units/pd2_dlc_dah/props/dah_prop_wiring_c/003 (117.0, 1.0, -107.0)
		disable_shadows True
		hide_on_projection_light True
	100022 units/pd2_dlc_des/props/des_prop_small_concrete_slab/001 (0.000130415, -31.0, 0.0)
		disable_shadows True
		hide_on_projection_light True
	100018 units/world/props/suburbia_circuitbreaker/002 (0.0, 0.0, 126.0)
		disable_shadows True
		hide_on_projection_light True
