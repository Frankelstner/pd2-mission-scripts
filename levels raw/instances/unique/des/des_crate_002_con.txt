ID range vs continent name:
	100000: world

instances
	levels/instances/unique/des/des_crowbar/001
		continent world
		index_size 150
		position 1400.0, -1300.0, 0.0
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 0
	levels/instances/unique/des/des_crowbar/002
		continent world
		index_size 150
		position 1400.0, -1300.0, 0.0
		rotation 0.0, 0.0, 0.0, 1.0
		script default
		start_index 150
statics
	100016 units/payday2/props/bnk_prop_vault_loot_toast/001 (-4.0, 0.0, 65.5)
		disable_shadows True
	100105 units/pd2_dlc1/props/res_prop_store_beer_six/001 (-16.0, -10.0, 42.5)
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_a
	100104 units/pd2_dlc1/props/res_prop_store_beer_six/002 (-14.0, 17.0, 40.5)
	100106 units/pd2_dlc1/props/res_prop_store_beer_six/003 (12.0, 15.0, 40.5)
	100107 units/pd2_dlc1/props/res_prop_store_beer_six/004 (12.0, -10.0, 40.5)
	100099 units/pd2_dlc_cro/props/off_prop_thebomb_computer/001 (-11.0, 0.0, 39.0)
		mesh_variation screen_off
	100097 units/pd2_dlc_des/props/des_prop_ray_battery/001 (-0.742996, 28.0, 61.3025)
		mesh_variation state_depleted
	100000 units/pd2_dlc_des/props/des_prop_small_crate/001 (0.0, 0.0, 0.0)
		mesh_variation enable_interaction
	100131 units/pd2_dlc_des/props/small_crate/des_prop_hay/001 (0.0, 0.0, 28.0)
	100101 units/pd2_dlc_flat/props/flt_prop_fire_extinguisher/001 (-27.778, 20.875, 51.2622)
	100102 units/pd2_dlc_jfr/props/jfr_crate_wine/001 (-5.0, -1.0, 33.5)
	100098 units/pd2_dlc_peta/props/pta_prop_city_wizardhat/001 (-5.0, -1.0, 52.5)
	100095 units/pd2_dlc_tag/props/tag_prop_plate/001 (-4.0, 0.0, 64.6874)
		disable_shadows True
	100001 units/pd2_indiana/props/mus_prop_egyptian_box/001 (75.0, -50.0, -300.0)
		mesh_variation state_interaction_disable
	100002 units/pd2_indiana/props/mus_prop_egyptian_brosch/001 (-26.0, -48.0, -300.0)
		mesh_variation state_interaction_disable
	100003 units/pd2_indiana/props/mus_prop_egyptian_painting/001 (-1.0, 102.0, -300.0)
		disable_shadows True
		mesh_variation disable_interaction
	100008 units/pd2_indiana/props/mus_prop_exhibit_b_pottery_a/001 (0.0, -100.0, -300.0)
		mesh_variation disable_interaction
	100007 units/pd2_indiana/props/mus_prop_exhibit_b_pottery_b/001 (-25.0, -250.0, -300.0)
		mesh_variation disable_interaction
	100009 units/pd2_indiana/props/mus_prop_exhibit_b_pottery_c/001 (-100.0, -100.0, -300.0)
		mesh_variation disable_interaction
	100010 units/pd2_indiana/props/mus_prop_exhibit_c_ankh/001 (-18.0, 0.0, 50.0)
		mesh_variation state_interaction_disable
	100011 units/pd2_indiana/props/mus_prop_exhibit_c_jewelry_a/001 (-16.9999, 0.000442505, 50.9999)
		mesh_variation state_interaction_disable
	100012 units/pd2_indiana/props/mus_prop_exhibit_c_jewelry_b/001 (0.0, 200.0, -300.0)
		mesh_variation state_interaction_disable
	100013 units/pd2_indiana/props/mus_prop_exhibit_c_necklace_a/001 (-18.9998, 0.000228882, 50.0)
		mesh_variation state_interaction_disable
	100014 units/pd2_indiana/props/mus_prop_exhibit_c_necklace_b/001 (4.62532e-05, 0.000228882, 48.0)
		mesh_variation state_interaction_disable
	100015 units/pd2_indiana/props/mus_prop_exhibit_c_necklace_c/001 (-16.9997, -4.57764e-05, 51.0)
		mesh_variation state_interaction_disable
	100017 units/pd2_indiana/props/mus_prop_exhibit_d_sword/001 (100.0, -400.0, -300.0)
		mesh_variation disable_interaction
	100018 units/pd2_indiana/props/mus_prop_exhibit_d_token/001 (1.62125e-05, 3.05176e-05, 51.0)
		mesh_variation state_interaction_disable
	100096 units/world/props/diamond_heist/apartment/apartment_livingroom/pillow_smooth/001 (-4.0, 0.0, 55.9672)
		disable_shadows True
	100100 units/world/props/toys/zombie_hunter/001 (-2.0, -1.0, 50.0)
