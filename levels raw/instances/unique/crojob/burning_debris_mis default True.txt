﻿´effect_fire_debris´ ElementPlayEffect 100001
	EXECUTE ON STARTUP
	base_time 0
	effect effects/payday2/environment/fire_big
	max_amount 0
	position 11.0, 40.0, -99.0
	random_time 0
	rotation 0.0, 0.0, -0.382683, -0.92388
	screen_space False
´trigger_area_001´ ElementAreaTrigger 100008
	amount 1
	depth 500
	height 150
	instigator player
	interval 0.1
	position -12.0, 1.0, -69.0
	radius 75
	rotation 0.0, 0.0, 0.0, -1.0
	shape_type cylinder
	spawn_unit_elements
	trigger_on both
	use_disabled_shapes False
	width 0
	on_executed
		´func_kill_zone_001´ (delay 0)
´func_kill_zone_001´ ElementKillZone 100015
	position -200.0, 400.0, -100.0
	rotation 0.0, 0.0, 0.0, -1.0
	type fire
