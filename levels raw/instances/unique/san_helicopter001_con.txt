ID range vs continent name:
	100000: world

statics
	100003 units/payday2/architecture/mkp_ext_wall_1m_a/001 (0.0, 0.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100009 units/payday2/props/gen_prop_square_goal_marker_8x15/001 (0.0, 350.0, 0.0)
	100020 units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/heli (4.0, 313.0, -100.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.861, 0.584
			enabled True
			falloff_exponent 4
			far_range 150
			multiplier identity
			name lo_omni
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
