ID range vs continent name:
	100000: world

statics
	100025 core/units/light_omni/001 (3.0, -7.0, 26.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.243137, 0.243137
			enabled True
			falloff_exponent 1
			far_range 25
			multiplier sun
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100032 units/pd2_dlc_tag/props/tag_prop_office_telephone/001 (0.0, 0.0, 0.0)
		disable_shadows True
		hide_on_projection_light True
