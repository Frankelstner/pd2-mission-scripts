ID range vs continent name:
	100000: world

statics
	100050 core/units/light_omni/001 (1.00606, -67.0, 180.685)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.321569, 0.458824, 0.596078
			enabled True
			falloff_exponent 1
			far_range 300
			multiplier match
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100008 units/pd2_dlc_dah/props/dah_prop_whiteboard_b/001 (0.0, 0.0, 100.0)
		disable_shadows True
		hide_on_projection_light True
	100055 units/pd2_dlc_tag/props/signs/tag_prop_whiteboard_sign_01/001 (-83.0, 7.0, 133.0)
