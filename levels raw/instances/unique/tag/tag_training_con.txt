ID range vs continent name:
	100000: world

statics
	100062 core/units/light_omni/004 (-858.0, 500.0, 128.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.392157, 0.533333, 0.47451
			enabled True
			falloff_exponent 1
			far_range 230
			multiplier match
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100089 core/units/light_omni/blue (-530.0, 402.0, 150.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.392157, 0.533333, 0.47451
			enabled True
			falloff_exponent 1
			far_range 650
			multiplier match
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100029 units/dev_tools/level_tools/dev_nav_blocker_1x3m/001 (-325.0, 150.0, 0.0)
	100030 units/dev_tools/level_tools/dev_nav_blocker_1x3m/002 (-325.0, 250.0, 0.0)
	100034 units/dev_tools/level_tools/dev_nav_blocker_1x3m/004 (-225.0, 150.0, 0.0)
	100094 units/dev_tools/level_tools/dev_nav_blocker_1x3m/005 (-175.0, 275.0, 0.0)
	100103 units/dev_tools/level_tools/dev_nav_blocker_1x3m/006 (-175.0, 150.0, 0.0)
	100109 units/dev_tools/level_tools/dev_nav_blocker_1x3m/007 (-143.0, 275.0, 0.0)
	100061 units/lights/light_omni_shadow_projection_01/001 (-764.0, 230.0, 175.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.827451, 0.901961, 0.74902
			enabled True
			falloff_exponent 1
			far_range 375
			multiplier neonsign
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
		projection_light lo_omni
	100099 units/lights/light_omni_shadow_projection_01/002 (-764.0, 631.0, 200.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.827451, 0.901961, 0.74902
			enabled True
			falloff_exponent 1
			far_range 375
			multiplier neonsign
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
		projection_light lo_omni
	100100 units/lights/light_omni_shadow_projection_01/003 (-239.0, 230.0, 175.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.827451, 0.901961, 0.74902
			enabled True
			falloff_exponent 1
			far_range 375
			multiplier neonsign
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
		projection_light lo_omni
	100101 units/lights/light_omni_shadow_projection_01/004 (-239.0, 631.0, 200.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.827451, 0.901961, 0.74902
			enabled True
			falloff_exponent 1
			far_range 375
			multiplier neonsign
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
		projection_light lo_omni
	100018 units/lights/light_projection_01/001 (-602.003, 389.488, 313.064)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 1.0
			enabled True
			falloff_exponent 1
			far_range 450
			multiplier searchlight
			name ls_spot
			near_range 80
			spot_angle_end 37
			spot_angle_start 43
		projection_light ls_spot
		ls_spot units/lights/spot_light_projection_textures/chl_projector_yacht_movie_df
	100064 units/payday2/props/gen_prop_appliance_projector/001 (-574.0, 378.0, 372.5)
	100008 units/payday2/props/off_prop_officehigh_projectorscreen/001 (-943.839, 564.924, 331.776)
	100014 units/payday2/props/pictures/lxa_prop_hallway_picture_02/001 (-675.0, 25.0, 150.0)
		disable_shadows True
		hide_on_projection_light True
	100065 units/payday2/props/pictures/lxa_prop_hallway_picture_03/001 (-25.0, 275.0, 150.0)
	100096 units/payday2/props/pictures/lxa_prop_hallway_picture_04/001 (-200.0, 775.0, 150.0)
	100017 units/payday2/props/stn_prop_lobby_flag_us/001 (-1018.6, 57.9492, 0.0)
		disable_shadows True
	100053 units/payday2/props/stn_prop_lobby_flag_us/002 (-57.9996, 739.0, 0.0)
		disable_shadows True
	100077 units/payday2/props/stn_prop_lobby_mostwanted/001 (-673.0, 775.0, 139.905)
		disable_shadows True
	100013 units/pd2_dlc_casino/props/cas_prop_entrance_valet_podium/001 (-760.0, 376.001, 0.934998)
		disable_shadows True
		mesh_variation state_interaction_disabled
	100095 units/pd2_dlc_chill/architecture/chl_int_mullplan_round/001 (-828.0, 753.0, 2.50659)
	100067 units/pd2_dlc_dah/props/dah_prop_light_switch/001 (-818.0, 25.0, 124.935)
		disable_shadows True
		hide_on_projection_light True
	100068 units/pd2_dlc_dah/props/dah_prop_light_switch/002 (-25.0, 450.0, 125.0)
		disable_shadows True
		hide_on_projection_light True
	100078 units/pd2_dlc_dah/props/dah_prop_light_switch/003 (-268.0, 25.0001, 124.935)
		disable_shadows True
		hide_on_projection_light True
	100066 units/pd2_dlc_dah/props/dah_prop_outlet/001 (-818.0, 27.0, 49.935)
		disable_shadows True
		hide_on_projection_light True
	100072 units/pd2_dlc_dah/props/dah_prop_outlet/002 (-27.0, 450.0, 50.0)
		disable_shadows True
		hide_on_projection_light True
	100079 units/pd2_dlc_dah/props/dah_prop_outlet/003 (-268.0, 27.0001, 49.935)
		disable_shadows True
		hide_on_projection_light True
	100083 units/pd2_dlc_dah/props/dah_prop_outlet/004 (-808.0, 772.0, 50.0)
		disable_shadows True
		hide_on_projection_light True
	100031 units/pd2_dlc_dah/props/dah_prop_paper_bin_v1/001 (-828.0, 753.0, 2.50659)
	100073 units/pd2_dlc_holly/mansion/props/lxa_prop_hallway_sprinkler/001 (-363.0, 230.0, 368.0)
		disable_shadows True
		hide_on_projection_light True
	100076 units/pd2_dlc_holly/mansion/props/lxa_prop_hallway_sprinkler/002 (-629.0, 630.0, 368.0)
		disable_shadows True
		hide_on_projection_light True
	100012 units/pd2_dlc_mad/props/mad_prop_whiteboard/001 (-1043.0, 250.001, 175.0)
		disable_shadows True
	100022 units/pd2_dlc_tag/architecture/main_mesh/tag_int_room_inst_01_training/001 (0.0, 0.0, 0.0)
	100026 units/pd2_dlc_tag/architecture/main_mesh/tag_int_room_inst_02_training/001 (0.0, 0.0, 0.0)
	100019 units/pd2_dlc_tag/architecture/tag_int/windows_doors/tag_int_door_frame_01/001 (-90.0001, 23.0001, 0.0)
		disable_shadows True
	100010 units/pd2_dlc_tag/architecture/tag_int/windows_doors/tag_int_door_frame_01/002 (-22.0, 630.0, 0.0)
		disable_shadows True
	100011 units/pd2_dlc_tag/architecture/tag_int/windows_doors/tag_int_door_frame_01/003 (-845.0, 23.0, 0.0)
		disable_shadows True
	100108 units/pd2_dlc_tag/props/tag_prop_crescent_table/001 (-569.0, 397.0, 0.0)
		disable_shadows True
	100021 units/pd2_dlc_tag/props/tag_prop_crescent_table/002 (-321.0, 397.0, 0.0)
		disable_shadows True
	100027 units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/001 (-230.0, 230.0, 375.0)
		mesh_variation light_off
	100051 units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/002 (-764.0, 230.0, 375.0)
		mesh_variation light_off
	100052 units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/003 (-764.0, 631.0, 375.0)
		mesh_variation light_off
	100054 units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/004 (-229.0, 631.0, 375.0)
		mesh_variation light_off
	100049 units/pd2_dlc_tag/props/tag_prop_sign_rooms/001 (-750.0, 0.0, 150.0)
		disable_shadows True
		hide_on_projection_light True
		mesh_variation sign_training
	100050 units/pd2_dlc_tag/props/tag_prop_sign_rooms/002 (0.0, 400.0, 150.0)
		disable_shadows True
		hide_on_projection_light True
		mesh_variation sign_training
	100024 units/world/architecture/hospital/props/chair_cafeteria/001 (-491.0, 364.0, 0.0)
		disable_shadows True
	100033 units/world/architecture/hospital/props/chair_cafeteria/002 (-519.0, 178.0, 0.0)
		disable_shadows True
	100036 units/world/architecture/hospital/props/chair_cafeteria/003 (-427.0, 225.0, 0.0)
		disable_shadows True
	100037 units/world/architecture/hospital/props/chair_cafeteria/004 (-410.0, 435.0, 0.0)
		disable_shadows True
	100039 units/world/architecture/hospital/props/chair_cafeteria/005 (-475.0, 556.0, 0.0)
		disable_shadows True
	100040 units/world/architecture/hospital/props/chair_cafeteria/006 (-515.0, 626.0, 0.0)
		disable_shadows True
	100041 units/world/architecture/hospital/props/chair_cafeteria/007 (-177.0, 635.0, 0.0)
		disable_shadows True
	100042 units/world/architecture/hospital/props/chair_cafeteria/008 (-240.07, 546.23, 0.0)
		disable_shadows True
	100043 units/world/architecture/hospital/props/chair_cafeteria/009 (-158.0, 441.0, 0.0)
		disable_shadows True
	100044 units/world/architecture/hospital/props/chair_cafeteria/010 (-243.0, 342.0, 0.0)
		disable_shadows True
	100045 units/world/architecture/hospital/props/chair_cafeteria/011 (-158.0, 281.0, 0.0)
		disable_shadows True
	100046 units/world/architecture/hospital/props/chair_cafeteria/012 (-184.0, 161.0, 0.0)
		disable_shadows True
