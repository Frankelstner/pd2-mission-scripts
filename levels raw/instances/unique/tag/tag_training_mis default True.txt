﻿´startup´ MissionScriptElement 100000
	EXECUTE ON STARTUP
	on_executed
		´delay´ (delay 1)
´delay´ MissionScriptElement 100001
	on_executed
		´seq_setup´ (delay 0)
		´unit_disabler´ (delay 0)
´test(disabled_by_default)´ MissionScriptElement 100002
	EXECUTE ON STARTUP
	DISABLED
	on_executed
		´point_spawn_player_001´ (delay 0)
		´test_delay´ (delay 3)
		´func_whisper_state_001´ (delay 0)
´point_spawn_player_001´ ElementPlayerSpawner 100003
	position -500.0, -350.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	state standard
´test_delay´ MissionScriptElement 100004
	on_executed
		´input_show_room_layout_A´ (delay 0)
		´input_show_people´ (delay 0)
´seq_setup´ ElementUnitSequence 100005
	position 600.0, 100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_sign_rooms/001 (-750.0, 0.0, 150.0)
			notify_unit_sequence hide_all
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_sign_rooms/002 (0.0, 400.0, 150.0)
			notify_unit_sequence hide_all
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/001 (-230.0, 230.0, 375.0)
			notify_unit_sequence hide
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/002 (-764.0, 230.0, 375.0)
			notify_unit_sequence hide
			time 0
		5
			id 5
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/003 (-764.0, 631.0, 375.0)
			notify_unit_sequence hide
			time 0
		6
			id 6
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/004 (-229.0, 631.0, 375.0)
			notify_unit_sequence hide
			time 0
´unit_disabler´ ElementDisableUnit 100006
	position 600.0, 0.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/pd2_dlc_casino/props/cas_prop_entrance_valet_podium/001 (-760.0, 376.001, 0.934998)
		2 units/world/architecture/hospital/props/chair_cafeteria/001 (-491.0, 364.0, 0.0)
		3 units/world/architecture/hospital/props/chair_cafeteria/002 (-519.0, 178.0, 0.0)
		4 units/world/architecture/hospital/props/chair_cafeteria/003 (-427.0, 225.0, 0.0)
		5 units/world/architecture/hospital/props/chair_cafeteria/004 (-410.0, 435.0, 0.0)
		6 units/world/architecture/hospital/props/chair_cafeteria/005 (-475.0, 556.0, 0.0)
		7 units/world/architecture/hospital/props/chair_cafeteria/006 (-515.0, 626.0, 0.0)
		8 units/world/architecture/hospital/props/chair_cafeteria/007 (-177.0, 635.0, 0.0)
		9 units/world/architecture/hospital/props/chair_cafeteria/008 (-240.07, 546.23, 0.0)
		10 units/world/architecture/hospital/props/chair_cafeteria/009 (-158.0, 441.0, 0.0)
		11 units/world/architecture/hospital/props/chair_cafeteria/010 (-243.0, 342.0, 0.0)
		12 units/world/architecture/hospital/props/chair_cafeteria/011 (-158.0, 281.0, 0.0)
		13 units/world/architecture/hospital/props/chair_cafeteria/012 (-184.0, 161.0, 0.0)
		14 units/pd2_dlc_tag/architecture/main_mesh/tag_int_room_inst_01_training/001 (0.0, 0.0, 0.0)
		15 units/pd2_dlc_tag/architecture/main_mesh/tag_int_room_inst_02_training/001 (0.0, 0.0, 0.0)
		16 units/lights/light_projection_01/001 (-602.003, 389.488, 313.064)
		17 units/pd2_dlc_mad/props/mad_prop_whiteboard/001 (-1043.0, 250.001, 175.0)
		18 units/payday2/props/stn_prop_lobby_flag_us/001 (-1018.6, 57.9492, 0.0)
		19 units/payday2/props/stn_prop_lobby_flag_us/002 (-57.9996, 739.0, 0.0)
		20 units/payday2/props/stn_prop_lobby_mostwanted/001 (-673.0, 775.0, 139.905)
		21 units/pd2_dlc_tag/architecture/tag_int/windows_doors/tag_int_door_frame_01/001 (-90.0001, 23.0001, 0.0)
		22 units/pd2_dlc_tag/architecture/tag_int/windows_doors/tag_int_door_frame_01/002 (-22.0, 630.0, 0.0)
		23 units/pd2_dlc_tag/architecture/tag_int/windows_doors/tag_int_door_frame_01/003 (-845.0, 23.0, 0.0)
		24 units/lights/light_omni_shadow_projection_01/001 (-764.0, 230.0, 175.0)
		25 core/units/light_omni/004 (-858.0, 500.0, 128.0)
		26 units/payday2/props/gen_prop_appliance_projector/001 (-574.0, 378.0, 372.5)
		27 units/payday2/props/off_prop_officehigh_projectorscreen/001 (-943.839, 564.924, 331.776)
		28 units/payday2/props/pictures/lxa_prop_hallway_picture_02/001 (-675.0, 25.0, 150.0)
		29 units/payday2/props/pictures/lxa_prop_hallway_picture_03/001 (-25.0, 275.0, 150.0)
		30 units/pd2_dlc_holly/mansion/props/lxa_prop_hallway_sprinkler/001 (-363.0, 230.0, 368.0)
		31 units/pd2_dlc_holly/mansion/props/lxa_prop_hallway_sprinkler/002 (-629.0, 630.0, 368.0)
		32 units/pd2_dlc_dah/props/dah_prop_light_switch/001 (-818.0, 25.0, 124.935)
		33 units/pd2_dlc_dah/props/dah_prop_light_switch/002 (-25.0, 450.0, 125.0)
		34 units/pd2_dlc_dah/props/dah_prop_outlet/001 (-818.0, 27.0, 49.935)
		35 units/pd2_dlc_dah/props/dah_prop_outlet/002 (-27.0, 450.0, 50.0)
		36 units/pd2_dlc_dah/props/dah_prop_outlet/004 (-808.0, 772.0, 50.0)
		37 units/pd2_dlc_dah/props/dah_prop_outlet/003 (-268.0, 27.0001, 49.935)
		38 units/pd2_dlc_dah/props/dah_prop_light_switch/003 (-268.0, 25.0001, 124.935)
		39 core/units/light_omni/blue (-530.0, 402.0, 150.0)
		40 units/pd2_dlc_tag/props/tag_prop_crescent_table/001 (-569.0, 397.0, 0.0)
		41 units/pd2_dlc_tag/props/tag_prop_crescent_table/002 (-321.0, 397.0, 0.0)
		42 units/pd2_dlc_dah/props/dah_prop_paper_bin_v1/001 (-828.0, 753.0, 2.50659)
		43 units/pd2_dlc_chill/architecture/chl_int_mullplan_round/001 (-828.0, 753.0, 2.50659)
		44 units/payday2/props/pictures/lxa_prop_hallway_picture_04/001 (-200.0, 775.0, 150.0)
		45 units/lights/light_omni_shadow_projection_01/002 (-764.0, 631.0, 200.0)
		46 units/lights/light_omni_shadow_projection_01/004 (-239.0, 631.0, 200.0)
		47 units/lights/light_omni_shadow_projection_01/003 (-239.0, 230.0, 175.0)
´input_show_room_layout_A´ ElementInstanceInput 100007
	event show_room_layout_A
	on_executed
		´func_enable_unit_001´ (delay 0)
		´func_sequence_002´ (delay 0)
		´enable_lights_projection´ (delay 0)
´input_show_people´ ElementInstanceInput 100069
	BASE DELAY 0.5
	event show_people
	on_executed
		´enable_lights´ (delay 0)
		´ai_spawn_enemy_007´ (delay 0)
		´ai_spawn_enemy_004´ (delay 0)
		´ai_spawn_enemy_002´ (delay 0)
		´ai_spawn_enemy_005´ (delay 0)
		´ai_spawn_enemy_006´ (delay 0)
		´ai_spawn_enemy_001´ (delay 0)
		´ai_spawn_enemy_008´ (delay 0)
		´ai_spawn_enemy_009´ (delay 0)
		´ai_spawn_enemy_010´ (delay 0)
		´sound_start´ (delay 15-20)
		´func_sequence_001´ (delay 0)
		´enable_lights_projection´ (delay 0)
		´logic_random_001´ (delay 30-45)
´input_show_room_layout_B´ ElementInstanceInput 100070
	event show_room_layout_B
	on_executed
		´func_enable_unit_002´ (delay 0)
		´func_sequence_002´ (delay 0)
		´enable_lights_projection´ (delay 0)
´func_enable_unit_001´ ElementEnableUnit 100071
	position 900.0, 100.0, 2.5
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/pd2_dlc_casino/props/cas_prop_entrance_valet_podium/001 (-760.0, 376.001, 0.934998)
		2 units/world/architecture/hospital/props/chair_cafeteria/001 (-491.0, 364.0, 0.0)
		3 units/world/architecture/hospital/props/chair_cafeteria/002 (-519.0, 178.0, 0.0)
		4 units/world/architecture/hospital/props/chair_cafeteria/003 (-427.0, 225.0, 0.0)
		5 units/world/architecture/hospital/props/chair_cafeteria/004 (-410.0, 435.0, 0.0)
		6 units/world/architecture/hospital/props/chair_cafeteria/005 (-475.0, 556.0, 0.0)
		7 units/world/architecture/hospital/props/chair_cafeteria/006 (-515.0, 626.0, 0.0)
		8 units/world/architecture/hospital/props/chair_cafeteria/007 (-177.0, 635.0, 0.0)
		9 units/world/architecture/hospital/props/chair_cafeteria/008 (-240.07, 546.23, 0.0)
		10 units/world/architecture/hospital/props/chair_cafeteria/009 (-158.0, 441.0, 0.0)
		11 units/world/architecture/hospital/props/chair_cafeteria/010 (-243.0, 342.0, 0.0)
		12 units/world/architecture/hospital/props/chair_cafeteria/011 (-158.0, 281.0, 0.0)
		13 units/world/architecture/hospital/props/chair_cafeteria/012 (-184.0, 161.0, 0.0)
		14 units/pd2_dlc_tag/props/tag_prop_crescent_table/002 (-321.0, 397.0, 0.0)
		15 units/pd2_dlc_tag/architecture/main_mesh/tag_int_room_inst_02_training/001 (0.0, 0.0, 0.0)
		16 units/pd2_dlc_mad/props/mad_prop_whiteboard/001 (-1043.0, 250.001, 175.0)
		17 units/payday2/props/stn_prop_lobby_flag_us/001 (-1018.6, 57.9492, 0.0)
		18 units/payday2/props/stn_prop_lobby_flag_us/002 (-57.9996, 739.0, 0.0)
		19 units/payday2/props/stn_prop_lobby_mostwanted/001 (-673.0, 775.0, 139.905)
		20 units/pd2_dlc_tag/architecture/tag_int/windows_doors/tag_int_door_frame_01/002 (-22.0, 630.0, 0.0)
		21 units/pd2_dlc_tag/architecture/tag_int/windows_doors/tag_int_door_frame_01/003 (-845.0, 23.0, 0.0)
		22 units/payday2/props/gen_prop_appliance_projector/001 (-574.0, 378.0, 372.5)
		23 units/payday2/props/off_prop_officehigh_projectorscreen/001 (-943.839, 564.924, 331.776)
		24 units/payday2/props/pictures/lxa_prop_hallway_picture_02/001 (-675.0, 25.0, 150.0)
		25 units/payday2/props/pictures/lxa_prop_hallway_picture_03/001 (-25.0, 275.0, 150.0)
		26 units/pd2_dlc_holly/mansion/props/lxa_prop_hallway_sprinkler/001 (-363.0, 230.0, 368.0)
		27 units/pd2_dlc_holly/mansion/props/lxa_prop_hallway_sprinkler/002 (-629.0, 630.0, 368.0)
		28 units/pd2_dlc_dah/props/dah_prop_light_switch/001 (-818.0, 25.0, 124.935)
		29 units/pd2_dlc_dah/props/dah_prop_light_switch/002 (-25.0, 450.0, 125.0)
		30 units/pd2_dlc_dah/props/dah_prop_outlet/001 (-818.0, 27.0, 49.935)
		31 units/pd2_dlc_dah/props/dah_prop_outlet/002 (-27.0, 450.0, 50.0)
		32 units/pd2_dlc_dah/props/dah_prop_outlet/004 (-808.0, 772.0, 50.0)
		33 core/units/light_omni/blue (-530.0, 402.0, 150.0)
		34 units/pd2_dlc_tag/props/tag_prop_crescent_table/001 (-569.0, 397.0, 0.0)
		35 units/pd2_dlc_dah/props/dah_prop_paper_bin_v1/001 (-828.0, 753.0, 2.50659)
		36 units/pd2_dlc_chill/architecture/chl_int_mullplan_round/001 (-828.0, 753.0, 2.50659)
		37 units/payday2/props/pictures/lxa_prop_hallway_picture_04/001 (-200.0, 775.0, 150.0)
´func_enable_unit_002´ ElementEnableUnit 100074
	position 1000.0, 100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/pd2_dlc_casino/props/cas_prop_entrance_valet_podium/001 (-760.0, 376.001, 0.934998)
		2 units/world/architecture/hospital/props/chair_cafeteria/001 (-491.0, 364.0, 0.0)
		3 units/world/architecture/hospital/props/chair_cafeteria/002 (-519.0, 178.0, 0.0)
		4 units/world/architecture/hospital/props/chair_cafeteria/003 (-427.0, 225.0, 0.0)
		5 units/world/architecture/hospital/props/chair_cafeteria/004 (-410.0, 435.0, 0.0)
		6 units/world/architecture/hospital/props/chair_cafeteria/005 (-475.0, 556.0, 0.0)
		7 units/world/architecture/hospital/props/chair_cafeteria/006 (-515.0, 626.0, 0.0)
		8 units/world/architecture/hospital/props/chair_cafeteria/007 (-177.0, 635.0, 0.0)
		9 units/world/architecture/hospital/props/chair_cafeteria/008 (-240.07, 546.23, 0.0)
		10 units/world/architecture/hospital/props/chair_cafeteria/009 (-158.0, 441.0, 0.0)
		11 units/world/architecture/hospital/props/chair_cafeteria/010 (-243.0, 342.0, 0.0)
		12 units/world/architecture/hospital/props/chair_cafeteria/011 (-158.0, 281.0, 0.0)
		13 units/world/architecture/hospital/props/chair_cafeteria/012 (-184.0, 161.0, 0.0)
		14 units/pd2_dlc_tag/props/tag_prop_crescent_table/002 (-321.0, 397.0, 0.0)
		15 units/pd2_dlc_tag/architecture/main_mesh/tag_int_room_inst_01_training/001 (0.0, 0.0, 0.0)
		16 units/pd2_dlc_mad/props/mad_prop_whiteboard/001 (-1043.0, 250.001, 175.0)
		17 units/payday2/props/stn_prop_lobby_flag_us/001 (-1018.6, 57.9492, 0.0)
		18 units/payday2/props/stn_prop_lobby_flag_us/002 (-57.9996, 739.0, 0.0)
		19 units/payday2/props/stn_prop_lobby_mostwanted/001 (-673.0, 775.0, 139.905)
		20 units/pd2_dlc_tag/architecture/tag_int/windows_doors/tag_int_door_frame_01/001 (-90.0001, 23.0001, 0.0)
		21 units/pd2_dlc_tag/architecture/tag_int/windows_doors/tag_int_door_frame_01/003 (-845.0, 23.0, 0.0)
		22 units/payday2/props/gen_prop_appliance_projector/001 (-574.0, 378.0, 372.5)
		23 units/payday2/props/off_prop_officehigh_projectorscreen/001 (-943.839, 564.924, 331.776)
		24 units/payday2/props/pictures/lxa_prop_hallway_picture_02/001 (-675.0, 25.0, 150.0)
		25 units/payday2/props/pictures/lxa_prop_hallway_picture_03/001 (-25.0, 275.0, 150.0)
		26 units/pd2_dlc_dah/props/dah_prop_light_switch/001 (-818.0, 25.0, 124.935)
		27 units/pd2_dlc_dah/props/dah_prop_outlet/001 (-818.0, 27.0, 49.935)
		28 units/pd2_dlc_dah/props/dah_prop_outlet/003 (-268.0, 27.0001, 49.935)
		29 units/pd2_dlc_dah/props/dah_prop_light_switch/003 (-268.0, 25.0001, 124.935)
		30 units/pd2_dlc_dah/props/dah_prop_outlet/004 (-808.0, 772.0, 50.0)
		31 core/units/light_omni/blue (-530.0, 402.0, 150.0)
		32 units/pd2_dlc_tag/props/tag_prop_crescent_table/001 (-569.0, 397.0, 0.0)
		33 units/pd2_dlc_dah/props/dah_prop_paper_bin_v1/001 (-828.0, 753.0, 2.50659)
		34 units/pd2_dlc_chill/architecture/chl_int_mullplan_round/001 (-828.0, 753.0, 2.50659)
		35 units/payday2/props/pictures/lxa_prop_hallway_picture_04/001 (-200.0, 775.0, 150.0)
		36 units/pd2_dlc_holly/mansion/props/lxa_prop_hallway_sprinkler/002 (-629.0, 630.0, 368.0)
´enable_lights´ ElementEnableUnit 100075
	position 1100.0, 100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 core/units/light_omni/004 (-858.0, 500.0, 128.0)
		2 units/lights/light_omni_shadow_projection_01/001 (-764.0, 230.0, 175.0)
		3 units/lights/light_omni_shadow_projection_01/004 (-239.0, 631.0, 200.0)
		4 units/lights/light_omni_shadow_projection_01/003 (-239.0, 230.0, 175.0)
		5 units/lights/light_omni_shadow_projection_01/002 (-764.0, 631.0, 200.0)
	on_executed
		´func_disable_unit_001´ (delay 0)
´func_whisper_state_001´ ElementWhisperState 100080
	position 1300.0, 300.0, 2.5
	rotation 0.0, 0.0, 0.0, -1.0
	state True
´ai_spawn_enemy_001´ ElementSpawnEnemyDummy 100081
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_fbi_office_1/ene_fbi_office_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position -482.0, 461.0, 2.0
	rotation 0.0, 0.0, -0.622515, -0.782608
	spawn_action none
	team default
	voice 0
	on_executed
		´point_special_objective_008´ (delay 0)
´ai_spawn_enemy_002´ ElementSpawnEnemyDummy 100082
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_fbi_2/ene_fbi_2
	force_pickup none
	interval 5
	participate_to_group_ai True
	position -461.0, 659.0, 0.0
	rotation 0.0, 0.0, -0.649448, -0.760406
	spawn_action none
	team default
	voice 0
	on_executed
		´point_special_objective_002´ (delay 0)
´ai_spawn_enemy_004´ ElementSpawnEnemyDummy 100084
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_fbi_office_2/ene_fbi_office_2
	force_pickup none
	interval 5
	participate_to_group_ai True
	position -492.0, 248.0, 2.0
	rotation 0.0, 0.0, -0.529919, -0.848048
	spawn_action none
	team default
	voice 0
	on_executed
		´point_special_objective_005´ (delay 0)
´ai_spawn_enemy_005´ ElementSpawnEnemyDummy 100085
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_fbi_female_3/ene_fbi_female_3
	force_pickup none
	interval 5
	participate_to_group_ai True
	position -247.0, 638.0, 0.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	spawn_action none
	team default
	voice 0
	on_executed
		´point_special_objective_006´ (delay 0)
´ai_spawn_enemy_006´ ElementSpawnEnemyDummy 100086
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_fbi_female_4/ene_fbi_female_4
	force_pickup none
	interval 5
	participate_to_group_ai True
	position -221.0, 437.0, 2.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	spawn_action none
	team default
	voice 0
	on_executed
		´point_special_objective_007´ (delay 0)
´ai_spawn_enemy_007´ ElementSpawnEnemyDummy 100087
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_fbi_boss_1/ene_fbi_boss_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position -867.0, 400.0, 0.0
	rotation 0.0, 0.0, 0.777146, -0.62932
	spawn_action none
	team default
	voice 0
	on_executed
		´point_special_objective_004´ (delay 0)
´ai_spawn_enemy_008´ ElementSpawnEnemyDummy 100088
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_fbi_office_4/ene_fbi_office_4
	force_pickup none
	interval 5
	participate_to_group_ai True
	position -245.0, 191.0, 2.0
	rotation 0.0, 0.0, -0.507538, -0.861629
	spawn_action none
	team default
	voice 0
	on_executed
		´point_special_objective_009´ (delay 0)
´point_special_objective_002´ ElementSpecialObjective 100090
	SO_access 1020
	action_duration_max 0
	action_duration_min 0
	ai_group enemies
	align_position False
	align_rotation False
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis -1
	interrupt_dmg 0
	interrupt_objective False
	interval -1
	is_navigation_link False
	needs_pos_rsrv False
	path_haste walk
	path_stance ntl
	path_style destination
	patrol_path none
	pose none
	position -461.0, 659.0, 0.0
	rotation 0.0, 0.0, -0.649448, -0.760406
	scan False
	search_distance 0
	search_position -850.0, 250.0, 0.0
	so_action e_so_sit_student_var1
	trigger_on none
	use_instigator True
´point_special_objective_004´ ElementSpecialObjective 100092
	SO_access 1020
	action_duration_max 0
	action_duration_min 0
	ai_group enemies
	align_position False
	align_rotation False
	attitude none
	base_chance 1
	chance_inc 0
	forced False
	interaction_voice none
	interrupt_dis -1
	interrupt_dmg 0
	interrupt_objective False
	interval -1
	is_navigation_link False
	needs_pos_rsrv False
	path_haste walk
	path_stance ntl
	path_style destination
	patrol_path none
	pose none
	position -867.0, 400.0, 0.0
	rotation 0.0, 0.0, 0.777146, -0.62932
	scan False
	search_distance 0
	search_position -850.0, 250.0, 0.0
	so_action e_so_ntl_bored
	trigger_on none
	use_instigator True
´point_special_objective_005´ ElementSpecialObjective 100093
	SO_access 1020
	action_duration_max 0
	action_duration_min 0
	ai_group enemies
	align_position False
	align_rotation False
	attitude none
	base_chance 1
	chance_inc 0
	forced True
	interaction_voice none
	interrupt_dis -1
	interrupt_dmg 0
	interrupt_objective False
	interval -1
	is_navigation_link False
	needs_pos_rsrv False
	path_haste walk
	path_stance ntl
	path_style destination
	patrol_path none
	pose none
	position -492.0, 248.0, 2.0
	rotation 0.0, 0.0, -0.529919, -0.848048
	scan False
	search_distance 0
	search_position -850.0, 250.0, 0.0
	so_action e_so_sit_student_var3
	trigger_on none
	use_instigator True
´point_special_objective_006´ ElementSpecialObjective 100104
	SO_access 1020
	action_duration_max 0
	action_duration_min 0
	ai_group enemies
	align_position False
	align_rotation False
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis -1
	interrupt_dmg 0
	interrupt_objective False
	interval -1
	is_navigation_link False
	needs_pos_rsrv False
	path_haste walk
	path_stance ntl
	path_style destination
	patrol_path none
	pose none
	position -247.0, 638.0, 0.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	scan False
	search_distance 0
	search_position -850.0, 250.0, 0.0
	so_action e_so_sit_student_var4
	trigger_on none
	use_instigator True
´point_special_objective_007´ ElementSpecialObjective 100105
	SO_access 1020
	action_duration_max 0
	action_duration_min 0
	ai_group enemies
	align_position False
	align_rotation False
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis -1
	interrupt_dmg 0
	interrupt_objective False
	interval -1
	is_navigation_link False
	needs_pos_rsrv False
	path_haste walk
	path_stance ntl
	path_style destination
	patrol_path none
	pose none
	position -221.0, 437.0, 2.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	scan False
	search_distance 0
	search_position -850.0, 250.0, 0.0
	so_action e_so_sit_student_var6
	trigger_on none
	use_instigator True
´point_special_objective_008´ ElementSpecialObjective 100106
	SO_access 1020
	action_duration_max 0
	action_duration_min 0
	ai_group enemies
	align_position False
	align_rotation False
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis -1
	interrupt_dmg 0
	interrupt_objective False
	interval -1
	is_navigation_link False
	needs_pos_rsrv False
	path_haste walk
	path_stance ntl
	path_style destination
	patrol_path none
	pose none
	position -482.0, 461.0, 2.0
	rotation 0.0, 0.0, -0.622515, -0.782608
	scan False
	search_distance 0
	search_position -850.0, 250.0, 0.0
	so_action e_so_sit_student_var8
	trigger_on none
	use_instigator True
´point_special_objective_009´ ElementSpecialObjective 100107
	SO_access 1020
	action_duration_max 0
	action_duration_min 0
	ai_group enemies
	align_position False
	align_rotation False
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis -1
	interrupt_dmg 0
	interrupt_objective False
	interval -1
	is_navigation_link False
	needs_pos_rsrv False
	path_haste walk
	path_stance ntl
	path_style destination
	patrol_path none
	pose none
	position -245.0, 191.0, 2.0
	rotation 0.0, 0.0, -0.507538, -0.861629
	scan False
	search_distance 0
	search_position -850.0, 250.0, 0.0
	so_action e_so_sit_student_var5
	trigger_on none
	use_instigator True
´ai_spawn_enemy_009´ ElementSpawnEnemyDummy 100016
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_secret_service_1/ene_secret_service_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position -47.0, 647.0, 0.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	spawn_action none
	team default
	voice 0
´ai_spawn_enemy_010´ ElementSpawnEnemyDummy 100023
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_secret_service_1/ene_secret_service_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position -728.0, 57.0, 0.0
	rotation 0.0, 0.0, 0.0958458, -0.995396
	spawn_action none
	team default
	voice 0
´sound_start´ ElementPlaySound 100009
	append_prefix False
	elements
		1 ´ai_spawn_enemy_007´
	interrupt True
	position -867.0, 400.0, 200.0
	rotation 0.0, 0.0, 0.92388, -0.382683
	sound_event Play_npc_tag_poi_01
	use_instigator False
´sound_stop´ ElementPlaySound 100020
	append_prefix False
	elements
		1 ´ai_spawn_enemy_007´
	interrupt True
	position 950.0, -500.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	sound_event Stop_man_ch1_01
	use_instigator False
´death´ ElementEnemyDummyTrigger 100032
	elements
		1 ´ai_spawn_enemy_007´
		2 ´ai_spawn_enemy_004´
		3 ´ai_spawn_enemy_001´
		4 ´ai_spawn_enemy_002´
		5 ´ai_spawn_enemy_005´
		6 ´ai_spawn_enemy_006´
		7 ´ai_spawn_enemy_008´
		8 ´ai_spawn_enemy_010´
		9 ´ai_spawn_enemy_009´
	event death
	position 900.0, -100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´link´ (delay 0)
´alerted´ ElementEnemyDummyTrigger 100035
	elements
		1 ´ai_spawn_enemy_007´
		2 ´ai_spawn_enemy_004´
		3 ´ai_spawn_enemy_001´
		4 ´ai_spawn_enemy_002´
		5 ´ai_spawn_enemy_005´
		6 ´ai_spawn_enemy_006´
		7 ´ai_spawn_enemy_008´
		8 ´ai_spawn_enemy_010´
		9 ´ai_spawn_enemy_009´
	event alerted
	position 1000.0, -100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´link´ (delay 0)
´link´ MissionScriptElement 100038
	on_executed
		´sound_stop´ (delay 0)
		´disable_SOs´ (delay 0)
		´remove_SOs´ (delay 0)
		´AI_hunt´ (delay 0)
		´sound_start001´ (delay 0)
´disable_SOs´ ElementToggle 100047
	elements
		1 ´point_special_objective_002´
		2 ´point_special_objective_004´
		3 ´point_special_objective_005´
		4 ´point_special_objective_006´
		5 ´point_special_objective_007´
		6 ´point_special_objective_008´
		7 ´point_special_objective_009´
		8 ´sound_start´
		9 ´sound_random001´
		10 ´sound_random002´
	set_trigger_times -1
	toggle off
´remove_SOs´ ElementOperator 100048
	elements
		1 ´point_special_objective_002´
		2 ´point_special_objective_004´
		3 ´point_special_objective_005´
		4 ´point_special_objective_006´
		5 ´point_special_objective_007´
		6 ´point_special_objective_008´
		7 ´point_special_objective_009´
	operation remove
´func_sequence_001´ ElementUnitSequence 100059
	position 1100.0, 0.0, 2.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/002 (-764.0, 230.0, 375.0)
			notify_unit_sequence light_on
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/003 (-764.0, 631.0, 375.0)
			notify_unit_sequence light_on
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/004 (-229.0, 631.0, 375.0)
			notify_unit_sequence light_on
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/001 (-230.0, 230.0, 375.0)
			notify_unit_sequence light_on
			time 0
´func_sequence_002´ ElementUnitSequence 100060
	position 950.0, 0.0, 2.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_sign_rooms/001 (-750.0, 0.0, 150.0)
			notify_unit_sequence sign_training
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_sign_rooms/002 (0.0, 400.0, 150.0)
			notify_unit_sequence sign_training
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/001 (-230.0, 230.0, 375.0)
			notify_unit_sequence show
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/002 (-764.0, 230.0, 375.0)
			notify_unit_sequence show
			time 0
		5
			id 5
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/003 (-764.0, 631.0, 375.0)
			notify_unit_sequence show
			time 0
		6
			id 6
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/004 (-229.0, 631.0, 375.0)
			notify_unit_sequence show
			time 0
		7
			id 7
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/001 (-230.0, 230.0, 375.0)
			notify_unit_sequence light_off
			time 0
		8
			id 8
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/002 (-764.0, 230.0, 375.0)
			notify_unit_sequence light_off
			time 0
		9
			id 9
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/003 (-764.0, 631.0, 375.0)
			notify_unit_sequence light_off
			time 0
		10
			id 10
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/004 (-229.0, 631.0, 375.0)
			notify_unit_sequence light_off
			time 0
´enable_lights_projection´ ElementEnableUnit 100063
	TRIGGER TIMES 1
	position 1100.0, -100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/lights/light_projection_01/001 (-602.003, 389.488, 313.064)
		2 core/units/light_omni/004 (-858.0, 500.0, 128.0)
´func_disable_unit_001´ ElementDisableUnit 100091
	position 1200.0, 100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 core/units/light_omni/blue (-530.0, 402.0, 150.0)
´AI_hunt´ ElementSpecialObjective 100028
	SO_access 1020
	action_duration_max 0
	action_duration_min 0
	ai_group enemies
	align_position False
	align_rotation False
	attitude engage
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis 0
	interrupt_dmg 1
	interrupt_objective False
	interval -1
	needs_pos_rsrv False
	path_haste none
	path_stance cbt
	path_style destination
	patrol_path none
	pose none
	position 1100.0, -400.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	scan False
	search_distance 0
	search_position 1100.0, -400.0, 0.0
	so_action AI_hunt
	trigger_on none
	use_instigator True
´sound_start001´ ElementPlaySound 100097
	append_prefix False
	elements
		1 ´ai_spawn_enemy_007´
	interrupt True
	position 900.0, -200.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	sound_event Stop_npc_tag_poi_01
	use_instigator False
´sound_random001´ ElementPlaySound 100015
	append_prefix False
	elements
		1 ´ai_spawn_enemy_004´
	interrupt True
	position 700.0, -300.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	sound_event l1n_a06
	use_instigator False
	on_executed
		´logic_link_001´ (delay 0)
´sound_random002´ ElementPlaySound 100025
	append_prefix False
	elements
		1 ´ai_spawn_enemy_002´
	interrupt True
	position 800.0, -300.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	sound_event l3n_a06
	use_instigator False
	on_executed
		´logic_link_001´ (delay 0)
´logic_random_001´ ElementRandom 100102
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´sound_random001´ (delay 0)
		´sound_random002´ (delay 0)
´logic_link_001´ MissionScriptElement 100055
	on_executed
		´logic_random_001´ (delay 30-45)
