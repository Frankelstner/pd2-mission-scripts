﻿´camera_guard´ ElementSpawnEnemyDummy 100030
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_security_1/ene_security_1
	force_pickup none
	interval 5
	participate_to_group_ai False
	position 449.0, -269.0, 0.0
	rotation 0.0, 0.0, 0.649448, -0.760406
	spawn_action none
	team default
	voice 0
	on_executed
		´SO_idle´ (delay 0)
´dead´ ElementEnemyDummyTrigger 100031
	elements
		1 ´camera_guard´
	event death
	position 300.0, -250.0, 0.0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	on_executed
		´kill_cameras´ (delay 0)
		´disable_sound_buzz´ (delay 0)
		´output_guard_disturbed´ (delay 0)
´kill_cameras´ MissionScriptElement 100032
	TRIGGER TIMES 1
	on_executed
		´output_disable_cameras´ (delay 0)
´output_disable_cameras´ ElementInstanceOutput 100033
	event disable_cameras
´setup_show´ MissionScriptElement 100034
	on_executed
		´show_me´ (delay 0)
		´enabler´ (delay 0)
		´camera_guard´ (delay 0)
		´hider001´ (delay 0)
´show_me´ ElementUnitSequence 100036
	position 200.0, 100.0, 0.0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/props/gen_prop_security_console/001 (531.0, -249.0, 73.0)
			notify_unit_sequence state_interaction_enabled
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc2/architecture/gov_d_int_door_c/001 (195.0, -52.0, 0.0)
			notify_unit_sequence activate_door
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/pd2_dlc2/architecture/gov_d_int_door_c/001 (195.0, -52.0, 0.0)
			notify_unit_sequence state_door_show
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/payday2/props/gen_prop_security_console/001 (531.0, -249.0, 73.0)
			notify_unit_sequence state_interaction_enabled
			time 0
		5
			id 5
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_pickup_harddrive/001 (526.0, -180.0, 74.0)
			notify_unit_sequence show
			time 0
		6
			id 6
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_pickup_harddrive/001 (526.0, -180.0, 74.0)
			notify_unit_sequence disable_interaction
			time 0
		7
			id 7
			name run_sequence
			notify_unit_id units/payday2/props/gen_prop_security_console/001 (531.0, -249.0, 73.0)
			notify_unit_sequence state_vis_show
			time 0
		8
			id 8
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_sign_rooms/001 (300.0, -25.0, 150.0)
			notify_unit_sequence sign_security
			time 0
´add_obstacle´ ElementNavObstacle 100038
	obstacle_list
		1
			guis_id 1
			obj_name 6f2452ed502b8b6f
			unit_id units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (86.0, -100.0, 0.0)
	operation add
	position 200.0, 300.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
´done_opened´ ElementUnitSequenceTrigger 100039
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence door_opened
			unit_id units/pd2_dlc2/architecture/gov_d_int_door_c/001 (195.0, -52.0, 0.0)
		2
			guis_id 2
			sequence done_exploded
			unit_id units/pd2_dlc2/architecture/gov_d_int_door_c/001 (195.0, -52.0, 0.0)
		3
			guis_id 3
			sequence done_opened
			unit_id units/pd2_dlc2/architecture/gov_d_int_door_c/001 (195.0, -52.0, 0.0)
	on_executed
		´remove_obstacle´ (delay 0)
		´remove_SO_crouch´ (delay 0)
´remove_obstacle´ ElementNavObstacle 100040
	obstacle_list
		1
			guis_id 1
			obj_name 6f2452ed502b8b6f
			unit_id units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (86.0, -100.0, 0.0)
	operation remove
	position 200.0, -250.0, 0.0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
´setup_hide´ MissionScriptElement 100041
	EXECUTE ON STARTUP
	BASE DELAY 1
	on_executed
		´hide´ (delay 0)
		´add_obstacle´ (delay 0)
		´hider´ (delay 0)
´hide´ ElementUnitSequence 100042
	position 300.0, 300.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc2/architecture/gov_d_int_door_c/001 (195.0, -52.0, 0.0)
			notify_unit_sequence deactivate_door
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc2/architecture/gov_d_int_door_c/001 (195.0, -52.0, 0.0)
			notify_unit_sequence state_door_hide
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/props/gen_prop_security_console/001 (531.0, -249.0, 73.0)
			notify_unit_sequence state_interaction_disabled
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_pickup_harddrive/001 (526.0, -180.0, 74.0)
			notify_unit_sequence hide
			time 0
		5
			id 5
			name run_sequence
			notify_unit_id units/payday2/props/gen_prop_security_console/001 (531.0, -249.0, 73.0)
			notify_unit_sequence state_vis_hidden
			time 0
		6
			id 6
			name run_sequence
			notify_unit_id units/pd2_dlc_old_hoxton/equipment/stn_interactable_computer_security/001 (587.0, -248.0, 73.0)
			notify_unit_sequence state_interact_disable
			time 0
		7
			id 8
			name run_sequence
			notify_unit_id units/pd2_dlc_tag/props/tag_prop_sign_rooms/001 (300.0, -25.0, 150.0)
			notify_unit_sequence hide_all
			time 0
´input_show_camera_room´ ElementInstanceInput 100043
	event show_camera_room
	on_executed
		´setup_show´ (delay 0)
´SO_crouch´ ElementSpecialObjective 100044
	SO_access 24
	action_duration_max 0
	action_duration_min 0
	ai_group enemies
	align_position True
	align_rotation True
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis 0
	interrupt_dmg 0
	interrupt_objective False
	interval -1
	needs_pos_rsrv True
	path_haste none
	path_stance cbt
	path_style destination
	patrol_path none
	pose crouch
	position 173.0, -300.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	scan True
	search_distance 0
	search_position 400.0, -50.0, 0.0
	so_action none
	trigger_on none
	use_instigator True
´alert´ ElementEnemyDummyTrigger 100045
	elements
		1 ´camera_guard´
	event alerted
	position 300.0, -175.0, 0.0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	on_executed
		´SO_crouch´ (delay 2)
		´output_guard_disturbed´ (delay 0)
´remove_SO_crouch´ ElementOperator 100046
	elements
		1 ´SO_crouch´
	operation remove
´hider´ ElementDisableUnit 100050
	position 100.0, 300.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/pd2_dlc2/props/bnk_prop_lobby_plant_dracaenafragrans_c/001 (543.0, -371.0, 139.0)
		2 units/world/props/office/coathanger/001 (34.0, -389.0, 0.0)
		3 units/payday2/props/signs/com_prop_jewelry_calendar/001 (275.0, -50.0, 175.0)
		4 units/world/props/office/computer_02/001 (559.0, -223.0, 0.0)
		5 units/world/props/office/computer_02/003 (379.0, -86.0, 0.0)
		6 units/world/props/office/computer_case/003 (351.0, -86.0, 0.0)
		7 units/dev_tools/level_tools/dev_collision_1x3m/001 (500.0, -58.0, 0.0)
		8 units/dev_tools/level_tools/dev_collision_1x3m/002 (500.0, -83.0, 0.0)
		9 units/dev_tools/level_tools/dev_collision_1x3m/005 (500.0, -38.0, 0.0)
		10 units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (86.0, -100.0, 0.0)
		11 units/dev_tools/level_tools/dev_nav_blocker_1x1x3m/002 (150.0, -450.0, 0.0)
		12 units/dev_tools/level_tools/dev_nav_blocker_1x3m/001 (217.0, -38.0, 0.0)
		13 units/dev_tools/level_tools/dev_nav_blocker_1x3m/002 (227.168, -60.8386, 0.0)
		14 units/dev_tools/level_tools/dev_nav_blocker_1x3m/003 (425.0, -75.0, 0.0)
		15 units/dev_tools/level_tools/dev_nav_blocker_1x3m/004 (425.0, -100.0, 0.0)
		16 units/payday2/equipment/gen_interactable_pickup_harddrive/001 (526.0, -180.0, 74.0)
		17 units/payday2/props/gen_prop_security_monitors_four_wall/001 (600.0, -251.0, 42.0)
		18 units/payday2/props/servers/gen_prop_security_server_hightech/003 (518.0, -84.0, 0.0)
		19 units/lights/light_omni_shadow_projection_01/001 (512.0, -251.0, 150.0)
		20 units/lights/light_omni_shadow_projection_01/002 (235.0, -222.0, 250.0)
		21 units/payday2/props/off_prop_ceiling_pipe_1_1m_b_black/002 (499.0, -300.001, 325.0)
		22 units/payday2/props/off_prop_ceiling_pipe_1_1m_b_black/003 (49.0, -450.0, 325.0)
		23 units/payday2/props/off_prop_ceiling_pipe_1_2m_b_black/002 (99.0, -300.0, 325.0)
		24 units/payday2/props/off_prop_ceiling_pipe_1_2m_b_black/003 (299.0, -300.0, 325.0)
		25 units/payday2/props/off_prop_ceiling_pipe_1_corner_black/001 (49.0, -350.0, 325.0)
		26 units/payday2/props/off_prop_officehigh_chair_standard/001 (503.991, -282.364, 0.0)
		27 units/world/props/box/prop_filebox_01/001 (25.0, -251.0, 199.445)
		28 units/world/props/office/roof_lamp/001 (235.0, -222.0, 375.0)
		29 units/pd2_dlc_old_hoxton/equipment/stn_interactable_computer_security/001 (587.0, -248.0, 73.0)
		30 units/world/props/gym/stn_prop_locker/001 (8.0, -306.0, 0.0)
		31 units/payday2/props/stn_prop_office_chair_visitor/001 (252.0, -373.0, 0.0)
		32 units/payday2/props/stn_prop_office_deskset_straight_a/001 (511.0, -163.0, 0.0)
		33 units/payday2/props/stn_prop_office_filecabinet_b_02/001 (532.0, -338.0, 0.0)
		34 units/payday2/props/stn_prop_office_filecabinet_b_02/002 (532.0, -378.0, 0.0)
		35 units/world/props/gym/socketwire/stn_prop_socket_wire/001 (269.0, -50.0, 8.4043)
		36 units/world/props/bank/paintings/suburbia_painting_21/001 (125.0, -429.0, 200.0)
		37 units/pd2_dlc_tag/architecture/main_mesh/tag_int_room_inst_06_security/room_inst_06_001 (0.0, -25.0, 0.0)
		38 units/world/props/trashcan/trash_dustbin_01/001 (579.0, -140.0, 0.0)
		39 units/pd2_dlc2/csgo_models/props_vents/vent_large_grill001/001 (445.0, -190.0, 375.0)
		40 units/pd2_dlc_tag/architecture/tag_int/windows_doors/tag_int_door_frame_01/001 (205.0, -28.0, 0.0)
´enabler´ ElementEnableUnit 100051
	position 100.0, 100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/pd2_dlc2/props/bnk_prop_lobby_plant_dracaenafragrans_c/001 (543.0, -371.0, 139.0)
		2 units/world/props/office/coathanger/001 (34.0, -389.0, 0.0)
		3 units/payday2/props/signs/com_prop_jewelry_calendar/001 (275.0, -50.0, 175.0)
		4 units/world/props/office/computer_02/001 (559.0, -223.0, 0.0)
		5 units/world/props/office/computer_02/003 (379.0, -86.0, 0.0)
		6 units/world/props/office/computer_case/003 (351.0, -86.0, 0.0)
		7 units/dev_tools/level_tools/dev_collision_1x3m/001 (500.0, -58.0, 0.0)
		8 units/dev_tools/level_tools/dev_collision_1x3m/002 (500.0, -83.0, 0.0)
		9 units/dev_tools/level_tools/dev_collision_1x3m/005 (500.0, -38.0, 0.0)
		10 units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (86.0, -100.0, 0.0)
		11 units/dev_tools/level_tools/dev_nav_blocker_1x1x3m/002 (150.0, -450.0, 0.0)
		12 units/dev_tools/level_tools/dev_nav_blocker_1x3m/001 (217.0, -38.0, 0.0)
		13 units/dev_tools/level_tools/dev_nav_blocker_1x3m/002 (227.168, -60.8386, 0.0)
		14 units/dev_tools/level_tools/dev_nav_blocker_1x3m/003 (425.0, -75.0, 0.0)
		15 units/dev_tools/level_tools/dev_nav_blocker_1x3m/004 (425.0, -100.0, 0.0)
		16 units/payday2/equipment/gen_interactable_pickup_harddrive/001 (526.0, -180.0, 74.0)
		17 units/payday2/props/gen_prop_security_monitors_four_wall/001 (600.0, -251.0, 42.0)
		18 units/payday2/props/servers/gen_prop_security_server_hightech/003 (518.0, -84.0, 0.0)
		19 units/lights/light_omni_shadow_projection_01/001 (512.0, -251.0, 150.0)
		20 units/lights/light_omni_shadow_projection_01/002 (235.0, -222.0, 250.0)
		21 units/payday2/props/off_prop_ceiling_pipe_1_1m_b_black/002 (499.0, -300.001, 325.0)
		22 units/payday2/props/off_prop_ceiling_pipe_1_1m_b_black/003 (49.0, -450.0, 325.0)
		23 units/payday2/props/off_prop_ceiling_pipe_1_2m_b_black/002 (99.0, -300.0, 325.0)
		24 units/payday2/props/off_prop_ceiling_pipe_1_2m_b_black/003 (299.0, -300.0, 325.0)
		25 units/payday2/props/off_prop_ceiling_pipe_1_corner_black/001 (49.0, -350.0, 325.0)
		26 units/payday2/props/off_prop_officehigh_chair_standard/001 (503.991, -282.364, 0.0)
		27 units/world/props/box/prop_filebox_01/001 (25.0, -251.0, 199.445)
		28 units/world/props/office/roof_lamp/001 (235.0, -222.0, 375.0)
		29 units/pd2_dlc_old_hoxton/equipment/stn_interactable_computer_security/001 (587.0, -248.0, 73.0)
		30 units/world/props/gym/stn_prop_locker/001 (8.0, -306.0, 0.0)
		31 units/payday2/props/stn_prop_office_chair_visitor/001 (252.0, -373.0, 0.0)
		32 units/payday2/props/stn_prop_office_deskset_straight_a/001 (511.0, -163.0, 0.0)
		33 units/payday2/props/stn_prop_office_filecabinet_b_02/001 (532.0, -338.0, 0.0)
		34 units/payday2/props/stn_prop_office_filecabinet_b_02/002 (532.0, -378.0, 0.0)
		35 units/world/props/gym/socketwire/stn_prop_socket_wire/001 (269.0, -50.0, 8.4043)
		36 units/world/props/bank/paintings/suburbia_painting_21/001 (125.0, -429.0, 200.0)
		37 units/pd2_dlc_tag/architecture/main_mesh/tag_int_room_inst_06_security/room_inst_06_001 (0.0, -25.0, 0.0)
		38 units/world/props/trashcan/trash_dustbin_01/001 (579.0, -140.0, 0.0)
		39 units/pd2_dlc2/csgo_models/props_vents/vent_large_grill001/001 (445.0, -190.0, 375.0)
		40 units/pd2_dlc_tag/architecture/tag_int/windows_doors/tag_int_door_frame_01/001 (205.0, -28.0, 0.0)
´panic´ ElementEnemyDummyTrigger 100026
	elements
		1 ´camera_guard´
	event panic
	position 350.0, -250.0, 0.0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	on_executed
		´kill_cameras´ (delay 0)
		´output_guard_disturbed´ (delay 0)
´tied´ ElementEnemyDummyTrigger 100027
	elements
		1 ´camera_guard´
	event tied
	position 400.0, -250.0, 0.0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	on_executed
		´kill_cameras´ (delay 0)
		´output_guard_disturbed´ (delay 0)
´hider001´ ElementDisableUnit 100019
	position 0.0, 100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
´point_spawn_player_001´ ElementPlayerSpawner 100023
	position 700.0, 300.0, 0.0
	rotation 0.0, 0.0, 0.92388, 0.382683
	state standard
´test(disabled_by_default)´ MissionScriptElement 100035
	EXECUTE ON STARTUP
	DISABLED
	on_executed
		´point_spawn_player_001´ (delay 0)
		´func_whisper_state_001´ (delay 0)
		´input_show_camera_room´ (delay 3)
´SO_idle´ ElementSpecialObjective 100070
	SO_access 24
	action_duration_max 0
	action_duration_min 0
	ai_group enemies
	align_position False
	align_rotation False
	attitude none
	base_chance 1
	chance_inc 0
	forced True
	interaction_voice none
	interrupt_dis -1
	interrupt_dmg 0
	interrupt_objective False
	interval -1
	needs_pos_rsrv False
	path_haste none
	path_stance none
	path_style destination
	patrol_path none
	pose none
	position 449.0, -269.0, 0.0
	rotation 0.0, 0.0, 0.649448, -0.760406
	scan False
	search_distance 0
	search_position 449.888, -217.157, 0.935135
	so_action e_so_ntl_idle_stand
	trigger_on none
	use_instigator True
´func_whisper_state_001´ ElementWhisperState 100071
	position 600.0, 100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	state True
´point_play_sound_001´ ElementPlaySound 100072
	append_prefix False
	elements
		1 ´camera_guard´
	interrupt True
	position 500.0, -200.0, 150.0
	rotation 0.0, 0.0, 0.649448, -0.760406
	sound_event dsp_radio_buzz
	use_instigator False
´chance_buzz´ ElementLogicChance 100073
	chance 40
	position 500.0, -100.0, 150.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´point_play_sound_001´ (delay 0)
´area_player_entered_security_room´ ElementAreaTrigger 100074
	TRIGGER TIMES 1
	amount 1
	depth 200
	height 500
	instigator player
	instigator_name 
	interval 0.1
	position 125.0, -100.0, 150.0
	radius 250
	rotation 0.0, 0.0, -0.707107, -0.707107
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 100
	on_executed
		´chance_buzz´ (delay 0)
´disable_sound_buzz´ ElementToggle 100075
	elements
		1 ´point_play_sound_001´
	set_trigger_times -1
	toggle off
´func_sequence_trigger_001´ ElementUnitSequenceTrigger 100001
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence animate
			unit_id units/world/props/office/roof_lamp/001 (235.0, -222.0, 375.0)
	on_executed
		´func_disable_unit_001´ (delay 0)
´func_disable_unit_001´ ElementDisableUnit 100003
	position 200.0, -200.0, 225.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/lights/light_omni_shadow_projection_01/002 (235.0, -222.0, 250.0)
´output_guard_disturbed´ ElementInstanceOutput 100004
	event guard_disturbed
