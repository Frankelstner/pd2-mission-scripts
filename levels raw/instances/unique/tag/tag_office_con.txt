ID range vs continent name:
	100000: world

statics
	100313 core/units/light_omni/blue (-469.0, 352.0, 250.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.133333, 0.192157, 0.258824
			enabled True
			falloff_exponent 1
			far_range 550
			multiplier match
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100263 core/units/light_omni/omni_box001 (-851.0, 337.0, 138.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.972549, 0.87451, 0.580392
			enabled True
			falloff_exponent 1
			far_range 50
			multiplier neonsign
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100272 core/units/light_omni/omni_box002 (-643.0, 791.0, 71.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.972549, 0.87451, 0.580392
			enabled True
			falloff_exponent 1
			far_range 50
			multiplier neonsign
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100295 core/units/light_omni/omni_box003 (-153.0, 492.0, 68.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.972549, 0.87451, 0.580392
			enabled True
			falloff_exponent 1
			far_range 50
			multiplier neonsign
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100297 core/units/light_omni/omni_box004 (158.0, 182.0, 148.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.972549, 0.87451, 0.580392
			enabled True
			falloff_exponent 1
			far_range 50
			multiplier neonsign
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100285 units/dev_tools/level_tools/dev_bag_collision_1x3m/001 (164.0, 132.0, -25.0)
	100166 units/dev_tools/level_tools/dev_bag_collision_4x3m/001 (-825.0, 25.0, 0.0)
	100249 units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-750.0, -50.0, 0.0)
	100122 units/lights/light_omni_shadow_projection_01/001 (-113.0, 225.0, 225.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.988235, 0.741176, 0.619608
			enabled True
			falloff_exponent 1
			far_range 350
			multiplier desklight
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
		projection_light lo_omni
	100017 units/lights/light_omni_shadow_projection_01/002 (-514.0, 630.0, 250.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.988235, 0.741176, 0.619608
			enabled True
			falloff_exponent 1
			far_range 450
			multiplier flashlight
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
		projection_light lo_omni
	100074 units/lights/light_omni_shadow_projection_01/003 (-514.0, 230.0, 250.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.988235, 0.741176, 0.619608
			enabled True
			falloff_exponent 1
			far_range 450
			multiplier flashlight
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
		projection_light lo_omni
	100077 units/lights/light_projection_01/001 (-307.0, 575.0, 349.111)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.984314, 0.713726, 0.584314
			enabled True
			falloff_exponent 1
			far_range 400
			multiplier candle
			name ls_spot
			near_range 80
			spot_angle_end 127
			spot_angle_start 34
		projection_light ls_spot
		ls_spot units/lights/spot_light_projection_textures/spotprojection_08_flashlight_df
	100078 units/lights/light_projection_01/002 (-330.0, 222.0, 357.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.984314, 0.713726, 0.584314
			enabled True
			falloff_exponent 1
			far_range 400
			multiplier match
			name ls_spot
			near_range 80
			spot_angle_end 92
			spot_angle_start 34
		projection_light ls_spot
		ls_spot units/lights/spot_light_projection_textures/spotprojection_08_flashlight_df
	100125 units/lights/light_projection_01/006 (-833.0, -103.0, 400.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.811765, 0.592157, 0.392157
			enabled True
			falloff_exponent 1
			far_range 400
			multiplier candle
			name ls_spot
			near_range 80
			spot_angle_end 92
			spot_angle_start 34
		projection_light ls_spot
		ls_spot units/lights/spot_light_projection_textures/spotprojection_08_flashlight_df
	100123 units/lights/light_projection_01/007 (-447.0, -103.0, 400.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.811765, 0.592157, 0.392157
			enabled True
			falloff_exponent 1
			far_range 400
			multiplier candle
			name ls_spot
			near_range 80
			spot_angle_end 92
			spot_angle_start 34
		projection_light ls_spot
		ls_spot units/lights/spot_light_projection_textures/spotprojection_08_flashlight_df
	100119 units/payday2/pickups/gen_pku_keycard/001 (-903.0, 332.0, 104.0)
		disable_shadows True
	100159 units/payday2/pickups/gen_pku_keycard/002 (204.0, 186.0, 112.0)
		disable_shadows True
	100160 units/payday2/pickups/gen_pku_keycard/003 (-647.0, 844.0, 33.0)
		disable_shadows True
	100233 units/payday2/pickups/gen_pku_keycard/004 (-107.0, 490.0, 35.0)
		disable_shadows True
	100025 units/payday2/props/bnk_prop_office_lamp_table/001 (-405.999, 611.0, 75.497)
		disable_shadows True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.941176, 0.77907, 0.553633
			enabled True
			falloff_exponent 1
			far_range 200
			multiplier identity
			name lo_omni_01
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100053 units/payday2/props/bnk_prop_office_lamp_table/002 (-826.0, 455.0, 75.304)
		disable_shadows True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.941176, 0.77907, 0.553633
			enabled True
			falloff_exponent 1
			far_range 100
			multiplier identity
			name lo_omni_01
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100046 units/payday2/props/bnk_prop_office_shelf_books_3m_b/002 (-5.0, 50.0, 0.0)
		disable_shadows True
	100066 units/payday2/props/bnk_prop_office_shelf_books_3m_b/003 (6.10352e-05, 368.0, 0.0)
		disable_shadows True
	100043 units/payday2/props/com_prop_lobby_loungechair/001 (-447.999, 374.0, -4.0)
		disable_shadows True
	100092 units/payday2/props/com_prop_lobby_loungechair/002 (-556.999, 372.0, -4.0)
		disable_shadows True
	100366 units/payday2/props/com_prop_mall_ceiling_lamp_wall/001 (-461.0, 0.0, 222.0)
		disable_shadows True
		hide_on_projection_light True
	100367 units/payday2/props/com_prop_mall_ceiling_lamp_wall/002 (-814.0, -0.00012207, 222.0)
		disable_shadows True
		hide_on_projection_light True
	100040 units/payday2/props/desk_shelves/bnk_prop_office_drawer_125cm/003 (-184.0, 649.0, 0.0)
		disable_shadows True
	100055 units/payday2/props/desk_shelves/bnk_prop_office_drawer_125cm/004 (-817.0, 409.0, 0.0)
		disable_shadows True
	100048 units/payday2/props/desk_shelves/bnk_prop_office_drawer_125cm/005 (-817.0, 537.0, 0.0)
		disable_shadows True
	100045 units/payday2/props/desk_shelves/bnk_prop_office_shelf_3m/002 (-5.0, 58.0, 0.0)
		disable_shadows True
	100065 units/payday2/props/desk_shelves/bnk_prop_office_shelf_3m/003 (0.0, 367.0, 0.0)
		disable_shadows True
	100309 units/payday2/props/gen_prop_glow_neon_02/001 (-555.0, 631.0, 425.0)
	100310 units/payday2/props/gen_prop_glow_neon_02/002 (-555.0, 231.0, 425.0)
	100311 units/payday2/props/gen_prop_glow_neon_02/003 (-155.0, 231.0, 425.0)
	100028 units/payday2/props/paperstacks/off_prop_officehigh_paper_stack_01/002 (-550.0, 475.0, 75.0)
		disable_shadows True
		hide_on_projection_light True
	100041 units/payday2/props/paperstacks/off_prop_officehigh_paper_stack_01/006 (-185.0, 641.0, 74.065)
		disable_shadows True
		hide_on_projection_light True
	100050 units/payday2/props/paperstacks/off_prop_officehigh_paper_stack_01/007 (-824.001, 490.0, 75.0)
		disable_shadows True
		hide_on_projection_light True
	100344 units/payday2/props/pictures/lxa_prop_hallway_picture_01/001 (-850.0, 575.0, 150.0)
		disable_shadows True
		hide_on_projection_light True
	100343 units/payday2/props/pictures/lxa_prop_hallway_picture_03/001 (-300.0, 800.0, 150.0)
		disable_shadows True
		hide_on_projection_light True
	100027 units/payday2/props/shelves/bnk_prop_office_desk_left/001 (-501.999, 537.0, 0.0)
		disable_shadows True
	100054 units/payday2/props/stn_prop_lobby_counter_plaque_a/002 (-857.0, 547.0, 125.0)
		disable_shadows True
	100042 units/payday2/props/stn_prop_lobby_fbicrest_small/002 (-650.0, 800.0, 275.0)
		disable_shadows True
	100345 units/payday2/props/stn_prop_lobby_fbicrest_small/003 (0.0, 25.0, 275.0)
		disable_shadows True
	100091 units/payday2/props/stn_prop_lobby_flag_fbi/002 (-179.0, 767.0, 0.0)
		disable_shadows True
	100124 units/payday2/props/stn_prop_lobby_flag_us/004 (-892.989, -44.7494, 0.0)
		disable_shadows True
		hide_on_projection_light True
	100030 units/payday2/props/stn_prop_office_chair_executive/001 (-569.509, 549.95, 0.0)
		disable_shadows True
	100106 units/pd2_dlc2/architecture/gov_d_int_door_b/001 (-654.0, -1.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100047 units/pd2_dlc2/props/bnk_prop_lobby_plant_dracaenafragrans_b/001 (-803.0, 757.0, 2.5)
		disable_shadows True
		hide_on_projection_light True
	100130 units/pd2_dlc_chill/architecture/chl_int_mullplan_round/001 (-431.0, 501.0, 0.0)
	100134 units/pd2_dlc_chill/architecture/chl_int_mullplan_round/002 (-176.0, 51.0, 1.5)
	100182 units/pd2_dlc_dah/props/dah_prop_outlet/001 (-466.0, 28.0, 52.0)
		disable_on_ai_graph True
		disable_shadows True
	100183 units/pd2_dlc_dah/props/dah_prop_outlet/002 (-352.0, 797.0, 52.0)
		disable_on_ai_graph True
		disable_shadows True
	100108 units/pd2_dlc_dah/props/dah_prop_paper_bin_v3/001 (-175.0, 50.0, 2.5)
	100114 units/pd2_dlc_dah/props/dah_prop_paper_bin_v3/002 (-428.0, 498.0, 2.0)
	100276 units/pd2_dlc_holly/mansion/props/lxa_prop_hallway_sprinkler/001 (-691.0, 625.0, 368.0)
	100177 units/pd2_dlc_holly/mansion/props/lxa_prop_hallway_sprinkler/002 (-18.0, 227.0, 368.0)
	100181 units/pd2_dlc_holly/mansion/props/lxa_prop_hallway_sprinkler/003 (-691.0, 225.0, 368.0)
	100026 units/pd2_dlc_old_hoxton/equipment/stn_interactable_computer_director/001 (-403.999, 547.0, 76.665)
		disable_shadows True
		mesh_variation state_interact_disable
	100158 units/pd2_dlc_tag/architecture/main_mesh/tag_int_room_inst_04_office/001 (-850.0, 0.0, 0.0)
		disable_shadows True
		hide_on_projection_light True
		mesh_variation wall_regular
	100088 units/pd2_dlc_tag/architecture/tag_int/windows_doors/tag_int_door_frame_02/001 (-786.0, 3.0, 0.0)
		disable_shadows True
	100173 units/pd2_dlc_tag/equipment/tag_interactable_sec_safe/001 (-861.0, 392.0, 97.0)
		disable_shadows True
	100049 units/pd2_dlc_tag/equipment/tag_interactable_sec_safe/002 (160.0, 126.0, 105.0)
		disable_shadows True
	100216 units/pd2_dlc_tag/equipment/tag_interactable_sec_safe/003 (-587.0, 809.0, 26.0)
		disable_shadows True
	100230 units/pd2_dlc_tag/equipment/tag_interactable_sec_safe/004 (-141.0, 436.0, 28.0)
		disable_shadows True
	100176 units/pd2_dlc_tag/props/desk_shelves/tag_prop_office_drawer_125cm/001 (-183.0, 524.0, 0.0)
		disable_shadows True
		hide_on_projection_light True
	100036 units/pd2_dlc_tag/props/tag_prop_board_notes/001 (-152.0, 700.0, 118.0)
	100294 units/pd2_dlc_tag/props/tag_prop_code_papers/002 (-822.0, 428.0, 76.0)
		mesh_variation postit_02_code_1234
	100150 units/pd2_dlc_tag/props/tag_prop_code_papers/003 (-403.0, 466.0, 77.2381)
		mesh_variation paper_02_code_1703
	100162 units/pd2_dlc_tag/props/tag_prop_code_papers/004 (-424.0, 597.0, 77.0)
		mesh_variation paper_01_code_3651
	100154 units/pd2_dlc_tag/props/tag_prop_code_papers/005 (-189.0, 607.0, 76.0)
		mesh_variation postit_02_code_1212
	100165 units/pd2_dlc_tag/props/tag_prop_code_papers/006 (-33.0, 354.0, 76.0)
		mesh_variation postit_02_code_9001
	100157 units/pd2_dlc_tag/props/tag_prop_code_papers/007 (-307.004, 27.0, 154.087)
		mesh_variation paper_02_code_1776
	100156 units/pd2_dlc_tag/props/tag_prop_code_papers/008 (-35.0, 67.0, 76.0)
		mesh_variation postit_03_code_1337
	100163 units/pd2_dlc_tag/props/tag_prop_code_papers/009 (-357.638, 26.0, 200.143)
		mesh_variation paper_01_code_2015
	100277 units/pd2_dlc_tag/props/tag_prop_com_alarm_keycard_keypad (-802.0, 0.0, 135.613)
		disable_shadows True
	100174 units/pd2_dlc_tag/props/tag_prop_dozer_bobbelhead/001 (-427.0, 489.0, 77.2381)
		disable_shadows True
	100209 units/pd2_dlc_tag/props/tag_prop_keypad/001 (-863.0, 264.0, 156.0)
		disable_shadows True
	100227 units/pd2_dlc_tag/props/tag_prop_keypad/002 (-715.0, 811.0, 81.0)
		disable_shadows True
	100229 units/pd2_dlc_tag/props/tag_prop_keypad/003 (161.0, 254.0, 164.0)
		disable_shadows True
	100232 units/pd2_dlc_tag/props/tag_prop_keypad/004 (-139.0, 564.0, 81.0)
		disable_shadows True
	100093 units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/001 (-555.0, 229.0, 375.0)
		disable_shadows True
	100161 units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/002 (-555.0, 631.0, 375.0)
		disable_shadows True
	100164 units/pd2_dlc_tag/props/tag_prop_light_ceiling_tiled/003 (-153.0, 229.0, 375.0)
		disable_shadows True
	100318 units/pd2_dlc_tag/props/tag_prop_office_shelf_2m/001 (-650.0, 775.0, 0.0)
	100279 units/pd2_dlc_tag/props/tag_prop_sign_rooms/001 (-425.0, 0.0, 150.0)
		disable_shadows True
		hide_on_projection_light True
		mesh_variation sign_solomon
	100349 units/pd2_dlc_tag/props/tag_prop_unknown/001 (205.0, 182.0, 112.0)
	100228 units/pd2_dlc_tag/props/tag_prop_unknown/002 (-100.0, 492.0, 35.0)
	100231 units/pd2_dlc_tag/props/tag_prop_unknown/003 (-643.0, 845.0, 33.0)
	100226 units/pd2_dlc_tag/props/tag_prop_unknown/004 (-902.0, 336.0, 104.0)
	100038 units/pd2_dlc_tag/props/tag_prop_whiteboard_b/001 (-840.0, 324.0, 85.0)
		disable_shadows True
		hide_on_projection_light True
	100075 units/pd2_dlc_tag/props/tag_prop_whiteboard_b/002 (140.0, 195.0, 75.0)
		disable_shadows True
		hide_on_projection_light True
	100013 units/pd2_mcmansion/props/mcm_prop_evidence_board/001 (-335.0, 28.0, 175.0)
		disable_shadows True
		mesh_variation show_evidence_02
	100031 units/world/props/box/prop_filebox_01/003 (-425.999, 556.0, 0.934998)
		disable_shadows True
	100039 units/world/props/box/prop_filebox_01/004 (-183.0, 678.0, 74.304)
		disable_shadows True
