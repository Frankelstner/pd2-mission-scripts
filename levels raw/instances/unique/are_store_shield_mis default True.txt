﻿´remove shields´ ElementDisableUnit 100007
	position -200.0, -200.0, 25.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/dev_tools/level_tools/dev_ai_vis_blocker_005x1x2m/003 (-50.0, 9.00003, 75.0)
		2 units/dev_tools/level_tools/dev_ai_vis_blocker_005x1x2m/002 (-150.0, 9.00002, 75.0)
		3 units/dev_tools/level_tools/dev_ai_vis_blocker_005x1x2m/001 (-250.0, 9.0, 75.0)
	on_executed
		´Open´ (delay 0)
´enable shields´ ElementEnableUnit 100014
	position -200.0, 200.0, 25.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/dev_tools/level_tools/dev_ai_vis_blocker_005x1x2m/001 (-250.0, 9.0, 75.0)
		2 units/dev_tools/level_tools/dev_ai_vis_blocker_005x1x2m/002 (-150.0, 9.00002, 75.0)
		3 units/dev_tools/level_tools/dev_ai_vis_blocker_005x1x2m/003 (-50.0, 9.00003, 75.0)
	on_executed
		´Close´ (delay 0)
´enable_booth´ ElementInstanceInput 100018
	event enable_shutters
	on_executed
		´remove shields´ (delay 0)
´Open´ ElementUnitSequence 100000
	position -200.0, -100.0, 25.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_arena/props/are_prop_barrier_big/001 (-200.0, 12.0001, 100.0)
			notify_unit_sequence anim_door_open
			time 0
´Close´ ElementUnitSequence 100003
	position -200.0, 300.0, 25.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_arena/props/are_prop_barrier_big/001 (-200.0, 12.0001, 100.0)
			notify_unit_sequence anim_door_close
			time 0
´input_force_closed´ ElementInstanceInput 100030
	event Force_Closed
	on_executed
		´enable shields´ (delay 0)
