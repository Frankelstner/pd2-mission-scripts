ID range vs continent name:
	100000: world

statics
	100039 units/dev_tools/level_tools/dev_collision_1x3m/001 (97.0, -48.0, 0.0)
		disable_shadows True
	100040 units/dev_tools/level_tools/dev_collision_1x3m/002 (-1.0, -48.0, 0.0)
		disable_shadows True
	100066 units/dev_tools/level_tools/dev_collision_1x3m/003 (97.0, -23.0, 0.0)
		disable_shadows True
	100074 units/dev_tools/level_tools/dev_collision_1x3m/004 (-1.0, -23.0, 0.0)
		disable_shadows True
	100081 units/dev_tools/level_tools/dev_collision_1x3m/005 (97.0, 2.0, 0.0)
		disable_shadows True
	100083 units/dev_tools/level_tools/dev_collision_1x3m/006 (-1.0, 2.0, 0.0)
		disable_shadows True
	100085 units/dev_tools/level_tools/dev_collision_1x3m/007 (97.0, 26.0, 0.0)
		disable_shadows True
	100088 units/dev_tools/level_tools/dev_collision_1x3m/008 (-1.0, 26.0, 0.0)
		disable_shadows True
	100089 units/dev_tools/level_tools/dev_door_blocker_1x2x3/001 (100.0, -50.0, 0.0)
		disable_shadows True
	100003 units/payday2/architecture/ind/level/ind_ext_stairs_c_base_half/001 (0.0, -64.0, 2.0)
		disable_shadows True
	100194 units/payday2/architecture/ind/level/ind_ext_stairs_c_base_half/002 (-200.0, -50.0, 7.99988)
		disable_shadows True
	100195 units/payday2/architecture/ind/level/ind_ext_stairs_c_base_half/003 (-200.0, -64.0, 1.99988)
		disable_shadows True
	100196 units/payday2/architecture/ind/level/ind_ext_stairs_c_base_half/004 (0.0, -50.0, 8.0)
		disable_shadows True
	100198 units/payday2/architecture/ind/level/ind_ext_stairs_c_base_half/005 (161.0, -50.0, 9.99988)
		disable_shadows True
	100199 units/payday2/architecture/ind/level/ind_ext_stairs_c_base_half/006 (-161.0, 49.9995, 9.99988)
		disable_shadows True
	100200 units/payday2/architecture/ind/level/ind_ext_stairs_c_base_half/007 (0.0, 50.0, 8.0)
		disable_shadows True
	100201 units/payday2/architecture/ind/level/ind_ext_stairs_c_base_half/008 (200.0, 50.0, 7.99988)
		disable_shadows True
	100193 units/payday2/equipment/gen_interactable_drill_small_no_jam_or_upgrades/001 (0.0, -35.0, 234.0)
		disable_shadows True
	100064 units/pd2_dlc_berry/props/bry_prop_keycard_box/001 (-99.0, 14.0, 104.0)
		disable_collision True
		disable_on_ai_graph True
		disable_shadows True
	100203 units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch_2/002 (0.0, 195.0, -290.0)
		disable_shadows True
	100046 units/pd2_dlc_help/pickups/gen_pku_keycard_infinite/001 (-96.9645, 0.0, 96.5463)
		disable_shadows True
		mesh_variation hide
	100192 units/pd2_dlc_help/props/hlp_black_plane/001 (3.57628e-06, -2.00003, 3.0)
		disable_shadows True
	100216 units/pd2_dlc_help/props/hlp_interactable_wheel/001 (0.0, 7.15254e-05, 0.0)
		disable_shadows True
	100043 units/pd2_dlc_help/props/hlp_interactable_wheel/002 (0.0, 7.15254e-05, 0.0)
		disable_shadows True
		mesh_variation move_up
	100033 units/pd2_dlc_help/props/hlp_interactable_wheel_timer/001 (0.0, -33.0, 230.0)
		disable_shadows True
		mesh_variation state_vis_show
