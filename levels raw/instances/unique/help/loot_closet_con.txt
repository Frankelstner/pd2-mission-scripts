ID range vs continent name:
	100000: world

statics
	100075 units/dev_tools/level_tools/dev_ai_vis_blocker_005x2x2m/004 (-3.05176e-05, 100.0, 0.0)
		disable_shadows True
	100011 units/dev_tools/level_tools/dev_ai_vis_blocker_005x2x2m/005 (-3.05176e-05, 100.0, 50.0)
		disable_shadows True
	100018 units/dev_tools/level_tools/dev_bag_collision_1x1m/001 (0.0, -50.0, 300.0)
		disable_shadows True
	100019 units/dev_tools/level_tools/dev_bag_collision_1x1m/002 (0.0, -50.0, -25.0)
		disable_shadows True
	100025 units/dev_tools/level_tools/dev_bag_collision_1x1m/003 (75.0, -50.0, 300.0)
		disable_shadows True
	100071 units/dev_tools/level_tools/dev_bag_collision_1x1m/004 (75.0, -50.0, -25.0)
		disable_shadows True
	100014 units/dev_tools/level_tools/dev_bag_collision_1x3m/001 (0.0, -50.0, 0.0)
		disable_shadows True
	100017 units/dev_tools/level_tools/dev_bag_collision_1x3m/002 (1.49012e-05, 75.0, 0.0)
		disable_shadows True
	100024 units/dev_tools/level_tools/dev_bag_collision_1x3m/003 (75.0, -50.0, 0.0)
		disable_shadows True
	100036 units/dev_tools/level_tools/dev_bag_collision_1x3m/004 (75.0, 75.0, 0.0)
		disable_shadows True
	100090 units/dev_tools/level_tools/dev_bag_collision_1x3m/005 (150.0, -50.0, 0.0)
		disable_shadows True
	100010 units/dev_tools/level_tools/dev_collision_1x3m/001 (-19.0, -50.0, 0.0)
		disable_shadows True
	100089 units/payday2/architecture/lcm_ext_door_metal_01/001 (-25.0, 68.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		mesh_variation 2_disable_interaction
	100013 units/pd2_dlc1/props/details/ind_ext_prop_cable_straight_01/001 (-25.0, 0.0, 352.0)
		disable_shadows True
	100078 units/pd2_dlc1/props/details/ind_ext_prop_cable_straight_01_1m/001 (-25.0, 0.0, 252.0)
		disable_shadows True
	100001 units/pd2_dlc_dark/props/drk_prop_mechanical_lamp_small/001 (-31.0, 0.0, 238.0)
		disable_shadows True
	100057 units/pd2_dlc_help/props/prop_white_glow/hlp_white_glow/001 (5.0, -3.21865e-06, 0.0)
		disable_shadows True
