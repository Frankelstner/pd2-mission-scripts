ID range vs continent name:
	100000: world

statics
	100197 core/units/light_omni/008 (-48.0, 875.0, 229.733)
		disable_shadows True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.556863, 0.701961, 0.858824
			enabled True
			falloff_exponent 1
			far_range 200
			multiplier match
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100202 core/units/light_omni/009 (0.0, 1316.0, 287.041)
		disable_shadows True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.901961, 0.396078, 0.14902
			enabled True
			falloff_exponent 1
			far_range 200
			multiplier flashlight
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100007 core/units/light_omni/010 (0.0, 546.0, 287.041)
		disable_shadows True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.901961, 0.396078, 0.14902
			enabled True
			falloff_exponent 1
			far_range 200
			multiplier flashlight
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100115 core/units/light_omni/011 (-48.0, 1557.0, 229.733)
		disable_shadows True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.556863, 0.701961, 0.858824
			enabled True
			falloff_exponent 1
			far_range 200
			multiplier match
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100231 core/units/light_omni/012 (0.0, 1781.0, 287.041)
		disable_shadows True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.901961, 0.396078, 0.14902
			enabled True
			falloff_exponent 1
			far_range 200
			multiplier flashlight
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100116 core/units/light_omni/013 (-48.0, 2067.0, 229.733)
		disable_shadows True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.556863, 0.701961, 0.858824
			enabled True
			falloff_exponent 1
			far_range 300
			multiplier match
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100009 core/units/light_omni/014 (-48.0, 313.0, 229.733)
		disable_shadows True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.556863, 0.701961, 0.858824
			enabled True
			falloff_exponent 1
			far_range 200
			multiplier match
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100171 core/units/light_omni/015 (-47.0, 1175.0, 229.733)
		disable_shadows True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.556863, 0.701961, 0.858824
			enabled True
			falloff_exponent 1
			far_range 200
			multiplier match
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100016 units/dev_tools/editable_text_long/001 (-163.0, 425.0, 165.153)
		disable_shadows True
		align left
		alpha 1
		blend_mode normal
		font fonts/font_eroded
		font_color 0.329412, 0.537255, 0.796079
		font_size 3.76
		render_template diffuse_vc_decal
		shape
			1 0
			2 0
			3 1
			4 1
		text MPD CORRECTION
		vertical center
		word_wrap False
		wrap False
	100142 units/dev_tools/editable_text_long/002 (-163.0, 1050.0, 165.153)
		disable_shadows True
		align left
		alpha 1
		blend_mode normal
		font fonts/font_eroded
		font_color 0.329412, 0.537255, 0.796079
		font_size 3.76
		render_template diffuse_vc_decal
		shape
			1 0
			2 0
			3 1
			4 1
		text MPD CORRECTION
		vertical center
		word_wrap False
		wrap False
	100143 units/dev_tools/editable_text_long/003 (-163.0, 1750.0, 165.153)
		disable_shadows True
		align left
		alpha 1
		blend_mode normal
		font fonts/font_eroded
		font_color 0.329412, 0.537255, 0.796079
		font_size 3.76
		render_template diffuse_vc_decal
		shape
			1 0
			2 0
			3 1
			4 1
		text MPD CORRECTION
		vertical center
		word_wrap False
		wrap False
	100082 units/dev_tools/level_tools/dev_collision_1x3m/001 (-164.0, 2143.0, 50.2901)
		disable_shadows True
	100081 units/dev_tools/level_tools/dev_collision_1x3m/002 (140.0, 1943.0, 50.2901)
		disable_shadows True
	100083 units/dev_tools/level_tools/dev_collision_1x3m/003 (140.0, 2093.0, 50.2901)
		disable_shadows True
	100111 units/dev_tools/level_tools/dev_collision_1x3m/004 (140.0, 2143.0, 50.2901)
		disable_shadows True
	100112 units/dev_tools/level_tools/dev_collision_1x3m/005 (-164.0, 1943.0, 50.2901)
		disable_shadows True
	100113 units/dev_tools/level_tools/dev_collision_1x3m/006 (-164.0, 2093.0, 50.2901)
		disable_shadows True
	100017 units/dev_tools/level_tools/dev_collision_4x3m/001 (-164.0, 749.0, 50.2901)
		disable_shadows True
	100018 units/dev_tools/level_tools/dev_collision_4x3m/002 (-164.0, 1149.0, 50.2901)
		disable_shadows True
	100077 units/dev_tools/level_tools/dev_collision_4x3m/003 (-164.0, 1543.0, 50.2901)
		disable_shadows True
	100078 units/dev_tools/level_tools/dev_collision_4x3m/004 (140.0, 50.0, 50.2901)
		disable_shadows True
	100079 units/dev_tools/level_tools/dev_collision_4x3m/005 (140.0, 749.0, 50.2901)
		disable_shadows True
	100080 units/dev_tools/level_tools/dev_collision_4x3m/006 (140.0, 640.0, 50.2901)
		disable_shadows True
	100106 units/dev_tools/level_tools/dev_collision_4x3m/007 (140.0, 1149.0, 50.2901)
		disable_shadows True
	100107 units/dev_tools/level_tools/dev_collision_4x3m/008 (140.0, 1543.0, 50.2901)
		disable_shadows True
	100108 units/dev_tools/level_tools/dev_collision_4x3m/009 (140.0, 300.0, 50.2901)
		disable_shadows True
	100109 units/dev_tools/level_tools/dev_collision_4x3m/010 (-164.0, 50.0, 50.2901)
		disable_shadows True
	100110 units/dev_tools/level_tools/dev_collision_4x3m/011 (-164.0, 440.0, 50.2901)
		disable_shadows True
	100256 units/payday2/architecture/apartment/res_ext_debris/001 (0.0, 900.0, 117.486)
		disable_shadows True
	100257 units/payday2/architecture/apartment/res_ext_debris/002 (0.0, 1500.0, 117.486)
		disable_shadows True
	100258 units/payday2/architecture/apartment/res_ext_debris/003 (95.0, 1900.0, 117.486)
		disable_shadows True
	100259 units/payday2/architecture/apartment/res_ext_debris/004 (0.0, 500.0, 117.486)
		disable_shadows True
	100166 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_2m/001 (-125.0, 100.0, 366.606)
		disable_shadows True
	100170 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_2m/002 (-50.0, 99.0, 365.606)
		disable_shadows True
	100177 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_2m/003 (-25.0, 300.0, 366.606)
		disable_shadows True
	100178 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_2m/004 (10.0, 300.0, 366.606)
		disable_shadows True
	100211 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_2m/005 (-125.0, 1500.0, 366.606)
		disable_shadows True
	100213 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_2m/006 (-75.0, 1499.0, 367.606)
		disable_shadows True
	100214 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_2m/007 (-50.0, 1499.0, 365.606)
		disable_shadows True
	100217 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_2m/008 (98.0, 1499.0, 367.606)
		disable_shadows True
	100222 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_2m/009 (-125.0, 1650.0, 366.606)
		disable_shadows True
	100223 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_2m/010 (-50.0, 1649.0, 365.606)
		disable_shadows True
	100224 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_2m/011 (-125.0, 1695.0, 366.606)
		disable_shadows True
	100225 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_2m/012 (-50.0, 1694.0, 365.606)
		disable_shadows True
	100233 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_2m/013 (-25.0, 1900.0, 367.606)
		disable_shadows True
	100234 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_2m/014 (9.0, 1900.0, 367.606)
		disable_shadows True
	100188 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_4m/001 (-25.0, 700.0, 367.3)
		disable_shadows True
	100190 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_4m/002 (9.0, 700.0, 367.3)
		disable_shadows True
	100244 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_4m/003 (-25.0, 2300.0, 367.3)
		disable_shadows True
	100246 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_4m/004 (9.0, 2300.0, 367.3)
		disable_shadows True
	100215 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_8m/001 (-25.0, 1500.0, 367.3)
		disable_shadows True
	100216 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_8m/002 (9.0, 1500.0, 367.3)
		disable_shadows True
	100205 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_8m/003 (-154.0, 1375.0, 108.3)
		disable_shadows True
	100206 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_8m/004 (-154.0, 2275.0, 108.3)
		disable_shadows True
	100207 units/payday2/architecture/ind/bld/ind_ext_dock_beam_small_8m/005 (-154.0, 775.0, 108.3)
		disable_shadows True
	100245 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/028 (137.181, 2265.0, 351.342)
		disable_shadows True
	100117 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/029 (-141.819, 2165.0, 351.342)
		disable_shadows True
	100118 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/030 (-141.819, 2080.0, 350.342)
		disable_shadows True
	100138 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/031 (137.181, 2180.0, 350.342)
		disable_shadows True
	100139 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/032 (-141.819, 1915.0, 351.342)
		disable_shadows True
	100140 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/033 (137.181, 2015.0, 351.342)
		disable_shadows True
	100141 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/034 (-141.819, 1815.0, 351.342)
		disable_shadows True
	100144 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/035 (137.181, 1915.0, 351.342)
		disable_shadows True
	100145 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/036 (-141.819, 1640.0, 336.342)
		disable_shadows True
	100153 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/037 (137.181, 1740.0, 336.342)
		disable_shadows True
	100159 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/038 (-141.819, 1225.0, 351.342)
		disable_shadows True
	100172 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/039 (137.181, 1325.0, 351.342)
		disable_shadows True
	100173 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/040 (-141.819, 1125.0, 351.342)
		disable_shadows True
	100174 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/041 (137.181, 1225.0, 351.342)
		disable_shadows True
	100175 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/042 (-141.819, 962.0, 351.342)
		disable_shadows True
	100176 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/043 (137.181, 1062.0, 351.342)
		disable_shadows True
	100179 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/044 (-141.819, 837.0, 351.342)
		disable_shadows True
	100181 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/045 (137.181, 937.0, 351.342)
		disable_shadows True
	100182 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/046 (-141.819, 712.0, 351.342)
		disable_shadows True
	100183 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/047 (137.181, 812.0, 351.342)
		disable_shadows True
	100184 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/048 (-141.819, 537.0, 351.342)
		disable_shadows True
	100185 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/049 (137.181, 637.0, 351.342)
		disable_shadows True
	100186 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/050 (-141.819, 437.0, 351.342)
		disable_shadows True
	100187 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/051 (137.181, 537.0, 351.342)
		disable_shadows True
	100189 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/052 (-141.819, 272.0, 351.342)
		disable_shadows True
	100193 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/053 (137.181, 372.0, 351.342)
		disable_shadows True
	100194 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/054 (-141.819, 172.0, 351.342)
		disable_shadows True
	100200 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/055 (137.181, 272.0, 351.342)
		disable_shadows True
	100201 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/056 (-141.819, 62.0, 351.342)
		disable_shadows True
	100204 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/057 (137.181, 162.0, 351.342)
		disable_shadows True
	100191 units/payday2/architecture/ind/level/ind_ext_fence_1m_grey/001 (45.0, 702.0, 117.041)
		disable_shadows True
	100195 units/payday2/architecture/ind/level/ind_ext_fence_1m_grey/002 (139.0, 702.0, 117.041)
		disable_shadows True
	100196 units/payday2/architecture/ind/level/ind_ext_fence_1m_grey/003 (39.0, 773.0, 117.041)
		disable_shadows True
	100198 units/payday2/architecture/ind/level/ind_ext_fence_1m_grey/004 (39.0, 873.0, 117.041)
		disable_shadows True
	100221 units/payday2/architecture/ind/level/ind_ext_fence_1m_grey/005 (-30.0, 1579.0, 117.041)
		disable_shadows True
	100226 units/payday2/architecture/ind/level/ind_ext_fence_1m_grey/006 (-30.0, 1679.0, 117.041)
		disable_shadows True
	100227 units/payday2/architecture/ind/level/ind_ext_fence_1m_grey/007 (-130.0, 1750.0, 117.041)
		disable_shadows True
	100228 units/payday2/architecture/ind/level/ind_ext_fence_1m_grey/008 (-36.0001, 1750.0, 117.041)
		disable_shadows True
	100260 units/payday2/architecture/ind/level/ind_ext_fence_1m_grey/009 (39.0, 948.0, 117.041)
		disable_shadows True
	100261 units/payday2/architecture/ind/level/ind_ext_fence_1m_grey/010 (39.0, 1048.0, 117.041)
		disable_shadows True
	100262 units/payday2/architecture/ind/level/ind_ext_fence_1m_grey/011 (45.0, 877.0, 117.041)
		disable_shadows True
	100265 units/payday2/architecture/ind/level/ind_ext_fence_1m_grey/012 (-30.0, 1579.0, 117.041)
		disable_shadows True
	100266 units/payday2/architecture/ind/level/ind_ext_fence_1m_grey/013 (-30.0, 1479.0, 117.041)
		disable_shadows True
	100199 units/payday2/architecture/ind/level/ind_ext_fence_2m1m_shorter_grey/001 (139.0, 873.0, 317.041)
		disable_shadows True
	100218 units/payday2/architecture/ind/level/ind_ext_fence_2m1m_shorter_grey/002 (-130.0, 1579.0, 317.041)
		disable_shadows True
	100263 units/payday2/architecture/ind/level/ind_ext_fence_2m1m_shorter_grey/003 (139.0, 1048.0, 317.041)
		disable_shadows True
	100267 units/payday2/architecture/ind/level/ind_ext_fence_2m1m_shorter_grey/004 (-130.0, 1479.0, 317.041)
		disable_shadows True
	100026 units/payday2/props/air_prop_runway_fence_b_v2/001 (-141.0, 1380.0, 99.041)
		disable_collision True
		disable_on_ai_graph True
		disable_shadows True
	100023 units/payday2/props/air_prop_runway_fence_b_v2/002 (-141.0, 691.0, 99.041)
		disable_collision True
		disable_on_ai_graph True
		disable_shadows True
	100027 units/payday2/props/air_prop_runway_fence_b_v2/003 (-146.0, 35.9996, 99.041)
		disable_collision True
		disable_on_ai_graph True
		disable_shadows True
	100001 units/payday2/props/air_prop_runway_fence_b_v2/004 (137.0, 436.0, 99.041)
		disable_collision True
		disable_on_ai_graph True
		disable_shadows True
	100004 units/payday2/props/air_prop_runway_fence_b_v2/005 (140.0, 1081.0, 99.041)
		disable_collision True
		disable_on_ai_graph True
		disable_shadows True
	100006 units/payday2/props/air_prop_runway_fence_b_v2/006 (142.999, 1767.0, 99.041)
		disable_collision True
		disable_on_ai_graph True
		disable_shadows True
	100149 units/payday2/props/bnk_prop_branch1_security_shutter_a/001 (152.597, 442.398, 171.0)
		disable_shadows True
	100150 units/payday2/props/bnk_prop_branch1_security_shutter_a/002 (152.597, 271.398, 171.0)
		disable_shadows True
	100151 units/payday2/props/bnk_prop_branch1_security_shutter_a/003 (152.597, 73.3986, 171.0)
		disable_shadows True
	100152 units/payday2/props/bnk_prop_branch1_security_shutter_a/004 (152.597, 692.398, 171.0)
		disable_shadows True
	100015 units/payday2/props/bnk_prop_branch1_security_shutter_a/005 (152.597, 867.398, 171.0)
		disable_shadows True
	100154 units/payday2/props/bnk_prop_branch1_security_shutter_a/006 (152.597, 1117.4, 171.0)
		disable_shadows True
	100155 units/payday2/props/bnk_prop_branch1_security_shutter_a/007 (152.597, 1367.4, 171.0)
		disable_shadows True
	100156 units/payday2/props/bnk_prop_branch1_security_shutter_a/008 (152.597, 1567.4, 171.0)
		disable_shadows True
	100157 units/payday2/props/bnk_prop_branch1_security_shutter_a/009 (152.597, 1817.4, 171.0)
		disable_shadows True
	100158 units/payday2/props/bnk_prop_branch1_security_shutter_a/010 (152.597, 2067.4, 171.0)
		disable_shadows True
	100003 units/payday2/props/bnk_prop_branch1_security_shutter_b/001 (-154.0, 249.0, 282.327)
		disable_shadows True
	100008 units/payday2/props/bnk_prop_branch1_security_shutter_b/002 (-154.0, 449.0, 282.327)
		disable_shadows True
	100025 units/payday2/props/bnk_prop_branch1_security_shutter_b/003 (-154.0, 667.0, 282.327)
		disable_shadows True
	100028 units/payday2/props/bnk_prop_branch1_security_shutter_b/004 (-154.0, 867.0, 282.327)
		disable_shadows True
	100130 units/payday2/props/bnk_prop_branch1_security_shutter_b/005 (-154.0, 1067.0, 282.327)
		disable_shadows True
	100131 units/payday2/props/bnk_prop_branch1_security_shutter_b/006 (-154.0, 1320.0, 282.327)
		disable_shadows True
	100132 units/payday2/props/bnk_prop_branch1_security_shutter_b/007 (-154.0, 1570.0, 282.327)
		disable_shadows True
	100133 units/payday2/props/bnk_prop_branch1_security_shutter_b/008 (-154.0, 1770.0, 282.327)
		disable_shadows True
		mesh_variation open_shutters_var_2
	100136 units/payday2/props/bnk_prop_branch1_security_shutter_b/010 (-154.0, 2012.0, 282.327)
		disable_shadows True
	100137 units/payday2/props/bnk_prop_branch1_security_shutter_b/011 (-154.0, 2267.0, 282.327)
		disable_shadows True
	100253 units/payday2/props/res_prop_apartment_mess_center_large/001 (-61.0, 1805.0, 117.041)
		disable_shadows True
	100252 units/payday2/props/res_prop_apartment_mess_center_small/001 (-104.0, 1300.0, 117.041)
		disable_shadows True
	100254 units/payday2/props/res_prop_apartment_mess_straight/001 (102.0, 1629.0, 117.041)
		disable_shadows True
	100264 units/payday2/props/res_prop_apartment_mess_straight/002 (102.0, 1027.0, 117.041)
		disable_shadows True
	100255 units/payday2/props/res_prop_apartment_mess_straight_flat/001 (100.0, 1400.0, 117.041)
		disable_shadows True
	100125 units/payday2/vehicles/halfdmg/sub_vehicle_train_hatch/001 (85.0, 1628.0, 400.0)
		disable_shadows True
	100192 units/payday2/vehicles/halfdmg/sub_vehicle_train_hatch/002 (50.0001, 710.418, 118.37)
		disable_shadows True
	100220 units/payday2/vehicles/halfdmg/sub_vehicle_train_hatch/003 (-134.898, 1351.29, 243.37)
		disable_shadows True
	100268 units/payday2/vehicles/halfdmg/sub_vehicle_train_hatch/004 (-134.898, 1351.29, 368.37)
		disable_shadows True
	100269 units/payday2/vehicles/halfdmg/sub_vehicle_train_hatch/005 (-32.1373, 1480.52, 243.37)
		disable_shadows True
	100270 units/payday2/vehicles/halfdmg/sub_vehicle_train_hatch/006 (44.2715, 46.5996, 173.37)
		disable_shadows True
	100271 units/payday2/vehicles/halfdmg/sub_vehicle_train_hatch/007 (-42.2543, 38.8172, 170.37)
		disable_shadows True
	100247 units/payday2/vehicles/halfdmg/sub_vehicle_train_hatch/011 (-43.2542, 2280.82, 184.37)
		disable_shadows True
	100135 units/payday2/vehicles/halfdmg/sub_vehicle_train_hatch/014 (45.9129, 2282.73, 184.37)
		disable_shadows True
	100180 units/pd2_dlc_berry/props/bry_prop_mechanical_lamp_small/001 (0.0, 541.0, 374.041)
		disable_shadows True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 1.0
			enabled True
			falloff_exponent 4
			far_range 100
			multiplier desklight
			name li_hanging_lamp
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
		mesh_variation light_on
	100203 units/pd2_dlc_berry/props/bry_prop_mechanical_lamp_small/003 (0.0, 1321.0, 374.041)
		disable_shadows True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 1.0
			enabled True
			falloff_exponent 4
			far_range 100
			multiplier desklight
			name li_hanging_lamp
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
		mesh_variation light_on
	100230 units/pd2_dlc_berry/props/bry_prop_mechanical_lamp_small/005 (0.0, 1786.0, 374.041)
		disable_shadows True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 1.0
			enabled True
			falloff_exponent 4
			far_range 100
			multiplier desklight
			name li_hanging_lamp
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
		mesh_variation light_on
	100164 units/pd2_dlc_help/props/hlp_prop_fence_4x2m/001 (-140.0, 2069.0, 100.041)
		disable_collision True
		disable_on_ai_graph True
		disable_shadows True
	100134 units/pd2_dlc_help/props/hlp_prop_fence_4x2m/010 (-149.0, 445.999, 109.041)
		disable_collision True
		disable_on_ai_graph True
		disable_shadows True
	100024 units/pd2_dlc_help/props/hlp_prop_fence_4x2m/011 (-149.0, 1136.0, 109.041)
		disable_collision True
		disable_on_ai_graph True
		disable_shadows True
	100022 units/pd2_dlc_help/props/hlp_prop_fence_4x2m/012 (-149.0, 1826.0, 109.041)
		disable_collision True
		disable_on_ai_graph True
		disable_shadows True
	100002 units/pd2_dlc_help/props/hlp_prop_fence_4x2m/013 (148.0, 628.0, 109.041)
		disable_collision True
		disable_on_ai_graph True
		disable_shadows True
	100005 units/pd2_dlc_help/props/hlp_prop_fence_4x2m/014 (138.0, 1314.0, 109.041)
		disable_collision True
		disable_on_ai_graph True
		disable_shadows True
	100010 units/pd2_dlc_help/props/hlp_prop_fence_4x2m/016 (137.999, 2004.0, 109.041)
		disable_collision True
		disable_on_ai_graph True
		disable_shadows True
	100011 units/pd2_dlc_help/props/hlp_prop_fence_4x2m/017 (137.999, 2257.0, 109.041)
		disable_collision True
		disable_on_ai_graph True
		disable_shadows True
	100251 units/pd2_dlc_holly/river/props/lxa_prop_debris_05/001 (-137.0, 589.0, 117.041)
		disable_shadows True
	100000 units/pd2_dlc_holly/river/props/lxa_prop_traincart/001 (-6.10352e-05, 6.10352e-05, 6.10352e-05)
	100012 units/pd2_dlc_holly/river/props/lxa_prop_traincart_sidedoors/001 (-159.0, 606.0, 121.0)
		disable_shadows True
	100013 units/pd2_dlc_holly/river/props/lxa_prop_traincart_sidedoors/002 (-158.0, 1296.0, 121.0)
		disable_shadows True
	100014 units/pd2_dlc_holly/river/props/lxa_prop_traincart_sidedoors/003 (-158.0, 1987.0, 121.0)
		disable_shadows True
	100146 units/pd2_dlc_holly/river/props/lxa_prop_traincart_sidedoors/004 (156.57, 1151.5, 121.0)
		disable_shadows True
	100147 units/pd2_dlc_holly/river/props/lxa_prop_traincart_sidedoors/005 (158.353, 1842.4, 121.0)
		disable_shadows True
	100148 units/pd2_dlc_holly/river/props/lxa_prop_traincart_sidedoors/006 (158.597, 462.398, 121.0)
		disable_shadows True
