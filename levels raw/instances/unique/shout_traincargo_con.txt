ID range vs continent name:
	100000: world

statics
	100003 core/units/light_omni/001 (1.0, -961.0, 374.615)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.709804, 0.886275, 0.976471
			enabled True
			falloff_exponent 1
			far_range 400
			multiplier identity
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100012 core/units/light_omni/002 (1.0, -636.0, 374.615)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.709804, 0.886275, 0.976471
			enabled True
			falloff_exponent 1
			far_range 400
			multiplier identity
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100018 core/units/light_omni/003 (1.0, -261.0, 374.615)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.709804, 0.886275, 0.976471
			enabled True
			falloff_exponent 1
			far_range 400
			multiplier identity
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100015 units/dev_tools/dev_ladder/001 (261.378, -776.0, -34.0921)
		height 180
		width 40
	100032 units/dev_tools/level_tools/dev_collision_1x1m/001 (-44.0, -513.0, 149.615)
	100030 units/dev_tools/level_tools/dev_collision_1x1m/002 (56.0, -513.0, 149.615)
	100006 units/payday2/props/gen_prop_long_lamp_v2/001 (-199.0, -986.0, 388.645)
	100007 units/payday2/props/gen_prop_long_lamp_v2/002 (-199.0, -586.0, 388.645)
	100020 units/payday2/props/gen_prop_long_lamp_v2/003 (-199.0, -186.0, 388.645)
	100021 units/payday2/props/gen_prop_long_lamp_v2/004 (201.0, -186.0, 388.645)
	100022 units/payday2/props/gen_prop_long_lamp_v2/005 (201.0, -586.0, 388.645)
	100024 units/payday2/props/gen_prop_long_lamp_v2/006 (201.0, -986.0, 388.645)
	100010 units/payday2/props/gen_prop_square_drop_marker_2x3/001 (57.0, -252.0, 159.615)
	100011 units/payday2/props/gen_prop_square_goal_marker_8x15/001 (-0.999989, -875.0, 141.615)
	100002 units/payday2/props/ind_prop_military_ibc/001 (143.0, -1115.0, 149.615)
	100019 units/payday2/props/ind_prop_military_mre_1x1_5m/001 (-124.0, -247.0, 149.615)
	100017 units/payday2/props/ind_prop_military_mre_1x2m/003 (-124.0, -32.0, 149.615)
	100038 units/payday2/props/set/ind_prop_warehouse_pallet_shout/001 (61.9999, -250.0, 149.615)
	100061 units/payday2/vehicles/eus_vehicle_train_cargo_carriage_lowres/001 (0.0, 0.0, 0.0)
	100027 units/payday2/vehicles/eus_vehicle_train_cargo_carriage_wall_b_lowres/001 (0.0, -25.0, 150.0)
	100054 units/payday2/vehicles/vehicle_train/eus_interactable_door_cargo_shout/001 (-1.0, -775.0, 150.0)
	100026 units/payday2/vehicles/vehicle_train/eus_interactable_door_cargo_shout/002 (0.0, 825.0, 150.0)
	100028 units/payday2/vehicles/vehicle_train/eus_interactable_door_cargo_shout/003 (0.0, 825.0, 150.0)
	100025 units/payday2/vehicles/vehicle_train/eus_interactable_door_cargo_shout/004 (0.0, -775.0, 150.0)
		mesh_variation state_door_open
	100031 units/payday2/vehicles/vehicle_train/eus_static_door_carriage_lowres/001 (50.0, 1235.0, 150.0)
	100000 units/payday2/vehicles/vehicle_train/eus_static_door_carriage_lowres/002 (-50.0, -1183.0, 150.0)
	100008 units/pd2_dlc1/props/details/ind_ext_prop_cable_straight_01_4m/001 (-199.0, -374.0, 388.682)
	100009 units/pd2_dlc1/props/details/ind_ext_prop_cable_straight_01_4m/002 (-199.0, -774.0, 388.682)
	100034 units/pd2_dlc1/props/details/ind_ext_prop_cable_straight_01_4m/003 (-199.0, -1174.0, 388.682)
	100035 units/pd2_dlc1/props/details/ind_ext_prop_cable_straight_01_4m/004 (199.0, 26.0, 388.682)
	100036 units/pd2_dlc1/props/details/ind_ext_prop_cable_straight_01_4m/005 (199.0, -374.0, 388.682)
	100037 units/pd2_dlc1/props/details/ind_ext_prop_cable_straight_01_4m/006 (199.0, -774.0, 388.682)
	100023 units/world/props/diamond_heist/int/construction_ladder/001 (210.143, -775.0, -25.641)
	100004 units/world/props/mansion/man_cover_int_shipping_crate_wide/001 (-124.0, -1086.0, 259.615)
	100005 units/world/props/mansion/man_cover_int_shipping_crate_wide/002 (-124.0, -1086.0, 149.615)
	100013 units/world/props/mansion/man_cover_int_shipping_crate_wide/003 (-140.145, -352.344, 149.615)
