ID range vs continent name:
	100000: world

statics
	100076 units/dev_tools/level_tools/dev_collision_1m_2/001 (-347.0, 4074.0, 0.0)
	100081 units/dev_tools/level_tools/dev_collision_1m_2/002 (-385.0, 3998.0, 0.0)
	100182 units/dev_tools/level_tools/door_blocker_1m/001 (-450.0, 3894.0, 0.0)
		disable_on_ai_graph True
	100168 units/dev_tools/level_tools/door_blocker_4x1/006 (2350.0, 4050.0, 0.0)
		disable_on_ai_graph True
	100169 units/dev_tools/level_tools/door_blocker_4x1/007 (1225.0, 4600.0, 0.0)
		disable_on_ai_graph True
	100170 units/dev_tools/level_tools/door_blocker_4x1/008 (1225.0, 3000.0, 0.0)
		disable_on_ai_graph True
	100171 units/dev_tools/level_tools/door_blocker_4x1/009 (2350.0, 3150.0, 0.0)
		disable_on_ai_graph True
	100172 units/dev_tools/level_tools/door_blocker_4x1/010 (1650.0, 2900.0, 0.0)
		disable_on_ai_graph True
	100082 units/dev_tools/level_tools/door_blocker_4x1/011 (1450.0, 2725.0, 0.0)
		disable_on_ai_graph True
	100174 units/dev_tools/level_tools/door_blocker_4x1/012 (-1850.0, 3150.0, 0.0)
		disable_on_ai_graph True
	100175 units/dev_tools/level_tools/door_blocker_4x1/013 (-1850.0, 4050.0, 0.0)
		disable_on_ai_graph True
	100176 units/dev_tools/level_tools/door_blocker_4x1/014 (-725.0, 4700.0, 0.0)
		disable_on_ai_graph True
	100163 units/dev_tools/level_tools/door_blocker_4x1/015 (-725.0, 3200.0, 0.0)
		disable_on_ai_graph True
	100037 units/dev_tools/level_tools/door_blocker_4x1/016 (1675.0, 4700.0, 0.0)
		disable_on_ai_graph True
	100100 units/dev_tools/level_tools/door_blocker_4x1/017 (1675.0, 3800.0, 0.0)
		disable_on_ai_graph True
	100069 units/dev_tools/level_tools/navigation_splitter/001 (-2100.0, 3100.0, 0.0)
	100071 units/dev_tools/level_tools/navigation_splitter/002 (-2100.0, 4025.0, 0.0)
	100072 units/dev_tools/level_tools/navigation_splitter/003 (-675.0, 4550.0, 0.0)
	100073 units/dev_tools/level_tools/navigation_splitter/004 (-675.0, 2950.0, 0.0)
	100078 units/dev_tools/level_tools/navigation_splitter/005 (1175.0, 3250.0, 0.0)
	100096 units/dev_tools/level_tools/navigation_splitter/006 (2600.0, 3175.0, 0.0)
	100181 units/dev_tools/level_tools/navigation_splitter/007 (2600.0, 4075.0, 0.0)
	100188 units/dev_tools/level_tools/navigation_splitter/008 (1175.0, 4850.0, 0.0)
	100065 units/payday2/architecture/ext_apartment/res_apartment_blockade_01/001 (1475.0, 4150.0, -9.00006)
		disable_on_ai_graph True
	100066 units/payday2/architecture/ext_apartment/res_apartment_blockade_01/002 (1475.0, 2700.0, 0.0)
		disable_on_ai_graph True
	100000 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/001 (1250.0, 3300.0, 0.0)
		disable_on_ai_graph True
	100014 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/003 (-750.0, 2900.0, 0.0)
		disable_on_ai_graph True
	100015 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/004 (1250.0, 4900.0, 0.0)
		disable_on_ai_graph True
	100018 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/005 (-1150.0, 2700.0, 0.0)
	100024 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/006 (-750.0, 4500.0, 0.0)
		disable_on_ai_graph True
	100027 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/007 (1650.0, 3600.0, 0.0)
		disable_on_ai_graph True
	100032 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/008 (1650.0, 2700.0, 0.0)
		disable_on_ai_graph True
	100034 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/009 (1650.0, 4500.0, 0.0)
		disable_on_ai_graph True
	100036 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/010 (-1150.0, 4500.0, 0.0)
	100008 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/002 (-1150.0, 3600.0, 0.0)
	100016 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/004 (1250.0, 4900.0, 0.0)
	100017 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/005 (-750.0, 4500.0, 0.0)
	100019 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/006 (1650.0, 2700.0, 0.0)
	100028 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/007 (1650.0, 4500.0, 0.0)
	100030 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/008 (-750.0, 2900.0, 0.0)
	100033 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/009 (1250.0, 3300.0, 0.0)
	100064 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/010 (1650.0, 3600.0, 0.0)
	100001 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/011 (-1750.0, 3175.0, 0.0)
	100012 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/012 (-1750.0, 4075.0, 0.0)
	100021 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/013 (2250.0, 4025.0, 0.0)
	100022 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/014 (2250.0, 3125.0, 0.0)
	100026 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken_doubleside/001 (225.0, 3500.0, 0.0)
	100023 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken_doubleside/002 (225.0, 4700.0, 0.0)
	100084 units/payday2/architecture/res_ext_apartment_power_cable_04/001 (250.0, 1900.0, 0.0)
	100085 units/payday2/architecture/res_ext_apartment_power_cable_05/001 (251.0, 1900.0, 0.0)
	100149 units/payday2/architecture/res_ext_apartment_power_cable_06/001 (253.0, 1899.0, 0.0)
	100161 units/payday2/architecture/res_ext_apartment_rooms_3windows_floor_3_in_gap/001 (250.0, 4900.0, 300.0)
		disable_on_ai_graph True
	100031 units/payday2/architecture/res_ext_apartment_rooms_3windows_floor_3_in_gap/002 (250.0, 2900.0, 300.0)
		disable_on_ai_graph True
	100193 units/payday2/architecture/res_ext_apartment_rooms_4windows_01_piece/001 (2200.0, 3125.0, 0.0)
		disable_on_ai_graph True
	100042 units/payday2/architecture/res_ext_apartment_rooms_4windows_01_piece/003 (-1150.0, 4550.0, 0.0)
	100074 units/payday2/architecture/res_ext_apartment_rooms_4windows_01_piece/005 (-1700.0, 4075.0, 0.0)
		disable_on_ai_graph True
	100058 units/payday2/architecture/res_ext_apartment_rooms_4windows_01_piece/007 (1650.0, 2650.0, 0.0)
		disable_on_ai_graph True
	100077 units/payday2/architecture/res_ext_apartment_rooms_4windows_02_piece/002 (2200.0, 4025.0, 0.0)
		disable_on_ai_graph True
	100055 units/payday2/architecture/res_ext_apartment_rooms_4windows_02_piece/004 (1650.0, 3550.0, 0.0)
		disable_on_ai_graph True
	100052 units/payday2/architecture/res_ext_apartment_rooms_4windows_03_piece/006 (-1150.0, 2750.0, 0.0)
	100057 units/payday2/architecture/res_ext_apartment_rooms_4windows_03_piece/007 (1650.0, 4450.0, 0.0)
		disable_on_ai_graph True
	100044 units/payday2/architecture/res_ext_apartment_rooms_4windows_04_piece/002 (-1700.0, 3175.0, 0.0)
		disable_on_ai_graph True
	100039 units/payday2/architecture/res_ext_apartment_wardrobe_variation/001 (2650.0, 3150.0, 0.0)
		mesh_variation broken
	100041 units/payday2/architecture/res_ext_apartment_wardrobe_variation/002 (-700.0, 4500.0, 0.0)
		mesh_variation broken
	100045 units/payday2/architecture/res_ext_apartment_wardrobe_variation/003 (-2150.0, 4050.0, 0.0)
		mesh_variation broken
	100053 units/payday2/architecture/res_ext_apartment_wardrobe_variation/004 (-2150.0, 3150.0, 0.0)
		mesh_variation broken
	100068 units/payday2/architecture/res_ext_apartment_wardrobe_variation/005 (1200.0, 4900.0, 0.0)
		mesh_variation broken
	100006 units/payday2/architecture/res_ext_apartment_wardrobe_variation/006 (1200.0, 3300.0, 0.0)
		mesh_variation broken
	100038 units/payday2/architecture/res_ext_apartment_wardrobe_variation/007 (2650.0, 4050.0, 0.0)
		mesh_variation broken
	100025 units/payday2/architecture/res_ext_apartment_wardrobe_variation/008 (-700.0, 2900.0, 0.0)
		mesh_variation broken
	100146 units/pd2_dlc_miami/props/gen_prop_door_metal_magnetic/001 (-450.0, 4100.0, 2.28882e-05)
		mesh_variation anim_door_open
	100162 units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/002 (-1684.0, 4499.0, 100.0)
		disable_on_ai_graph True
	100112 units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/003 (-1684.0, 3600.0, 100.0)
		disable_on_ai_graph True
	100184 units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/004 (-1684.0, 2700.0, 100.0)
		disable_on_ai_graph True
