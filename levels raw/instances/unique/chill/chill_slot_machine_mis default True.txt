﻿´STARTUP´ MissionScriptElement 100006
	EXECUTE ON STARTUP
	on_executed
		´DELAY´ (delay 2)
		´set_chill_state´ (delay 5)
		´disable´ (delay 4)
´INIT´ MissionScriptElement 100007
	on_executed
		´pick_1´ (delay 0)
		´harder difficulty´ (delay 0)
		´hide_chips_start´ (delay 2)
´DELAY´ MissionScriptElement 100008
	on_executed
		´INIT´ (delay 0)
		´hide_chips_start´ (delay 0)
		´if_bonnie_lvl_2´ (delay 3)
´random_start_001´ ElementUnitSequence 100009
	position -1000.0, -325.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_01
			time 0
´random_start_002´ ElementUnitSequence 100010
	position -1000.0, -400.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_02
			time 0
´random_start_003´ ElementUnitSequence 100011
	position -1000.0, -475.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_03
			time 0
´random_start_004´ ElementUnitSequence 100012
	position -1000.0, -550.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_04
			time 0
´random_start_005´ ElementUnitSequence 100013
	position -1000.0, -625.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_05
			time 0
´random_start_006´ ElementUnitSequence 100014
	position -1000.0, -700.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_06
			time 0
´random_start_007´ ElementUnitSequence 100015
	position -1000.0, -775.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_07
			time 0
´random_start_008´ ElementUnitSequence 100016
	position -1000.0, -850.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_08
			time 0
´random_start_009´ ElementUnitSequence 100017
	position -1000.0, -925.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_09
			time 0
´random_start_010´ ElementUnitSequence 100018
	position -1000.0, -1000.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_10
			time 0
´random_start_011´ ElementUnitSequence 100019
	position -1000.0, -1075.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_11
			time 0
´random_start_012´ ElementUnitSequence 100020
	position -1000.0, -1150.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_12
			time 0
´random_start_013´ ElementUnitSequence 100021
	position -1000.0, -1225.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_13
			time 0
´random_start_014´ ElementUnitSequence 100022
	position -1000.0, -1300.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_14
			time 0
´random_start_015´ ElementUnitSequence 100023
	position -1000.0, -1375.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_15
			time 0
´random_start_016´ ElementUnitSequence 100024
	position -1000.0, -1450.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_16
			time 0
´random_start_017´ ElementUnitSequence 100025
	position -1000.0, -1525.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_17
			time 0
´random_start_018´ ElementUnitSequence 100026
	position -1000.0, -1600.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_18
			time 0
´random_start_019´ ElementUnitSequence 100027
	position -1000.0, -1675.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_starting_slot_position_19
			time 0
´random_win_001´ ElementUnitSequence 100029
	position -1750.0, 25.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence win01_pick_one_randomly_after_interact
			time 0
	on_executed
		´start_slots´ (delay 0)
´random_win_002´ ElementUnitSequence 100030
	position -1750.0, -50.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence win02_pick_one_randomly_after_interact
			time 0
	on_executed
		´start_slots´ (delay 0)
´random_win_003´ ElementUnitSequence 100031
	position -1750.0, -125.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence win03_pick_one_randomly_after_interact
			time 0
	on_executed
		´start_slots´ (delay 0)
´random_win_004´ ElementUnitSequence 100032
	position -1750.0, -200.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence win04_pick_one_randomly_after_interact
			time 0
	on_executed
		´start_slots´ (delay 0)
´random_win_005´ ElementUnitSequence 100033
	position -1750.0, -275.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence win05_pick_one_randomly_after_interact
			time 0
	on_executed
		´start_slots´ (delay 0)
´random_win_006´ ElementUnitSequence 100034
	position -1750.0, -350.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence win06_pick_one_randomly_after_interact
			time 0
	on_executed
		´start_slots´ (delay 0)
´random_win_007´ ElementUnitSequence 100035
	position -1750.0, -425.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence win07_pick_one_randomly_after_interact
			time 0
	on_executed
		´start_slots´ (delay 0)
´random_win_008´ ElementUnitSequence 100036
	position -1750.0, -500.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence win08_pick_one_randomly_after_interact
			time 0
	on_executed
		´start_slots´ (delay 0)
´random_win_009´ ElementUnitSequence 100037
	position -1750.0, -575.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence win09_pick_one_randomly_after_interact
			time 0
	on_executed
		´start_slots´ (delay 0)
´random_win_010´ ElementUnitSequence 100038
	position -1750.0, -650.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence win10_pick_one_randomly_after_interact
			time 0
	on_executed
		´start_slots´ (delay 0)
´random_win_011´ ElementUnitSequence 100039
	position -1750.0, -725.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence win11_pick_one_randomly_after_interact
			time 0
	on_executed
		´start_slots´ (delay 0)
´random_win_012´ ElementUnitSequence 100040
	position -1750.0, -800.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence win12_pick_one_randomly_after_interact
			time 0
	on_executed
		´start_slots´ (delay 0)
´lose´ ElementUnitSequence 100041
	position -1750.0, -875.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence lose_pick_one_randomly_after_interact
			time 0
	on_executed
		´start_slots´ (delay 0)
´enable int slot a´ ElementUnitSequence 100042
	position -875.0, -75.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
´pick_1´ ElementRandom 100043
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´random_start_001´ (delay 0)
		´random_start_002´ (delay 0)
		´random_start_003´ (delay 0)
		´random_start_004´ (delay 0)
		´random_start_005´ (delay 0)
		´random_start_006´ (delay 0)
		´random_start_007´ (delay 0)
		´random_start_008´ (delay 0)
		´random_start_009´ (delay 0)
		´random_start_010´ (delay 0)
		´random_start_011´ (delay 0)
		´random_start_012´ (delay 0)
		´random_start_013´ (delay 0)
		´random_start_014´ (delay 0)
		´random_start_015´ (delay 0)
		´random_start_016´ (delay 0)
		´random_start_017´ (delay 0)
		´random_start_018´ (delay 0)
		´random_start_019´ (delay 0)
´1_chance´ ElementLogicChance 100044
	chance 15
	position -1125.0, -450.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
´fail´ ElementLogicChanceTrigger 100045
	elements
		1 ´1_chance´
	outcome fail
	position -1175.0, -500.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´lose´ (delay 0)
		´pick_1´ (delay 0)
		´chance to be angry´ (delay 6)
		´logic_chance_001´ (delay 7)
		´start_blink´ (delay 0)
´success´ ElementLogicChanceTrigger 100046
	elements
		1 ´1_chance´
	outcome success
	position -1175.0, -400.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´pick_1_2´ (delay 0)
		´use instigator to win´ (delay 0)
		´show_winning_chips_a´ (delay 0) DISABLED
		´show_winning_chips_b´ (delay 0) DISABLED
		´show_winning_chips_c´ (delay 0) DISABLED
		´logic_chance_operator_001´ (delay 0)
		´logic_chance_002´ (delay 7)
		´start_blink´ (delay 0)
		´func_money_001´ (delay 0)
´pick_1_2´ ElementRandom 100047
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´random_win_012´ (delay 0)
		´random_win_011´ (delay 0)
		´random_win_010´ (delay 0)
		´random_win_009´ (delay 0)
		´random_win_008´ (delay 0)
		´random_win_007´ (delay 0)
		´random_win_006´ (delay 0)
		´random_win_005´ (delay 0)
		´random_win_004´ (delay 0)
		´random_win_003´ (delay 0)
		´random_win_002´ (delay 0)
		´random_win_001´ (delay 0)
´slotmachine_INTERACT´ ElementUnitSequenceTrigger 100048
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
	on_executed
		´7_sec_delay´ (delay 7)
		´1_chance´ (delay 0)
		´func_money_002´ (delay 0)
´7_sec_delay´ MissionScriptElement 100049
	on_executed
		´enable int slot a´ (delay 0)
		´enable int slot b´ (delay 0)
		´enable int slot c´ (delay 0)
´start_slots´ ElementUnitSequence 100053
	position -2000.0, -500.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence start_slots
			time 0
´slotmachine_BREAK´ ElementUnitSequenceTrigger 100000
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence int_seq_break
			unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
	on_executed
		´enable_interaction_OFF´ (delay 0)
´enable_interaction_OFF´ ElementToggle 100002
	elements
		1 ´show_chips_c´
		2 ´show_chips_b´
		3 ´show_chips_a´
	set_trigger_times -1
	toggle off
´play´ ElementSpecialObjective 100054
	TRIGGER TIMES 1
	SO_access 3
	action_duration_max 0
	action_duration_min 0
	ai_group civilians
	align_position True
	align_rotation True
	attitude none
	base_chance 1
	chance_inc 0
	forced False
	interaction_voice none
	interrupt_dis -1
	interrupt_dmg 0
	interrupt_objective False
	interval -1
	needs_pos_rsrv True
	path_haste none
	path_stance none
	path_style none
	patrol_path none
	pose none
	position 4.0, 68.0, 0.0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	scan True
	search_distance 0
	search_position -150.0, 400.0, 400.0
	so_action cmf_so_slotmachine
	trigger_on none
	use_instigator True
´ai_insert´ ElementInstanceInput 100052
	event AI_Insert
	on_executed
		´link´ (delay 0)
		´instance_busy´ (delay 0) DISABLED
´win´ ElementSpecialObjective 100055
	SO_access 3
	action_duration_max 0
	action_duration_min 0
	ai_group civilians
	align_position False
	align_rotation False
	attitude none
	base_chance 1
	chance_inc 0
	forced True
	interaction_voice none
	interrupt_dis -1
	interrupt_dmg 0
	interrupt_objective False
	interval -1
	needs_pos_rsrv True
	path_haste none
	path_stance none
	path_style none
	patrol_path none
	pose none
	position -2200.0, -1325.0, 402.5
	rotation 0.0, 0.0, 0.707107, -0.707107
	scan True
	search_distance 0
	search_position -2221.38, -1365.71, 402.5
	so_action cmf_so_slotmachine_win
	trigger_on none
	use_instigator True
	on_executed
		´add 50´ (delay 0)
		´add 10´ (delay 0) DISABLED
´lose_2´ ElementSpecialObjective 100056
	SO_access 3
	action_duration_max 0
	action_duration_min 0
	ai_group civilians
	align_position False
	align_rotation False
	attitude none
	base_chance 1
	chance_inc 0
	forced True
	interaction_voice none
	interrupt_dis -1
	interrupt_dmg 0
	interrupt_objective False
	interval -1
	needs_pos_rsrv True
	no_arrest False
	path_haste none
	path_stance none
	path_style none
	patrol_path none
	pose none
	position -2200.0, -1500.0, 402.5
	repeatable True
	rotation 0.0, 0.0, 0.707107, -0.707107
	scan True
	search_distance 0
	search_position -2282.24, -1577.43, 402.5
	so_action cmf_so_slotmachine_loose
	trigger_on none
	use_instigator True
	on_executed
		´remove 50´ (delay 0) DISABLED
		´remove 10´ (delay 0)
´disable interaction´ ElementUnitSequence 100057
	position -2325.0, -1425.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_interaction_disable
			time 0
´show_slotmachine_c´ ElementInstanceInput 100058
	TRIGGER TIMES 1
	event show_slotmachine_c
	on_executed
		´logic_toggle_003´ (delay 0)
		´s2´ (delay 0)
´enable int slot b´ ElementUnitSequence 100065
	position -875.0, -150.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
´enable int slot c´ ElementUnitSequence 100066
	position -875.0, -225.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_interaction_enable
			time 0
´disable other machines´ ElementToggle 100069
	elements
		1 ´enable int slot a´
		2 ´enable int slot b´
	set_trigger_times -1
	toggle off
´ai lever pulled´ ElementSpecialObjectiveTrigger 100070
	elements
		1 ´play´
	event anim_start
	position -1250.0, -150.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´1_chance´ (delay 3.2)
		´point_play_sound_001´ (delay 0) DISABLED
´chance to stay´ ElementLogicChance 100071
	chance 100
	position -1850.0, -1400.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
´add 50´ ElementLogicChanceOperator 100072
	chance 20
	elements
		1 ´chance to stay´
	operation add_chance
	position -2100.0, -1325.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
´remove 10´ ElementLogicChanceOperator 100073
	chance 10
	elements
		1 ´chance to stay´
	operation subtract_chance
	position -2100.0, -1500.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
´success_2´ ElementLogicChanceTrigger 100074
	elements
		1 ´chance to stay´
	outcome success
	position -1750.0, -1325.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´set instigator to play´ (delay 0)
´fail_2´ ElementLogicChanceTrigger 100075
	elements
		1 ´chance to stay´
	outcome fail
	position -1750.0, -1475.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´set instigator´ (delay 0)
´character leaves´ ElementInstanceOutput 100076
	event character leaves
	on_executed
		´enable int slot a´ (delay 0)
		´enable int slot b´ (delay 0)
		´enable int slot c´ (delay 0)
		´clear instigator´ (delay 0.1)
´harder difficulty´ ElementFilter 100078
	difficulty_easy False
	difficulty_easy_wish True
	difficulty_hard False
	difficulty_normal False
	difficulty_overkill False
	difficulty_overkill_145 True
	difficulty_overkill_290 True
	difficulty_sm_wish True
	mode_assault False
	mode_control False
	platform_ps3 False
	platform_win32 False
	player_1 False
	player_2 False
	player_3 False
	player_4 False
	position -875.0, 125.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´switch dificulty chance operators´ (delay 0)
´add 10´ ElementLogicChanceOperator 100077
	DISABLED
	chance 10
	elements
		1 ´chance to stay´
	operation add_chance
	position -2100.0, -1200.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
´remove 50´ ElementLogicChanceOperator 100079
	DISABLED
	chance 50
	elements
		1 ´chance to stay´
	operation subtract_chance
	position -2100.0, -1625.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
´switch dificulty chance operators´ ElementToggle 100080
	TRIGGER TIMES 1
	elements
		1 ´remove 50´ DISABLED
		2 ´remove 10´
		3 ´add 50´
		4 ´add 10´ DISABLED
	set_trigger_times -1
	toggle toggle
´done anim´ ElementSpecialObjectiveTrigger 100081
	elements
		1 ´win´
		2 ´lose_2´
	event anim_start
	position -1975.0, -1400.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´chance to stay´ (delay 3)
´instigator´ ElementInstigator 100082
´record instigator´ ElementInstigatorOperator 100083
	elements
		1 ´instigator´
	keep_on_use True
	operation add_first
	on_executed
		´toggle´ (delay 0.1)
´use instigator to win´ ElementInstigatorOperator 100084
	elements
		1 ´instigator´
	keep_on_use True
	operation use_first
	on_executed
		´win´ (delay 6)
´use instigator to lose´ ElementInstigatorOperator 100085
	elements
		1 ´instigator´
	keep_on_use True
	operation use_first
	on_executed
		´lose_2´ (delay 0)
´set instigator to play´ ElementInstigatorOperator 100086
	elements
		1 ´instigator´
	keep_on_use True
	operation use_first
	on_executed
		´play´ (delay 0)
´set instigator´ ElementInstigatorOperator 100087
	elements
		1 ´instigator´
	keep_on_use True
	operation use_first
	on_executed
		´character leaves´ (delay 0)
´clear instigator´ ElementInstigatorOperator 100088
	elements
		1 ´instigator´
	keep_on_use False
	operation clear
	on_executed
		´reset chance´ (delay 0)
		´toggle´ (delay 0)
´reset chance´ ElementLogicChanceOperator 100089
	chance 0
	elements
		1 ´chance to stay´
	operation reset
	position -1450.0, -1400.0, 400.0
	rotation 0.0, 0.0, 0.0, -1.0
´chance to be angry´ ElementLogicChance 100090
	chance 15
	position -2200.0, -1725.0, 400.0
	rotation 0.0, 0.0, 0.0, -1.0
´if angry´ ElementLogicChanceTrigger 100091
	elements
		1 ´chance to be angry´
	outcome success
	position -2200.0, -1625.0, 400.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´use instigator to lose´ (delay 0)
´if not angry´ ElementLogicChanceTrigger 100092
	elements
		1 ´chance to be angry´
	outcome fail
	position -2125.0, -1725.0, 400.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´remove 50´ (delay 0) DISABLED
		´remove 10´ (delay 0)
		´chance to stay´ (delay 0)
´toggle´ ElementToggle 100093
	elements
		1 ´link´
		2 ´instance_busy´ DISABLED
		3 ´logic_chance_operator_001´
		4 ´show_chips_a´
		5 ´show_chips_b´
		6 ´show_chips_c´
	set_trigger_times -1
	toggle toggle
´link´ MissionScriptElement 100094
	on_executed
		´record instigator´ (delay 0)
		´disable interaction´ (delay 0)
		´set instigator to play´ (delay 0)
´instance_busy´ ElementInstanceOutput 100095
	DISABLED
	event instance occupied
´show_winning_chips_a´ MissionScriptElement 100097
	DISABLED
	on_executed
		´show_chips_a´ (delay 8)
´show_winning_chips_b´ MissionScriptElement 100098
	DISABLED
	on_executed
		´show_chips_b´ (delay 8)
´show_winning_chips_c´ MissionScriptElement 100099
	DISABLED
	on_executed
		´show_chips_c´ (delay 8)
´hide_chips_start´ ElementUnitSequence 100102
	position -700.0, -75.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_cashout_ticket/money_ticket_c (-18.0, 21.0001, 135.0)
			notify_unit_sequence state_vis_hide
			time 0
´show_chips_a´ ElementUnitSequence 100103
	position -1500.0, -175.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
´show_chips_b´ ElementUnitSequence 100104
	position -1500.0, -250.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
´show_chips_c´ ElementUnitSequence 100105
	position -1500.0, -325.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_cashout_ticket/money_ticket_c (-18.0, 21.0001, 135.0)
			notify_unit_sequence state_vis_show
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_cashout_ticket/money_ticket_c (-18.0, 21.0001, 135.0)
			notify_unit_sequence state_interaction_enabled
			time 0
´logic_toggle_001´ ElementToggle 100106
	elements
		1 ´show_winning_chips_a´ DISABLED
	set_trigger_times -1
	toggle on
´logic_toggle_002´ ElementToggle 100107
	elements
		1 ´show_winning_chips_b´ DISABLED
	set_trigger_times -1
	toggle on
´logic_toggle_003´ ElementToggle 100108
	elements
		1 ´show_winning_chips_c´ DISABLED
	set_trigger_times -1
	toggle on
´logic_chance_operator_001´ ElementLogicChanceOperator 100109
	chance 0
	elements
		1 ´1_chance´
	operation set_chance
	position -1275.0, -425.0, 400.0
	rotation 0.0, 0.0, 0.0, -1.0
´point_play_sound_001´ ElementPlaySound 100116
	DISABLED
	append_prefix False
	elements
	interrupt True
	position -1250.0, -75.0, 400.0
	rotation 0.0, 0.0, 0.0, -1.0
	sound_event Play_cv2_ca1_01
	use_instigator True
´logic_chance_001´ ElementLogicChance 100117
	chance 10
	position -1125.0, -550.0, 400.0
	rotation 0.0, 0.0, 0.0, -1.0
´logic_chance_002´ ElementLogicChance 100118
	chance 10
	position -1125.0, -375.0, 400.0
	rotation 0.0, 0.0, 0.0, -1.0
´trg_police_called´ ElementGlobalEventTrigger 100050
	TRIGGER TIMES 1
	global_event police_called
	position -1125.0, 100.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´seq_stop_looping_sound´ (delay 0)
´seq_stop_looping_sound´ ElementUnitSequence 100051
	position -1250.0, 100.0, 402.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence sound_slotmachines_loop_stop
			time 0
´start_blink´ ElementUnitSequence 100119
	position -1475.0, -750.0, 400.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_blinking
			time 0
	on_executed
		´stop_blink´ (delay 8)
´stop_blink´ ElementUnitSequence 100028
	position -1475.0, -850.0, 400.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_not_blinking
			time 0
´if_bonnie_lvl_2´ ElementCustomSafehouseFilter 100005
	check_type greater_or_equal
	position -900.0, 600.0, 400.0
	room_id bonnie
	room_tier 2
	rotation 0.0, 0.0, 0.0, -1.0
	tier_check current
	on_executed
		´s2´ (delay 0)
´func_money_001´ ElementMoney 100001
	action AddSpending
	amount ´slotmachine_BREAK´
	only_local_player True
	position -1175.0, -325.0, 400.0
	remove_all False
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´point_debug_002´ (delay 0)
´func_money_002´ ElementMoney 100003
	action DeductOffshore
	amount 10000
	only_local_player True
	position -1125.0, -75.0, 400.0
	remove_all False
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´point_debug_001´ (delay 0)
´point_debug_001´ ElementDebug 100059
	as_subtitle False
	debug_string deduct 10000 spending
	show_instigator False
´point_debug_002´ ElementDebug 100060
	as_subtitle False
	debug_string Add 100000
	show_instigator False
´set_chill_state´ ElementUnitSequence 100061
	only_for_local_player True
	position -600.0, 100.0, 400.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_chill_slot_machine
			time 0
´disable´ ElementUnitSequence 100067
	position -700.0, 100.0, 400.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_interaction_disable
			time 0
´func_sequence_001´ ElementUnitSequence 100062
	position -1000.0, 800.0, 400.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
			notify_unit_sequence state_interaction_disable
			time 0
´func_instance_input_001´ ElementInstanceInput 100068
	BASE DELAY 5
	event disable_interaction
	on_executed
		´func_sequence_001´ (delay 0)
´s2´ ElementLoadDelayed 100063
	unit_ids
		1 units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c_base_high/base_c (1.0, 6.00005, 0.0)
		2 units/pd2_dlc_casino/props/cas_prop_gambling_slotmachine_c/slotmachine_c (0.0, 0.0, 87.0)
	on_executed
		´enable int slot c´ (delay 0)
		´disable other machines´ (delay 0)
