﻿´disable_all´ ElementDisableUnit 100070
	position 300.0, -1500.0, 2.5
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/pd2_dlc_chill/props/chl_props_trophies/props_trophyharvest_sign/chl_prop_trophy_harvest_sign/001 (168.0, -697.0, 80.0)
		2 units/pd2_dlc_chill/props/chl_props_trophies/props_trophyharvest_sign/chl_prop_trophy_harvest_sign/003 (149.0, -529.0, 107.0)
		3 units/pd2_dlc_chill/props/chl_props_trophies/props_trophyharvest_sign/chl_prop_trophy_harvest_sign/002 (149.0, -529.0, 107.0)
´startup´ MissionScriptElement 100072
	EXECUTE ON STARTUP
	on_executed
		´delay´ (delay 1)
		´disable_all_2´ (delay 0)
´delay´ MissionScriptElement 100074
	on_executed
		´tier_3´ (delay 0)
		´tier_2´ (delay 0)
		´tier_1´ (delay 0)
´disable_all_2´ MissionScriptElement 100076
	BASE DELAY 0.5
	on_executed
		´disable_all´ (delay 0)
		´disable_all_crimenet_laptops´ (delay 0)
´tier_1´ ElementCustomSafehouseFilter 100078
	check_type equal
	position -100.0, -1500.0, 2.5
	room_id russian
	room_tier 1
	rotation 0.0, 0.0, 0.0, -1.0
	tier_check current
	on_executed
		´s1´ (delay 0)
´tier_2´ ElementCustomSafehouseFilter 100080
	check_type equal
	position 0.0, -1500.0, 2.5
	room_id russian
	room_tier 2
	rotation 0.0, 0.0, 0.0, -1.0
	tier_check current
	on_executed
		´s2´ (delay 0)
´tier_3´ ElementCustomSafehouseFilter 100082
	check_type equal
	position 100.0, -1500.0, 2.5
	room_id russian
	room_tier 3
	rotation 0.0, 0.0, 0.0, -1.0
	tier_check current
	on_executed
		´s3´ (delay 0)
´enable_crimenet_laptop_001´ ElementUnitSequence 100119
	position -100.0, -1700.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_laptop_crimenet/S1_gen_interactable_laptop_crimenet_001 (-7.00012, -286.0, 74.1498)
			notify_unit_sequence interaction_enable
			time 0
´enable_crimenet_laptop_002´ ElementUnitSequence 100120
	position 0.0, -1700.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_laptop_crimenet/S2_gen_interactable_laptop_crimenet (-288.964, -303.902, 75.0)
			notify_unit_sequence interaction_enable
			time 0
´enable_crimenet_laptop_003´ ElementUnitSequence 100121
	position 100.0, -1700.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_laptop_crimenet/S3_gen_interactable_laptop_crimenet_003 (167.0, -591.0, 80.0)
			notify_unit_sequence interaction_enable
			time 0
´disable_all_crimenet_laptops´ ElementUnitSequence 100122
	position 400.0, -1400.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_laptop_crimenet/S1_gen_interactable_laptop_crimenet_001 (-7.00012, -286.0, 74.1498)
			notify_unit_sequence interaction_disable
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_laptop_crimenet/S2_gen_interactable_laptop_crimenet (-288.964, -303.902, 75.0)
			notify_unit_sequence interaction_disable
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_laptop_crimenet/S3_gen_interactable_laptop_crimenet_003 (167.0, -591.0, 80.0)
			notify_unit_sequence interaction_disable
			time 0
´interacted_with_laptop´ ElementUnitSequenceTrigger 100123
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/equipment/gen_interactable_laptop_crimenet/S1_gen_interactable_laptop_crimenet_001 (-7.00012, -286.0, 74.1498)
		2
			guis_id 2
			sequence interact
			unit_id units/payday2/equipment/gen_interactable_laptop_crimenet/S2_gen_interactable_laptop_crimenet (-288.964, -303.902, 75.0)
		3
			guis_id 3
			sequence interact
			unit_id units/payday2/equipment/gen_interactable_laptop_crimenet/S3_gen_interactable_laptop_crimenet_003 (167.0, -591.0, 80.0)
	on_executed
		´func_mission_end_leave_safehouse´ (delay 0)
		´logic_custom_safehouse_filter_001´ (delay 0)
		´logic_custom_safehouse_filter_002´ (delay 0)
		´logic_custom_safehouse_filter_003´ (delay 0)
´func_mission_end_leave_safehouse´ ElementMissionEnd 100124
	position 400.0, -1700.0, 2.5
	rotation 0.0, 0.0, 0.0, -1.0
	state leave_safehouse
´logic_custom_safehouse_filter_001´ ElementCustomSafehouseFilter 100025
	check_type equal
	position 300.0, -1800.0, 2.5
	room_id russian
	room_tier 1
	rotation 0.0, 0.0, 0.0, -1.0
	tier_check current
	on_executed
		´func_sequence_001´ (delay 1)
´logic_custom_safehouse_filter_002´ ElementCustomSafehouseFilter 100126
	check_type equal
	position 400.0, -1800.0, 2.5
	room_id russian
	room_tier 2
	rotation 0.0, 0.0, 0.0, -1.0
	tier_check current
	on_executed
		´func_sequence_002´ (delay 1)
´logic_custom_safehouse_filter_003´ ElementCustomSafehouseFilter 100127
	check_type equal
	position 500.0, -1800.0, 2.5
	room_id russian
	room_tier 3
	rotation 0.0, 0.0, 0.0, -1.0
	tier_check current
	on_executed
		´func_sequence_003´ (delay 1)
´func_sequence_001´ ElementUnitSequence 100128
	position 300.0, -1900.0, 2.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_laptop_crimenet/S1_gen_interactable_laptop_crimenet_001 (-7.00012, -286.0, 74.1498)
			notify_unit_sequence interaction_enable
			time 0
´func_sequence_002´ ElementUnitSequence 100129
	position 400.0, -1900.0, 2.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_laptop_crimenet/S2_gen_interactable_laptop_crimenet (-288.964, -303.902, 75.0)
			notify_unit_sequence interaction_enable
			time 0
´func_sequence_003´ ElementUnitSequence 100130
	position 500.0, -1900.0, 2.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_laptop_crimenet/S3_gen_interactable_laptop_crimenet_003 (167.0, -591.0, 80.0)
			notify_unit_sequence interaction_enable
			time 0
´func_instance_input_002´ ElementInstanceInput 100131
	event toggle_trophy
	on_executed
		´logic_custom_safehouse_filter_004´ (delay 0)
		´logic_custom_safehouse_filter_005´ (delay 0)
		´logic_custom_safehouse_filter_006´ (delay 0)
´logic_custom_safehouse_filter_004´ ElementCustomSafehouseFilter 100132
	check_type equal
	position 1100.0, -1900.0, 100.0
	room_id russian
	room_tier 1
	rotation 0.0, 0.0, 0.0, -1.0
	tier_check current
	on_executed
		´func_enable_unit_001´ (delay 0)
´logic_custom_safehouse_filter_005´ ElementCustomSafehouseFilter 100133
	check_type equal
	position 1200.0, -1900.0, 100.0
	room_id russian
	room_tier 2
	rotation 0.0, 0.0, 0.0, -1.0
	tier_check current
	on_executed
		´func_enable_unit_002´ (delay 0)
´logic_custom_safehouse_filter_006´ ElementCustomSafehouseFilter 100134
	check_type equal
	position 1300.0, -1900.0, 100.0
	room_id russian
	room_tier 3
	rotation 0.0, 0.0, 0.0, -1.0
	tier_check current
	on_executed
		´func_enable_unit_003´ (delay 0)
´func_enable_unit_001´ ElementEnableUnit 100135
	position 1100.0, -1800.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/pd2_dlc_chill/props/chl_props_trophies/props_trophyharvest_sign/chl_prop_trophy_harvest_sign/002 (149.0, -529.0, 107.0)
´func_enable_unit_002´ ElementEnableUnit 100136
	position 1200.0, -1800.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/pd2_dlc_chill/props/chl_props_trophies/props_trophyharvest_sign/chl_prop_trophy_harvest_sign/003 (149.0, -529.0, 107.0)
´func_enable_unit_003´ ElementEnableUnit 100137
	position 1300.0, -1800.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/pd2_dlc_chill/props/chl_props_trophies/props_trophyharvest_sign/chl_prop_trophy_harvest_sign/001 (168.0, -697.0, 80.0)
´s1´ ElementLoadDelayed 100003
	unit_ids
		1 units/payday2/props/com_prop_election_chair/S1_com_prop_election_chair_001 (36.1979, -197.183, 0.0)
		2 units/payday2/props/com_prop_election_table/S1_com_prop_election_table_001 (49.9999, -275.0, 0.0)
		3 units/payday2/props/com_prop_election_table_folded/S1_com_prop_election_table_folded_001 (-41.7445, -315.112, 0.0)
		4 units/payday2/equipment/gen_interactable_laptop_crimenet/S1_gen_interactable_laptop_crimenet_001 (-7.00012, -286.0, 74.1498)
		5 units/payday2/props/set/ind_prop_warehouse_box_b/S1_ind_prop_warehouse_box_b_002 (-373.083, -300.456, 0.0)
		6 units/payday2/props/set/ind_prop_warehouse_box_d/S1_ind_prop_warehouse_box_d_001 (-75.0, -39.0001, 108.0)
		7 units/payday2/props/set/ind_prop_warehouse_box_d/S1_ind_prop_warehouse_box_d_003 (-472.444, -219.411, 0.0)
		8 units/payday2/props/set/ind_prop_warehouse_box_d/S1_ind_prop_warehouse_box_d_005 (95.9999, -574.0, -6.10352e-05)
		9 units/payday2/props/set/ind_prop_warehouse_box_e/S1_ind_prop_warehouse_box_e_001 (183.0, -602.0, 108.0)
		10 units/payday2/props/set/ind_prop_warehouse_box_e/S1_ind_prop_warehouse_box_e_002 (-400.0, -200.0, 108.0)
		11 units/payday2/props/set/ind_prop_warehouse_box_f/S1_ind_prop_warehouse_box_f_001 (138.0, -27.0002, 0.0)
		12 units/payday2/props/set/ind_prop_warehouse_box_stack_4/S1_ind_prop_warehouse_box_stack_4_001 (56.9999, -40.0001, 0.0)
		13 units/payday2/props/set/ind_prop_warehouse_box_stack_8/S1_ind_prop_warehouse_box_stack_8_001 (-75.0, -49.0001, 0.0)
		14 units/payday2/props/set/ind_prop_warehouse_box_stack_8/S1_ind_prop_warehouse_box_stack_8_003 (-400.0, -200.0, 0.0)
		15 units/payday2/props/set/ind_prop_warehouse_box_stack_8/S1_ind_prop_warehouse_box_stack_8_004 (171.0, -574.0, -6.10352e-05)
		16 units/payday2/props/off_prop_officehigh_filebox_long_open_b/S1_off_prop_officehigh_filebox_long_open_b_001 (2.99988, -332.0, 0.0)
		17 units/payday2/props/off_prop_officehigh_filebox_long_open_b/S1_off_prop_officehigh_filebox_long_open_b_002 (138.0, -27.0002, 67.0)
		18 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/S1_off_prop_officehigh_filebox_short_whole_b_001 (105.0, -272.0, 0.0)
		19 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/S1_off_prop_officehigh_filebox_short_whole_b_002 (138.0, -27.0002, 42.0)
		20 units/payday2/props/paperstacks/off_prop_officehigh_paper_stack_01/S1_off_prop_officehigh_paper_stack_01_001 (114.785, -278.132, 75.1498)
		21 units/payday2/props/paperstacks/off_prop_officehigh_paper_stack_02/S1_off_prop_officehigh_paper_stack_02_001 (34.4435, -304.243, 74.1498)
		22 units/payday2/props/paperstacks/off_prop_officehigh_paper_stack_03/S1_off_prop_officehigh_paper_stack_03_001 (-39.8757, -249.66, 0.0)
	on_executed
		´enable_crimenet_laptop_001´ (delay 1)
´s2´ ElementLoadDelayed 100005
	unit_ids
		1 units/world/props/bridge/construction_blueprint/blueprint/S2_blueprint_001 (-356.547, -282.554, 76.0)
		2 units/payday2/props/com_prop_election_table/S2_com_prop_election_table_001 (-341.207, -300.82, 0.0)
		3 units/payday2/props/com_prop_election_table/S2_com_prop_election_table_002 (-358.514, -217.307, 0.0)
		4 units/payday2/equipment/gen_interactable_laptop_crimenet/S2_gen_interactable_laptop_crimenet (-288.964, -303.902, 75.0)
		5 units/payday2/props/set/ind_prop_warehouse_box_b/S2_ind_prop_warehouse_box_b_001 (-377.444, -309.3, 0.0)
		6 units/payday2/props/set/ind_prop_warehouse_box_b/S2_ind_prop_warehouse_box_b_003 (-387.0, -209.0, 74.1498)
		7 units/payday2/props/set/ind_prop_warehouse_box_d/S2_ind_prop_warehouse_box_d_002 (-309.38, -295.044, 0.0)
		8 units/payday2/props/set/ind_prop_warehouse_box_d/S2_ind_prop_warehouse_box_d_004 (96.0, -574.0, -6.10352e-05)
		9 units/payday2/props/set/ind_prop_warehouse_box_e/S2_ind_prop_warehouse_box_e_003 (183.0, -602.0, 108.0)
		10 units/payday2/props/set/ind_prop_warehouse_box_stack_8/S2_ind_prop_warehouse_box_stack_8_002 (171.0, -574.0, -6.10352e-05)
		11 units/payday2/props/off_prop_appliance_keyboard/S2_off_prop_appliance_keyboard_002 (7.04602, -256.479, 74.3717)
		12 units/payday2/props/off_prop_appliance_mouse/S2_off_prop_appliance_mouse_002 (-25.9538, -247.479, 74.3717)
		13 units/payday2/props/off_prop_generic_desk/S2_off_prop_generic_desk_001 (0.0, -262.0, 0.0)
		14 units/payday2/props/off_prop_office_monitor/S2_off_prop_office_monitor_003 (-33.0, -271.0, 74.3717)
		15 units/payday2/props/off_prop_officehigh_filebox_long_open_b/S2_off_prop_officehigh_filebox_long_open_b_001 (-306.547, -207.554, 75.0)
		16 units/payday2/props/off_prop_officehigh_filebox_long_open_b/S2_off_prop_officehigh_filebox_long_open_b_003 (158.0, -35.0, 74.4495)
		17 units/payday2/props/off_prop_officehigh_filebox_long_open_b/S2_off_prop_officehigh_filebox_long_open_b_004 (-69.9299, -257.667, 74.0)
		18 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/S2_off_prop_officehigh_filebox_short_whole_b_002 (-498.324, -43.1413, 139.0)
		19 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/S2_off_prop_officehigh_filebox_short_whole_b_003 (-175.0, -25.0001, 0.0)
		20 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/S2_off_prop_officehigh_filebox_short_whole_b_003 (80.0, -24.0001, 0.0)
		21 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/S2_off_prop_officehigh_filebox_short_whole_b_004 (-175.0, -25.0001, 25.0)
		22 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/S2_off_prop_officehigh_filebox_short_whole_b_005 (-216.0, -24.9999, 0.0)
		23 units/payday2/props/paperstacks/off_prop_officehigh_paper_stack_01/S2_off_prop_officehigh_paper_stack_01_002 (117.0, -36.0, 73.4495)
		24 units/payday2/props/paperstacks/off_prop_officehigh_paper_stack_02/S2_off_prop_officehigh_paper_stack_02_002 (-7.99988, -29.0, 0.0)
		25 units/world/props/office/chair/office_chair_boss/S2_office_chair_boss_001 (10.0, -176.0, 0.0)
		26 units/payday2/props/res_prop_livingroom_tv/S2_res_prop_livingroom_tv_001 (-103.0, -27.0, 140.486)
		27 units/pd2_dlc1/props/res_prop_motel_telephone/S2_res_prop_motel_telephone_001 (42.1223, -277.993, 74.3717)
		28 units/payday2/props/stn_prop_office_filecabinet_a_02/S2_stn_prop_office_filecabinet_a_02_001 (100.0, -69.0, 0.0)
		29 units/payday2/props/stn_prop_office_filecabinet_a_02/S2_stn_prop_office_filecabinet_a_02_002 (140.0, -69.0, 0.0)
		30 units/payday2/props/stn_prop_office_filecabinet_b_02/S2_stn_prop_office_filecabinet_b_02_001 (-512.0, -75.0, 0.0)
		31 units/payday2/props/stn_prop_office_filecabinet_b_02/S2_stn_prop_office_filecabinet_b_02_001 (-155.0, -67.0, 0.0)
		32 units/payday2/props/stn_prop_office_filecabinet_b_02/S2_stn_prop_office_filecabinet_b_02_002 (-566.0, -70.0, 0.0)
		33 units/payday2/props/stn_prop_office_filecabinet_b_02/S2_stn_prop_office_filecabinet_b_02_002 (-70.0, -68.0, 0.0)
		34 units/payday2/props/stn_prop_office_filecabinet_c_02/S2_stn_prop_office_filecabinet_c_02_001 (-109.0, -75.0, 0.0)
		35 units/payday2/props/stn_prop_office_paperswall_a/S2_stn_prop_office_paperswall_a_001 (-1.0, -1.99988, 100.0)
		36 units/payday2/props/stn_prop_office_paperswall_b/S2_stn_prop_office_paperswall_b_001 (-376.0, -1.99988, 75.0)
	on_executed
		´enable_crimenet_laptop_002´ (delay 1)
´s3´ ElementLoadDelayed 100052
	unit_ids
		1 units/world/props/bridge/construction_blueprint/blueprint/S3_blueprint_001 (-386.0, -189.0, 41.9997)
		2 units/pd2_dlc_chill/props/chl_prop_control_screen/S3_chl_prop_control_screen_001 (-175.0, 4.00009, 241.0)
		3 units/pd2_dlc_chill/props/chl_prop_dining_carpet/S3_chl_prop_dining_carpet_001 (-175.0, -211.0, -0.000278473)
		4 units/pd2_dlc_chill/props/chl_prop_livingroom_chair_lounge/S3_chl_prop_livingroom_chair_lounge_001 (-486.274, -111.98, -0.000278473)
		5 units/pd2_dlc_chill/props/chl_prop_livingroom_chair_lounge/S3_chl_prop_livingroom_chair_lounge_002 (-372.0, -316.0, -0.000278473)
		6 units/pd2_dlc_chill/props/chl_prop_livingroom_chair_lounge/S3_chl_prop_livingroom_chair_lounge_003 (-250.776, -197.102, 3.50414)
		7 units/pd2_dlc_chill/props/chl_prop_livingroom_lamp_floor/S3_chl_prop_livingroom_lamp_floor_001 (-442.0, -106.0, 0.504139)
		8 units/pd2_dlc_chill/props/chl_prop_livingroom_lamp_table_a/S3_chl_prop_livingroom_lamp_table_a_001 (168.0, -649.0, 79.6008)
		9 units/pd2_dlc_chill/props/chl_prop_livingroom_lamp_table_a/S3_chl_prop_livingroom_lamp_table_a_001 (-440.0, -29.0, 80.3357)
		10 units/pd2_dlc_chill/props/chl_prop_livingroom_sideboard_a/S3_chl_prop_livingroom_sideboard_a_001 (-324.0, -29.9999, -0.000278473)
		11 units/pd2_dlc_chill/props/chl_prop_livingroom_sideboard_a/S3_chl_prop_livingroom_sideboard_a_002 (-36.0, -29.9998, -0.000278473)
		12 units/pd2_dlc_chill/props/chl_prop_livingroom_sideboard_a/S3_chl_prop_livingroom_sideboard_a_003 (167.0, -575.0, -0.000301361)
		13 units/pd2_dlc_chill/props/chl_prop_mahogny_desk/S3_chl_prop_mahogny_desk_001 (28.3494, -262.5, 1.99972)
		14 units/pd2_dlc_chill/props/chl_prop_planingroom_dallas_chair_modern/S3_chl_prop_planingroom_dallas_chair_modern_001 (112.741, -189.914, -0.000278473)
		15 units/pd2_dlc_chill/props/chl_prop_planingroom_dallas_phone/S3_chl_prop_planingroom_dallas_phone_001 (74.3257, -336.77, 75.6074)
		16 units/pd2_dlc_chill/props/chl_veg_planingroom_dallas_plant_medium/S3_chl_veg_planingroom_dallas_plant_medium_001 (-542.0, -31.0, -0.000278473)
		17 units/pd2_dlc_chill/props/chl_veg_planingroom_dallas_plant_medium/S3_chl_veg_planingroom_dallas_plant_medium_002 (170.0, -402.0, -0.000301361)
		18 units/pd2_dlc_chill/props/chl_veg_planingroom_dallas_plant_small/S3_chl_veg_planingroom_dallas_plant_small_001 (77.0, -31.0, 80.6008)
		19 units/pd2_dlc_cro/props/gen_int_tablet/S3_gen_int_tablet_001 (-100.0, -25.0, 80.3357)
		20 units/payday2/equipment/gen_interactable_laptop_crimenet/S3_gen_interactable_laptop_crimenet_003 (167.0, -591.0, 80.0)
		21 units/payday2/props/gen_prop_glow_01_very_small/S3_gen_prop_glow_01_very_small_001 (-390.804, -184.835, 128.504)
		22 units/pd2_dlc2/architecture/gov_d_int_decal_mull_3x3m/S3_gov_d_int_decal_mull_3x3m_001 (-385.0, -197.0, -0.000278473)
		23 units/payday2/props/lxa_prop_balcony_table_large/S3_lxa_prop_balcony_table_large_001 (-385.0, -197.0, -0.000278473)
		24 units/payday2/props/off_prop_appliance_keyboard/S3_off_prop_appliance_keyboard_001 (18.5603, -206.337, 76.9997)
		25 units/payday2/props/off_prop_appliance_mouse/S3_off_prop_appliance_mouse_001 (21.6478, -166.641, 76.6074)
		26 units/payday2/props/off_prop_office_monitor/S3_off_prop_office_monitor_001 (-9.1506, -197.548, 76.9997)
		27 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/S3_off_prop_officehigh_filebox_short_whole_b_005 (174.0, -739.0, -0.000301361)
	on_executed
		´enable_crimenet_laptop_003´ (delay 1)
