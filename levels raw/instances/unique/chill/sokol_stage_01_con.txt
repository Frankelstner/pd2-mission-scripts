ID range vs continent name:
	100000: world

statics
	100072 units/lights/01/light_projection_02/S3_light_projection_02_004 (250.0, -925.0, 254.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.627451, 0.258824
			enabled True
			falloff_exponent 1
			far_range 200
			multiplier streetlight
			name ls_spot
			near_range 0
			spot_angle_end 65
			spot_angle_start 45.0819
		projection_light ls_spot
		ls_spot units/lights/spot_light_projection_textures/spotprojection_06_downlight_df
	100175 units/lights/01/light_projection_02/S3_light_projection_02_005 (250.0, -675.0, 254.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.627451, 0.258824
			enabled True
			falloff_exponent 1
			far_range 200
			multiplier streetlight
			name ls_spot
			near_range 0
			spot_angle_end 65
			spot_angle_start 45.0819
		projection_light ls_spot
		ls_spot units/lights/spot_light_projection_textures/spotprojection_06_downlight_df
	100176 units/lights/01/light_projection_02/S3_light_projection_02_006 (250.0, -550.0, 254.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.627451, 0.258824
			enabled True
			falloff_exponent 1
			far_range 200
			multiplier streetlight
			name ls_spot
			near_range 0
			spot_angle_end 65
			spot_angle_start 45.0819
		projection_light ls_spot
		ls_spot units/lights/spot_light_projection_textures/spotprojection_06_downlight_df
	100068 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S2_lxa_int_hockey_room_ice_2x2m_013 (271.0, -725.0, 0.0914001)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100065 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S2_lxa_int_hockey_room_ice_2x2m_019 (-200.0, -200.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100066 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S2_lxa_int_hockey_room_ice_2x2m_020 (0.0, -6.10352e-05, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100074 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S2_lxa_int_hockey_room_ice_2x2m_025 (200.0, -200.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100076 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S2_lxa_int_hockey_room_ice_2x2m_026 (0.0, -400.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100135 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S2_lxa_int_hockey_room_ice_2x2m_032 (26.8113, -838.809, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100136 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S2_lxa_int_hockey_room_ice_2x2m_033 (204.81, -618.999, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100059 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S2_lxa_int_hockey_room_ice_2x2m_034 (-192.999, -660.81, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100078 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S2_lxa_int_hockey_room_ice_2x2m_035 (-15.0, -441.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100043 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S3_lxa_int_hockey_room_ice_2x2m_001 (-300.0, -200.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100044 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S3_lxa_int_hockey_room_ice_2x2m_002 (-100.0, -200.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100045 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S3_lxa_int_hockey_room_ice_2x2m_003 (100.0, -200.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100046 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S3_lxa_int_hockey_room_ice_2x2m_004 (100.0, -400.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100048 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S3_lxa_int_hockey_room_ice_2x2m_005 (-100.0, -400.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100049 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S3_lxa_int_hockey_room_ice_2x2m_006 (-300.0, -400.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100050 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S3_lxa_int_hockey_room_ice_2x2m_007 (100.0, -600.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100051 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S3_lxa_int_hockey_room_ice_2x2m_008 (-100.0, -600.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100052 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S3_lxa_int_hockey_room_ice_2x2m_009 (-300.0, -600.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100053 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S3_lxa_int_hockey_room_ice_2x2m_010 (100.0, -800.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100054 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S3_lxa_int_hockey_room_ice_2x2m_011 (-100.0, -800.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100055 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S3_lxa_int_hockey_room_ice_2x2m_012 (-100.0, -800.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100057 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S3_lxa_int_hockey_room_ice_2x2m_014 (-100.0, -1000.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100058 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S3_lxa_int_hockey_room_ice_2x2m_015 (300.0, -800.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100056 units/payday2/architecture/lxa_int_hockey_room_ice_2x2m/S3_lxa_int_hockey_room_ice_2x2m_031 (100.0, -800.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100134 units/payday2/architecture/lxa_int_hockey_room_ice_lines/S3_lxa_int_hockey_room_ice_lines_001 (-300.0, -54.9999, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100199 units/payday2/props/gen_prop_glow_01_orange_v2/S3_gen_prop_glow_01_orange_v2_001 (213.0, -925.0, 306.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100200 units/payday2/props/gen_prop_glow_01_orange_v2/S3_gen_prop_glow_01_orange_v2_002 (213.0, -675.0, 306.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100201 units/payday2/props/gen_prop_glow_01_orange_v2/S3_gen_prop_glow_01_orange_v2_003 (213.0, -550.0, 306.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100191 units/payday2/props/lxa_prop_hockey_gear_helmet/S1_lxa_prop_hockey_gear_helmet_001 (251.0, -559.0, 137.343)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100082 units/payday2/props/lxa_prop_hockey_gear_helmet/S3_lxa_prop_hockey_gear_helmet_001 (264.0, -1034.0, 99.3348)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100186 units/payday2/props/lxa_prop_hockey_gear_puck/S1_lxa_prop_hockey_gear_puck_001 (225.0, -600.0, 107.976)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100187 units/payday2/props/lxa_prop_hockey_gear_puck/S1_lxa_prop_hockey_gear_puck_002 (209.0, -618.0, 107.976)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100188 units/payday2/props/lxa_prop_hockey_gear_puck/S1_lxa_prop_hockey_gear_puck_003 (232.0, -615.0, 107.976)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100025 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_001 (-128.0, -38.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100026 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_002 (-162.0, -39.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100027 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_003 (-130.0, -66.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100034 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_004 (-183.0, -47.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100146 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_005 (-191.0, -26.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100147 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_006 (-130.0, -157.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100148 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_007 (62.0, -92.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100149 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_008 (17.0, -100.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100150 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_009 (-22.0, -75.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100151 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_010 (-75.0, -95.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100152 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_011 (158.0, -32.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100153 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_012 (115.0, -33.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100154 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_013 (181.0, -121.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100155 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_014 (185.0, -165.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100156 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_015 (119.0, -121.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100157 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_016 (162.0, -84.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100158 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_017 (191.0, -30.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100159 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_018 (136.0, -25.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100160 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_019 (146.0, -63.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100161 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_020 (187.0, -63.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100162 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_021 (-99.0, -24.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100163 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_022 (-187.0, -74.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100164 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_023 (52.0, -688.0, 3.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100165 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_024 (34.0, -612.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100166 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_025 (-32.0, -608.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100167 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_026 (-44.0, -744.0, 3.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100168 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_027 (-112.0, -719.0, 3.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100169 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_028 (-112.0, -740.0, 3.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100170 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_029 (-95.0, -720.0, 3.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100171 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_030 (-126.0, -713.0, 3.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100172 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_031 (-108.0, -726.0, 3.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100173 units/payday2/props/lxa_prop_hockey_gear_puck/S2_lxa_prop_hockey_gear_puck_032 (-104.0, -712.0, 3.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100085 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_001 (-221.0, -46.9999, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100086 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_002 (-260.0, -72.9999, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100087 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_003 (-195.0, -118.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100094 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_004 (-239.0, -154.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100095 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_005 (-266.0, -131.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100096 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_006 (-235.0, -104.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100097 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_007 (237.0, -67.9999, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100098 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_008 (255.0, -133.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100099 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_009 (272.0, -184.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100101 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_011 (235.0, -163.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100102 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_012 (205.0, -88.9999, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100103 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_013 (-271.0, -213.0, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100104 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_014 (-11.0, -81.9999, 2.83234)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100105 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_015 (0.0, -118.0, 2.80206)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100106 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_016 (-35.0, -71.9999, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100107 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_017 (52.0, -84.9999, 3.42401)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100108 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_018 (-101.0, -173.0, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100109 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_019 (76.0, -45.9999, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100110 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_020 (192.0, -23.9999, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100111 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_021 (-186.0, -44.9999, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100112 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_022 (-171.0, -34.9999, 3.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100116 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_026 (130.0, -873.0, 4.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100118 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_028 (128.0, -896.0, 4.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100119 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_029 (135.0, -899.0, 4.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100120 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_030 (167.0, -898.0, 4.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100121 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_031 (127.0, -906.0, 4.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100122 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_032 (111.0, -913.0, 4.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100123 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_033 (143.0, -885.0, 4.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100124 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_034 (149.0, -872.0, 4.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100125 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_035 (124.0, -848.0, 4.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100126 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_036 (117.0, -883.0, 4.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100127 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_037 (121.0, -900.0, 4.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100128 units/payday2/props/lxa_prop_hockey_gear_puck/S3_lxa_prop_hockey_gear_puck_038 (166.0, -869.0, 4.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100189 units/payday2/props/lxa_prop_hockey_gear_skate/S1_lxa_prop_hockey_gear_skate_001 (208.0, -1121.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100190 units/payday2/props/lxa_prop_hockey_gear_skate/S1_lxa_prop_hockey_gear_skate_002 (227.0, -1059.0, 112.496)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100195 units/payday2/props/lxa_prop_hockey_gear_skate/S2_lxa_prop_hockey_gear_skate_003 (227.0, -1059.0, 112.496)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100196 units/payday2/props/lxa_prop_hockey_gear_skate/S2_lxa_prop_hockey_gear_skate_004 (208.0, -1121.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100137 units/payday2/props/lxa_prop_hockey_gear_skate/S3_lxa_prop_hockey_gear_skate_002 (259.0, -1028.0, 36.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100138 units/payday2/props/lxa_prop_hockey_gear_skate/S3_lxa_prop_hockey_gear_skate_003 (261.0, -1044.0, 36.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100139 units/payday2/props/lxa_prop_hockey_gear_skate/S3_lxa_prop_hockey_gear_skate_004 (259.0, -1067.0, 36.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100140 units/payday2/props/lxa_prop_hockey_gear_skate/S3_lxa_prop_hockey_gear_skate_005 (261.0, -1109.0, 48.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100184 units/payday2/props/lxa_prop_hockey_gear_stick/S1_lxa_prop_hockey_gear_stick_001 (219.0, -790.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100185 units/payday2/props/lxa_prop_hockey_gear_stick/S1_lxa_prop_hockey_gear_stick_002 (195.0, -441.0, 108.976)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100129 units/payday2/props/lxa_prop_hockey_gear_stick/S2_lxa_prop_hockey_gear_stick_001 (9.0, -723.0, 4.0)
		delayed_load True
		disable_collision True
		disable_shadows True
		hide_on_projection_light True
	100143 units/payday2/props/lxa_prop_hockey_gear_stick/S3_lxa_prop_hockey_gear_stick_007 (237.0, -1157.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100130 units/payday2/props/off_prop_generic_shelf_wire_b/S3_off_prop_generic_shelf_wire_b_001 (261.0, -1075.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100132 units/payday2/props/posters/com_prop_hockeyposter1/S1_com_prop_hockeyposter1_001 (150.0, -3.0, 89.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100204 units/payday2/props/posters/com_prop_hockeyposter1/S2_com_prop_hockeyposter1_002 (297.0, -951.604, 107.056)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100133 units/payday2/props/posters/com_prop_hockeyposter2/S1_com_prop_hockeyposter2_001 (-50.0, -3.0, 100.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100205 units/payday2/props/posters/com_prop_hockeyposter2/S2_com_prop_hockeyposter2_002 (297.0, -1051.43, 118.031)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100117 units/payday2/props/set/ind_prop_warehouse_box_d/S1_ind_prop_warehouse_box_d_001 (271.0, -1003.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100174 units/payday2/props/set/ind_prop_warehouse_box_d/S1_ind_prop_warehouse_box_d_002 (-258.0, -330.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100193 units/payday2/props/set/ind_prop_warehouse_box_d/S2_ind_prop_warehouse_box_d_003 (271.0, -1003.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100113 units/payday2/props/set/ind_prop_warehouse_box_e/S1_ind_prop_warehouse_box_e_001 (251.563, -547.752, 108.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100177 units/payday2/props/set/ind_prop_warehouse_box_e/S1_ind_prop_warehouse_box_e_002 (-274.0, -225.0, 107.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100114 units/payday2/props/set/ind_prop_warehouse_box_f/S1_ind_prop_warehouse_box_f_001 (271.428, -727.055, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100115 units/payday2/props/set/ind_prop_warehouse_box_f/S1_ind_prop_warehouse_box_f_002 (273.0, -729.0, 42.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100183 units/payday2/props/set/ind_prop_warehouse_box_f/S1_ind_prop_warehouse_box_f_003 (269.0, -1100.0, 108.496)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100194 units/payday2/props/set/ind_prop_warehouse_box_f/S2_ind_prop_warehouse_box_f_004 (269.0, -1100.0, 108.496)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100197 units/payday2/props/set/ind_prop_warehouse_box_f/S2_ind_prop_warehouse_box_f_005 (271.427, -487.055, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100198 units/payday2/props/set/ind_prop_warehouse_box_f/S2_ind_prop_warehouse_box_f_006 (273.0, -489.0, 42.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100100 units/payday2/props/set/ind_prop_warehouse_box_stack_4/S1_ind_prop_warehouse_box_stack_4_001 (260.0, -1095.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100192 units/payday2/props/set/ind_prop_warehouse_box_stack_4/S2_ind_prop_warehouse_box_stack_4_002 (260.0, -1095.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100028 units/payday2/props/set/ind_prop_warehouse_box_stack_8/S1_ind_prop_warehouse_box_stack_8_001 (246.0, -550.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100047 units/payday2/props/set/ind_prop_warehouse_box_stack_8/S1_ind_prop_warehouse_box_stack_8_002 (-246.0, -225.0, -1.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100039 units/payday2/props/spotlight/com_prop_store_spot_light_b_2/S3_com_prop_store_spot_light_b_2_001 (200.0, -925.0, 342.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100038 units/payday2/props/spotlight/com_prop_store_spot_light_b_2/S3_com_prop_store_spot_light_b_2_002 (200.0, -675.0, 342.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100063 units/payday2/props/spotlight/com_prop_store_spot_light_b_2/S3_com_prop_store_spot_light_b_2_003 (200.0, -550.0, 342.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100178 units/payday2/props/spotlight/com_prop_store_spot_rail_b/S3_com_prop_store_spot_rail_b_001 (200.0, -613.0, 349.304)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100180 units/payday2/props/spotlight/com_prop_store_spot_rail_b/S3_com_prop_store_spot_rail_b_002 (200.0, -988.0, 349.304)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100017 units/pd2_dlc_arena/props/guitar/are_prop_hallway_jersey/S3_are_prop_hallway_jersey_001 (300.0, -500.0, 229.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100018 units/pd2_dlc_arena/props/guitar/are_prop_hallway_jersey/S3_are_prop_hallway_jersey_002 (300.0, -625.0, 229.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
		mesh_variation plaque02
	100024 units/pd2_dlc_arena/props/guitar/are_prop_hallway_jersey/S3_are_prop_hallway_jersey_003 (300.0, -875.0, 229.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
		mesh_variation plaque03
	100083 units/pd2_dlc_cage/props/ind_prop_cardealership_waterbottle/S3_ind_prop_cardealership_waterbottle_001 (263.0, -1091.0, 160.335)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100144 units/pd2_dlc_cage/props/ind_prop_cardealership_waterbottle/S3_ind_prop_cardealership_waterbottle_002 (263.0, -1108.0, 160.335)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100145 units/pd2_dlc_cage/props/ind_prop_cardealership_waterbottle/S3_ind_prop_cardealership_waterbottle_003 (242.0, -1121.0, 160.335)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100010 units/pd2_dlc_chill/props/chl_prop_gym_sokol_hockey_goal_large/S2_chl_prop_gym_sokol_hockey_goal_large_001 (0.0, -100.0, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100042 units/pd2_dlc_chill/props/chl_prop_gym_sokol_hockey_goal_large/S3_chl_prop_gym_sokol_hockey_goal_large_002 (0.0, -59.9999, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100009 units/pd2_dlc_chill/props/chl_prop_gym_sokol_hockey_goal_small/S1_chl_prop_gym_sokol_hockey_goal_small_001 (0.0, -100.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100000 units/pd2_dlc_chill/props/chl_prop_gym_sokol_player_rails/001 (-1700.0, 0.000322819, 400.0)
		delayed_load True
	100022 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_a/S2_chl_prop_gym_sokol_wallpadding_a_005 (189.0, 0.0, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100023 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_a/S2_chl_prop_gym_sokol_wallpadding_a_006 (-261.0, 0.0, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100019 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_a/S3_chl_prop_gym_sokol_wallpadding_a_001 (300.0, -119.0, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100029 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_a/S3_chl_prop_gym_sokol_wallpadding_a_002 (-300.0, -324.0, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100020 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_a/S3_chl_prop_gym_sokol_wallpadding_a_003 (300.0, -282.0, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100041 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_a/S3_chl_prop_gym_sokol_wallpadding_a_004 (247.0, -70.0, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100203 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_a/S3_chl_prop_gym_sokol_wallpadding_a_006 (-275.0, -65.201, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100202 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_a/S3_chl_prop_gym_sokol_wallpadding_a_007 (245.0, -20.201, 4.00002)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100011 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_b/S2_chl_prop_gym_sokol_wallpadding_b_001 (-211.0, 0.0, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100012 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_b/S2_chl_prop_gym_sokol_wallpadding_b_002 (-111.0, 0.0, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100015 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_b/S2_chl_prop_gym_sokol_wallpadding_b_003 (-11.0, 0.0, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100016 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_b/S2_chl_prop_gym_sokol_wallpadding_b_004 (89.0, 0.0, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100035 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_b/S3_chl_prop_gym_sokol_wallpadding_b_006 (-300.0, -273.0, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100036 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_b/S3_chl_prop_gym_sokol_wallpadding_b_007 (-300.0, -173.0, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100061 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_b/S3_chl_prop_gym_sokol_wallpadding_b_009 (300.0, -169.0, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100030 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_b/S3_chl_prop_gym_sokol_wallpadding_b_017 (-262.0, 0.000427246, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100031 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_b/S3_chl_prop_gym_sokol_wallpadding_b_018 (-162.0, 0.000427246, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100032 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_b/S3_chl_prop_gym_sokol_wallpadding_b_019 (-62.0, 0.000427246, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100033 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_b/S3_chl_prop_gym_sokol_wallpadding_b_020 (38.0, 0.000427246, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100179 units/pd2_dlc_chill/props/chl_prop_gym_sokol_wallpadding_b/S3_chl_prop_gym_sokol_wallpadding_b_022 (138.0, 0.000457764, 4.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100037 units/world/props/office/fan_table/S3_fan_table_001 (261.0, -401.0, 254.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100040 units/world/props/office/fan_table/S3_fan_table_002 (-296.0, -398.0, 253.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100181 units/world/props/office/fan_table/S3_fan_table_003 (-296.0, -797.0, 253.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100182 units/world/props/office/fan_table/S3_fan_table_004 (261.0, -798.0, 254.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
