﻿´func_disable_unit_001´ ElementDisableUnit 100024
	position 300.0, -1500.0, 2.5
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/ind/level/ind_ext_decal_07/001 (300.0, -404.0, 1.0)
		2 units/payday2/architecture/ind/level/ind_ext_decal_08/001 (125.468, -245.88, 2.0)
´startup´ MissionScriptElement 100026
	EXECUTE ON STARTUP
	on_executed
		´delay´ (delay 1)
		´disable_all´ (delay 0)
´delay´ MissionScriptElement 100036
	on_executed
		´tier_1´ (delay 0)
		´tier_2´ (delay 0)
		´tier_3´ (delay 0)
´disable_all´ MissionScriptElement 100037
	on_executed
		´func_disable_unit_001´ (delay 0)
´tier_1´ ElementCustomSafehouseFilter 100038
	check_type equal
	position -200.0, -1500.0, 2.5
	room_id american
	room_tier 1
	rotation 0.0, 0.0, 0.0, -1.0
	tier_check current
	on_executed
		´func_instance_output_001´ (delay 0)
		´disable_car´ (delay 0)
´tier_2´ ElementCustomSafehouseFilter 100039
	check_type equal
	position -100.0, -1500.0, 2.5
	room_id american
	room_tier 2
	rotation 0.0, 0.0, 0.0, -1.0
	tier_check current
	on_executed
		´func_instance_output_002´ (delay 0)
		´disable_car´ (delay 0)
´tier_3´ ElementCustomSafehouseFilter 100040
	check_type equal
	position 0.0, -1500.0, 2.5
	room_id american
	room_tier 3
	rotation 0.0, 0.0, 0.0, -1.0
	tier_check current
	on_executed
		´func_instance_output_003´ (delay 0)
		´disable_car´ (delay 0)
´func_instance_output_001´ ElementInstanceOutput 100000
	event Room is tier 1
	on_executed
		´s1´ (delay 0)
´func_instance_output_002´ ElementInstanceOutput 100001
	event Room is tier 2
	on_executed
		´s2´ (delay 0)
´func_instance_output_003´ ElementInstanceOutput 100002
	event Room is tier 3
	on_executed
		´s3´ (delay 0)
´disable_car´ ElementUnitSequence 100006
	position 100.0, -1600.0, 2.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
´s1´ ElementLoadDelayed 100003
	unit_ids
		1 units/pd2_dlc_born/props/bor_prop_garage_tooltrolley/S1_bor_prop_garage_tooltrolley_001 (-60.0, -54.0, -2.0)
		2 units/pd2_dlc_chill/props/chl_prop_contruction_tool_hammer/S1_chl_prop_contruction_tool_hammer_001 (6.0, -29.0, 144.171)
		3 units/pd2_dlc_chill/props/chl_prop_contruction_tool_multimeter/S1_chl_prop_contruction_tool_multimeter_001 (112.0, -36.0, 90.9711)
		4 units/pd2_dlc_chill/props/chl_prop_contruction_tool_plier/S1_chl_prop_contruction_tool_plier_001 (75.0, -6.99999, 90.9711)
		5 units/pd2_dlc_chill/props/chl_prop_contruction_tool_wrench01/S1_chl_prop_contruction_tool_wrench01_001 (211.0, -82.0, 104.976)
		6 units/pd2_dlc_chill/props/chl_prop_garage_batterycharger/S1_chl_prop_garage_batterycharger_001 (168.0, -29.0, -2.3)
		7 units/pd2_dlc_chill/props/drawer/chl_prop_garage_cabinets_a_desk/S1_chl_prop_garage_cabinets_a_desk_002 (50.0, -25.0, -2.0)
		8 units/payday2/props/set/ind_prop_warehouse_box_d/S1_ind_prop_warehouse_box_d_003 (275.0, -35.0, 105.0)
		9 units/payday2/props/set/ind_prop_warehouse_box_e/S1_ind_prop_warehouse_box_e_001 (-194.0, -40.0, 106.0)
		10 units/payday2/props/set/ind_prop_warehouse_box_f/S1_ind_prop_warehouse_box_f_001 (-114.0, -30.0, 0.0)
		11 units/payday2/props/set/ind_prop_warehouse_box_g/S1_ind_prop_warehouse_box_g_001 (5.00002, -26.0, 91.0)
		12 units/payday2/props/set/ind_prop_warehouse_box_stack_4/S1_ind_prop_warehouse_box_stack_4_001 (-194.0, -40.0, 0.0)
		13 units/payday2/props/set/ind_prop_warehouse_box_stack_8/S1_ind_prop_warehouse_box_stack_8_001 (275.0, -53.0, -3.0)
		14 units/world/props/mop_bucket/S1_mop_bucket_001 (-171.0, -116.0, 0.0)
´s2´ ElementLoadDelayed 100005
	unit_ids
		1 units/pd2_dlc_born/props/bor_prop_garage_hydralicjack/S2_bor_prop_garage_hydralicjack_001 (173.0, -48.0, -2.3)
		2 units/pd2_dlc_born/props/bor_prop_garage_postlift/S2_bor_prop_garage_postlift_001 (196.0, -400.0, 0.0)
		3 units/pd2_dlc_chill/props/chl_prop_construction_tool_level/S2_chl_prop_construction_tool_level_001 (12.0, -33.0, 90.9711)
		4 units/pd2_dlc_chill/props/chl_prop_contruction_tool_electricalscrewdriver/S2_chl_prop_contruction_tool_electricalscrewdriver_001 (116.0, -28.0, 90.9711)
		5 units/pd2_dlc_chill/props/chl_prop_contruction_tool_multimeter/S2_chl_prop_contruction_tool_multimeter_002 (49.0, -21.0, 90.9711)
		6 units/pd2_dlc_chill/props/chl_prop_contruction_tool_wrench01/S2_chl_prop_contruction_tool_wrench01_002 (59.0, -0.999998, 117.157)
		7 units/pd2_dlc_chill/props/chl_prop_contruction_tool_wrench01/S2_chl_prop_contruction_tool_wrench01_003 (69.0, -0.999998, 114.157)
		8 units/pd2_dlc_chill/props/chl_prop_contruction_tool_wrench01/S2_chl_prop_contruction_tool_wrench01_004 (79.0, -0.999996, 117.157)
		9 units/pd2_dlc_chill/props/wrench01/chl_prop_contruction_tool_wrench02/S2_chl_prop_contruction_tool_wrench02_001 (102.0, -0.999998, 117.157)
		10 units/pd2_dlc_chill/props/wrench01/chl_prop_contruction_tool_wrench02/S2_chl_prop_contruction_tool_wrench02_002 (116.0, -1.0, 117.157)
		11 units/pd2_dlc_chill/props/chl_prop_garage_batterycharger/S2_chl_prop_garage_batterycharger_002 (235.0, -50.0, 0.0)
		12 units/pd2_dlc_chill/props/drawer/chl_prop_garage_cabinets_a_cabinet_left/S2_chl_prop_garage_cabinets_a_cabinet_left_003 (9.0, -17.0, -2.0)
		13 units/pd2_dlc_chill/props/drawer/chl_prop_garage_cabinets_a_cabinet_left/S2_chl_prop_garage_cabinets_a_cabinet_left_004 (91.0, -17.0, -2.0)
		14 units/pd2_dlc_chill/props/drawer/chl_prop_garage_cabinets_a_cupboard/S2_chl_prop_garage_cabinets_a_cupboard_001 (225.0, -19.0, -2.0)
		15 units/pd2_dlc_chill/props/drawer/chl_prop_garage_cabinets_a_desk/S2_chl_prop_garage_cabinets_a_desk_001 (50.0, -25.0, -2.0)
		16 units/pd2_dlc_chill/props/drawer/chl_prop_garage_cabinets_a_pegboard/S2_chl_prop_garage_cabinets_a_pegboard_002 (50.0, 0.0, -2.0)
		17 units/pd2_dlc_chill/props/chl_prop_workshop_wolf_toolkit_bottom/S2_chl_prop_workshop_wolf_toolkit_bottom_001 (-10.0, 0.0, 119.0)
		18 units/pd2_dlc_chill/props/chl_prop_workshop_wolf_toolkit_top/S2_chl_prop_workshop_wolf_toolkit_top_001 (29.0, 0.0, 119.0)
		19 units/payday2/props/gen_prop_long_lamp_v3/S2_gen_prop_long_lamp_v3_003 (50.0, -4.0, 142.0)
		20 units/pd2_dlc_shoutout_raid/props/gen_prop_tire_stack_5/S2_gen_prop_tire_stack_5_001 (-147.0, -44.0, -2.3)
		21 units/payday2/props/set/ind_prop_warehouse_box_d/S2_ind_prop_warehouse_box_d_002 (310.0, -25.0, 0.0)
		22 units/payday2/props/set/ind_prop_warehouse_box_f/S2_ind_prop_warehouse_box_f_002 (310.0, -25.0, 54.0)
		23 units/world/props/mop_bucket/S2_mop_bucket_001 (-67.0, -33.0, -2.3)
´s3´ ElementLoadDelayed 100025
	unit_ids
		1 units/pd2_dlc_born/props/bor_prop_garage_hydralicjack/S3_bor_prop_garage_hydralicjack_002 (125.0, -16.0, 0.0)
		2 units/pd2_dlc_born/props/bor_prop_garage_postlift/S3_bor_prop_garage_postlift_002 (196.0, -400.0, 0.0)
		3 units/pd2_dlc_chill/props/chl_prop_construction_tool_saw/S3_chl_prop_construction_tool_saw_001 (277.0, -41.0, 89.2838)
		4 units/pd2_dlc_chill/props/chl_prop_contruction_tool_electricalscrewdriver/S3_chl_prop_contruction_tool_electricalscrewdriver_001 (70.2629, -52.7461, 93.6725)
		5 units/pd2_dlc_chill/props/chl_prop_contruction_tool_hammer/S3_chl_prop_contruction_tool_hammer_001 (21.0, -15.0, 128.076)
		6 units/pd2_dlc_chill/props/chl_prop_contruction_tool_multimeter/S3_chl_prop_contruction_tool_multimeter_001 (-67.6065, -32.3934, 87.2838)
		7 units/pd2_dlc_chill/props/chl_prop_contruction_tool_plier/S3_chl_prop_contruction_tool_plier_001 (-77.0, -18.0, 128.043)
		8 units/pd2_dlc_chill/props/chl_prop_contruction_tool_wrench01/S3_chl_prop_contruction_tool_wrench01_001 (55.0, -27.0, 88.2838)
		9 units/pd2_dlc_chill/props/wrench01/chl_prop_contruction_tool_wrench02/S3_chl_prop_contruction_tool_wrench02_001 (280.0, -84.0, 82.46)
		10 units/pd2_dlc_chill/props/chl_prop_garage_batterycharger/S3_chl_prop_garage_batterycharger_003 (219.011, -31.8781, -2.2135)
		11 units/pd2_dlc_chill/props/cabinets_b/chl_prop_garage_cabinet_b_pegboard/S3_chl_prop_garage_cabinet_b_pegboard_001 (-1.00001, -0.999996, 125.0)
		12 units/pd2_dlc_chill/props/drawer/chl_prop_garage_cabinets_a_cabinet_left/S3_chl_prop_garage_cabinets_a_cabinet_left_001 (270.0, -18.0, 0.0)
		13 units/pd2_dlc_chill/props/drawer/chl_prop_garage_cabinets_a_cabinet_left/S3_chl_prop_garage_cabinets_a_cabinet_left_002 (188.0, -18.0, 0.0)
		14 units/pd2_dlc_chill/props/chl_prop_garage_cabinets_b_counter/S3_chl_prop_garage_cabinets_b_counter_002 (-7.62939e-06, -39.0, 83.0)
		15 units/pd2_dlc_chill/props/chl_prop_garage_cabinets_b_counter/S3_chl_prop_garage_cabinets_b_counter_003 (208.0, -39.0, 83.0)
		16 units/pd2_dlc_chill/props/chl_prop_garage_cabinets_b_cupboard/S3_chl_prop_garage_cabinets_b_cupboard_002 (-153.0, -40.0, 0.0)
		17 units/pd2_dlc_chill/props/chl_prop_garage_cabinets_b_drawer/S3_chl_prop_garage_cabinets_b_drawer_002 (272.0, -52.0, 0.0)
		18 units/pd2_dlc_chill/props/chl_prop_garage_cabinets_b_drawers/S3_chl_prop_garage_cabinets_b_drawers_002 (-25.0, -45.0, 0.0)
		19 units/pd2_dlc_chill/props/chl_prop_garage_cabinets_b_hutch/S3_chl_prop_garage_cabinets_b_hutch_002 (-1.00001, -15.0, 125.0)
		20 units/pd2_dlc_chill/props/chl_prop_workshop_wolf_toolkit_bottom/S3_chl_prop_workshop_wolf_toolkit_bottom_001 (64.0, -0.999996, 150.0)
		21 units/pd2_dlc_chill/props/chl_prop_workshop_wolf_toolkit_bottom/S3_chl_prop_workshop_wolf_toolkit_bottom_002 (-66.0, -0.999996, 180.0)
		22 units/pd2_dlc_chill/props/chl_prop_workshop_wolf_toolkit_top/S3_chl_prop_workshop_wolf_toolkit_top_001 (0.999985, -0.999994, 150.0)
		23 units/pd2_dlc_chill/props/chl_prop_workshop_wolf_toolkit_top/S3_chl_prop_workshop_wolf_toolkit_top_002 (0.999985, -0.999994, 187.0)
		24 units/payday2/props/gen_prop_long_lamp_v3/S3_gen_prop_long_lamp_v3_001 (192.0, -3.0, 143.522)
		25 units/payday2/props/gen_prop_long_lamp_v3/S3_gen_prop_long_lamp_v3_002 (266.0, -3.0, 143.522)
		26 units/pd2_dlc_cage/props/book_set/ind_prop_cardealership_magazine_c/S3_ind_prop_cardealership_magazine_c_001 (171.0, -41.0, 88.2838)
		27 units/world/props/mop_bucket/S3_mop_bucket_002 (330.0, -44.0, 0.0)
