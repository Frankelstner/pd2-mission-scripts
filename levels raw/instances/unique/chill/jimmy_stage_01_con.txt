ID range vs continent name:
	100000: world

statics
	100164 units/dev_tools/level_tools/dev_bag_collision_1x1m/001 (-225.0, -175.0, 348.134)
	100172 units/dev_tools/level_tools/dev_bag_collision_1x1m/002 (-225.0, -75.0, 348.134)
	100173 units/dev_tools/level_tools/dev_bag_collision_1x1m/003 (269.0, -75.0, 348.134)
	100174 units/dev_tools/level_tools/dev_bag_collision_1x1m/004 (269.0, -175.0, 348.134)
	100148 units/dev_tools/level_tools/dev_bag_collision_1x3m/001 (-6.0, -175.0, 348.134)
	100163 units/dev_tools/level_tools/dev_bag_collision_1x3m/002 (-225.0, -175.0, 348.134)
	100160 units/payday2/pickups/gen_pku_cocaine/S3gen_pku_cocaine_001 (314.0, -165.0, -1.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
		mesh_variation disable_interaction
	100042 units/payday2/pickups/gen_pku_cocaine/S3gen_pku_cocaine_002 (343.0, -165.0, -1.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
		mesh_variation disable_interaction
	100161 units/payday2/pickups/gen_pku_cocaine/S3gen_pku_cocaine_003 (320.565, -164.293, 20.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
		mesh_variation disable_interaction
	100072 units/payday2/props/bottles/com_prop_club_bottle_1/S2_com_prop_club_bottle_1_001 (21.0, -11.0, 163.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100074 units/payday2/props/bottles/com_prop_club_bottle_1/S2_com_prop_club_bottle_1_002 (9.0, -11.0, 163.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100076 units/payday2/props/bottles/com_prop_club_bottle_1/S2_com_prop_club_bottle_1_003 (33.0, -11.0, 163.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100123 units/payday2/props/bottles/com_prop_club_bottle_1/S3_com_prop_club_bottle_1_004 (-8.0, -11.9922, 103.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100124 units/payday2/props/bottles/com_prop_club_bottle_1/S3_com_prop_club_bottle_1_005 (9.0, -11.9922, 103.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100125 units/payday2/props/bottles/com_prop_club_bottle_1/S3_com_prop_club_bottle_1_006 (23.0, -11.9922, 103.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100126 units/payday2/props/bottles/com_prop_club_bottle_1/S3_com_prop_club_bottle_1_007 (38.0, -11.9922, 103.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100049 units/payday2/props/bottles/com_prop_club_bottle_2_b_stack/S2_com_prop_club_bottle_2_b_stack_001 (70.0, -160.0, 38.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100050 units/payday2/props/bottles/com_prop_club_bottle_2_b_stack/S2_com_prop_club_bottle_2_b_stack_002 (-47.0, -155.0, 38.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100058 units/payday2/props/bottles/com_prop_club_bottle_2_b_stack/S2_com_prop_club_bottle_2_b_stack_003 (71.0, -11.0, 104.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100070 units/payday2/props/bottles/com_prop_club_bottle_2_b_stack/S2_com_prop_club_bottle_2_b_stack_004 (-31.0, -11.0, 163.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100097 units/payday2/props/bottles/com_prop_club_bottle_2_b_stack/S3_com_prop_club_bottle_2_b_stack_005 (85.0, -152.992, 38.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100143 units/payday2/props/bottles/com_prop_club_bottle_2_b_stack/S3_com_prop_club_bottle_2_b_stack_006 (-57.0, -7.99219, 162.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100100 units/payday2/props/bottles/com_prop_club_bottle_2_stack/S3_com_prop_club_bottle_2_stack_001 (-67.0, -162.992, 38.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100135 units/payday2/props/bottles/com_prop_club_bottle_2_stack/S3_com_prop_club_bottle_2_stack_002 (147.0, -9.99219, 163.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100066 units/payday2/props/bottles/com_prop_club_bottle_3/S2_com_prop_club_bottle_3_001 (-41.0, -11.0, 104.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100067 units/payday2/props/bottles/com_prop_club_bottle_3/S2_com_prop_club_bottle_3_002 (-55.0, -11.0, 104.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100068 units/payday2/props/bottles/com_prop_club_bottle_3/S2_com_prop_club_bottle_3_003 (-69.0, -11.0, 104.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100141 units/payday2/props/bottles/com_prop_club_bottle_3/S3_com_prop_club_bottle_3_004 (4.0, -7.99219, 162.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100142 units/payday2/props/bottles/com_prop_club_bottle_3/S3_com_prop_club_bottle_3_005 (-12.0, -7.99219, 162.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100138 units/payday2/props/bottles/com_prop_club_bottle_5/S3_com_prop_club_bottle_5_001 (49.0, -7.99219, 161.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100139 units/payday2/props/bottles/com_prop_club_bottle_5/S3_com_prop_club_bottle_5_002 (36.0, -7.99219, 161.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100140 units/payday2/props/bottles/com_prop_club_bottle_5/S3_com_prop_club_bottle_5_003 (23.0, -7.99219, 161.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100059 units/payday2/props/bottles/com_prop_club_bottle_6/S2_com_prop_club_bottle_6_001 (19.0, -11.0, 104.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100061 units/payday2/props/bottles/com_prop_club_bottle_6/S2_com_prop_club_bottle_6_002 (6.0, -11.0, 104.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100063 units/payday2/props/bottles/com_prop_club_bottle_6/S2_com_prop_club_bottle_6_003 (-14.0, -11.0, 104.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100065 units/payday2/props/bottles/com_prop_club_bottle_6/S2_com_prop_club_bottle_6_004 (-27.0, -11.0, 104.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100136 units/payday2/props/bottles/com_prop_club_bottle_8/S3_com_prop_club_bottle_8_001 (85.0, -6.99219, 162.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100137 units/payday2/props/bottles/com_prop_club_bottle_8/S3_com_prop_club_bottle_8_002 (70.0, -7.99219, 162.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100083 units/payday2/props/bottles/com_prop_club_bottle_9/S2_com_prop_club_bottle_9_001 (77.73, -8.17188, 163.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100084 units/payday2/props/bottles/com_prop_club_bottle_9/S2_com_prop_club_bottle_9_002 (91.8198, -7.63574, 163.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100154 units/payday2/props/bottles/com_prop_club_bottle_9/S3_com_prop_club_bottle_9_001 (-115.0, -185.998, 104.384)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100155 units/payday2/props/bottles/com_prop_club_bottle_9/S3_com_prop_club_bottle_9_002 (172.0, -174.998, 104.384)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100156 units/payday2/props/bottles/com_prop_club_bottle_9/S3_com_prop_club_bottle_9_003 (247.0, -188.998, 104.384)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100127 units/payday2/props/bottles/com_prop_club_bottle_9/S3_com_prop_club_bottle_9_003 (57.0, -11.9922, 103.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100128 units/payday2/props/bottles/com_prop_club_bottle_9/S3_com_prop_club_bottle_9_004 (74.0, -16.9922, 103.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100129 units/payday2/props/bottles/com_prop_club_bottle_9/S3_com_prop_club_bottle_9_005 (92.0, -11.9922, 103.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100056 units/payday2/props/com_prop_club_beer_keg/S2_com_prop_club_beer_keg_001 (-119.0, -22.0, 0.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100104 units/payday2/props/com_prop_club_beer_keg/S3_com_prop_club_beer_keg_002 (-112.96, -25.0005, -1.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100105 units/payday2/props/com_prop_club_beer_keg/S3_com_prop_club_beer_keg_003 (-61.9331, -26.1103, -1.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100106 units/payday2/props/com_prop_club_beer_keg/S3_com_prop_club_beer_keg_004 (-19.1821, -26.6953, -1.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100052 units/payday2/props/com_prop_club_beer_tap/S2_com_prop_club_beer_tap_001 (-48.0, -184.0, 105.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100107 units/payday2/props/com_prop_club_beer_tap/S3_com_prop_club_beer_tap_002 (202.8, -187.992, 104.995)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100110 units/payday2/props/com_prop_election_chair/S1_com_prop_election_chair_001 (125.0, -266.998, -1.86635)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100111 units/payday2/props/com_prop_election_chair_folded/S1_com_prop_election_chair_folded_001 (-89.9997, -255.998, -1.86635)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100114 units/payday2/props/set/ind_prop_warehouse_box_d/S1_ind_prop_warehouse_box_d_001 (-302.0, -36.9986, 105.134)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100147 units/payday2/props/set/ind_prop_warehouse_box_f/S1_ind_prop_warehouse_box_f_001 (-311.0, -90.9985, 105.134)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100113 units/payday2/props/set/ind_prop_warehouse_box_stack_4/S1_ind_prop_warehouse_box_stack_4_001 (374.0, -39.9986, -2.86635)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100112 units/payday2/props/set/ind_prop_warehouse_box_stack_8/S1_ind_prop_warehouse_box_stack_8_001 (-302.0, -85.9985, -2.86635)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100026 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S1_res_prop_store_beer_carton_a_001 (-140.19, -32.8766, 22.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100027 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S1_res_prop_store_beer_carton_a_002 (-137.75, -6.98656, -2.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100029 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S1_res_prop_store_beer_carton_a_003 (-93.0597, -30.9088, -2.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100031 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S1_res_prop_store_beer_carton_a_004 (-134.99, -188.824, 22.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100032 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S1_res_prop_store_beer_carton_a_005 (-135.0, -188.998, -2.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100033 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S1_res_prop_store_beer_carton_a_006 (-165.0, -188.998, -2.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100034 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S1_res_prop_store_beer_carton_a_007 (-137.649, -198.521, -2.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100053 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S2_res_prop_store_beer_carton_a_008 (-145.39, -57.751, 105.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100054 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S2_res_prop_store_beer_carton_a_009 (-150.0, -4.0, 105.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100099 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_010 (30.0, -184.992, 39.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100101 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_011 (183.0, -145.992, 38.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100102 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_012 (-165.0, -93.9922, 39.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100103 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_013 (-165.0, -48.9922, 39.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100025 units/pd2_dlc2/architecture/gov_d_int_floor_rubber_2x2m/S2_gov_d_int_floor_rubber_2x2m_001 (224.0, 0.0, 1.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100057 units/pd2_dlc2/architecture/gov_d_int_floor_rubber_2x2m/S2_gov_d_int_floor_rubber_2x2m_002 (24.0, 0.0, 1.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100145 units/pd2_dlc2/architecture/gov_d_int_floor_rubber_2x2m/S3_gov_d_int_floor_rubber_2x2m_003 (37.0, 25.0078, 1.13354)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100146 units/pd2_dlc2/architecture/gov_d_int_floor_rubber_2x2m/S3_gov_d_int_floor_rubber_2x2m_004 (237.0, 25.0078, 1.13354)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100085 units/pd2_dlc_chill/architecture/chl_int_bar_v2/S2_chl_int_bar_v2_001 (-386.0, 12.9999, -1.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100144 units/pd2_dlc_chill/architecture/v2/chl_int_bar_v3/S3_chl_int_bar_v3_001 (-388.0, 13.0078, -1.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100037 units/pd2_dlc_chill/props/chl_prop_jimmy_bar_simple_center/S1_chl_prop_jimmy_bar_simple_center_001 (-24.9999, -199.998, -2.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100038 units/pd2_dlc_chill/props/chl_prop_jimmy_bar_simple_center/S1_chl_prop_jimmy_bar_simple_center_002 (25.0002, -199.998, -2.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100035 units/pd2_dlc_chill/props/chl_prop_jimmy_bar_simple_corner/S1_chl_prop_jimmy_bar_simple_corner_001 (-95.0002, -194.998, -2.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100039 units/pd2_dlc_chill/props/chl_prop_jimmy_bar_simple_corner/S1_chl_prop_jimmy_bar_simple_corner_002 (95.0001, -194.998, -2.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100036 units/pd2_dlc_chill/props/chl_prop_jimmy_bar_simple_end/S1_chl_prop_jimmy_bar_simple_end_001 (-99.9994, -133.998, -2.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100040 units/pd2_dlc_chill/props/chl_prop_jimmy_bar_simple_end/S1_chl_prop_jimmy_bar_simple_end_002 (100.0, -133.998, -2.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100045 units/pd2_dlc_chill/props/chl_prop_jimmy_barstool_v2/S1_chl_prop_jimmy_barstool_v2_002 (16.0, -282.998, -2.1336)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100086 units/pd2_dlc_chill/props/chl_prop_jimmy_barstool_v2/S2_chl_prop_jimmy_barstool_v2_004 (165.0, -234.0, -0.866402)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100087 units/pd2_dlc_chill/props/chl_prop_jimmy_barstool_v2/S2_chl_prop_jimmy_barstool_v2_005 (52.9502, -254.896, -0.866402)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100094 units/pd2_dlc_chill/props/chl_prop_jimmy_barstool_v2/S2_chl_prop_jimmy_barstool_v2_006 (-59.7598, -232.932, -0.866402)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100095 units/pd2_dlc_chill/props/chl_prop_jimmy_barstool_v2/S2_chl_prop_jimmy_barstool_v2_007 (-167.61, -234.731, -0.866402)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100096 units/pd2_dlc_chill/props/chl_prop_jimmy_barstool_v2/S2_chl_prop_jimmy_barstool_v2_008 (-242.27, -152.088, -0.866394)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100044 units/pd2_dlc_chill/props/chl_prop_neon_sign_bar/S2_chl_prop_neon_sign_bar_002 (192.0, -4.0, 148.134)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100046 units/pd2_dlc_chill/props/chl_prop_neon_sign_bar/S3_chl_prop_neon_sign_bar_003 (253.0, -3.99805, 148.134)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100010 units/pd2_dlc_chill/props/chl_prop_wine_cardboard_box_open/S1_chl_prop_wine_cardboard_box_open_002 (-210.0, -37.9987, -2.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100055 units/pd2_dlc_chill/props/chl_prop_wine_cardboard_box_open/S1_chl_prop_wine_cardboard_box_open_004 (158.0, -137.285, -2.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100009 units/pd2_dlc_chill/props/chl_prop_wine_cardboard_box_open/S2_chl_prop_wine_cardboard_box_open_001 (-46.0, -26.0, -1.0001)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100048 units/pd2_dlc_chill/props/open/chl_prop_wine_cardboard_box/S1_chl_prop_wine_cardboard_box_001 (-198.0, -95.9986, -2.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100030 units/pd2_dlc_chill/props/open/chl_prop_wine_cardboard_box/S1_chl_prop_wine_cardboard_box_002 (164.29, -193.285, -2.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100011 units/pd2_dlc_chill/props/open/chl_prop_wine_cardboard_box/S1_chl_prop_wine_cardboard_box_003 (164.22, -189.285, 40.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100109 units/pd2_dlc_chill/vegetation/chl_veg_indoor_plant_table/S3_chl_veg_indoor_plant_table_001 (-185.2, -80.9922, 103.995)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100149 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_stool/S3_jry_prop_diner_stool_002 (-157.0, -244.992, 1.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100150 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_stool/S3_jry_prop_diner_stool_003 (-57.0, -244.992, 1.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100151 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_stool/S3_jry_prop_diner_stool_004 (43.0, -252.992, 1.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100152 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_stool/S3_jry_prop_diner_stool_005 (143.0, -244.992, 1.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100153 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_stool/S3_jry_prop_diner_stool_006 (243.0, -244.992, 1.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100167 units/world/props/apartment/cocaine/cokebag/s2cokebag_004 (198.0, -128.0, 104.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100168 units/world/props/apartment/cocaine/cokebag/s2cokebag_005 (198.0, -164.0, 104.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100159 units/world/props/apartment/cocaine/cokebag/s3cokebag_001 (271.0, -147.0, 104.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100165 units/world/props/apartment/cocaine/cokebag/s3cokebag_002 (271.0, -147.0, 114.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100166 units/world/props/apartment/cocaine/cokebag/s3cokebag_003 (273.0, -117.0, 104.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100169 units/world/props/apartment/cocaine/cokebag/s3cokebag_006 (314.0, -115.0, 1.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100170 units/world/props/apartment/cocaine/cokebag/s3cokebag_007 (314.0, -115.0, 11.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100171 units/world/props/apartment/cocaine/cokebag/s3cokebag_008 (346.0, -119.0, 1.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100158 units/world/props/apartment/cocaine/cokebag_depleted/s3cokebag_depleted_001 (156.021, -159.808, 105.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100157 units/world/props/apartment/cocaine/cokebag_ripped/s2cokebag_ripped_001 (118.0, -170.0, 106.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100162 units/world/props/apartment/cocaine/cokebag_ripped/s3cokebag_ripped_002 (124.021, -171.808, 105.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100041 units/world/props/apartment/messed_towels/apartment_messed_towel_02/S1_apartment_messed_towel_02_001 (51.0005, -176.998, 108.186)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100051 units/world/props/apartment/messed_towels/apartment_messed_towel_02/S2_apartment_messed_towel_02_002 (150.0, -151.0, 104.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100098 units/world/props/apartment/messed_towels/apartment_messed_towel_02/S3_apartment_messed_towel_02_003 (104.0, -157.992, 67.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100108 units/world/props/apartment/messed_towels/apartment_messed_towel_02/S3_apartment_messed_towel_02_004 (242.0, -120.992, 104.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100019 units/world/props/bottle_4/S1_bottle_4_001 (104.0, -164.998, 108.186)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100020 units/world/props/bottle_4/S1_bottle_4_002 (96.0006, -172.998, 108.186)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100022 units/world/props/bottle_6/S1_bottle_6_001 (-115.798, -149.341, 108.384)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100023 units/world/props/bottle_6/S1_bottle_6_002 (-196.0, -101.998, 40.561)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100024 units/world/props/bottle_6/S1_bottle_6_003 (-204.0, -92.9987, 40.561)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100043 units/world/props/bottle_6/S1_bottle_6_004 (-95.9996, -171.998, 108.384)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100120 units/world/props/bottle_6/S3_bottle_6_005 (-56.0, -11.9922, 103.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100121 units/world/props/bottle_6/S3_bottle_6_006 (-42.0, -11.9922, 103.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100122 units/world/props/bottle_6/S3_bottle_6_007 (-26.0, -11.9922, 103.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100130 units/world/props/bottle_7/S3_bottle_7_001 (113.0, -20.9922, 103.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100131 units/world/props/bottle_7/S3_bottle_7_002 (128.0, -22.9922, 103.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100012 units/world/props/bottle_8/S1_bottle_8_001 (155.291, -174.285, 82.3848)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100015 units/world/props/bottle_8/S1_bottle_8_002 (155.29, -183.285, 82.3848)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100016 units/world/props/bottle_8/S1_bottle_8_003 (157.412, -192.164, 82.3848)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100017 units/world/props/bottle_8/S1_bottle_8_004 (155.29, -204.285, 82.3848)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100018 units/world/props/bottle_8/S1_bottle_8_005 (107.0, -140.998, 111.384)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100021 units/world/props/bottle_8/S1_bottle_8_006 (86.0004, -172.998, 108.384)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100115 units/world/props/bottle_8/S3_bottle_8_007 (-72.5601, -183.285, 109.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100116 units/world/props/bottle_8/S3_bottle_8_008 (-94.0, -177.992, 104.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100117 units/world/props/bottle_8/S3_bottle_8_009 (-94.0, -13.9922, 103.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100118 units/world/props/bottle_8/S3_bottle_8_010 (-82.0, -16.9922, 103.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100119 units/world/props/bottle_8/S3_bottle_8_011 (-71.0, -10.9922, 103.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100132 units/world/props/bottle_8/S3_bottle_8_012 (144.0, -22.9922, 103.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100133 units/world/props/bottle_8/S3_bottle_8_013 (156.0, -15.9922, 103.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100134 units/world/props/bottle_8/S3_bottle_8_014 (167.0, -22.9922, 103.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100078 units/world/props/mansion/winebottles/man_prop_int_winebottle_02/S2_man_prop_int_winebottle_02_001 (46.0, -11.0, 163.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100080 units/world/props/mansion/winebottles/man_prop_int_winebottle_02/S2_man_prop_int_winebottle_02_002 (56.0, -11.0, 163.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
	100082 units/world/props/mansion/winebottles/man_prop_int_winebottle_02/S2_man_prop_int_winebottle_02_003 (65.0, -11.0, 163.0)
		delayed_load True
		disable_shadows True
		hide_on_projection_light True
