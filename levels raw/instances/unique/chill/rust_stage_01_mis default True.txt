﻿´startup´ MissionScriptElement 100000
	EXECUTE ON STARTUP
	on_executed
		´delay´ (delay 1)
´delay´ MissionScriptElement 100001
	on_executed
		´tier_1´ (delay 0)
		´tier_2´ (delay 0)
		´tier_3´ (delay 0)
´tier_1´ ElementCustomSafehouseFilter 100004
	check_type equal
	position -100.0, -1500.0, 2.5
	room_id wild
	room_tier 1
	rotation 0.0, 0.0, 0.0, -1.0
	tier_check current
	on_executed
		´output001´ (delay 0)
´tier_2´ ElementCustomSafehouseFilter 100006
	check_type equal
	position 0.0, -1500.0, 2.5
	room_id wild
	room_tier 2
	rotation 0.0, 0.0, 0.0, -1.0
	tier_check current
	on_executed
		´output002´ (delay 0)
´tier_3´ ElementCustomSafehouseFilter 100009
	check_type equal
	position 100.0, -1500.0, 2.5
	room_id wild
	room_tier 3
	rotation 0.0, 0.0, 0.0, -1.0
	tier_check current
	on_executed
		´output003´ (delay 0)
´output001´ ElementInstanceOutput 100010
	event Room is tier 1
	on_executed
		´s1´ (delay 0)
´output002´ ElementInstanceOutput 100011
	event Room is tier 2
	on_executed
		´s2´ (delay 0)
´output003´ ElementInstanceOutput 100012
	event Room is tier 3
	on_executed
		´s3´ (delay 0)
´s1´ ElementLoadDelayed 100007
	unit_ids
		1 units/pd2_dlc_born/props/bor_prop_garage_bikepart_exhaustpipe/S1_bor_prop_garage_bikepart_exhaustpipe_001 (-42.8613, -52.7747, 114.0)
		2 units/pd2_dlc_born/props/bor_prop_garage_bikepart_frame/S1_bor_prop_garage_bikepart_frame_001 (149.0, -550.0, 106.0)
		3 units/pd2_dlc_born/props/bor_prop_garage_rollingseat/S1_bor_prop_garage_rollingseat_001 (73.052, -157.69, 1.0)
		4 units/pd2_dlc_chill/props/chl_prop_garage_toolbox_base/S1_chl_prop_garage_toolbox_base_005 (-11.0, -50.0001, 0.0)
		5 units/payday2/props/com_prop_election_chair/S1_com_prop_election_chair_001 (-105.686, -157.314, 0.0)
		6 units/payday2/props/set/ind_prop_warehouse_box_d/S1_ind_prop_warehouse_box_d_001 (-217.0, -300.0, 104.0)
		7 units/payday2/props/set/ind_prop_warehouse_box_d/S1_ind_prop_warehouse_box_d_002 (49.0012, -53.909, 114.0)
		8 units/payday2/props/set/ind_prop_warehouse_box_f/S1_ind_prop_warehouse_box_f_001 (192.0, -605.0, -2.3)
		9 units/payday2/props/set/ind_prop_warehouse_box_f/S1_ind_prop_warehouse_box_f_002 (-220.0, -407.0, -2.3)
		10 units/payday2/props/set/ind_prop_warehouse_box_stack_4/S1_ind_prop_warehouse_box_stack_4_001 (175.0, -82.0, -3.0)
		11 units/payday2/props/set/ind_prop_warehouse_box_stack_4/S1_ind_prop_warehouse_box_stack_4_002 (-202.001, -152.001, 0.0)
		12 units/payday2/props/set/ind_prop_warehouse_box_stack_8/S1_ind_prop_warehouse_box_stack_8_001 (525.0, -90.0, -3.0)
		13 units/payday2/props/set/ind_prop_warehouse_box_stack_8/S1_ind_prop_warehouse_box_stack_8_003 (160.0, -525.0, -3.0)
		14 units/payday2/props/set/ind_prop_warehouse_box_stack_8/S1_ind_prop_warehouse_box_stack_8_004 (-194.0, -300.0, -3.0)
´s2´ ElementLoadDelayed 100008
	unit_ids
		1 units/pd2_dlc_born/props/bor_prop_garage_bike_assembly/parts/bor_prop_garage_assembly_part_engine/S2_bor_prop_garage_assembly_part_engine_001 (175.0, -522.0, 138.0)
		2 units/pd2_dlc_born/props/bor_prop_garage_bike_assembly/parts/bor_prop_garage_assembly_part_fueltank/S2_bor_prop_garage_assembly_part_fueltank_001 (114.0, -553.0, 116.0)
		3 units/pd2_dlc_born/props/bor_prop_garage_bikepart_cylinderhead/S2_bor_prop_garage_bikepart_cylinderhead_001 (253.0, -495.0, 113.0)
		4 units/pd2_dlc_born/props/bor_prop_garage_bikepart_exhaustpipe/S2_bor_prop_garage_bikepart_exhaustpipe_002 (-42.86, -52.7746, 126.0)
		5 units/pd2_dlc_born/props/bor_prop_garage_bikepart_exhaustpipe/S2_bor_prop_garage_bikepart_exhaustpipe_002 (31.26, -70.7464, 122.0)
		6 units/pd2_dlc_born/props/bor_prop_garage_bikepart_handlebar/S2_bor_prop_garage_bikepart_handlebar_001 (-203.81, -284.253, 82.0)
		7 units/pd2_dlc_born/props/bor_prop_garage_bikepart_seat/S2_bor_prop_garage_bikepart_seat_001 (-180.41, -275.413, 115.0)
		8 units/pd2_dlc_born/props/bor_prop_garage_bikepart_wheel/S2_bor_prop_garage_bikepart_wheel_001 (-14.48, -70.2148, 121.0)
		9 units/pd2_dlc_born/props/bor_prop_garage_bikepart_wheel/S2_bor_prop_garage_bikepart_wheel_002 (-68.87, -57.2835, 121.0)
		10 units/pd2_dlc_born/props/bor_prop_garage_tooltrolley/S2_bor_prop_garage_tooltrolley_002 (186.56, -65.4748, -3.0)
		11 units/pd2_dlc_born/props/bor_prop_garage_welder/S2_bor_prop_garage_welder_001 (308.0, -74.9999, -2.3)
		12 units/pd2_dlc_born/props/posters/bor_prop_poster_custom_rider/S2_bor_prop_poster_custom_rider_001 (32.59, -2.41382, 193.0)
		13 units/pd2_dlc_born/props/posters/bor_prop_poster_death_riders/S2_bor_prop_poster_death_riders_001 (-83.4101, -2.41333, 157.0)
		14 units/world/props/bottle_2/S2_bottle_2_001 (235.0, -484.0, 112.6)
		15 units/world/props/bottle_2/S2_bottle_2_002 (261.0, -537.0, 112.6)
		16 units/world/props/bottle_2/S2_bottle_2_003 (206.0, -549.0, 112.6)
		17 units/world/props/bottle_2/S2_bottle_2_004 (309.0, -72.9998, 87.7412)
		18 units/world/props/bottle_2/S2_bottle_2_005 (71.0, -83.9999, 114.6)
		19 units/world/props/bottle_2/S2_bottle_2_006 (70.0, -49.0, 114.6)
		20 units/world/props/bottle_2/S2_bottle_2_007 (46.0, -20.0, 139.6)
		21 units/world/props/bottle_2/S2_bottle_2_008 (13.0, -34.9999, 114.6)
		22 units/world/props/bottle_2/S2_bottle_2_009 (42.0, -49.0, 114.6)
		23 units/world/props/bottle_2/S2_bottle_2_010 (-223.0, -386.0, 45.7)
		24 units/world/props/bottle_2/S2_bottle_2_011 (-47.0, -112.0, -1.3)
		25 units/world/props/bottle_2/S2_bottle_2_012 (-66.0, -110.0, -1.3)
		26 units/world/props/bottle_2/S2_bottle_2_013 (-127.0, -225.0, 1.7)
		27 units/world/props/bottle_2/S2_bottle_2_014 (-127.0, -163.0, -1.3)
		28 units/world/props/bottle_3/S2_bottle_3_001 (-175.0, -225.0, 114.6)
		29 units/world/props/bottle_3/S2_bottle_3_001 (109.0, -492.0, 113.0)
		30 units/world/props/bottle_3/S2_bottle_3_002 (-131.0, -243.0, -2.3)
		31 units/world/props/bottle_3/S2_bottle_3_003 (287.0, -82.9998, 87.7412)
		32 units/pd2_dlc_chill/props/chl_prop_garage_toolbox_base/S2_chl_prop_garage_toolbox_base_002 (175.0, -522.0, -2.0)
		33 units/pd2_dlc_chill/props/chl_prop_garage_toolbox_base/S2_chl_prop_garage_toolbox_base_003 (-191.0, -277.001, 0.0)
		34 units/pd2_dlc_chill/props/chl_prop_garage_toolbox_base/S2_chl_prop_garage_toolbox_base_004 (-11.0, -50.0, 0.0)
		35 units/pd2_dlc_chill/props/chl_prop_garage_toolbox_hutch/S2_chl_prop_garage_toolbox_hutch_002 (-191.0, -276.0, 116.0)
		36 units/payday2/props/com_prop_election_chair/S2_com_prop_election_chair_002 (-105.686, -157.314, 0.0)
		37 units/payday2/props/set/ind_prop_warehouse_box_stack_8/S2_ind_prop_warehouse_box_stack_8_002 (525.0, -89.0, -3.0)
		38 units/payday2/props/mcm_prop_basement_bottles/S2_mcm_prop_basement_bottles_001 (-192.0, -341.001, 115.0)
		39 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S2_res_prop_store_beer_carton_a_001 (-239.44, -214.386, 201.457)
		40 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S2_res_prop_store_beer_carton_a_002 (-210.0, -259.0, 201.457)
		41 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S2_res_prop_store_beer_carton_a_003 (-212.49, -235.788, 225.457)
		42 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S2_res_prop_store_beer_carton_a_004 (-131.71, -159.433, 0.0)
		43 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S2_res_prop_store_beer_carton_a_005 (-238.05, -382.696, -2.3)
		44 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S2_res_prop_store_beer_carton_a_006 (-206.03, -410.513, -2.3)
		45 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S2_res_prop_store_beer_carton_a_007 (-235.0, -406.0, 21.7)
		46 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S2_res_prop_store_beer_carton_a_008 (27.0499, -30.673, 115.6)
		47 units/pd2_dlc1/props/res_prop_store_beer_cutout/S2_res_prop_store_beer_cutout_002 (-234.37, -378.0, 112.828)
		48 units/payday2/props/stn_prop_medic_stool/S2_stn_prop_medic_stool_001 (125.55, -115.444, 13.0)
		49 units/world/props/tools_screwdriver/S2_tools_screwdriver_006 (214.0, -503.0, 113.6)
		50 units/world/props/tools_screwdriver/S2_tools_screwdriver_007 (-159.0, -248.0, 115.6)
´s3´ ElementLoadDelayed 100060
	unit_ids
		1 units/pd2_dlc_born/props/bor_prop_garage_bike_assembly/parts/bor_prop_garage_assembly_part_engine/S3_bor_prop_garage_assembly_part_engine_002 (354.047, -82.2633, 59.0)
		2 units/pd2_dlc_born/props/bor_prop_garage_bikepart_cylinderhead/S3_bor_prop_garage_bikepart_cylinderhead_001 (550.0, -84.0, 100.199)
		3 units/pd2_dlc_born/props/bor_prop_garage_bikepart_exhaustpipe/S3_bor_prop_garage_bikepart_exhaustpipe_001 (108.0, -517.0, -2.3)
		4 units/pd2_dlc_born/props/bor_prop_garage_bikepart_exhaustpipe/S3_bor_prop_garage_bikepart_exhaustpipe_003 (-203.704, -217.756, 109.0)
		5 units/pd2_dlc_born/props/bor_prop_garage_bikepart_exhaustpipe/S3_bor_prop_garage_bikepart_exhaustpipe_003 (-209.859, -293.775, 113.0)
		6 units/pd2_dlc_born/props/bor_prop_garage_bikepart_fueltank/S3_bor_prop_garage_bikepart_fueltank_002 (-94.592, -42.4216, 102.0)
		7 units/pd2_dlc_born/props/bor_prop_garage_bikepart_handlebar/S3_bor_prop_garage_bikepart_handlebar_002 (23.4883, -50.9105, 101.0)
		8 units/pd2_dlc_born/props/bor_prop_garage_bikepart_seat/S3_bor_prop_garage_bikepart_seat_001 (201.0, -545.0, 101.766)
		9 units/pd2_dlc_born/props/bor_prop_garage_bikepart_wheel/S3_bor_prop_garage_bikepart_wheel_001 (300.0, -543.0, -0.300003)
		10 units/pd2_dlc_born/props/bor_prop_garage_bikepart_wheel/S3_bor_prop_garage_bikepart_wheel_003 (-201.337, -318.759, 108.0)
		11 units/pd2_dlc_born/props/bor_prop_garage_bikepart_wheel/S3_bor_prop_garage_bikepart_wheel_004 (-197.074, -263.016, 108.0)
		12 units/pd2_dlc_born/props/bor_prop_garage_enginestand/S3_bor_prop_garage_enginestand_002 (350.56, -80.274, -3.0)
		13 units/pd2_dlc_born/props/bor_prop_garage_tooltrolley/S3_bor_prop_garage_tooltrolley_001 (210.0, -600.0, -2.3)
		14 units/pd2_dlc_born/props/bor_prop_garage_welder/S3_bor_prop_garage_welder_002 (181.0, -82.0, -3.0)
		15 units/pd2_dlc_born/props/posters/bor_prop_poster_custom_rider/S3_bor_prop_poster_custom_rider_002 (32.5899, -2.41362, 193.0)
		16 units/pd2_dlc_born/props/posters/bor_prop_poster_death_riders/S3_bor_prop_poster_death_riders_002 (-83.4099, -2.41338, 157.0)
		17 units/pd2_dlc_born/props/posters/bor_prop_poster_fearless_skull/S3_bor_prop_poster_fearless_skull_001 (-160.0, -50.0, 172.7)
		18 units/world/props/bottle_2/S3_bottle_2_015 (173.0, -555.0, 102.766)
		19 units/world/props/bottle_2/S3_bottle_2_016 (135.0, -564.0, 126.766)
		20 units/world/props/bottle_2/S3_bottle_2_017 (86.0, -514.0, 101.766)
		21 units/world/props/bottle_2/S3_bottle_2_018 (432.0, -89.0, 101.199)
		22 units/world/props/bottle_2/S3_bottle_2_019 (482.0, -62.0, 101.199)
		23 units/world/props/bottle_2/S3_bottle_2_020 (409.0, -78.0, 101.199)
		24 units/world/props/bottle_2/S3_bottle_2_021 (413.0, -96.0, 104.199)
		25 units/world/props/bottle_3/S3_bottle_3_004 (-133.0, -104.003, 0.0)
		26 units/pd2_dlc_chill/props/drawer/chl_prop_garage_cabinets_a_cabinet_right/S3_chl_prop_garage_cabinets_a_cabinet_right_001 (-226.0, -350.0, -2.3)
		27 units/pd2_dlc_chill/props/drawer/chl_prop_garage_cabinets_a_cabinet_right/S3_chl_prop_garage_cabinets_a_cabinet_right_002 (-226.0, -268.0, -2.3)
		28 units/pd2_dlc_chill/props/drawer/chl_prop_garage_cabinets_a_cabinet_right/S3_chl_prop_garage_cabinets_a_cabinet_right_003 (-226.0, -186.0, -2.3)
		29 units/pd2_dlc_chill/props/chl_prop_garage_toolbox_chrome_a/S3_chl_prop_garage_toolbox_chrome_a_005 (500.0, -82.0, -3.0)
		30 units/pd2_dlc_chill/props/chl_prop_garage_toolbox_chrome_a/S3_chl_prop_garage_toolbox_chrome_a_008 (-203.0, -277.001, -2.0)
		31 units/pd2_dlc_chill/props/chl_prop_garage_toolbox_chrome_b/S3_chl_prop_garage_toolbox_chrome_b_001 (-26.0, -44.0001, -2.0)
		32 units/pd2_dlc_chill/props/chl_prop_garage_toolbox_chrome_b/S3_chl_prop_garage_toolbox_chrome_b_002 (175.0, -533.0, -2.0)
		33 units/payday2/props/com_prop_election_chair/S3_com_prop_election_chair_003 (-105.686, -157.314, 0.0)
		34 units/payday2/props/mcm_prop_basement_bottles/S3_mcm_prop_basement_bottles_001 (-23.0, -29.0, 101.766)
		35 units/world/props/oil_stains_decal_02/S3_oil_stains_decal_02_001 (183.0, -299.0, 0.0)
		36 units/world/props/oil_stains_decal_02/S3_oil_stains_decal_02_002 (251.0, -448.0, 0.0)
		37 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_009 (-243.0, -312.0, 202.289)
		38 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_010 (-243.0, -318.0, 202.289)
		39 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_011 (-243.0, -162.0, 202.289)
		40 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_012 (-235.0, -205.0, 202.289)
		41 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_013 (-243.0, -184.0, 226.289)
		42 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_014 (-243.0, -162.0, -2.3)
		43 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_015 (-237.0, -133.0, -2.3)
		44 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_016 (-195.0, -103.0, -2.3)
		45 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_017 (-181.0, -103.0, 21.7)
		46 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_018 (-199.0, -137.0, 21.7)
		47 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_019 (-225.0, -153.0, 45.7)
		48 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_020 (-229.0, -381.0, -2.3)
		49 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_021 (-240.0, -381.0, 21.7)
		50 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_022 (82.0, -567.0, 102.766)
		51 units/pd2_dlc1/props/res_prop_store_beer_carton_a/S3_res_prop_store_beer_carton_a_023 (120.0, -567.0, 102.766)
		52 units/pd2_dlc1/props/res_prop_store_beer_cutout/S3_res_prop_store_beer_cutout_003 (167.0, -573.0, 123.0)
		53 units/payday2/props/stn_prop_medic_stool/S3_stn_prop_medic_stool_001 (60.0, -103.0, -2.3)
		54 units/world/props/tools_screwdriver/S3_tools_screwdriver_001 (524.0, -89.0, 101.199)
		55 units/world/props/tools_screwdriver/S3_tools_screwdriver_002 (581.0, -83.0, 101.199)
		56 units/world/props/tools_screwdriver/S3_tools_screwdriver_003 (-56.0, -54.0, 102.766)
		57 units/world/props/tools_screwdriver/S3_tools_screwdriver_004 (-195.0, -249.0, 102.199)
		58 units/world/props/tools_screwdriver/S3_tools_screwdriver_005 (172.0, -522.0, 102.766)
