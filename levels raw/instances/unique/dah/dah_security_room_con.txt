ID range vs continent name:
	100000: world

statics
	100038 core/units/light_omni/001 (-225.0, -225.0, 171.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.286275, 0.388235, 0.486275
			enabled True
			falloff_exponent 1
			far_range 270
			multiplier identity
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100083 core/units/light_omni/002 (-749.0, -37.0, 237.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.984314, 0.937255, 0.666667
			enabled True
			falloff_exponent 1
			far_range 60
			multiplier sun
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100039 core/units/light_spot/001 (-188.0, -213.0, 182.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.172549, 0.211765, 0.270588
			enabled True
			falloff_exponent 1
			far_range 530
			multiplier identity
			name ls_spot
			near_range 80
			spot_angle_end 56
			spot_angle_start 43
	100155 units/dev_tools/level_tools/dev_bag_collision_1x3m/001 (-62.0, -190.0, -3.9998)
	100156 units/dev_tools/level_tools/dev_bag_collision_1x3m/002 (-62.0, -290.0, -3.9998)
	100157 units/dev_tools/level_tools/dev_bag_collision_1x3m/003 (-62.0, -390.0, -3.9998)
	100159 units/dev_tools/level_tools/dev_bag_collision_1x3m/004 (-62.0, -369.0, -3.9998)
	100160 units/dev_tools/level_tools/dev_bag_collision_1x3m/005 (-162.0, -369.0, -3.9998)
	100161 units/dev_tools/level_tools/dev_bag_collision_1x3m/006 (-262.0, -369.0, -3.9998)
	100162 units/dev_tools/level_tools/dev_bag_collision_1x3m/007 (-348.0, -369.0, -3.9998)
	100085 units/dev_tools/level_tools/dev_bag_collision_4x3m/001 (4.05312e-05, -413.0, 297.0)
	100148 units/dev_tools/level_tools/dev_bag_collision_4x3m/002 (-300.0, -413.0, 296.999)
	100149 units/dev_tools/level_tools/dev_bag_collision_4x3m/003 (-600.0, -413.0, 296.999)
	100150 units/dev_tools/level_tools/dev_bag_collision_4x3m/004 (-771.0, -413.0, 296.998)
	100151 units/dev_tools/level_tools/dev_bag_collision_4x3m/005 (-771.0, -83.0, 296.998)
	100152 units/dev_tools/level_tools/dev_bag_collision_4x3m/006 (-300.0, -83.0, 296.999)
	100153 units/dev_tools/level_tools/dev_bag_collision_4x3m/007 (-600.0, -83.0, 296.999)
	100154 units/dev_tools/level_tools/dev_bag_collision_4x3m/008 (-6.10352e-05, -83.0, 297.0)
	100036 units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-96.0, 151.0, 0.0)
	100170 units/dev_tools/level_tools/dev_nav_blocker_1x3m/001 (-1076.0, 59.0, -1.0)
	100171 units/dev_tools/level_tools/dev_nav_blocker_1x3m/002 (-1076.0, 248.0, -1.0)
	100172 units/dev_tools/level_tools/dev_nav_blocker_1x3m/003 (-25.0, 248.0, -1.0)
	100007 units/dev_tools/level_tools/dev_nav_blocker_1x3m/005 (-25.0, -41.0, -1.0)
	100042 units/dev_tools/level_tools/dev_nav_blocker_1x3m/006 (-25.0, -141.0, -1.0)
	100144 units/dev_tools/level_tools/dev_nav_blocker_1x3m/007 (-25.0, 45.0, -1.0)
	100168 units/dev_tools/level_tools/dev_nav_blocker_2x3m/001 (-1100.0, -366.0, 14.6034)
	100169 units/dev_tools/level_tools/dev_nav_blocker_2x3m/002 (-1100.0, -324.0, 14.6034)
	100174 units/dev_tools/level_tools/dev_nav_blocker_2x3m/003 (-24.0, -366.0, 14.6034)
	100175 units/dev_tools/level_tools/dev_nav_blocker_2x3m/004 (-24.0, -324.0, 14.6034)
	100166 units/dev_tools/level_tools/dev_nav_blocker_4x3m/001 (-442.0, -376.0, 0.0)
	100167 units/dev_tools/level_tools/dev_nav_blocker_4x3m/002 (-442.0, -120.0, 0.0)
	100037 units/lights/light_omni_shadow_projection_01/001 (-187.0, -317.0, 183.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.321569, 0.462745, 0.694118
			enabled True
			falloff_exponent 1
			far_range 600
			multiplier match
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
		projection_light lo_omni
	100052 units/lights/light_omni_shadow_projection_01/004 (-751.0, -37.0, 222.974)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.976471, 0.819608, 0.666667
			enabled True
			falloff_exponent 1
			far_range 500
			multiplier match
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
		projection_light lo_omni
	100022 units/payday2/equipment/gen_interactable_door_reinforced/001 (-22.0003, 255.0, 0.0)
		disable_on_ai_graph True
	100090 units/payday2/props/air_prop_office_binder_black/001 (-469.0, 91.0, 8.0)
	100091 units/payday2/props/air_prop_office_binder_black/002 (-468.0, 84.0, 8.0)
	100093 units/payday2/props/air_prop_office_binder_black/004 (-470.0, 70.0, 8.0)
	100095 units/payday2/props/air_prop_office_binder_black/006 (-468.0, 56.0, 8.0)
	100096 units/payday2/props/air_prop_office_binder_black/007 (-468.0, 84.0, 43.0)
	100098 units/payday2/props/air_prop_office_binder_black/009 (-469.0, 77.0, 43.0)
	100100 units/payday2/props/air_prop_office_binder_black/011 (-469.0, 63.0, 43.0)
	100101 units/payday2/props/air_prop_office_binder_black/012 (-468.0, 56.0, 43.0)
	100102 units/payday2/props/air_prop_office_binder_black/013 (-468.0, 84.0, 91.0)
	100103 units/payday2/props/air_prop_office_binder_black/014 (-469.0, 91.0, 91.0)
	100105 units/payday2/props/air_prop_office_binder_black/016 (-470.0, 70.0, 91.0)
	100107 units/payday2/props/air_prop_office_binder_black/018 (-468.0, 56.0, 91.0)
	100108 units/payday2/props/air_prop_office_binder_black/019 (-468.0, 84.0, 137.0)
	100110 units/payday2/props/air_prop_office_binder_black/021 (-469.0, 77.0, 137.0)
	100112 units/payday2/props/air_prop_office_binder_black/023 (-469.0, 63.0, 137.0)
	100113 units/payday2/props/air_prop_office_binder_black/024 (-468.0, 56.0, 137.0)
	100114 units/payday2/props/air_prop_office_binder_black/025 (-468.0, -166.0, 137.0)
	100116 units/payday2/props/air_prop_office_binder_black/027 (-469.0, -173.0, 137.0)
	100120 units/payday2/props/air_prop_office_binder_black/029 (-469.0, -187.0, 137.0)
	100121 units/payday2/props/air_prop_office_binder_black/030 (-468.0, -194.0, 137.0)
	100122 units/payday2/props/air_prop_office_binder_black/031 (-468.0, -166.0, 92.0)
	100123 units/payday2/props/air_prop_office_binder_black/032 (-469.0, -159.0, 92.0)
	100125 units/payday2/props/air_prop_office_binder_black/034 (-470.0, -180.0, 92.0)
	100127 units/payday2/props/air_prop_office_binder_black/036 (-468.0, -194.0, 92.0)
	100128 units/payday2/props/air_prop_office_binder_black/037 (-468.0, -166.0, 43.0)
	100130 units/payday2/props/air_prop_office_binder_black/039 (-469.0, -173.0, 43.0)
	100132 units/payday2/props/air_prop_office_binder_black/041 (-469.0, -187.0, 43.0)
	100133 units/payday2/props/air_prop_office_binder_black/042 (-468.0, -194.0, 43.0)
	100134 units/payday2/props/air_prop_office_binder_black/043 (-468.0, -166.0, 8.0)
	100136 units/payday2/props/air_prop_office_binder_black/045 (-469.0, -173.0, 8.0)
	100137 units/payday2/props/air_prop_office_binder_black/046 (-470.0, -180.0, 8.0)
	100139 units/payday2/props/air_prop_office_binder_black/048 (-468.0, -194.0, 8.0)
	100140 units/payday2/props/air_prop_office_binder_blue/001 (-469.0, -159.0, 43.0)
	100129 units/payday2/props/air_prop_office_binder_blue/002 (-469.0, -187.0, 92.0)
	100126 units/payday2/props/air_prop_office_binder_blue/003 (-469.0, -173.0, 92.0)
	100124 units/payday2/props/air_prop_office_binder_blue/004 (-469.0, -159.0, 137.0)
	100115 units/payday2/props/air_prop_office_binder_blue/005 (-470.0, -180.0, 137.0)
	100119 units/payday2/props/air_prop_office_binder_blue/006 (-470.0, 70.0, 137.0)
	100111 units/payday2/props/air_prop_office_binder_blue/007 (-469.0, 77.0, 91.0)
	100104 units/payday2/props/air_prop_office_binder_blue/008 (-470.0, 70.0, 43.0)
	100099 units/payday2/props/air_prop_office_binder_blue/009 (-469.0, 77.0, 8.0)
	100092 units/payday2/props/air_prop_office_binder_blue/010 (-469.0, -187.0, 8.0)
	100064 units/payday2/props/air_prop_office_binder_pack_1/001 (-410.0, -37.0002, 98.0293)
	100065 units/payday2/props/air_prop_office_binder_pack_2/001 (-402.0, -25.0, 138.123)
	100138 units/payday2/props/air_prop_office_binder_white/001 (-468.511, 63.0043, 91.0)
	100106 units/payday2/props/air_prop_office_binder_white/002 (-469.0, 91.0, 43.0)
	100097 units/payday2/props/air_prop_office_binder_white/003 (-468.511, 63.0043, 8.0)
	100094 units/payday2/props/air_prop_office_binder_white/004 (-465.27, -179.976, 43.0)
	100131 units/payday2/props/air_prop_office_binder_white/005 (-464.637, -158.962, 8.0)
	100135 units/payday2/props/air_prop_office_binder_white/006 (-469.0, 91.0, 137.0)
	100066 units/payday2/props/books/off_prop_officehigh_binder_01/001 (-388.0, 111.0, 138.123)
	100067 units/payday2/props/books/off_prop_officehigh_binder_02/001 (-275.0, 200.0, 158.277)
	100068 units/payday2/props/books/off_prop_officehigh_binder_02/002 (-275.0, 200.0, 158.277)
	100069 units/payday2/props/books/off_prop_officehigh_binder_02/003 (-391.0, 86.0, 98.1235)
	100082 units/payday2/props/gen_prop_glow_garage_lamp_dim/001 (-745.0, -37.0, 238.0)
	100087 units/payday2/props/off_prop_officehigh_bulletinboard_02/001 (-26.0, 14.0, 152.0)
	100002 units/payday2/props/off_prop_officehigh_bulletinboard_long_02/001 (-419.0, -174.0, 145.0)
	100109 units/payday2/props/off_prop_officehigh_bulletinboard_long_03/001 (-750.0, 311.0, 168.0)
	100063 units/payday2/props/off_prop_officehigh_chair_standard_black/001 (-255.837, -251.835, 0.0721741)
	100061 units/payday2/props/off_prop_officehigh_chair_standard_black/002 (-754.881, -138.173, 0.0721703)
	100079 units/payday2/props/off_prop_officehigh_chair_standard_black/003 (-754.881, 66.827, 0.0721703)
	100080 units/payday2/props/off_prop_officehigh_chair_standard_black/004 (-657.881, -40.173, 0.0721741)
	100081 units/payday2/props/off_prop_officehigh_chair_standard_black/005 (-858.881, -40.173, 0.0721741)
	100019 units/payday2/props/off_prop_officehigh_desk_dark_straight_10/001 (-414.998, -300.192, 0.0)
	100145 units/payday2/props/off_prop_officehigh_desk_zebra_straight_15/002 (-750.0, 38.0, 0.0)
	100076 units/payday2/props/off_prop_officehigh_desk_zebra_straight_15/003 (-675.0, 38.0, 0.0)
	100000 units/payday2/props/off_prop_officehigh_drawer_zebra_large_glass_door/001 (-436.0, -1.0, 1.0)
	100088 units/payday2/props/off_prop_officehigh_drawer_zebra_large_glass_door_left/001 (-436.0, -201.0, 1.0)
	100089 units/payday2/props/off_prop_officehigh_drawer_zebra_large_glass_doors_2/001 (-436.0, -101.0, 1.0)
	100141 units/payday2/props/off_prop_officehigh_drawer_zebra_small_drawers_5/001 (-600.0, 310.0, 0.0)
	100142 units/payday2/props/off_prop_officehigh_drawer_zebra_small_drawers_5/002 (-700.0, 310.0, 0.0)
	100077 units/payday2/props/off_prop_officehigh_drawer_zebra_small_drawers_5/004 (-800.0, 310.0, 0.0)
	100001 units/payday2/props/servers/gen_prop_security_server_hightech/001 (-387.0, 187.0, 0.0)
	100009 units/payday2/props/servers/gen_prop_security_server_hightech/002 (-321.0, 187.0, 0.0)
	100017 units/payday2/props/servers/gen_prop_security_server_hightech/003 (-254.0, 187.0, 0.0)
	100008 units/payday2/props/shelves/off_prop_officehigh_shelf_wenge/001 (-418.0, 23.9998, 93.0293)
	100011 units/payday2/props/shelves/off_prop_officehigh_shelf_wenge/002 (-418.0, 23.9998, 133.029)
	100049 units/payday2/props/signs/gen_prop_security_sign_1/001 (3.0, 118.0, 125.0)
	100020 units/pd2_dlc2/props/console/gen_prop_security_monitor_hanging_01/001 (-179.0, -368.0, 297.0)
	100003 units/pd2_dlc_casino/props/cas_prop_wine_cardboard_box_open/001 (-389.881, -106.625, 0.174248)
	100006 units/pd2_dlc_casino/props/int_security_table_props/cas_prop_office_monitor_group_02/001 (-34.0, -167.0, 298.0)
	100072 units/pd2_dlc_chill/props/chl_prop_garage_cabinets_b_drawer/001 (-379.0, -12.0, 0.0)
	100073 units/pd2_dlc_chill/props/chl_prop_garage_cabinets_b_drawer/002 (-379.0, 62.0, 0.0)
	100015 units/pd2_dlc_chill/props/chl_prop_kitchen_lamp_ceiling/001 (-267.0, -213.0, 214.927)
		mesh_variation state_unlit
	100014 units/pd2_dlc_chill/props/chl_prop_kitchen_lamp_ceiling/002 (-267.0, 103.0, 214.927)
		mesh_variation state_unlit
	100040 units/pd2_dlc_chill/props/chl_prop_kitchen_lamp_ceiling/003 (-748.0, -37.0, 236.927)
		mesh_variation state_lit
	100024 units/pd2_dlc_dah/architecture/instances/dah_secutity_room_01/001 (0.0, 325.0, 0.0)
	100004 units/pd2_dlc_dah/props/dah_prop_airconditioning_c/001 (-220.0, 264.0, 247.897)
	100012 units/pd2_dlc_dah/props/dah_prop_box_plastic/001 (-73.0, -11.0, -0.318054)
	100013 units/pd2_dlc_dah/props/dah_prop_box_plastic/002 (-394.605, -200.749, -0.017643)
	100070 units/pd2_dlc_dah/props/dah_prop_box_plastic/003 (-399.379, -173.168, -0.017643)
	100071 units/pd2_dlc_dah/props/dah_prop_box_plastic/004 (-397.673, -187.064, 23.9824)
	100023 units/pd2_dlc_dah/props/dah_prop_ceiling_vent/001 (-271.0, -108.0, 291.956)
	100078 units/pd2_dlc_dah/props/dah_prop_ceiling_vent/002 (-750.0, 169.0, 312.956)
	100051 units/pd2_dlc_dah/props/dah_prop_ceiling_vent/003 (-750.0, -242.0, 312.956)
	100025 units/pd2_dlc_dah/props/dah_prop_chair_b/001 (-56.9994, -13.9651, 0.0)
	100026 units/pd2_dlc_dah/props/dah_prop_coffee_dispenser/001 (-371.826, -328.677, 75.0)
	100027 units/pd2_dlc_dah/props/dah_prop_fan_table/001 (-47.25, -194.105, 98.0)
	100028 units/pd2_dlc_dah/props/dah_prop_file_box/001 (-373.992, -314.153, 0.0)
	100074 units/pd2_dlc_dah/props/dah_prop_file_box/002 (-198.57, 228.747, 0.0)
	100075 units/pd2_dlc_dah/props/dah_prop_file_box/003 (-200.54, 228.4, 36.0)
	100143 units/pd2_dlc_dah/props/dah_prop_floor_lamp/001 (-943.0, 273.0, 0.0)
		mesh_variation state_unlit
	100043 units/pd2_dlc_dah/props/dah_prop_folders_paper/002 (-72.0, -314.0, 98.1807)
	100010 units/pd2_dlc_dah/props/dah_prop_light_switch/001 (-21.0, 123.0, 101.029)
	100016 units/pd2_dlc_dah/props/dah_prop_magazine_open/001 (-244.011, -349.75, 99.1807)
	100044 units/pd2_dlc_dah/props/dah_prop_outlet/001 (-23.0, 56.0, 25.0293)
	100045 units/pd2_dlc_dah/props/dah_prop_outlet/002 (-23.0, 38.0, 25.0293)
	100046 units/pd2_dlc_dah/props/dah_prop_outlet/003 (-418.0, 163.0, 25.0293)
	100047 units/pd2_dlc_dah/props/dah_prop_outlet/004 (-418.0, 145.0, 25.0293)
	100050 units/pd2_dlc_dah/props/dah_prop_outlet/005 (-418.0, -279.0, 25.0293)
	100053 units/pd2_dlc_dah/props/dah_prop_outlet/006 (-418.0, -297.0, 25.0293)
	100054 units/pd2_dlc_dah/props/dah_prop_paper_bin_v2/001 (-43.0, -71.0, 0.0)
	100055 units/pd2_dlc_dah/props/dah_prop_party_cup/001 (-336.715, -339.127, 76.0)
	100056 units/pd2_dlc_dah/props/dah_prop_party_cup/002 (-351.583, -344.809, 76.0)
	100057 units/pd2_dlc_dah/props/dah_prop_party_cup/003 (-336.715, -339.127, 78.0)
	100058 units/pd2_dlc_dah/props/dah_prop_party_cup/004 (-336.715, -339.127, 80.0)
	100059 units/pd2_dlc_dah/props/dah_prop_party_cup/005 (-336.715, -339.127, 82.0)
	100060 units/pd2_dlc_dah/props/dah_prop_party_cup/006 (-336.715, -339.127, 84.0)
	100062 units/pd2_dlc_dah/props/dah_prop_party_cup/007 (-336.715, -339.127, 86.0)
	100018 units/pd2_dlc_dah/props/dah_prop_plant/001 (-494.0, 260.0, 0.0)
	100086 units/pd2_dlc_dah/props/dah_prop_plant/002 (-986.0, -303.0, 0.0)
	100005 units/pd2_dlc_dah/props/dah_prop_security_desk/001 (-32.0, -369.0, 0.0)
	100146 units/pd2_dlc_dah/props/dah_prop_wall_lamp_b/001 (-428.0, 160.0, 126.0)
		mesh_variation lamp_off
	100147 units/pd2_dlc_dah/props/dah_prop_wall_lamp_b/002 (-428.0, -265.0, 126.0)
		mesh_variation lamp_off
	100084 units/pd2_dlc_dah/props/dah_prop_whiteboard_a/001 (-696.0, -298.0, 0.0)
