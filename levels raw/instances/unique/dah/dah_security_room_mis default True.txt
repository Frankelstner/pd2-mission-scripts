﻿´ai_spawn_enemy_001´ ElementSpawnEnemyDummy 100021
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_secret_service_1/ene_secret_service_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position -200.0, -202.0, 0.0
	rotation 0.0, 0.0, 0.923879, -0.382684
	spawn_action none
	team default
	voice 0
´guard_killed_output´ ElementInstanceOutput 100029
	event guard_killed
´activate_door´ ElementUnitSequence 100030
	position 250.0, -150.0, 50.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_door_reinforced/001 (-22.0003, 255.0, 0.0)
			notify_unit_sequence activate_door
			time 0
´func_instance_input_001´ ElementInstanceInput 100031
	event startup
´door_opened´ ElementUnitSequenceTrigger 100032
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence done_opened
			unit_id units/payday2/equipment/gen_interactable_door_reinforced/001 (-22.0003, 255.0, 0.0)
	on_executed
		´func_nav_obstacle_001´ (delay 0)
´func_instance_output_001´ ElementInstanceOutput 100033
	event door_opened
´guard_killed´ ElementEnemyDummyTrigger 100041
	TRIGGER TIMES 1
	elements
		1 ´ai_spawn_enemy_001´
	event death
	position 150.0, -250.0, 50.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´guard_killed_output´ (delay 0)
		´activate_console´ (delay 0)
´spawn_guard´ MissionScriptElement 100048
	EXECUTE ON STARTUP
	on_executed
		´ai_spawn_enemy_001´ (delay 1)
		´point_debug_002´ (delay 0)
		´activate_door´ (delay 0)
		´func_nav_obstacle_002´ (delay 1)
´point_debug_002´ ElementDebug 100178
	as_subtitle True
	debug_string instance_started
	show_instigator False
´guard_tied´ ElementEnemyDummyTrigger 100117
	TRIGGER TIMES 1
	elements
		1 ´ai_spawn_enemy_001´
	event tied
	position 150.0, -300.0, 50.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´guard_killed_output´ (delay 0)
		´activate_console´ (delay 0)
´guard_panicked´ ElementEnemyDummyTrigger 100118
	TRIGGER TIMES 1
	elements
		1 ´ai_spawn_enemy_001´
	event panic
	position 150.0, -350.0, 50.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´guard_killed_output´ (delay 0)
		´activate_console´ (delay 0)
´activate_console´ ElementUnitSequence 100158
	position 250.0, -350.0, 50.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_dah/props/dah_prop_security_desk/001 (-32.0, -369.0, 0.0)
			notify_unit_sequence state_interaction_enabled
			time 0
´func_nav_obstacle_001´ ElementNavObstacle 100034
	obstacle_list
		1
			guis_id 2
			obj_name 6f2452ed502b8b6f
			unit_id units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-96.0, 151.0, 0.0)
	operation remove
	position 250.0, -50.0, 50.0
	rotation 0.0, 0.0, 0.0, -1.0
´func_nav_obstacle_002´ ElementNavObstacle 100035
	obstacle_list
		1
			guis_id 2
			obj_name 6f2452ed502b8b6f
			unit_id units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-96.0, 151.0, 0.0)
	operation add
	position 350.0, -125.0, 50.0
	rotation 0.0, 0.0, 0.0, -1.0
