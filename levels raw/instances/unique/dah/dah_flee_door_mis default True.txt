﻿´trigger_area_report_001´ ElementAreaReportTrigger 100002
	amount 1
	depth 5
	height 500
	instigator civilians
	instigator_name 
	interval 0.1
	position 100.0, 500.0, 100.0
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	shape_type box
	spawn_unit_elements
	use_disabled_shapes False
	use_shape_element_ids
		1 ´point_shape_001´
	width 5
	on_executed
		´if_closed´ (delay 0) (alternative enter)
		´if_open_and_empty´ (delay 0.5) (alternative leave)
		´add_1´ (delay 0) (alternative enter)
		´reduce_1´ (delay 0) (alternative leave)
´point_shape_001´ ElementShape 100001
	depth 110
	height 230
	position -84.0, -50.0, 100.0
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	shape_type box
	width 222.65
´if_closed´ ElementCounterFilter 100003
	check_type equal
	elements
		1 ´closed_bool´
	needed_to_execute all
	value 1
	on_executed
		´open_door´ (delay 0)
´if_open_and_empty´ ElementCounterFilter 100004
	check_type equal
	elements
		1 ´closed_bool´
		2 ´dudes_in_trigger´
	needed_to_execute all
	value 0
	on_executed
		´close_door´ (delay 0)
´close_door´ ElementUnitSequence 100005
	position 200.0, 700.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 2
			name run_sequence
			notify_unit_id units/world/architecture/bank/bank_door_toilet/001 (0.0, 0.0, 5.42101e-20)
			notify_unit_sequence anim_close_door
			time 0
	on_executed
		´set_true´ (delay 0.05)
´open_door´ ElementUnitSequence 100006
	position 100.0, 700.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 2
			name run_sequence
			notify_unit_id units/world/architecture/bank/bank_door_toilet/001 (0.0, 0.0, 5.42101e-20)
			notify_unit_sequence anim_open_door
			time 0
	on_executed
		´set_false´ (delay 0.05)
´set_false´ ElementCounterOperator 100007
	amount 0
	elements
		1 ´closed_bool´
	operation set
´closed_bool´ ElementCounter 100008
	counter_target 1
	digital_gui_unit_ids
´set_true´ ElementCounterOperator 100009
	amount 1
	elements
		1 ´closed_bool´
	operation set
´add_1´ ElementCounterOperator 100010
	amount 1
	elements
		1 ´dudes_in_trigger´
	operation add
	on_executed
		´point_debug_001´ (delay 0)
´dudes_in_trigger´ ElementCounter 100011
	counter_target 0
	digital_gui_unit_ids
´reduce_1´ ElementCounterOperator 100012
	amount 1
	elements
		1 ´dudes_in_trigger´
	operation subtract
	on_executed
		´point_debug_002´ (delay 0)
´trigger_area_report_002´ ElementAreaReportTrigger 100013
	amount 1
	depth 5
	height 500
	instigator enemies
	instigator_name 
	interval 0.1
	position 200.0, 500.0, 100.0
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	shape_type box
	spawn_unit_elements
	use_disabled_shapes False
	use_shape_element_ids
		1 ´point_shape_001´
	width 5
	on_executed
		´if_closed´ (delay 0) (alternative enter)
		´if_open_and_empty´ (delay 0.5) (alternative leave)
		´add_1´ (delay 0) (alternative enter)
		´reduce_1´ (delay 0) (alternative leave)
´start´ MissionScriptElement 100014
	EXECUTE ON STARTUP
	DISABLED
´point_debug_001´ ElementDebug 100015
	as_subtitle False
	debug_string add1
	show_instigator False
´point_debug_002´ ElementDebug 100016
	as_subtitle False
	debug_string reduce1
	show_instigator False
