ID range vs continent name:
	100000: world

statics
	100008 units/dev_tools/level_tools/dev_ai_vis_blocker_005x2x2m/001 (136.0, 125.0, 1998.89)
	100010 units/dev_tools/level_tools/dev_ai_vis_blocker_005x2x2m/002 (136.0, -50.0, 1998.89)
	100011 units/dev_tools/level_tools/dev_ai_vis_blocker_005x2x2m/003 (136.0, -50.0, 2200.89)
	100012 units/dev_tools/level_tools/dev_ai_vis_blocker_005x2x2m/004 (136.0, -49.9999, 2000.89)
	100013 units/dev_tools/level_tools/dev_ai_vis_blocker_005x2x2m/005 (100.0, 150.0, 1995.89)
	100015 units/dev_tools/level_tools/dev_collision_1x1m/001 (-50.0, 25.0, 1600.0)
	100016 units/dev_tools/level_tools/dev_collision_1x1m/002 (-50.0, -50.0, 1600.0)
	100017 units/dev_tools/level_tools/dev_collision_1x1m/003 (25.0, -50.0, 1600.0)
	100018 units/dev_tools/level_tools/dev_collision_1x1m/004 (25.0, 25.0, 1600.0)
	100003 units/payday2/architecture/mkp_int_floor_2x2m_a/001 (-60.0, -63.0, 1600.5)
	100009 units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0)
		disable_collision True
		disable_on_ai_graph True
		mesh_variation hidden
