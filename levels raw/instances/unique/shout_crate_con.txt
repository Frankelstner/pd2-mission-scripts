ID range vs continent name:
	100000: world

statics
	100003 units/payday2/equipment/ind_interactable_hardcase_loot_gold/001 (0.0, 0.0, 33.6459)
		disable_on_ai_graph True
		hide_on_projection_light True
	100004 units/payday2/equipment/ind_interactable_hardcase_loot_money/001 (0.0, 2.0, 31.6459)
		disable_on_ai_graph True
		hide_on_projection_light True
	100006 units/payday2/pickups/gen_pku_cocaine/001 (2.98023e-06, 0.0, 32.6459)
		disable_on_ai_graph True
		hide_on_projection_light True
	100000 units/pd2_dlc1/equipment/ind_interactable_shipping_crate_2x1x1m/001 (0.0, 0.0, 0.0)
		hide_on_projection_light True
		mesh_variation state_requires_crowbar
