ID range vs continent name:
	100000: world

statics
	100011 units/dev_tools/level_tools/dev_collision_1x3m/001 (-246.0, -96.0, 1200.0)
	100012 units/dev_tools/level_tools/dev_collision_1x3m/002 (-146.0, -96.0, 1200.0)
	100013 units/dev_tools/level_tools/dev_collision_1x3m/003 (-46.0, -96.0, 1200.0)
	100015 units/equipment/requires_crowbar_interactive_template/001 (-199.0, -544.0, 1330.0)
		mesh_variation enable_interaction
	100022 units/equipment/requires_crowbar_interactive_template/002 (-199.0, -551.0, -670.0)
		mesh_variation enable_interaction
	100008 units/payday2/architecture/elevator_shaft/com_int_election_elevator_shaft/003 (-100.0, -25.002, 400.0)
	100014 units/payday2/architecture/elevator_shaft/com_int_election_elevator_shaft/005 (-100.0, -25.002, 800.0)
	100020 units/payday2/architecture/elevator_shaft/com_int_election_elevator_shaft/007 (-100.0, -25.002, 1200.0)
	100078 units/payday2/architecture/elevator_shaft/com_int_election_elevator_shaft/010 (-100.0, -25.002, 0.0)
	100075 units/payday2/architecture/elevator_shaft/com_int_election_elevator_shaft/011 (-100.0, -25.002, -400.0)
	100079 units/payday2/architecture/elevator_shaft/com_int_election_elevator_shaft/012 (-100.0, -25.002, 1600.0)
	100032 units/payday2/equipment/gen_equipment_zipline_motor/001 (-199.0, -176.0, 1154.0)
		mesh_variation state_zipline_enable
		end_pos -203.82, -114.22, 98.7463
		slack 0
		speed 500
		usage_type person
	100080 units/payday2/equipment/gen_equipment_zipline_motor/003 (-221.416, -181.916, 1154.0)
		mesh_variation state_zipline_enable
		end_pos -228.599, -115.791, 72.2057
		slack 0
		speed 500
		usage_type person
	100019 units/pd2_dlc_fish/lxy_int_election_elevator_exterior/001 (-100.0, -6.10352e-05, -400.0)
		mesh_variation anim_close
	100002 units/pd2_dlc_spa/architecture/int/spa_int_election_elevator/009 (-100.0, 9.0, 800.0)
		mesh_variation close_doors
	100004 units/pd2_dlc_spa/architecture/int/spa_int_election_elevator/010 (-100.0, 9.0, 400.0)
		mesh_variation close_doors
	100009 units/pd2_dlc_spa/architecture/int/spa_int_election_elevator/011 (-100.0, 9.0, 0.0)
		mesh_variation close_doors
	100025 units/pd2_dlc_spa/architecture/int/spa_int_election_elevator/main (-100.0, 9.0, 1200.0)
		mesh_variation close_doors
	100024 units/pd2_dlc_spa/architecture/int/spa_int_election_elevator_inside/001 (-100.0, 0.0, -400.0)
	100001 units/pd2_dlc_spa/architecture/int/spa_int_election_elevator_shaft_plaster/001 (-100.0, -25.0, 0.0)
	100007 units/pd2_dlc_spa/architecture/int/spa_int_election_elevator_shaft_plaster/002 (-100.0, -25.0, -400.0)
	100003 units/pd2_dlc_spa/architecture/int/spa_int_election_elevator_shaft_plaster/003 (-100.0, -25.0, 400.0)
	100005 units/pd2_dlc_spa/architecture/int/spa_int_election_elevator_shaft_plaster/005 (-100.0, -25.0, 800.0)
	100016 units/pd2_dlc_spa/architecture/int/spa_int_election_elevator_shaft_plaster/008 (-100.0, -25.0, 1200.0)
	100076 units/pd2_dlc_spa/architecture/int/spa_int_election_elevator_shaft_plaster_topp/001 (-100.0, -25.0, 1600.0)
	100081 units/pd2_dlc_spa/architecture/int/spa_int_election_elevator_shaft_plaster_topp/002 (-300.0, -25.0, -400.0)
