ID range vs continent name:
	100000: world

statics
	100014 units/dev_tools/level_tools/dev_collision_4x3m/001 (200.0, 0.0, 2.5)
	100001 units/pd2_dlc_peta/props/pta_prop_debris_wood_01/001 (43.0, -10.0, 15.5)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
