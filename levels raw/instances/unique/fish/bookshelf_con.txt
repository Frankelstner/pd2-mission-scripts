ID range vs continent name:
	100000: world

statics
	100000 units/pd2_dlc_fish/lxy_prop_auction_tag/001 (85.0881, -47.0968, 36.0)
	100044 units/pd2_dlc_fish/lxy_prop_auction_tag/002 (-89.9022, -47.134, 36.9657)
	100046 units/pd2_dlc_fish/lxy_prop_auction_tag/003 (86.584, 47.9062, 35.4)
	100016 units/pd2_dlc_fish/lxy_prop_auction_tag/004 (-87.7021, 47.9062, 36.3496)
	100009 units/pd2_dlc_lxy/architecture/yacht_dhruva/walls/lxy_int_wall_bookshelf_drawer/001 (-187.0, -26.9998, 0.0)
		mesh_variation disable_interaction
	100028 units/pd2_dlc_lxy/architecture/yacht_dhruva/walls/lxy_int_wall_bookshelf_drawer/002 (-12.0, -26.9998, 0.0)
		mesh_variation disable_interaction
	100054 units/pd2_dlc_lxy/architecture/yacht_dhruva/walls/lxy_int_wall_bookshelf_drawer/003 (13.0, 28.0002, 0.0)
		mesh_variation disable_interaction
	100079 units/pd2_dlc_lxy/architecture/yacht_dhruva/walls/lxy_int_wall_bookshelf_drawer/004 (188.0, 28.0001, 0.0)
		mesh_variation disable_interaction
	100004 units/pd2_dlc_lxy/architecture/yacht_dhruva/walls/lxy_int_wall_bookshelf_var02/001 (-175.0, -50.0, 0.0)
	100039 units/pd2_dlc_lxy/pickups/lxy_bookshelf_money_plastic_wrapped/001 (-115.0, -27.0, -2.28492e-05)
		mesh_variation disable_interaction
	100041 units/pd2_dlc_lxy/pickups/lxy_bookshelf_money_plastic_wrapped/002 (-65.0, -27.0, -1.55483e-05)
		mesh_variation disable_interaction
	100042 units/pd2_dlc_lxy/pickups/lxy_bookshelf_money_plastic_wrapped/003 (110.0, -27.0, 1.00045e-05)
		mesh_variation disable_interaction
	100051 units/pd2_dlc_lxy/pickups/lxy_bookshelf_money_plastic_wrapped/004 (60.0, -27.0, 2.70368e-06)
		mesh_variation disable_interaction
	100062 units/pd2_dlc_lxy/pickups/lxy_bookshelf_money_plastic_wrapped/005 (60.0, 27.0, -3.067e-06)
		mesh_variation anim_close
	100063 units/pd2_dlc_lxy/pickups/lxy_bookshelf_money_plastic_wrapped/006 (110.0, 27.0, 4.2338e-06)
		mesh_variation disable_interaction
	100064 units/pd2_dlc_lxy/pickups/lxy_bookshelf_money_plastic_wrapped/007 (-61.0, 27.0, -2.0735e-05)
		mesh_variation disable_interaction
	100065 units/pd2_dlc_lxy/pickups/lxy_bookshelf_money_plastic_wrapped/008 (-111.0, 27.0, -2.80358e-05)
		mesh_variation disable_interaction
