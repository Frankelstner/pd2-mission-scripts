ID range vs continent name:
	100000: world

statics
	100211 units/pd2_dlc_fish/editable_text_system/txt_color (-10.0, 21.0, -68.0)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text   
  
  
  
  

         COLOR_TAG:

		vertical top
		word_wrap False
		wrap False
	100052 units/pd2_dlc_fish/editable_text_system/txt_color_blue (-10.0, 21.0, -68.0)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text   
  
  
  
  
  
                   .BLUE
		vertical top
		word_wrap False
		wrap False
	100053 units/pd2_dlc_fish/editable_text_system/txt_color_green (-10.0, 21.0, -68.0)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text   
  
  
  
  
  
                   .GREEN
		vertical top
		word_wrap False
		wrap False
	100056 units/pd2_dlc_fish/editable_text_system/txt_color_red (-10.0, 21.0, -68.0)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text   
  
  
  
  
  
                   .RED
		vertical top
		word_wrap False
		wrap False
	100057 units/pd2_dlc_fish/editable_text_system/txt_color_white (-10.0, 21.0, -68.0)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text   
  
  
  
  
  
                  .WHITE
		vertical top
		word_wrap False
		wrap False
	100064 units/pd2_dlc_fish/editable_text_system/txt_color_yellow (-10.0, 21.0, -68.0)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 

  
  
  
  
                  .YELLOW
		vertical top
		word_wrap False
		wrap False
	100198 units/pd2_dlc_fish/editable_text_system/txt_header (-10.2227, 20.5359, -68.1001)
		align left
		alpha 0.75
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text B41N emergency reboot sequence suspended
C:> Root
C:>run MEM_DUMP.exe -c..Outputting range 0xA000-0xFFFF
Dumping: "secure_dbase_cntrbnd";
		vertical top
		word_wrap False
		wrap False
	100048 units/pd2_dlc_fish/editable_text_system/txt_lower_deck (-10.2227, 20.5359, -68.1001)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 







        LOWER_DECK:
		vertical top
		word_wrap False
		wrap False
	100095 units/pd2_dlc_fish/editable_text_system/txt_lower_deck_cabinet (-10.2227, 20.5359, -68.1001)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 









        CABINET
		vertical top
		word_wrap False
		wrap False
	100099 units/pd2_dlc_fish/editable_text_system/txt_lower_deck_cabinet_OFF (-10.2227, 20.5359, -68.1001)
		align left
		alpha 0.5
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 









        ..>/............{
		vertical top
		word_wrap False
		wrap False
	100094 units/pd2_dlc_fish/editable_text_system/txt_lower_deck_fridge (-10.2227, 20.5359, -68.1001)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 








        FRIDGE
		vertical top
		word_wrap False
		wrap False
	100104 units/pd2_dlc_fish/editable_text_system/txt_lower_deck_fridge_OFF (-10.2227, 20.5359, -68.1001)
		align left
		alpha 0.5
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 








        ]......<.....(}.[
		vertical top
		word_wrap False
		wrap False
	100074 units/pd2_dlc_fish/editable_text_system/txt_main_cigar (-10.0, 20.5359, -68.0)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 














        CIGAR_and_WINE

		vertical top
		word_wrap False
		wrap False
	100105 units/pd2_dlc_fish/editable_text_system/txt_main_cigar_OFF (-10.0, 20.5359, -68.0)
		align left
		alpha 0.5
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 














        .......)./....../

		vertical top
		word_wrap False
		wrap False
	100047 units/pd2_dlc_fish/editable_text_system/txt_main_deck (-10.0, 20.5359, -68.0)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 













        MAIN_DECK:

		vertical top
		word_wrap False
		wrap False
	100077 units/pd2_dlc_fish/editable_text_system/txt_main_lifeboat (-10.0, 20.5359, -68.0)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 















        LIFEBOAT

		vertical top
		word_wrap False
		wrap False
	100106 units/pd2_dlc_fish/editable_text_system/txt_main_lifeboat_OFF (-10.0, 20.5359, -68.0)
		align left
		alpha 0.5
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 















        .....!.{....(..[.

		vertical top
		word_wrap False
		wrap False
	100079 units/pd2_dlc_fish/editable_text_system/txt_main_room_101 (-10.0, 20.5359, -68.0)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 
















        ROOM_101

		vertical top
		word_wrap False
		wrap False
	100082 units/pd2_dlc_fish/editable_text_system/txt_main_room_102 (-10.0, 20.5359, -68.0)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 
















        ROOM_102

		vertical top
		word_wrap False
		wrap False
	100085 units/pd2_dlc_fish/editable_text_system/txt_main_room_103 (-10.0, 20.5359, -68.0)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 
















        ROOM_103

		vertical top
		word_wrap False
		wrap False
	100087 units/pd2_dlc_fish/editable_text_system/txt_main_room_104 (-10.0, 20.5359, -68.0)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 
















        ROOM_104

		vertical top
		word_wrap False
		wrap False
	100107 units/pd2_dlc_fish/editable_text_system/txt_main_room_OFF (-10.0, 20.5359, -68.0)
		align left
		alpha 0.5
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 
















        ..:(/.].]<*:!..)*

		vertical top
		word_wrap False
		wrap False
	100089 units/pd2_dlc_fish/editable_text_system/txt_main_room_aquarium (-10.0, 20.5359, -68.0)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 


















        AQUARIUM

		vertical top
		word_wrap False
		wrap False
	100108 units/pd2_dlc_fish/editable_text_system/txt_main_room_aquarium_OFF (-10.0, 20.5359, -68.0)
		align left
		alpha 0.5
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 


















        ..}.....<..]%..:$

		vertical top
		word_wrap False
		wrap False
	100088 units/pd2_dlc_fish/editable_text_system/txt_main_room_food_cart (-10.0, 20.5359, -68.0)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 

















        FOOD_CART

		vertical top
		word_wrap False
		wrap False
	100109 units/pd2_dlc_fish/editable_text_system/txt_main_room_food_cart_OFF (-10.0, 20.5359, -68.0)
		align left
		alpha 0.5
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 

















        .).{....$/....]./

		vertical top
		word_wrap False
		wrap False
	100050 units/pd2_dlc_fish/editable_text_system/txt_numbers_01 (-10.0364, 20.9999, -68.1465)
		align left
		alpha 0.5
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 




  0xAD22 BLK_BEGIN_FILE.
  0xE249
  0x4B54 ..<[..}........%.
  0xD299
  0xa11F
  0x2E14
  0xFB30 ..)...)>.......$.
  0x54DA >./...[:.{..%../(
  0x51AA .........(.....>.
  0xEA21
  0x658L
  0xC12u
  0xFD9c
  0x460y
  0xFD92
		vertical top
		word_wrap False
		wrap False
	100049 units/pd2_dlc_fish/editable_text_system/txt_numbers_02 (-10.0364, 20.9999, -68.1465)
		align left
		alpha 0.5
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 




                             0x3117
                             0x2fca
                             0xa3c4
                             0x11dF .......(.)..<.....
                             0x413r ...{...*){.*..>)..
                             0xd94A ....)}...........*
                             0x901s .....]..!........>
                             0x4fcs .{$...<:........./
                             0x978E .*.......<}.......
                             0x4ca0
                             0x4101
                             0x4054
                             0xc95f .>)$.:............
                             0x3c4c .)....>.<>:/.{..%.
                             0x65c8 [*]:}%.!.....<...:
		vertical top
		word_wrap False
		wrap False
	100199 units/pd2_dlc_fish/editable_text_system/txt_scramble_01 (-10.0364, 20.9999, -68.1465)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 




  0xVivi %..$<:}|..!..$>%.   0x65c8 [*]<:$}%.!....<..
  0x6V3a $....[:.{..]..!..   0x3c4c .)....<>.<]>:.{.%
  0x4B54 ..<[..}........%.   0x3117 .)...<.>.<]>:.{.%
  0xD299 $..<}..//..*.$.$.   0x2fca [*]:$...!....<<..
  0xa11F ....)}.........}|   0xa3c4 ...{...*){..<.).}
  0x2E14 ...{...*){...).*$   0x11de ....<...(.).<...%
  0xFB30 ..)...)>.......$.   0x4139 ...{...*){..<.).}
  0x54DA >./...[:.{..%../(   0xd94b ....)}..{.}......
  0x51AA .........(.....>.   0x901e .....]..!..<..]..
  0xEA21 .).{....$/....]./   0x4fc5 .{$...{<:...$....
  0x658L .>)$.:..$....}%.$   0x9783 .*...{....<}$....
  0xC12u .)....>.<]>:.{.%.   0x4ca0 .)....<>.<]>:.{.%
  0xFD9c ....)}.........}|   0x4101 >./...[:.{..%../(
  0x460y .).{....$/....]./   0x4054 .....]..!..<..]..
  0xFD92 .$^&..^$..&#...{}   0xc95f .>)$<.:..$....}%.
		vertical top
		word_wrap False
		wrap False
	100204 units/pd2_dlc_fish/editable_text_system/txt_scramble_02 (-10.0364, 20.9999, -68.1465)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 




  0xa11F ....)}.........}|   0xa3c4 ...{...*){..<.).}
  0x6V3a $....[:.{..]..!..   0x3c4c .)....<>.<]>:.{.%
  0xEA21 .).{....$/....]./   0x4fc5 .{$...{<:...$....
  0xFB30 ..)...)>.......$.   0x4139 ...{...*){..<.).}
  0xD299 $..<}..//..*.$.$.   0x2fca [*]:$...!....<<..
  0x4B54 ..<[..}........%.   0x3117 .)...<.>.<]>:.{.%
  0xFD9c ....)}.........}|   0x4101 >./...[:.{..%../(
  0xFD92 .$^&..^$..&#...{}   0xc95f .>)$<.:..$....}%.
  0x54DA >./...[:.{..%../(   0xd94b ....)}..{.}......
  0x658L .>)$.:..$....}%.$   0x9783 .*...{....<}$....
  0xC12u .)....>.<]>:.{.%.   0x4ca0 .)....<>.<]>:.{.%
  0x460y .).{....$/....]./   0x4054 .....]..!..<..]..
  0x51AA .........(.....>.   0x901e .....]..!..<..]..
  0xVivi %..$<:}|..!..$>%.   0x65c8 [*]<:$}%.!....<..
  0x2E14 ...{...*){...).*$   0x11de ....<...(.).<...%
		vertical top
		word_wrap False
		wrap False
	100212 units/pd2_dlc_fish/editable_text_system/txt_scramble_03 (-10.0364, 20.9999, -68.1465)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 




  0x3c4c .)....<>.<]>:.{.%   0x6V3a $....[:.{..]..!..
  0x4fc5 .{$...{<:...$....   0xEA21 .).{....$/....]./
  0x2fca [*]:$...!....<<..   0xD299 $..<}..//..*.$.$.
  0x3117 .)...<.>.<]>:.{.%   0x4B54 ..<[..}........%.
  0xa3c4 ...{...*){..<.).}   0xa11F ....)}.........}|
  0x4139 ...{...*){..<.).}   0xFB30 ..)...)>.......$.
  0xd94b ....)}..{.}......   0x54DA >./...[:.{..%../(
  0xc95f .>)$<.:..$....}%.   0xFD92 .$^&..^$..&#...{}
  0x4101 >./...[:.{..%../(   0xFD9c ....)}.........}|
  0x4ca0 .)....<>.<]>:.{.%   0xC12u .)....>.<]>:.{.%.
  0x9783 .*...{....<}$....   0x658L .>)$.:..$....}%.$
  0x4054 .....]..!..<..]..   0x460y .).{....$/....]./
  0x65c8 [*]<:$}%.!....<..   0xVivi %..$<:}|..!..$>%.
  0x901e .....]..!..<..]..   0x51AA .........(.....>.
  0x11de ....<...(.).<...%   0x2E14 ...{...*){...).*$
		vertical top
		word_wrap False
		wrap False
	100045 units/pd2_dlc_fish/editable_text_system/txt_upper_level_01 (-10.2227, 20.5359, -68.1001)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 




                                   UPPER_LEVEL_01:
		vertical top
		word_wrap False
		wrap False
	100044 units/pd2_dlc_fish/editable_text_system/txt_upper_level_02 (-10.2227, 20.5359, -68.1001)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 













                                   UPPER_LEVEL_02:

		vertical top
		word_wrap False
		wrap False
	100091 units/pd2_dlc_fish/editable_text_system/txt_upper_level_1_aquarium (-10.2227, 20.5359, -68.1001)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 






                                   AQUARIUM
		vertical top
		word_wrap False
		wrap False
	100110 units/pd2_dlc_fish/editable_text_system/txt_upper_level_1_aquarium_OFF (-10.2227, 20.5359, -68.1001)
		align left
		alpha 0.5
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 






                                   ....>....]!.>....!
		vertical top
		word_wrap False
		wrap False
	100090 units/pd2_dlc_fish/editable_text_system/txt_upper_level_1_food_cart (-10.2227, 20.5359, -68.1001)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 





                                   FOOD_CART
		vertical top
		word_wrap False
		wrap False
	100111 units/pd2_dlc_fish/editable_text_system/txt_upper_level_1_food_cart_OFF (-10.2227, 20.5359, -68.1001)
		align left
		alpha 0.5
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 





                                   )..[..].*.(..%..{.
		vertical top
		word_wrap False
		wrap False
	100093 units/pd2_dlc_fish/editable_text_system/txt_upper_level_2_Food_cart (-10.2227, 20.5359, -68.1001)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 















                                   FOOD_CART

		vertical top
		word_wrap False
		wrap False
	100114 units/pd2_dlc_fish/editable_text_system/txt_upper_level_2_Food_cart_OFF (-10.2227, 20.5359, -68.1001)
		align left
		alpha 0.5
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 















                                   ...]..:.).....).).

		vertical top
		word_wrap False
		wrap False
	100092 units/pd2_dlc_fish/editable_text_system/txt_upper_level_2_bookshelf (-10.2227, 20.5359, -68.1001)
		align left
		alpha 1
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 














                                   BOOKSHELF

		vertical top
		word_wrap False
		wrap False
	100112 units/pd2_dlc_fish/editable_text_system/txt_upper_level_2_bookshelf_OFF (-10.2227, 20.5359, -68.1001)
		align left
		alpha 0.5
		blend_mode add
		font fonts/font_system
		font_color 0.454902, 0.886275, 0.196078
		font_size 0.12
		render_template Text
		shape
			1 0
			2 0
			3 0.03999999910593
			4 0.15000000596046
		text 














                                   ..>(*..:.!.....&&.

		vertical top
		word_wrap False
		wrap False
	100043 units/pd2_dlc_fish/lxy_interactable_laptop/001 (-1.0, 0.0, 0.0)
		mesh_variation state_password_interact
	100011 units/pd2_dlc_fish/lxy_prop_auction_tag/001 (-8.0, -32.4852, 0.372223)
		mesh_variation show
