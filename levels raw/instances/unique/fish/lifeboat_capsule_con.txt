ID range vs continent name:
	100000: world

statics
	100052 units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/001 (29.0, 1.99998, 34.0)
		hide_on_projection_light True
		mesh_variation disable_interaction
	100054 units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/002 (-21.0, 0.999985, 34.0)
		hide_on_projection_light True
		mesh_variation disable_interaction
	100043 units/pd2_dlc_fish/boat_capsule_01/lxy_prop_life_boat_capsule/001 (0.0, 0.0, 0.0)
		hide_on_projection_light True
		mesh_variation disable_interaction
	100000 units/pd2_dlc_fish/lxy_prop_auction_tag/001 (1.0, -34.0, 51.0)
		hide_on_projection_light True
	100044 units/pd2_dlc_fish/lxy_prop_auction_tag/002 (6.0, -34.0, 51.0)
		hide_on_projection_light True
	100046 units/pd2_dlc_fish/lxy_prop_auction_tag/003 (-3.00761, -34.0, 51.8257)
		hide_on_projection_light True
