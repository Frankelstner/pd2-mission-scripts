ID range vs continent name:
	100000: world

statics
	100007 units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/001 (39.0, 1.0, 2.00002)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100044 units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/002 (-68.0, -26.0, 23.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100051 units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/003 (100.0, 25.5152, 80.1254)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100052 units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/004 (99.0122, -14.8436, 61.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100000 units/pd2_dlc_fish/lxy_prop_auction_tag/001 (-36.0, -97.0, 124.0)
	100046 units/pd2_dlc_fish/lxy_prop_auction_tag/003 (-35.0219, -97.0, 122.792)
	100027 units/pd2_dlc_lxy/architecture/yacht/bath/lxy_int_main_bath_shower/001 (-100.0, -100.0, 0.0)
		mesh_variation disable_interaction
