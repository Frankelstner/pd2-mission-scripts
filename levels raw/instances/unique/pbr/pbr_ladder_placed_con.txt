ID range vs continent name:
	100000: world

statics
	100017 units/dev_tools/dev_ladder/001 (22.0, -4.17233e-06, -27.996)
		height 430
		width 80
	100008 units/pd2_dlc_cro/gen_prop_ladder_marker/temp_marker (125.0, 0.0, 0.00400394)
		mesh_variation hide
	100034 units/pd2_dlc_jerry/equipment/jry_int_placeable_ladder/001 (4.0, 0.0, -0.996002)
	100027 units/pd2_dlc_jerry/pickups/ladder/jry_pku_pickable_ladder_extended/001 (4.0, 0.0, -0.996002)
		mesh_variation interaction_disable
