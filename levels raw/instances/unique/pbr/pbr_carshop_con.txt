ID range vs continent name:
	100000: world

statics
	100166 units/dev_tools/level_tools/dev_bag_collision_1x1m/001 (850.0, -1064.0, 795.0)
	100167 units/dev_tools/level_tools/dev_bag_collision_1x1m/002 (1750.0, -1064.0, 795.0)
	100163 units/dev_tools/level_tools/dev_bag_collision_1x3m/001 (250.0, -1064.0, 795.0)
	100165 units/dev_tools/level_tools/dev_bag_collision_1x3m/002 (550.0, -1064.0, 795.0)
	100168 units/dev_tools/level_tools/dev_bag_collision_1x3m/003 (1450.0, -1064.0, 795.0)
	100169 units/dev_tools/level_tools/dev_bag_collision_1x3m/004 (1150.0, -1064.0, 795.0)
	100068 units/payday2/architecture/com_int_basement_garage_parkingline_c_6m/001 (-199.965, -3100.0, -25.9994)
	100069 units/payday2/architecture/com_int_basement_garage_parkingline_c_6m/002 (-199.966, -2800.0, -25.9994)
	100281 units/payday2/architecture/hcm_int_handrail_2x1m_a/001 (1100.0, -500.0, 350.0)
		disable_shadows True
	100287 units/payday2/architecture/hcm_int_handrail_2x1m_a/002 (900.0, -500.0, 350.0)
		disable_shadows True
	100288 units/payday2/architecture/hcm_int_handrail_2x1m_a/003 (700.0, -500.0, 350.0)
		disable_shadows True
	100293 units/payday2/architecture/hcm_int_handrail_2x1m_a/007 (1300.0, -500.0, 350.0)
		disable_shadows True
	100294 units/payday2/architecture/hcm_int_handrail_2x1m_a/008 (1300.0, -700.0, 350.0)
		disable_shadows True
	100295 units/payday2/architecture/hcm_int_handrail_2x1m_a/009 (1300.0, -1050.0, 350.0)
		disable_shadows True
	100296 units/payday2/architecture/hcm_int_handrail_2x1m_a/010 (1300.0, -1250.0, 350.0)
		disable_shadows True
	100297 units/payday2/architecture/hcm_int_handrail_2x1m_a/011 (1300.0, -1650.0, 350.0)
		disable_shadows True
	100298 units/payday2/architecture/hcm_int_handrail_2x1m_a/012 (1300.0, -1450.0, 350.0)
		disable_shadows True
	100299 units/payday2/architecture/hcm_int_handrail_2x1m_a/013 (1300.0, -1850.0, 350.0)
		disable_shadows True
	100133 units/payday2/architecture/ind/ind_ext_level_0_window_small/001 (2532.0, -2004.0, 739.0)
		disable_shadows True
	100134 units/payday2/architecture/ind/ind_ext_level_0_window_small/002 (2532.0, -1250.0, 739.0)
		disable_shadows True
	100135 units/payday2/architecture/ind/ind_ext_level_0_window_small/003 (2532.0, -503.0, 739.0)
		disable_shadows True
	100136 units/payday2/architecture/ind/ind_ext_level_0_window_small/004 (2531.0, 231.0, 739.0)
		disable_shadows True
	100137 units/payday2/architecture/ind/ind_ext_level_0_window_small/005 (2687.0, -2496.0, 739.0)
		disable_shadows True
	100138 units/payday2/architecture/ind/ind_ext_level_0_window_small/006 (3140.0, -2496.0, 739.0)
		disable_shadows True
	100139 units/payday2/architecture/ind/ind_ext_level_0_window_small/007 (3600.0, -2496.0, 739.0)
		disable_shadows True
	100140 units/payday2/architecture/ind/ind_ext_level_0_window_small/008 (2943.0, 812.0, 739.0)
		disable_shadows True
	100141 units/payday2/architecture/ind/ind_ext_level_0_window_small/009 (3491.0, 812.0, 739.0)
		disable_shadows True
	100123 units/payday2/equipment/gen_interactable_door_reinforced_lab/001 (1321.0, -2455.0, 0.0)
		disable_shadows True
	100004 units/payday2/equipment/gen_interactable_door_reinforced_lab/002 (1755.0, -2078.0, 350.0)
		disable_on_ai_graph True
		hide_on_projection_light True
		mesh_variation state_door_open
	100005 units/payday2/equipment/gen_interactable_door_reinforced_lab/003 (2078.0, -1745.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		mesh_variation state_door_open
	100006 units/payday2/equipment/gen_interactable_door_reinforced_lab/004 (2078.0, -244.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		mesh_variation state_door_open
	100064 units/payday2/props/01/gen_prop_door_ind_03_v2/001 (2566.0, -2084.0, -29.8726)
		mesh_variation state_close_door
	100065 units/payday2/props/01/gen_prop_door_ind_03_v2/002 (2566.0, -1559.0, -29.8726)
		mesh_variation state_close_door
	100122 units/payday2/props/01/gen_prop_door_ind_03_v2/005 (2566.0, -33.0, -29.8726)
		mesh_variation state_close_door
	100124 units/payday2/props/01/gen_prop_door_ind_03_v2/006 (2876.0, 806.0, -29.8726)
		mesh_variation state_close_door
	100498 units/payday2/props/str_prop_rooftop_ac_unit_a_big/001 (3050.0, 150.0, 900.0)
	100499 units/payday2/props/str_prop_rooftop_ac_unit_a_big/002 (3050.0, -1700.0, 900.0)
	100347 units/payday2/props/str_prop_rooftop_ac_unit_a_flat/001 (1700.0, -2575.0, 350.0)
	100043 units/payday2/vehicles/str_vehicle_car_modernsedan/001 (475.0, -1700.0, 0.0)
		disable_on_ai_graph True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.94, 0.7
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_left
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
		2
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.934481, 0.657
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_right
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
		3
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.420139, 0.275
			enabled False
			falloff_exponent 4
			far_range 350
			multiplier identity
			name ls_rear
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100247 units/payday2/vehicles/str_vehicle_car_modernsedan/002 (475.0, -1225.0, 0.0)
		disable_on_ai_graph True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.94, 0.7
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_left
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
		2
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.934481, 0.657
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_right
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
		3
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.420139, 0.275
			enabled False
			falloff_exponent 4
			far_range 350
			multiplier identity
			name ls_rear
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
		mesh_variation red
	100458 units/payday2/vehicles/str_vehicle_car_modernsedan/003 (200.001, -2650.0, -25.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.94, 0.7
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_left
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
		2
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.934481, 0.657
			enabled False
			falloff_exponent 1
			far_range 2000
			multiplier identity
			name ls_right
			near_range 0
			spot_angle_end 40.0728
			spot_angle_start 40.0728
		3
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.420139, 0.275
			enabled False
			falloff_exponent 4
			far_range 350
			multiplier identity
			name ls_rear
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
		mesh_variation black
	100246 units/payday2/vehicles/str_vehicle_car_modernsedan2/002 (475.0, -750.0, 0.0)
		disable_on_ai_graph True
	100407 units/payday2/vehicles/str_vehicle_car_modernsedan2/003 (200.001, -3250.0, -25.0)
		mesh_variation red
	100038 units/pd2_dlc1/architecture/str_ext_park_wall_a_2x2m/002 (-200.0, -2400.0, -100.0)
	100039 units/pd2_dlc1/architecture/str_ext_park_wall_a_2x2m/003 (-200.0, -2600.0, -100.0)
	100042 units/pd2_dlc1/architecture/str_ext_park_wall_a_2x2m/004 (-200.0, -2800.0, -100.0)
	100044 units/pd2_dlc1/architecture/str_ext_park_wall_a_2x2m/005 (-200.0, -3000.0, -100.0)
	100045 units/pd2_dlc1/architecture/str_ext_park_wall_a_2x2m/006 (-200.0, -3200.0, -100.0)
	100047 units/pd2_dlc1/architecture/str_ext_park_wall_a_2x2m/007 (-100.0, -3500.0, -100.0)
	100050 units/pd2_dlc1/architecture/str_ext_park_wall_a_2x2m/008 (99.9999, -3500.0, -100.0)
	100051 units/pd2_dlc1/architecture/str_ext_park_wall_a_2x2m/009 (-100.0, -2325.0, -100.0)
	100052 units/pd2_dlc1/architecture/str_ext_park_wall_a_2x2m/010 (99.9999, -2325.0, -100.0)
	100037 units/pd2_dlc1/architecture/str_ext_park_wall_a_corner_inner_1x1m/001 (-100.0, -2325.0, -100.0)
	100046 units/pd2_dlc1/architecture/str_ext_park_wall_a_corner_inner_1x1m/002 (-175.0, -3400.0, -100.0)
	100161 units/pd2_dlc2/csgo_models/props_downtown/sign_donotenter/001 (2540.0, -2183.08, 206.288)
	100162 units/pd2_dlc2/csgo_models/props_downtown/sign_donotenter/002 (2525.21, 492.524, 47.5273)
	100127 units/pd2_dlc2/csgo_models/props_windows/window_urban_bars_med/001 (2965.0, -2886.0, 209.104)
		disable_shadows True
	100128 units/pd2_dlc2/csgo_models/props_windows/window_urban_bars_med/002 (2965.0, -2766.0, 209.104)
		disable_shadows True
	100129 units/pd2_dlc2/csgo_models/props_windows/window_urban_bars_med/003 (3197.0, -3254.0, 209.104)
		disable_shadows True
	100130 units/pd2_dlc2/csgo_models/props_windows/window_urban_bars_med/004 (3317.0, -3254.0, 209.104)
		disable_shadows True
	100131 units/pd2_dlc2/csgo_models/props_windows/window_urban_bars_med/005 (3683.0, -3254.0, 209.104)
		disable_shadows True
	100132 units/pd2_dlc2/csgo_models/props_windows/window_urban_bars_med/006 (3803.0, -3254.0, 209.104)
		disable_shadows True
	100304 units/pd2_dlc_cage/architecture/cardealership_int/ind_prop_cardealership_pillar_3/003 (1365.0, -444.0, 6.10352e-05)
		disable_shadows True
	100300 units/pd2_dlc_cage/architecture/cardealership_int/ind_prop_cardealership_pillar_3/004 (658.0, -440.0, 6.10352e-05)
		disable_shadows True
	100301 units/pd2_dlc_cage/architecture/cardealership_int/ind_prop_cardealership_pillar_3/005 (1365.0, -436.0, 350.0)
		disable_shadows True
	100032 units/pd2_dlc_cage/architecture/cardealership_int/ind_prop_cardealership_pillar_3/006 (675.0, -436.0, 350.0)
		disable_shadows True
	100305 units/pd2_dlc_cage/architecture/cardealership_int/ind_prop_cardealership_pillar_3/012 (1365.0, -1668.0, 6.10352e-05)
		disable_shadows True
	100308 units/pd2_dlc_cage/architecture/cardealership_int/ind_prop_cardealership_pillar_3/013 (1365.0, -1675.0, 350.0)
		disable_shadows True
	100303 units/pd2_dlc_cage/architecture/cardealership_int/ind_prop_cardealership_pillar_3/014 (1365.0, -1053.0, 6.10352e-05)
		disable_shadows True
	100054 units/pd2_dlc_cage/architecture/ind_cardealership_facade_glass_01/001 (1857.0, -1070.0, 791.0)
		disable_shadows True
	100055 units/pd2_dlc_cage/architecture/ind_cardealership_facade_glass_01/002 (957.0, -1070.0, 791.0)
		disable_shadows True
	100056 units/pd2_dlc_cage/architecture/ind_cardealership_facade_glass_01/003 (1148.0, -1070.0, 791.0)
		disable_shadows True
	100057 units/pd2_dlc_cage/architecture/ind_cardealership_facade_glass_01/004 (248.0, -1070.0, 791.0)
		disable_shadows True
	100026 units/pd2_dlc_cage/architecture/ind_cardealership_facade_window_01/001 (3500.0, -1470.0, 1013.0)
	100031 units/pd2_dlc_cage/architecture/ind_cardealership_facade_window_01/002 (4400.0, -1470.0, 1013.0)
	100053 units/pd2_dlc_cage/architecture/ind_cardealership_facade_window_01_broken/001 (3500.0, -1470.0, 1013.0)
		disable_on_ai_graph True
		hide_on_projection_light True
	100110 units/pd2_dlc_cage/architecture/ind_cardealership_facade_window_01_broken/002 (4400.0, -1470.0, 1013.0)
		disable_on_ai_graph True
		hide_on_projection_light True
	100328 units/pd2_dlc_cage/architecture/ind_cardealership_int_logo/002 (646.0, -2107.0, 649.704)
		disable_shadows True
	100506 units/pd2_dlc_cage/architecture/ind_cardealership_int_logo/003 (-8.99997, -1863.0, 649.704)
		disable_shadows True
	100245 units/pd2_dlc_cage/architecture/ind_cardealership_stairs_spiral/001 (875.0, -1325.0, 0.0)
	100007 units/pd2_dlc_cage/architecture/set/ind_prop_cardealership_glass_rail/001 (475.0, -2100.0, 1.30798)
		disable_shadows True
	100144 units/pd2_dlc_cage/props/pictures/ind_prop_cardealership_picture_02/001 (1878.0, -2075.0, 138.463)
		disable_shadows True
	100009 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_door/001 (475.0, -2089.0, 1.30798)
		disable_shadows True
	100012 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_door/002 (1075.0, -2089.0, 1.30798)
		disable_shadows True
	100011 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/001 (-0.00012207, -400.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
	100014 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/002 (0.0, -600.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
	100015 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/003 (0.0, -600.0, 300.0)
		disable_shadows True
	100016 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/004 (-0.00012207, -400.0, 300.0)
		disable_shadows True
	100000 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/005 (0.00012207, -825.0, 300.0)
		disable_shadows True
	100001 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/006 (0.00012207, -1025.0, 300.0)
		disable_shadows True
	100002 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/007 (0.00012207, -1025.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
	100018 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/008 (0.00012207, -825.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
	100021 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/009 (0.000244141, -1450.0, 300.0)
		disable_shadows True
	100022 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/010 (0.000244141, -1250.0, 300.0)
		disable_shadows True
	100023 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/011 (0.000244141, -1250.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
	100024 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/012 (0.000244141, -1450.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
	100027 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/013 (0.000488281, -1875.0, 300.0)
		disable_shadows True
	100028 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/014 (0.000366211, -1675.0, 300.0)
		disable_shadows True
	100029 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/015 (0.000366211, -1675.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
	100030 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/016 (0.000488281, -1875.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
	100033 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/017 (225.001, -2100.0, 300.0)
		disable_shadows True
	100034 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/018 (25.0006, -2100.0, 300.0)
		disable_shadows True
	100035 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/019 (25.0006, -2100.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
	100036 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/020 (225.001, -2100.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
	100040 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/021 (475.001, -2100.0, 300.0)
		disable_shadows True
	100041 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/022 (675.001, -2100.0, 300.0)
		disable_shadows True
	100008 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/024 (475.0, -2100.0, 1.30798)
		disable_shadows True
	100048 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/025 (1075.0, -2100.0, 300.0)
		disable_shadows True
	100049 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/026 (875.001, -2100.0, 300.0)
		disable_shadows True
	100010 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/027 (1075.0, -2100.0, 1.30798)
		disable_shadows True
	100320 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/032 (2100.0, -1600.0, 300.0)
		disable_on_ai_graph True
		disable_shadows True
	100344 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/033 (25.0, -475.0, 350.0)
		disable_shadows True
	100289 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/034 (225.0, -475.0, 350.0)
		disable_shadows True
	100290 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/035 (425.0, -475.0, 350.0)
		disable_shadows True
	100292 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_wall/036 (700.0, -225.0, 350.0)
		disable_shadows True
	100058 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_window/001 (1490.0, -10.0, 350.0)
		disable_on_ai_graph True
		disable_shadows True
	100059 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_window/002 (1715.0, -10.0, 350.0)
		disable_on_ai_graph True
		disable_shadows True
	100060 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_window/003 (2091.0, -500.0, 350.0)
		disable_on_ai_graph True
		disable_shadows True
	100061 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_window/004 (2091.0, -725.0, 350.0)
		disable_on_ai_graph True
		disable_shadows True
	100062 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_window/005 (2091.0, -950.0, 350.0)
		disable_on_ai_graph True
		disable_shadows True
	100063 units/pd2_dlc_cage/props/set/ind_prop_cardealership_glass_window/006 (2091.0, -1175.0, 350.0)
		disable_on_ai_graph True
		disable_shadows True
	100345 units/pd2_dlc_holly/river/vehicles/lxa_vehicle_car_subcompact_wrecked/001 (475.0, -1225.0, 0.0)
		disable_on_ai_graph True
	100346 units/pd2_dlc_holly/river/vehicles/lxa_vehicle_car_subcompact_wrecked/002 (200.001, -3250.0, -25.0)
	100025 units/pd2_dlc_jerry/architecture/jry_cardealership_back_building/001 (3100.0, -900.0, 0.0)
	100003 units/pd2_dlc_jerry/architecture/jry_cardealership_int/001 (1200.0, -1100.0, 0.0)
	100150 units/pd2_dlc_jerry/terrain/jry_decaying_grass_plane_1m/001 (-100.0, -100.0, -10.0)
	100151 units/pd2_dlc_jerry/terrain/jry_decaying_grass_plane_1m/002 (-200.0, -100.0, -10.0)
	100013 units/pd2_dlc_jerry/terrain/jry_decaying_grass_plane_2m/grass_plane_2m_001 (-200.0, -300.0, -10.0)
	100017 units/pd2_dlc_jerry/terrain/jry_decaying_grass_plane_2m/grass_plane_2m_002 (-200.0, -500.0, -10.0)
	100019 units/pd2_dlc_jerry/terrain/jry_decaying_grass_plane_2m/grass_plane_2m_003 (-200.0, -700.0, -10.0)
	100020 units/pd2_dlc_jerry/terrain/jry_decaying_grass_plane_2m/grass_plane_2m_004 (-200.0, -900.0, -10.0)
	100066 units/pd2_dlc_jerry/terrain/jry_decaying_grass_plane_2m/grass_plane_2m_005 (-200.0, -1100.0, -10.0)
	100067 units/pd2_dlc_jerry/terrain/jry_decaying_grass_plane_2m/grass_plane_2m_006 (-200.0, -1300.0, -10.0)
	100142 units/pd2_dlc_jerry/terrain/jry_decaying_grass_plane_2m/grass_plane_2m_007 (-200.0, -1500.0, -10.0)
	100143 units/pd2_dlc_jerry/terrain/jry_decaying_grass_plane_2m/grass_plane_2m_008 (-200.0, -1700.0, -10.0)
	100145 units/pd2_dlc_jerry/terrain/jry_decaying_grass_plane_2m/grass_plane_2m_009 (-200.0, -1900.0, -10.0)
	100146 units/pd2_dlc_jerry/terrain/jry_decaying_grass_plane_2m/grass_plane_2m_010 (-200.0, -2100.0, -10.0)
	100147 units/pd2_dlc_jerry/terrain/jry_decaying_grass_plane_2m/grass_plane_2m_011 (-3.05176e-05, -2500.0, -8.0)
	100148 units/pd2_dlc_jerry/terrain/jry_decaying_grass_plane_2m/grass_plane_2m_012 (-200.0, -2500.0, -8.0)
	100149 units/pd2_dlc_jerry/terrain/jry_decaying_grass_plane_2m/grass_plane_2m_013 (200.0, -2500.0, -8.0)
	100075 units/pd2_mcmansion/props/mcm_prop_garage_door_closed/001 (1550.0, -2850.0, 0.0)
		disable_shadows True
	100076 units/pd2_mcmansion/props/mcm_prop_garage_door_closed/002 (1550.0, -3250.0, 0.0)
		disable_shadows True
	100471 units/pd2_mcmansion/props/mcm_prop_garden_bush_green_b_var1/003 (-50.0, -2375.0, -25.0)
	100472 units/pd2_mcmansion/props/mcm_prop_garden_bush_green_b_var1/004 (200.0, -2375.0, -25.0)
	100526 units/pd2_mcmansion/props/mcm_prop_garden_bush_green_b_var2/001 (-100.001, -1450.0, -10.0)
	100527 units/pd2_mcmansion/props/mcm_prop_garden_bush_green_b_var2/002 (-100.001, -1950.0, -10.0)
	100528 units/pd2_mcmansion/props/mcm_prop_garden_bush_green_b_var2/003 (-100.001, -1700.0, -10.0)
	100529 units/pd2_mcmansion/props/mcm_prop_garden_bush_green_b_var2/004 (-100.001, -1200.0, -10.0)
	100530 units/pd2_mcmansion/props/mcm_prop_garden_bush_green_b_var2/005 (-100.001, -950.0, -10.0)
	100531 units/pd2_mcmansion/props/mcm_prop_garden_bush_green_b_var2/006 (-99.9996, -700.0, -10.0)
	100532 units/pd2_mcmansion/props/mcm_prop_garden_bush_green_b_var2/007 (-99.9995, -450.0, -10.0)
	100533 units/pd2_mcmansion/props/mcm_prop_garden_bush_green_b_var2/008 (-99.9993, -200.0, -10.0)
	100114 units/world/architecture/hospital/props/rooftop/acunit/acunit01/001 (2800.0, -2500.0, 485.705)
	100115 units/world/architecture/hospital/props/rooftop/acunit/acunit02/001 (3237.0, -2723.0, 656.054)
	100116 units/world/architecture/hospital/props/rooftop/acunit/acunit02/002 (3786.0, -2723.0, 656.054)
	100117 units/world/architecture/hospital/props/rooftop/vent/roof_vent001/001 (3461.0, -3120.0, 464.963)
		disable_shadows True
	100118 units/world/architecture/hospital/props/rooftop/vent/roof_vent001/002 (3890.0, -2174.0, 896.963)
		disable_shadows True
	100119 units/world/architecture/hospital/props/rooftop/vent/roof_vent001/003 (3890.0, -1324.0, 896.963)
		disable_shadows True
	100120 units/world/architecture/hospital/props/rooftop/vent/roof_vent001/004 (3890.0, -474.0, 896.963)
		disable_shadows True
	100121 units/world/architecture/hospital/props/rooftop/vent/roof_vent001/005 (3890.0, 326.0, 896.963)
		disable_shadows True
	100125 units/world/architecture/mansion/exterior/man_drainpipe_ext_medium/001 (2573.15, -990.925, 38.8608)
		disable_shadows True
	100126 units/world/architecture/mansion/exterior/man_drainpipe_ext_medium/002 (2573.15, -290.925, 38.8608)
		disable_shadows True
	100164 units/world/props/signs/signs/construction_sign_hardhat/001 (2101.0, 289.0, 150.721)
