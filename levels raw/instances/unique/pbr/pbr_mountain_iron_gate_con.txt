ID range vs continent name:
	100000: world

statics
	100020 units/dev_tools/level_tools/dev_nav_blocker_1x3m/005 (162.614, 66.9907, 0.0)
		disable_shadows True
	100021 units/dev_tools/level_tools/dev_nav_blocker_1x3m/006 (157.382, -7.82659, 0.0)
		disable_shadows True
	100022 units/dev_tools/level_tools/dev_nav_blocker_1x3m/007 (-183.579, -6.71067, 0.0)
		disable_shadows True
	100029 units/dev_tools/level_tools/dev_nav_blocker_1x3m/008 (-191.418, 67.8785, 0.0)
		disable_shadows True
	100068 units/dev_tools/level_tools/dev_nav_blocker_1x3m/009 (-179.0, -6.00003, 0.0)
		disable_shadows True
	100069 units/dev_tools/level_tools/dev_nav_blocker_1x3m/010 (-279.0, -6.00004, 0.0)
		disable_shadows True
	100070 units/dev_tools/level_tools/dev_nav_blocker_1x3m/011 (271.0, -5.99998, 0.0)
		disable_shadows True
	100071 units/dev_tools/level_tools/dev_nav_blocker_1x3m/012 (371.0, -5.99997, 0.0)
		disable_shadows True
	100072 units/dev_tools/level_tools/dev_nav_blocker_1x3m/013 (396.0, -5.99996, 0.0)
		disable_shadows True
	100073 units/dev_tools/level_tools/dev_nav_blocker_1x3m/014 (-329.0, -6.00005, 0.0)
		disable_shadows True
	100015 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/extra (-275.0, 0.0, 0.0)
		disable_shadows True
	100016 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/extra (-300.0, 0.0, 0.0)
		disable_shadows True
	100017 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/extra001 (-275.0, 0.0, 400.0)
		disable_shadows True
	100018 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/extra002 (-300.0, 0.0, 400.0)
		disable_shadows True
	100030 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/extra003 (-449.0, -1.0, 196.0)
		disable_shadows True
	100031 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/extra004 (-449.0, -1.0, 100.0)
		disable_shadows True
	100058 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/extra005 (449.0, 0.0, 196.0)
		disable_shadows True
	100059 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/extra006 (449.0, 0.0, 100.0)
		disable_shadows True
	100060 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/extra007 (275.0, -1.0, 0.0)
		disable_shadows True
	100061 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/extra008 (300.0, -1.0, 0.0)
		disable_shadows True
	100062 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/extra009 (275.0, -1.0, 400.0)
		disable_shadows True
	100063 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/extra010 (300.0, -1.0, 400.0)
		disable_shadows True
	100064 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/extra011 (200.0, -1.0, 325.0)
		disable_shadows True
	100065 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/extra012 (-200.0, -1.00012, 325.0)
		disable_shadows True
	100066 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/extra013 (200.0, -1.0, 325.0)
		disable_shadows True
	100067 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/extra014 (-200.0, -1.00012, 325.0)
		disable_shadows True
	100054 units/pd2_dlc_berry/props/bry_prop_mountain_gate/001 (0.0, 0.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
