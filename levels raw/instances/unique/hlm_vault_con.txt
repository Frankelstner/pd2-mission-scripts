ID range vs continent name:
	100000: world

statics
	100177 units/dev_tools/level_tools/door_blocker_1m/001 (-200.0, 0.0, 0.0)
		disable_on_ai_graph True
	100162 units/dev_tools/level_tools/door_blocker_1m/002 (-175.0, -150.0, 0.0)
	100163 units/dev_tools/level_tools/door_blocker_1m/003 (-150.0, -175.0, 0.0)
	100164 units/dev_tools/level_tools/door_blocker_1m/004 (-50.0, -175.0, 0.0)
	100165 units/dev_tools/level_tools/door_blocker_1m/005 (50.0, -175.0, 0.0)
	100166 units/dev_tools/level_tools/door_blocker_1m/006 (150.0, -175.0, 0.0)
	100168 units/dev_tools/level_tools/door_blocker_1m/008 (175.0, -150.0, 0.0)
	100169 units/dev_tools/level_tools/door_blocker_1m/009 (175.0, -50.0, 0.0)
	100170 units/dev_tools/level_tools/door_blocker_1m/010 (175.0, 50.0, 0.0)
	100171 units/dev_tools/level_tools/door_blocker_1m/011 (175.0, 150.0, 0.0)
	100172 units/dev_tools/level_tools/door_blocker_1m/012 (150.0, 175.0, 0.0)
	100173 units/dev_tools/level_tools/door_blocker_1m/013 (50.0, 175.0, 0.0)
	100174 units/dev_tools/level_tools/door_blocker_1m/014 (-50.0, 175.0, 0.0)
	100175 units/dev_tools/level_tools/door_blocker_1m/015 (-150.0, 175.0, 0.0)
	100176 units/dev_tools/level_tools/door_blocker_1m/016 (-175.0, 150.0, 0.0)
	100006 units/dev_tools/level_tools/door_blocker_1m/017 (-148.0, 99.0, 0.0)
	100007 units/dev_tools/level_tools/door_blocker_1m/018 (-100.0, 150.0, 0.0)
	100008 units/dev_tools/level_tools/door_blocker_1m/019 (0.0, 150.0, 0.0)
	100009 units/dev_tools/level_tools/door_blocker_1m/020 (100.0, 150.0, 0.0)
	100010 units/dev_tools/level_tools/door_blocker_1m/021 (67.0, 79.0, 0.0)
	100011 units/dev_tools/level_tools/door_blocker_1m/022 (67.0, -21.0, 0.0)
	100012 units/dev_tools/level_tools/door_blocker_1m/023 (76.0, -71.0, 0.0)
	100013 units/dev_tools/level_tools/door_blocker_1m/024 (16.0, -131.0, 0.0)
	100014 units/dev_tools/level_tools/door_blocker_1m/025 (-13.0, -160.0, 0.0)
	100015 units/dev_tools/level_tools/door_blocker_1m/026 (-125.0, -128.0, 0.0)
	100019 units/dev_tools/level_tools/door_blocker_1m/027 (-294.0, -92.0, 0.0)
	100147 units/payday2/architecture/res_ext_apartment_vault/001 (25.0, 0.0, 0.0)
	100030 units/payday2/pickups/gen_pku_money/001 (134.0, -128.0, 12.0)
		mesh_variation disable_interaction
	100031 units/payday2/pickups/gen_pku_money/002 (138.0, -25.0, 12.0)
		mesh_variation disable_interaction
	100032 units/payday2/pickups/gen_pku_money/003 (138.0, -78.0, 12.0)
		mesh_variation disable_interaction
	100033 units/payday2/pickups/gen_pku_money/004 (138.0, -19.0, 144.0)
		mesh_variation disable_interaction
	100034 units/payday2/pickups/gen_pku_money/005 (138.0, -69.0, 144.0)
		mesh_variation disable_interaction
	100035 units/payday2/pickups/gen_pku_money/006 (138.0, -125.0, 78.0)
		mesh_variation disable_interaction
	100036 units/payday2/pickups/gen_pku_money/007 (146.0, 101.0, 0.999999)
		mesh_variation disable_interaction
	100037 units/payday2/pickups/gen_pku_money/008 (138.0, -125.0, 144.0)
		mesh_variation disable_interaction
	100038 units/payday2/pickups/gen_pku_money/009 (138.0, -130.0, 200.0)
		mesh_variation disable_interaction
	100039 units/payday2/pickups/gen_pku_money/010 (138.0, -79.0, 200.0)
		mesh_variation disable_interaction
	100040 units/payday2/pickups/gen_pku_money/011 (24.165, -144.058, 0.0)
		mesh_variation disable_interaction
	100041 units/payday2/pickups/gen_pku_money/012 (75.0, -93.0, 0.0)
		mesh_variation disable_interaction
	100046 units/payday2/pickups/gen_pku_money/013 (75.0, -150.0, 0.0)
		mesh_variation disable_interaction
	100049 units/payday2/pickups/gen_pku_money/014 (138.0, -25.0, 200.0)
		mesh_variation disable_interaction
	100053 units/payday2/pickups/gen_pku_money/015 (138.0, -25.0, 78.0)
		mesh_variation disable_interaction
	100054 units/payday2/pickups/gen_pku_money/016 (138.0, -75.0, 78.0)
		mesh_variation disable_interaction
	100001 units/payday2/props/bnk_prop_vault_door_light_steel_wall/001 (-200.0, 200.0, 0.0)
		mesh_variation state_opened
	100061 units/payday2/props/gen_prop_square_drop_marker_2x3/001 (-325.0, 0.0, 0.0)
	100000 units/payday2/props/off_prop_generic_filecabinet_d/001 (-50.0, 175.0, -7.45058e-07)
		mesh_variation grey
	100002 units/payday2/props/off_prop_generic_filecabinet_d/002 (35.0, 175.0, -7.45058e-07)
		mesh_variation grey
	100003 units/payday2/props/off_prop_generic_filecabinet_d/003 (121.0, 175.0, -7.45058e-07)
		mesh_variation grey
	100005 units/payday2/props/res_prop_kitchen_chair_a_worn_grime/001 (153.0, 71.0, -7.45058e-07)
	100004 units/pd2_dlc2/csgo_models/props/cs_office/shelves_metal/001 (138.0, -75.0, -7.45058e-07)
