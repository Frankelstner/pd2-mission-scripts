ID range vs continent name:
	100000: world

statics
	100013 units/payday2/props/signs/gen_prop_security_sign_6/001 (-6.99997, -106.0, 202.238)
	100015 units/pd2_mcmansion/props/house/mcm_prop_window/001 (5.96046e-06, -50.0, -2.0)
		mesh_variation interact_enable
	100011 units/pd2_mcmansion/props/mcm_prop_small_alarm/001 (-5.0, -108.0, 145.291)
		mesh_variation blink_on
	100002 units/world/architecture/secret_stash/props/secret_stash_planks_outline_no_sunray_mcmansion/001 (27.0, -50.0, 98.0)
		mesh_variation hide
