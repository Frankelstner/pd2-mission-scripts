﻿´startup´ MissionScriptElement 100004
	EXECUTE ON STARTUP
	on_executed
		´seq_hide_drill´ (delay 1)
´seq_hide_drill´ ElementUnitSequence 100005
	position -300.0, 150.0, 75.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-77.0, -6.0, 84.0)
			notify_unit_sequence state_hide
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/002 (-92.0, -1.00001, 84.0)
			notify_unit_sequence state_hide
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-77.0, -6.0, 84.0)
			notify_unit_sequence deactivate
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/002 (-92.0, -1.00001, 84.0)
			notify_unit_sequence deactivate
			time 0
		5
			id 5
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/environment/cas_int/cas_int_fence_door/001 (0.0, 0.0, 0.0)
			notify_unit_sequence state_door_open
			time 0
´trg_seq_drill_01_placed´ ElementUnitSequenceTrigger 100007
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-77.0, -6.0, 84.0)
	on_executed
		´seq_hide_drill_2´ (delay 0)
		´toggle_on´ (delay 0)
		´func_sequence_004´ (delay 0) DISABLED
´trg_seq_drill_02_placed´ ElementUnitSequenceTrigger 100008
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/002 (-92.0, -1.00001, 84.0)
	on_executed
		´seq_hide_drill_3´ (delay 0)
		´toggle_on´ (delay 0)
		´func_sequence_004´ (delay 0) DISABLED
´seq_hide_drill_2´ ElementUnitSequence 100009
	position -300.0, 250.0, 75.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/002 (-92.0, -1.00001, 84.0)
			notify_unit_sequence state_hide
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/002 (-92.0, -1.00001, 84.0)
			notify_unit_sequence deactivate
			time 0
´seq_hide_drill_3´ ElementUnitSequence 100010
	position -300.0, 350.0, 75.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-77.0, -6.0, 84.0)
			notify_unit_sequence state_hide
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-77.0, -6.0, 84.0)
			notify_unit_sequence deactivate
			time 0
´trg_seq_drill_done´ ElementUnitSequenceTrigger 100011
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence timer_done
			unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/002 (-92.0, -1.00001, 84.0)
		2
			guis_id 2
			sequence timer_done
			unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-77.0, -6.0, 84.0)
	on_executed
		´seq_door_open´ (delay 0)
		´door_blocker_remove´ (delay 0)
´seq_door_open´ ElementUnitSequence 100012
	TRIGGER TIMES 1
	position -300.0, 450.0, 75.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/environment/cas_int/cas_int_fence_door/001 (0.0, 0.0, 0.0)
			notify_unit_sequence open_door
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-77.0, -6.0, 84.0)
			notify_unit_sequence state_hide
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/002 (-92.0, -1.00001, 84.0)
			notify_unit_sequence state_hide
			time 0
		4
			id 6
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-77.0, -6.0, 84.0)
			notify_unit_sequence deactivate
			time 0
		5
			id 7
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/002 (-92.0, -1.00001, 84.0)
			notify_unit_sequence deactivate
			time 0
		6
			id 8
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-77.0, -6.0, 84.0)
			notify_unit_sequence state_attention_disabled
			time 0
		7
			id 9
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/002 (-92.0, -1.00001, 84.0)
			notify_unit_sequence state_attention_disabled
			time 0
´trg_seq_door_open_saw´ ElementUnitSequenceTrigger 100013
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence open_door_with_saw
			unit_id units/pd2_dlc_casino/environment/cas_int/cas_int_fence_door/001 (0.0, 0.0, 0.0)
	on_executed
		´seq_drill_hide´ (delay 0)
		´door_blocker_remove´ (delay 0)
		´seq_power_off_drill´ (delay 0) DISABLED
´input_close_door´ ElementInstanceInput 100003
	event input_close_door
	on_executed
		´seq_close_door´ (delay 0)
		´door_blocker_add´ (delay 0)
´seq_close_door´ ElementUnitSequence 100014
	position -300.0, 550.0, 75.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/environment/cas_int/cas_int_fence_door/001 (0.0, 0.0, 0.0)
			notify_unit_sequence close_door
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/environment/cas_int/cas_int_fence_door/001 (0.0, 0.0, 0.0)
			notify_unit_sequence activate_door
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-77.0, -6.0, 84.0)
			notify_unit_sequence state_show
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/002 (-92.0, -1.00001, 84.0)
			notify_unit_sequence state_show
			time 0
		5
			id 5
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-77.0, -6.0, 84.0)
			notify_unit_sequence activate
			time 0
		6
			id 6
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/002 (-92.0, -1.00001, 84.0)
			notify_unit_sequence activate
			time 0
		7
			id 7
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-77.0, -6.0, 84.0)
			notify_unit_sequence set_hack_time_state_3
			time 0
		8
			id 8
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/002 (-92.0, -1.00001, 84.0)
			notify_unit_sequence set_hack_time_state_3
			time 0
	on_executed
		´seq_enable_door´ (delay 5)
´trg_seq_c4_placed´ ElementUnitSequenceTrigger 100015
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence all_c4_placed
			unit_id units/pd2_dlc_casino/environment/cas_int/cas_int_fence_door/001 (0.0, 0.0, 0.0)
	on_executed
		´seq_hide_drill_2´ (delay 0)
		´seq_drill_hide´ (delay 0)
		´door_blocker_remove´ (delay 0)
		´seq_power_off_drill´ (delay 0) DISABLED
´seq_drill_hide´ ElementUnitSequence 100016
	position -300.0, 650.0, 75.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-77.0, -6.0, 84.0)
			notify_unit_sequence state_hide
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/002 (-92.0, -1.00001, 84.0)
			notify_unit_sequence state_hide
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-77.0, -6.0, 84.0)
			notify_unit_sequence deactivate
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/002 (-92.0, -1.00001, 84.0)
			notify_unit_sequence deactivate
			time 0
		5
			id 5
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-77.0, -6.0, 84.0)
			notify_unit_sequence state_attention_disabled
			time 0
		6
			id 6
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/002 (-92.0, -1.00001, 84.0)
			notify_unit_sequence state_attention_disabled
			time 0
´door_blocker_add´ ElementNavObstacle 100017
	TRIGGER TIMES 1
	obstacle_list
		1
			guis_id 1
			obj_name 6238b94f58205d1a
			unit_id units/dev_tools/level_tools/dev_door_blocker_1x2x3/001 (50.0, -25.0, 0.0)
	operation add
	position -50.0, 275.0, 75.0
	rotation 0.0, 0.0, 0.0, -1.0
´door_blocker_remove´ ElementNavObstacle 100018
	TRIGGER TIMES 1
	obstacle_list
		1
			guis_id 1
			obj_name 6238b94f58205d1a
			unit_id units/dev_tools/level_tools/dev_door_blocker_1x2x3/001 (50.0, -25.0, 0.0)
	operation remove
	position 75.0, 275.0, 75.0
	rotation 0.0, 0.0, 0.0, -1.0
´seq_power_off_drill´ ElementUnitSequence 100020
	DISABLED
	position -300.0, 750.0, 75.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-77.0, -6.0, 84.0)
			notify_unit_sequence power_off
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/002 (-92.0, -1.00001, 84.0)
			notify_unit_sequence power_off
			time 0
´toggle_on´ ElementToggle 100021
	elements
		1 ´seq_power_off_drill´ DISABLED
	set_trigger_times -1
	toggle on
´seq_enable_door´ ElementUnitSequence 100022
	position -375.0, 550.0, 75.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_casino/environment/cas_int/cas_int_fence_door/001 (0.0, 0.0, 0.0)
			notify_unit_sequence interaction_enabled
			time 0
´seq_trg_door_interact´ ElementUnitSequenceTrigger 100023
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/pd2_dlc_casino/environment/cas_int/cas_int_fence_door/001 (0.0, 0.0, 0.0)
	on_executed
		´logic_chance_001´ (delay 0)
´logic_chance_001´ ElementLogicChance 100024
	chance 50
	position -600.0, 75.0, 75.0
	rotation 0.0, 0.0, 0.0, -1.0
´logic_chance_trigger_001´ ElementLogicChanceTrigger 100025
	elements
		1 ´logic_chance_001´
	outcome success
	position -600.0, 175.0, 75.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´seq_door_open´ (delay 0)
		´seq_power_off_drill´ (delay 0) DISABLED
´logic_chance_trigger_002´ ElementLogicChanceTrigger 100026
	elements
		1 ´logic_chance_001´
	outcome fail
	position -700.0, 75.0, 75.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´seq_enable_door´ (delay 0)
		´logic_chance_operator_001´ (delay 0)
´logic_chance_operator_001´ ElementLogicChanceOperator 100027
	chance 10
	elements
		1 ´logic_chance_001´
	operation add_chance
	position -800.0, 75.0, 75.0
	rotation 0.0, 0.0, 0.0, -1.0
´input_disable_attention´ ElementInstanceInput 100029
	event disable_drill_attention
	on_executed
		´logic_toggle_001´ (delay 0)
´logic_toggle_001´ ElementToggle 100030
	elements
		1 ´func_sequence_004´ DISABLED
	set_trigger_times -1
	toggle on
´func_sequence_004´ ElementUnitSequence 100006
	DISABLED
	position -375.0, 300.0, 75.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-77.0, -6.0, 84.0)
			notify_unit_sequence state_attention_disabled
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/002 (-92.0, -1.00001, 84.0)
			notify_unit_sequence state_attention_disabled
			time 0
