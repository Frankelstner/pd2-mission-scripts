ID range vs continent name:
	100000: world

statics
	100007 core/units/light_omni/001 (-2061.0, -4546.0, 9325.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 1.0
			enabled True
			falloff_exponent 1
			far_range 200
			multiplier identity
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100008 core/units/light_omni/002 (-2050.0, -4450.0, 9325.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 1.0
			enabled True
			falloff_exponent 1
			far_range 200
			multiplier identity
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100011 core/units/light_omni/004 (-350.0, -664.0, 202.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.811765, 0.913726, 0.996078
			enabled True
			falloff_exponent 1
			far_range 400
			multiplier streetlight
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100067 core/units/light_omni/006 (50.0, -648.0, 225.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.788235, 0.866667, 0.996078
			enabled True
			falloff_exponent 1
			far_range 400
			multiplier flashlight
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100010 core/units/light_omni/010 (-420.0, -240.0, 239.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.690196, 0.803922, 0.992157
			enabled True
			falloff_exponent 1
			far_range 500
			multiplier match
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100068 core/units/light_omni/011 (71.0, -145.0, 239.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.690196, 0.803922, 0.992157
			enabled True
			falloff_exponent 1
			far_range 400
			multiplier match
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100107 units/dev_tools/level_tools/ai_coverpoint/001 (-150.0, -100.0, 0.0)
	100108 units/dev_tools/level_tools/ai_coverpoint/002 (50.0, -200.0, 0.0)
	100030 units/dev_tools/level_tools/dev_bag_collision_1x3m/001 (-87.0, -809.0, 300.0)
	100019 units/dev_tools/level_tools/dev_bag_collision_1x3m/002 (202.0, -809.0, 300.0)
	100022 units/dev_tools/level_tools/dev_bag_collision_1x3m/003 (250.0, -809.0, 300.0)
	100109 units/dev_tools/level_tools/dev_bag_collision_1x3m/004 (-223.0, -659.0, 300.0)
	100110 units/dev_tools/level_tools/dev_bag_collision_1x3m/005 (-223.0, -659.0, 300.0)
	100129 units/dev_tools/level_tools/dev_bag_collision_1x3m/006 (-223.0, -759.0, 300.0)
	100130 units/dev_tools/level_tools/dev_bag_collision_1x3m/007 (-323.0, -659.0, 325.0)
	100132 units/dev_tools/level_tools/dev_bag_collision_1x3m/008 (-423.0, -659.0, 300.0)
	100021 units/dev_tools/level_tools/dev_bag_collision_8x3m/001 (-885.0, -809.0, 0.0)
	100309 units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-199.999, -74.9975, 0.00457764)
		disable_on_ai_graph True
	100310 units/dev_tools/level_tools/dev_door_blocker_1x1x3/002 (-299.999, -74.9977, 0.00457764)
		disable_on_ai_graph True
	100311 units/dev_tools/level_tools/dev_door_blocker_1x1x3/003 (-399.999, -74.9978, 0.00457764)
		disable_on_ai_graph True
	100312 units/dev_tools/level_tools/dev_door_blocker_1x1x3/004 (-499.999, -74.9979, 0.00457764)
		disable_on_ai_graph True
	100313 units/dev_tools/level_tools/dev_door_blocker_1x1x3/005 (-574.999, -74.998, 0.00457764)
		disable_on_ai_graph True
	100314 units/dev_tools/level_tools/dev_door_blocker_1x1x3/006 (-574.999, -549.998, 0.0037486)
		disable_on_ai_graph True
	100315 units/dev_tools/level_tools/dev_door_blocker_1x1x3/007 (-549.999, -474.998, 0.0038795)
		disable_on_ai_graph True
	100316 units/dev_tools/level_tools/dev_door_blocker_1x1x3/008 (-549.999, -374.998, 0.00405404)
		disable_on_ai_graph True
	100317 units/dev_tools/level_tools/dev_door_blocker_1x1x3/009 (-574.999, -274.998, 0.00422857)
		disable_on_ai_graph True
	100318 units/dev_tools/level_tools/dev_door_blocker_1x1x3/010 (-574.999, -174.998, 0.0044031)
		disable_on_ai_graph True
	100319 units/dev_tools/level_tools/dev_door_blocker_1x1x3/011 (125.002, -849.997, 0.003225)
		disable_on_ai_graph True
	100320 units/dev_tools/level_tools/dev_door_blocker_1x1x3/012 (25.0019, -849.997, 0.003225)
		disable_on_ai_graph True
	100321 units/dev_tools/level_tools/dev_door_blocker_1x1x3/013 (-74.9981, -849.997, 0.003225)
		disable_on_ai_graph True
	100322 units/dev_tools/level_tools/dev_door_blocker_1x1x3/014 (-174.998, -849.997, 0.003225)
		disable_on_ai_graph True
	100323 units/dev_tools/level_tools/dev_door_blocker_1x1x3/015 (-274.998, -849.997, 0.003225)
		disable_on_ai_graph True
	100324 units/dev_tools/level_tools/dev_door_blocker_1x1x3/016 (-374.998, -849.998, 0.003225)
		disable_on_ai_graph True
	100325 units/dev_tools/level_tools/dev_door_blocker_1x1x3/017 (-474.998, -849.998, 0.003225)
		disable_on_ai_graph True
	100326 units/dev_tools/level_tools/dev_door_blocker_1x1x3/018 (-574.998, -849.998, 0.003225)
		disable_on_ai_graph True
	100327 units/dev_tools/level_tools/dev_door_blocker_1x1x3/019 (-574.998, -749.998, 0.00339954)
		disable_on_ai_graph True
	100328 units/dev_tools/level_tools/dev_door_blocker_1x1x3/020 (-574.998, -649.998, 0.00357407)
		disable_on_ai_graph True
	100329 units/dev_tools/level_tools/dev_door_blocker_1x1x3/021 (175.001, -74.9969, 0.00457764)
		disable_on_ai_graph True
	100330 units/dev_tools/level_tools/dev_door_blocker_1x1x3/022 (275.001, -99.9967, 0.004534)
		disable_on_ai_graph True
	100331 units/dev_tools/level_tools/dev_door_blocker_1x1x3/023 (275.001, -149.997, 0.00444674)
		disable_on_ai_graph True
	100332 units/dev_tools/level_tools/dev_door_blocker_1x1x3/024 (275.001, -249.997, 0.0042722)
		disable_on_ai_graph True
	100333 units/dev_tools/level_tools/dev_door_blocker_1x1x3/025 (275.001, -349.997, 0.00409767)
		disable_on_ai_graph True
	100334 units/dev_tools/level_tools/dev_door_blocker_1x1x3/026 (275.001, -449.997, 0.00392314)
		disable_on_ai_graph True
	100335 units/dev_tools/level_tools/dev_door_blocker_1x1x3/027 (275.001, -549.997, 0.00374861)
		disable_on_ai_graph True
	100336 units/dev_tools/level_tools/dev_door_blocker_1x1x3/028 (275.002, -649.997, 0.00357407)
		disable_on_ai_graph True
	100337 units/dev_tools/level_tools/dev_door_blocker_1x1x3/029 (275.002, -749.997, 0.00339954)
		disable_on_ai_graph True
	100338 units/dev_tools/level_tools/dev_door_blocker_1x1x3/030 (225.002, -849.997, 0.00322501)
		disable_on_ai_graph True
	100342 units/dev_tools/level_tools/dev_door_blocker_1x1x3/034 (-324.998, -574.997, 0.00370497)
		disable_on_ai_graph True
	100343 units/dev_tools/level_tools/dev_door_blocker_1x1x3/035 (-399.998, -674.997, 0.00353044)
		disable_on_ai_graph True
	100344 units/dev_tools/level_tools/dev_door_blocker_1x1x3/036 (-274.998, -674.997, 0.00353044)
		disable_on_ai_graph True
	100345 units/dev_tools/level_tools/dev_door_blocker_1x1x3/037 (-174.998, -674.997, 0.00353044)
		disable_on_ai_graph True
	100346 units/dev_tools/level_tools/dev_door_blocker_1x1x3/038 (-324.998, -749.997, 0.00339954)
		disable_on_ai_graph True
	100347 units/dev_tools/level_tools/dev_door_blocker_1x1x3/039 (-174.998, -749.997, 0.00339954)
		disable_on_ai_graph True
	100348 units/dev_tools/level_tools/dev_door_blocker_1x1x3/040 (150.001, -74.9969, 0.00457764)
		disable_on_ai_graph True
	100154 units/dev_tools/level_tools/dev_nav_blocker_8x3m/001 (-25.0, -800.0, 0.0)
	100157 units/dev_tools/level_tools/dev_nav_blocker_8x3m/002 (-325.0, -800.0, 0.0)
	100160 units/dev_tools/level_tools/dev_nav_blocker_8x3m/003 (-625.0, -800.0, 0.0)
	100134 units/payday2/architecture/ind/level/ind_ext_decal_04/001 (104.001, -635.998, 0.999146)
	100139 units/payday2/architecture/ind/level/ind_ext_decal_04/002 (-446.384, -510.238, 0.99939)
	100137 units/payday2/architecture/ind/level/ind_ext_decal_mul_a/002 (-846.0, -764.0, 2.0)
	100138 units/payday2/architecture/ind/level/ind_ext_decal_mul_a/003 (-846.0, -39.0, 2.0)
	100213 units/payday2/architecture/ind/level/ind_ext_decal_mul_a/004 (-543.0, 33.0, 2.0)
	100214 units/payday2/architecture/ind/level/ind_ext_decal_mul_a/005 (-298.0, 33.0, 2.0)
	100215 units/payday2/architecture/ind/level/ind_ext_decal_mul_a/006 (-232.0, -615.0, 2.0)
	100112 units/payday2/architecture/ind/level/ind_ext_fence_1m/001 (-650.0, -799.999, -0.00120354)
		disable_on_ai_graph True
	100117 units/payday2/architecture/ind/level/ind_ext_fence_1m/002 (-650.0, -95.998, 0.0)
		disable_on_ai_graph True
	100119 units/payday2/architecture/ind/level/ind_ext_fence_1m/003 (-825.0, -399.999, -0.00050354)
		disable_on_ai_graph True
	100122 units/payday2/architecture/ind/level/ind_ext_fence_1m1m/001 (-650.0, -799.999, 199.999)
		disable_on_ai_graph True
	100012 units/payday2/architecture/ind/level/ind_ext_fence_1m1m/002 (-650.0, -95.9984, 200.0)
	100127 units/payday2/architecture/ind/level/ind_ext_fence_1m1m/006 (-825.0, -399.999, 199.999)
		disable_on_ai_graph True
	100120 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door/001 (-650.0, -698.999, 199.999)
		disable_on_ai_graph True
	100072 units/payday2/architecture/ind/level/ind_ext_fence_2m1m_shorter/001 (-650.0, -414.0, 199.999)
		disable_on_ai_graph True
	100073 units/payday2/architecture/ind/level/ind_ext_fence_2m1m_shorter/002 (-650.001, -225.0, 200.0)
		disable_on_ai_graph True
	100014 units/payday2/architecture/ind/level/ind_ext_fence_2m1m_shorter/003 (-650.001, -400.0, 200.0)
	100114 units/payday2/architecture/ind/level/ind_ext_fence_2m_shorter/001 (-650.0, -413.999, -0.000578498)
		disable_on_ai_graph True
	100115 units/payday2/architecture/ind/level/ind_ext_fence_2m_shorter/002 (-650.001, -224.999, -0.000244141)
		disable_on_ai_graph True
	100118 units/payday2/architecture/ind/level/ind_ext_fence_2m_shorter/003 (-650.0, -399.999, -0.00050354)
		disable_on_ai_graph True
	100076 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/001 (-650.0, -581.999, -0.00082119)
		disable_on_ai_graph True
	100016 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/002 (-650.0, -698.999, -0.00102539)
		disable_on_ai_graph True
	100071 units/payday2/architecture/ind/level/ind_ext_fence_pole_2m/003 (-925.0, -399.999, -0.00050354)
		disable_on_ai_graph True
	100170 units/payday2/architecture/ind/level/ind_ext_stripes_03_4m/001 (0.000976563, -774.998, -0.00115991)
		disable_on_ai_graph True
	100174 units/payday2/architecture/ind/level/ind_ext_stripes_03_4m/002 (-424.999, -774.999, -0.00115991)
		disable_on_ai_graph True
	100001 units/payday2/architecture/pox/com_int_parking_garage_door/garage_door (-200.0, -824.998, -6.66016)
		disable_on_ai_graph True
		mesh_variation close_doors
	100224 units/payday2/mockup/occluder_plane_4x8/001 (400.0, 3.0, 0.0)
	100226 units/payday2/mockup/occluder_plane_4x8/002 (-3.79654e-06, 3.0, 0.0)
	100135 units/payday2/props/com_prop_club_beer_keg/001 (225.0, -74.998, -3.04634e-05)
		disable_on_ai_graph True
	100136 units/payday2/props/com_prop_club_beer_keg/002 (-358.999, -661.998, 16.999)
		disable_on_ai_graph True
	100091 units/payday2/props/com_prop_club_beer_keg/003 (-358.999, -618.998, 16.9991)
		disable_on_ai_graph True
	100063 units/payday2/props/com_prop_club_beer_keg/004 (225.0, -114.998, -0.000101039)
		disable_on_ai_graph True
	100009 units/payday2/props/com_prop_club_beer_keg/005 (221.5, -88.9359, 50.9999)
		disable_on_ai_graph True
	100123 units/payday2/props/com_prop_club_beer_keg/006 (-345.999, -578.998, 16.9992)
		disable_on_ai_graph True
	100185 units/payday2/props/com_prop_club_beer_keg/007 (225.0, -349.998, -0.000511192)
		disable_on_ai_graph True
	100186 units/payday2/props/com_prop_club_beer_keg/008 (225.0, -311.998, -0.000444869)
		disable_on_ai_graph True
	100124 units/payday2/props/com_prop_club_beer_keg/009 (-392.999, -618.998, 16.9991)
		disable_on_ai_graph True
	100125 units/payday2/props/com_prop_club_beer_keg/010 (-386.999, -573.998, 16.9992)
		disable_on_ai_graph True
	100187 units/payday2/props/com_prop_club_beer_keg/011 (225.0, -334.998, 50.9995)
		disable_on_ai_graph True
	100131 units/payday2/props/com_prop_club_beer_keg/012 (-287.999, -605.998, -0.000862122)
		disable_on_ai_graph True
	100101 units/payday2/props/com_prop_janitor_shelf/001 (-425.0, -53.0, 0.0)
		disable_on_ai_graph True
	100102 units/payday2/props/com_prop_janitor_shelf/002 (-200.0, -53.0, 0.0)
		disable_on_ai_graph True
	100060 units/payday2/props/com_prop_janitor_shelf/003 (248.0, -53.0, 0.0)
		disable_on_ai_graph True
	100143 units/payday2/props/gen_prop_long_lamp_broken/001 (-351.0, -139.0, 312.0)
	100084 units/payday2/props/gen_prop_long_lamp_broken/002 (66.0, -139.0, 312.0)
	100013 units/payday2/props/gen_prop_long_lamp_broken/003 (-809.0, -139.0, 312.0)
	100015 units/payday2/props/gen_prop_long_lamp_broken/004 (-809.0, -664.0, 312.0)
	100080 units/payday2/props/gen_prop_long_lamp_v2/002 (-370.0, -660.999, 310.999)
	100081 units/payday2/props/gen_prop_long_lamp_v2/003 (49.9995, -660.999, 310.999)
	100065 units/payday2/props/ind_prop_warehouse_pallettruck/001 (-417.0, -614.0, -1.0)
		disable_on_ai_graph True
	100103 units/payday2/props/lcm_prop_rooftop_vent/001 (-750.0, -4.99805, 187.0)
		disable_on_ai_graph True
	100140 units/payday2/props/lcm_prop_rooftop_vent/002 (-857.0, -794.998, 180.0)
		disable_on_ai_graph True
	100192 units/payday2/props/set/ind_prop_warehouse_box_a/001 (-469.0, -25.0, 102.0)
		disable_on_ai_graph True
	100193 units/payday2/props/set/ind_prop_warehouse_box_a/002 (-253.0, -32.0, 102.0)
		disable_on_ai_graph True
	100194 units/payday2/props/set/ind_prop_warehouse_box_a/003 (-253.0, -25.0, 117.0)
		disable_on_ai_graph True
	100195 units/payday2/props/set/ind_prop_warehouse_box_a/004 (-253.0, -25.0, 207.0)
		disable_on_ai_graph True
	100196 units/payday2/props/set/ind_prop_warehouse_box_a/005 (-575.0, -25.0, 207.0)
		disable_on_ai_graph True
	100197 units/payday2/props/set/ind_prop_warehouse_box_a/006 (-575.0, -32.0, 48.0)
		disable_on_ai_graph True
	100198 units/payday2/props/set/ind_prop_warehouse_box_a/007 (-252.0, -25.0, 48.0)
		disable_on_ai_graph True
	100199 units/payday2/props/set/ind_prop_warehouse_box_a/008 (100.0, -30.0, 48.0)
		disable_on_ai_graph True
	100200 units/payday2/props/set/ind_prop_warehouse_box_a/009 (100.0, -25.0, 156.0)
		disable_on_ai_graph True
	100201 units/payday2/props/set/ind_prop_warehouse_box_a/010 (100.0, -25.0, 63.0)
		disable_on_ai_graph True
	100211 units/payday2/props/set/ind_prop_warehouse_box_a/011 (-623.263, -518.83, 81.5703)
		disable_on_ai_graph True
	100202 units/payday2/props/set/ind_prop_warehouse_box_b/001 (-476.0, -33.0, 208.0)
		disable_on_ai_graph True
	100203 units/payday2/props/set/ind_prop_warehouse_box_c/001 (-331.0, -27.0, 49.0)
		disable_on_ai_graph True
	100207 units/payday2/props/set/ind_prop_warehouse_box_c/002 (-355.0, -27.0, 1.0)
		disable_on_ai_graph True
	100208 units/payday2/props/set/ind_prop_warehouse_box_c/003 (-486.0, -27.0, 1.0)
		disable_on_ai_graph True
	100217 units/payday2/props/set/ind_prop_warehouse_box_c/004 (83.0, -27.0, 1.0)
		disable_on_ai_graph True
	100218 units/payday2/props/set/ind_prop_warehouse_box_c/005 (184.0, -27.0, 1.0)
		disable_on_ai_graph True
	100204 units/payday2/props/set/ind_prop_warehouse_box_e/001 (-354.0, -25.0, 102.0)
		disable_on_ai_graph True
	100205 units/payday2/props/set/ind_prop_warehouse_box_e/002 (-348.0, -25.0, 156.0)
		disable_on_ai_graph True
	100206 units/payday2/props/set/ind_prop_warehouse_box_e/003 (204.0, -25.0, 156.0)
		disable_on_ai_graph True
	100209 units/payday2/props/set/ind_prop_warehouse_box_e/006 (-576.0, -25.0, 0.0)
		disable_on_ai_graph True
	100210 units/payday2/props/set/ind_prop_warehouse_box_e/007 (-253.0, -25.0, 0.0)
		disable_on_ai_graph True
	100121 units/payday2/props/set/ind_prop_warehouse_pallet/001 (-358.999, -630.998, -0.000907898)
		disable_on_ai_graph True
	100113 units/payday2/props/set/ind_prop_warehouse_pallet/002 (-272.409, -682.669, 79.3824)
		disable_on_ai_graph True
	100126 units/payday2/props/set/ind_prop_warehouse_pallet_stack_c/001 (-850.0, -74.9982, -11.66)
		disable_on_ai_graph True
	100128 units/payday2/props/set/ind_prop_warehouse_pallet_stack_c/002 (-850.0, -749.998, -11.66)
		disable_on_ai_graph True
	100111 units/payday2/props/set/ind_prop_warehouse_pallet_stack_c/003 (-748.0, -909.998, -11.66)
	100153 units/payday2/props/set/ind_prop_warehouse_pallet_stack_c/004 (-190.636, -963.37, 2.49969)
	100104 units/payday2/props/set/ind_prop_warehouse_pallet_stack_c/005 (-519.0, -909.998, -11.66)
	100306 units/payday2/props/str_prop_street_lamppost_single_02_il/006 (-1775.0, -2575.0, -59.0)
	100307 units/payday2/props/str_prop_street_lamppost_single_02_il/007 (-2450.0, -2550.0, -59.0)
	100308 units/payday2/props/str_prop_street_lamppost_single_02_il/008 (-2475.0, -2175.0, -58.0)
	100189 units/payday2/vehicles/str_vehicle_truck/001 (-636.0, -1441.0, -67.0)
		disable_on_ai_graph True
	100025 units/payday2/vehicles/str_vehicle_van_player/001 (9.0, -1080.0, -19.4999)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.861333, 0.584
			enabled True
			falloff_exponent 4
			far_range 150
			multiplier identity
			name lo_omni_1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
		2
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.861, 0.584
			enabled True
			falloff_exponent 4
			far_range 150
			multiplier identity
			name lo_omni_2
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
		mesh_variation state_door_rear_both_open
	100029 units/pd2_dlc_arena/dev/are_gen_square_goal_marker_2_4x4_35/escape_marker (-397.0, -413.0, -6.50023)
	100020 units/pd2_dlc_arena/props/editable_text_caslon/002 (5.99989, 35.0, 261.5)
		align left
		alpha 1
		blend_mode normal
		font fonts/font_caslon
		font_color 1.0, 1.0, 1.0
		font_size 0.64
		render_template diffuse_vc_decal
		shape
			1 0
			2 0
			3 1
			4 1
		text DELIVERY ROOM
		vertical center
		word_wrap False
		wrap False
	100144 units/pd2_dlc_casino/architecture/cas_ext_wall_6m/019 (-975.0, -1400.0, -61.0)
	100158 units/pd2_dlc_casino/architecture/cas_ext_wall_6m/020 (-975.0, -1000.0, -61.0)
	100173 units/pd2_dlc_casino/architecture/cas_ext_wall_6m/021 (-975.0, -1400.0, 539.0)
	100175 units/pd2_dlc_casino/architecture/cas_ext_wall_6m/022 (-975.0, -1000.0, 539.0)
	100163 units/pd2_dlc_casino/architecture/cas_fence_pillar_5m/001 (225.0, -2575.0, -61.0)
	100166 units/pd2_dlc_casino/architecture/cas_fence_pillar_5m/003 (-575.0, -2575.0, -61.0)
	100168 units/pd2_dlc_casino/architecture/cas_fence_pillar_5m/004 (-975.0, -2575.0, -61.0)
	100296 units/pd2_dlc_casino/architecture/cas_fence_pillar_5m/005 (-1775.0, -2575.0, -61.0)
	100298 units/pd2_dlc_casino/architecture/cas_fence_pillar_5m/006 (-1375.0, -2575.0, -61.0)
	100299 units/pd2_dlc_casino/architecture/cas_fence_pillar_5m/007 (-2175.0, -2575.0, -61.0)
	100152 units/pd2_dlc_casino/architecture/cas_fence_pillar_5m/008 (-2450.0, -2550.0, -61.0)
	100301 units/pd2_dlc_casino/architecture/cas_fence_pillar_5m/009 (-2475.0, -2175.0, -61.0)
	100302 units/pd2_dlc_casino/architecture/cas_fence_pillar_5m/010 (-2475.0, -1325.0, -61.0)
	100026 units/pd2_dlc_casino/architecture/cas_fence_pillar_5m/011 (-2475.0, -925.0, -61.0)
	100097 units/pd2_dlc_casino/architecture/cas_fence_pillar_5m/012 (-175.0, -2575.0, -61.0)
	100167 units/pd2_dlc_casino/architecture/cas_fence_solid_4m/003 (-575.0, -2575.0, -61.0)
	100190 units/pd2_dlc_casino/architecture/cas_fence_solid_4m/004 (-975.0, -2575.0, -61.0)
	100191 units/pd2_dlc_casino/architecture/cas_fence_solid_4m/005 (-1775.0, -2575.0, -61.0)
	100297 units/pd2_dlc_casino/architecture/cas_fence_solid_4m/006 (-1375.0, -2575.0, -61.0)
	100155 units/pd2_dlc_casino/architecture/cas_fence_solid_4m/007 (-2175.0, -2575.0, -61.0)
	100300 units/pd2_dlc_casino/architecture/cas_fence_solid_4m/008 (-2475.0, -2575.0, -61.0)
	100303 units/pd2_dlc_casino/architecture/cas_fence_solid_4m/009 (-2475.0, -1325.0, -61.0)
	100000 units/pd2_dlc_casino/architecture/cas_fence_solid_4m/010 (-2475.0, -925.0, -61.0)
	100034 units/pd2_dlc_casino/architecture/cas_fence_solid_4m/011 (225.0, -2575.0, -61.0)
	100035 units/pd2_dlc_casino/architecture/cas_fence_solid_4m/012 (-175.0, -2575.0, -61.0)
	100045 units/pd2_dlc_casino/architecture/cas_storage_room_01/001 (-950.0, 0.0, 0.0)
		disable_on_ai_graph True
	100182 units/pd2_dlc_casino/environment/cas_ext_garage_wall/001 (250.0, -800.0, -61.0)
	100279 units/pd2_dlc_casino/environment/cas_ext_gates/001 (-2475.0, -1750.0, -35.0)
	100156 units/pd2_dlc_casino/environment/cas_ext_gates/002 (-2475.0, -1750.0, -35.0)
	100036 units/pd2_dlc_casino/props/cas_prop_drill_toolbox/drill_parts (-710.858, -64.1421, 1.47032)
		mesh_variation enable_interaction
	100018 units/pd2_dlc_casino/props/hanging_wall/cas_prop_lobby_sign_wall/001 (-50.0001, 25.0, 245.5)
	100098 units/pd2_dlc_cro/ind_ext_fuse_box/001 (250.0, -520.998, 210.0)
		disable_on_ai_graph True
	100075 units/world/props/cables_pipes/prop_pipes_1_b/002 (250.0, 0.0, 300.0)
	100178 units/world/props/cables_pipes/prop_pipes_1_b/003 (250.0, -250.0, 300.0)
	100180 units/world/props/cables_pipes/prop_pipes_1_b/004 (250.0, -500.0, 300.0)
	100181 units/world/props/cables_pipes/prop_pipes_1_b/005 (0.000244141, 0.0, 300.0)
	100077 units/world/props/cables_pipes/prop_pipes_1_b/006 (-250.0, 0.0, 300.0)
	100177 units/world/props/cables_pipes/prop_pipes_1_b/007 (-500.0, 0.0, 300.0)
	100184 units/world/props/cables_pipes/prop_pipes_1_b/008 (-750.0, 0.0, 300.0)
	100212 units/world/props/flickering_light/001 (-324.0, -235.0, 226.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.936507, 0.936507, 0.936507
			enabled True
			falloff_exponent 1
			far_range 250
			multiplier identity
			name lo_light_flicker
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
		mesh_variation flicker_2
		projection_light lo_light_flicker
	100116 units/world/props/mansion/man_cover_int_shipping_crate_wide/001 (-332.0, -757.999, -0.00112915)
		disable_on_ai_graph True
	100069 units/world/props/mansion/man_cover_int_shipping_crate_wide/002 (-332.0, -757.999, 110.999)
		disable_on_ai_graph True
	100141 units/world/props/office/chair/office_chair_detective_noarms/001 (-688.066, -357.181, 2.49977)
		disable_on_ai_graph True
	100142 units/world/props/office/chair/office_chair_detective_noarms/002 (-611.6, -523.579, 0.0)
		disable_on_ai_graph True
	100133 units/world/props/oil_stains_decal_03/001 (103.0, -376.0, 0.0)
	100085 units/world/props/oil_stains_decal_03/002 (96.3829, -174.506, 0.0)
	100169 units/world/props/oil_stains_decal_03/004 (228.0, -376.0, 175.0)
