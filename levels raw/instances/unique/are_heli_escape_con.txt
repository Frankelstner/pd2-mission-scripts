ID range vs continent name:
	100000: world

statics
	100083 units/dev_tools/level_tools/dev_bag_collision_1x3m/001 (135.0, -285.0, 450.0)
	100084 units/dev_tools/level_tools/dev_bag_collision_1x3m/002 (35.0001, -285.0, 450.0)
	100088 units/dev_tools/level_tools/dev_bag_collision_1x3m/003 (-39.9999, -285.0, 450.0)
	100089 units/dev_tools/level_tools/dev_bag_collision_1x3m/004 (35.0, 314.0, 450.0)
	100090 units/dev_tools/level_tools/dev_bag_collision_1x3m/005 (-40.0, 314.0, 450.0)
	100091 units/dev_tools/level_tools/dev_bag_collision_1x3m/006 (-140.0, 314.0, 450.0)
	100082 units/dev_tools/level_tools/dev_bag_collision_1x3m/007 (-140.0, 10.9999, 450.0)
	100092 units/dev_tools/level_tools/dev_bag_collision_1x3m/008 (-39.9998, 11.0, 450.0)
	100093 units/dev_tools/level_tools/dev_bag_collision_1x3m/009 (35.0001, 11.0, 450.0)
	100094 units/dev_tools/level_tools/dev_bag_collision_1x3m/010 (-140.0, -264.0, 450.0)
	100095 units/dev_tools/level_tools/dev_bag_collision_1x3m/011 (-39.9997, -264.0, 450.0)
	100096 units/dev_tools/level_tools/dev_bag_collision_1x3m/012 (35.0003, -264.0, 450.0)
	100079 units/dev_tools/level_tools/dev_bag_collision_4x3m/001 (138.0, 119.0, 450.0)
	100043 units/dev_tools/level_tools/dev_bag_collision_4x3m/002 (138.0, 311.0, 450.0)
	100080 units/dev_tools/level_tools/dev_bag_collision_4x3m/003 (-115.0, 119.0, 450.0)
	100081 units/dev_tools/level_tools/dev_bag_collision_4x3m/004 (-115.0, 311.0, 450.0)
	100100 units/dev_tools/level_tools/dev_bag_collision_4x3m/005 (13.0, 396.0, 725.0)
	100101 units/dev_tools/level_tools/dev_bag_collision_4x3m/006 (-287.0, 396.0, 725.0)
	100102 units/dev_tools/level_tools/dev_bag_collision_4x3m/007 (-287.0, -4.00006, 725.0)
	100103 units/dev_tools/level_tools/dev_bag_collision_4x3m/008 (13.0001, -4.0, 725.0)
	100005 units/pd2_dlc_arena/architecture/lobby/are_concession_corridor_window_large/002 (0.0, 0.0, 756.0)
	100087 units/pd2_dlc_arena/dev/are_gen_square_goal_marker_2_4x4_35/001 (5.72205e-05, -2.41995e-05, 212.945)
		disable_on_ai_graph True
		mesh_variation hide
	100024 units/pd2_dlc_arena/vehicles/are_vehicle_blackhawk/001 (0.0, 150.0, 450.0)
		mesh_variation hidden
