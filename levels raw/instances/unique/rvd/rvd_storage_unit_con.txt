ID range vs continent name:
	100000: world

statics
	100039 core/units/light_omni/001 (0.0, 200.0, 78.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.992157, 0.901961, 0.803922
			enabled True
			falloff_exponent 1
			far_range 300
			multiplier match
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100113 core/units/light_spot (0.0, 200.0, 375.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.992157, 0.901961, 0.803922
			enabled True
			falloff_exponent 1
			far_range 500
			multiplier reddot
			name ls_spot
			near_range 80
			spot_angle_end 65
			spot_angle_start 43
	100048 units/dev_tools/level_tools/dev_bag_collision_1x1m/001 (-25.0, 38.0, -19.0)
	100155 units/dev_tools/level_tools/dev_bag_collision_1x1m/003 (-44.0, 38.0, -19.0)
	100156 units/dev_tools/level_tools/dev_bag_collision_1x1m/004 (-44.0, 138.0, -19.0)
	100157 units/dev_tools/level_tools/dev_bag_collision_1x1m/005 (-44.0, 210.0, -19.0)
	100158 units/dev_tools/level_tools/dev_bag_collision_1x1m/006 (-44.0, 285.0, -19.0)
	100045 units/dev_tools/level_tools/dev_door_blocker_1x2x3/001 (98.0, -43.0, 0.0)
	100029 units/dev_tools/level_tools/dev_nav_blocker_1x1x3m/001 (95.0, 50.0, 0.0)
	100034 units/dev_tools/level_tools/dev_nav_blocker_1x1x3m/002 (95.0, 150.0, 0.0)
	100078 units/dev_tools/level_tools/dev_nav_blocker_1x1x3m/003 (-116.0, 64.0, 0.0)
	100085 units/dev_tools/level_tools/dev_nav_blocker_1x1x3m/004 (-116.0, 164.0, 0.0)
	100095 units/dev_tools/level_tools/dev_nav_blocker_1x1x3m/005 (-116.0, 264.0, 0.0)
	100154 units/dev_tools/level_tools/dev_nav_blocker_1x1x3m/006 (-25.0, 350.0, 0.0)
	100159 units/dev_tools/level_tools/dev_nav_blocker_1x1x3m/007 (75.0, 350.0, 0.0)
	100160 units/dev_tools/level_tools/dev_nav_blocker_1x1x3m/008 (-116.0, 40.0, 0.0)
	100161 units/dev_tools/level_tools/dev_nav_blocker_1x1x3m/009 (125.0, 250.0, 0.0)
	100010 units/dev_tools/level_tools/dev_nav_splitter_2x3m/001 (91.0, 0.0, 0.0)
	100112 units/payday2/props/bnk_prop_vault_loot_toast/001 (7.62939e-06, 200.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
	100055 units/payday2/props/ind_prop_dock_box_stacked/001 (-82.0, 149.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100064 units/payday2/props/ind_prop_dock_box_stacked/004 (48.0, 347.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100102 units/payday2/props/previs/mannequins/previs_mannequin_01/001 (-91.0, 260.0, 0.0)
		disable_shadows True
	100101 units/payday2/props/previs/mannequins/previs_mannequin_02/001 (-76.0, 329.0, 0.0)
		disable_shadows True
	100103 units/payday2/props/previs/mannequins/previs_mannequin_03/001 (-18.0, 361.0, 0.0)
		disable_shadows True
	100107 units/payday2/props/previs/mannequins/previs_mannequin_03/002 (90.0, 146.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
	100104 units/payday2/props/previs/mannequins/previs_mannequin_dst_02/001 (93.0, 78.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
	100106 units/payday2/props/previs/mannequins/previs_mannequin_dst_02/002 (-73.0, 175.0, 32.0)
		disable_on_ai_graph True
		disable_shadows True
	100108 units/payday2/props/previs/mannequins/previs_mannequin_dst_02/003 (-95.0, 138.0, 0.0)
		disable_shadows True
	100105 units/payday2/props/previs/mannequins/previs_mannequin_dst_03/001 (-95.0, 78.0, 0.0)
		disable_shadows True
	100063 units/payday2/props/res_prop_apartment_mess_straight/001 (-83.0, 319.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100062 units/payday2/props/res_prop_livingroom_plant_table_worn/001 (-68.0, 112.0, 155.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100027 units/pd2_dlc_dah/props/dah_prop_ceiling_light_a/001 (0.0, 200.0, 296.695)
		mesh_variation lamp_off
	100005 units/pd2_dlc_rvd/architecture/rvd_ext/alley_houses/rvd_ext_storage_unit/001 (125.0, 0.0, 0.0)
	100096 units/pd2_dlc_rvd/equipment/rvd_interactable_saw_no_jam/real_saw (-1.99998, -1.0, 3.55271e-13)
		disable_on_ai_graph True
		disable_shadows True
	100091 units/pd2_dlc_rvd/pickups/rvd_pku_saw/saw_take (-1.99999, -1.0, 1.80783e-07)
		disable_on_ai_graph True
		disable_shadows True
	100162 units/pd2_dlc_rvd/props/rvd_prop_diamond_bag/001 (-27.0, 309.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100076 units/pd2_dlc_rvd/props/rvd_prop_diamond_bag/002 (61.0, 314.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100077 units/pd2_dlc_rvd/props/rvd_prop_diamond_bag/003 (17.0, 280.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100147 units/pd2_dlc_rvd/props/rvd_prop_diamond_bag/004 (-32.0, 244.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100148 units/pd2_dlc_rvd/props/rvd_prop_diamond_bag/005 (69.0, 266.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100149 units/pd2_dlc_rvd/props/rvd_prop_diamond_bag/006 (-94.0, 221.0, -4.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100058 units/pd2_dlc_rvd/props/rvd_prop_livingroom_shelf_d_tall_grime/001 (100.0, 243.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100006 units/pd2_dlc_rvd/props/rvd_prop_storage_unit_door/shutter (-128.0, -102.0, 10.0)
		mesh_variation state_open
	100118 units/pd2_dlc_rvd/props/rvd_prop_warehouse_workbench/001 (-72.0, 174.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
	100036 units/pd2_dlc_spa/props/spa_prop_armory_shelf_ammo/001 (-90.0, 339.0, 0.0)
	100120 units/world/props/bank/money_wrap_single_bundle/001 (-63.0, 90.0, 81.0)
		disable_shadows True
	100121 units/world/props/bank/money_wrap_single_bundle/002 (-68.0, 109.0, 81.0)
		disable_shadows True
	100122 units/world/props/bank/money_wrap_single_bundle/003 (-66.0, 76.0, 81.0)
		disable_shadows True
	100123 units/world/props/bank/money_wrap_single_bundle/004 (-77.0, 130.0, 81.0)
		disable_shadows True
	100124 units/world/props/bank/money_wrap_single_bundle/005 (-58.0, 123.0, 81.0)
		disable_shadows True
	100125 units/world/props/bank/money_wrap_single_bundle/006 (-76.0, 175.0, 81.0)
		disable_shadows True
	100126 units/world/props/bank/money_wrap_single_bundle/007 (-90.0, 186.0, 81.0)
		disable_shadows True
	100127 units/world/props/bank/money_wrap_single_bundle/008 (-76.0, 195.0, 81.0)
		disable_shadows True
	100128 units/world/props/bank/money_wrap_single_bundle/009 (-84.0, 217.0, 81.0)
		disable_shadows True
	100129 units/world/props/bank/money_wrap_single_bundle/010 (-95.0, 232.0, 81.0)
		disable_shadows True
	100130 units/world/props/bank/money_wrap_single_bundle/011 (-66.0, 215.0, 81.0)
		disable_shadows True
	100131 units/world/props/bank/money_wrap_single_bundle/012 (-53.0, 211.0, 81.0)
		disable_shadows True
	100132 units/world/props/bank/money_wrap_single_bundle/013 (-59.0, 231.0, 81.0)
		disable_shadows True
	100133 units/world/props/bank/money_wrap_single_bundle/014 (-61.0, 245.0, 81.0)
		disable_shadows True
	100134 units/world/props/bank/money_wrap_single_bundle/015 (-72.0, 263.0, 81.0)
		disable_shadows True
	100135 units/world/props/bank/money_wrap_single_bundle/016 (-84.0, 270.0, 81.0)
		disable_shadows True
	100136 units/world/props/bank/money_wrap_single_bundle/017 (-56.0, 173.0, 81.0)
		disable_shadows True
	100137 units/world/props/bank/money_wrap_single_bundle/018 (-63.0, 154.0, 81.0)
		disable_shadows True
	100138 units/world/props/bank/money_wrap_single_bundle/019 (-57.0, 138.0, 81.0)
		disable_shadows True
	100139 units/world/props/bank/money_wrap_single_bundle/020 (-34.0, 145.0, 81.0)
		disable_shadows True
	100140 units/world/props/bank/money_wrap_single_bundle/021 (-38.0, 161.0, 81.0)
		disable_shadows True
	100011 units/world/props/diamondheist_diamond_pickups/diamond_pickup_single_02/001 (-35.0, 142.0, 0.0)
		disable_shadows True
		mesh_variation interactive_off
	100012 units/world/props/diamondheist_diamond_pickups/diamond_pickup_single_02/002 (7.00002, 64.0, 0.0)
		disable_shadows True
		mesh_variation interactive_off
	100013 units/world/props/diamondheist_diamond_pickups/diamond_pickup_single_02/003 (-4.99999, 133.0, 0.0)
		disable_shadows True
		mesh_variation interactive_off
	100014 units/world/props/diamondheist_diamond_pickups/diamond_pickup_single_02/004 (6.00002, 149.0, 0.0)
		disable_shadows True
		mesh_variation interactive_off
