ID range vs continent name:
	100000: world

statics
	100020 units/dev_tools/level_tools/dev_bag_collision_1x1m/001 (79.0, -9.0, -1.0)
	100021 units/dev_tools/level_tools/dev_bag_collision_1x1m/002 (25.0, -9.0, -1.0)
	100026 units/dev_tools/level_tools/dev_bag_collision_1x1m/003 (25.0, -9.0, 5.0)
	100027 units/dev_tools/level_tools/dev_bag_collision_1x1m/004 (79.0, -9.0, 5.0)
	100005 units/pd2_dlc_rvd/props/rvd_prop_warehouse_window/001 (-75.0, 0.0, 100.0)
	100008 units/world/architecture/secret_stash/props/secret_stash_planks_outline/planks_ray (1.0, -4.0, 208.0)
		mesh_variation hide
	100009 units/world/architecture/secret_stash/props/secret_stash_planks_outline_no_sunray/planks_no_ray (1.0, -4.0, 208.0)
		mesh_variation hide
