ID range vs continent name:
	100000: world

statics
	100004 units/payday2/props/gen_interactable_panel_keycard/002 (162.0, 30.9998, 117.546)
		mesh_variation state_4
	100020 units/payday2/props/gen_prop_security_timelock/001 (173.0, 31.0001, 147.943)
	100042 units/payday2/props/interactable_panel_keycard/gen_panel_keycard_hack/001 (162.0, 30.9998, 117.546)
		mesh_variation hide
	100049 units/pd2_indiana/architecture/int/mus_int_lobby_door_3x3/001 (-150.0, -25.0, 0.0)
	100030 units/pd2_indiana/props/door/mus_prop_diamond_securitygate/001 (150.0, 0.000183105, 2.5)
		disable_on_ai_graph True
