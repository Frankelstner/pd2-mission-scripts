﻿´coke_picked_up´ ElementUnitSequenceTrigger 100083
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/pickups/gen_pku_cocaine/002 (106.0, -53.0, 84.0)
		2
			guis_id 2
			sequence interact
			unit_id units/payday2/pickups/gen_pku_cocaine/003 (42.0, -59.9999, 84.0)
		3
			guis_id 3
			sequence interact
			unit_id units/payday2/pickups/gen_pku_cocaine/004 (136.0, -73.9999, 85.0)
		4
			guis_id 4
			sequence interact
			unit_id units/payday2/pickups/gen_pku_cocaine/005 (93.9999, -78.0, 83.0)
	on_executed
		´hide_coke_related´ (delay 0)
		´disable_lookat´ (delay 0)
´hide_coke_related´ ElementDisableUnit 100084
	position 100.0, 200.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/world/props/apartment/cocaine/cokebag_depleted/002 (62.0, -74.0, 87.0)
		2 units/world/props/apartment/cocaine/cokebag_ripped/002 (138.0, -78.0, 85.0)
		3 units/world/props/apartment/cocaine/cokebag_varia/003 (63.0, -90.9999, 85.0)
		4 units/world/props/apartment/cocaine/cokebag_varia/002 (95.948, -69.9999, 87.0014)
		5 units/world/props/apartment/cocaine/cokebag_depleted/003 (105.0, -89.9999, 86.0)
		6 units/world/props/apartment/cocaine/cokebag_ripped/003 (53.0001, -72.0, 84.0)
		7 units/world/props/apartment/cocaine/cokebag_varia/004 (164.0, -32.9999, 89.0)
	on_executed
		´out_took_loot´ (delay 0)
´disable_table_layouts´ ElementDisableUnit 100085
	TRIGGER TIMES 1
	position 100.0, -400.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/world/props/apartment/cocaine/cokebag_depleted/002 (62.0, -74.0, 87.0)
		2 units/world/props/box/a4paperbox_02/002 (163.0, -22.0, 85.0)
		3 units/world/props/box/a4paperbox_02/003 (121.0, -28.0, 85.0)
		4 units/world/props/box/a4paperbox_02/004 (77.9999, -27.0, 85.0)
		5 units/world/props/box/a4paperbox_bottom_01/002 (24.0, -29.0, 86.0)
		6 units/world/props/box/a4paperbox_bottom_01/003 (162.0, -35.9999, 86.0)
		7 units/world/props/box/a4paperbox_bottom_01/004 (164.0, -23.0, 111.0)
		8 units/world/props/box/a4paperbox_top_01/002 (150.002, -19.9999, 80.7768)
		9 units/world/props/apartment/kitchen_counter/apartment_frying_pan/002 (43.0, -78.9999, 86.0)
		10 units/world/props/apartment/kitchen_counter/apartment_frying_pan/003 (138.0, -75.6744, 97.9455)
		11 units/world/props/apartment/kitchen_counter/apartment_kitchen_knife/002 (82.0, -92.0, 86.0)
		12 units/world/props/apartment/kitchen_counter/apartment_kitchen_knife/003 (178.0, -87.9999, 86.0)
		13 units/world/props/apartment/kitchen_counter/apartment_kitchen_knife/004 (90.0, -48.9999, 127.0)
		14 units/world/props/apartment/kitchen_counter/apartment_microwave/002 (145.0, -28.0, 86.0)
		15 units/world/props/apartment/kitchen_counter/apartment_microwave/003 (27.0, -19.9999, 84.0)
		16 units/world/props/apartment/kitchen_counter/apartment_microwave/004 (27.0, -19.9999, 107.0)
		17 units/world/props/apartment/kitchen_counter/apartment_small_kastrull/002 (72.0, -86.9999, 86.0)
		18 units/world/props/apartment/cocaine_mirror/002 (171.0, -95.9999, 86.0)
		19 units/world/props/apartment/cocaine_mirror/003 (28.0, -97.0, 86.0)
		20 units/world/props/apartment/cocaine/cokebag_depleted/003 (105.0, -89.9999, 86.0)
		21 units/world/props/apartment/cocaine/cokebag_ripped/002 (138.0, -78.0, 85.0)
		22 units/world/props/apartment/cocaine/cokebag_ripped/003 (53.0001, -72.0, 84.0)
		23 units/world/props/apartment/cocaine/cokebag_varia/002 (95.948, -69.9999, 87.0014)
		24 units/world/props/apartment/cocaine/cokebag_varia/003 (63.0, -90.9999, 85.0)
		25 units/world/props/apartment/cocaine/cokebag_varia/004 (164.0, -32.9999, 89.0)
		26 units/world/props/office/accessories/desk_lamp_04_omni/002 (13.0, -57.9999, 85.0)
		27 units/world/props/office/accessories/desk_lamp_04_omni/003 (173.0, -53.9999, 85.0)
		28 units/world/props/office/accessories/desk_lamp_04_omni/004 (91.0, -15.0, 85.0)
		29 units/world/props/office/accessories/desk_lamp_04_omni/005 (19.0001, -48.0, 85.0)
		30 units/pd2_dlc_lxy/props/yacht/drugs/lxy_prop_drugs_cocain/002 (142.0, -80.9999, 86.0)
		31 units/world/props/mansion/man_prop_int_brass_bowl/002 (160.0, -26.0, 85.0)
		32 units/world/props/mansion/man_prop_int_brass_bowl/003 (139.0, -71.0, 87.0)
		33 units/world/props/apartment/pizza_box/002 (98.0, -28.0, 88.0)
		34 units/world/props/survivalist/potspans/plate_large/002 (99.0, -73.9999, 86.0)
		35 units/world/props/survivalist/potspans/plate_large/003 (86.0, -27.0, 91.0)
		36 units/world/props/box/prop_filebox_01/002 (81.0, -34.9999, 84.0)
		37 units/world/props/apartment/take_away_coffe_mug/002 (35.0, -22.0, 90.0)
		38 units/world/props/apartment/take_away_coffe_mug/003 (17.0, -42.9999, 85.0)
		39 units/world/props/apartment/take_away_cup_holder/003 (27.0, -25.0, 86.0)
		40 units/world/props/apartment/take_away_cup_holder/004 (30.0, -16.0, 86.0)
		41 units/world/props/train/tra_prop_waterbottle/002 (66.0, -15.0, 89.0)
		42 units/world/props/train/tra_prop_waterbottle/003 (76.0, -40.0, 89.0)
		43 units/world/props/train/tra_prop_waterbottle/004 (53.0, -16.0, 85.0)
		44 units/world/props/train/tra_prop_waterbottle/005 (52.0001, -35.0, 85.0)
		45 units/world/props/train/tra_prop_waterbottle/006 (49.0001, -19.0, 85.0)
		46 units/world/props/apartment/wild_box/002 (120.0, -25.0, 110.0)
	on_executed
		´seq_hide_all_cocaine´ (delay 0)
´enable_layout_001´ ElementEnableUnit 100086
	position 200.0, -500.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/world/props/apartment/kitchen_counter/apartment_kitchen_knife/002 (82.0, -92.0, 86.0)
		2 units/world/props/mansion/man_prop_int_brass_bowl/002 (160.0, -26.0, 85.0)
		3 units/world/props/office/accessories/desk_lamp_04_omni/004 (91.0, -15.0, 85.0)
		4 units/world/props/train/tra_prop_waterbottle/003 (76.0, -40.0, 89.0)
		5 units/world/props/train/tra_prop_waterbottle/002 (66.0, -15.0, 89.0)
		6 units/world/props/train/tra_prop_waterbottle/004 (53.0, -16.0, 85.0)
		7 units/world/props/box/a4paperbox_bottom_01/002 (24.0, -29.0, 86.0)
		8 units/world/props/apartment/take_away_cup_holder/003 (27.0, -25.0, 86.0)
	on_executed
		´enable_layout_001_loot´ (delay 0) DISABLED
´enable_layout_002´ ElementEnableUnit 100087
	position 300.0, -500.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/world/props/survivalist/potspans/plate_large/002 (99.0, -73.9999, 86.0)
		2 units/pd2_dlc_lxy/props/yacht/drugs/lxy_prop_drugs_cocain/002 (142.0, -80.9999, 86.0)
		3 units/world/props/apartment/cocaine_mirror/002 (171.0, -95.9999, 86.0)
		4 units/world/props/office/accessories/desk_lamp_04_omni/003 (173.0, -53.9999, 85.0)
		5 units/world/props/apartment/kitchen_counter/apartment_microwave/002 (145.0, -28.0, 86.0)
		6 units/world/props/apartment/pizza_box/002 (98.0, -28.0, 88.0)
		7 units/world/props/survivalist/potspans/plate_large/003 (86.0, -27.0, 91.0)
		8 units/world/props/apartment/take_away_cup_holder/004 (30.0, -16.0, 86.0)
		9 units/world/props/apartment/take_away_coffe_mug/002 (35.0, -22.0, 90.0)
		10 units/world/props/apartment/take_away_coffe_mug/003 (17.0, -42.9999, 85.0)
	on_executed
		´enable_layout_002_loot´ (delay 0) DISABLED
´enable_layout_003´ ElementEnableUnit 100088
	position 400.0, -500.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/world/props/apartment/kitchen_counter/apartment_frying_pan/002 (43.0, -78.9999, 86.0)
		2 units/world/props/apartment/kitchen_counter/apartment_small_kastrull/002 (72.0, -86.9999, 86.0)
		3 units/world/props/box/a4paperbox_bottom_01/003 (162.0, -35.9999, 86.0)
		4 units/world/props/box/a4paperbox_top_01/002 (150.002, -19.9999, 80.7768)
		5 units/world/props/box/prop_filebox_01/002 (81.0, -34.9999, 84.0)
		6 units/world/props/apartment/kitchen_counter/apartment_kitchen_knife/004 (90.0, -48.9999, 127.0)
		7 units/world/props/apartment/kitchen_counter/apartment_kitchen_knife/003 (178.0, -87.9999, 86.0)
		8 units/world/props/apartment/kitchen_counter/apartment_microwave/003 (27.0, -19.9999, 84.0)
		9 units/world/props/apartment/kitchen_counter/apartment_microwave/004 (27.0, -19.9999, 107.0)
		10 units/world/props/office/accessories/desk_lamp_04_omni/002 (13.0, -57.9999, 85.0)
	on_executed
		´enable_layout_003_loot´ (delay 0) DISABLED
´enable_layout_004´ ElementEnableUnit 100089
	position 500.0, -500.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/world/props/apartment/cocaine_mirror/003 (28.0, -97.0, 86.0)
		2 units/world/props/office/accessories/desk_lamp_04_omni/005 (19.0001, -48.0, 85.0)
		3 units/world/props/train/tra_prop_waterbottle/005 (52.0001, -35.0, 85.0)
		4 units/world/props/train/tra_prop_waterbottle/006 (49.0001, -19.0, 85.0)
		5 units/world/props/box/a4paperbox_02/004 (77.9999, -27.0, 85.0)
		6 units/world/props/box/a4paperbox_02/003 (121.0, -28.0, 85.0)
		7 units/world/props/apartment/wild_box/002 (120.0, -25.0, 110.0)
		8 units/world/props/box/a4paperbox_02/002 (163.0, -22.0, 85.0)
		9 units/world/props/box/a4paperbox_bottom_01/004 (164.0, -23.0, 111.0)
		10 units/world/props/mansion/man_prop_int_brass_bowl/003 (139.0, -71.0, 87.0)
		11 units/world/props/apartment/kitchen_counter/apartment_frying_pan/003 (138.0, -75.6744, 97.9455)
	on_executed
		´enable_layout_004_loot´ (delay 0) DISABLED
´enable_layout_001_loot´ ElementEnableUnit 100090
	DISABLED
	position 200.0, -600.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/world/props/apartment/cocaine/cokebag_depleted/002 (62.0, -74.0, 87.0)
		2 units/world/props/apartment/cocaine/cokebag_ripped/002 (138.0, -78.0, 85.0)
	on_executed
		´seq_show_coke_001´ (delay 0)
´enable_layout_002_loot´ ElementEnableUnit 100091
	DISABLED
	position 300.0, -600.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/world/props/apartment/cocaine/cokebag_varia/003 (63.0, -90.9999, 85.0)
		2 units/world/props/apartment/cocaine/cokebag_varia/002 (95.948, -69.9999, 87.0014)
	on_executed
		´seq_show_coke_002´ (delay 0)
´enable_layout_003_loot´ ElementEnableUnit 100092
	DISABLED
	position 400.0, -600.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/world/props/apartment/cocaine/cokebag_depleted/003 (105.0, -89.9999, 86.0)
		2 units/world/props/apartment/cocaine/cokebag_varia/004 (164.0, -32.9999, 89.0)
	on_executed
		´seq_show_coke_003´ (delay 0)
´enable_layout_004_loot´ ElementEnableUnit 100093
	DISABLED
	position 500.0, -600.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/world/props/apartment/cocaine/cokebag_ripped/003 (53.0001, -72.0, 84.0)
	on_executed
		´seq_show_coke_004´ (delay 0)
´spawn_with_loot´ ElementToggle 100098
	elements
		1 ´enable_layout_001_loot´ DISABLED
		2 ´enable_layout_002_loot´ DISABLED
		3 ´enable_layout_003_loot´ DISABLED
		4 ´enable_layout_004_loot´ DISABLED
		5 ´area_coke_seen´ DISABLED
	set_trigger_times -1
	toggle on
´in_startup´ ElementInstanceInput 100094
	event coketable_startup
	on_executed
		´disable_table_layouts´ (delay 0)
´in_spawn_layout´ ElementInstanceInput 100095
	event coketable_spawn_layout
	on_executed
		´logic_random_001´ (delay 0)
		´out_table_shown´ (delay 0)
´in_enable_loot´ ElementInstanceInput 100096
	event coketable_enable_loot
	on_executed
		´spawn_with_loot´ (delay 0)
		´cant_hide´ (delay 0)
´logic_random_001´ ElementRandom 100097
	TRIGGER TIMES 1
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´enable_layout_001´ (delay 0)
		´enable_layout_002´ (delay 0)
		´enable_layout_003´ (delay 0)
		´enable_layout_004´ (delay 0)
´out_took_loot´ ElementInstanceOutput 100099
	event coke_table_loot_interacted
´seq_hide_all_cocaine´ ElementUnitSequence 100101
	position 200.0, -400.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_cocaine/002 (106.0, -53.0, 84.0)
			notify_unit_sequence disable_interaction
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_cocaine/003 (42.0, -59.9999, 84.0)
			notify_unit_sequence disable_interaction
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_cocaine/004 (136.0, -73.9999, 85.0)
			notify_unit_sequence disable_interaction
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_cocaine/005 (93.9999, -78.0, 83.0)
			notify_unit_sequence disable_interaction
			time 0
		5
			id 5
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_cocaine/002 (106.0, -53.0, 84.0)
			notify_unit_sequence hide
			time 0
		6
			id 6
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_cocaine/003 (42.0, -59.9999, 84.0)
			notify_unit_sequence hide
			time 0
		7
			id 7
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_cocaine/004 (136.0, -73.9999, 85.0)
			notify_unit_sequence hide
			time 0
		8
			id 8
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_cocaine/005 (93.9999, -78.0, 83.0)
			notify_unit_sequence hide
			time 0
´seq_show_coke_001´ ElementUnitSequence 100102
	position 200.0, -700.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_cocaine/002 (106.0, -53.0, 84.0)
			notify_unit_sequence show_and_enable_interaction
			time 0
´seq_show_coke_002´ ElementUnitSequence 100103
	position 300.0, -700.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_cocaine/003 (42.0, -59.9999, 84.0)
			notify_unit_sequence show_and_enable_interaction
			time 0
´seq_show_coke_003´ ElementUnitSequence 100104
	position 400.0, -700.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_cocaine/004 (136.0, -73.9999, 85.0)
			notify_unit_sequence show_and_enable_interaction
			time 0
´seq_show_coke_004´ ElementUnitSequence 100105
	position 500.0, -700.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_cocaine/005 (93.9999, -78.0, 83.0)
			notify_unit_sequence show_and_enable_interaction
			time 0
´in_hide_table´ ElementInstanceInput 100009
	event coketable_hide
	on_executed
		´hide_table´ (delay 0)
´hide_table´ ElementDisableUnit 100010
	position 100.0, -800.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/world/props/western/wst_prop_int_gen_table_medium/001 (0.0, 0.0, 0.0)
	on_executed
		´disable_show_related_inputs´ (delay 0)
´disable_show_related_inputs´ ElementToggle 100011
	elements
		1 ´in_spawn_layout´
		2 ´in_enable_loot´
	set_trigger_times -1
	toggle off
´out_table_shown´ ElementInstanceOutput 100012
	event coketable_shown
´cant_hide´ ElementToggle 100013
	elements
		1 ´in_hide_table´
	set_trigger_times -1
	toggle off
´area_coke_seen´ ElementAreaTrigger 100014
	DISABLED
	TRIGGER TIMES 1
	amount 1
	depth 500
	height 300
	instigator player
	instigator_name 
	interval 0.1
	position 100.0, -275.0, 125.0
	radius 250
	rotation 0.0, 0.0, -0.707107, -0.707107
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 500
	on_executed
		´point_debug_001´ (delay 0)
		´enable_lookat´ (delay 0)
´point_debug_001´ ElementDebug 100015
	as_subtitle False
	debug_string area_coke_seen
	show_instigator False
´point_lookat_trigger_001´ ElementLookAtTrigger 100016
	DISABLED
	TRIGGER TIMES 1
	distance 200
	in_front False
	interval 0.1
	position 100.0, -75.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	sensitivity 0.88
	on_executed
		´teammate_comment_jackpot´ (delay 0)
		´instance_output_seen_coke´ (delay 1)
´enable_lookat´ ElementToggle 100017
	elements
		1 ´point_lookat_trigger_001´ DISABLED
	set_trigger_times -1
	toggle on
´disable_lookat´ ElementToggle 100018
	elements
		1 ´point_lookat_trigger_001´ DISABLED
		2 ´area_coke_seen´ DISABLED
	set_trigger_times -1
	toggle off
´point_debug_002´ ElementDebug 100019
	as_subtitle False
	debug_string teammate_comment_jackpot
	show_instigator False
´teammate_comment_jackpot´ ElementTeammateComment 100020
	close_to_element True
	comment v21
	position 100.0, -75.0, 150.0
	radius 0
	rotation 0.0, 0.0, 0.0, -1.0
	use_instigator False
	on_executed
		´point_debug_002´ (delay 0)
´disable_VO´ ElementToggle 100021
	elements
		1 ´teammate_comment_jackpot´
	set_trigger_times -1
	toggle off
´instance_input_disable_VO´ ElementInstanceInput 100022
	event disable_VO
	on_executed
		´disable_VO´ (delay 0)
´instance_output_seen_coke´ ElementInstanceOutput 100023
	event seen_coke
