ID range vs continent name:
	100000: world

statics
	100003 units/lights/light_omni_shadow_projection_01_seq/001 (0.0, 0.0, -100.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.6, 0.447059, 0.266667
			enabled True
			falloff_exponent 1
			far_range 250
			multiplier identity
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
		projection_light lo_omni
	100000 units/world/props/apartment/lightbulb/hanging_lightbulb2/001 (0.0, 0.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.6, 0.447059, 0.266667
			enabled False
			falloff_exponent 1
			far_range 250
			multiplier identity
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
		mesh_variation turn_on_pendulum
