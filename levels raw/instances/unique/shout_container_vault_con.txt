ID range vs continent name:
	100000: world

statics
	100048 units/dev_tools/level_tools/dev_door_blocker_1x2x3/001 (100.0, -350.0, 0.0)
	100049 units/dev_tools/level_tools/dev_door_blocker_1x2x3/002 (100.0, 250.0, 0.0)
	100084 units/dev_tools/level_tools/dev_door_blocker_1x2x3/003 (-99.9999, -150.0, 0.0)
	100089 units/dev_tools/level_tools/dev_vehicle_only_collision_1x3m/001 (-25.0, 225.0, 25.0)
	100000 units/payday2/props/gen_prop_container_murky/container_with_vault (0.0, 0.0, 0.0)
	100001 units/payday2/props/gen_prop_container_murky_doors/container_with_vault_doors_front (116.0, 295.0, 18.0)
		disable_on_ai_graph True
		mesh_variation state_interaction_enabled
	100093 units/payday2/props/gen_prop_container_murky_doors_vented/back_door_vented (-115.0, -295.0, 18.0)
		mesh_variation state_interaction_enabled
	100105 units/payday2/props/gen_prop_long_lamp_v2/003 (-112.0, -125.0, 249.784)
	100106 units/payday2/props/gen_prop_long_lamp_v2/004 (-112.0, 74.9999, 249.784)
	100103 units/payday2/props/gen_prop_long_lamp_v2/005 (110.0, -125.0, 249.784)
	100104 units/payday2/props/gen_prop_long_lamp_v2/006 (110.0, 74.9999, 249.784)
	100003 units/pd2_dlc_shoutout_raid/props/gen_prop_container_a_vault_seq/container_vault (0.0, 0.0, 0.0)
		disable_on_ai_graph True
	100075 units/pd2_dlc_shoutout_raid/props/ind_prop_murky_box/001 (-56.0001, -31.0, 25.0)
		disable_on_ai_graph True
		mesh_variation state_interaction_disabled
	100076 units/pd2_dlc_shoutout_raid/props/ind_prop_murky_box/002 (-56.0001, 124.0, 25.0)
		disable_on_ai_graph True
		mesh_variation state_interaction_disabled
	100077 units/pd2_dlc_shoutout_raid/props/ind_prop_murky_box/003 (62.0, -34.0, 25.0)
		disable_on_ai_graph True
		mesh_variation state_interaction_disabled
	100086 units/pd2_dlc_shoutout_raid/props/ind_prop_murky_box/004 (62.0, 123.0, 25.0)
		disable_on_ai_graph True
		mesh_variation state_interaction_disabled
	100014 units/pd2_indiana/props/gen_prop_security_timer/001 (46.0, -267.0, 121.0)
		disable_on_ai_graph True
		mesh_variation black_on_light_blue
	100107 units/world/props/flickering_light/001 (0.0, 75.0, 125.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.227451, 0.337255, 0.619608
			enabled True
			falloff_exponent 1
			far_range 200
			multiplier identity
			name lo_light_flicker
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
		mesh_variation light_on
		projection_light lo_light_flicker
	100108 units/world/props/flickering_light/002 (0.0, -100.0, 125.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.227451, 0.337255, 0.619608
			enabled True
			falloff_exponent 1
			far_range 200
			multiplier identity
			name lo_light_flicker
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
		mesh_variation light_on
		projection_light lo_light_flicker
