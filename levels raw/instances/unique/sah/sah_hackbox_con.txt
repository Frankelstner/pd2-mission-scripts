ID range vs continent name:
	100000: world

statics
	100049 core/units/light_omni/001 (-8.0, -69.0, 75.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.0666667, 0.0823529, 0.101961
			enabled True
			falloff_exponent 1
			far_range 100
			multiplier identity
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100036 units/pd2_dlc_sah/props/sah_interactable_hackbox/001 (-13.0, -7.00001, -1.0)
		disable_shadows True
		hide_on_projection_light True
