ID range vs continent name:
	100000: world

statics
	100038 units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (50.0, -50.0, 0.0)
	100035 units/dev_tools/level_tools/dev_nav_blocker_1x3m/001 (0.0, -50.0, 0.0)
	100036 units/dev_tools/level_tools/dev_nav_blocker_1x3m/002 (200.0, -50.0, 0.0)
	100034 units/dev_tools/level_tools/dev_nav_splitter_2x3m/001 (200.0, 0.0, 0.0)
	100016 units/lights/01/light_projection_02/002 (100.0, -18.0, 282.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.65098, 0.372549, 0.235294
			enabled True
			falloff_exponent 1
			far_range 550
			multiplier desklight
			name ls_spot
			near_range 200
			spot_angle_end 68
			spot_angle_start 63
		projection_light ls_spot
		ls_spot units/lights/spot_light_projection_textures/spotprojection_34_window01_df
	100015 units/lights/01/light_projection_02/003 (100.0, 41.0, 282.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.65098, 0.372549, 0.235294
			enabled True
			falloff_exponent 1
			far_range 550
			multiplier reddot
			name ls_spot
			near_range 200
			spot_angle_end 68
			spot_angle_start 63
		projection_light ls_spot
		ls_spot units/lights/spot_light_projection_textures/spotprojection_brb_door_df_df
	100000 units/pd2_dlc_sah/architecture/sah_ext/door/sah_ext_glass_window/001 (0.0, 0.0, 0.00405163)
		disable_on_ai_graph True
