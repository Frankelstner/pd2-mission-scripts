ID range vs continent name:
	100000: world

statics
	100017 core/units/light_omni/001 (1.0, -36.0, 182.59)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.694118, 0.54902
			enabled True
			falloff_exponent 1
			far_range 300
			multiplier streetlight
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100026 units/dev_tools/level_tools/dev_door_blocker_1x2x3/001 (300.0, 0.0, 0.0)
	100025 units/dev_tools/level_tools/dev_door_blocker_1x4x3/001 (100.0, 0.0, 0.0)
	100016 units/payday2/architecture/lux_int_lightspot1_small_a/001 (-50.0, 50.0, 279.053)
	100018 units/payday2/architecture/lux_int_lightspot1_small_a/002 (50.0, 50.0, 279.053)
	100011 units/pd2_dlc_chill/props/a/chl_prop_livingroom_coffeetable_lid/001 (48.3101, 49.8198, 75.0)
		disable_on_ai_graph True
		disable_shadows False
		hide_on_projection_light True
	100013 units/pd2_dlc_fish/lxy_int_wall_wine_fridge_01/001 (93.0, 12.9999, 0.0)
		disable_on_ai_graph True
		disable_shadows False
		hide_on_projection_light True
	100015 units/pd2_dlc_fish/lxy_int_wall_wine_fridge_01/002 (-300.0, 10.0002, 0.0)
		disable_on_ai_graph True
		disable_shadows False
		hide_on_projection_light True
	100012 units/pd2_dlc_fish/props/lxy_prop_glow_wine_fridge_01/001 (93.0, 13.0, 0.0)
		disable_on_ai_graph True
		disable_shadows False
		hide_on_projection_light True
	100014 units/pd2_dlc_fish/props/lxy_prop_glow_wine_fridge_01/002 (-300.0, 10.0002, 0.0)
		disable_on_ai_graph True
		disable_shadows False
		hide_on_projection_light True
	100008 units/pd2_dlc_friend/props/and_glass/sfm_prop_office_decanter/001 (41.2404, 54.2402, 72.98)
		disable_on_ai_graph True
		disable_shadows False
		hide_on_projection_light True
	100009 units/pd2_dlc_friend/props/and_glass/sfm_prop_office_decanter/002 (57.0, 53.0, 76.98)
		disable_on_ai_graph True
		disable_shadows False
		hide_on_projection_light True
	100010 units/pd2_dlc_jfr/props/jfr_crate_wine/001 (-7.0, 40.0, 74.0)
		disable_on_ai_graph True
		disable_shadows False
		hide_on_projection_light True
	100007 units/pd2_dlc_jfr/props/jfr_int_wall_displaycase/001 (0.0, 0.0, 0.0)
		disable_on_ai_graph True
		disable_shadows False
		hide_on_projection_light True
