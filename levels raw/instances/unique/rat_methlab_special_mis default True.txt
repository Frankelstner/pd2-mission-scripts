﻿´interacted_with_caustic_soda´ ElementUnitSequenceTrigger 100046
	sequence_list
		1
			guis_id 2
			sequence interact
			unit_id units/payday2/pickups/gen_pku_methlab_caustic_cooler/002 (88.3954, 129.008, 92.9999)
	on_executed
		´caus_soda´ (delay 0)
´interacted_with_hydrogen_chloride´ ElementUnitSequenceTrigger 100047
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/pickups/gen_pku_methlab_liquid_meth/002 (150.999, 129.0, 92.9999)
	on_executed
		´hydr_chlor´ (delay 0)
´interacted_with_meth´ ElementUnitSequenceTrigger 100049
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/props/gen_prop_methlab_meth/002 (-56.0013, -221.0, 96.9999)
	on_executed
		´meth_taken´ (delay 0)
´interacted_with_muriatic_acid´ ElementUnitSequenceTrigger 100050
	sequence_list
		1
			guis_id 2
			sequence interact
			unit_id units/payday2/pickups/gen_pku_methlab_bubbling/002 (100.999, -250.0, 93.9999)
	on_executed
		´mur_acid´ (delay 0)
´mur_acid´ MissionScriptElement 100048
	on_executed
		´instance_output_interacted_with_muriatic_acid´ (delay 0)
		´sound_add_ingredient´ (delay 0)
		´disable_interaction_methlab´ (delay 0)
´hydr_chlor´ MissionScriptElement 100051
	on_executed
		´instance_output_interacted_with_hydrogen_chloride´ (delay 0)
		´sound_add_ingredient´ (delay 0)
		´disable_interaction_methlab´ (delay 0)
´caus_soda´ MissionScriptElement 100052
	on_executed
		´instance_output_interacted_with_caustic_chloride´ (delay 0)
		´sound_add_ingredient´ (delay 0)
		´disable_interaction_methlab´ (delay 0)
´instance_output_interacted_with_muriatic_acid´ ElementInstanceOutput 100053
	event interacted_with_muriatic_acid
´instance_output_interacted_with_hydrogen_chloride´ ElementInstanceOutput 100054
	event interacted_with_hydrogen_chloride
´instance_output_interacted_with_caustic_chloride´ ElementInstanceOutput 100055
	event interacted_with_caustic_soda
´meth_taken´ MissionScriptElement 100056
	on_executed
		´instance_output_meth_taken´ (delay 0)
		´remove_WP_meth´ (delay 0)
´instance_output_meth_taken´ ElementInstanceOutput 100057
	event meth_taken
´setup´ MissionScriptElement 100061
	EXECUTE ON STARTUP
	BASE DELAY 1
	on_executed
		´disabler´ (delay 0)
		´sound_methlab´ (delay 0)
		´seq_hider´ (delay 0)
´seq_hider´ ElementUnitSequence 100062
	position -450.0, 300.0, 100.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_methlab_bubbling/002 (100.999, -250.0, 93.9999)
			notify_unit_sequence interaction_disable
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_methlab_caustic_cooler/002 (88.3954, 129.008, 92.9999)
			notify_unit_sequence interaction_disable
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_methlab_liquid_meth/002 (150.999, 129.0, 92.9999)
			notify_unit_sequence interaction_disable
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/payday2/props/gen_prop_methlab_meth/002 (-56.0013, -221.0, 96.9999)
			notify_unit_sequence state_vis_hide
			time 0
´disabler´ ElementDisableUnit 100063
	position -450.0, 200.0, 100.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	unit_ids
´seq_show_meth´ ElementUnitSequence 100064
	position -350.0, -350.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/props/gen_prop_methlab_meth/002 (-56.0013, -221.0, 96.9999)
			notify_unit_sequence state_vis_show
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/props/gen_prop_methlab_meth/002 (-56.0013, -221.0, 96.9999)
			notify_unit_sequence state_interaction_enabled
			time 0
´show_meth´ MissionScriptElement 100065
	on_executed
		´seq_show_meth´ (delay 0)
		´WP_meth´ (delay 0)
		´effect_stop´ (delay 0)
		´remove_WP_methlab´ (delay 0)
´instance_input_show_meth´ ElementInstanceInput 100066
	event show_meth
	on_executed
		´show_meth´ (delay 0)
´WP_meth´ ElementWaypoint 100067
	icon pd2_goto
	only_in_civilian False
	only_on_instigator False
	position -61.0, -224.0, 172.0
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´WP_methlab´ ElementWaypoint 100068
	icon pd2_methlab
	only_in_civilian False
	only_on_instigator False
	position 25.0, -48.0, 125.0
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´area_methlab_found´ ElementAreaTrigger 100069
	TRIGGER TIMES 1
	amount 1
	depth 800
	height 300
	instigator player
	instigator_name 
	interval 0.1
	position -7.62939e-06, -48.0, 125.0
	radius 250
	rotation 0.0, 0.0, -0.707107, -0.707107
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 500
	on_executed
		´methlab_found´ (delay 0)
´methlab_found´ MissionScriptElement 100070
	on_executed
		´instance_output_methlab_found´ (delay 0)
		´blurzone_start_cooking´ (delay 0)
		´effect004´ (delay 0)
		´effect005´ (delay 0)
		´effect006´ (delay 0)
´remove_WP_methlab´ ElementOperator 100071
	elements
		1 ´WP_methlab´
	operation remove
´instance_output_methlab_found´ ElementInstanceOutput 100072
	event methlab_found
´instance_input_wrong´ ElementInstanceInput 100073
	event wrong
	on_executed
		´wrong´ (delay 0)
´instance_input_right´ ElementInstanceInput 100074
	event right
	on_executed
		´right´ (delay 0)
´right´ MissionScriptElement 100075
	on_executed
		´remove_WP_methlab´ (delay 0)
´wrong´ MissionScriptElement 100076
	on_executed
		´remove_WP_methlab´ (delay 0)
		´sound_methlab_stop´ (delay 0)
		´effect_stop´ (delay 0)
		´teammate_comment_swear´ (delay 0)
		´explode´ (delay 5)
		´sound_explosion´ (delay 0)
		´disable_interaction_methlab´ (delay 0)
		´effect_starting_fire001´ (delay 0)
		´effect_starting_fire002´ (delay 0)
´instance_input_ready´ ElementInstanceInput 100077
	event ready
	on_executed
		´ready´ (delay 0)
´ready´ MissionScriptElement 100078
	on_executed
		´enable_interaction_methlab´ (delay 0)
		´blurzone_start_cooking´ (delay 0)
		´WP_methlab´ (delay 0)
´remove_WP_meth´ ElementOperator 100079
	elements
		1 ´WP_meth´
	operation remove
´enable_interaction_methlab´ ElementUnitSequence 100080
	position -350.0, -450.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_methlab_bubbling/002 (100.999, -250.0, 93.9999)
			notify_unit_sequence interaction_enable
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_methlab_caustic_cooler/002 (88.3954, 129.008, 92.9999)
			notify_unit_sequence interaction_enable
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_methlab_liquid_meth/002 (150.999, 129.0, 92.9999)
			notify_unit_sequence interaction_enable
			time 0
´effect001´ ElementPlayEffect 100081
	base_time 0
	effect effects/payday2/environment/smoke_alex_must_die_1
	max_amount 0
	position 51.0, -209.0, 157.0
	random_time 0
	rotation 0.0, 0.0, 0.0, -1.0
	screen_space False
´effect002´ ElementPlayEffect 100082
	base_time 0
	effect effects/payday2/environment/bubble_alex_must_die_1
	max_amount 0
	position 102.0, -249.0, 212.0
	random_time 0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	screen_space False
´effect003´ ElementPlayEffect 100083
	base_time 0
	effect effects/payday2/environment/smoke_alex_must_die_1
	max_amount 0
	position 23.0, -249.0, 212.0
	random_time 0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	screen_space False
´effect004´ ElementPlayEffect 100084
	base_time 0
	effect effects/payday2/environment/bubble_alex_must_die_1
	max_amount 0
	position 23.0, -239.0, 137.0
	random_time 0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	screen_space False
´effect005´ ElementPlayEffect 100085
	base_time 0
	effect effects/payday2/environment/smoke_alex_must_die_1
	max_amount 0
	position 87.0, 130.0, 162.0
	random_time 0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	screen_space False
´effect006´ ElementPlayEffect 100086
	base_time 0
	effect effects/payday2/environment/bubble_alex_must_die_1
	max_amount 0
	position 182.0, -253.0, 134.0
	random_time 0
	rotation 0.0, 0.0, 0.0, -1.0
	screen_space False
´sound_methlab´ ElementPlaySound 100087
	append_prefix False
	elements
	interrupt True
	position 25.0, -48.0, 50.0
	rotation 0.0, 0.0, 0.0, -1.0
	sound_event cooking_meth
	use_instigator False
´sound_methlab_stop´ ElementPlaySound 100088
	append_prefix False
	elements
	interrupt True
	position 75.0, -48.0, 50.0
	rotation 0.0, 0.0, 0.0, -1.0
	sound_event cooking_meth_stop
	use_instigator False
´effect_stop´ ElementStopEffect 100089
	elements
		1 ´effect002´
		2 ´effect003´
		3 ´effect001´
	operation fade_kill
	position -250.0, -350.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
´effect_fire´ ElementPlayEffect 100090
	base_time 0
	effect effects/payday2/environment/fire_alex_must_die_1
	max_amount 0
	position -15.0, 10.0, 0.0
	random_time 0
	rotation 0.0, 0.0, 0.0, -1.0
	screen_space False
´teammate_comment_swear´ ElementTeammateComment 100091
	close_to_element True
	comment g60
	position 25.0, -48.0, 100.0
	radius 0
	rotation 0.0, 0.0, 0.0, -1.0
	use_instigator False
´explode´ MissionScriptElement 100092
	on_executed
		´effect_fire´ (delay 0)
		´destroy_methlab´ (delay 0)
		´explosion_damage´ (delay 0)
		´enable_area_damage´ (delay 0)
		´blurzone_fire´ (delay 0)
´sound_add_ingredient´ ElementPlaySound 100094
	append_prefix False
	elements
	interrupt True
	position -25.0, -48.0, 50.0
	rotation 0.0, 0.0, 0.0, -1.0
	sound_event add_meth_ingredients
	use_instigator False
´destroy_methlab´ ElementUnitSequence 100095
	position -150.0, -550.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_methlab_bubbling/002 (100.999, -250.0, 93.9999)
			notify_unit_sequence Destroyed
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_methlab_caustic_cooler/002 (88.3954, 129.008, 92.9999)
			notify_unit_sequence interaction_disable
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_methlab_drying_meth/002 (-53.0, -202.0, 0.0)
			notify_unit_sequence interaction_disable
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_methlab_liquid_meth/002 (150.999, 129.0, 92.9999)
			notify_unit_sequence interaction_disable
			time 0
		5
			id 5
			name run_sequence
			notify_unit_id units/payday2/props/gen_prop_methlab_meth/002 (-56.0013, -221.0, 96.9999)
			notify_unit_sequence state_interaction_disabled
			time 0
		6
			id 6
			name run_sequence
			notify_unit_id units/payday2/props/gen_prop_methlab_meth/002 (-56.0013, -221.0, 96.9999)
			notify_unit_sequence state_vis_hide
			time 0
		7
			id 7
			name run_sequence
			notify_unit_id units/payday2/props/gen_prop_methlab_meth/002 (-56.0013, -221.0, 96.9999)
			notify_unit_sequence state_interaction_disabled
			time 0
´blurzone_start_cooking´ ElementBlurZone 100096
	height 20
	mode 1
	position -7.62939e-06, -23.0, 100.0
	radius 320
	rotation 0.0, 0.0, 0.0, -1.0
´blurzone_end´ ElementBlurZone 100097
	height 200
	mode 0
	position -7.62939e-06, -23.0, 50.0
	radius 300
	rotation 0.0, 0.0, 0.0, -1.0
´sound_explosion´ ElementPlaySound 100098
	append_prefix False
	elements
	interrupt True
	position 125.0, -48.0, 50.0
	rotation 0.0, 0.0, 0.0, -1.0
	sound_event explosion_fire
	use_instigator False
´disable_interaction_methlab´ ElementUnitSequence 100000
	position -650.0, -50.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_methlab_bubbling/002 (100.999, -250.0, 93.9999)
			notify_unit_sequence interaction_disable
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_methlab_caustic_cooler/002 (88.3954, 129.008, 92.9999)
			notify_unit_sequence interaction_disable
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_methlab_liquid_meth/002 (150.999, 129.0, 92.9999)
			notify_unit_sequence interaction_disable
			time 0
´explosion_damage´ ElementExplosionDamage 100004
	damage 40
	position 25.0, -48.0, 150.0
	range 450
	rotation 0.0, 0.0, 0.0, -1.0
´area_fire_damage´ ElementAreaTrigger 100005
	DISABLED
	amount 1
	depth 800
	height 300
	instigator player
	instigator_name 
	interval 0.5
	position 25.0, -48.0, 125.0
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	shape_type box
	spawn_unit_elements
	trigger_on both
	use_disabled_shapes False
	width 600
	on_executed
		´kill_zone_fire´ (delay 0)
´enable_area_damage´ ElementToggle 100006
	elements
		1 ´area_fire_damage´ DISABLED
	set_trigger_times -1
	toggle on
´kill_zone_fire´ ElementKillZone 100007
	position 50.0, -550.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	type fire
´blurzone_fire´ ElementBlurZone 100109
	height 200
	mode 2
	position 75.0, 2.0, 100.0
	radius 600
	rotation 0.0, 0.0, 0.0, -1.0
´area_fire_damage_bag´ ElementAreaTrigger 100008
	DISABLED
	amount 1
	depth 800
	height 300
	instigator loot
	instigator_name 
	interval 5
	position 25.0, -48.0, 125.0
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	shape_type box
	spawn_unit_elements
	trigger_on both
	use_disabled_shapes False
	width 600
	on_executed
		´destroy_meth´ (delay 0) DISABLED
´destroy_meth´ ElementCarry 100009
	DISABLED
	operation remove
	type_filter meth
´cook001´ ElementSpawnCivilian 100023
	enemy units/payday2/characters/civ_male_meth_cook_1/civ_male_meth_cook_1
	force_pickup none
	position 50.0, 25.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	state cf_sp_stand_look_up
	team default
´cook002´ ElementSpawnCivilian 100024
	enemy units/payday2/characters/civ_male_meth_cook_1/civ_male_meth_cook_1
	force_pickup none
	position 75.0, -125.0, 0.0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	state cf_sp_stand_look_up
	team default
´instance_input_spawn_cooks´ ElementInstanceInput 100025
	event spawn_cooks
	on_executed
		´spawn_cooks´ (delay 0)
´spawn_cooks´ MissionScriptElement 100026
	on_executed
		´cook002´ (delay 0)
		´cook001´ (delay 0)
´instance_input_kill_cooks´ ElementInstanceInput 100027
	event kill_cooks
	on_executed
		´kill_cooks´ (delay 0)
´kill_cooks´ MissionScriptElement 100028
	on_executed
		´ai_remove_001´ (delay 0)
´ai_remove_001´ ElementAIRemove 100029
	elements
		1 ´cook002´
		2 ´cook001´
	force_ragdoll False
	position -350.0, -850.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	true_death True
	use_instigator False
´effect_starting_fire001´ ElementPlayEffect 100001
	base_time 0
	effect effects/payday2/environment/starting_fire_alex_must_die_1
	max_amount 0
	position 30.0, 117.0, 162.0
	random_time 0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	screen_space False
´effect_starting_fire002´ ElementPlayEffect 100002
	base_time 0
	effect effects/payday2/environment/starting_fire_alex_must_die_1
	max_amount 0
	position 30.0001, -231.0, 162.0
	random_time 0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	screen_space False
