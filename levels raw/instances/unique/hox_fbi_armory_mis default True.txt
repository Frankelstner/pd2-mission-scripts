﻿´opened_armoury_door´ ElementUnitSequenceTrigger 100021
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence done_opened
			unit_id units/pd2_dlc2/architecture/gov_d_int_door_b/001 (-882.0, -27.0, 0.0)
	on_executed
		´allow_armoury_graph´ (delay 0)
		´point_play_sound_001´ (delay 0)
´allow_armoury_graph´ ElementInstanceOutput 100022
	event allow_armoury_graph
	on_executed
		´func_dialogue_001´ (delay 0)
´point_play_sound_001´ ElementPlaySound 100033
	append_prefix False
	elements
	position -900.0, -150.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	sound_event Play_sys_hb2_07
	use_instigator False
´vo_trigger´ ElementAreaTrigger 100034
	TRIGGER TIMES 1
	amount 1
	depth 400
	height 300
	instigator player
	interval 0.1
	position -775.0, 200.0, 125.935
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 700
´func_dialogue_001´ ElementDialogue 100035
	dialogue Play_pln_hb2_14
	execute_on_executed_when_done False
	use_position False
´used_keycard´ ElementUnitSequenceTrigger 100031
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence open_door_keycard
			unit_id units/pd2_dlc2/architecture/gov_d_int_door_b/001 (-882.0, -27.0, 0.0)
	on_executed
		´used_keycard_2´ (delay 0)
´used_keycard_2´ ElementInstanceOutput 100032
	event used_keycard
