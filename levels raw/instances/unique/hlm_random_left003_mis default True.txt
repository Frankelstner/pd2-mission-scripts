﻿´start_here´ MissionScriptElement 100002
	EXECUTE ON STARTUP
	BASE DELAY 1
	on_executed
		´logic_link_004´ (delay 0)
´logic_random_001´ ElementRandom 100003
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´open_left_wall´ (delay 0)
		´close_left_wall´ (delay 0)
´open_left_wall´ MissionScriptElement 100004
	on_executed
		´func_disable_unit_003´ (delay 0)
´close_left_wall´ MissionScriptElement 100005
	on_executed
		´func_disable_unit_002´ (delay 0)
		´func_sequence_001´ (delay 0)
´logic_link_004´ MissionScriptElement 100010
	on_executed
		´logic_random_001´ (delay 0.5)
		´cont´ (delay 0.5)
´func_disable_unit_002´ ElementDisableUnit 100013
	position -900.0, 2700.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/011 (-1750.0, 3175.0, 0.0)
	on_executed
		´func_nav_obstacle_003´ (delay 0)
´func_disable_unit_003´ ElementDisableUnit 100035
	position -1100.0, 2700.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_rooms_4windows_04_piece/002 (-1700.0, 3175.0, 0.0)
´func_nav_obstacle_003´ ElementNavObstacle 100046
	TRIGGER TIMES 1
	obstacle_list
		1
			guis_id 1
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/012 (-1850.0, 3150.0, 0.0)
	operation add
	position -900.0, 2800.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
´cont´ MissionScriptElement 100047
	on_executed
		´logic_random_003´ (delay 1)
		´logic_random_005´ (delay 0.5)
		´cont_2´ (delay 0.5)
´logic_random_003´ ElementRandom 100048
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´logic_link_005´ (delay 0)
		´logic_link_006´ (delay 0)
´logic_link_005´ MissionScriptElement 100049
	on_executed
		´func_disable_unit_004´ (delay 0)
		´func_disable_unit_007´ (delay 0)
		´func_sequence_004´ (delay 0)
´logic_link_006´ MissionScriptElement 100050
	on_executed
		´func_disable_unit_005´ (delay 0)
		´func_disable_unit_006´ (delay 0)
		´func_sequence_003´ (delay 0)
´func_disable_unit_004´ ElementDisableUnit 100056
	position -900.0, 4100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/006 (-750.0, 4500.0, 0.0)
	on_executed
		´func_nav_obstacle_004´ (delay 0)
´func_disable_unit_005´ ElementDisableUnit 100059
	position -900.0, 3700.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/003 (-750.0, 2900.0, 0.0)
	on_executed
		´func_nav_obstacle_005´ (delay 0)
´func_disable_unit_006´ ElementDisableUnit 100062
	position -1000.0, 3700.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/005 (-750.0, 4500.0, 0.0)
´func_disable_unit_007´ ElementDisableUnit 100063
	position -1000.0, 4100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/008 (-750.0, 2900.0, 0.0)
´logic_random_005´ ElementRandom 100086
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´logic_link_009´ (delay 0)
		´logic_link_010´ (delay 0)
´logic_link_009´ MissionScriptElement 100087
	on_executed
		´func_disable_unit_011´ (delay 0)
´logic_link_010´ MissionScriptElement 100088
	on_executed
		´func_disable_unit_010´ (delay 0)
		´func_sequence_002´ (delay 0)
´func_disable_unit_010´ ElementDisableUnit 100090
	position -900.0, 3000.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/012 (-1750.0, 4075.0, 0.0)
	on_executed
		´func_nav_obstacle_008´ (delay 0)
´func_disable_unit_011´ ElementDisableUnit 100091
	position -1000.0, 3300.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_rooms_4windows_01_piece/005 (-1700.0, 4075.0, 0.0)
´func_nav_obstacle_008´ ElementNavObstacle 100093
	TRIGGER TIMES 1
	obstacle_list
		1
			guis_id 1
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/013 (-1850.0, 4050.0, 0.0)
	operation add
	position -1000.0, 3000.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
´cont_2´ MissionScriptElement 100097
	on_executed
		´logic_random_006´ (delay 0)
´logic_random_006´ ElementRandom 100098
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´logic_link_012´ (delay 0)
		´left´ (delay 0)
´func_disable_unit_013´ ElementDisableUnit 100099
	position -100.0, 3200.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken_doubleside/002 (225.0, 4700.0, 0.0)
		2 units/payday2/architecture/res_ext_apartment_rooms_3windows_floor_3_in_gap/002 (250.0, 2900.0, 300.0)
	on_executed
		´func_nav_obstacle_012´ (delay 0)
´logic_link_012´ MissionScriptElement 100101
	on_executed
		´func_disable_unit_013´ (delay 0)
		´cont_3´ (delay 0)
´left´ MissionScriptElement 100102
	on_executed
		´func_disable_unit_001´ (delay 0)
		´cont_3´ (delay 0)
´cont_3´ MissionScriptElement 100109
	on_executed
		´cont_4´ (delay 0.5)
		´cont_5´ (delay 0.5)
		´logic_link_015´ (delay 0)
´logic_link_015´ MissionScriptElement 100111
	on_executed
		´func_disable_unit_015´ (delay 0)
		´logic_toggle_001´ (delay 0)
		´logic_link_022´ (delay 1)
´func_disable_unit_015´ ElementDisableUnit 100113
	position 900.0, 4600.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/004 (1250.0, 4900.0, 0.0)
	on_executed
		´func_nav_obstacle_014´ (delay 0)
´cont_4´ MissionScriptElement 100115
	on_executed
		´logic_random_009´ (delay 0)
´logic_random_009´ ElementRandom 100116
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´logic_link_016´ (delay 0)
		´nope´ (delay 0)
´logic_link_016´ MissionScriptElement 100117
	on_executed
		´func_disable_unit_016´ (delay 0)
		´logic_link_013´ (delay 0)
		´logic_toggle_002´ (delay 0)
´nope´ MissionScriptElement 100118
	on_executed
		´func_disable_unit_018´ (delay 0)
		´wardrobe_close´ (delay 0)
´func_disable_unit_016´ ElementDisableUnit 100119
	position 900.0, 3100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/001 (1250.0, 3300.0, 0.0)
´func_disable_unit_018´ ElementDisableUnit 100122
	position 900.0, 3200.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/009 (1250.0, 3300.0, 0.0)
	on_executed
		´func_nav_obstacle_015´ (delay 0)
´cont_5´ MissionScriptElement 100121
	on_executed
		´logic_random_010´ (delay 0)
		´cont_6´ (delay 0.5)
´logic_random_010´ ElementRandom 100123
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´logic_link_017´ (delay 0)
		´logic_link_018´ (delay 0)
´logic_link_017´ MissionScriptElement 100124
	on_executed
		´func_disable_unit_019´ (delay 0)
´logic_link_018´ MissionScriptElement 100125
	on_executed
		´func_disable_unit_020´ (delay 0)
´func_disable_unit_019´ ElementDisableUnit 100126
	position 1600.0, 4600.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/009 (1650.0, 4500.0, 0.0)
		2 units/payday2/architecture/res_ext_apartment_rooms_4windows_03_piece/007 (1650.0, 4450.0, 0.0)
		3 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/010 (1650.0, 3600.0, 0.0)
	on_executed
		´func_nav_obstacle_011´ (delay 0)
´func_disable_unit_020´ ElementDisableUnit 100127
	position 1400.0, 4400.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/007 (1650.0, 3600.0, 0.0)
		2 units/payday2/architecture/res_ext_apartment_rooms_4windows_02_piece/004 (1650.0, 3550.0, 0.0)
		3 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/007 (1650.0, 4500.0, 0.0)
	on_executed
		´func_nav_obstacle_016´ (delay 0)
´cont_6´ MissionScriptElement 100130
	on_executed
		´cont_7´ (delay 0)
		´logic_random_012´ (delay 0)
´cont_7´ MissionScriptElement 100131
	on_executed
		´logic_random_011´ (delay 0)
		´cont_8´ (delay 0)
´nope_2´ MissionScriptElement 100132
	on_executed
		´func_sequence_005´ (delay 0)
		´func_disable_unit_012´ (delay 0)
´logic_link_019´ MissionScriptElement 100133
	on_executed
		´func_disable_unit_021´ (delay 0)
´logic_link_021´ MissionScriptElement 100135
	on_executed
		´func_disable_unit_022´ (delay 0)
´logic_random_011´ ElementRandom 100136
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´logic_link_021´ (delay 0)
		´nope_4´ (delay 0)
´logic_random_012´ ElementRandom 100137
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´nope_2´ (delay 0)
		´logic_link_019´ (delay 0)
´func_disable_unit_021´ ElementDisableUnit 100138
	position 2400.0, 4300.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_rooms_4windows_02_piece/002 (2200.0, 4025.0, 0.0)
´func_disable_unit_022´ ElementDisableUnit 100139
	position 2400.0, 3500.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_rooms_4windows_01_piece/001 (2200.0, 3125.0, 0.0)
´func_nav_obstacle_017´ ElementNavObstacle 100140
	TRIGGER TIMES 1
	obstacle_list
		1
			guis_id 1
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/009 (2350.0, 3150.0, 0.0)
	operation add
	position 2500.0, 3500.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
´func_nav_obstacle_018´ ElementNavObstacle 100141
	TRIGGER TIMES 1
	obstacle_list
		1
			guis_id 1
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/006 (2350.0, 4050.0, 0.0)
	operation add
	position 2475.0, 4300.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
´cont_8´ MissionScriptElement 100134
	on_executed
		´logic_random_013´ (delay 0)
´logic_random_013´ ElementRandom 100142
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´nope_3´ (delay 0)
		´logic_link_020´ (delay 0)
´nope_3´ MissionScriptElement 100143
	on_executed
		´func_disable_unit_023´ (delay 0)
´logic_link_020´ MissionScriptElement 100144
	on_executed
		´func_disable_unit_024´ (delay 0)
´func_disable_unit_024´ ElementDisableUnit 100147
	position 2200.0, 2800.0, 0.0
	rotation 0.0, 0.0, 0.707107, -0.707107
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_rooms_4windows_01_piece/007 (1650.0, 2650.0, 0.0)
		2 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/008 (1650.0, 2700.0, 0.0)
´func_disable_unit_023´ ElementDisableUnit 100145
	position 2200.0, 3000.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/006 (1650.0, 2700.0, 0.0)
	on_executed
		´func_nav_obstacle_020´ (delay 0)
´logic_link_013´ MissionScriptElement 100151
	on_executed
		´func_disable_unit_025´ (delay 0)
´func_disable_unit_025´ ElementDisableUnit 100152
	position 1400.0, 4000.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/ext_apartment/res_apartment_blockade_01/001 (1475.0, 4150.0, -9.00006)
	on_executed
		´func_nav_obstacle_019´ (delay 0)
´func_nav_obstacle_019´ ElementNavObstacle 100153
	TRIGGER TIMES 1
	obstacle_list
		1
			guis_id 2
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/011 (1450.0, 2725.0, 0.0)
	operation add
	position 1500.0, 4000.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
´logic_link_022´ MissionScriptElement 100154
	on_executed
		´func_disable_unit_026´ (delay 0)
´func_disable_unit_026´ ElementDisableUnit 100155
	position 1400.0, 3800.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/ext_apartment/res_apartment_blockade_01/002 (1475.0, 2700.0, 0.0)
	on_executed
		´func_nav_obstacle_021´ (delay 0)
´func_nav_obstacle_021´ ElementNavObstacle 100156
	TRIGGER TIMES 1
	obstacle_list
		1
			guis_id 1
			obj_name 27702f552c996d1e
			unit_id units/payday2/architecture/ext_apartment/res_apartment_blockade_01/001 (1475.0, 4150.0, -9.00006)
	operation add
	position 1500.0, 3800.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
´nope_4´ MissionScriptElement 100157
	on_executed
		´wardrobe_close_2´ (delay 0)
		´func_disable_unit_014´ (delay 0)
´logic_toggle_001´ ElementToggle 100158
	elements
		1 ´nope_2´
	set_trigger_times -1
	toggle off
´logic_toggle_002´ ElementToggle 100160
	elements
		1 ´nope_4´
		2 ´logic_link_022´
	set_trigger_times -1
	toggle off
´point_spawn_player_001´ ElementPlayerSpawner 100054
	EXECUTE ON STARTUP
	DISABLED
	position -1000.0, 2700.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	state standard
´func_nav_obstacle_015´ ElementNavObstacle 100120
	TRIGGER TIMES 1
	obstacle_list
		1
			guis_id 1
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/008 (1225.0, 3000.0, 0.0)
	operation add
	position 800.0, 3300.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
´func_nav_obstacle_014´ ElementNavObstacle 100114
	TRIGGER TIMES 1
	obstacle_list
	operation remove
	position 900.0, 4700.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
´func_nav_obstacle_012´ ElementNavObstacle 100104
	TRIGGER TIMES 1
	obstacle_list
		1
			guis_id 1
			obj_name 206e28d8fb71672e
			unit_id units/payday2/architecture/res_ext_apartment_rooms_3windows_floor_3_in_gap/001 (250.0, 4900.0, 300.0)
	operation add
	position 0.0, 3200.0, 4.57764e-05
	rotation 0.0, 0.0, 0.0, -1.0
´func_nav_obstacle_005´ ElementNavObstacle 100061
	TRIGGER TIMES 1
	obstacle_list
		1
			guis_id 1
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/014 (-725.0, 4700.0, 0.0)
	operation add
	position -900.0, 3600.0, -0.000617981
	rotation 0.0, 0.0, 0.0, -1.0
´func_nav_obstacle_004´ ElementNavObstacle 100060
	TRIGGER TIMES 1
	obstacle_list
		1
			guis_id 1
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/015 (-725.0, 3200.0, 0.0)
	operation add
	position -900.0, 4200.0, -0.000602722
	rotation 0.0, 0.0, 0.0, -1.0
´func_nav_obstacle_011´ ElementNavObstacle 100128
	TRIGGER TIMES 1
	obstacle_list
		1
			guis_id 1
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/017 (1675.0, 3800.0, 0.0)
	operation add
	position 1600.0, 4500.0, -0.000617981
	rotation 0.0, 0.0, 0.0, -1.0
´func_nav_obstacle_016´ ElementNavObstacle 100129
	TRIGGER TIMES 1
	obstacle_list
		1
			guis_id 1
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/016 (1675.0, 4700.0, 0.0)
	operation add
	position 1400.0, 4300.0, -0.000579834
	rotation 0.0, 0.0, 0.0, -1.0
´func_nav_obstacle_020´ ElementNavObstacle 100148
	TRIGGER TIMES 1
	obstacle_list
		1
			guis_id 1
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker_4x1/010 (1650.0, 2900.0, 0.0)
	operation add
	position 2100.0, 3000.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
´func_sequence_001´ ElementUnitSequence 100029
	position -1000.0, 2600.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/architecture/res_ext_apartment_wardrobe_variation/004 (-2150.0, 3150.0, 0.0)
			notify_unit_sequence closed
			time 0
´func_sequence_002´ ElementUnitSequence 100089
	position -1000.0, 3100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/architecture/res_ext_apartment_wardrobe_variation/003 (-2150.0, 4050.0, 0.0)
			notify_unit_sequence open
			time 0
´func_sequence_003´ ElementUnitSequence 100092
	position -800.0, 3800.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/architecture/res_ext_apartment_wardrobe_variation/002 (-700.0, 4500.0, 0.0)
			notify_unit_sequence open
			time 0
´func_sequence_004´ ElementUnitSequence 100094
	position -800.0, 4000.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/architecture/res_ext_apartment_wardrobe_variation/008 (-700.0, 2900.0, 0.0)
			notify_unit_sequence closed
			time 0
´func_disable_unit_001´ ElementDisableUnit 100103
	position -100.0, 4600.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken_doubleside/001 (225.0, 3500.0, 0.0)
		2 units/payday2/architecture/res_ext_apartment_rooms_3windows_floor_3_in_gap/001 (250.0, 4900.0, 300.0)
	on_executed
		´func_nav_obstacle_001´ (delay 0)
´func_nav_obstacle_001´ ElementNavObstacle 100105
	obstacle_list
		1
			guis_id 1
			obj_name 206e28d8fb71672e
			unit_id units/payday2/architecture/res_ext_apartment_rooms_3windows_floor_3_in_gap/002 (250.0, 2900.0, 300.0)
	operation add
	position 0.0, 4600.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
´wardrobe_close´ ElementUnitSequence 100106
	position 900.0, 3300.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/architecture/res_ext_apartment_wardrobe_variation/006 (1200.0, 3300.0, 0.0)
			notify_unit_sequence closed
			time 0
´func_sequence_005´ ElementUnitSequence 100107
	position 2600.0, 4300.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/architecture/res_ext_apartment_wardrobe_variation/007 (2650.0, 4050.0, 0.0)
			notify_unit_sequence open
			time 0
´func_disable_unit_012´ ElementDisableUnit 100108
	position 2600.0, 4200.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/013 (2250.0, 4025.0, 0.0)
	on_executed
		´func_nav_obstacle_018´ (delay 0)
´wardrobe_close_2´ ElementUnitSequence 100040
	position 2600.0, 3500.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/architecture/res_ext_apartment_wardrobe_variation/001 (2650.0, 3150.0, 0.0)
			notify_unit_sequence closed
			time 0
´func_disable_unit_014´ ElementDisableUnit 100110
	position 2600.0, 3400.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/014 (2250.0, 3125.0, 0.0)
	on_executed
		´func_nav_obstacle_017´ (delay 0)
´area_door_seen001´ ElementAreaTrigger 100011
	DISABLED
	TRIGGER TIMES 1
	amount 1
	depth 500
	height 500
	instigator player
	interval 0.1
	position -600.0, 3900.0, 0.0
	radius 250
	rotation 0.0, 0.0, -0.707107, -0.707107
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 405
	on_executed
		´start_open_door´ (delay 0)
		´enable_comments001´ (delay 0)
		´point_waypoint_001´ (delay 0)
		´enable_instance_output_door_opened´ (delay 0)
´start_open_door´ MissionScriptElement 100165
	on_executed
		´start_new_obj´ (delay 0)
		´bain_door_seen´ (delay 0)
´door001´ ElementUnitSequence 100180
	EXECUTE ON STARTUP
	BASE DELAY 1
	position -1000.0, 5500.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_miami/props/gen_prop_door_metal_magnetic/001 (-450.0, 4100.0, 2.28882e-05)
			notify_unit_sequence anim_door_close
			time 0
	on_executed
		´nav_door_add001´ (delay 0)
		´start_blocking_path001´ (delay 0)
		´disable_stuff_door002´ (delay 0)
		´enable_stuff_door001´ (delay 0)
		´hide_circuit_breaker´ (delay 0)
		´random_box_location001´ (delay 0.5)
´nav_door_add001´ ElementNavObstacle 100183
	obstacle_list
		1
			guis_id 1
			obj_name d6780cea5f022bc0
			unit_id units/dev_tools/level_tools/door_blocker_1m/001 (-450.0, 3894.0, 0.0)
	operation add
	position -1100.0, 5500.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
´nav_door_remove001´ ElementNavObstacle 100185
	obstacle_list
		1
			guis_id 1
			obj_name d6780cea5f022bc0
			unit_id units/dev_tools/level_tools/door_blocker_1m/001 (-450.0, 3894.0, 0.0)
	operation remove
	position -2400.0, 5500.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
´start_blocking_path001´ MissionScriptElement 100009
	on_executed
		´func_disable_unit_017´ (delay 0)
		´func_nav_obstacle_002´ (delay 0)
		´set_walls001´ (delay 0)
´enable_stuff_door001´ ElementToggle 100043
	elements
		1 ´area_door_seen001´ DISABLED
	set_trigger_times -1
	toggle on
´func_disable_unit_017´ ElementDisableUnit 100079
	position -1300.0, 5500.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/005 (-750.0, 4500.0, 0.0)
		2 units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/008 (-750.0, 2900.0, 0.0)
		3 units/payday2/architecture/res_ext_apartment_power_cable_04/001 (250.0, 1900.0, 0.0)
		4 units/payday2/architecture/res_ext_apartment_power_cable_05/001 (251.0, 1900.0, 0.0)
		5 units/payday2/architecture/res_ext_apartment_power_cable_06/001 (253.0, 1899.0, 0.0)
´func_nav_obstacle_002´ ElementNavObstacle 100080
	obstacle_list
	operation add
	position -1400.0, 5500.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´func_nav_obstacle_004´ (delay 0)
		´func_nav_obstacle_005´ (delay 0)
´set_walls001´ ElementUnitSequence 100159
	position -1700.0, 5500.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/architecture/res_ext_apartment_wardrobe_variation/002 (-700.0, 4500.0, 0.0)
			notify_unit_sequence closed
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/architecture/res_ext_apartment_wardrobe_variation/008 (-700.0, 2900.0, 0.0)
			notify_unit_sequence open
			time 0
´random_box_location001´ ElementRandom 100191
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´func_sequence_006´ (delay 0)
		´func_sequence_007´ (delay 0)
		´func_sequence_008´ (delay 0)
´seq_open_door´ ElementUnitSequence 100195
	TRIGGER TIMES 1
	position -2300.0, 5600.0, 0.0
	rotation 0.0, 0.0, 0.707107, -0.707107
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_miami/props/gen_prop_door_metal_magnetic/001 (-450.0, 4100.0, 2.28882e-05)
			notify_unit_sequence anim_door_open
			time 0
´point_debug_004´ ElementDebug 100196
	as_subtitle True
	debug_string Bain: "The door is open! Good jerb."
	show_instigator False
´disable_area_door001´ ElementToggle 100198
	elements
		1 ´area_door_seen001´ DISABLED
		2 ´start_new_obj´
		3 ´instance_output_old_obj´ DISABLED
	set_trigger_times -1
	toggle off
´disable_stuff_door002´ ElementToggle 100199
	elements
		1 ´logic_random_003´
		2 ´point_debug_004´
	set_trigger_times -1
	toggle off
´enable_comments001´ ElementToggle 100221
	elements
		1 ´point_debug_004´
	set_trigger_times -1
	toggle on
´start_new_obj´ MissionScriptElement 100007
	on_executed
		´func_instance_output_001´ (delay 0)
´func_instance_output_001´ ElementInstanceOutput 100020
	event cable_door_seen
´instance_output_old_obj´ ElementInstanceOutput 100051
	DISABLED
	event cable_door_opened
´open_door´ MissionScriptElement 100095
	TRIGGER TIMES 1
	on_executed
		´nav_door_remove001´ (delay 0)
		´disable_area_door001´ (delay 0.01)
		´instance_output_old_obj´ (delay 0) DISABLED
		´seq_open_door´ (delay 0)
		´point_debug_004´ (delay 0)
		´logic_operator_001´ (delay 0)
		´func_experience_001´ (delay 0)
´func_sequence_006´ ElementUnitSequence 100186
	position -1900.0, 5600.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/002 (-1684.0, 4499.0, 100.0)
			notify_unit_sequence state_interaction_enable
			time 0
	on_executed
		´logic_toggle_003´ (delay 0)
		´func_enable_unit_001´ (delay 0)
´func_sequence_007´ ElementUnitSequence 100187
	position -1900.0, 5500.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/003 (-1684.0, 3600.0, 100.0)
			notify_unit_sequence state_interaction_enable
			time 0
	on_executed
		´logic_toggle_004´ (delay 0)
		´func_enable_unit_002´ (delay 0)
´func_sequence_008´ ElementUnitSequence 100189
	position -1900.0, 5400.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/004 (-1684.0, 2700.0, 100.0)
			notify_unit_sequence state_interaction_enable
			time 0
	on_executed
		´logic_toggle_005´ (delay 0)
		´func_enable_unit_003´ (delay 0)
´hide_circuit_breaker´ ElementDisableUnit 100197
	position -1100.0, 5600.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/002 (-1684.0, 4499.0, 100.0)
		2 units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/003 (-1684.0, 3600.0, 100.0)
		3 units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/004 (-1684.0, 2700.0, 100.0)
´open_circuit_breaker´ ElementUnitSequenceTrigger 100190
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/002 (-1684.0, 4499.0, 100.0)
		2
			guis_id 2
			sequence interact
			unit_id units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/003 (-1684.0, 3600.0, 100.0)
		3
			guis_id 3
			sequence interact
			unit_id units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/004 (-1684.0, 2700.0, 100.0)
	on_executed
		´interacted_3_times´ (delay 0)
		´logic_counter_filter_001´ (delay 0.5)
		´logic_counter_filter_002´ (delay 0.5)
		´point_debug_002´ (delay 0)
		´logic_counter_filter_003´ (delay 0.5)
´interacted_3_times´ ElementCounter 100192
	counter_target 3
	digital_gui_unit_ids
´state_remove_cover001´ ElementUnitSequence 100200
	DISABLED
	position -2150.0, 5800.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/002 (-1684.0, 4499.0, 100.0)
			notify_unit_sequence hud_text_remove_cover
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/002 (-1684.0, 4499.0, 100.0)
			notify_unit_sequence state_interaction_enable
			time 0
´logic_toggle_003´ ElementToggle 100194
	elements
		1 ´state_remove_cover001´ DISABLED
		2 ´state_cut_cable001´ DISABLED
	set_trigger_times -1
	toggle on
´logic_toggle_004´ ElementToggle 100201
	elements
		1 ´state_remove_cover002´ DISABLED
		2 ´state_cut_cable002´ DISABLED
	set_trigger_times -1
	toggle on
´logic_toggle_005´ ElementToggle 100202
	elements
		1 ´state_remove_cover003´ DISABLED
		2 ´state_cut_cable003´ DISABLED
	set_trigger_times -1
	toggle on
´state_remove_cover002´ ElementUnitSequence 100203
	DISABLED
	position -2050.0, 5800.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/003 (-1684.0, 3600.0, 100.0)
			notify_unit_sequence hud_text_remove_cover
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/003 (-1684.0, 3600.0, 100.0)
			notify_unit_sequence state_interaction_enable
			time 0
´state_remove_cover003´ ElementUnitSequence 100204
	DISABLED
	position -1950.0, 5800.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/004 (-1684.0, 2700.0, 100.0)
			notify_unit_sequence hud_text_remove_cover
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/004 (-1684.0, 2700.0, 100.0)
			notify_unit_sequence state_interaction_enable
			time 0
´logic_counter_filter_001´ ElementCounterFilter 100205
	check_type equal
	elements
		1 ´interacted_3_times´
	needed_to_execute all
	value 1
	on_executed
		´state_cut_cable001´ (delay 0) DISABLED
		´state_cut_cable002´ (delay 0) DISABLED
		´state_cut_cable003´ (delay 0) DISABLED
´logic_counter_filter_002´ ElementCounterFilter 100206
	check_type equal
	elements
		1 ´interacted_3_times´
	needed_to_execute all
	value 2
	on_executed
		´state_remove_cover001´ (delay 0) DISABLED
		´state_remove_cover002´ (delay 0) DISABLED
		´state_remove_cover003´ (delay 0) DISABLED
´state_cut_cable001´ ElementUnitSequence 100207
	DISABLED
	position -2150.0, 5900.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/002 (-1684.0, 4499.0, 100.0)
			notify_unit_sequence hud_text_cut_cable
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/002 (-1684.0, 4499.0, 100.0)
			notify_unit_sequence state_interaction_enable
			time 0
´func_enable_unit_001´ ElementEnableUnit 100208
	position -1850.0, 5600.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/002 (-1684.0, 4499.0, 100.0)
		2 units/payday2/architecture/res_ext_apartment_power_cable_06/001 (253.0, 1899.0, 0.0)
´func_enable_unit_002´ ElementEnableUnit 100209
	position -1850.0, 5500.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/003 (-1684.0, 3600.0, 100.0)
		2 units/payday2/architecture/res_ext_apartment_power_cable_05/001 (251.0, 1900.0, 0.0)
´func_enable_unit_003´ ElementEnableUnit 100210
	position -1850.0, 5400.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/004 (-1684.0, 2700.0, 100.0)
		2 units/payday2/architecture/res_ext_apartment_power_cable_04/001 (250.0, 1900.0, 0.0)
´point_debug_002´ ElementDebug 100211
	as_subtitle False
	debug_string 1
	show_instigator False
´logic_counter_filter_003´ ElementCounterFilter 100212
	check_type equal
	elements
		1 ´interacted_3_times´
	needed_to_execute all
	value 0
	on_executed
		´open_door´ (delay 0)
		´point_debug_003´ (delay 0)
´point_debug_003´ ElementDebug 100213
	as_subtitle False
	debug_string 0
	show_instigator False
´state_cut_cable002´ ElementUnitSequence 100214
	DISABLED
	position -2050.0, 5900.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/003 (-1684.0, 3600.0, 100.0)
			notify_unit_sequence hud_text_cut_cable
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/003 (-1684.0, 3600.0, 100.0)
			notify_unit_sequence state_interaction_enable
			time 0
´state_cut_cable003´ ElementUnitSequence 100215
	DISABLED
	position -1950.0, 5900.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/004 (-1684.0, 2700.0, 100.0)
			notify_unit_sequence hud_text_cut_cable
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/004 (-1684.0, 2700.0, 100.0)
			notify_unit_sequence state_interaction_enable
			time 0
´point_waypoint_001´ ElementWaypoint 100067
	icon pd2_door
	only_in_civilian False
	position -449.0, 3893.0, 125.0
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´logic_operator_001´ ElementOperator 100070
	elements
		1 ´point_waypoint_001´
	operation remove
´bain_door_seen´ ElementDialogue 100075
	dialogue Play_pln_hm2_28
	execute_on_executed_when_done False
	use_position False
´enable_instance_output_door_opened´ ElementToggle 100083
	elements
		1 ´instance_output_old_obj´ DISABLED
	set_trigger_times -1
	toggle on
´func_experience_001´ ElementExperience 100150
	amount 2000
	on_executed
		´point_debug_001´ (delay 0)
´point_debug_001´ ElementDebug 100164
	as_subtitle False
	debug_string + 2000
	show_instigator False
