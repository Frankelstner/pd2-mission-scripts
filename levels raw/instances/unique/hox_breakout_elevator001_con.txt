ID range vs continent name:
	100000: world

statics
	100022 units/dev_tools/level_tools/dev_collision_1m_2_bag/001 (-150.0, -25.0, -25.0)
	100023 units/dev_tools/level_tools/dev_collision_1m_2_bag/002 (-50.0, -25.0, -25.0)
	100002 units/dev_tools/level_tools/door_blocker/001 (25.0, -50.0, 50.0)
	100021 units/dev_tools/level_tools/navigation_splitter/001 (25.0, -50.0, -50.0)
	100001 units/pd2_dlc2/architecture/gov_c_int_elevator/001 (0.0, 0.0, 0.0)
		disable_on_ai_graph True
	100000 units/pd2_dlc2/architecture/gov_c_int_elevator_inside/001 (0.0, 0.0, 0.0)
		mesh_variation enable_hatch
