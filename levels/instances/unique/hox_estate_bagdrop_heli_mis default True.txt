﻿´heli_setup´ ElementUnitSequence 100005
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 2.65)
	Execute sequence: "hidden" - units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/bagdrop_heli (100.0, 0.0, 0.0)
´picked_up_lance´ ElementLootBagTrigger 100003
	Upon event "load" acting on ´lance_bag´:
		remove ´point_waypoint_001´
		Send instance event "picked_up_lance" to mission.
		Make teammate close to instigator say: g61
´lance_bag´ ElementLootBag 100001
	Spawn loot bag "lance_bag" at position (-6.0, 25.0, 1800.0)
´point_waypoint_001´ ElementWaypoint 100002
	Place waypoint with icon "pd2_generic_interact" at position (0.0, 0.0, 25.0)
´flyin_and_drop´ ElementInstanceInput 100007
	Upon mission event "flyin_and_drop":
		Execute sequence: "diamondheist" - units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/bagdrop_heli (100.0, 0.0, 0.0)
		Execute sequence: "flyin_fwd_hover" - units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/bagdrop_heli (100.0, 0.0, 0.0)
		Execute sequence: "open_door_left" - units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/bagdrop_heli (100.0, 0.0, 0.0) (DELAY 30)
		Send instance event "wait_complete" to mission. (DELAY 32)
		´lance_bag´ (DELAY 33)
		´point_waypoint_001´ (DELAY 33.5)
		Execute sequence: "hover_flyout_back" - units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/bagdrop_heli (100.0, 0.0, 0.0) (DELAY 34.5)
		Execute sequence: "hidden" - units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/bagdrop_heli (100.0, 0.0, 0.0) (DELAY 59.5)
