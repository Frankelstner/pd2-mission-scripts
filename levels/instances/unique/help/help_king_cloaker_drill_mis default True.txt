﻿´Auto_Start´ ElementCounterOperator 100020
	EXECUTE ON STARTUP
	´set_attack（1）´ = 1
´set_attack（1）´ ElementCounter 100036
´enter´ ElementInstanceInput 100000
	Upon mission event "enter":
		´if_entered´
´if_entered´ MissionScriptElement 100003
	TRIGGER TIMES 1
	Toggle on: ´shake_loop_2_second´
	Toggle on: ´Shake_loop_Start´
	If ´set_attack（1）´ == 1:
		Execute sequence: "anim_enter_drills_enabled" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_1_close (170.0, 1100.0, -2100.0)
	If ´set_attack（1）´ == 2:
		Execute sequence: "anim_enter_drills_enabled" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_2_medium (170.0, 2300.0, -2126.67)
	If ´set_attack（1）´ == 3:
		Execute sequence: "anim_enter_drills_enabled" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_3_far (369.999, 3100.0, -2120.06)
	If ´set_attack（1）´ == 4:
		Execute sequence: "anim_enter_drills_enabled" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_1_close_single (1776.98, 3050.96, -2498.48)
	Toggle on, trigger times 1: ´if_entered_2´ (DELAY 5)
´if_entered_2´ MissionScriptElement 100002
	DISABLED
	TRIGGER TIMES 1
	Toggle off: ´if_entered_2´ (DELAY 0.1)
	Toggle on, trigger times 1: ´if_entered´ (DELAY 5)
´shake_loop_2_second´ MissionScriptElement 100034
	Screen shake.
	Screen shake.
	Screen shake.
	Explosion damage 50 with range 400 at position (0.0, 0.0, 125.0)
	´Shake_loop_Start´ (DELAY 1)
´Shake_loop_Start´ MissionScriptElement 100050
	´shake_loop_2_second´
´exit´ ElementInstanceInput 100001
	Upon mission event "exit":
		´if_entered_2´
´drill_dmg_area´ ElementAreaTrigger 100040
	DISABLED
	Cylinder, radius 420.0, height 1000.0, at position (0.0, 0.0, 400.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.2
	Upon both, execute:
		Deal gas damage to instigator.
´Intro_Finished´ ElementUnitSequenceTrigger 100005
	Upon any sequence:
		"anim_enter_drills_finished" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_1_close (170.0, 1100.0, -2100.0)
		"anim_enter_drills_finished" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_2_medium (170.0, 2300.0, -2126.67)
		"anim_enter_drills_finished" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_3_far (369.999, 3100.0, -2120.06)
		"anim_enter_drills_finished" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_1_close_single (1776.98, 3050.96, -2498.48)
	Execute:
		Debug message: Drill_Attack
		If ´set_attack（1）´ == 1:
			Execute sequence: "anim_drill_attack_close_enter" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_1_close (170.0, 1100.0, -2100.0)
			Play audio "Play_big_clk_help_03" at position (0.0, 0.0, 1800.0)
			Execute sequence: "anim_drill_attack_close_exit" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_1_close (170.0, 1100.0, -2100.0) (DELAY 12-17)
			Toggle off: ´drill_dmg_area´ (DELAY 12-17)
			Toggle off: ´shake_loop_2_second´ (DELAY 12-17)
			Toggle off: ´Shake_loop_Start´ (DELAY 12-17)
			Stop effect: ´drills_big´ (DELAY 12-17)
			Stop effect: ´heli_smoke´ (DELAY 12-17)
			Execute sequence: "state_vis_hide" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_1_close (170.0, 1100.0, -2100.0) (DELAY 18-23)
			Execute sequence: "state_vis_hide" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_2_medium (170.0, 2300.0, -2126.67) (DELAY 18-23)
			Execute sequence: "state_vis_hide" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_3_far (369.999, 3100.0, -2120.06) (DELAY 18-23)
		If ´set_attack（1）´ == 2:
			Execute sequence: "anim_drill_attack_medium_enter" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_2_medium (170.0, 2300.0, -2126.67)
			Play audio "Play_big_clk_help_03" at position (0.0, 0.0, 1800.0)
			Execute sequence: "anim_drill_attack_medium_exit" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_2_medium (170.0, 2300.0, -2126.67) (DELAY 12-17)
			Toggle off: ´drill_dmg_area´ (DELAY 12-17)
			Toggle off: ´shake_loop_2_second´ (DELAY 12-17)
			Toggle off: ´Shake_loop_Start´ (DELAY 12-17)
			Stop effect: ´drills_big´ (DELAY 12-17)
			Stop effect: ´heli_smoke´ (DELAY 12-17)
			Execute sequence: "state_vis_hide" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_1_close (170.0, 1100.0, -2100.0) (DELAY 18-23)
			Execute sequence: "state_vis_hide" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_2_medium (170.0, 2300.0, -2126.67) (DELAY 18-23)
			Execute sequence: "state_vis_hide" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_3_far (369.999, 3100.0, -2120.06) (DELAY 18-23)
		If ´set_attack（1）´ == 3:
			Execute sequence: "anim_drill_attack_far_enter" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_3_far (369.999, 3100.0, -2120.06)
			Play audio "Play_big_clk_help_03" at position (0.0, 0.0, 1800.0)
			Execute sequence: "anim_drill_attack_far_exit" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_3_far (369.999, 3100.0, -2120.06) (DELAY 12-17)
			Toggle off: ´drill_dmg_area´ (DELAY 12-17)
			Toggle off: ´shake_loop_2_second´ (DELAY 12-17)
			Toggle off: ´Shake_loop_Start´ (DELAY 12-17)
			Stop effect: ´drills_big´ (DELAY 12-17)
			Stop effect: ´heli_smoke´ (DELAY 12-17)
			Execute sequence: "state_vis_hide" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_1_close (170.0, 1100.0, -2100.0) (DELAY 18-23)
			Execute sequence: "state_vis_hide" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_2_medium (170.0, 2300.0, -2126.67) (DELAY 18-23)
			Execute sequence: "state_vis_hide" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_3_far (369.999, 3100.0, -2120.06) (DELAY 18-23)
		If ´set_attack（1）´ == 4:
			Execute sequence: "anim_drill_attack_single_enter" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_1_close_single (1776.98, 3050.96, -2498.48)
			Play audio "Play_big_clk_help_03" at position (0.0, 0.0, 1800.0)
			Execute sequence: "anim_drill_attack_single_exit" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_1_close_single (1776.98, 3050.96, -2498.48) (DELAY 12-17)
			Toggle off: ´drill_dmg_area´ (DELAY 12-17)
			Toggle off: ´shake_loop_2_second´ (DELAY 12-17)
			Toggle off: ´Shake_loop_Start´ (DELAY 12-17)
			Stop effect: ´drills_big´ (DELAY 12-17)
			Stop effect: ´heli_smoke´ (DELAY 12-17)
			Execute sequence: "state_vis_hide" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_1_close (170.0, 1100.0, -2100.0) (DELAY 18-23)
			Execute sequence: "state_vis_hide" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_2_medium (170.0, 2300.0, -2126.67) (DELAY 18-23)
			Execute sequence: "state_vis_hide" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_3_far (369.999, 3100.0, -2120.06) (DELAY 18-23)
´drills_big´ ElementPlayEffect 100007
	Play effect effects/payday2/environment/drills_big at position (0.0, 0.0, 75.0)
´heli_smoke´ ElementPlayEffect 100061
	Play effect effects/particles/dest/bridge_heli_smoke at position (0.0, 0.0, 75.0)
´Attack_HIT´ ElementUnitSequenceTrigger 100017
	Upon any sequence:
		"anim_drill_hit" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_1_close (170.0, 1100.0, -2100.0)
		"anim_drill_hit" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_2_medium (170.0, 2300.0, -2126.67)
		"anim_drill_hit" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_3_far (369.999, 3100.0, -2120.06)
		"anim_drill_hit" - units/pd2_dlc_help/props/hlp_prop_giant_cloaker/giant_cloaker_1_close_single (1776.98, 3050.96, -2498.48)
	Execute:
		Toggle on: ´drill_dmg_area´
		´drills_big´
		´Shake_loop_Start´
		´heli_smoke´
