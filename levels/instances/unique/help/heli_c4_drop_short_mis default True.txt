﻿´startup´ ElementUnitSequence 100010
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Execute sequence: "state_interaction_disable" - units/pd2_dlc_cro/gen_pku_c4_explosives_x1/001 (0.0, 0.0, 0.0)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_cro/gen_pku_c4_explosives_x1/001 (0.0, 0.0, 0.0)
´Start´ ElementInstanceInput 100002
	Upon mission event "Fly_In":
		Execute sequence: "diamondheist" - units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/001 (-125.0, 0.0, 0.0)
		Execute sequence: "flyin_fwd_hover" - units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/001 (-125.0, 0.0, 0.0)
		Execute sequence: "open_door_right" - units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/001 (-125.0, 0.0, 0.0)
		Execute sequence: "hover_flyout_back" - units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/001 (-125.0, 0.0, 0.0) (DELAY 27.25)
		Execute sequence: "hidden" - units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/001 (-125.0, 0.0, 0.0) (DELAY 27.25)
		Execute sequence: "state_vis_show" - units/pd2_dlc1/pickups/gen_pku_c4_explosives_dyn/001 (0.0, 15.0, 1925.0) (DELAY 27.25)
		Execute sequence: "make_dynamic" - units/pd2_dlc1/pickups/gen_pku_c4_explosives_dyn/001 (0.0, 15.0, 1925.0) (DELAY 27.25)
		Send instance event "bag_landed" to mission. (DELAY 27.25)
		Send instance event "C4_bag_falls_down" to mission. (DELAY 27.25)
		Execute sequence: "state_interaction_disable" - units/pd2_dlc1/pickups/gen_pku_c4_explosives_dyn/001 (0.0, 15.0, 1925.0) (DELAY 29.6)
		Execute sequence: "state_vis_hide" - units/pd2_dlc1/pickups/gen_pku_c4_explosives_dyn/001 (0.0, 15.0, 1925.0) (DELAY 29.6)
		Execute sequence: "state_interaction_enable" - units/pd2_dlc_cro/gen_pku_c4_explosives_x1/001 (0.0, 0.0, 0.0) (DELAY 29.6)
		Execute sequence: "state_vis_show" - units/pd2_dlc_cro/gen_pku_c4_explosives_x1/001 (0.0, 0.0, 0.0) (DELAY 29.6)
		Toggle on: ´area_player_near_C4´ (DELAY 29.6)
		´c4_waypoint´ (DELAY 29.6)
´c4_waypoint´ ElementWaypoint 100021
	Place waypoint with icon "pd2_c4" at position (0.0, 0.0, 50.0)
´area_player_near_C4´ ElementAreaTrigger 100016
	DISABLED
	TRIGGER TIMES 1
	Box, width 500.0, height 500.0, depth 500.0, at position (0.0, 0.0, 125.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Make instigator say: v04
		Debug message: point_teammate_comment_found_it
´took_c4´ ElementUnitSequenceTrigger 100017
	TRIGGER TIMES 1
	Upon sequence "load" - units/pd2_dlc_cro/gen_pku_c4_explosives_x1/001 (0.0, 0.0, 0.0):
		remove ´c4_waypoint´
		Execute sequence: "state_interaction_disable" - units/pd2_dlc1/pickups/gen_pku_c4_explosives_dyn/001 (0.0, 15.0, 1925.0)
		Execute sequence: "state_vis_hide" - units/pd2_dlc1/pickups/gen_pku_c4_explosives_dyn/001 (0.0, 15.0, 1925.0)
		Send instance event "bag_taken" to mission.
		Debug message: LOADED AND TUk
