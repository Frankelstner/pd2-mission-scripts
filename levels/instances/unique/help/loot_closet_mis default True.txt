﻿´Auto_Start´ ElementDisableUnit 100007
	EXECUTE ON STARTUP
	Hide object: units/pd2_dlc_help/props/prop_white_glow/hlp_white_glow/001 (5.0, -3.21865e-06, 0.0)
´spawn_1_bag´ ElementInstanceInput 100020
	Upon mission event "spawn_loot_1":
		´how_man_bags_alive（0）´ = 5
´how_man_bags_alive（0）´ ElementCounter 100082
´open_door´ ElementInstanceInput 100026
	Upon mission event "door_open":
		´open´
´open´ MissionScriptElement 100027
	´env_effect_play_001´
	´ON´
	´ghost_start´
´env_effect_play_001´ ElementPlayEffect 100002
	Play effect effects/payday2/particles/smoke_trail/door_smoke at position (125.0, 2.84124e-05, 120.0)
´ON´ ElementToggle 100034
	Toggle on: ´close´
	Toggle on: ´subtract_1´
	Toggle on: ´spawn_loot_1´
	Toggle on: ´spawn_1_bag_2´
	Toggle on: ´5´
	Toggle on: ´add_1´
	Toggle on: ´spawn_1_loot_bag´
	Toggle on: ´pick_bag´
	Toggle on: ´close_2´
	´open_2´
´close´ MissionScriptElement 100028
	DISABLED
	´OFF´
	´ghost_stop´
´subtract_1´ ElementCounterOperator 100104
	DISABLED
	´bags_in_level（0）´ -= 1
	´Start_Loot´ (DELAY 0.25)
´bags_in_level（0）´ ElementCounter 100103
´spawn_loot_1´ ElementCounterFilter 100000
	If ´how_man_bags_alive（0）´ == 5:
		´spawn_1_bag_2´
´spawn_1_bag_2´ MissionScriptElement 100008
	´5´
´5´ ElementCounterFilter 100107
	If ´bags_in_level（0）´ < 5:
		´add_1´
´add_1´ ElementCounterOperator 100100
	´bags_in_level（0）´ += 1
	´spawn_1_loot_bag´
´spawn_1_loot_bag´ MissionScriptElement 100099
	´pick_bag´
	´Start_Loot´
´pick_bag´ ElementRandom 100101
	Execute 1:
		´coke_004´
		´coke_010´
		´coke_003´
		´coke_009´
		´coke_002´
		´coke_008´
		´coke_001´
		´coke_007´
		´coke_005´
		´coke_006´
´close_2´ ElementUnitSequence 100096
	DISABLED
	Execute sequence: "anim_close_door" - units/payday2/architecture/lcm_ext_door_metal_01/001 (-25.0, 68.0, 0.0)
	Execute sequence: "2_disable_interaction" - units/payday2/architecture/lcm_ext_door_metal_01/001 (-25.0, 68.0, 0.0)
´ghost_start´ ElementPlaySound 100055
	Play audio "hlp_bag_door_loop_start" at position (-25.0, 0.0, 125.0)
´open_2´ ElementUnitSequence 100032
	Execute sequence: "2_disable_interaction" - units/payday2/architecture/lcm_ext_door_metal_01/001 (-25.0, 68.0, 0.0)
	Execute sequence: "anim_open_door" - units/payday2/architecture/lcm_ext_door_metal_01/001 (-25.0, 68.0, 0.0)
	´show_-_1´
´OFF´ ElementToggle 100035
	Toggle off: ´close´
	Toggle off: ´subtract_1´
	Toggle off: ´bag_accidentally_removed´
	Toggle off: ´spawn_1_bag_2´
	Toggle off: ´5´
	Toggle off: ´spawn_loot_1´
	Toggle off: ´spawn_1_loot_bag´
	Toggle off: ´add_1´
	Toggle off: ´pick_bag´
	Toggle off: ´close_2´
	´stop_fx´
´bag_accidentally_removed´ ElementInstanceOutput 100054
	DISABLED
	Send instance event "bag_accidentally_removed" to mission.
´ghost_stop´ ElementPlaySound 100056
	Play audio "hlp_bag_door_loop_stop" at position (-25.0, 0.0, 125.0)
´Start_Loot´ MissionScriptElement 100092
	BASE DELAY  (DELAY 1)
	´spawn_loot_1´
´coke_004´ ElementLootBag 100042
	Spawn loot bag "cloaker_gold" at position (75.0, 0.0, 175.0)
	´something_spawns_from_door´
´coke_010´ ElementLootBag 100048
	Spawn loot bag "cloaker_gold" at position (100.0, 0.0, 175.0)
	´something_spawns_from_door´
´coke_003´ ElementLootBag 100041
	Spawn loot bag "cloaker_gold" at position (75.0, 0.0, 150.0)
	´something_spawns_from_door´
´coke_009´ ElementLootBag 100047
	Spawn loot bag "cloaker_gold" at position (100.0, 0.0, 150.0)
	´something_spawns_from_door´
´coke_002´ ElementLootBag 100038
	Spawn loot bag "cloaker_gold" at position (75.0, 0.0, 125.0)
	´something_spawns_from_door´
´coke_008´ ElementLootBag 100046
	Spawn loot bag "cloaker_gold" at position (100.0, 0.0, 125.0)
	´something_spawns_from_door´
´coke_001´ ElementLootBag 100006
	Spawn loot bag "cloaker_gold" at position (75.0, 0.0, 100.0)
	´something_spawns_from_door´
´coke_007´ ElementLootBag 100045
	Spawn loot bag "cloaker_gold" at position (100.0, 0.0, 100.0)
	´something_spawns_from_door´
´coke_005´ ElementLootBag 100043
	Spawn loot bag "cloaker_gold" at position (75.0, 0.0, 75.0)
	´something_spawns_from_door´
´coke_006´ ElementLootBag 100044
	Spawn loot bag "cloaker_gold" at position (100.0, 0.0, 75.0)
	´something_spawns_from_door´
´show_-_1´ ElementEnableUnit 100030
	Show object: units/pd2_dlc_help/props/prop_white_glow/hlp_white_glow/001 (5.0, -3.21865e-06, 0.0)
	´show001´ (DELAY 0.15)
´stop_fx´ ElementStopEffect 100003
	Stop effect: ´env_effect_play_001´
	´close_3´
´something_spawns_from_door´ ElementPlaySound 100049
	Play audio "bell_ring" at position (-25.0, 0.0, 125.0)
´show001´ MissionScriptElement 100037
	´door_opened´
´close_3´ ElementUnitSequence 100033
	Execute sequence: "anim_close_door" - units/payday2/architecture/lcm_ext_door_metal_01/001 (-25.0, 68.0, 0.0)
	Execute sequence: "2_disable_interaction" - units/payday2/architecture/lcm_ext_door_metal_01/001 (-25.0, 68.0, 0.0)
	´hide´ (DELAY 0.5)
´door_opened´ ElementInstanceOutput 100039
	Send instance event "door_opened" to mission.
	´Start_Loot´
	´show_waypoint´
´hide´ ElementDisableUnit 100031
	Hide object: units/pd2_dlc_help/props/prop_white_glow/hlp_white_glow/001 (5.0, -3.21865e-06, 0.0)
	´door_closed´
´show_waypoint´ MissionScriptElement 100016
	´remove´
´door_closed´ ElementInstanceOutput 100040
	Send instance event "door_closed" to mission.
	´ON_2´
´remove´ ElementWaypoint 100004
	Place waypoint with icon "pd2_goto" at position (-25.0, 0.0, 175.0)
´ON_2´ ElementToggle 100095
	Toggle on: ´Start_Loot´
	Toggle on: ´env_effect_play_001´
	Toggle on: ´ghost_start´
	Toggle on: ´ghost_stop´
	Toggle on: ´show_waypoint´
	´set_0´
´set_0´ ElementCounterOperator 100012
	´bags_in_level（0）´ = 0
	remove ´remove´
´close_door´ ElementInstanceInput 100029
	Upon mission event "door_close":
		´close´
´open_door_for_spawns´ ElementInstanceInput 100093
	Upon mission event "open_door_for_spawns":
		´open_3´
´open_3´ MissionScriptElement 100091
	´OFF_2´
´OFF_2´ ElementToggle 100094
	Toggle off: ´Start_Loot´
	Toggle off: ´env_effect_play_001´
	Toggle off: ´ghost_start´
	Toggle off: ´ghost_stop´
	Toggle off: ´show_waypoint´
	Toggle on: ´bag_accidentally_removed´
´bag_removal´ ElementAreaReportTrigger 100073
	Use shape: ´delete_bag001´ (100.0, 0.0, 7.0)
	Amount: 1
	Instigator: loot
	Interval: 0.1
	Upon trigger, execute:
		´None：poof´ (ALTERNATIVE enter)
´None：poof´ ElementCarry 100074
	´bag_accidentally_removed´
	´subtract_1´
´delete_bag001´ ElementShape 100072
	Box, width 169.725006104, height 39.3772010803, depth 105.822998047, at position (100.0, 0.0, 7.0) with rotation (0.0, 0.0, 0.0, -1.0)
´kill_all´ ElementInstanceInput 100023
	Upon mission event "kill_all":
		Toggle off: ´door_closed´
		Toggle off: ´door_opened´
		Toggle off: ´open_door´
		Toggle off: ´close_door´
		Toggle off: ´open_door_for_spawns´
		Toggle off: ´open_3´
		Toggle off: ´open´
		Toggle off: ´close´
		Toggle off: ´OFF´
		Toggle off: ´ON´
		Toggle off: ´OFF_2´
		Toggle off: ´spawn_1_bag´
		Toggle off: ´spawn_1_bag_2´
		Toggle off: ´Start_Loot´
		Toggle off: ´something_spawns_from_door´
		Toggle off: ´open_2´
		Toggle off: ´show_-_1´
		Toggle off: ´show001´
		Toggle off: ´close_3´
		Toggle off: ´hide´
		Toggle off: ´stop_fx´
		Toggle off: ´ON_2´
		Toggle off: ´coke_001´
		Toggle off: ´coke_002´
		Toggle off: ´coke_003´
		Toggle off: ´coke_004´
		Toggle off: ´coke_005´
		Toggle off: ´coke_006´
		Toggle off: ´coke_007´
		Toggle off: ´coke_008´
		Toggle off: ´coke_009´
		Toggle off: ´coke_010´
		Toggle off: ´set_0´
		Toggle off: ´ghost_start´
		Stop effect: ´env_effect_play_001´
		´close_2´
		Hide object: units/pd2_dlc_help/props/prop_white_glow/hlp_white_glow/001 (5.0, -3.21865e-06, 0.0) (DELAY 1.15)
		remove ´remove´ (DELAY 1.15)
		´ghost_stop´ (DELAY 1.15)
´loot_secured´ ElementInstanceInput 100105
	Upon mission event "loot_secured":
		´subtract_1´
