﻿´start´ ElementUnitSequence 100018
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Execute sequence: "hide" - units/pd2_dlc_wwh/equipment/wwh_interactable_saw_no_jam/001 (0.0, 16.0, 0.00400394)
	Execute sequence: "hide_dummy" - units/pd2_dlc_wwh/equipment/wwh_interactable_saw_no_jam/001 (0.0, 16.0, 0.00400394)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_wwh/pickups/wwh_pku_saw/001 (0.0, 16.0, 0.00400394)
	Hide object: units/pd2_dlc_brb/props/brb_prop_shutter_02/001 (0.0, 0.0, 0.0)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/001 (-100.0, 93.0, 0.00400394)
´input_activate_saw´ ElementInstanceInput 100007
	Upon mission event "Activate saw":
		´activate_wp´
´activate_wp´ MissionScriptElement 100012
	DISABLED
	Execute sequence: "set_hack_time_90_sec" - units/pd2_dlc_wwh/equipment/wwh_interactable_saw_no_jam/001 (0.0, 16.0, 0.00400394)
	Execute sequence: "show_and_enable_interaction" - units/pd2_dlc_wwh/equipment/wwh_interactable_saw_no_jam/001 (0.0, 16.0, 0.00400394)
	Send instance event "door_picked" to mission.
	Toggle on: ´area_player_close_to_saw´
	´saw_wp´
´saw_wp´ ElementWaypoint 100020
	Place waypoint with icon "pd2_generic_saw" at position (0.0, 50.0, 100.004)
´area_player_close_to_saw´ ElementAreaTrigger 100037
	DISABLED
	TRIGGER TIMES 1
	Box, width 400.0, height 500.0, depth 300.0, at position (0.0, 100.0, 100.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Make instigator say: v16
		Debug message: from instance: teammate_comment_open_it_up
´anim_complete´ ElementSpecialObjectiveTrigger 100041
	DISABLED
	Upon special objective event "complete" - ´interrupt_so´:
		Toggle off: ´anim_complete´
		Toggle off: ´turn_on_execute_so´
		Toggle off: ´anim_fail´
		remove ´interrupt_so´
		Send instance event "Saw interrupted" to mission.
		remove ´defend_wp´
		Execute sequence: "set_jammed" - units/pd2_dlc_wwh/equipment/wwh_interactable_saw_no_jam/001 (0.0, 16.0, 0.00400394)
		If 40% chance to execute succeeds:
			´teammate_comment_swear´
		´fix_wp´
´interrupt_so´ ElementSpecialObjective 100034
	TRIGGER TIMES 1
	Create objective for enemies in 2000 radius around (6.61639, 297.457, 0.00400394)
	Make chosen person path towards objective and show "e_so_low_kicks".
´turn_on_execute_so´ ElementToggle 100040
	DISABLED
	Toggle on, trigger times 1: ´interrupt_so´
	´interrupt_so´ (DELAY 1)
´anim_fail´ ElementSpecialObjectiveTrigger 100042
	DISABLED
	Upon special objective event "fail" - ´interrupt_so´:
		´loop´
´fix_wp´ ElementWaypoint 100022
	Place waypoint with icon "pd2_fix" at position (0.0, 50.0, 100.004)
´defend_wp´ ElementWaypoint 100021
	Place waypoint with icon "pd2_defend" at position (0.0, 50.0, 100.004)
´teammate_comment_swear´ ElementTeammateComment 100063
	TRIGGER TIMES 1
	Make teammate close to instigator say: g60
	Debug message: from instance: teammate_comment_swear
´loop´ MissionScriptElement 100039
	´turn_on_execute_so´
´saw_placed´ ElementUnitSequenceTrigger 100049
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_wwh/equipment/wwh_interactable_saw_no_jam/001 (0.0, 16.0, 0.00400394):
		Send instance event "Saw placed" to mission.
		remove ´saw_wp´
		´defend_wp´
		Toggle on: ´saw_placed001´ (DELAY 1)
		´interrupt_logic_start´ (DELAY 5)
´interrupt_logic_start´ MissionScriptElement 100033
	´turn_on_interrupt_entities´
´saw_placed001´ ElementUnitSequenceTrigger 100050
	DISABLED
	Upon sequence "interact" - units/pd2_dlc_wwh/equipment/wwh_interactable_saw_no_jam/001 (0.0, 16.0, 0.00400394):
		Send instance event "Saw fixed" to mission.
		remove ´fix_wp´
		´defend_wp´
		´interrupt_logic_start´ (DELAY 5)
´turn_on_interrupt_entities´ ElementToggle 100056
	Toggle on: ´anim_complete´
	Toggle on: ´turn_on_execute_so´
	Toggle on: ´anim_fail´
	´loop´ (DELAY 0.05)
´saw_done´ ElementUnitSequenceTrigger 100057
	TRIGGER TIMES 1
	Upon sequence "timer_done" - units/pd2_dlc_wwh/equipment/wwh_interactable_saw_no_jam/001 (0.0, 16.0, 0.00400394):
		Send instance event "Saw done" to mission.
		remove ´defend_wp´
		remove ´fix_wp´
		remove ´saw_wp´
		Toggle off: ´anim_complete´
		Toggle off: ´turn_on_execute_so´
		Toggle off: ´anim_fail´
		remove ´interrupt_so´
		Toggle off: ´interrupt_logic_start´
		Toggle off: ´loop´
		Toggle off: ´turn_on_execute_so´
		Toggle off: ´anim_fail´
		Toggle off: ´anim_complete´
		Toggle off: ´turn_on_interrupt_entities´
		Execute sequence: "hide" - units/pd2_dlc_wwh/equipment/wwh_interactable_saw_no_jam/001 (0.0, 16.0, 0.00400394)
		Execute sequence: "show_prop_only" - units/pd2_dlc_wwh/pickups/wwh_pku_saw/001 (0.0, 16.0, 0.00400394)
		Execute sequence: "state_interaction_enabled" - units/pd2_dlc_wwh/pickups/wwh_pku_saw/001 (0.0, 16.0, 0.00400394)
		Execute sequence: "int_seq_show_complete_screen" - units/pd2_dlc_wwh/pickups/wwh_pku_saw/001 (0.0, 16.0, 0.00400394)
		´interact_wp´
		Make teammate close to instigator say: g60 (DELAY 1)
		Debug message: from instance: teammate_comment_done (DELAY 1)
´interact_wp´ ElementWaypoint 100029
	Place waypoint with icon "pd2_generic_interact" at position (0.0, 50.0, 100.004)
´saw_done001´ ElementUnitSequenceTrigger 100059
	TRIGGER TIMES 1
	Upon sequence "load" - units/pd2_dlc_wwh/pickups/wwh_pku_saw/001 (0.0, 16.0, 0.00400394):
		Send instance event "Saw taken" to mission.
		Execute sequence: "anim_open" - units/pd2_dlc_brb/props/brb_prop_shutter_02/001 (0.0, 0.0, 0.0)
		remove ´defend_wp´
		remove ´fix_wp´
		remove ´saw_wp´
		remove ´interact_wp´
		Remove nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/001 (-100.0, 93.0, 0.00400394)
		Execute sequence: "hide" - units/pd2_dlc_wwh/equipment/wwh_interactable_saw_no_jam/001 (0.0, 16.0, 0.00400394)
´set_can_open´ ElementInstanceInput 100071
	Upon mission event "can_open":
		Hide object: units/pd2_dlc_brb/props/brb_prop_titan_shutters_03/001 (0.0, 0.0, 0.0)
		Show object: units/pd2_dlc_brb/props/brb_prop_shutter_02/001 (0.0, 0.0, 0.0)
		Toggle on: ´activate_wp´
´button_pressed_open´ ElementInstanceInput 100083
	Upon mission event "button_pressed_open":
		Remove nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/001 (-100.0, 93.0, 0.00400394)
		Execute sequence: "anim_open" - units/pd2_dlc_brb/props/brb_prop_titan_shutters_03/001 (0.0, 0.0, 0.0)
		Execute sequence: "anim_open" - units/pd2_dlc_brb/props/brb_prop_shutter_02/001 (0.0, 0.0, 0.0)
