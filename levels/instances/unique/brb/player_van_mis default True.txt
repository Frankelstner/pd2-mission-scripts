﻿´Auto_Start´ ElementUnitSequence 100003
	EXECUTE ON STARTUP
	Execute sequence: "state_vis_hide" - units/pd2_dlc1/pickups/gen_pku_c4_explosives/001 (-17.2506, -26.667, 14.81)
	Execute sequence: "state_interaction_disable" - units/pd2_dlc1/pickups/gen_pku_c4_explosives/001 (-17.2506, -26.667, 14.81)
	Execute sequence: "hide" - units/payday2/pickups/gen_pku_toolbag_static/001 (-36.3181, 36.156, 109.81)
	Execute sequence: "disable_interaction" - units/payday2/pickups/gen_pku_toolbag_static/001 (-36.3181, 36.156, 109.81)
	Execute sequence: " disable_interaction " - units/pd2_dlc_brb/props/brb_prop_construction_portabletoilet_open/001 (0.0, 0.0, 0.0)
	Execute sequence: "hide" - units/pd2_dlc_brb/pickups/gen_pku_winch_static/001 (-36.3181, 36.156, 109.81)
	Execute sequence: "hide" - units/pd2_dlc_brb/pickups/gen_pku_winch_static/002 (1.16865, 38.3426, 76.1368)
	Execute sequence: "hide" - units/pd2_dlc_brb/pickups/gen_pku_winch_static/003 (0.760801, 14.5282, 65.7591)
	Execute sequence: "disable_interaction" - units/pd2_dlc_brb/pickups/gen_pku_winch_static/001 (-36.3181, 36.156, 109.81)
	Execute sequence: "disable_interaction" - units/pd2_dlc_brb/pickups/gen_pku_winch_static/002 (1.16865, 38.3426, 76.1368)
	Execute sequence: "disable_interaction" - units/pd2_dlc_brb/pickups/gen_pku_winch_static/003 (0.760801, 14.5282, 65.7591)
	Execute sequence: "anim_door_close" - units/pd2_dlc_brb/props/brb_prop_construction_portabletoilet_open/001 (0.0, 0.0, 0.0)
	Execute sequence: "hide" - units/payday2/equipment/gen_interactable_thermite_backpack_inf/001 (38.4223, -21.6733, 17.1371)
	Execute sequence: "state_interact_disabled" - units/payday2/equipment/gen_interactable_thermite_backpack_inf/001 (38.4223, -21.6733, 17.1371)
´show_c4´ ElementInstanceInput 100008
	Upon mission event "show_c4":
		Toggle on: ´spawn´
´spawn´ MissionScriptElement 100007
	DISABLED
	Execute sequence: "state_interaction_enable" - units/pd2_dlc1/pickups/gen_pku_c4_explosives/001 (-17.2506, -26.667, 14.81)
	Execute sequence: "state_vis_show" - units/pd2_dlc1/pickups/gen_pku_c4_explosives/001 (-17.2506, -26.667, 14.81)
	Execute sequence: "show_outline" - units/pd2_dlc1/pickups/gen_pku_c4_explosives/001 (-17.2506, -26.667, 14.81)
	´objects（0）´ += 1
´objects（0）´ ElementCounter 100047
´show_thermal_paste´ ElementInstanceInput 100009
	Upon mission event "show_thermal_paste":
		Toggle on: ´spawn_2´
´spawn_2´ MissionScriptElement 100010
	DISABLED
	Execute sequence: "show" - units/payday2/equipment/gen_interactable_thermite_backpack_inf/001 (38.4223, -21.6733, 17.1371)
	Execute sequence: "state_interact_enabled" - units/payday2/equipment/gen_interactable_thermite_backpack_inf/001 (38.4223, -21.6733, 17.1371)
	´objects（0）´ += 1
´spawn_lance´ ElementInstanceInput 100012
	Upon mission event "spawn_lance":
		Toggle on: ´spawn_3´
´spawn_3´ MissionScriptElement 100011
	DISABLED
	Execute sequence: "show" - units/payday2/pickups/gen_pku_toolbag_static/001 (-36.3181, 36.156, 109.81)
	Execute sequence: "enable_interaction" - units/payday2/pickups/gen_pku_toolbag_static/001 (-36.3181, 36.156, 109.81)
	Execute sequence: "show_outline" - units/payday2/pickups/gen_pku_toolbag_static/001 (-36.3181, 36.156, 109.81)
	´objects（0）´ += 1
´used´ ElementUnitSequenceTrigger 100021
	Upon sequence "load" - units/pd2_dlc1/pickups/gen_pku_c4_explosives/001 (-17.2506, -26.667, 14.81):
		Send instance event "c4_used" to mission.
		´2（2）´
		´1´
´2（2）´ ElementCounter 100050
´1´ MissionScriptElement 100063
	TRIGGER TIMES 1
	´objects（0）´ -= 1
	If ´objects（0）´ == 0: (DELAY 0.1)
		remove ´point_waypoint_001´
´point_waypoint_001´ ElementWaypoint 100030
	Place waypoint with icon "pd2_generic_interact" at position (0.0, -3.0, 157.0)
´used_3´ ElementUnitSequenceTrigger 100025
	TRIGGER TIMES 1
	Upon sequence "load" - units/payday2/equipment/gen_interactable_thermite_backpack_inf/001 (38.4223, -21.6733, 17.1371):
		Send instance event "thermal_paste_used" to mission.
		´2（2）´
		´001´
´001´ MissionScriptElement 100057
	TRIGGER TIMES 1
	´objects（0）´ -= 1
	If ´objects（0）´ == 0: (DELAY 0.1)
		remove ´point_waypoint_001´
´used_5´ ElementUnitSequenceTrigger 100026
	TRIGGER TIMES 1
	Upon sequence "load" - units/payday2/pickups/gen_pku_toolbag_static/001 (-36.3181, 36.156, 109.81):
		Send instance event "lance_used" to mission.
		´2（2）´
		´002´
´002´ MissionScriptElement 100058
	TRIGGER TIMES 1
	´objects（0）´ -= 1
	If ´objects（0）´ == 0: (DELAY 0.1)
		remove ´point_waypoint_001´
´show_winch_parts´ ElementInstanceInput 100037
	Upon mission event "show_winch_parts":
		Toggle on: ´spawn_4´
´spawn_4´ MissionScriptElement 100038
	DISABLED
	Execute sequence: "show_and_enable_interaction" - units/pd2_dlc_brb/pickups/gen_pku_winch_static/001 (-36.3181, 36.156, 109.81)
	Execute sequence: "show_and_enable_interaction" - units/pd2_dlc_brb/pickups/gen_pku_winch_static/002 (1.16865, 38.3426, 76.1368)
	Execute sequence: "show_and_enable_interaction" - units/pd2_dlc_brb/pickups/gen_pku_winch_static/003 (0.760801, 14.5282, 65.7591)
	´objects（0）´ += 1
´enable_interaction´ ElementInstanceInput 100045
	Upon mission event "enable_interaction":
		Execute sequence: " enable_interaction " - units/pd2_dlc_brb/props/brb_prop_construction_portabletoilet_open/001 (0.0, 0.0, 0.0)
		´enable_waypoint´
´enable_waypoint´ ElementInstanceOutput 100052
	DISABLED
	Send instance event "enable_waypoint_near_me" to mission.
´enable_equipment´ ElementInstanceInput 100002
	Upon mission event "enable_equipment":
		Toggle on: ´door_opened´
		Toggle on: ´spawn´
		Toggle on: ´enable_waypoint´
		Toggle on: ´open´
		Send instance event "vo_call_out" to mission.
´door_opened´ ElementInstanceOutput 100018
	DISABLED
	Send instance event "door_opened" to mission.
´open´ MissionScriptElement 100061
	DISABLED
	´spawn´
	´spawn_2´
	´spawn_3´
	´door_opened´
	´spawn_4´
	´point_waypoint_001´
	Make teammate close to instigator say: v04 (DELAY 0.5)
´remove_waypoint´ ElementInstanceInput 100062
	Upon mission event "remove_waypoint":
		remove ´point_waypoint_001´
´used_7´ ElementUnitSequenceTrigger 100035
	Upon sequence "interact" - units/pd2_dlc_brb/props/brb_prop_construction_portabletoilet_open/001 (0.0, 0.0, 0.0):
		´open´
´taken´ ElementUnitSequenceTrigger 100015
	Upon any sequence:
		"load" - units/pd2_dlc_brb/pickups/gen_pku_winch_static/001 (-36.3181, 36.156, 109.81)
		"load" - units/pd2_dlc_brb/pickups/gen_pku_winch_static/002 (1.16865, 38.3426, 76.1368)
		"load" - units/pd2_dlc_brb/pickups/gen_pku_winch_static/003 (0.760801, 14.5282, 65.7591)
	Execute:
		Send instance event "winch_taken" to mission.
		´003´
´003´ MissionScriptElement 100059
	TRIGGER TIMES 1
	´objects（0）´ -= 1
	If ´objects（0）´ == 0: (DELAY 0.1)
		remove ´point_waypoint_001´
