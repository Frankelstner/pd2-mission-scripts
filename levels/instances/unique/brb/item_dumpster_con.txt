ID range vs continent name:
	100000: world

statics
	100042 units/dev_tools/level_tools/dev_collision_1x1m/001 (-7.62939e-06, -53.0, 26.0)
	100043 units/dev_tools/level_tools/dev_collision_1x1m/002 (100.0, -53.0, 26.0)
	100047 units/dev_tools/level_tools/dev_collision_1x1m/003 (-25.0, -53.0, 125.0)
	100065 units/dev_tools/level_tools/dev_collision_1x1m/004 (329.0, -60.0, 27.0)
	100066 units/dev_tools/level_tools/dev_collision_1x1m/005 (229.061, -56.5101, 27.0)
	100006 units/payday2/equipment/gen_interactable_thermite_backpack_inf/001 (27.0, -5.0, 88.0)
		mesh_variation state_interact_disabled
	100020 units/payday2/pickups/gen_pku_toolbag_static/001 (39.6819, 31.156, 118.81)
		mesh_variation disable_interaction
	100001 units/pd2_dlc1/pickups/gen_pku_c4_explosives/001 (58.6311, -23.2428, 96.2608)
	100000 units/pd2_dlc_brb/pickups/gen_pku_winch_static/001 (295.0, -36.0, 113.027)
		mesh_variation disable_interaction
	100040 units/pd2_dlc_brb/pickups/gen_pku_winch_static/002 (295.0, -4.0, 120.027)
		mesh_variation disable_interaction
	100049 units/pd2_dlc_brb/pickups/gen_pku_winch_static/003 (295.0, 25.0, 128.027)
		mesh_variation disable_interaction
	100031 units/pd2_dlc_brb/props/brb_prop_alley_trash_container/001 (0.0, 0.0, 0.0)
		mesh_variation interaction_disabled
	100064 units/pd2_dlc_brb/props/brb_prop_alley_trash_container/002 (230.0, -5.0, 0.0)
		mesh_variation interaction_disabled
	100034 units/pd2_dlc_cane/props/cne_prop_snow_pile_03/001 (49.4838, -7.08331, 93.3036)
	100035 units/pd2_dlc_cane/props/cne_prop_snow_pile_03/002 (-25.5162, -7.08331, 93.3036)
	100057 units/pd2_dlc_cane/props/cne_prop_snow_pile_03/003 (199.484, -7.08331, 93.3036)
	100058 units/pd2_dlc_cane/props/cne_prop_snow_pile_03/004 (274.484, -7.08331, 103.304)
