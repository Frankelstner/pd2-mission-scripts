﻿´start_display´ ElementInstanceInput 100001
	Upon mission event "start_display":
		Toggle on: ´cutoff´
		Execute 1:
			´time001´
			´time002´
			´time003´
			´time004´
		´select_screen´ (DELAY 0.05)
´select_screen´ ElementRandom 100002
	Execute 1:
		´display_001´
		´display_002´
		´display_003´
		´display_004´
´time001´ MissionScriptElement 100011
	´set_next_screen´ (DELAY 13.37)
´time002´ MissionScriptElement 100012
	´set_next_screen´ (DELAY 4.21)
´time003´ MissionScriptElement 100013
	´set_next_screen´ (DELAY 7.72)
´time004´ MissionScriptElement 100014
	´set_next_screen´ (DELAY 9.06)
´cutoff´ MissionScriptElement 100034
	´select_screen´
´display_001´ ElementUnitSequence 100003
	TRIGGER TIMES 1
	Execute sequence: "use_screen_a" - units/payday2/props/stn_prop_screen_b/001 (0.0, 0.0, 0.0)
	´3_times（3）´
´display_002´ ElementUnitSequence 100006
	TRIGGER TIMES 1
	Execute sequence: "use_screen_b" - units/payday2/props/stn_prop_screen_b/001 (0.0, 0.0, 0.0)
	´3_times（3）´
´display_003´ ElementUnitSequence 100007
	TRIGGER TIMES 1
	Execute sequence: "use_screen_c" - units/payday2/props/stn_prop_screen_b/001 (0.0, 0.0, 0.0)
	´3_times（3）´
´display_004´ ElementUnitSequence 100008
	TRIGGER TIMES 1
	Execute sequence: "use_screen_d" - units/payday2/props/stn_prop_screen_b/001 (0.0, 0.0, 0.0)
	´3_times（3）´
´set_next_screen´ MissionScriptElement 100010
	Execute 1:
		´time001´
		´time002´
		´time003´
		´time004´
	´cutoff´
´3_times（3）´ ElementCounter 100022
	When counter target reached: 
		Toggle on, trigger times 1: ´display_001´
		Toggle on, trigger times 1: ´display_002´
		Toggle on, trigger times 1: ´display_003´
		Toggle on, trigger times 1: ´display_004´
		´3_times（3）´ = 3 (DELAY 0.05)
´power_lost´ ElementInstanceInput 100016
	Upon mission event "power_lost":
		Toggle off: ´cutoff´
		Execute 1:
			´dead001´
			´dead002´
			´dead003´
´dead001´ ElementUnitSequence 100036
	BASE DELAY  (DELAY 0.95)
	Execute sequence: "use_screen_e" - units/payday2/props/stn_prop_screen_b/001 (0.0, 0.0, 0.0)
´dead002´ ElementUnitSequence 100037
	BASE DELAY  (DELAY 0.25)
	Execute sequence: "use_screen_e" - units/payday2/props/stn_prop_screen_b/001 (0.0, 0.0, 0.0)
´dead003´ ElementUnitSequence 100038
	BASE DELAY  (DELAY 0.35)
	Execute sequence: "use_screen_e" - units/payday2/props/stn_prop_screen_b/001 (0.0, 0.0, 0.0)
´power_resumed´ ElementInstanceInput 100025
	Upon mission event "power_resumed":
		Execute 1:
			´short001´
			´short002´
			´short003´
			´short004´
		Toggle on: ´cutoff´ (DELAY 5)
´short001´ MissionScriptElement 100026
	´rebooted（90％）´ (DELAY 1.55)
´short002´ MissionScriptElement 100028
	´rebooted（90％）´ (DELAY 3.25)
´short003´ MissionScriptElement 100029
	´rebooted（90％）´ (DELAY 1.38)
´short004´ MissionScriptElement 100030
	´rebooted（90％）´ (DELAY 2.37)
´rebooted（90％）´ ElementLogicChance 100035
	If chance to execute succeeds:
		´select_screen´
	Else:
		Execute sequence: "use_screen_f" - units/payday2/props/stn_prop_screen_b/001 (0.0, 0.0, 0.0)
		Toggle off: ´select_screen´
		Toggle off: ´set_next_screen´
		´rebooted（90％）´ = 0%
´func_sequence_trigger_001´ ElementUnitSequenceTrigger 100000
	TRIGGER TIMES 1
	Upon sequence "kill_monitor" - units/payday2/props/stn_prop_screen_b/001 (0.0, 0.0, 0.0):
		Toggle off: ´select_screen´
		Toggle off: ´set_next_screen´
