﻿´auto_start´ MissionScriptElement 100014
	EXECUTE ON STARTUP
	Hide object: units/pd2_indiana/props/mus_prop_construction_saw/001 (0.0, 40.0, 0.0)
	Hide object: units/dev_tools/level_tools/dev_ai_vis_blocker_001x2x2m/001 (-100.0, 150.0, 0.0)
	Hide object: units/dev_tools/level_tools/dev_ai_vis_blocker_001x2x2m/004 (100.0, 0.0, -75.0)
	Hide object: units/pd2_dlc_dinner/props/din_barrel/001 (70.0, 39.0, 0.0)
	Hide object: units/pd2_dlc_dark/props/drk_prop_safety_light/002 (75.3881, 32.908, 108.0)
	Hide object: units/pd2_dlc_dark/props/drk_prop_labpipe_50cm/001 (-31.0, 40.0, 87.0)
	Hide object: units/payday2/props/ind_prop_construction_light_flood/001 (126.113, 41.6666, -9.0)
	Hide object: units/pd2_dlc_holly/river/props/lxa_prop_hobo_plasticcrate/001 (-60.0, 40.0, 0.0)
	Hide object: core/units/light_omni/001 (57.0711, 52.3726, 136.0)
	Execute sequence: "hide" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/001 (-68.7808, 34.0, 2.57381)
	Execute sequence: "hide" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/002 (-51.2216, 44.0, 1.8158)
	Execute sequence: "hide" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/003 (-52.4934, 26.7415, 1.8409)
	Execute sequence: "disable_interaction" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/001 (-68.7808, 34.0, 2.57381)
	Execute sequence: "disable_interaction" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/002 (-51.2216, 44.0, 1.8158)
	Execute sequence: "disable_interaction" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/003 (-52.4934, 26.7415, 1.8409)
	Execute sequence: "hide" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/004 (-63.3078, 47.0, 1.9109)
	Execute sequence: "disable_interaction" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/004 (-63.3078, 47.0, 1.9109)
´func_whisper_state_001´ ElementWhisperState 100017
	EXECUTE ON STARTUP
	Enable whisper mode.
´used´ ElementUnitSequenceTrigger 100006
	TRIGGER TIMES 1
	Upon any sequence:
		"load" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/001 (-68.7808, 34.0, 2.57381)
		"load" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/002 (-51.2216, 44.0, 1.8158)
		"load" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/003 (-52.4934, 26.7415, 1.8409)
		"load" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/004 (-63.3078, 47.0, 1.9109)
	Execute:
		Send instance event "thermal_found" to mission.
		Debug message: thermal_found
´unhide´ ElementInstanceInput 100003
	Upon mission event "unhide":
		Show object: units/pd2_indiana/props/mus_prop_construction_saw/001 (0.0, 40.0, 0.0)
		Show object: units/dev_tools/level_tools/dev_ai_vis_blocker_001x2x2m/001 (-100.0, 150.0, 0.0)
		Show object: units/dev_tools/level_tools/dev_ai_vis_blocker_001x2x2m/004 (100.0, 0.0, -75.0)
		Show object: units/pd2_dlc_dinner/props/din_barrel/001 (70.0, 39.0, 0.0)
		Show object: units/pd2_dlc_dark/props/drk_prop_safety_light/002 (75.3881, 32.908, 108.0)
		Show object: units/pd2_dlc_dark/props/drk_prop_labpipe_50cm/001 (-31.0, 40.0, 87.0)
		Show object: units/payday2/props/ind_prop_construction_light_flood/001 (126.113, 41.6666, -9.0)
		Show object: units/pd2_dlc_holly/river/props/lxa_prop_hobo_plasticcrate/001 (-60.0, 40.0, 0.0)
		Show object: core/units/light_omni/001 (57.0711, 52.3726, 136.0)
		Toggle on: ´enable´
		Toggle on: ´disable´
		Execute sequence: "show" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/001 (-68.7808, 34.0, 2.57381)
		Execute sequence: "show" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/002 (-51.2216, 44.0, 1.8158)
		Execute sequence: "show" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/003 (-52.4934, 26.7415, 1.8409)
		Execute sequence: "show" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/004 (-63.3078, 47.0, 1.9109)
		Play audio "table_saw" at position (5.0, 32.0, 90.0)
		´env_effect_play_001´
		´civ_male_worker_docks_1´
´enable´ ElementUnitSequence 100054
	DISABLED
	Execute sequence: "enable_interaction" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/001 (-68.7808, 34.0, 2.57381)
	Execute sequence: "enable_interaction" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/002 (-51.2216, 44.0, 1.8158)
	Execute sequence: "enable_interaction" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/003 (-52.4934, 26.7415, 1.8409)
	Execute sequence: "enable_interaction" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/004 (-63.3078, 47.0, 1.9109)
´disable´ ElementUnitSequence 100055
	DISABLED
	Execute sequence: "disable_interaction" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/001 (-68.7808, 34.0, 2.57381)
	Execute sequence: "disable_interaction" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/002 (-51.2216, 44.0, 1.8158)
	Execute sequence: "disable_interaction" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/003 (-52.4934, 26.7415, 1.8409)
	Execute sequence: "disable_interaction" - units/pd2_dlc_dark/pickups/drk_prop_thermal_paste/004 (-63.3078, 47.0, 1.9109)
´env_effect_play_001´ ElementPlayEffect 100012
	Play effect effects/payday2/particles/electric/saw_sparks_small at position (2.0, 38.3151, 92.696)
´civ_male_worker_docks_1´ ElementSpawnCivilian 100042
	Spawn civ: civ_male_worker_docks_1 (-3.05176e-05, 100.0, 0.0)
´Death´ ElementEnemyDummyTrigger 100037
	Upon event "death" of ´civ_male_worker_docks_1´:
		Stop effect: ´env_effect_play_001´
		Play audio "table_saw_stop_global" at position (-300.0, -100.0, 0.0)
		Hide object: units/dev_tools/level_tools/dev_ai_vis_blocker_001x2x2m/001 (-100.0, 150.0, 0.0)
		Hide object: units/dev_tools/level_tools/dev_ai_vis_blocker_001x2x2m/004 (100.0, 0.0, -75.0)
´Alerted´ ElementEnemyDummyTrigger 100041
	TRIGGER TIMES 1
	Upon event "alerted" of ´civ_male_worker_docks_1´:
		Stop effect: ´env_effect_play_001´
		Play audio "table_saw_stop_global" at position (-300.0, -100.0, 0.0)
		Hide object: units/dev_tools/level_tools/dev_ai_vis_blocker_001x2x2m/001 (-100.0, 150.0, 0.0)
		Hide object: units/dev_tools/level_tools/dev_ai_vis_blocker_001x2x2m/004 (100.0, 0.0, -75.0)
´Alarm´ ElementInstanceInput 100035
	Upon mission event "Alarm_Sounded":
		Stop effect: ´env_effect_play_001´
		Play audio "table_saw_stop_global" at position (-300.0, -100.0, 0.0)
		Hide object: units/dev_tools/level_tools/dev_ai_vis_blocker_001x2x2m/001 (-100.0, 150.0, 0.0)
		Hide object: units/dev_tools/level_tools/dev_ai_vis_blocker_001x2x2m/004 (100.0, 0.0, -75.0)
´inside´ ElementAreaReportTrigger 100033
	Use shape: ´point_shape_001´ (0.0, 75.0, 100.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon trigger, execute:
		´enable´ (ALTERNATIVE enter)
´point_shape_001´ ElementShape 100022
	Cylinder, radius 287.5, height 230, at position (0.0, 75.0, 100.0) with rotation (0.0, 0.0, 0.0, -1.0)
´empty´ ElementAreaReportTrigger 100050
	Use shape: ´point_shape_001´ (0.0, 75.0, 100.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon trigger, execute:
		´disable´ (ALTERNATIVE empty)
