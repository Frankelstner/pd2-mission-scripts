ID range vs continent name:
	100000: world

statics
	100016 units/dev_tools/level_tools/dev_bag_collision_4x3m/001 (200.0, 20.0, 2.5)
	100001 units/dev_tools/level_tools/dev_bag_collision_4x3m/002 (200.0, 45.0, 2.5)
	100014 units/dev_tools/level_tools/dev_bag_collision_4x3m/003 (-100.0, 45.0, 2.5)
	100017 units/dev_tools/level_tools/dev_bag_collision_4x3m/004 (-100.0, 20.0, 2.5)
	100008 units/pd2_dlc_peta/props/pta_prop_debris_wood_02/001 (5.00003, 3.00002, 0.499999)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
