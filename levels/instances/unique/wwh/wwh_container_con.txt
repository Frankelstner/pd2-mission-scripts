ID range vs continent name:
	100000: world

statics
	100079 units/payday2/props/gen_prop_square_drop_marker_2x3/001 (-31.0, -263.0, 6.004)
	100063 units/pd2_dlc_dinner/props/prop_hanging_meat/din_hanging_meat_03/001 (73.0, -181.0, 266.234)
		mesh_variation pick_var_3
	100000 units/pd2_dlc_friend/props/sfm_interactable_shipping_crate_2x1x1m/001 (-83.0, -500.0, 14.7743)
	100060 units/pd2_dlc_friend/props/sfm_interactable_shipping_crate_2x1x1m/002 (-83.0, -500.0, 81.7743)
	100061 units/pd2_dlc_friend/props/sfm_interactable_shipping_crate_2x1x1m/003 (-9.0, -500.0, 14.7743)
	100062 units/pd2_dlc_friend/props/sfm_interactable_shipping_crate_2x1x1m/004 (-9.0, -500.0, 81.7743)
	100082 units/pd2_dlc_wwh/pickups/wwh_pku_saw/001 (-87.8281, -375.46, 20.0)
	100059 units/pd2_dlc_wwh/props/wwh_prop_container/001 (0.0, -300.0, 0.0)
