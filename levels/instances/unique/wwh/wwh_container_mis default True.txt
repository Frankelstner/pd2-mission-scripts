﻿´start´ MissionScriptElement 100001
	EXECUTE ON STARTUP
	Execute sequence: "hide" - units/payday2/props/gen_prop_square_drop_marker_2x3/001 (-31.0, -263.0, 6.004)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_wwh/pickups/wwh_pku_saw/001 (-87.8281, -375.46, 20.0)
	Execute sequence: "state_interaction_disabled" - units/pd2_dlc_wwh/pickups/wwh_pku_saw/001 (-87.8281, -375.46, 20.0)
	Hide object: units/pd2_dlc_friend/props/sfm_interactable_shipping_crate_2x1x1m/004 (-9.0, -500.0, 81.7743)
	Hide object: units/pd2_dlc_friend/props/sfm_interactable_shipping_crate_2x1x1m/002 (-83.0, -500.0, 81.7743)
	Hide object: units/pd2_dlc_friend/props/sfm_interactable_shipping_crate_2x1x1m/003 (-9.0, -500.0, 14.7743)
	Hide object: units/pd2_dlc_friend/props/sfm_interactable_shipping_crate_2x1x1m/001 (-83.0, -500.0, 14.7743)
	Hide object: units/pd2_dlc_dinner/props/prop_hanging_meat/din_hanging_meat_03/001 (73.0, -181.0, 266.234)
	Execute 1:
		´set_color005´
		´set_color004´
		´set_color003´
		´set_color002´
		´set_color001´
´set_color005´ ElementUnitSequence 100008
	Execute sequence: "var_set_color_yellow" - units/pd2_dlc_wwh/props/wwh_prop_container/001 (0.0, -300.0, 0.0)
´set_color004´ ElementUnitSequence 100007
	Execute sequence: "var_set_color_white" - units/pd2_dlc_wwh/props/wwh_prop_container/001 (0.0, -300.0, 0.0)
´set_color003´ ElementUnitSequence 100006
	Execute sequence: "var_set_color_red" - units/pd2_dlc_wwh/props/wwh_prop_container/001 (0.0, -300.0, 0.0)
´set_color002´ ElementUnitSequence 100005
	Execute sequence: "var_set_color_green" - units/pd2_dlc_wwh/props/wwh_prop_container/001 (0.0, -300.0, 0.0)
´set_color001´ ElementUnitSequence 100004
	Execute sequence: "var_set_color_blue" - units/pd2_dlc_wwh/props/wwh_prop_container/001 (0.0, -300.0, 0.0)
´set_tool_container´ ElementInstanceInput 100009
	BASE DELAY  (DELAY 1)
	Upon mission event "set tool container":
		Toggle on: ´open_container_so´
		Toggle on: ´activate_tool_pickup´
		Toggle on: ´activate_loot_drop_off´
		Toggle on: ´wait_container_so001´
		Execute sequence: "show_prop_only" - units/pd2_dlc_wwh/pickups/wwh_pku_saw/001 (-87.8281, -375.46, 20.0)
		Debug message: PICkED A CONTAINER
		Show object: units/pd2_dlc_friend/props/sfm_interactable_shipping_crate_2x1x1m/004 (-9.0, -500.0, 81.7743)
		Show object: units/pd2_dlc_friend/props/sfm_interactable_shipping_crate_2x1x1m/002 (-83.0, -500.0, 81.7743)
		Show object: units/pd2_dlc_friend/props/sfm_interactable_shipping_crate_2x1x1m/001 (-83.0, -500.0, 14.7743)
		Show object: units/pd2_dlc_friend/props/sfm_interactable_shipping_crate_2x1x1m/003 (-9.0, -500.0, 14.7743)
		Show object: units/pd2_dlc_dinner/props/prop_hanging_meat/din_hanging_meat_03/001 (73.0, -181.0, 266.234)
´open_container_so´ ElementSpecialObjective 100011
	DISABLED
	TRIGGER TIMES 1
	Make instigator path towards (3.0, 60.0, 0.0) and show "e_so_open_container".
´activate_tool_pickup´ ElementInstanceInput 100020
	DISABLED
	Upon mission event "Activate tool pickup":
		´2（2）´
´activate_loot_drop_off´ ElementInstanceInput 100028
	DISABLED
	Upon mission event "Activate loot drop off":
		´logic_counter_002（2）´
´wait_container_so001´ ElementSpecialObjective 100039
	DISABLED
	Make instigator path towards (3.0, 60.0, 0.0) and show "e_so_idle_by_container".
´2（2）´ ElementCounter 100041
	When counter target reached: 
		Toggle on, trigger times 1: ´func_set_outline_001´
		´point_waypoint_001´
´logic_counter_002（2）´ ElementCounter 100067
	When counter target reached: 
		Toggle on: ´trigger_area_001´
		Execute sequence: "show" - units/payday2/props/gen_prop_square_drop_marker_2x3/001 (-31.0, -263.0, 6.004)
		´loot_drop_wp´
´point_waypoint_001´ ElementWaypoint 100043
	Place waypoint with icon "pd2_generic_look" at position (0.0, 37.0, 158.0)
´func_set_outline_001´ ElementSetOutline 100048
	DISABLED
	TRIGGER TIMES 1
	Toggle on, trigger times 1: ´player_trigger´
´trigger_area_001´ ElementAreaTrigger 100027
	DISABLED
	Box, width 256.608, height 198.949, depth 575.598, at position (-2.0, -306.0, 113.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: loot
	Interval: 0.1
	Upon on_enter, execute:
		If instigator loot type is "weapon":
			Loot action: secure
			Loot action: remove
			Execute sequence: "action_add_lootbag" - units/pd2_dlc_wwh/props/wwh_prop_container/001 (0.0, -300.0, 0.0)
			Award experience: 600
			´logic_counter_001（8）´
		If instigator loot type is "money":
			Loot action: secure
			Loot action: remove
			Execute sequence: "action_add_lootbag" - units/pd2_dlc_wwh/props/wwh_prop_container/001 (0.0, -300.0, 0.0)
			Award experience: 400
			´logic_counter_001（8）´
´loot_drop_wp´ ElementWaypoint 100031
	Place waypoint with icon "pd2_lootdrop" at position (0.0, -250.0, 85.0)
´player_trigger´ ElementAreaTrigger 100050
	DISABLED
	TRIGGER TIMES 1
	Box, width 351.0, height 500.0, depth 343.875, at position (0.0, 172.0, 231.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Make teammate close to instigator say: v30
		Debug message: from instance: teammate_comment_there_he_is
		Toggle on, trigger times 1: ´second_trigger´ (DELAY 2)
´logic_counter_001（8）´ ElementCounter 100034
	When counter target reached: 
		Toggle off: ´trigger_area_001´
		remove ´loot_drop_wp´
		Execute sequence: "hide" - units/payday2/props/gen_prop_square_drop_marker_2x3/001 (-31.0, -263.0, 6.004)
´second_trigger´ MissionScriptElement 100047
	DISABLED
	TRIGGER TIMES 1
	Debug message: second_trigger_triggerd
	´open_container_so´
	Send instance event "instigator_output" to mission. (DELAY 10)
	Toggle off: ´instigator_storer´ (DELAY 10)
	Toggle off: ´instigator_storer001´ (DELAY 10)
´instigator_storer´ MissionScriptElement 100071
	´instigator_storer001´ (DELAY 1.5)
´instigator_storer001´ MissionScriptElement 100072
	´instigator_storer´
	´func_set_outline_001´
	´second_trigger´
´trigger_special_objective_001´ ElementSpecialObjectiveTrigger 100012
	TRIGGER TIMES 1
	Upon special objective event "anim_start" - ´open_container_so´:
		Execute sequence: "anim_front_door_open" - units/pd2_dlc_wwh/props/wwh_prop_container/001 (0.0, -300.0, 0.0) (DELAY 1.75)
		´logic_counter_002（2）´ (DELAY 1.75)
´picked_up_saw´ ElementUnitSequenceTrigger 100017
	TRIGGER TIMES 1
	Upon sequence "load" - units/pd2_dlc_wwh/pickups/wwh_pku_saw/001 (-87.8281, -375.46, 20.0):
		Send instance event "Picked up tool" to mission.
		remove ´tool_wp´
´tool_wp´ ElementWaypoint 100025
	Place waypoint with icon "pd2_generic_saw" at position (-88.0, -390.0, 85.0)
´gangster_waiting´ ElementSpecialObjectiveTrigger 100040
	Upon special objective event "anim_start" - ´wait_container_so001´:
		´2（2）´
		´instigator_storer´
´func_sequence_trigger_001´ ElementUnitSequenceTrigger 100042
	TRIGGER TIMES 1
	Upon sequence "anim_front_door_open" - units/pd2_dlc_wwh/props/wwh_prop_container/001 (0.0, -300.0, 0.0):
		Execute sequence: "state_interaction_enabled" - units/pd2_dlc_wwh/pickups/wwh_pku_saw/001 (-87.8281, -375.46, 20.0) (DELAY 0.5)
		remove ´point_waypoint_001´ (DELAY 0.5)
		´tool_wp´ (DELAY 0.5)
´so_input´ ElementInstanceInput 100054
	Upon mission event "So input":
		´wait_container_so001´
´area_player_close_to_equipment´ ElementAreaTrigger 100076
	TRIGGER TIMES 1
	Box, width 260.0, height 300.0, depth 300.0, at position (-13.0, -275.0, 25.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Make instigator say: v04
		Debug message: from instance: teammate_comment_got_it
