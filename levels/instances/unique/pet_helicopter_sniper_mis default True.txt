﻿´auto_hide_helicopter´ ElementUnitSequence 100002
	EXECUTE ON STARTUP
	Execute sequence: "hidden" - units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0)
	Hide object: units/payday2/architecture/mkp_int_floor_2x2m_a/001 (-60.0, -63.0, 1600.5)
´start_helicopter_sniper_logic´ ElementInstanceInput 100000
	Upon mission event "start_helicopter_sniper_logic":
		Execute sequence: "swat" - units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0)
		Execute sequence: "flyin_left_hover" - units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0)
		Execute sequence: "state_collision_enabled" - units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0)
		Execute sequence: "open_door_left" - units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0) (DELAY 24)
		´spawn_left1_helicopter001_normal´ (75.0, 32.0, 1600.5) (DELAY 25)
´spawn_left1_helicopter001_normal´ ElementSpawnEnemyDummy 100014
	Spawn ene_sniper_1 (75.0, 32.0, 1600.5) with action e_sp_aim_rifle_crh.
	Equipment no_pickup
	No group AI
	Make instigator path towards (3.0, 32.0, 1600.5) and show "AI_sniper".
´sniper_dies´ ElementEnemyDummyTrigger 100005
	Upon event "death" of ´spawn_left1_helicopter001_normal´:
		Execute sequence: "state_collision_disabled" - units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0)
		Execute sequence: "close_door_left" - units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0) (DELAY 2-4)
		Execute sequence: "hover_flyout_left" - units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0) (DELAY 4-9)
		Execute sequence: "hidden" - units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0) (DELAY 27-32)
		Send instance event "helicopter_sniper_logic_finished" to mission. (DELAY 27-32)
