﻿´startup´ ElementDisableUnit 100052
	EXECUTE ON STARTUP
	Hide object: units/payday2/architecture/mkp_int_wall_1m_a/001 (50.0, -25.0, 0.0)
	Hide object: units/payday2/architecture/mkp_int_wall_1x1m_a_trim/001 (-8.0, -25.0, 135.9)
´spawn´ ElementInstanceInput 100019
	Upon mission event "spawn":
		Execute 1:
			´dozers´
			´shields´
´dozers´ MissionScriptElement 100041
	If Very Hard, Overkill:
		If Normal, Hard:
			´dozer_green001´ (475.0, 0.0, 277.5)
		If Very Hard, Overkill:
			´dozer_black001_changed_to_green´ (475.0, 0.0, 502.5)
		If Mayhem, Death Wish, One Down:
			´dozer_skull001´ (475.0, 0.0, 727.5)
	If Mayhem, Death Wish, One Down:
		If Normal, Hard:
			´dozer_green001´ (475.0, 0.0, 277.5)
		If Very Hard, Overkill:
			´dozer_black001_changed_to_green´ (475.0, 0.0, 502.5)
		If Mayhem, Death Wish, One Down:
			´dozer_skull001´ (475.0, 0.0, 727.5)
	If Hard:
		If Normal, Hard:
			´dozer_green001´ (475.0, 0.0, 277.5)
		If Very Hard, Overkill:
			´dozer_black001_changed_to_green´ (475.0, 0.0, 502.5)
		If Mayhem, Death Wish, One Down:
			´dozer_skull001´ (475.0, 0.0, 727.5)
´shields´ MissionScriptElement 100042
	If Hard:
		If Normal, Hard:
			´shield_blue2´ (-850.0, 0.0, 502.5)
		If Very Hard, Overkill, Mayhem, Death Wish, One Down:
			´shield_green2´ (-850.0, 0.0, 727.5)
	If Very Hard, Overkill:
		If Normal, Hard:
			´shield_blue´ (-725.0, 0.0, 502.5)
		If Very Hard, Overkill, Mayhem, Death Wish, One Down:
			´shield_green´ (-725.0, 0.0, 727.5)
		If Normal, Hard:
			´shield_blue3´ (-975.0, 0.0, 502.5)
		If Very Hard, Overkill, Mayhem, Death Wish, One Down:
			´shield_green3´ (-950.0, 0.0, 727.5)
	If Mayhem, Death Wish, One Down:
		If Normal, Hard:
			´shield_blue´ (-725.0, 0.0, 502.5)
		If Very Hard, Overkill, Mayhem, Death Wish, One Down:
			´shield_green´ (-725.0, 0.0, 727.5)
		If Normal, Hard:
			´shield_blue3´ (-975.0, 0.0, 502.5)
		If Very Hard, Overkill, Mayhem, Death Wish, One Down:
			´shield_green3´ (-950.0, 0.0, 727.5)
´dozer_green001´ ElementSpawnEnemyDummy 100008
	Spawn ene_bulldozer_1.
	No group AI
	Orientations:
		(0.0, 0.0, 0.829132)
	´sniper_stand_001´
´dozer_black001_changed_to_green´ ElementSpawnEnemyDummy 100007
	Spawn ene_bulldozer_1.
	No group AI
	Orientations:
		(0.0, 0.0, 0.829132)
	´sniper_stand_001´
´dozer_skull001´ ElementSpawnEnemyDummy 100004
	Spawn ene_bulldozer_3.
	No group AI
	Orientations:
		(0.0, 0.0, 0.829132)
	´sniper_stand_001´
´shield_blue2´ ElementSpawnEnemyDummy 100015
	Spawn ene_shield_2.
	No group AI
	Orientations:
		(0.0, 0.0, 0.829132)
	´sniper_stand_001´
´shield_green2´ ElementSpawnEnemyDummy 100012
	Spawn ene_shield_1.
	No group AI
	Orientations:
		(0.0, 0.0, 0.829132)
	´sniper_stand_001´
´shield_blue´ ElementSpawnEnemyDummy 100017
	Spawn ene_shield_2.
	No group AI
	Orientations:
		(50.0, 0.0, 0.823059)
	´sniper_stand_002´
´shield_green´ ElementSpawnEnemyDummy 100014
	Spawn ene_shield_1.
	No group AI
	Orientations:
		(50.0, 0.0, 0.823059)
	´sniper_stand_002´
´shield_blue3´ ElementSpawnEnemyDummy 100016
	Spawn ene_shield_2.
	No group AI
	Orientations:
		(-50.0, -25.0, 0.823059)
	´sniper_stand_003´
´shield_green3´ ElementSpawnEnemyDummy 100013
	Spawn ene_shield_1.
	No group AI
	Orientations:
		(-50.0, -25.0, 0.823059)
	´sniper_stand_003´
´sniper_stand_001´ ElementSpecialObjective 100018
	Make person path towards (0.0, 0.0, 0.829132) and show "AI_sniper".
	Create objective for someone from any group:
		´dozer_green001´
		´dozer_black001_changed_to_green´
		´dozer_skull001´
		´shield_blue2´
		´shield_green2´
´sniper_stand_002´ ElementSpecialObjective 100056
	Make person path towards (50.0, 0.0, 0.823059) and show "AI_sniper".
	Create objective for someone from any group:
		´dozer_green´
		´dozer_black_changed_to_green´
		´dozer_skull´
		´shield_blue´
		´shield_green´
´dozer_green´ ElementSpawnEnemyDummy 100001
	Spawn ene_bulldozer_1.
	No group AI
	Orientations:
		(50.0, 0.0, 0.823059)
	´sniper_stand_002´
´dozer_black_changed_to_green´ ElementSpawnEnemyDummy 100002
	Spawn ene_bulldozer_1.
	No group AI
	Orientations:
		(50.0, 0.0, 0.823059)
	´sniper_stand_002´
´dozer_skull´ ElementSpawnEnemyDummy 100003
	Spawn ene_bulldozer_3.
	No group AI
	Orientations:
		(50.0, 0.0, 0.823059)
	´sniper_stand_002´
´sniper_stand_003´ ElementSpecialObjective 100057
	Make person path towards (-50.0, -25.0, 0.823059) and show "AI_sniper".
	Create objective for someone from any group:
		´shield_blue3´
		´shield_green3´
		´dozer_green002´
		´dozer_black002_changed_to_green´
		´dozer_skull002´
´dozer_green002´ ElementSpawnEnemyDummy 100011
	Spawn ene_bulldozer_1.
	No group AI
	Orientations:
		(-50.0, -25.0, 0.823059)
	´sniper_stand_003´
´dozer_black002_changed_to_green´ ElementSpawnEnemyDummy 100010
	Spawn ene_bulldozer_1.
	No group AI
	Orientations:
		(-50.0, -25.0, 0.823059)
	´sniper_stand_003´
´dozer_skull002´ ElementSpawnEnemyDummy 100009
	Spawn ene_bulldozer_3.
	No group AI
	Orientations:
		(-50.0, -25.0, 0.823059)
	´sniper_stand_003´
