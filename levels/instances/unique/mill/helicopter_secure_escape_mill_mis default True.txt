﻿´secure area trigger´ ElementAreaTrigger 100001
	DISABLED
	amount 1
	depth 150
	height 150
	instigator loot
	interval 0.1
	position 300.0, 15.0, 159.0
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	rules_element_ids
		1 ´data_instigator_rule_001´
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 150
	on_executed
		´secure_coke´ (delay 0)
		´secure_diamonds´ (delay 0)
		´secure_gold´ (delay 0)
		´secure_money´ (delay 0)
		´tc_loot_secured´ (delay 0)
´end_level_trigger´ ElementAreaTrigger 100004
	DISABLED
	TRIGGER TIMES 1
	amount all
	depth 400
	height 300
	instigator player
	interval 1
	position 597.0, 0.0, 0.0
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 300
	on_executed
		´func_mission_end_001´ (delay 2)
		´tc_mission_end´ (delay 0)
		´output_mission_end´ (delay 0)
´input call heli´ ElementInstanceInput 100005
	event call_helicopter
	on_executed
		´play anim heli flyin´ (delay 0)
´play anim heli flyin´ ElementUnitSequence 100006
	position -200.0, -575.0, 150.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/001 (300.0, 0.0, -1760.0)
			notify_unit_sequence diamondheist
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/001 (300.0, 0.0, -1760.0)
			notify_unit_sequence flyin_fwd_hover
			time 0
	on_executed
		´open doors´ (delay 25)
´open doors´ ElementUnitSequence 100007
	position -200.0, -425.0, 150.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/001 (300.0, 0.0, -1760.0)
			notify_unit_sequence open_door_left
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/001 (300.0, 0.0, -1760.0)
			notify_unit_sequence open_door_right
			time 0
	on_executed
		´logic_toggle_001´ (delay 0)
´logic_toggle_001´ ElementToggle 100008
	TRIGGER TIMES 1
	elements
		1 ´secure area trigger´ DISABLED
	set_trigger_times 0
	toggle on
´input enable escape´ ElementInstanceInput 100012
	event enable_escape
	on_executed
		´func_sequence_001´ (delay 0)
´logic_toggle_002´ ElementToggle 100013
	elements
		1 ´end_level_trigger´ DISABLED
	set_trigger_times 1
	toggle on
´startup´ MissionScriptElement 100014
	EXECUTE ON STARTUP
	on_executed
		´hide helicopter´ (delay 1)
´hide helicopter´ ElementUnitSequence 100015
	position -300.0, -150.0, 150.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/001 (300.0, 0.0, -1760.0)
			notify_unit_sequence hidden
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/props/gen_prop_square_goal_marker_8x15/escape_marker (600.0, 0.0, -125.0)
			notify_unit_sequence hide
			time 0
´func_sequence_001´ ElementUnitSequence 100003
	position 75.0, -425.0, 150.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/props/gen_prop_square_goal_marker_8x15/escape_marker (600.0, 0.0, -125.0)
			notify_unit_sequence show
			time 0
	on_executed
		´logic_toggle_002´ (delay 0)
´func_mission_end_001´ ElementMissionEnd 100016
	position -25.0, 225.0, 150.0
	rotation 0.0, 0.0, 0.0, -1.0
	state success
´tc_mission_end´ ElementTeammateComment 100019
	close_to_element False
	comment g24
	position -100.0, 225.0, 150.0
	radius 0
	rotation 0.0, 0.0, 0.0, -1.0
	use_instigator False
´secure_coke´ ElementCarry 100018
	operation secure
	type_filter coke
	on_executed
		´remove_coke´ (delay 0)
´remove_coke´ ElementCarry 100021
	operation remove
	type_filter coke
´secure_diamonds´ ElementCarry 100022
	operation secure
	type_filter diamonds
	on_executed
		´remove_diamonds´ (delay 0)
´remove_diamonds´ ElementCarry 100023
	operation remove
	type_filter diamonds
´secure_gold´ ElementCarry 100024
	operation secure
	type_filter gold
	on_executed
		´remove_gold´ (delay 0)
´remove_gold´ ElementCarry 100025
	operation remove
	type_filter gold
´secure_money´ ElementCarry 100026
	operation secure
	type_filter money
	on_executed
		´remove_money´ (delay 0)
´remove_money´ ElementCarry 100027
	operation remove
	type_filter money
´tc_loot_secured´ ElementTeammateComment 100028
	close_to_element False
	comment p27
	position -175.0, 225.0, 150.0
	radius 0
	rotation 0.0, 0.0, 0.0, -1.0
	use_instigator False
´output_mission_end´ ElementInstanceOutput 100029
	event mission_end
´data_instigator_rule_001´ ElementInstigatorRule 100031
	instigator loot
	invert False
	rules
		loot
			carry_ids
				1 coke
				2 cro_loot1
				3 cro_loot2
				4 diamonds
				5 gold
				6 money
´point_waypoint_001´ ElementWaypoint 100002
	icon pd2_lootdrop
	only_in_civilian False
	position 300.0, 15.0, 209.0
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´func_instance_input_001´ ElementInstanceInput 100009
	event show_lootdrop_waypoint
	on_executed
		´point_waypoint_001´ (delay 0)
