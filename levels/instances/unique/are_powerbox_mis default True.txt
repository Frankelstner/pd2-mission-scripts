﻿´start_interrupt´ ElementInstanceInput 100002
	Upon mission event "start_interrupt":
		Toggle on: ´enable_and_action_so´
		Toggle on: ´anim_end´
		Toggle on: ´anim_failed´
		´enable_and_action_so´ (DELAY 0.05)
´enable_and_action_so´ ElementToggle 100008
	DISABLED
	Toggle on, trigger times 1: ´interrupt_so´
	´interrupt_so´ (DELAY 5)
´interrupt_so´ ElementSpecialObjective 100001
	TRIGGER TIMES 1
	Create objective for enemies in 4500 radius around (0.0, 100.0, 25.0)
	Make chosen person path towards objective and show "e_so_pull_lever".
	Debug message: start_interrupt
´anim_end´ ElementSpecialObjectiveTrigger 100005
	DISABLED
	Upon special objective event "complete" - ´interrupt_so´:
		Toggle off: ´anim_end´
		Toggle off: ´enable_and_action_so´
		Toggle off: ´anim_failed´
		Toggle off: ´interrupt_so´
		remove ´interrupt_so´
		Send instance event "power_interrupted" to mission.
		´power_wp001´
		Execute sequence: "enable_interaction" - units/world/props/suburbia_circuitbreaker/001 (-5.32907e-12, 0.0, 150.0) (DELAY 0.05)
´anim_failed´ ElementSpecialObjectiveTrigger 100009
	DISABLED
	Upon special objective event "fail" - ´interrupt_so´:
		Debug message: failed_interrupt
		Send instance event "failed_interrupt" to mission.
´power_wp001´ ElementWaypoint 100016
	Place waypoint with icon "pd2_power" at position (1.52588e-05, 21.0, 150.0)
´anim_start´ ElementSpecialObjectiveTrigger 100003
	Upon special objective event "anim_start" - ´interrupt_so´:
		´open_box_once´
´open_box_once´ ElementUnitSequence 100004
	TRIGGER TIMES 1
	Execute sequence: "open_box" - units/world/props/suburbia_circuitbreaker/001 (-5.32907e-12, 0.0, 150.0)
´anim_admin´ ElementSpecialObjectiveTrigger 100006
	Upon special objective event "administered" - ´interrupt_so´:
		Debug message: administered_interrupt
´stop_interrupt´ ElementInstanceInput 100013
	Upon mission event "stop_interrupt":
		Toggle off: ´anim_end´
		Toggle off: ´enable_and_action_so´
		Toggle off: ´anim_failed´
		Toggle off: ´interrupt_so´
		remove ´interrupt_so´
´power_resumed´ ElementUnitSequenceTrigger 100017
	Upon sequence "interact" - units/world/props/suburbia_circuitbreaker/001 (-5.32907e-12, 0.0, 150.0):
		remove ´power_wp001´
		Send instance event "power_resumed" to mission.
