﻿´force_flee´ ElementInstanceInput 100000
	Upon mission event "force_flee":
		´flee_so_harasser´
´flee_so_harasser´ ElementSpecialObjective 100014
	Make person path towards (0.0, 0.0, 0.0) and show "None".
	Create objective for someone from any group:
		´harasser_norm´
		´harasser_hards´
		´harasser_over´
´harasser_norm´ ElementSpawnEnemyDummy 100016
	Spawn ene_swat_1.
	No group AI
	Send instance event "spawned" to mission.
	Orientations:
		(0.0, 0.0, 0.0)
	´Ai_hunt_enable´
	´Ai_hunt_enable´
	´Ai_hunt_enable´ (DELAY 11)
	´delay_loop_10sec´ (DELAY 12)
´harasser_hards´ ElementSpawnEnemyDummy 100017
	Spawn ene_swat_heavy_1.
	No group AI
	Send instance event "spawned" to mission.
	Orientations:
		(0.0, 0.0, 0.0)
	´Ai_hunt_enable´
	´Ai_hunt_enable´
	´Ai_hunt_enable´ (DELAY 11)
	´delay_loop_10sec´ (DELAY 12)
´harasser_over´ ElementSpawnEnemyDummy 100018
	Spawn ene_fbi_heavy_1.
	No group AI
	Send instance event "spawned" to mission.
	Orientations:
		(0.0, 0.0, 0.0)
	´Ai_hunt_enable´
	´Ai_hunt_enable´
	´Ai_hunt_enable´ (DELAY 11)
	´delay_loop_10sec´ (DELAY 12)
´delay_loop_10sec´ MissionScriptElement 100041
	BASE DELAY  (DELAY 10)
	´Ai_hunt_enable´
	´delay_loop_10sec´ (DELAY 1)
´Ai_hunt_enable´ ElementSpecialObjective 100037
	DISABLED
	Make person path towards (-200.0, 1100.0, 0.0) and show "AI_hunt".
	Create objective for someone from any group:
		´harasser_hards´
		´harasser_norm´
		´harasser_over´
´force_spawn´ ElementInstanceInput 100001
	Upon mission event "force_spawn":
		´harasser´
		´on´
´harasser´ MissionScriptElement 100009
	TRIGGER TIMES 1
	If Normal:
		´harasser_norm´ (100.0, 900.0, 0.0)
	If Hard, Very Hard:
		´harasser_hards´ (100.0, 1000.0, 0.0)
	If Overkill, Mayhem, Death Wish, One Down:
		´harasser_over´ (100.0, 1100.0, 0.0)
´on´ ElementToggle 100026
	TRIGGER TIMES 1
	Toggle on: ´start_assault´
	Toggle on: ´end_assault´
´start_assault´ ElementGlobalEventTrigger 100002
	DISABLED
	Upon global event "start_assault":
		Debug message: start
		´harasser´
		´on´
´end_assault´ ElementGlobalEventTrigger 100019
	DISABLED
	Upon global event "end_assault":
		´flee_so_harasser´
´harasser_death´ ElementEnemyDummyTrigger 100011
	Upon any event:
		death: ´harasser_norm´
		death: ´harasser_hards´
		death: ´harasser_over´
	Execute:
		Reset value of: ´countdown_counter（15）´
		Debug message: timer reset
		If ´countdown_counter（15）´ > 0: (DELAY 0.1-10.1)
			´countdown_counter（15）´ -= 1 (DELAY 0.9)
			´if_above_0´ (DELAY 1)
´countdown_counter（15）´ ElementCounter 100036
	Instance var name: var_delay_between_spawns
´if_above_0´ ElementCounterFilter 100024
	If ´countdown_counter（15）´ > 0:
		´countdown_counter（15）´ -= 1 (DELAY 0.9)
		´if_above_0´ (DELAY 1)
´harasser_fled´ ElementSpecialObjectiveTrigger 100013
	Upon special objective event "complete" - ´flee_so_harasser´:
		Remove instigator from game.
		Reset value of: ´countdown_counter（15）´
		Debug message: timer reset
		If ´countdown_counter（15）´ > 0: (DELAY 0.1-10.1)
			´countdown_counter（15）´ -= 1 (DELAY 0.9)
			´if_above_0´ (DELAY 1)
´enable_spawn_loop´ ElementInstanceInput 100027
	Upon mission event "enable_spawn_loop":
		If Assault:
			´harasser´
		´on´
´disable_spawn_loop´ ElementInstanceInput 100028
	Upon mission event "disable_spawn_loop":
		Toggle off: ´end_assault´
		Toggle off: ´start_assault´
		´flee_so_harasser´
´disable_harasser_spawn_and_flee´ ElementInstanceInput 100008
	Upon mission event "disable_harasser_spawn_and_flee":
		´off_2´
		´flee_so_harasser´
´off_2´ ElementToggle 100030
	TRIGGER TIMES 1
	Toggle off: ´start_assault´
	Toggle off: ´end_assault´
	Toggle off: ´harasser´
	Toggle off: ´enable_harasser´
	Toggle off: ´harasser_norm´
	Toggle off: ´harasser_hards´
	Toggle off: ´harasser_over´
´enable_harasser´ ElementToggle 100004
	Toggle on, trigger times 1: ´harasser´
	If Assault: (DELAY 0.05)
		´harasser´
´reached_0´ ElementCounterTrigger 100025
	Upon event ´countdown_counter（15）´ == 0:
		Debug message: timer done
		´enable_harasser´
´enable_AI_Hunt´ ElementInstanceInput 100039
	Upon mission event "enable_ai_hunt":
		Toggle on: ´Ai_hunt_enable´
