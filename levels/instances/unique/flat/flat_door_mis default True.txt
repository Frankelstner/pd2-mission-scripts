﻿´door_startup´ ElementDisableUnit 100015
	EXECUTE ON STARTUP
	Hide object: units/pd2_dlc_flat/props/flt_prop_door_bars/001 (15.0, -61.0, 0.004004)
	Execute sequence: "turn_on" - units/pd2_dlc_flat/props/apartment_door/flt_prop_interactable_door_corridor/001 (15.0, -7.0, 0.004004)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-50.0, -100.0, 0.0)
´inst_in_block_door´ ElementInstanceInput 100017
	Upon mission event "door_block":
		Show object: units/pd2_dlc_flat/props/flt_prop_door_bars/001 (15.0, -61.0, 0.004004)
		Execute sequence: "power_off" - units/pd2_dlc_flat/props/apartment_door/flt_prop_interactable_door_corridor/001 (15.0, -7.0, 0.004004)
		Execute sequence: "deactivate_door" - units/pd2_dlc_flat/props/apartment_door/flt_prop_interactable_door_corridor/001 (15.0, -7.0, 0.004004)
		Execute sequence: "turn_off" - units/pd2_dlc_flat/props/apartment_door/flt_prop_interactable_door_corridor/001 (15.0, -7.0, 0.004004)
´inst_in_open_door´ ElementInstanceInput 100020
	TRIGGER TIMES 1
	Upon mission event "door_open":
		Hide object: units/pd2_dlc_flat/props/flt_prop_door_bars/001 (15.0, -61.0, 0.004004)
		Execute sequence: "anim_door_open" - units/pd2_dlc_flat/props/apartment_door/flt_prop_interactable_door_corridor/001 (15.0, -7.0, 0.004004)
		Remove nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-50.0, -100.0, 0.0)
´inst_in_enable_equipment´ ElementInstanceInput 100030
	Upon mission event "door_enable_equipment":
		Execute sequence: "turn_on" - units/pd2_dlc_flat/props/apartment_door/flt_prop_interactable_door_corridor/001 (15.0, -7.0, 0.004004)
		Execute sequence: "activate_door" - units/pd2_dlc_flat/props/apartment_door/flt_prop_interactable_door_corridor/001 (15.0, -7.0, 0.004004)
		Execute sequence: "power_on" - units/pd2_dlc_flat/props/apartment_door/flt_prop_interactable_door_corridor/001 (15.0, -7.0, 0.004004)
´func_sequence_trigger_001´ ElementUnitSequenceTrigger 100011
	TRIGGER TIMES 1
	Upon sequence "done_opened" - units/pd2_dlc_flat/props/apartment_door/flt_prop_interactable_door_corridor/001 (15.0, -7.0, 0.004004):
		Send instance event "door_opened" to mission.
´hide_instance´ ElementInstanceInput 100004
	Upon mission event "hide instance":
		Execute sequence: "state_vis_hide" - units/pd2_dlc_flat/props/apartment_door/flt_prop_interactable_door_corridor/001 (15.0, -7.0, 0.004004)
		Hide object: units/pd2_dlc_flat/props/flt_prop_door_bars/001 (15.0, -61.0, 0.004004)
		Remove nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-50.0, -100.0, 0.0)
