﻿´input_set_turbo_man´ ElementInstanceInput 100019
	Upon mission event "set_turbo_man":
		Execute 1:
			´set_turbo_man001´
			´set_turbo_man002´
			´set_turbo_man004´
			´set_turbo_man005´
			´set_turbo_man006´
			´set_turbo_man007´
			´set_turbo_man009´
			´set_turbo_man010´
			´set_turbo_man011´
´set_turbo_man001´ ElementUnitSequence 100064
	DISABLED
	TRIGGER TIMES 1
	Execute sequence: "var_content_moon" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_001 (-1.32892, 65.484, 49.0)
	Toggle: ´set_others001´
	Toggle: ´gift_opened001´
´set_turbo_man002´ ElementUnitSequence 100065
	DISABLED
	TRIGGER TIMES 1
	Execute sequence: "var_content_moon" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_002 (-42.0, 7.0, 48.0)
	Toggle: ´set_others002´
	Toggle: ´gift_opened002´
´set_turbo_man004´ ElementUnitSequence 100067
	DISABLED
	TRIGGER TIMES 1
	Execute sequence: "var_content_moon" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_004 (17.0, 3.99997, 0.0)
	Toggle: ´set_others004´
	Toggle: ´gift_opened004´
´set_turbo_man005´ ElementUnitSequence 100068
	DISABLED
	TRIGGER TIMES 1
	Execute sequence: "var_content_moon" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_001 (-6.0, 21.0, 47.0)
	Toggle: ´set_others005´
	Toggle: ´gift_opened005´
´set_turbo_man006´ ElementUnitSequence 100069
	DISABLED
	TRIGGER TIMES 1
	Execute sequence: "var_content_moon" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_002 (-25.0, -40.0, 0.0)
	Toggle: ´set_others006´
	Toggle: ´gift_opened006´
´set_turbo_man007´ ElementUnitSequence 100070
	DISABLED
	TRIGGER TIMES 1
	Execute sequence: "var_content_moon" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_003 (-7.0, 71.0, -1.0)
	Toggle: ´set_others007´
	Toggle: ´gift_opened007´
´set_turbo_man009´ ElementUnitSequence 100072
	DISABLED
	TRIGGER TIMES 1
	Execute sequence: "var_content_moon" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_001 (8.0, 5.0, 42.0)
	Toggle: ´set_others009´
	Toggle: ´gift_opened009´
´set_turbo_man010´ ElementUnitSequence 100073
	DISABLED
	TRIGGER TIMES 1
	Execute sequence: "var_content_moon" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_002 (-49.0, 19.0, 0.0)
	Toggle: ´set_others010´
	Toggle: ´gift_opened010´
´set_turbo_man011´ ElementUnitSequence 100074
	DISABLED
	TRIGGER TIMES 1
	Execute sequence: "var_content_moon" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_003 (16.0, 58.0, 0.0)
	Toggle: ´set_others011´
	Toggle: ´gift_opened011´
´set_others001´ ElementUnitSequence 100076
	DISABLED
	Execute sequence: "var_content_empty" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_001 (-1.32892, 65.484, 49.0)
´gift_opened001´ ElementUnitSequenceTrigger 100060
	DISABLED
	TRIGGER TIMES 1
	Upon sequence "int_seq_open" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_001 (-1.32892, 65.484, 49.0):
		´activate_WP_toy_found´
´set_others002´ ElementUnitSequence 100077
	DISABLED
	Execute sequence: "var_content_2" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_002 (-42.0, 7.0, 48.0)
´gift_opened002´ ElementUnitSequenceTrigger 100103
	DISABLED
	TRIGGER TIMES 1
	Upon sequence "int_seq_open" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_002 (-42.0, 7.0, 48.0):
		´activate_WP_toy_found´
´set_others004´ ElementUnitSequence 100079
	DISABLED
	Execute sequence: "var_content_4" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_004 (17.0, 3.99997, 0.0)
´gift_opened004´ ElementUnitSequenceTrigger 100105
	DISABLED
	TRIGGER TIMES 1
	Upon sequence "int_seq_open" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_004 (17.0, 3.99997, 0.0):
		´activate_WP_toy_found´
´set_others005´ ElementUnitSequence 100080
	DISABLED
	Execute sequence: "var_content_1" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_001 (-6.0, 21.0, 47.0)
´gift_opened005´ ElementUnitSequenceTrigger 100106
	DISABLED
	TRIGGER TIMES 1
	Upon sequence "int_seq_open" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_001 (-6.0, 21.0, 47.0):
		´activate_WP_toy_found´
´set_others006´ ElementUnitSequence 100081
	DISABLED
	Execute sequence: "var_content_empty" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_002 (-25.0, -40.0, 0.0)
´gift_opened006´ ElementUnitSequenceTrigger 100107
	DISABLED
	TRIGGER TIMES 1
	Upon sequence "int_seq_open" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_002 (-25.0, -40.0, 0.0):
		´activate_WP_toy_found´
´set_others007´ ElementUnitSequence 100082
	DISABLED
	Execute sequence: "var_content_3" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_003 (-7.0, 71.0, -1.0)
´gift_opened007´ ElementUnitSequenceTrigger 100108
	DISABLED
	TRIGGER TIMES 1
	Upon sequence "int_seq_open" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_003 (-7.0, 71.0, -1.0):
		´activate_WP_toy_found´
´set_others009´ ElementUnitSequence 100084
	DISABLED
	Execute sequence: "var_content_1" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_001 (8.0, 5.0, 42.0)
´gift_opened009´ ElementUnitSequenceTrigger 100110
	DISABLED
	TRIGGER TIMES 1
	Upon sequence "int_seq_open" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_001 (8.0, 5.0, 42.0):
		´activate_WP_toy_found´
´set_others010´ ElementUnitSequence 100085
	DISABLED
	Execute sequence: "var_content_2" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_002 (-49.0, 19.0, 0.0)
´gift_opened010´ ElementUnitSequenceTrigger 100111
	DISABLED
	TRIGGER TIMES 1
	Upon sequence "int_seq_open" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_002 (-49.0, 19.0, 0.0):
		´activate_WP_toy_found´
´set_others011´ ElementUnitSequence 100086
	DISABLED
	Execute sequence: "var_content_empty" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_003 (16.0, 58.0, 0.0)
´gift_opened011´ ElementUnitSequenceTrigger 100112
	DISABLED
	TRIGGER TIMES 1
	Upon sequence "int_seq_open" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_003 (16.0, 58.0, 0.0):
		´activate_WP_toy_found´
´activate_WP_toy_found´ MissionScriptElement 100114
	TRIGGER TIMES 1
	´WP_toy_found´
´WP_toy_found´ ElementWaypoint 100057
	Place waypoint with icon "pd2_generic_interact" at position (0.0, 0.0, 151.0)
´input_set_pile1´ ElementInstanceInput 100023
	Upon mission event "set_pile1":
		Show object: units/payday2/props/gifts/com_prop_christmas_gift_3/christmas_gift_pile1_003 (0.0, 65.0, 0.0)
		Show object: units/payday2/props/gifts/com_prop_christmas_gift_3/christmas_gift_pile1_005 (-46.0, 6.0, 0.0)
		Show object: units/equipment/hospital_saw_teddy/001 (-14.0, -57.0, -4.0)
		Hide object: units/payday2/props/gifts/com_prop_christmas_gift_3/christmas_gift_pile2_003 (-9.0, 17.0, -1.0)
		Hide object: units/payday2/props/gifts/com_prop_christmas_gift_2/christmas_gift_pile3_004 (9.00003, 4.0, 0.0)
		Toggle on: ´set_turbo_man001´
		Toggle on: ´set_turbo_man002´
		Toggle on: ´set_turbo_man004´
		Toggle on: ´set_others001´
		Toggle on: ´set_others002´
		Toggle on: ´set_others004´
		Toggle on: ´enable_interactions_no_lid001´
		Execute sequence: "state_vis_hide" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_001 (-6.0, 21.0, 47.0)
		Execute sequence: "state_vis_hide" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_002 (-25.0, -40.0, 0.0)
		Execute sequence: "state_vis_hide" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_003 (-7.0, 71.0, -1.0)
		Execute sequence: "state_vis_hide" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_001 (8.0, 5.0, 42.0)
		Execute sequence: "state_vis_hide" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_002 (-49.0, 19.0, 0.0)
		Execute sequence: "state_vis_hide" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_003 (16.0, 58.0, 0.0)
´enable_interactions_no_lid001´ ElementUnitSequence 100047
	DISABLED
	Execute sequence: "state_interaction_enable_no_lid" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_001 (-1.32892, 65.484, 49.0)
	Execute sequence: "state_interaction_enable_no_lid" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_002 (-42.0, 7.0, 48.0)
	Execute sequence: "state_interaction_enable_no_lid" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_004 (17.0, 3.99997, 0.0)
	Execute sequence: "state_outline_enabled" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_001 (-1.32892, 65.484, 49.0)
	Execute sequence: "state_outline_enabled" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_002 (-42.0, 7.0, 48.0)
	Execute sequence: "state_outline_enabled" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_004 (17.0, 3.99997, 0.0)
´input_set_pile2´ ElementInstanceInput 100045
	Upon mission event "set_pile2":
		Hide object: units/payday2/props/gifts/com_prop_christmas_gift_3/christmas_gift_pile1_003 (0.0, 65.0, 0.0)
		Hide object: units/payday2/props/gifts/com_prop_christmas_gift_3/christmas_gift_pile1_005 (-46.0, 6.0, 0.0)
		Hide object: units/equipment/hospital_saw_teddy/001 (-14.0, -57.0, -4.0)
		Hide object: units/payday2/props/gifts/com_prop_christmas_gift_2/christmas_gift_pile3_004 (9.00003, 4.0, 0.0)
		Show object: units/payday2/props/gifts/com_prop_christmas_gift_3/christmas_gift_pile2_003 (-9.0, 17.0, -1.0)
		Toggle on: ´set_turbo_man005´
		Toggle on: ´set_turbo_man006´
		Toggle on: ´set_turbo_man007´
		Toggle on: ´set_others005´
		Toggle on: ´set_others006´
		Toggle on: ´set_others007´
		Toggle on: ´enable_interactions_no_lid002´
		Execute sequence: "state_vis_hide" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_001 (-1.32892, 65.484, 49.0)
		Execute sequence: "state_vis_hide" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_002 (-42.0, 7.0, 48.0)
		Execute sequence: "state_vis_hide" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_004 (17.0, 3.99997, 0.0)
		Execute sequence: "state_vis_hide" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_001 (8.0, 5.0, 42.0)
		Execute sequence: "state_vis_hide" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_002 (-49.0, 19.0, 0.0)
		Execute sequence: "state_vis_hide" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_003 (16.0, 58.0, 0.0)
´enable_interactions_no_lid002´ ElementUnitSequence 100062
	DISABLED
	Execute sequence: "state_interaction_enable_no_lid" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_001 (-6.0, 21.0, 47.0)
	Execute sequence: "state_interaction_enable_no_lid" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_002 (-25.0, -40.0, 0.0)
	Execute sequence: "state_interaction_enable_no_lid" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_003 (-7.0, 71.0, -1.0)
	Execute sequence: "state_outline_enabled" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_001 (-6.0, 21.0, 47.0)
	Execute sequence: "state_outline_enabled" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_002 (-25.0, -40.0, 0.0)
	Execute sequence: "state_outline_enabled" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_003 (-7.0, 71.0, -1.0)
´input_set_pile3´ ElementInstanceInput 100058
	Upon mission event "set_pile3":
		Hide object: units/payday2/props/gifts/com_prop_christmas_gift_3/christmas_gift_pile2_003 (-9.0, 17.0, -1.0)
		Hide object: units/payday2/props/gifts/com_prop_christmas_gift_3/christmas_gift_pile1_003 (0.0, 65.0, 0.0)
		Hide object: units/payday2/props/gifts/com_prop_christmas_gift_3/christmas_gift_pile1_005 (-46.0, 6.0, 0.0)
		Hide object: units/equipment/hospital_saw_teddy/001 (-14.0, -57.0, -4.0)
		Show object: units/payday2/props/gifts/com_prop_christmas_gift_2/christmas_gift_pile3_004 (9.00003, 4.0, 0.0)
		Toggle on: ´set_turbo_man009´
		Toggle on: ´set_turbo_man010´
		Toggle on: ´set_turbo_man011´
		Toggle on: ´set_others009´
		Toggle on: ´set_others010´
		Toggle on: ´set_others011´
		Toggle on: ´enable_interactions_no_lid003´
		Execute sequence: "state_vis_hide" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_001 (-1.32892, 65.484, 49.0)
		Execute sequence: "state_vis_hide" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_002 (-42.0, 7.0, 48.0)
		Execute sequence: "state_vis_hide" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_004 (17.0, 3.99997, 0.0)
		Execute sequence: "state_vis_hide" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_001 (-6.0, 21.0, 47.0)
		Execute sequence: "state_vis_hide" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_002 (-25.0, -40.0, 0.0)
		Execute sequence: "state_vis_hide" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_003 (-7.0, 71.0, -1.0)
´enable_interactions_no_lid003´ ElementUnitSequence 100063
	DISABLED
	Execute sequence: "state_interaction_enable_no_lid" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_001 (8.0, 5.0, 42.0)
	Execute sequence: "state_interaction_enable_no_lid" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_002 (-49.0, 19.0, 0.0)
	Execute sequence: "state_interaction_enable_no_lid" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_003 (16.0, 58.0, 0.0)
	Execute sequence: "state_outline_enabled" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_001 (8.0, 5.0, 42.0)
	Execute sequence: "state_outline_enabled" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_002 (-49.0, 19.0, 0.0)
	Execute sequence: "state_outline_enabled" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_003 (16.0, 58.0, 0.0)
´area_player_carries_toy´ ElementAreaTrigger 100051
	TRIGGER TIMES 1
	Box, width 50000.0, height 50000.0, depth 50000.0, at position (200.0, -1100.0, 0.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Rule: ´data_instigator_rule_toy´
	Upon on_enter, execute:
		Debug message: player carries special loot
		remove ´WP_toy_found´
		Send instance event "player_carries_toy" to mission.
		Execute sequence: "state_outline_disabled" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_001 (-1.32892, 65.484, 49.0)
		Execute sequence: "state_outline_disabled" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_002 (-42.0, 7.0, 48.0)
		Execute sequence: "state_outline_disabled" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_004 (17.0, 3.99997, 0.0)
		Execute sequence: "state_outline_disabled" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_001 (-6.0, 21.0, 47.0)
		Execute sequence: "state_outline_disabled" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_002 (-25.0, -40.0, 0.0)
		Execute sequence: "state_outline_disabled" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_003 (-7.0, 71.0, -1.0)
		Execute sequence: "state_outline_disabled" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_001 (8.0, 5.0, 42.0)
		Execute sequence: "state_outline_disabled" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_002 (-49.0, 19.0, 0.0)
		Execute sequence: "state_outline_disabled" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_003 (16.0, 58.0, 0.0)
		Execute sequence: "state_interact_disable" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_001 (-1.32892, 65.484, 49.0)
		Execute sequence: "state_interact_disable" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_002 (-42.0, 7.0, 48.0)
		Execute sequence: "state_interact_disable" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile1_004 (17.0, 3.99997, 0.0)
		Execute sequence: "state_interact_disable" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_001 (-6.0, 21.0, 47.0)
		Execute sequence: "state_interact_disable" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_002 (-25.0, -40.0, 0.0)
		Execute sequence: "state_interact_disable" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile2_003 (-7.0, 71.0, -1.0)
		Execute sequence: "state_interact_disable" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_001 (8.0, 5.0, 42.0)
		Execute sequence: "state_interact_disable" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_002 (-49.0, 19.0, 0.0)
		Execute sequence: "state_interact_disable" - units/payday2/props/gifts/com_prop_christmas_gift_multi/christmas_gift_interaction_pile3_003 (16.0, 58.0, 0.0)
´data_instigator_rule_toy´ ElementInstigatorRule 100052
	  robot_toy
´input_enable_interactions´ ElementInstanceInput 100046
	Upon mission event "enable_interactions":
		´set_others001´
		´set_others002´
		´set_others004´
		´set_others005´
		´set_others006´
		´set_others007´
		´set_others009´
		´set_others010´
		´set_others011´
		´enable_interactions_no_lid001´
		´enable_interactions_no_lid002´
		´enable_interactions_no_lid003´
