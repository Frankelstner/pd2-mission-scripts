﻿´setup_computer´ ElementRandom 100009
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Execute 1:
		´screen001´
		´screen002´
		´screen003´
		´screen004´
´screen001´ ElementUnitSequence 100008
	Execute sequence: "main_screen_on_v1" - units/payday2/equipment/gen_interactable_hack_computer_b/001 (-31.0, 33.0, 80.0)
´screen002´ ElementUnitSequence 100011
	Execute sequence: "main_screen_on_v2" - units/payday2/equipment/gen_interactable_hack_computer_b/001 (-31.0, 33.0, 80.0)
´screen003´ ElementUnitSequence 100012
	Execute sequence: "main_screen_on_v3" - units/payday2/equipment/gen_interactable_hack_computer_b/001 (-31.0, 33.0, 80.0)
´screen004´ ElementUnitSequence 100013
	Execute sequence: "main_screen_on_v4" - units/payday2/equipment/gen_interactable_hack_computer_b/001 (-31.0, 33.0, 80.0)
´instance_input_enable_interaction´ ElementInstanceInput 100016
	Upon mission event "I_am_the_chosen_one":
		Execute sequence: "state_interaction_enabled" - units/payday2/equipment/gen_interactable_hack_computer_b/001 (-31.0, 33.0, 80.0)
		´WP_computer_hack´
´WP_computer_hack´ ElementWaypoint 100018
	Place waypoint with icon "pd2_computer" at position (-29.0, 33.0, 150.0)
´setup_computer_2´ ElementUnitSequenceTrigger 100017
	TRIGGER TIMES 1
	Upon sequence "interact" - units/payday2/equipment/gen_interactable_hack_computer_b/001 (-31.0, 33.0, 80.0):
		Toggle on, trigger times 1: ´interrupt_hacking´ (DELAY 0-5)
		Toggle on, trigger times 1: ´interrupted´ (DELAY 0-5)
		Send instance event "player_set_up_computer" to mission.
		Execute sequence: "state_device_start" - units/payday2/equipment/gen_interactable_hack_computer_b/001 (-31.0, 33.0, 80.0)
		Execute sequence: "main_screen_off" - units/payday2/equipment/gen_interactable_hack_computer_b/001 (-31.0, 33.0, 80.0)
		Toggle on: ´fixed_computer´ (DELAY 1)
		´interrupt_hacking´ (DELAY 5-10)
´interrupt_hacking´ ElementSpecialObjective 100006
	TRIGGER TIMES 1
	Create objective for enemies in 2000 radius around (-25.0, 200.0, 0.0)
	Make chosen person path towards objective and show "e_so_interact_mid".
´interrupted´ MissionScriptElement 100023
	Toggle off: ´interrupt_failed´
	remove ´interrupt_hacking´
	remove ´WP_computer_hack´
	remove ´interrupt_hacking´
	´interrupt_computer´
	´WP_computer_fix´
´fixed_computer´ ElementUnitSequenceTrigger 100026
	DISABLED
	Upon sequence "interact" - units/payday2/equipment/gen_interactable_hack_computer_b/001 (-31.0, 33.0, 80.0):
		Toggle on: ´interrupt_failed´
		Execute sequence: "state_device_resumed" - units/payday2/equipment/gen_interactable_hack_computer_b/001 (-31.0, 33.0, 80.0)
		remove ´WP_computer_fix´
		Toggle on, trigger times 1: ´interrupt_hacking´
		Toggle on, trigger times 1: ´interrupted´
		Send instance event "fixed_computer" to mission.
		´WP_computer_hack´
		´interrupt_hacking´ (DELAY 5)
´interrupt_computer´ ElementUnitSequence 100022
	Execute sequence: "state_device_jammed" - units/payday2/equipment/gen_interactable_hack_computer_b/001 (-31.0, 33.0, 80.0)
´WP_computer_fix´ ElementWaypoint 100024
	Place waypoint with icon "pd2_fix" at position (-29.0, 33.0, 150.0)
´interrupt_failed´ ElementSpecialObjectiveTrigger 100029
	Upon special objective event "fail" - ´interrupt_hacking´:
		Debug message: anim fail
		Toggle off: ´interrupted´
		Toggle on, trigger times 1: ´interrupt_hacking´ (DELAY 5)
		Toggle on, trigger times 1: ´interrupted´ (DELAY 5)
		´interrupt_hacking´ (DELAY 10)
´interrupt_started´ ElementSpecialObjectiveTrigger 100021
	Upon special objective event "anim_start" - ´interrupt_hacking´:
		´interrupted´ (DELAY 2)
´hacking_done´ ElementUnitSequenceTrigger 100034
	TRIGGER TIMES 1
	Upon sequence "timer_done" - units/payday2/equipment/gen_interactable_hack_computer_b/001 (-31.0, 33.0, 80.0):
		remove ´interrupt_hacking´
		Send instance event "hacking_done" to mission.
		remove ´WP_computer_hack´
		remove ´interrupt_hacking´
		Toggle off: ´interrupt_hacking´
		Toggle off: ´interrupted´
		Toggle off: ´interrupt_started´
		Toggle off: ´interrupt_failed´
		Toggle off: ´interrupted´
		Toggle off: ´interrupt_computer´
		Toggle off: ´interrupt_complete´
		Toggle off: ´interrupt_hacking´
´interrupt_complete´ ElementSpecialObjectiveTrigger 100042
	Upon special objective event "complete" - ´interrupt_hacking´:
		remove ´interrupt_hacking´
