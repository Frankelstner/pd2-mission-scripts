﻿´auto_hide_helicopter´ ElementUnitSequence 100002
	EXECUTE ON STARTUP
	Execute sequence: "hidden" - units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0)
´start_helicopter_swat_logic´ ElementInstanceInput 100000
	Upon mission event "start_helicopter_swat_logic":
		Execute sequence: "swat_night" - units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0)
		Execute sequence: "flyin_fwd_hover" - units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0)
		Execute sequence: "open_door_left" - units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0) (DELAY 23)
		Execute sequence: "open_door_right" - units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0) (DELAY 23)
		´spawn_right1_helicopter001´ (225.0, 0.0, 0.0) (DELAY 25)
		´spawn_left1_helicopter001´ (-175.0, -25.0, 0.0) (DELAY 27)
		´spawn_right2_helicopter001´ (325.0, 0.0, 0.0) (DELAY 31)
		´spawn_left2_helicopter001´ (-275.0, -25.0, 0.0) (DELAY 33)
		Execute sequence: "close_door_left" - units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0) (DELAY 39)
		Execute sequence: "close_door_right" - units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0) (DELAY 39)
		Execute sequence: "hover_flyout_back" - units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0) (DELAY 45)
		Execute sequence: "hidden" - units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0) (DELAY 68)
		Send instance event "helicopter_swat_logic_finished" to mission. (DELAY 68)
´spawn_left2_helicopter001´ ElementSpawnEnemyDummy 100013
	Spawn ene_swat_1 with action e_sp_down_16m_left.
	Orientations:
		(-200.0, 75.0, 0.0)
´spawn_left1_helicopter001´ ElementSpawnEnemyDummy 100014
	Spawn ene_swat_1 with action e_sp_down_16m_left.
	Orientations:
		(-200.0, 75.0, 0.0)
´spawn_right1_helicopter001´ ElementSpawnEnemyDummy 100015
	Spawn ene_swat_1 with action e_sp_down_16m_right.
	Orientations:
		(200.0, -25.0, 0.0)
´spawn_right2_helicopter001´ ElementSpawnEnemyDummy 100016
	Spawn ene_swat_1 with action e_sp_down_16m_right.
	Orientations:
		(200.0, -25.0, 0.0)
