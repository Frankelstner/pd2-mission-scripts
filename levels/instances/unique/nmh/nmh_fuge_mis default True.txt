﻿´startup´ ElementUnitSequence 100000
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Execute sequence: "hide" - units/pd2_dlc_nmh/props/rack_2/nmh_hospital_veil_interaction_centrifuge/veil_interaction (0.0, 0.0, 0.0)
´input_enable_interaction´ ElementInstanceInput 100007
	Upon mission event "enable_interaction":
		Execute sequence: "	enable_interaction	" - units/pd2_dlc_nmh/props/nmh_interactable_blood_centrifuge/centrifuge (0.0, 0.0, 0.0)
		Execute sequence: "state_reset" - units/pd2_dlc_nmh/props/rack_2/nmh_hospital_veil_interaction_centrifuge/veil_interaction (0.0, 0.0, 0.0)
		Debug message: enable_interaction
´interacted_with_centrifuge´ ElementUnitSequenceTrigger 100015
	TRIGGER TIMES 9
	Upon sequence "interact" - units/pd2_dlc_nmh/props/nmh_interactable_blood_centrifuge/centrifuge (0.0, 0.0, 0.0):
		Execute sequence: "show" - units/pd2_dlc_nmh/props/rack_2/nmh_hospital_veil_interaction_centrifuge/veil_interaction (0.0, 0.0, 0.0)
		Debug message: show
		If 28% chance to execute succeeds:
			Execute sequence: "success" - units/pd2_dlc_nmh/props/nmh_interactable_blood_centrifuge/centrifuge (0.0, 0.0, 0.0) (DELAY 30)
			Execute sequence: "enable_interaction" - units/pd2_dlc_nmh/props/rack_2/nmh_hospital_veil_interaction_centrifuge/veil_interaction (0.0, 0.0, 0.0) (DELAY 30)
			Execute sequence: "show" - units/pd2_dlc_nmh/props/rack_2/nmh_hospital_veil_interaction_centrifuge/veil_interaction (0.0, 0.0, 0.0) (DELAY 30)
			Send instance event "sample_success" to mission. (DELAY 30)
			Execute sequence: "anim_spin_stop" - units/pd2_dlc_nmh/props/rack_2/nmh_hospital_veil_interaction_centrifuge/veil_interaction (0.0, 0.0, 0.0) (DELAY 30)
			Debug message: seq_success (DELAY 30)
		Else:
			Execute sequence: "fail" - units/pd2_dlc_nmh/props/nmh_interactable_blood_centrifuge/centrifuge (0.0, 0.0, 0.0) (DELAY 15-20)
			Send instance event "sample_failed" to mission. (DELAY 15-20)
			Execute sequence: "hide" - units/pd2_dlc_nmh/props/rack_2/nmh_hospital_veil_interaction_centrifuge/veil_interaction (0.0, 0.0, 0.0) (DELAY 15-20)
			´2（2）´ (DELAY 15-20)
			´logic_link_001´ (DELAY 15-20)
			Execute sequence: "cleaning" - units/pd2_dlc_nmh/props/nmh_interactable_blood_centrifuge/centrifuge (0.0, 0.0, 0.0) (DELAY 30-35)
			Debug message: seq_cleaning (DELAY 30-35)
		Execute sequence: "anim_spin_start" - units/pd2_dlc_nmh/props/rack_2/nmh_hospital_veil_interaction_centrifuge/veil_interaction (0.0, 0.0, 0.0) (DELAY 1.5)
		Debug message: anim_spin_start (DELAY 1.5)
´2（2）´ ElementCounter 100022
	When counter target reached: 
		Toggle off: ´logic_link_001´
		If 5% chance to execute succeeds:
			Play dialogue: Play_pln_nmh_71
´logic_link_001´ MissionScriptElement 100023
	TRIGGER TIMES 1
	Play dialogue: Play_pln_nmh_71
´interacted_with_correct_sample´ ElementUnitSequenceTrigger 100032
	Upon sequence "load" - units/pd2_dlc_nmh/props/rack_2/nmh_hospital_veil_interaction_centrifuge/veil_interaction (0.0, 0.0, 0.0):
		Send instance event "took_correct_sample" to mission.
		Execute sequence: "hide" - units/pd2_dlc_nmh/props/rack_2/nmh_hospital_veil_interaction_centrifuge/veil_interaction (0.0, 0.0, 0.0)
		Execute sequence: "disable_interaction" - units/pd2_dlc_nmh/props/rack_2/nmh_hospital_veil_interaction_centrifuge/veil_interaction (0.0, 0.0, 0.0)
		Execute sequence: "	enable_interaction	" - units/pd2_dlc_nmh/props/nmh_interactable_blood_centrifuge/centrifuge (0.0, 0.0, 0.0) (DELAY 15)
		Execute sequence: "state_reset" - units/pd2_dlc_nmh/props/rack_2/nmh_hospital_veil_interaction_centrifuge/veil_interaction (0.0, 0.0, 0.0) (DELAY 15)
		Debug message: enable_interaction (DELAY 15)
´input_disable_interaction´ ElementInstanceInput 100009
	Upon mission event "disable_interaction":
		Execute sequence: "	disable_interaction		" - units/pd2_dlc_nmh/props/nmh_interactable_blood_centrifuge/centrifuge (0.0, 0.0, 0.0)
		Execute sequence: "disable_interaction" - units/pd2_dlc_nmh/props/rack_2/nmh_hospital_veil_interaction_centrifuge/veil_interaction (0.0, 0.0, 0.0)
