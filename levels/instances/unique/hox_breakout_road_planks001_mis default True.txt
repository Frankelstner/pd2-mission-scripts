﻿´show´ ElementInstanceInput 100002
	Upon mission event "show":
		Execute sequence: "state_show" - units/pd2_dlc2/architecture/ext_road_c/str_construction_metal_sheet_interactable/001 (0.0, 0.0, 4.0)
´enable_interaction´ ElementInstanceInput 100003
	Upon mission event "enable_interaction":
		Execute sequence: "interaction_enable" - units/pd2_dlc2/architecture/ext_road_c/str_construction_metal_sheet_interactable/001 (0.0, 0.0, 4.0)
		´point_waypoint_001´
´point_waypoint_001´ ElementWaypoint 100001
	Place waypoint with icon "pd2_generic_look" at position (0.0, 0.0, 25.0)
´func_sequence_trigger_001´ ElementUnitSequenceTrigger 100005
	TRIGGER TIMES 1
	Upon sequence "load" - units/pd2_dlc2/architecture/ext_road_c/str_construction_metal_sheet_interactable/001 (0.0, 0.0, 4.0):
		Send instance event "picked_up" to mission.
		remove ´point_waypoint_001´
