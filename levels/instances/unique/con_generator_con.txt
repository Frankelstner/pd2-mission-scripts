ID range vs continent name:
	100000: world
	200000: editor_only

statics
	100000 units/dev_tools/level_tools/dev_door_blocker_1x1x3/003 (-50.0, -50.0, -25.0)
		disable_on_ai_graph True
	100003 units/payday2/equipment/hlm_interactable_motor_start/001 (0.0, 50.0, 102.5)
	100004 units/pd2_dlc_cro/ind_ext_water_pump_generator/001 (0.0, 50.0, 0.0)
		disable_on_ai_graph True
	200000 units/test/jocke/plane_black_temp/001 (0.0, 100.0, 0.0)
