﻿´Auto_Start´ ElementUnitSequence 100003
	EXECUTE ON STARTUP
	Execute sequence: "hide" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/001 (23.0, -39.0, 8.0)
	Execute sequence: "hide" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/002 (-12.1005, -40.0, 23.5564)
´money_used´ ElementUnitSequenceTrigger 100012
	TRIGGER TIMES 2
	Upon any sequence:
		"load" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/001 (23.0, -39.0, 8.0)
		"load" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/002 (-12.1005, -40.0, 23.5564)
	Execute:
		Send instance event "money_grabbed" to mission.
		Debug message: money_taken
		´2（2）´
´2（2）´ ElementCounter 100043
	BASE DELAY  (DELAY 1)
	When counter target reached: 
		Execute sequence: "anim_close_doors" - units/pd2_dlc_fish/props/lxy_prop_bar_kitchen_unit_b/001 (-50.0, -3.05176e-05, 0.0)
´disable_money´ ElementInstanceInput 100002
	Upon mission event "disable_money":
		´link_3´
´link_3´ MissionScriptElement 100019
	´OFF´
´OFF´ ElementToggle 100020
	Toggle off: ´Open_Bag´
	Toggle off: ´link_4´
	Toggle off: ´show´
	Toggle off: ´Off´
	Toggle off: ´output_money_found´
	Toggle off: ´OFF_2´
	Toggle off: ´picked_money´
´Open_Bag´ MissionScriptElement 100004
	´show´
´link_4´ MissionScriptElement 100047
	BASE DELAY  (DELAY 0-0.5)
	TRIGGER TIMES 1
	´OFF_2´
´show´ ElementUnitSequence 100010
	Execute sequence: "show" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/001 (23.0, -39.0, 8.0)
	Execute sequence: "enable_interaction" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/001 (23.0, -39.0, 8.0)
	Execute sequence: "show" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/002 (-12.1005, -40.0, 23.5564)
	Execute sequence: "enable_interaction" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/002 (-12.1005, -40.0, 23.5564)
	´Off´
´Off´ ElementToggle 100011
	Toggle off: ´cart_opened´
	´output_money_found´
´cart_opened´ ElementUnitSequenceTrigger 100022
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_fish/props/lxy_prop_bar_kitchen_unit_b/001 (-50.0, -3.05176e-05, 0.0):
		Execute sequence: "hide" - units/pd2_dlc_fish/lxy_prop_auction_tag/001 (-1.0, -65.0, 70.0)
		Execute sequence: "hide" - units/pd2_dlc_fish/lxy_prop_auction_tag/002 (23.0, -65.0, 90.0)
		Execute sequence: "hide" - units/pd2_dlc_fish/lxy_prop_auction_tag/003 (-25.0, -66.0, 89.0)
		´Open_Bag´
		´2_sec´
´output_money_found´ ElementInstanceOutput 100014
	Send instance event "money_found" to mission.
	Debug message: money_found
´OFF_2´ ElementToggle 100001
	Toggle off: ´disable_money´
	Toggle off: ´link_3´
	Toggle off: ´OFF´
	Toggle off: ´2_sec´
	´picked_money´
´2_sec´ MissionScriptElement 100049
	BASE DELAY  (DELAY 2)
	Execute sequence: "anim_close_doors" - units/pd2_dlc_fish/props/lxy_prop_bar_kitchen_unit_b/001 (-50.0, -3.05176e-05, 0.0)
´picked_money´ ElementInstanceOutput 100040
	Send instance event "picked_money" to mission.
´enable´ ElementInstanceInput 100009
	Upon mission event "enable":
		Execute sequence: "enable_interaction" - units/pd2_dlc_fish/props/lxy_prop_bar_kitchen_unit_b/001 (-50.0, -3.05176e-05, 0.0)
´Pick_Random_Color´ ElementInstanceInput 100026
	Upon mission event "Pick_Random_Color":
		If HeistPersistentTable.global_var_color == 3:
			Execute sequence: "state_tag_red" - units/pd2_dlc_fish/lxy_prop_auction_tag/001 (-1.0, -65.0, 70.0)
			Execute sequence: "state_tag_red" - units/pd2_dlc_fish/lxy_prop_auction_tag/002 (23.0, -65.0, 90.0)
			Execute sequence: "state_tag_red" - units/pd2_dlc_fish/lxy_prop_auction_tag/003 (-25.0, -66.0, 89.0)
			´link_4´
			´pick_tag´
		If HeistPersistentTable.global_var_color == 2:
			Execute sequence: "state_tag_green" - units/pd2_dlc_fish/lxy_prop_auction_tag/001 (-1.0, -65.0, 70.0)
			Execute sequence: "state_tag_green" - units/pd2_dlc_fish/lxy_prop_auction_tag/002 (23.0, -65.0, 90.0)
			Execute sequence: "state_tag_green" - units/pd2_dlc_fish/lxy_prop_auction_tag/003 (-25.0, -66.0, 89.0)
			´link_4´
			´pick_tag´
		If HeistPersistentTable.global_var_color == 4:
			Execute sequence: "state_tag_white" - units/pd2_dlc_fish/lxy_prop_auction_tag/001 (-1.0, -65.0, 70.0)
			Execute sequence: "state_tag_white" - units/pd2_dlc_fish/lxy_prop_auction_tag/002 (23.0, -65.0, 90.0)
			Execute sequence: "state_tag_white" - units/pd2_dlc_fish/lxy_prop_auction_tag/003 (-25.0, -66.0, 89.0)
			´link_4´
			´pick_tag´
		If HeistPersistentTable.global_var_color == 1:
			Execute sequence: "state_tag_blue" - units/pd2_dlc_fish/lxy_prop_auction_tag/001 (-1.0, -65.0, 70.0)
			Execute sequence: "state_tag_blue" - units/pd2_dlc_fish/lxy_prop_auction_tag/002 (23.0, -65.0, 90.0)
			Execute sequence: "state_tag_blue" - units/pd2_dlc_fish/lxy_prop_auction_tag/003 (-25.0, -66.0, 89.0)
			´link_4´
			´pick_tag´
		If HeistPersistentTable.global_var_color == 5:
			Execute sequence: "state_tag_yellow" - units/pd2_dlc_fish/lxy_prop_auction_tag/001 (-1.0, -65.0, 70.0)
			Execute sequence: "state_tag_yellow" - units/pd2_dlc_fish/lxy_prop_auction_tag/002 (23.0, -65.0, 90.0)
			Execute sequence: "state_tag_yellow" - units/pd2_dlc_fish/lxy_prop_auction_tag/003 (-25.0, -66.0, 89.0)
			´link_4´
			´pick_tag´
´pick_tag´ ElementRandom 100048
	TRIGGER TIMES 1
	Execute 2:
		´hide_2´
		´hide_3´
		´hide_4´
´hide_2´ ElementUnitSequence 100050
	Execute sequence: "hide" - units/pd2_dlc_fish/lxy_prop_auction_tag/001 (-1.0, -65.0, 70.0)
´hide_3´ ElementUnitSequence 100053
	Execute sequence: "hide" - units/pd2_dlc_fish/lxy_prop_auction_tag/002 (23.0, -65.0, 90.0)
´hide_4´ ElementUnitSequence 100057
	Execute sequence: "hide" - units/pd2_dlc_fish/lxy_prop_auction_tag/003 (-25.0, -66.0, 89.0)
