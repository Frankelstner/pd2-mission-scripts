﻿´Auto_Start´ ElementUnitSequence 100003
	EXECUTE ON STARTUP
	Execute sequence: "hide" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/001 (29.0, 1.99998, 34.0)
	Execute sequence: "hide" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/002 (-21.0, 0.999985, 34.0)
´money_used´ ElementUnitSequenceTrigger 100012
	TRIGGER TIMES 2
	Upon any sequence:
		"load" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/001 (29.0, 1.99998, 34.0)
		"load" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/002 (-21.0, 0.999985, 34.0)
	Execute:
		Send instance event "money_grabbed" to mission.
		Debug message: money_taken
		´2（2）´
´2（2）´ ElementCounter 100049
	BASE DELAY  (DELAY 1)
	When counter target reached: 
		Execute sequence: "boat_capsule_close" - units/pd2_dlc_fish/boat_capsule_01/lxy_prop_life_boat_capsule/001 (0.0, 0.0, 0.0)
´disable_money´ ElementInstanceInput 100002
	Upon mission event "disable_money":
		´link_3´
´link_3´ MissionScriptElement 100019
	´OFF´
´OFF´ ElementToggle 100020
	Toggle off: ´Open_Bag´
	Toggle off: ´link_4´
	Toggle off: ´show´
	Toggle off: ´Off´
	Toggle off: ´output_money_found´
	Toggle off: ´Off_2´
	Toggle off: ´picked_money´
´Open_Bag´ MissionScriptElement 100004
	´show´
´link_4´ MissionScriptElement 100047
	TRIGGER TIMES 1
	´Off_2´
´show´ ElementUnitSequence 100010
	Execute sequence: "hide_boat" - units/pd2_dlc_fish/boat_capsule_01/lxy_prop_life_boat_capsule/001 (0.0, 0.0, 0.0)
	Execute sequence: "show_and_enable_interaction" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/001 (29.0, 1.99998, 34.0)
	Execute sequence: "show_and_enable_interaction" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/002 (-21.0, 0.999985, 34.0)
	´Off´
´Off´ ElementToggle 100011
	Toggle off: ´cart_opened´
	´output_money_found´
´cart_opened´ ElementUnitSequenceTrigger 100022
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_fish/boat_capsule_01/lxy_prop_life_boat_capsule/001 (0.0, 0.0, 0.0):
		Execute sequence: "hide" - units/pd2_dlc_fish/lxy_prop_auction_tag/001 (1.0, -34.0, 51.0)
		Execute sequence: "hide" - units/pd2_dlc_fish/lxy_prop_auction_tag/002 (6.0, -34.0, 51.0)
		Execute sequence: "hide" - units/pd2_dlc_fish/lxy_prop_auction_tag/003 (-3.00761, -34.0, 51.8257)
		´Open_Bag´
		´2_sec´
´output_money_found´ ElementInstanceOutput 100014
	Send instance event "money_found" to mission.
	Debug message: money_found
´Off_2´ ElementToggle 100001
	Toggle off: ´disable_money´
	Toggle off: ´link_3´
	Toggle off: ´OFF´
	Toggle off: ´2_sec´
	´picked_money´
´2_sec´ MissionScriptElement 100028
	BASE DELAY  (DELAY 2)
	Execute sequence: "boat_capsule_close" - units/pd2_dlc_fish/boat_capsule_01/lxy_prop_life_boat_capsule/001 (0.0, 0.0, 0.0)
´picked_money´ ElementInstanceOutput 100040
	Send instance event "picked_money" to mission.
´enable´ ElementInstanceInput 100009
	Upon mission event "enable":
		Execute sequence: "enable_interaction" - units/pd2_dlc_fish/boat_capsule_01/lxy_prop_life_boat_capsule/001 (0.0, 0.0, 0.0)
´Pick_Random_Color´ ElementInstanceInput 100026
	Upon mission event "Pick_Random_Color":
		If HeistPersistentTable.global_var_color == 3:
			Execute sequence: "state_tag_red" - units/pd2_dlc_fish/lxy_prop_auction_tag/001 (1.0, -34.0, 51.0)
			Execute sequence: "state_tag_red" - units/pd2_dlc_fish/lxy_prop_auction_tag/002 (6.0, -34.0, 51.0)
			Execute sequence: "state_tag_red" - units/pd2_dlc_fish/lxy_prop_auction_tag/003 (-3.00761, -34.0, 51.8257)
			´link_4´
			´pick_tag´
		If HeistPersistentTable.global_var_color == 2:
			Execute sequence: "state_tag_green" - units/pd2_dlc_fish/lxy_prop_auction_tag/001 (1.0, -34.0, 51.0)
			Execute sequence: "state_tag_green" - units/pd2_dlc_fish/lxy_prop_auction_tag/002 (6.0, -34.0, 51.0)
			Execute sequence: "state_tag_green" - units/pd2_dlc_fish/lxy_prop_auction_tag/003 (-3.00761, -34.0, 51.8257)
			´link_4´
			´pick_tag´
		If HeistPersistentTable.global_var_color == 4:
			Execute sequence: "state_tag_white" - units/pd2_dlc_fish/lxy_prop_auction_tag/001 (1.0, -34.0, 51.0)
			Execute sequence: "state_tag_white" - units/pd2_dlc_fish/lxy_prop_auction_tag/002 (6.0, -34.0, 51.0)
			Execute sequence: "state_tag_white" - units/pd2_dlc_fish/lxy_prop_auction_tag/003 (-3.00761, -34.0, 51.8257)
			´link_4´
			´pick_tag´
		If HeistPersistentTable.global_var_color == 1:
			Execute sequence: "state_tag_blue" - units/pd2_dlc_fish/lxy_prop_auction_tag/001 (1.0, -34.0, 51.0)
			Execute sequence: "state_tag_blue" - units/pd2_dlc_fish/lxy_prop_auction_tag/002 (6.0, -34.0, 51.0)
			Execute sequence: "state_tag_blue" - units/pd2_dlc_fish/lxy_prop_auction_tag/003 (-3.00761, -34.0, 51.8257)
			´link_4´
			´pick_tag´
		If HeistPersistentTable.global_var_color == 5:
			Execute sequence: "state_tag_yellow" - units/pd2_dlc_fish/lxy_prop_auction_tag/001 (1.0, -34.0, 51.0)
			Execute sequence: "state_tag_yellow" - units/pd2_dlc_fish/lxy_prop_auction_tag/002 (6.0, -34.0, 51.0)
			Execute sequence: "state_tag_yellow" - units/pd2_dlc_fish/lxy_prop_auction_tag/003 (-3.00761, -34.0, 51.8257)
			´link_4´
			´pick_tag´
´pick_tag´ ElementRandom 100048
	TRIGGER TIMES 1
	Execute 2:
		´hide_2´
		´hide_3´
		´hide_4´
´hide_2´ ElementUnitSequence 100050
	Execute sequence: "hide" - units/pd2_dlc_fish/lxy_prop_auction_tag/001 (1.0, -34.0, 51.0)
´hide_3´ ElementUnitSequence 100053
	Execute sequence: "hide" - units/pd2_dlc_fish/lxy_prop_auction_tag/002 (6.0, -34.0, 51.0)
´hide_4´ ElementUnitSequence 100057
	Execute sequence: "hide" - units/pd2_dlc_fish/lxy_prop_auction_tag/003 (-3.00761, -34.0, 51.8257)
