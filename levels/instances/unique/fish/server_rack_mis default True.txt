﻿´Auto_Start´ ElementUnitSequence 100018
	EXECUTE ON STARTUP
	Execute sequence: "state_interaction_disable" - units/pd2_dlc_fish/props/lxy_int_prop_button/001 (-86.3056, -86.5862, 88.0)
	Execute sequence: "state_interaction_disable" - units/pd2_dlc_fish/props/lxy_int_prop_button/002 (85.4446, -87.2669, 95.0)
	Execute sequence: "state_interaction_disable" - units/pd2_dlc_fish/props/lxy_int_prop_button/003 (86.7932, 85.9916, 88.0)
	Execute sequence: "state_interaction_disable" - units/pd2_dlc_fish/props/lxy_int_prop_button/004 (-84.5486, 87.0452, 93.0)
	Execute sequence: "state_outline_disabled" - units/pd2_dlc_fish/props/lxy_int_prop_button_outline/001 (-86.3056, -86.5862, 88.0)
	Execute sequence: "state_outline_disabled" - units/pd2_dlc_fish/props/lxy_int_prop_button_outline/002 (85.4446, -87.2669, 95.0)
	Execute sequence: "state_outline_disabled" - units/pd2_dlc_fish/props/lxy_int_prop_button_outline/003 (86.7932, 85.9916, 88.0)
	Execute sequence: "state_outline_disabled" - units/pd2_dlc_fish/props/lxy_int_prop_button_outline/004 (-84.5486, 87.0452, 93.0)
´button_used´ ElementUnitSequenceTrigger 100010
	Upon any sequence:
		"interact" - units/pd2_dlc_fish/props/lxy_int_prop_button/001 (-86.3056, -86.5862, 88.0)
		"interact" - units/pd2_dlc_fish/props/lxy_int_prop_button/002 (85.4446, -87.2669, 95.0)
		"interact" - units/pd2_dlc_fish/props/lxy_int_prop_button/003 (86.7932, 85.9916, 88.0)
		"interact" - units/pd2_dlc_fish/props/lxy_int_prop_button/004 (-84.5486, 87.0452, 93.0)
	Execute:
		Toggle off: ´disable_all´
		Toggle off: ´link´
		Toggle off: ´exit´
		Toggle off: ´enter´
		Toggle off: ´1´
		Toggle off: ´1_2´
		Toggle off: ´2´
		Toggle off: ´2_2´
		Toggle off: ´3´
		Toggle off: ´3_2´
		Toggle off: ´4´
		Toggle off: ´4_2´
		Execute sequence: "play_sound_button_pushed" - units/pd2_dlc_fish/architecture/lxy_server_racks_stand/001 (0.0, 0.0, 0.0)
		Execute sequence: "state_outline_disabled" - units/pd2_dlc_fish/props/lxy_int_prop_button_outline/001 (-86.3056, -86.5862, 88.0)
		Execute sequence: "state_outline_disabled" - units/pd2_dlc_fish/props/lxy_int_prop_button_outline/002 (85.4446, -87.2669, 95.0)
		Execute sequence: "state_outline_disabled" - units/pd2_dlc_fish/props/lxy_int_prop_button_outline/003 (86.7932, 85.9916, 88.0)
		Execute sequence: "state_outline_disabled" - units/pd2_dlc_fish/props/lxy_int_prop_button_outline/004 (-84.5486, 87.0452, 93.0)
		Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/002 (-108.345, -65.9187, 106.0)
		Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/003 (68.0688, -106.931, 121.0)
		Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/004 (107.546, 65.5334, 105.0)
		Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/005 (-67.9899, 106.424, 121.0)
		Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/006 (65.1191, 107.96, 106.0)
		Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/007 (106.96, -68.04, 121.0)
		Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/008 (-65.9189, -108.345, 106.0)
		Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/009 (-106.881, 67.5334, 121.0)
		´counter（0）´ = 0
		remove ´click´
		Send instance event "button_used" to mission.
		Debug message: Button_used
´disable_all´ ElementUnitSequence 100050
	Execute sequence: "state_interaction_disable" - units/pd2_dlc_fish/props/lxy_int_prop_button/001 (-86.3056, -86.5862, 88.0)
	Execute sequence: "state_interaction_disable" - units/pd2_dlc_fish/props/lxy_int_prop_button/002 (85.4446, -87.2669, 95.0)
	Execute sequence: "state_interaction_disable" - units/pd2_dlc_fish/props/lxy_int_prop_button/003 (86.7932, 85.9916, 88.0)
	Execute sequence: "state_interaction_disable" - units/pd2_dlc_fish/props/lxy_int_prop_button/004 (-84.5486, 87.0452, 93.0)
´link´ MissionScriptElement 100029
	´link_2´
´exit´ ElementAreaReportTrigger 100028
	DISABLED
	Use shape: ´point_shape_001´ (0.0, 0.0, 125.0)
	Amount: all
	Instigator: player
	Interval: 0.1
	Upon trigger, execute:
		´disable_all´ (ALTERNATIVE empty)
´enter´ ElementAreaReportTrigger 100026
	DISABLED
	Use shape: ´point_shape_001´ (0.0, 0.0, 125.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon trigger, execute:
		´link´ (ALTERNATIVE enter)
´1´ ElementCounterFilter 100034
	If ´counter（0）´ == 1:
		´reshow_waypoint´
		´1_2´ (DELAY 0.1)
´counter（0）´ ElementCounter 100031
´1_2´ ElementUnitSequence 100011
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_fish/props/lxy_int_prop_button/001 (-86.3056, -86.5862, 88.0)
	Execute sequence: "state_outline_enabled" - units/pd2_dlc_fish/props/lxy_int_prop_button_outline/001 (-86.3056, -86.5862, 88.0)
	Execute sequence: "state_blinking_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/002 (-108.345, -65.9187, 106.0)
	Execute sequence: "state_blinking_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/008 (-65.9189, -108.345, 106.0)
	Toggle off: ´2´
	Toggle off: ´2_2´
	Toggle off: ´3´
	Toggle off: ´3_2´
	Toggle off: ´4´
	Toggle off: ´4_2´
´2´ ElementUnitSequence 100013
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_fish/props/lxy_int_prop_button/002 (85.4446, -87.2669, 95.0)
	Execute sequence: "state_outline_enabled" - units/pd2_dlc_fish/props/lxy_int_prop_button_outline/002 (85.4446, -87.2669, 95.0)
	Execute sequence: "state_blinking_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/003 (68.0688, -106.931, 121.0)
	Execute sequence: "state_blinking_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/007 (106.96, -68.04, 121.0)
	Toggle off: ´1_2´
	Toggle off: ´1´
	Toggle off: ´3´
	Toggle off: ´3_2´
	Toggle off: ´4´
	Toggle off: ´4_2´
´2_2´ ElementCounterFilter 100036
	If ´counter（0）´ == 2:
		´reshow_waypoint´
		´2´ (DELAY 0.1)
´3´ ElementCounterFilter 100038
	If ´counter（0）´ == 3:
		´reshow_waypoint´
		´3_2´ (DELAY 0.1)
´3_2´ ElementUnitSequence 100015
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_fish/props/lxy_int_prop_button/003 (86.7932, 85.9916, 88.0)
	Execute sequence: "state_outline_enabled" - units/pd2_dlc_fish/props/lxy_int_prop_button_outline/003 (86.7932, 85.9916, 88.0)
	Execute sequence: "state_blinking_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/004 (107.546, 65.5334, 105.0)
	Execute sequence: "state_blinking_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/006 (65.1191, 107.96, 106.0)
	Toggle off: ´2´
	Toggle off: ´2_2´
	Toggle off: ´1´
	Toggle off: ´1_2´
	Toggle off: ´4´
	Toggle off: ´4_2´
´4´ ElementUnitSequence 100052
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_fish/props/lxy_int_prop_button/004 (-84.5486, 87.0452, 93.0)
	Execute sequence: "state_outline_enabled" - units/pd2_dlc_fish/props/lxy_int_prop_button_outline/004 (-84.5486, 87.0452, 93.0)
	Execute sequence: "state_blinking_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/005 (-67.9899, 106.424, 121.0)
	Execute sequence: "state_blinking_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/009 (-106.881, 67.5334, 121.0)
	Toggle off: ´3_2´
	Toggle off: ´3´
	Toggle off: ´2_2´
	Toggle off: ´2´
	Toggle off: ´1_2´
	Toggle off: ´1´
´4_2´ ElementCounterFilter 100039
	If ´counter（0）´ == 4:
		´reshow_waypoint´
		´4´ (DELAY 0.1)
´click´ ElementWaypoint 100000
	Place waypoint with icon "pd2_generic_interact" at position (0.0, 0.0, 100.0)
´link_2´ MissionScriptElement 100047
	´1´
	´2_2´
	´3´
	´4_2´
´point_shape_001´ ElementShape 100061
	Cylinder, radius 288.804992676, height 275, at position (0.0, 0.0, 125.0) with rotation (0.0, 0.0, 0.0, -1.0)
´reshow_waypoint´ MissionScriptElement 100099
	TRIGGER TIMES 1
	Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/002 (-108.345, -65.9187, 106.0)
	Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/003 (68.0688, -106.931, 121.0)
	Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/004 (107.546, 65.5334, 105.0)
	Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/005 (-67.9899, 106.424, 121.0)
	Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/006 (65.1191, 107.96, 106.0)
	Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/007 (106.96, -68.04, 121.0)
	Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/008 (-65.9189, -108.345, 106.0)
	Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/009 (-106.881, 67.5334, 121.0)
	´click´
´pick_button´ ElementInstanceInput 100021
	Upon mission event "Pick_Button":
		´show_waypoint´
´show_waypoint´ MissionScriptElement 100001
	TRIGGER TIMES 1
	Toggle on: ´link´
	Toggle on: ´disable_all´
	Toggle on: ´enter´
	Toggle on: ´exit´
	Execute 1:
		´1_3´
		´2_3´
		´3_3´
		´4_3´
´1_3´ ElementCounterOperator 100043
	´counter（0）´ = 1
	´link_2´ (DELAY 0.25)
´2_3´ ElementCounterOperator 100044
	´counter（0）´ = 2
	´link_2´ (DELAY 0.25)
´3_3´ ElementCounterOperator 100045
	´counter（0）´ = 3
	´link_2´ (DELAY 0.25)
´4_3´ ElementCounterOperator 100046
	´counter（0）´ = 4
	´link_2´ (DELAY 0.25)
´start_overheat´ ElementInstanceInput 100005
	Upon mission event "start_overheat":
		´link_3´
´link_3´ MissionScriptElement 100007
	´timer（0）´ += 1
	´1_sec_loop´
	´play_sound´
´timer（0）´ ElementTimer 100012
	Timer: 0
	Digital GUI units:
		units/pd2_dlc_chill/props/chl_prop_scorer/001 (-119.0, 3.05176e-05, -71.193)
		units/pd2_dlc_chill/props/chl_prop_scorer/002 (119.0, 0.000499725, -71.193)
		units/pd2_dlc_chill/props/chl_prop_scorer/003 (-3.05176e-05, -119.0, -71.193)
		units/pd2_dlc_chill/props/chl_prop_scorer/004 (6.10352e-05, 119.0, -71.193)
		units/pd2_dlc_chill/props/chl_prop_scorer/005 (118.0, 0.000495911, 231.807)
		units/pd2_dlc_chill/props/chl_prop_scorer/006 (-1.52588e-05, -119.0, 231.807)
		units/pd2_dlc_chill/props/chl_prop_scorer/007 (-118.0, 3.43323e-05, 231.807)
		units/pd2_dlc_chill/props/chl_prop_scorer/008 (7.62939e-05, 119.0, 231.807)
´1_sec_loop´ MissionScriptElement 100040
	´10（10）´
	´50（50）´
	´30（30）´
	´20（20）´
	´link_3´ (DELAY 0.35)
´play_sound´ ElementUnitSequence 100004
	TRIGGER TIMES 1
	Execute sequence: "play_sound_cooling_system_off" - units/pd2_dlc_fish/architecture/lxy_server_racks_stand/001 (0.0, 0.0, 0.0)
´10（10）´ ElementCounter 100042
	When counter target reached: 
		Execute sequence: "black_on_light_yellow" - units/pd2_dlc_chill/props/chl_prop_scorer/001 (-119.0, 3.05176e-05, -71.193)
		Execute sequence: "black_on_light_yellow" - units/pd2_dlc_chill/props/chl_prop_scorer/002 (119.0, 0.000499725, -71.193)
		Execute sequence: "black_on_light_yellow" - units/pd2_dlc_chill/props/chl_prop_scorer/003 (-3.05176e-05, -119.0, -71.193)
		Execute sequence: "black_on_light_yellow" - units/pd2_dlc_chill/props/chl_prop_scorer/004 (6.10352e-05, 119.0, -71.193)
		Execute sequence: "black_on_light_yellow" - units/pd2_dlc_chill/props/chl_prop_scorer/005 (118.0, 0.000495911, 231.807)
		Execute sequence: "black_on_light_yellow" - units/pd2_dlc_chill/props/chl_prop_scorer/006 (-1.52588e-05, -119.0, 231.807)
		Execute sequence: "black_on_light_yellow" - units/pd2_dlc_chill/props/chl_prop_scorer/007 (-118.0, 3.43323e-05, 231.807)
		Execute sequence: "black_on_light_yellow" - units/pd2_dlc_chill/props/chl_prop_scorer/008 (7.62939e-05, 119.0, 231.807)
´50（50）´ ElementCounter 100009
	When counter target reached: 
		Toggle off: ´1_sec_loop´
		Toggle off: ´link_3´
´30（30）´ ElementCounter 100016
	When counter target reached: 
		Execute sequence: "black_on_light_red" - units/pd2_dlc_chill/props/chl_prop_scorer/001 (-119.0, 3.05176e-05, -71.193)
		Execute sequence: "black_on_light_red" - units/pd2_dlc_chill/props/chl_prop_scorer/002 (119.0, 0.000499725, -71.193)
		Execute sequence: "black_on_light_red" - units/pd2_dlc_chill/props/chl_prop_scorer/003 (-3.05176e-05, -119.0, -71.193)
		Execute sequence: "black_on_light_red" - units/pd2_dlc_chill/props/chl_prop_scorer/004 (6.10352e-05, 119.0, -71.193)
		Execute sequence: "black_on_light_red" - units/pd2_dlc_chill/props/chl_prop_scorer/005 (118.0, 0.000495911, 231.807)
		Execute sequence: "black_on_light_red" - units/pd2_dlc_chill/props/chl_prop_scorer/006 (-1.52588e-05, -119.0, 231.807)
		Execute sequence: "black_on_light_red" - units/pd2_dlc_chill/props/chl_prop_scorer/007 (-118.0, 3.43323e-05, 231.807)
		Execute sequence: "black_on_light_red" - units/pd2_dlc_chill/props/chl_prop_scorer/008 (7.62939e-05, 119.0, 231.807)
		Execute sequence: "anim_open" - units/pd2_dlc_fish/props/lxy_prop_server_01/002 (0.0, -117.0, 14.0)
		Execute sequence: "anim_open" - units/pd2_dlc_fish/props/lxy_prop_server_01/004 (117.166, 0.104187, 14.0)
		Execute sequence: "anim_open" - units/pd2_dlc_fish/props/lxy_prop_server_01/006 (-0.829132, 116.744, 14.0)
		Execute sequence: "anim_open" - units/pd2_dlc_fish/props/lxy_prop_server_01/008 (-116.791, -0.573799, 14.0)
		Send instance event "overheated" to mission.
		Play effect effects/payday2/particles/smoke_trail/smoke_light_orange_color at position (0.0, 0.0, -161.193)
´20（20）´ ElementCounter 100059
	When counter target reached: 
		Execute sequence: "black_on_light_orange" - units/pd2_dlc_chill/props/chl_prop_scorer/001 (-119.0, 3.05176e-05, -71.193)
		Execute sequence: "black_on_light_orange" - units/pd2_dlc_chill/props/chl_prop_scorer/002 (119.0, 0.000499725, -71.193)
		Execute sequence: "black_on_light_orange" - units/pd2_dlc_chill/props/chl_prop_scorer/003 (-3.05176e-05, -119.0, -71.193)
		Execute sequence: "black_on_light_orange" - units/pd2_dlc_chill/props/chl_prop_scorer/004 (6.10352e-05, 119.0, -71.193)
		Execute sequence: "black_on_light_orange" - units/pd2_dlc_chill/props/chl_prop_scorer/005 (118.0, 0.000495911, 231.807)
		Execute sequence: "black_on_light_orange" - units/pd2_dlc_chill/props/chl_prop_scorer/006 (-1.52588e-05, -119.0, 231.807)
		Execute sequence: "black_on_light_orange" - units/pd2_dlc_chill/props/chl_prop_scorer/007 (-118.0, 3.43323e-05, 231.807)
		Execute sequence: "black_on_light_orange" - units/pd2_dlc_chill/props/chl_prop_scorer/008 (7.62939e-05, 119.0, 231.807)
´ecm_on´ ElementGlobalEventTrigger 100064
	Upon global event "ecm_jammer_on":
		Toggle off: ´link_2´
		Toggle off: ´exit´
		Toggle off: ´enter´
		Toggle off: ´link_5´
		Execute sequence: "state_blinking_blue" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/002 (-108.345, -65.9187, 106.0)
		Execute sequence: "state_blinking_blue" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/003 (68.0688, -106.931, 121.0)
		Execute sequence: "state_blinking_blue" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/004 (107.546, 65.5334, 105.0)
		Execute sequence: "state_blinking_blue" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/005 (-67.9899, 106.424, 121.0)
		Execute sequence: "state_blinking_blue" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/006 (65.1191, 107.96, 106.0)
		Execute sequence: "state_blinking_blue" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/007 (106.96, -68.04, 121.0)
		Execute sequence: "state_blinking_blue" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/008 (-65.9189, -108.345, 106.0)
		Execute sequence: "state_blinking_blue" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/009 (-106.881, 67.5334, 121.0)
		Execute sequence: "state_outline_disabled" - units/pd2_dlc_fish/props/lxy_int_prop_button_outline/001 (-86.3056, -86.5862, 88.0)
		Execute sequence: "state_outline_disabled" - units/pd2_dlc_fish/props/lxy_int_prop_button_outline/002 (85.4446, -87.2669, 95.0)
		Execute sequence: "state_outline_disabled" - units/pd2_dlc_fish/props/lxy_int_prop_button_outline/003 (86.7932, 85.9916, 88.0)
		Execute sequence: "state_outline_disabled" - units/pd2_dlc_fish/props/lxy_int_prop_button_outline/004 (-84.5486, 87.0452, 93.0)
		remove ´click´
		´disable_all´
´link_5´ MissionScriptElement 100114
	Toggle on: ´link_2´
	Toggle on: ´exit´
	Toggle on: ´enter´
	Toggle on, trigger times 1: ´reshow_waypoint´
	Debug message: ecm_off
	If ´counter（0）´ == 0:
		Execute sequence: "state_static_blue" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/002 (-108.345, -65.9187, 106.0) (DELAY 0.1)
		Execute sequence: "state_static_blue" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/003 (68.0688, -106.931, 121.0) (DELAY 0.1)
		Execute sequence: "state_static_blue" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/004 (107.546, 65.5334, 105.0) (DELAY 0.1)
		Execute sequence: "state_static_blue" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/005 (-67.9899, 106.424, 121.0) (DELAY 0.1)
		Execute sequence: "state_static_blue" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/006 (65.1191, 107.96, 106.0) (DELAY 0.1)
		Execute sequence: "state_static_blue" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/007 (106.96, -68.04, 121.0) (DELAY 0.1)
		Execute sequence: "state_static_blue" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/008 (-65.9189, -108.345, 106.0) (DELAY 0.1)
		Execute sequence: "state_static_blue" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/009 (-106.881, 67.5334, 121.0) (DELAY 0.1)
	If ´counter（0）´ > 0:
		´link_2´
		Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/002 (-108.345, -65.9187, 106.0) (DELAY 0.1)
		Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/003 (68.0688, -106.931, 121.0) (DELAY 0.1)
		Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/004 (107.546, 65.5334, 105.0) (DELAY 0.1)
		Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/005 (-67.9899, 106.424, 121.0) (DELAY 0.1)
		Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/006 (65.1191, 107.96, 106.0) (DELAY 0.1)
		Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/007 (106.96, -68.04, 121.0) (DELAY 0.1)
		Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/008 (-65.9189, -108.345, 106.0) (DELAY 0.1)
		Execute sequence: "state_static_red" - units/pd2_dlc_fish/props/lxy_prop_glow_01_blink/009 (-106.881, 67.5334, 121.0) (DELAY 0.1)
´ecm_off_2´ ElementGlobalEventTrigger 100065
	Upon global event "ecm_jammer_off":
		Toggle on: ´link_5´
		´link_5´ (DELAY 5)
