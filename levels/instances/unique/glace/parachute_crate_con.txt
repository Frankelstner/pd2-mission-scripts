ID range vs continent name:
	100000: world

statics
	100001 units/pd2_dlc1/equipment/ind_interactable_shipping_crate_2x1x1m/001 (0.0, 0.0, 0.0)
	100000 units/pd2_dlc_jerry/pickups/gen_pku_parachute/001 (-3.99998, 65.0, 37.0)
	100002 units/pd2_dlc_jerry/pickups/gen_pku_parachute/002 (-3.99998, 25.0, 37.0)
	100003 units/pd2_dlc_jerry/pickups/gen_pku_parachute/003 (-3.99999, -16.0, 35.0)
	100004 units/pd2_dlc_jerry/pickups/gen_pku_parachute/004 (-4.0, -58.0, 35.0)
