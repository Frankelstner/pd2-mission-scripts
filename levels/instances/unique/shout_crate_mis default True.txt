﻿´startup´ ElementUnitSequence 100014
	EXECUTE ON STARTUP
	Execute sequence: "hide" - units/payday2/pickups/gen_pku_cocaine/001 (2.98023e-06, 0.0, 32.6459)
	Execute sequence: "hide" - units/payday2/equipment/ind_interactable_hardcase_loot_gold/001 (0.0, 0.0, 33.6459)
	Execute sequence: "hide" - units/payday2/equipment/ind_interactable_hardcase_loot_money/001 (0.0, 2.0, 31.6459)
	Execute sequence: "disable_interaction" - units/payday2/pickups/gen_pku_cocaine/001 (2.98023e-06, 0.0, 32.6459)
	Execute sequence: "disable_interaction" - units/payday2/equipment/ind_interactable_hardcase_loot_gold/001 (0.0, 0.0, 33.6459)
	Execute sequence: "disable_interaction" - units/payday2/equipment/ind_interactable_hardcase_loot_money/001 (0.0, 2.0, 31.6459)
	Execute sequence: "state_interaction_disabled" - units/pd2_dlc1/equipment/ind_interactable_shipping_crate_2x1x1m/001 (0.0, 0.0, 0.0)
	Execute sequence: "state_vis_hide" - units/pd2_dlc1/equipment/ind_interactable_shipping_crate_2x1x1m/001 (0.0, 0.0, 0.0)
´once_opened´ ElementUnitSequenceTrigger 100012
	TRIGGER TIMES 1
	Upon sequence "int_seq_open" - units/pd2_dlc1/equipment/ind_interactable_shipping_crate_2x1x1m/001 (0.0, 0.0, 0.0):
		Execute 1:
			´spawn_gold´
			´spawn_coke´
			´spawn_money´
´spawn_gold´ ElementUnitSequence 100008
	Execute sequence: "show" - units/payday2/equipment/ind_interactable_hardcase_loot_gold/001 (0.0, 0.0, 33.6459)
	Execute sequence: "enable_interaction" - units/payday2/equipment/ind_interactable_hardcase_loot_gold/001 (0.0, 0.0, 33.6459)
´spawn_coke´ ElementUnitSequence 100009
	Execute sequence: "show" - units/payday2/pickups/gen_pku_cocaine/001 (2.98023e-06, 0.0, 32.6459)
	Execute sequence: "enable_interaction" - units/payday2/pickups/gen_pku_cocaine/001 (2.98023e-06, 0.0, 32.6459)
´spawn_money´ ElementUnitSequence 100010
	Execute sequence: "show" - units/payday2/equipment/ind_interactable_hardcase_loot_money/001 (0.0, 2.0, 31.6459)
	Execute sequence: "enable_interaction" - units/payday2/equipment/ind_interactable_hardcase_loot_money/001 (0.0, 2.0, 31.6459)
´spawn_crate´ ElementInstanceInput 100016
	Upon mission event "spawn_crate":
		Execute sequence: "state_vis_show" - units/pd2_dlc1/equipment/ind_interactable_shipping_crate_2x1x1m/001 (0.0, 0.0, 0.0)
		Execute sequence: "state_interaction_enabled" - units/pd2_dlc1/equipment/ind_interactable_shipping_crate_2x1x1m/001 (0.0, 0.0, 0.0)
		Execute sequence: "state_requires_crowbar" - units/pd2_dlc1/equipment/ind_interactable_shipping_crate_2x1x1m/001 (0.0, 0.0, 0.0)
