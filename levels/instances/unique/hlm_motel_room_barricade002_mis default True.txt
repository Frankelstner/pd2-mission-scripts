﻿´SO_jump_window001´ ElementSpecialObjective 100042
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Create nav link for None with action "e_nl_cs_jump_window" from (-350.0, 125.0, 0.0) to (-396.469, 407.286, 8.19564e-06)
´SO_jump_window002´ ElementSpecialObjective 100044
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Create nav link for None with action "e_nl_cs_jump_window" from (-350.0, 350.0, 0.0) to (-375.197, -19.1546, 8.19564e-06)
´SO_jump_window003´ ElementSpecialObjective 100050
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Create nav link for None with action "e_nl_cs_jump_window" from (-131.0, -775.0, 0.0) to (-74.0716, -491.045, 8.19564e-06)
´SO_jump_window004´ ElementSpecialObjective 100051
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Create nav link for None with action "e_nl_cs_jump_window" from (-131.0, -550.0, 0.0) to (-62.2209, -874.589, 0.0)
´trigger_special_objective_001´ ElementSpecialObjectiveTrigger 100036
	Upon any special objective event:
		"administered" - ´SO_remove_plank001´
		"administered" - ´SO_remove_plank002´
	Execute:
		Debug message: someone tries to steal ur planks!!!!!!!!!!!
´SO_remove_plank001´ ElementSpecialObjective 100025
	Create objective for enemies in 6000 radius around (-377.406, 2.61312, 8.19564e-06)
	Make chosen person path towards objective and show "e_so_plant_c4_hi".
´SO_remove_plank002´ ElementSpecialObjective 100026
	TRIGGER TIMES 1
	Create objective for enemies in 2000 radius around (-79.2255, -508.913, 8.19564e-06)
	Make chosen person path towards objective and show "e_so_plant_c4_hi".
´completed_removing_planks001´ ElementSpecialObjectiveTrigger 100008
	Upon special objective event "complete" - ´SO_remove_plank001´:
		Execute sequence: "hide" - units/payday2/equipment/hlm_interactable_win_planks/001 (-362.0, 223.0, 190.0)
		Debug message: OH NO YOU DID-ENT!2
		Toggle off: ´SO_remove_plank001´
		remove ´SO_remove_plank001´
´laser_player´ ElementLaserTrigger 100055
	DISABLED
	Dynamic lasers.
	Explosion
	Hide object: units/pd2_dlc2/props/gen_prop_swat_tripmine_shootable/001 (-58.0, 241.0, 45.0)
	Hide object: units/dev_tools/level_tools/door_blocker_1m/001 (-110.0, 227.0, 0.0)
	Toggle on: ´remove_blocker´
	Toggle off: ´enable_lasers´
	Toggle off: ´laser_player´
	´remove_blocker´ (DELAY 1)
´remove_blocker´ ElementNavObstacle 100073
	DISABLED
	Remove nav obstacle: units/dev_tools/level_tools/door_blocker_1m/001 (-110.0, 227.0, 0.0)
´enable_lasers´ ElementToggle 100066
	Toggle on: ´laser_player´
´jumped_through001´ ElementSpecialObjectiveTrigger 100059
	TRIGGER TIMES 1
	Upon any special objective event:
		"anim_act_04" - ´SO_jump_window002´
		"anim_act_04" - ´SO_jump_window001´
	Execute:
		´damage_window´
´damage_window´ ElementUnitSequence 100060
	TRIGGER TIMES 1
	Execute sequence: "destroy" - units/pd2_dlc1/architecture/res_int_motel_window_01/001 (-298.0, 224.0, 73.0)
´trap_explode´ ElementUnitSequenceTrigger 100067
	TRIGGER TIMES 1
	Upon sequence "done_explode" - units/pd2_dlc2/props/gen_prop_swat_tripmine_shootable/001 (-58.0, 241.0, 45.0):
		Explosion
		Hide object: units/pd2_dlc2/props/gen_prop_swat_tripmine_shootable/001 (-58.0, 241.0, 45.0)
		Hide object: units/dev_tools/level_tools/door_blocker_1m/001 (-110.0, 227.0, 0.0)
		Toggle on: ´remove_blocker´
		Toggle off: ´enable_lasers´
		Toggle off: ´laser_player´
		Toggle off: ´start_lasershow´
		´remove_blocker´ (DELAY 1)
´start_lasershow´ MissionScriptElement 100065
	Execute sequence: "anim_close_door" - units/pd2_dlc1/equipment/gen_interactable_door_wooden_c/001 (-49.0, 231.0, 0.0)
	´enable_lasers´ (DELAY 0.1)
´instance_higher_floor´ ElementInstanceInput 100083
	BASE DELAY  (DELAY 1)
	Upon mission event "higher_floor":
		Toggle off: ´SO_jump_window004´
		Toggle off: ´SO_remove_plank002´
		Toggle off: ´SO_jump_window003´
´done_opened´ ElementUnitSequenceTrigger 100092
	DISABLED
	Upon sequence "done_opened" - units/pd2_dlc1/equipment/gen_interactable_door_wooden_c/001 (-49.0, 231.0, 0.0):
		Debug message: done opened
		Toggle off: ´SO_remove_plank001´
		´remove_SO_breach_door´
		Toggle on: ´remove_blocker´ (DELAY 0.5)
		´remove_blocker´ (DELAY 1)
´remove_SO_breach_door´ ElementOperator 100113
	TRIGGER TIMES 1
	remove ´SO_breach_through_door´
	remove ´SO_remove_plank001´
	Toggle off: ´SO_breach_through_door´
´SO_breach_through_door´ ElementSpecialObjective 100110
	DISABLED
	TRIGGER TIMES 1
	Create nav link for None with action "e_nl_kick_enter" from (-125.0, 375.0, 0.0) to (-48.8569, 12.3647, 8.19564e-06)
´door_closed_by_mobster´ ElementUnitSequenceTrigger 100095
	Upon sequence "anim_close_door" - units/pd2_dlc1/equipment/gen_interactable_door_wooden_c/001 (-49.0, 231.0, 0.0):
		Add nav obstacle: units/dev_tools/level_tools/door_blocker_1m/001 (-110.0, 227.0, 0.0)
´area_mobster_inside´ ElementAreaTrigger 100096
	DISABLED
	TRIGGER TIMES 1
	Box, width 571.386, height 500.0, depth 453.826, at position (-125.0, -25.0, 25.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: enemies
	Interval: 0.1
	Rule: ´data_instigator_rule_001´
	Upon while_inside, execute:
		´start_lasershow´
´data_instigator_rule_001´ ElementInstigatorRule 100019
	units/payday2/characters/ene_gang_russian_1/ene_gang_russian_1,units/payday2/characters/ene_gang_russian_2/ene_gang_russian_2,units/payday2/characters/ene_gang_russian_3/ene_gang_russian_3,units/payday2/characters/ene_gang_russian_4/ene_gang_russian_4,units/payday2/characters/ene_gang_russian_5/ene_gang_russian_5
´thug_planks_windows´ ElementInstanceInput 100107
	Upon mission event "thug_planks_windows":
		Execute sequence: "show_gfx_only" - units/payday2/equipment/hlm_interactable_win_planks/001 (-362.0, 223.0, 190.0)
´instigator_input´ ElementInstanceInput 100104
	Upon mission event "player_spawn":
		Hide object: units/pd2_dlc2/props/gen_prop_swat_tripmine_shootable/001 (-58.0, 241.0, 45.0)
		Execute 1: (DELAY 1)
			´open_door´
			´closed_door´
´open_door´ ElementUnitSequence 100079
	Execute sequence: "state_door_open" - units/pd2_dlc1/equipment/gen_interactable_door_wooden_c/001 (-49.0, 231.0, 0.0)
	Show object: units/pd2_dlc2/props/gen_prop_swat_tripmine_shootable/001 (-58.0, 241.0, 45.0)
	Toggle on: ´area_mobster_inside´
´closed_door´ ElementUnitSequence 100080
	Execute sequence: "state_door_close" - units/pd2_dlc1/equipment/gen_interactable_door_wooden_c/001 (-49.0, 231.0, 0.0)
	remove ´laser_player´
	Toggle on: ´SO_breach_through_door´
	Toggle on: ´remove_blocker´
	Add nav obstacle: units/dev_tools/level_tools/door_blocker_1m/001 (-110.0, 227.0, 0.0)
	´SO_breach_through_door´
	Toggle on: ´done_opened´ (DELAY 3)
´started_breaching´ ElementSpecialObjectiveTrigger 100114
	TRIGGER TIMES 1
	Upon special objective event "anim_act_04" - ´SO_breach_through_door´:
		Execute sequence: "open_door" - units/pd2_dlc1/equipment/gen_interactable_door_wooden_c/001 (-49.0, 231.0, 0.0)
		´remove_SO_breach_door´
´func_sequence_trigger_001´ ElementUnitSequenceTrigger 100001
	Upon any sequence:
		"show_planks" - units/payday2/equipment/hlm_interactable_win_planks/001 (-362.0, 223.0, 190.0)
		"show_gfx_only" - units/payday2/equipment/hlm_interactable_win_planks/001 (-362.0, 223.0, 190.0)
	Execute:
		Debug message: planks 1 planted
		remove ´SO_jump_window001´
		remove ´SO_jump_window002´
´func_sequence_trigger_002´ ElementUnitSequenceTrigger 100002
	Upon any sequence:
		"show_planks" - units/payday2/equipment/hlm_interactable_win_planks/002 (-133.0, -655.0, 188.0)
		"show_gfx_only" - units/payday2/equipment/hlm_interactable_win_planks/002 (-133.0, -655.0, 188.0)
	Execute:
		Debug message: planks 2 planted
		remove ´SO_jump_window004´
		remove ´SO_jump_window003´
´func_sequence_trigger_003´ ElementUnitSequenceTrigger 100009
	Upon sequence "destroy_planks" - units/payday2/equipment/hlm_interactable_win_planks/001 (-362.0, 223.0, 190.0):
		Debug message: planks destroyed 1
		´SO_jump_window001´ (DELAY 1)
		´SO_jump_window002´ (DELAY 1)
´func_sequence_trigger_004´ ElementUnitSequenceTrigger 100010
	Upon sequence "destroy_planks" - units/payday2/equipment/hlm_interactable_win_planks/002 (-133.0, -655.0, 188.0):
		Debug message: planks destroyed 2
		Toggle on: ´SO_jump_window004´
		Toggle on: ´SO_jump_window003´
		´SO_jump_window003´ (DELAY 1)
		´SO_jump_window004´ (DELAY 1)
´trigger_special_objective_002´ ElementSpecialObjectiveTrigger 100015
	Upon any special objective event:
		"anim_act_04" - ´SO_jump_window003´
		"anim_act_04" - ´SO_jump_window004´
	Execute:
		´func_sequence_001´
´func_sequence_001´ ElementUnitSequence 100017
	Execute sequence: "destroy" - units/pd2_dlc1/architecture/res_int_motel_window_01/002 (-200.999, -656.0, 74.0003)
´func_sequence_trigger_005´ ElementUnitSequenceTrigger 100018
	TRIGGER TIMES 1
	Upon any sequence:
		"damage" - units/pd2_dlc1/architecture/res_int_motel_window_01/002 (-200.999, -656.0, 74.0003)
		"destroy" - units/pd2_dlc1/architecture/res_int_motel_window_01/002 (-200.999, -656.0, 74.0003)
	Execute:
		Toggle off: ´func_sequence_001´
