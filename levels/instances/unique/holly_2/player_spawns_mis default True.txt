﻿´point_spawn_player_001´ ElementPlayerSpawner 100000
	EXECUTE ON STARTUP
	Enable "standard" player spawn at position (50.0, -99.9998, 0.0)
´point_spawn_player_002´ ElementPlayerSpawner 100001
	EXECUTE ON STARTUP
	Enable "standard" player spawn at position (-50.0, -100.0, 0.0)
´point_spawn_player_003´ ElementPlayerSpawner 100002
	EXECUTE ON STARTUP
	Enable "standard" player spawn at position (49.9998, 0.000213623, 0.0)
´point_spawn_player_004´ ElementPlayerSpawner 100003
	EXECUTE ON STARTUP
	Enable "standard" player spawn at position (-50.0002, 0.0, 0.0)
´trigger_spawn_area´ ElementAreaTrigger 100004
	TRIGGER TIMES 1
	Box, width 500.0, height 500.0, depth 500.0, at position (0.0, 0.0, 100.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Execute sequence: "takeoff_to_right" - units/pd2_dlc_cro/vehicles/veh_helicopter_water/001 (-233.655, 358.9, -50.0)
		Execute sequence: "diamondheist" - units/pd2_dlc_cro/vehicles/veh_helicopter_water/001 (-233.655, 358.9, -50.0)
		Execute sequence: "hidden" - units/pd2_dlc_cro/vehicles/veh_helicopter_water/001 (-233.655, 358.9, -50.0)
		Give 1 saw to instigator.
		´players_spawned´
		Hide object: units/dev_tools/level_tools/dev_collision_4x3m/006 (100.0, -300.0, 300.0) (DELAY 2)
		Hide object: units/dev_tools/level_tools/dev_collision_4x3m/005 (200.0, -300.0, 300.0) (DELAY 2)
		Hide object: units/dev_tools/level_tools/dev_collision_4x3m/002 (-200.0, -300.0, 0.0) (DELAY 2)
		Hide object: units/dev_tools/level_tools/dev_collision_4x3m/003 (200.0, -300.0, 0.0) (DELAY 2)
		Hide object: units/dev_tools/level_tools/dev_collision_4x3m/004 (-200.0, 100.0, 0.0) (DELAY 2)
		Hide object: units/dev_tools/level_tools/dev_collision_4x3m/001 (-200.0, -300.0, 0.0) (DELAY 2)
´players_spawned´ ElementInstanceOutput 100005
	TRIGGER TIMES 1
	Send instance event "players_spawned" to mission.
