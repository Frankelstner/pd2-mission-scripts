ID range vs continent name:
	100000: world

statics
	100004 units/payday2/vehicles/str_vehicle_truck_ambulance/001 (0.0, 0.0, 0.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.118, 0.118
			enabled False
			falloff_exponent 4
			far_range 350
			multiplier reddot
			name li_light_orange
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
		2
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.118, 0.118
			enabled False
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light_red
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
		mesh_variation state_vis_show
