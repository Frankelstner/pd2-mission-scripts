﻿´STARTUP´ MissionScriptElement 100001
	EXECUTE ON STARTUP
	on_executed
		´DELAY´ (delay 1)
´DELAY´ MissionScriptElement 100002
´start_car´ ElementUnitSequence 100003
	position 850.0, -800.0, 2000.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_car_police/001 (0.0, 0.0, 0.0)
			notify_unit_sequence anim_car_hox_intro_skidding
			time 0
´input_start_car´ ElementInstanceInput 100004
	event start_car
	on_executed
		´start_car´ (delay 0)
´activate_wheel_driving´ ElementUnitSequence 100040
	position 725.0, -325.0, 2000.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_old_hoxton/equipment/gen_interactable_police_car_steering_wheel/001 (-371.0, -3796.63, 77.5474)
			notify_unit_sequence state_interaction_enabled
			time 0
´wheel_interact_driving´ ElementUnitSequenceTrigger 100041
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/pd2_dlc_old_hoxton/equipment/gen_interactable_police_car_steering_wheel/001 (-371.0, -3796.63, 77.5474)
	on_executed
		´car_move_away´ (delay 0)
		´output_wheel_interact_driving´ (delay 0)
		´remove_driving_waypoint´ (delay 0)
		´output_driving_wheel_interact´ (delay 0)
´car_move_away´ ElementUnitSequence 100042
	position 994.0, -475.0, 2000.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_car_police/001 (0.0, 0.0, 0.0)
			notify_unit_sequence anim_car_hox_move_away
			time 0
´open_doors´ ElementUnitSequence 100043
	position 994.0, -800.0, 2000.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_car_police/001 (0.0, 0.0, 0.0)
			notify_unit_sequence anim_door_left_open
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_car_police/001 (0.0, 0.0, 0.0)
			notify_unit_sequence anim_door_right_open
			time 0
´input_activate_wheel_driving´ ElementInstanceInput 100044
	event activate_wheel_driving
	on_executed
		´activate_wheel_driving´ (delay 0)
		´show_waypoint_driving´ (delay 0)
´output_wheel_interact_driving´ ElementInstanceOutput 100006
	event wheel_interect_driving
´input_police_off´ ElementInstanceInput 100007
	event set_police_off
	on_executed
		´police_state_off´ (delay 0)
´police_state_off´ ElementUnitSequence 100008
	position 1250.0, -800.0, 2002.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_car_police/001 (0.0, 0.0, 0.0)
			notify_unit_sequence state_police_officers_off
			time 0
´input_doors_open´ ElementInstanceInput 100009
	event doors_open
	on_executed
		´open_doors´ (delay 0)
´input_open_left_door´ ElementInstanceInput 100010
	event open_left_door
	on_executed
		´open_left_door´ (delay 0)
´open_left_door´ ElementUnitSequence 100011
	position 1125.0, -800.0, 2000.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_car_police/001 (0.0, 0.0, 0.0)
			notify_unit_sequence anim_door_left_open
			time 0
´wheel_ineract_default´ ElementUnitSequenceTrigger 100013
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/pd2_dlc_old_hoxton/equipment/gen_interactable_police_car_steering_wheel/002 (42.0, -48.9693, 79.7769)
	on_executed
		´car_move_away´ (delay 0)
		´output_wheel_interact_default´ (delay 0)
		´remove_default_waypoint´ (delay 0)
		´output_default_wheel_interact´ (delay 0)
´input_activate_wheel_default´ ElementInstanceInput 100014
	event activate_wheel_default
	on_executed
		´activate_wheel_default´ (delay 0)
		´show_waypoint_default´ (delay 0)
´activate_wheel_default´ ElementUnitSequence 100015
	position 850.0, -325.0, 2000.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_old_hoxton/equipment/gen_interactable_police_car_steering_wheel/002 (42.0, -48.9693, 79.7769)
			notify_unit_sequence state_interaction_enabled
			time 0
´output_wheel_interact_default´ ElementInstanceOutput 100016
	event wheel_interact_default
´INPUT_variation_driving´ ElementInstanceInput 100017
	event variation_driving
	on_executed
		´start_car´ (delay 0)
		´set_siren_on´ (delay 0)
´driving_done´ ElementUnitSequenceTrigger 100020
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence done_anim_police_responce
			unit_id units/payday2/vehicles/anim_vehicle_car_police/001 (0.0, 0.0, 0.0)
	on_executed
		´open_doors´ (delay 0)
		´activate_wheel_driving´ (delay 0)
		´show_waypoint_driving´ (delay 0)
		´output_driving_done´ (delay 0)
´INPUT_variation_default´ ElementInstanceInput 100005
	event variation_default
	on_executed
		´activate_wheel_default´ (delay 0)
		´police_state_off´ (delay 0)
		´open_left_door´ (delay 0)
		´set_siren_on´ (delay 0)
		´set_trigger_off´ (delay 0)
		´show_waypoint_default´ (delay 0)
´input_siren_on´ ElementInstanceInput 100018
	event siren_on
	on_executed
		´set_siren_on´ (delay 0)
´set_siren_on´ ElementUnitSequence 100019
	position 1375.0, -800.0, 2002.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_car_police/001 (0.0, 0.0, 0.0)
			notify_unit_sequence state_lights_siren_on
			time 0
´set_trigger_off´ ElementToggle 100021
	elements
		1 ´driving_done´
	set_trigger_times -1
	toggle off
´show_waypoint_driving´ MissionScriptElement 100022
	on_executed
		´waypoint_wheel_driving´ (delay 0)
´show_waypoint_default´ MissionScriptElement 100023
	on_executed
		´waypoint_wheel_default´ (delay 0)
´waypoint_wheel_default´ ElementWaypoint 100024
	icon pd2_generic_look
	only_in_civilian False
	position 75.0, -25.0, 100.396
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´waypoint_wheel_driving´ ElementWaypoint 100025
	icon pd2_generic_look
	only_in_civilian False
	position -296.0, -3822.0, 78.0
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´remove_default_waypoint´ ElementOperator 100026
	elements
		1 ´waypoint_wheel_default´
	operation remove
´remove_driving_waypoint´ ElementOperator 100027
	elements
		1 ´waypoint_wheel_driving´
	operation remove
´output_driving_done´ ElementInstanceOutput 100028
	event driving_done
´output_default_wheel_interact´ ElementInstanceOutput 100029
	event default_wheel_interact
´output_driving_wheel_interact´ ElementInstanceOutput 100030
	event driving_wheel_interact
