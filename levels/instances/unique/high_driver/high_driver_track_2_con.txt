ID range vs continent name:
	100000: world

statics
	100082 units/payday2/props/air_prop_runway_fence_gate_v2/001 (1562.18, -12690.3, 504.74)
	100090 units/payday2/props/air_prop_runway_fence_gate_v2/002 (2150.45, 9733.43, 504.74)
	100097 units/payday2/props/air_prop_runway_fence_gate_v2/003 (-2165.75, -6204.17, 504.74)
	100104 units/payday2/props/air_prop_runway_fence_gate_v2/004 (-1665.86, 18975.6, 504.74)
	100472 units/payday2/props/hwy_prop_sign_construction_mobile/013 (-800.0, -14000.0, 500.0)
		mesh_variation normal
	100474 units/payday2/props/hwy_prop_sign_construction_mobile/015 (5309.0, -11363.0, 500.0)
		mesh_variation normal
	100484 units/payday2/props/str_prop_construction_passage/003 (3300.0, 9575.0, 525.0)
	100485 units/payday2/props/str_prop_construction_passage/004 (4100.0, 9575.0, 525.0)
	100005 units/payday2/props/str_prop_construction_passage/005 (4900.0, 9575.0, 525.0)
	100015 units/payday2/props/str_prop_construction_passage/006 (4100.0, -13275.0, 525.0)
	100018 units/payday2/props/str_prop_construction_passage/007 (4900.0, -13275.0, 525.0)
	100019 units/payday2/props/str_prop_construction_passage/008 (5700.0, -13275.0, 525.0)
	100026 units/payday2/props/str_prop_construction_passage/009 (400.0, -11800.0, 525.0)
	100027 units/payday2/props/str_prop_construction_passage/010 (-400.0, -11800.0, 525.0)
	100028 units/payday2/props/str_prop_construction_passage/011 (-1200.0, -11800.0, 525.0)
	100029 units/payday2/props/str_prop_construction_passage/012 (-400.0, 8300.0, 525.0)
	100033 units/payday2/props/str_prop_construction_passage/013 (400.0, 8300.0, 525.0)
	100037 units/payday2/props/str_prop_construction_passage/014 (1200.0, 8300.0, 525.0)
	100038 units/payday2/props/str_prop_construction_passage/015 (400.0, -4900.0, 525.0)
	100039 units/payday2/props/str_prop_construction_passage/016 (-400.0, -4900.0, 525.0)
	100040 units/payday2/props/str_prop_construction_passage/017 (-1200.0, -4900.0, 525.0)
	100047 units/payday2/props/str_prop_construction_passage/018 (-4900.0, -6450.0, 525.0)
	100048 units/payday2/props/str_prop_construction_passage/019 (-4100.0, -6450.0, 525.0)
	100049 units/payday2/props/str_prop_construction_passage/020 (-3300.0, -6450.0, 525.0)
	100065 units/payday2/props/str_prop_construction_passage/021 (-4100.0, 19575.0, 525.0)
	100066 units/payday2/props/str_prop_construction_passage/022 (-4900.0, 19575.0, 525.0)
	100067 units/payday2/props/str_prop_construction_passage/023 (-5700.0, 19575.0, 525.0)
	100074 units/payday2/props/str_prop_construction_passage/024 (400.0, 18150.0, 525.0)
	100075 units/payday2/props/str_prop_construction_passage/025 (-400.0, 18150.0, 525.0)
	100076 units/payday2/props/str_prop_construction_passage/026 (1200.0, 18150.0, 525.0)
	100077 units/payday2/props/str_prop_construction_sign_road_lights/001 (1397.7, -13018.7, 505.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier desklight
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100078 units/payday2/props/str_prop_construction_sign_road_lights/002 (1592.18, -13213.2, 505.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier monitor
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100079 units/payday2/props/str_prop_construction_sign_road_lights/003 (1786.64, -13407.6, 505.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100084 units/payday2/props/str_prop_construction_sign_road_lights/004 (1627.54, 9703.43, 505.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier monitor
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100085 units/payday2/props/str_prop_construction_sign_road_lights/005 (1433.08, 9508.98, 505.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100086 units/payday2/props/str_prop_construction_sign_road_lights/006 (1821.99, 9897.91, 505.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100091 units/payday2/props/str_prop_construction_sign_road_lights/007 (-1430.7, -5997.4, 505.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier flashlight
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100093 units/payday2/props/str_prop_construction_sign_road_lights/008 (-1642.83, -6174.18, 505.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier candle
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100095 units/payday2/props/str_prop_construction_sign_road_lights/009 (-1837.29, -6368.65, 505.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier flashlight
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100098 units/payday2/props/str_prop_construction_sign_road_lights/010 (-1890.32, 19693.0, 505.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier identity
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100502 units/payday2/props/str_prop_construction_sign_road_lights/011 (4000.0, 8775.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier identity
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100006 units/payday2/props/str_prop_construction_sign_road_lights/012 (4275.0, 8775.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100007 units/payday2/props/str_prop_construction_sign_road_lights/013 (3725.0, 8775.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100008 units/payday2/props/str_prop_construction_sign_road_lights/014 (4700.0, 8775.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier identity
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100009 units/payday2/props/str_prop_construction_sign_road_lights/015 (4975.0, 8775.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100010 units/payday2/props/str_prop_construction_sign_road_lights/016 (5250.0, 8775.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier identity
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100011 units/payday2/props/str_prop_construction_sign_road_lights/017 (3750.0, -12475.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100012 units/payday2/props/str_prop_construction_sign_road_lights/018 (4025.0, -12475.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100013 units/payday2/props/str_prop_construction_sign_road_lights/019 (4300.0, -12475.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier identity
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100014 units/payday2/props/str_prop_construction_sign_road_lights/020 (4725.0, -12475.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier neonsign
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100016 units/payday2/props/str_prop_construction_sign_road_lights/021 (5000.0, -12475.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100017 units/payday2/props/str_prop_construction_sign_road_lights/022 (5275.0, -12475.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100020 units/payday2/props/str_prop_construction_sign_road_lights/023 (200.0, -12600.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier identity
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100021 units/payday2/props/str_prop_construction_sign_road_lights/024 (-225.0, -12600.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier identity
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100022 units/payday2/props/str_prop_construction_sign_road_lights/025 (475.0, -12600.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100023 units/payday2/props/str_prop_construction_sign_road_lights/026 (-500.0, -12600.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100024 units/payday2/props/str_prop_construction_sign_road_lights/027 (750.0, -12600.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100025 units/payday2/props/str_prop_construction_sign_road_lights/028 (-775.0, -12600.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier identity
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100030 units/payday2/props/str_prop_construction_sign_road_lights/029 (-750.0, 9100.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100031 units/payday2/props/str_prop_construction_sign_road_lights/030 (-475.0, 9100.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100032 units/payday2/props/str_prop_construction_sign_road_lights/031 (-200.0, 9100.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier monitor
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100034 units/payday2/props/str_prop_construction_sign_road_lights/032 (225.0, 9100.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier monitor
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100035 units/payday2/props/str_prop_construction_sign_road_lights/033 (500.0, 9100.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier candle
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100036 units/payday2/props/str_prop_construction_sign_road_lights/034 (775.0, 9100.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier identity
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100041 units/payday2/props/str_prop_construction_sign_road_lights/035 (200.0, -5700.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100042 units/payday2/props/str_prop_construction_sign_road_lights/036 (475.0, -5700.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier identity
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100043 units/payday2/props/str_prop_construction_sign_road_lights/037 (750.0, -5700.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier identity
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100044 units/payday2/props/str_prop_construction_sign_road_lights/038 (-225.0, -5700.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier identity
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100045 units/payday2/props/str_prop_construction_sign_road_lights/039 (-500.0, -5700.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100046 units/payday2/props/str_prop_construction_sign_road_lights/040 (-775.0, -5700.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100050 units/payday2/props/str_prop_construction_sign_road_lights/041 (-4700.0, -5650.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier identity
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100051 units/payday2/props/str_prop_construction_sign_road_lights/042 (-4975.0, -5650.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier candle
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100052 units/payday2/props/str_prop_construction_sign_road_lights/043 (-5250.0, -5650.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier desklight
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100053 units/payday2/props/str_prop_construction_sign_road_lights/044 (-4275.0, -5650.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier match
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100054 units/payday2/props/str_prop_construction_sign_road_lights/045 (-4000.0, -5650.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier neonsign
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100055 units/payday2/props/str_prop_construction_sign_road_lights/046 (-3725.0, -5650.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier match
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100059 units/payday2/props/str_prop_construction_sign_road_lights/047 (-4300.0, 18775.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier neonsign
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100060 units/payday2/props/str_prop_construction_sign_road_lights/048 (-4025.0, 18775.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier desklight
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100061 units/payday2/props/str_prop_construction_sign_road_lights/049 (-4725.0, 18775.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100062 units/payday2/props/str_prop_construction_sign_road_lights/050 (-3750.0, 18775.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier identity
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100063 units/payday2/props/str_prop_construction_sign_road_lights/051 (-5000.0, 18775.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier flashlight
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100064 units/payday2/props/str_prop_construction_sign_road_lights/052 (-5275.0, 18775.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier identity
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100068 units/payday2/props/str_prop_construction_sign_road_lights/053 (225.0, 18950.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier flashlight
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100069 units/payday2/props/str_prop_construction_sign_road_lights/054 (-200.0, 18950.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier monitor
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100070 units/payday2/props/str_prop_construction_sign_road_lights/055 (500.0, 18950.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier monitor
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100071 units/payday2/props/str_prop_construction_sign_road_lights/056 (-475.0, 18950.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100072 units/payday2/props/str_prop_construction_sign_road_lights/057 (775.0, 18950.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier flashlight
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100073 units/payday2/props/str_prop_construction_sign_road_lights/058 (-750.0, 18950.0, 525.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100100 units/payday2/props/str_prop_construction_sign_road_lights/059 (-1695.86, 19498.5, 505.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier none
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100102 units/payday2/props/str_prop_construction_sign_road_lights/060 (-1501.39, 19304.1, 505.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.344, 0.0
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier identity
			name li_light1
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100596 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_connection_a/007 (4500.0, 10500.0, 500.0)
	100001 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_connection_a/008 (-4500.0, -7000.0, 500.0)
	100735 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_connection_b/006 (4500.0, -14000.0, 500.0)
	100000 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_connection_b/007 (0.0, 17500.0, 500.0)
	100105 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/001 (-4500.0, 15044.0, 500.0)
	100109 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/002 (-4500.0, -3398.0, 500.0)
	100114 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/003 (-4500.0, 11544.0, 500.0)
	100115 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/004 (-4500.0, 102.0, 500.0)
	100117 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/006 (-4500.0, 1747.0, 500.0)
	100118 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/007 (-4500.0, 3497.0, 500.0)
	100119 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/008 (-4500.0, 5247.0, 500.0)
	100120 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/009 (-4500.0, 6997.0, 500.0)
	100121 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/010 (-4500.0, 8747.0, 500.0)
	100122 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/011 (-4500.0, -7003.0, 500.0)
	100123 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/012 (-4500.0, -8753.0, 500.0)
	100124 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/013 (-4500.0, 20122.0, 500.0)
	100125 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/014 (0.0, 21072.0, 500.0)
	100126 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/015 (0.0, 19172.0, 500.0)
	100129 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/016 (0.0, 15672.0, 500.0)
	100130 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/017 (0.0, 12962.0, 500.0)
	100131 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/018 (0.0, 9202.0, 500.0)
	100134 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/019 (0.0, 5702.0, 500.0)
	100135 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/020 (0.0, 1747.0, 500.0)
	100136 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/021 (0.0, -3.0, 500.0)
	100184 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/024 (-4500.0, 10497.0, 500.0)
	100138 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/025 (0.0, -5706.0, 500.0)
	100143 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/026 (0.0, -9461.0, 500.0)
	100146 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/027 (0.0, -11264.0, 500.0)
	100145 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/028 (0.0, -12704.0, 500.0)
	100147 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/029 (-350.471, 22610.6, 501.0)
	100148 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/030 (-1218.17, 23915.3, 500.0)
	100149 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/031 (-2590.83, 24788.8, 500.0)
	100150 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/032 (-5632.09, 25427.0, 500.0)
	100151 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/033 (-7017.48, 26319.7, 500.0)
	100153 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/034 (-7843.72, 27592.1, 500.0)
	100154 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/035 (-4103.27, 25098.9, 500.0)
	100155 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/036 (0.0, -16461.0, 500.0)
	100156 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/037 (-317.43, -19024.1, 500.0)
	100157 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/038 (-1211.47, -20404.6, 500.0)
	100158 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/039 (-2535.26, -21267.4, 500.0)
	100159 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/040 (-4107.61, -21598.7, 500.0)
	100160 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/041 (-5709.7, -21957.2, 500.0)
	100161 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/042 (-7010.0, -22822.1, 500.0)
	100162 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/043 (-7868.34, -24150.4, 500.0)
	100163 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/044 (4500.0, -6901.0, 500.0)
	100164 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/045 (4500.0, -10398.0, 500.0)
	100165 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/046 (0.0, -2203.0, 500.0)
	100166 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/047 (-4500.0, 18797.0, 500.0)
	100116 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/048 (0.0, 14362.0, 500.0)
	100168 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/049 (-4500.0, -5328.0, 500.0)
	100169 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/050 (4500.0, -12331.0, 500.0)
	100170 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/051 (4500.0, -14878.0, 500.0)
	100171 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/052 (4500.0, -16628.0, 500.0)
	100172 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/053 (4500.0, 3402.0, 500.0)
	100175 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/054 (4500.0, 6899.0, 500.0)
	100176 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/055 (4500.0, 8832.0, 500.0)
	100177 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/056 (4500.0, 11367.0, 500.0)
	100178 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/057 (4500.0, 13121.0, 500.0)
	100179 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/058 (4500.0, -5253.0, 500.0)
	100180 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/059 (4500.0, -3503.0, 500.0)
	100181 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/060 (4500.0, -1753.0, 500.0)
	100182 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/061 (4500.0, -3.0, 500.0)
	100183 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/062 (4500.0, 1758.0, 500.0)
	100185 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/063 (0.0, 3497.0, 500.0)
	100186 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_pillar/064 (0.0, -17511.0, 500.0)
	102397 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_straight_a/026 (0.0, 17500.0, 500.0)
	100736 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_straight_a/160 (4500.0, -10500.0, 500.0)
	100737 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_straight_a/161 (4500.0, -7000.0, 500.0)
	100738 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_straight_a/162 (0.0, 0.0, 500.0)
	100739 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_straight_a/163 (4500.0, -3500.0, 500.0)
	100740 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_straight_a/164 (0.0, 3500.0, 500.0)
	100741 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_straight_a/165 (4500.0, 0.0, 500.0)
	100746 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_straight_a/166 (-4500.0, 0.0, 500.0)
	100747 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_straight_a/167 (4500.0, 3500.0, 500.0)
	100002 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_straight_a/168 (0.0, -3500.0, 500.0)
	100056 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_straight_a/169 (-4500.0, 3500.0, 500.0)
	100750 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_straight_a/170 (4500.0, -17500.0, 500.0)
	100003 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_straight_a/171 (-4500.0, -3500.0, 500.0)
	100004 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_straight_a/172 (-4500.0, -10500.0, 500.0)
	100754 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_straight_a/173 (4500.0, 10499.0, 500.0)
	100057 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_straight_a/174 (-4500.0, 7000.0, 500.0)
	100058 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_straight_a/175 (-4500.0, 10500.0, 500.0)
	100619 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_turn_small_b/018 (-8200.0, -25700.0, 500.0)
	100620 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_turn_small_b/019 (0.0, -17500.0, 500.0)
	100152 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_turn_small_b/020 (-4100.0, 25100.0, 500.0)
	100106 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_turn_small_b/021 (-4100.0, 25100.0, 500.0)
	100107 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_wall_straight_a/002 (-4500.0, 15044.0, 500.0)
	100108 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_wall_straight_a/003 (-4500.0, 13344.0, 500.0)
	100112 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_wall_straight_a/004 (-4500.0, -3398.0, 500.0)
	100113 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_wall_straight_a/005 (-4500.0, -1698.0, 500.0)
	100127 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_wall_straight_a/006 (0.0, 19172.0, 500.0)
	100128 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_wall_straight_a/007 (0.0, 17472.0, 500.0)
	100132 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_wall_straight_a/008 (0.0, 9202.0, 500.0)
	100133 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_wall_straight_a/009 (0.0, 7502.0, 500.0)
	100139 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_wall_straight_a/010 (4500.0, -8698.0, 500.0)
	100140 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_wall_straight_a/011 (4500.0, -10398.0, 500.0)
	100144 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_wall_straight_a/012 (0.0, -9461.0, 500.0)
	100141 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_wall_straight_a/013 (0.0, -2198.0, 500.0)
	100167 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_wall_straight_a/014 (0.0, -3898.0, 500.0)
	100173 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_wall_straight_a/015 (4500.0, 3402.0, 500.0)
	100174 units/pd2_dlc_cage/architecture/ind_tunnel_expressway_wall_straight_a/016 (4500.0, 5102.0, 500.0)
	100080 units/pd2_dlc_cage/props/str_prop_streetcone_dyn_02/001 (1963.38, -13301.6, 505.0)
	100081 units/pd2_dlc_cage/props/str_prop_streetcone_dyn_02/002 (1733.6, -13071.7, 505.0)
	100083 units/pd2_dlc_cage/props/str_prop_streetcone_dyn_02/003 (1521.4, -12859.6, 505.0)
	100087 units/pd2_dlc_cage/props/str_prop_streetcone_dyn_02/004 (1768.96, 9562.01, 505.0)
	100088 units/pd2_dlc_cage/props/str_prop_streetcone_dyn_02/005 (1539.15, 9332.23, 505.0)
	100089 units/pd2_dlc_cage/props/str_prop_streetcone_dyn_02/006 (1981.09, 9774.21, 505.0)
	100092 units/pd2_dlc_cage/props/str_prop_streetcone_dyn_02/007 (-1554.44, -5802.98, 505.0)
	100094 units/pd2_dlc_cage/props/str_prop_streetcone_dyn_02/008 (-1784.25, -6032.75, 505.0)
	100096 units/pd2_dlc_cage/props/str_prop_streetcone_dyn_02/009 (-1996.39, -6244.95, 505.0)
	100099 units/pd2_dlc_cage/props/str_prop_streetcone_dyn_02/010 (-2067.06, 19586.9, 505.0)
	100101 units/pd2_dlc_cage/props/str_prop_streetcone_dyn_02/011 (-1837.29, 19357.1, 505.0)
	100103 units/pd2_dlc_cage/props/str_prop_streetcone_dyn_02/012 (-1625.09, 19145.0, 505.0)
