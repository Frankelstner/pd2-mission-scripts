﻿´STARTUP´ MissionScriptElement 100006
	EXECUTE ON STARTUP
	on_executed
		´DELAY´ (delay 1)
´DELAY´ MissionScriptElement 100007
´input_hide_truck´ ElementInstanceInput 100013
	event hide_truck
	on_executed
		´hide_swat_truck´ (delay 0)
´hide_and_act_drill´ ElementUnitSequence 100020
	position -150.0, 25.0, 2552.5
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-104.0, -95.0, 150.0)
			notify_unit_sequence state_hide
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-104.0, -95.0, 150.0)
			notify_unit_sequence activate
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-104.0, -95.0, 150.0)
			notify_unit_sequence set_hack_time_state_2
			time 0
	on_executed
		´show_drill_waypoint´ (delay 0)
´drill_done_sequence´ ElementUnitSequence 100021
	position 125.0, 250.0, 2550.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_van_swat/001 (-50.0001, -6.10352e-05, 0.0)
			notify_unit_sequence anim_door_driver_open
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_old_hoxton/equipment/gen_interactable_swat_van_steering_wheel/001 (-164.032, -44.0, 169.396)
			notify_unit_sequence state_interaction_enabled
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-104.0, -95.0, 150.0)
			notify_unit_sequence deactivate
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-104.0, -95.0, 150.0)
			notify_unit_sequence state_hide
			time 0
´hide_swat_truck´ ElementUnitSequence 100022
	position -150.0, 250.0, 2550.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_van_swat/001 (-50.0001, -6.10352e-05, 0.0)
			notify_unit_sequence state_vis_hide
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-104.0, -95.0, 150.0)
			notify_unit_sequence deactivate
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-104.0, -95.0, 150.0)
			notify_unit_sequence state_hide
			time 0
´input_show_truck´ ElementInstanceInput 100023
	event show_truck
	on_executed
		´show_swat_truck´ (delay 0)
´show_swat_truck´ ElementUnitSequence 100024
	position -150.0, 125.0, 2550.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_van_swat/001 (-50.0001, -6.10352e-05, 0.0)
			notify_unit_sequence state_vis_show
			time 0
´drill_done´ ElementUnitSequenceTrigger 100025
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence timer_done
			unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-104.0, -95.0, 150.0)
	on_executed
		´drill_done_sequence´ (delay 0)
		´show_waypoint_wheel´ (delay 0)
		´output_drill_done´ (delay 0)
´swat_wheel_interact´ ElementUnitSequenceTrigger 100026
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/pd2_dlc_old_hoxton/equipment/gen_interactable_swat_van_steering_wheel/001 (-164.032, -44.0, 169.396)
	on_executed
		´move_away´ (delay 0)
		´remove_wheel_waypoint´ (delay 0)
		´output_wheel_interacted´ (delay 0)
´move_away´ ElementUnitSequence 100000
	position 125.0, 25.0, 2550.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/anim_vehicle_van_swat/001 (-50.0001, -6.10352e-05, 0.0)
			notify_unit_sequence anim_hox_move_away
			time 0
´waypoint_drill´ ElementWaypoint 100001
	icon pd2_drill
	only_in_civilian False
	position -125.0, -125.0, 150.121
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´drill_put_on´ ElementUnitSequenceTrigger 100004
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-104.0, -95.0, 150.0)
	on_executed
		´remove_drill_waypoint´ (delay 0)
		´output_drill_put_on´ (delay 0)
´remove_drill_waypoint´ ElementOperator 100005
	elements
		1 ´waypoint_drill´
	operation remove
´show_drill_waypoint´ MissionScriptElement 100003
	on_executed
		´waypoint_drill´ (delay 0)
´input_enable_drill´ ElementInstanceInput 100008
	event enable_drill
	on_executed
		´hide_and_act_drill´ (delay 0)
´show_waypoint_wheel´ MissionScriptElement 100009
	on_executed
		´waypoint_wheel´ (delay 0)
´waypoint_wheel´ ElementWaypoint 100010
	icon pd2_generic_look
	only_in_civilian False
	position -150.0, -50.0, 176.599
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´remove_wheel_waypoint´ ElementOperator 100011
	elements
		1 ´waypoint_wheel´
	operation remove
´swat_truck_moved_away´ ElementUnitSequenceTrigger 100014
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence done_car_anim
			unit_id units/payday2/vehicles/anim_vehicle_van_swat/001 (-50.0001, -6.10352e-05, 0.0)
	on_executed
		´output_truck_moved_away´ (delay 0)
´output_truck_moved_away´ ElementInstanceOutput 100015
	event truck_moved_away
´output_wheel_interacted´ ElementInstanceOutput 100016
	event wheel_interacted
´output_drill_done´ ElementInstanceOutput 100017
	event drill_done
´output_drill_put_on´ ElementInstanceOutput 100019
	event drill_put_on
