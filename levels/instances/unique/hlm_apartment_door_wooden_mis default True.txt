﻿´isntance_input001´ ElementInstanceInput 100002
	event add_nav_blocker
´func_nav_obstacle_001´ ElementNavObstacle 100003
	obstacle_list
		1
			guis_id 1
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker/001 (25.0, 0.0, 0.0)
	operation add
	position -100.0, 0.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
´func_sequence_trigger_001´ ElementUnitSequenceTrigger 100004
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence done_opened
			unit_id units/payday2/equipment/gen_interactable_door_wooden_a/001 (25.0, -50.0, 0.0)
	on_executed
		´door_opened´ (delay 0)
		´point_debug_004´ (delay 0)
´func_nav_obstacle_002´ ElementNavObstacle 100005
	obstacle_list
		1
			guis_id 1
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker/001 (25.0, 0.0, 0.0)
	operation remove
	position 0.0, -100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
´isntance_input_001´ ElementInstanceInput 100006
	event activate_SO_enemy_enter
´SO_enemy_enter001´ ElementSpecialObjective 100007
	TRIGGER TIMES 1
	SO_access 15864
	action_duration_max 0
	action_duration_min 0
	ai_group enemies
	align_position True
	align_rotation True
	attitude engage
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis 0
	interrupt_dmg 1
	interval 8
	is_navigation_link True
	needs_pos_rsrv True
	path_haste run
	path_stance hos
	path_style destination
	patrol_path none
	pose none
	position 175.0, 0.0, 0.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	scan True
	search_distance 0
	search_position -82.5632, 2.36009, 0.0
	so_action e_nl_kick_enter
	trigger_on none
´trigger_special_objective_001´ ElementSpecialObjectiveTrigger 100008
	elements
		1 ´SO_enemy_enter001´
	event anim_act_04
	position 250.0, 0.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´door_opened´ (delay 0)
		´open_door´ (delay 0)
		´point_debug_003´ (delay 0)
´logic_operator_001´ ElementOperator 100009
	DISABLED
	TRIGGER TIMES 1
	elements
		1 ´SO_enemy_enter001´
	operation remove
´door_opened´ MissionScriptElement 100010
	TRIGGER TIMES 1
	on_executed
		´func_nav_obstacle_002´ (delay 0)
		´logic_operator_001´ (delay 0) DISABLED
		´disable_open_door_seq´ (delay 0.05) DISABLED
´activate_SO_enemy_enter´ MissionScriptElement 100011
	on_executed
		´SO_enemy_enter001´ (delay 0)
´open_door´ ElementUnitSequence 100014
	position 250.0, 100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_door_wooden_a/001 (25.0, -50.0, 0.0)
			notify_unit_sequence open_door
			time 0
	on_executed
		´logic_operator_001´ (delay 0) DISABLED
		´point_debug_002´ (delay 0)
´disable_open_door_seq´ ElementToggle 100015
	DISABLED
	elements
		1 ´open_door´
	set_trigger_times -1
	toggle off
	on_executed
		´point_debug_001´ (delay 0)
´point_debug_001´ ElementDebug 100012
	as_subtitle False
	debug_string disabled
	show_instigator False
´point_debug_002´ ElementDebug 100013
	as_subtitle False
	debug_string seq
	show_instigator False
´point_debug_003´ ElementDebug 100016
	as_subtitle False
	debug_string done
	show_instigator False
´point_debug_004´ ElementDebug 100017
	as_subtitle False
	debug_string door opened
	show_instigator False
´logic_link_001´ MissionScriptElement 100018
	EXECUTE ON STARTUP
	on_executed
		´activate_SO_enemy_enter´ (delay 3)
		´func_nav_obstacle_001´ (delay 1)
