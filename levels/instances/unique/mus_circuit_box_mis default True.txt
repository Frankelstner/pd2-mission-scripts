﻿´setup_hide´ ElementUnitSequence 100012
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 0.75)
	Execute sequence: "state_hide" - units/pd2_indiana/props/mus_prop_electric_box/001 (0.0, 0.0, 11.0)
´open_interaction001´ ElementInteraction 100002
	DISABLED
	Create interaction object with ID "invisible_interaction_open" with timer 2.0at position (1.0, -9.0, 100.0)
	Upon interaction:
		´open_box´
		´interact_enabled´ (DELAY 0.5)
´open_box´ ElementUnitSequence 100000
	TRIGGER TIMES 1
	Execute sequence: "open_door" - units/pd2_indiana/props/mus_prop_electric_box/001 (0.0, 0.0, 11.0)
´interact_enabled´ ElementUnitSequence 100039
	TRIGGER TIMES 1
	Execute sequence: "interact_enabled" - units/pd2_indiana/props/mus_prop_electric_box/001 (0.0, 0.0, 11.0)
´interacted´ ElementUnitSequenceTrigger 100003
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_indiana/props/mus_prop_electric_box/001 (0.0, 0.0, 11.0):
		Send instance event "circuit_fixed" to mission.
		Execute sequence: "close_door" - units/pd2_indiana/props/mus_prop_electric_box/001 (0.0, 0.0, 11.0) (DELAY 0.5)
´disable_circuit_box´ ElementInstanceInput 100004
	Upon mission event "disable_circuit_box":
		Toggle off: ´open_interaction001´
		Execute sequence: "interact_disabled" - units/pd2_indiana/props/mus_prop_electric_box/001 (0.0, 0.0, 11.0)
´set_circuit_box´ ElementInstanceInput 100009
	BASE DELAY  (DELAY 2)
	Upon mission event "set_circuit_box":
		Toggle on: ´open_interaction001´
		Execute sequence: "state_show" - units/pd2_indiana/props/mus_prop_electric_box/001 (0.0, 0.0, 11.0)
´computer_electric_box´ ElementInstanceInput 100014
	Upon mission event "computer_electric_box":
		Toggle on: ´start_interrupt_link´
		Toggle on: ´set_defend_link´
´start_interrupt_link´ MissionScriptElement 100017
	DISABLED
	´enable_and_action_so´
´set_defend_link´ MissionScriptElement 100036
	DISABLED
	´defend_wp´
´enable_and_action_so´ ElementToggle 100029
	Toggle on, trigger times 1: ´interrupt_so´
	Toggle on, trigger times 1: ´failed_interrupt´
	Toggle on, trigger times 1: ´completed_interrupt´
	Debug message: started_interrupt_at_power_box
	´interrupt_so´ (DELAY 0.05)
´interrupt_so´ ElementSpecialObjective 100020
	TRIGGER TIMES 1
	Create objective for enemies in 4500 radius around (0.0, -200.0, 100.0)
	Make chosen person path towards objective and show "e_so_interact_mid".
´failed_interrupt´ ElementSpecialObjectiveTrigger 100022
	TRIGGER TIMES 1
	Upon special objective event "fail" - ´interrupt_so´:
		Send instance event "failed_interrupt" to mission.
		Debug message: failed_at_power_box
´completed_interrupt´ ElementSpecialObjectiveTrigger 100021
	TRIGGER TIMES 1
	Upon special objective event "complete" - ´interrupt_so´:
		Send instance event "sucessful_interrupt" to mission.
		Toggle on: ´power_resume_interaction001´
		remove ´defend_wp´
		´power_wp´
´defend_wp´ ElementWaypoint 100030
	Place waypoint with icon "pd2_defend" at position (0.0, -29.0, 100.0)
´power_resume_interaction001´ ElementInteraction 100025
	DISABLED
	Create interaction object with ID "hold_circuit_breaker" with timer 2.0at position (1.0, -9.0, 100.0)
	Upon interaction:
		Send instance event "power_resumed" to mission.
		remove ´power_wp´
		´defend_wp´
´power_wp´ ElementWaypoint 100028
	Place waypoint with icon "pd2_power" at position (0.0, -29.0, 100.0)
´start_interrupt´ ElementInstanceInput 100015
	Upon mission event "start_interrupt":
		´start_interrupt_link´
´open_door_if_not_open´ ElementSpecialObjectiveTrigger 100023
	Upon special objective event "anim_start" - ´interrupt_so´:
		´open_box´
´set_defend´ ElementInstanceInput 100031
	Upon mission event "set_defend":
		´set_defend_link´
´disable_interrupting´ ElementInstanceInput 100033
	Upon mission event "disable_interrupting":
		remove ´interrupt_so´
		remove ´defend_wp´
		remove ´power_wp´
		Toggle off: ´completed_interrupt´
		Toggle off: ´failed_interrupt´
		Toggle off: ´start_interrupt_link´
		Toggle off: ´enable_and_action_so´
		Toggle off: ´interrupt_so´
