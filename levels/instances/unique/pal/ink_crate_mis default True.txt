﻿´logic_link_001´ ElementUnitSequence 100009
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Execute sequence: "state_requires_crowbar" - units/pd2_dlc_pal/props/pal_prop_shipping_crate/001 (-50.0, 75.0, 0.0)
	Execute sequence: "hay" - units/pd2_dlc_pal/props/pal_prop_shipping_crate/001 (-50.0, 75.0, 0.0)
	Execute sequence: "interaction_enabled" - units/pd2_dlc_pal/props/pal_prop_shipping_crate/001 (-50.0, 75.0, 0.0)
´activate_ink_crate´ ElementInstanceInput 100002
	Upon mission event "Activate ink crate":
		´add_wp´
´add_wp´ ElementWaypoint 100008
	Place waypoint with icon "pd2_generic_look" at position (-50.0, 75.0, 178.504)
´picked_up_can´ ElementUnitSequenceTrigger 100006
	Upon sequence "load" - units/pd2_dlc_pal/pickups/pal_pku_printer_ink_group/001 (-67.0, 75.0, 55.0):
		remove ´add_wp´
´deactivate_ink_crate´ ElementInstanceInput 100001
	Upon mission event "deactivate_ink_crate":
		remove ´add_wp´
´func_sequence_trigger_001´ ElementUnitSequenceTrigger 100010
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_pal/props/pal_prop_shipping_crate/001 (-50.0, 75.0, 0.0):
		Execute sequence: "state_interaction_enabled" - units/pd2_dlc_pal/pickups/pal_pku_printer_ink_group/001 (-67.0, 75.0, 55.0)
