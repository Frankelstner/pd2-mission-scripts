﻿´thermite_done´ ElementUnitSequenceTrigger 100001
	TRIGGER TIMES 1
	Upon sequence "done_thermite_burn" - units/pd2_indiana/equipment/mus_interactable_grate_small/001 (0.0, -50.0, 50.0):
		Execute sequence: "state_interaction_enable" - units/pd2_indiana/equipment/mus_interactable_win_small/001 (0.0, -50.0, 50.0)
