﻿´start_here´ MissionScriptElement 100002
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Execute 1: (DELAY 0.5)
		´open_left_wall´
		´close_left_wall´
	Execute 1: (DELAY 1)
		´logic_link_009´
		´logic_link_010´
	Execute 1: (DELAY 1)
		´logic_link_012´
		´left´
	´logic_random_003´ (DELAY 1.5)
´door001´ ElementUnitSequence 100180
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Execute sequence: "anim_door_close" - units/pd2_dlc_miami/props/gen_prop_door_metal_magnetic/001 (-450.0, 4100.0, 2.28882e-05)
	Add nav obstacle: units/dev_tools/level_tools/door_blocker_1m/001 (-450.0, 3894.0, 0.0)
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/005 (-750.0, 4500.0, 0.0)
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/008 (-750.0, 2900.0, 0.0)
	Hide object: units/payday2/architecture/res_ext_apartment_power_cable_04/001 (250.0, 1900.0, 0.0)
	Hide object: units/payday2/architecture/res_ext_apartment_power_cable_05/001 (251.0, 1900.0, 0.0)
	Hide object: units/payday2/architecture/res_ext_apartment_power_cable_06/001 (253.0, 1899.0, 0.0)
	Execute sequence: "closed" - units/payday2/architecture/res_ext_apartment_wardrobe_variation/002 (-700.0, 4500.0, 0.0)
	Execute sequence: "open" - units/payday2/architecture/res_ext_apartment_wardrobe_variation/008 (-700.0, 2900.0, 0.0)
	Toggle off: ´logic_random_003´
	Toggle off: ´Bain:_"The_door_is_open!_Good_jerb."´
	Toggle on: ´area_door_seen001´
	Hide object: units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/002 (-1684.0, 4499.0, 100.0)
	Hide object: units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/003 (-1684.0, 3600.0, 100.0)
	Hide object: units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/004 (-1684.0, 2700.0, 100.0)
	´addObstacle´
	´addObstacle_2´
	Execute 1: (DELAY 0.5)
		´func_sequence_006´
		´func_sequence_007´
		´func_sequence_008´
´open_left_wall´ ElementDisableUnit 100004
	Hide object: units/payday2/architecture/res_ext_apartment_rooms_4windows_04_piece/002 (-1700.0, 3175.0, 0.0)
´close_left_wall´ MissionScriptElement 100005
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/011 (-1750.0, 3175.0, 0.0)
	Execute sequence: "closed" - units/payday2/architecture/res_ext_apartment_wardrobe_variation/004 (-2150.0, 3150.0, 0.0)
	´addObstacle_3´
´logic_random_003´ ElementRandom 100048
	Execute 1:
		´logic_link_005´
		´logic_link_006´
´logic_link_009´ ElementDisableUnit 100087
	Hide object: units/payday2/architecture/res_ext_apartment_rooms_4windows_01_piece/005 (-1700.0, 4075.0, 0.0)
´logic_link_010´ MissionScriptElement 100088
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/012 (-1750.0, 4075.0, 0.0)
	Execute sequence: "open" - units/payday2/architecture/res_ext_apartment_wardrobe_variation/003 (-2150.0, 4050.0, 0.0)
	´addObstacle_4´
´logic_link_012´ MissionScriptElement 100101
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken_doubleside/002 (225.0, 4700.0, 0.0)
	Hide object: units/payday2/architecture/res_ext_apartment_rooms_3windows_floor_3_in_gap/002 (250.0, 2900.0, 300.0)
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/004 (1250.0, 4900.0, 0.0)
	Toggle off: ´nope_4´
	´addObstacle_5´
	Execute 1: (DELAY 0.5)
		´logic_link_016´
		´nope´
	Execute 1: (DELAY 0.5)
		´logic_link_017´
		´logic_link_018´
	Execute 1: (DELAY 1)
		´logic_link_021´
		´nope_2´
	Execute 1: (DELAY 1)
		´nope_3´
		´logic_link_020´
	Execute 1: (DELAY 1)
		´nope_4´
		´logic_link_019´
	´logic_link_022´ (DELAY 1)
´left´ MissionScriptElement 100102
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken_doubleside/001 (225.0, 3500.0, 0.0)
	Hide object: units/payday2/architecture/res_ext_apartment_rooms_3windows_floor_3_in_gap/001 (250.0, 4900.0, 300.0)
	Add nav obstacle: units/payday2/architecture/res_ext_apartment_rooms_3windows_floor_3_in_gap/002 (250.0, 2900.0, 300.0)
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/004 (1250.0, 4900.0, 0.0)
	Toggle off: ´nope_4´
	Execute 1: (DELAY 0.5)
		´logic_link_016´
		´nope´
	Execute 1: (DELAY 0.5)
		´logic_link_017´
		´logic_link_018´
	Execute 1: (DELAY 1)
		´logic_link_021´
		´nope_2´
	Execute 1: (DELAY 1)
		´nope_3´
		´logic_link_020´
	Execute 1: (DELAY 1)
		´nope_4´
		´logic_link_019´
	´logic_link_022´ (DELAY 1)
´addObstacle´ ElementNavObstacle 100060
	TRIGGER TIMES 1
	Add nav obstacle: units/dev_tools/level_tools/door_blocker_4x1/015 (-725.0, 3200.0, 0.0)
´addObstacle_2´ ElementNavObstacle 100061
	TRIGGER TIMES 1
	Add nav obstacle: units/dev_tools/level_tools/door_blocker_4x1/014 (-725.0, 4700.0, 0.0)
´Bain:_"The_door_is_open!_Good_jerb."´ ElementDebug 100196
	Debug message: Bain: "The door is open! Good jerb."
´area_door_seen001´ ElementAreaTrigger 100011
	DISABLED
	TRIGGER TIMES 1
	Box, width 405.0, height 500.0, depth 500.0, at position (-600.0, 3900.0, 0.0) with rotation (0.0, 0.0, -0.707107, -0.707107)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Play dialogue: Play_pln_hm2_28
		Toggle on: ´Bain:_"The_door_is_open!_Good_jerb."´
		Toggle on: ´instance_output_old_obj´
		´start_new_obj´
		´point_waypoint_001´
´func_sequence_006´ ElementUnitSequence 100186
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/002 (-1684.0, 4499.0, 100.0)
	Toggle on: ´state_remove_cover001´
	Toggle on: ´state_cut_cable001´
	Show object: units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/002 (-1684.0, 4499.0, 100.0)
	Show object: units/payday2/architecture/res_ext_apartment_power_cable_06/001 (253.0, 1899.0, 0.0)
´func_sequence_007´ ElementUnitSequence 100187
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/003 (-1684.0, 3600.0, 100.0)
	Toggle on: ´state_remove_cover002´
	Toggle on: ´state_cut_cable002´
	Show object: units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/003 (-1684.0, 3600.0, 100.0)
	Show object: units/payday2/architecture/res_ext_apartment_power_cable_05/001 (251.0, 1900.0, 0.0)
´func_sequence_008´ ElementUnitSequence 100189
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/004 (-1684.0, 2700.0, 100.0)
	Toggle on: ´state_remove_cover003´
	Toggle on: ´state_cut_cable003´
	Show object: units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/004 (-1684.0, 2700.0, 100.0)
	Show object: units/payday2/architecture/res_ext_apartment_power_cable_04/001 (250.0, 1900.0, 0.0)
´addObstacle_3´ ElementNavObstacle 100046
	TRIGGER TIMES 1
	Add nav obstacle: units/dev_tools/level_tools/door_blocker_4x1/012 (-1850.0, 3150.0, 0.0)
´logic_link_005´ MissionScriptElement 100049
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/006 (-750.0, 4500.0, 0.0)
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/008 (-750.0, 2900.0, 0.0)
	Execute sequence: "closed" - units/payday2/architecture/res_ext_apartment_wardrobe_variation/008 (-700.0, 2900.0, 0.0)
	´addObstacle´
´logic_link_006´ MissionScriptElement 100050
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/003 (-750.0, 2900.0, 0.0)
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/005 (-750.0, 4500.0, 0.0)
	Execute sequence: "open" - units/payday2/architecture/res_ext_apartment_wardrobe_variation/002 (-700.0, 4500.0, 0.0)
	´addObstacle_2´
´addObstacle_4´ ElementNavObstacle 100093
	TRIGGER TIMES 1
	Add nav obstacle: units/dev_tools/level_tools/door_blocker_4x1/013 (-1850.0, 4050.0, 0.0)
´addObstacle_5´ ElementNavObstacle 100104
	TRIGGER TIMES 1
	Add nav obstacle: units/payday2/architecture/res_ext_apartment_rooms_3windows_floor_3_in_gap/001 (250.0, 4900.0, 300.0)
´logic_link_016´ MissionScriptElement 100117
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/001 (1250.0, 3300.0, 0.0)
	Hide object: units/payday2/architecture/ext_apartment/res_apartment_blockade_01/001 (1475.0, 4150.0, -9.00006)
	Toggle off: ´nope_2´
	Toggle off: ´logic_link_022´
	´addObstacle_7´
´nope´ MissionScriptElement 100118
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/009 (1250.0, 3300.0, 0.0)
	Execute sequence: "closed" - units/payday2/architecture/res_ext_apartment_wardrobe_variation/006 (1200.0, 3300.0, 0.0)
	´addObstacle_8´
´logic_link_017´ ElementDisableUnit 100124
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/009 (1650.0, 4500.0, 0.0)
	Hide object: units/payday2/architecture/res_ext_apartment_rooms_4windows_03_piece/007 (1650.0, 4450.0, 0.0)
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/010 (1650.0, 3600.0, 0.0)
	´addObstacle_9´
´logic_link_018´ ElementDisableUnit 100125
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/007 (1650.0, 3600.0, 0.0)
	Hide object: units/payday2/architecture/res_ext_apartment_rooms_4windows_02_piece/004 (1650.0, 3550.0, 0.0)
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/007 (1650.0, 4500.0, 0.0)
	´addObstacle_10´
´logic_link_021´ ElementDisableUnit 100135
	Hide object: units/payday2/architecture/res_ext_apartment_rooms_4windows_01_piece/001 (2200.0, 3125.0, 0.0)
´nope_2´ MissionScriptElement 100157
	Execute sequence: "closed" - units/payday2/architecture/res_ext_apartment_wardrobe_variation/001 (2650.0, 3150.0, 0.0)
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/014 (2250.0, 3125.0, 0.0)
	´addObstacle_11´
´nope_3´ ElementDisableUnit 100143
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/006 (1650.0, 2700.0, 0.0)
	´addObstacle_12´
´logic_link_020´ ElementDisableUnit 100144
	Hide object: units/payday2/architecture/res_ext_apartment_rooms_4windows_01_piece/007 (1650.0, 2650.0, 0.0)
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_01/008 (1650.0, 2700.0, 0.0)
´nope_4´ MissionScriptElement 100132
	Execute sequence: "open" - units/payday2/architecture/res_ext_apartment_wardrobe_variation/007 (2650.0, 4050.0, 0.0)
	Hide object: units/payday2/architecture/res_ext_apartment_corridor_wall_piece_broken/013 (2250.0, 4025.0, 0.0)
	´addObstacle_13´
´logic_link_019´ ElementDisableUnit 100133
	Hide object: units/payday2/architecture/res_ext_apartment_rooms_4windows_02_piece/002 (2200.0, 4025.0, 0.0)
´logic_link_022´ ElementDisableUnit 100155
	Hide object: units/payday2/architecture/ext_apartment/res_apartment_blockade_01/002 (1475.0, 2700.0, 0.0)
	´addObstacle_14´
´start_new_obj´ ElementInstanceOutput 100020
	Send instance event "cable_door_seen" to mission.
´point_waypoint_001´ ElementWaypoint 100067
	Place waypoint with icon "pd2_door" at position (-449.0, 3893.0, 125.0)
´instance_output_old_obj´ ElementInstanceOutput 100051
	DISABLED
	Send instance event "cable_door_opened" to mission.
´state_remove_cover001´ ElementUnitSequence 100200
	DISABLED
	Execute sequence: "hud_text_remove_cover" - units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/002 (-1684.0, 4499.0, 100.0)
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/002 (-1684.0, 4499.0, 100.0)
´state_cut_cable001´ ElementUnitSequence 100207
	DISABLED
	Execute sequence: "hud_text_cut_cable" - units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/002 (-1684.0, 4499.0, 100.0)
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/002 (-1684.0, 4499.0, 100.0)
´state_remove_cover002´ ElementUnitSequence 100203
	DISABLED
	Execute sequence: "hud_text_remove_cover" - units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/003 (-1684.0, 3600.0, 100.0)
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/003 (-1684.0, 3600.0, 100.0)
´state_cut_cable002´ ElementUnitSequence 100214
	DISABLED
	Execute sequence: "hud_text_cut_cable" - units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/003 (-1684.0, 3600.0, 100.0)
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/003 (-1684.0, 3600.0, 100.0)
´state_remove_cover003´ ElementUnitSequence 100204
	DISABLED
	Execute sequence: "hud_text_remove_cover" - units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/004 (-1684.0, 2700.0, 100.0)
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/004 (-1684.0, 2700.0, 100.0)
´state_cut_cable003´ ElementUnitSequence 100215
	DISABLED
	Execute sequence: "hud_text_cut_cable" - units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/004 (-1684.0, 2700.0, 100.0)
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/004 (-1684.0, 2700.0, 100.0)
´addObstacle_7´ ElementNavObstacle 100153
	TRIGGER TIMES 1
	Add nav obstacle: units/dev_tools/level_tools/door_blocker_4x1/011 (1450.0, 2725.0, 0.0)
´addObstacle_8´ ElementNavObstacle 100120
	TRIGGER TIMES 1
	Add nav obstacle: units/dev_tools/level_tools/door_blocker_4x1/008 (1225.0, 3000.0, 0.0)
´addObstacle_9´ ElementNavObstacle 100128
	TRIGGER TIMES 1
	Add nav obstacle: units/dev_tools/level_tools/door_blocker_4x1/017 (1675.0, 3800.0, 0.0)
´addObstacle_10´ ElementNavObstacle 100129
	TRIGGER TIMES 1
	Add nav obstacle: units/dev_tools/level_tools/door_blocker_4x1/016 (1675.0, 4700.0, 0.0)
´addObstacle_11´ ElementNavObstacle 100140
	TRIGGER TIMES 1
	Add nav obstacle: units/dev_tools/level_tools/door_blocker_4x1/009 (2350.0, 3150.0, 0.0)
´addObstacle_12´ ElementNavObstacle 100148
	TRIGGER TIMES 1
	Add nav obstacle: units/dev_tools/level_tools/door_blocker_4x1/010 (1650.0, 2900.0, 0.0)
´addObstacle_13´ ElementNavObstacle 100141
	TRIGGER TIMES 1
	Add nav obstacle: units/dev_tools/level_tools/door_blocker_4x1/006 (2350.0, 4050.0, 0.0)
´addObstacle_14´ ElementNavObstacle 100156
	TRIGGER TIMES 1
	Add nav obstacle: units/payday2/architecture/ext_apartment/res_apartment_blockade_01/001 (1475.0, 4150.0, -9.00006)
´open_circuit_breaker´ ElementUnitSequenceTrigger 100190
	Upon any sequence:
		"interact" - units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/002 (-1684.0, 4499.0, 100.0)
		"interact" - units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/003 (-1684.0, 3600.0, 100.0)
		"interact" - units/pd2_dlc_miami/props/ind_prop_mechanical_circuitbreaker/004 (-1684.0, 2700.0, 100.0)
	Execute:
		´interacted_3_times（3）´
		If ´interacted_3_times（3）´ == 1: (DELAY 0.5)
			´state_cut_cable001´
			´state_cut_cable002´
			´state_cut_cable003´
		If ´interacted_3_times（3）´ == 2: (DELAY 0.5)
			´state_remove_cover001´
			´state_remove_cover002´
			´state_remove_cover003´
		If ´interacted_3_times（3）´ == 0: (DELAY 0.5)
			´open_door´
´interacted_3_times（3）´ ElementCounter 100192
´open_door´ MissionScriptElement 100095
	TRIGGER TIMES 1
	Remove nav obstacle: units/dev_tools/level_tools/door_blocker_1m/001 (-450.0, 3894.0, 0.0)
	remove ´point_waypoint_001´
	Award experience: 2000
	Debug message: + 2000
	´instance_output_old_obj´
	´seq_open_door´
	´Bain:_"The_door_is_open!_Good_jerb."´
	Toggle off: ´area_door_seen001´ (DELAY 0.01)
	Toggle off: ´start_new_obj´ (DELAY 0.01)
	Toggle off: ´instance_output_old_obj´ (DELAY 0.01)
´seq_open_door´ ElementUnitSequence 100195
	TRIGGER TIMES 1
	Execute sequence: "anim_door_open" - units/pd2_dlc_miami/props/gen_prop_door_metal_magnetic/001 (-450.0, 4100.0, 2.28882e-05)
