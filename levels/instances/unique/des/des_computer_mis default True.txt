﻿´startup´ ElementUnitSequence 100029
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Execute sequence: "state_interaction_disabled" - units/pd2_dlc_des/equipment/des_interactable_hack_computer/001 (0.0, -2.38418e-05, 80.0)
´on_interact_trigger´ ElementUnitSequenceTrigger 100009
	Upon sequence "interact" - units/pd2_dlc_des/equipment/des_interactable_hack_computer/001 (0.0, -2.38418e-05, 80.0):
		Send instance event "hacking_started" to mission.
		remove ´interract_wp´
		remove ´fix_wp´
		Start timer: ´hack_timer（30）´
		´defend_wp´
		´logic_toggle_001´
		´start_timer´
		´toggle_interupt_on´ (DELAY 5)
´toggle_interupt_on´ ElementToggle 100018
	Toggle on: ´interupt_action_none´
	Toggle on: ´interupt_action´
	Debug message: toggle_interupt_on
	´interupt_action_none´ (DELAY 1)
´interupt_action_none´ ElementSpecialObjective 100017
	DISABLED
	Create objective for enemies in 1500 radius around (0.0, 100.0, 2.5)
	Make chosen person path towards objective and show "None". Afterwards, choose new objective: ´interupt_action´
´interupt_action´ ElementSpecialObjective 100031
	DISABLED
	Make instigator path towards (0.0, 100.0, 102.5) and show "e_so_interact_mid".
´interract_wp´ ElementWaypoint 100000
	Place waypoint with icon "pd2_generic_interact" at position (0.0, 32.0, 117.967)
´defend_wp´ ElementWaypoint 100025
	Place waypoint with icon "pd2_defend" at position (0.0, 32.0, 117.967)
´fix_wp´ ElementWaypoint 100026
	Place waypoint with icon "pd2_fix" at position (0.0, 32.0, 117.967)
´logic_toggle_001´ ElementToggle 100045
	Toggle on, trigger times 1: ´kill_power´
	Toggle on, trigger times 1: ´kill_power_interupt´
	´logic_toggle_002´
	´logic_toggle_003´
´kill_power´ MissionScriptElement 100034
	DISABLED
	´kill_power_interupt´
	´toggle_interupt_off´
´kill_power_interupt´ ElementUnitSequence 100037
	DISABLED
	Execute sequence: "power_off" - units/pd2_dlc_des/equipment/des_interactable_hack_computer/001 (0.0, -2.38418e-05, 80.0)
	Execute sequence: "state_interaction_disabled" - units/pd2_dlc_des/equipment/des_interactable_hack_computer/001 (0.0, -2.38418e-05, 80.0)
	Execute sequence: "state_power_off" - units/pd2_dlc_des/equipment/des_interactable_hack_computer/001 (0.0, -2.38418e-05, 80.0)
	´remove_defend_wp001´
´start_timer´ ElementTimerOperator 100068
	TRIGGER TIMES 1
	Start timer: ´hack_timer（30）´
´hack_timer（30）´ ElementTimer 100067
	Timer: 30
´logic_toggle_002´ ElementToggle 100047
	Toggle off: ´kill_power_before_hack´
´kill_power_before_hack´ ElementUnitSequence 100046
	Execute sequence: "state_interaction_disabled" - units/pd2_dlc_des/equipment/des_interactable_hack_computer/001 (0.0, -2.38418e-05, 80.0)
´logic_toggle_003´ ElementToggle 100048
	Toggle on: ´show_defend_wp´
	Toggle on: ´restore_power_resume´
´show_defend_wp´ MissionScriptElement 100062
	DISABLED
	´defend_wp´
´restore_power_resume´ ElementUnitSequence 100038
	Execute sequence: "power_on" - units/pd2_dlc_des/equipment/des_interactable_hack_computer/001 (0.0, -2.38418e-05, 80.0)
	Execute sequence: "state_interaction_disabled" - units/pd2_dlc_des/equipment/des_interactable_hack_computer/001 (0.0, -2.38418e-05, 80.0)
	´show_defend_wp´
´toggle_interupt_off´ ElementToggle 100022
	Toggle off: ´interupt_action_none´
	Toggle off: ´interupt_action´
	remove ´interupt_action_none´
	remove ´interupt_action´
´remove_defend_wp001´ ElementOperator 100044
	remove ´defend_wp´
´enable_interaction_input´ ElementInstanceInput 100011
	Upon mission event "enable_interaction":
		´enable_interaction´
		´interract_wp´
´enable_interaction´ ElementUnitSequence 100010
	Execute sequence: "set_hack_time_state_4" - units/pd2_dlc_des/equipment/des_interactable_hack_computer/001 (0.0, -2.38418e-05, 80.0)
	Execute sequence: "state_interaction_enabled" - units/pd2_dlc_des/equipment/des_interactable_hack_computer/001 (0.0, -2.38418e-05, 80.0)
´disable_interaction_input´ ElementInstanceInput 100012
	Upon mission event "disable_interaction":
		´disable_interaction´
´disable_interaction´ ElementUnitSequence 100016
	Execute sequence: "state_interaction_disabled" - units/pd2_dlc_des/equipment/des_interactable_hack_computer/001 (0.0, -2.38418e-05, 80.0)
´disable_interaction_final_input´ ElementInstanceInput 100013
	Upon mission event "disable_interaction_final":
		Toggle on: ´enable_interaction´
		´disable_interaction´
´on_interupt´ ElementSpecialObjectiveTrigger 100019
	Upon special objective event "complete" - ´interupt_action´:
		´jammed´
´jammed´ ElementUnitSequence 100020
	Execute sequence: "state_device_jammed" - units/pd2_dlc_des/equipment/des_interactable_hack_computer/001 (0.0, -2.38418e-05, 80.0)
	remove ´defend_wp´
	Send instance event "hacking_jammed" to mission.
	Pause timer: ´hack_timer（30）´
	´toggle_interupt_off´
	´fix_wp´
´on_complete´ ElementUnitSequenceTrigger 100023
	TRIGGER TIMES 1
	Upon sequence "timer_done" - units/pd2_dlc_des/equipment/des_interactable_hack_computer/001 (0.0, -2.38418e-05, 80.0):
		Send instance event "hacking_complete" to mission.
		remove ´defend_wp´
		remove ´interupt_action_none´
		remove ´interupt_action´
		Toggle off: ´interupt_action_none´
		Toggle off: ´interupt_action´
		Toggle off: ´enable_interaction´
		Toggle off: ´disable_interaction´
		Toggle off: ´jammed´
		Toggle off: ´toggle_interupt_off´
		Toggle off: ´toggle_interupt_on´
		Toggle off: ´on_interupt´
		Toggle off: ´restore_power_resume´
		Toggle off: ´kill_power_interupt´
		Toggle off: ´kill_power´
		Toggle off: ´logic_toggle_001´
		Toggle off: ´logic_toggle_003´
		Toggle off: ´logic_toggle_002´
		Toggle off: ´kill_power_before_hack´
		Toggle off: ´remove_defend_wp001´
		Toggle off: ´fail_computer_interrupt´
		Execute sequence: "state_interaction_disabled" - units/pd2_dlc_des/equipment/des_interactable_hack_computer/001 (0.0, -2.38418e-05, 80.0)
		Toggle off: ´interupt_action_none´ (DELAY 0.5)
		Toggle off: ´interupt_action´ (DELAY 0.5)
		Toggle off: ´enable_interaction´ (DELAY 0.5)
		Toggle off: ´disable_interaction´ (DELAY 0.5)
		Toggle off: ´jammed´ (DELAY 0.5)
		Toggle off: ´toggle_interupt_off´ (DELAY 0.5)
		Toggle off: ´toggle_interupt_on´ (DELAY 0.5)
		Toggle off: ´on_interupt´ (DELAY 0.5)
		Toggle off: ´restore_power_resume´ (DELAY 0.5)
		Toggle off: ´kill_power_interupt´ (DELAY 0.5)
		Toggle off: ´kill_power´ (DELAY 0.5)
		Toggle off: ´logic_toggle_001´ (DELAY 0.5)
		Toggle off: ´logic_toggle_003´ (DELAY 0.5)
		Toggle off: ´logic_toggle_002´ (DELAY 0.5)
		Toggle off: ´kill_power_before_hack´ (DELAY 0.5)
		Toggle off: ´remove_defend_wp001´ (DELAY 0.5)
		Toggle off: ´fail_computer_interrupt´ (DELAY 0.5)
		Execute sequence: "state_interaction_disabled" - units/pd2_dlc_des/equipment/des_interactable_hack_computer/001 (0.0, -2.38418e-05, 80.0) (DELAY 0.5)
		´teammate_comment_computer_done´ (DELAY 0.5)
´fail_computer_interrupt´ ElementSpecialObjectiveTrigger 100060
	Upon any special objective event:
		"fail" - ´interupt_action_none´
		"fail" - ´interupt_action´
	Execute:
		Debug message: fail computer interrupt
		´toggle_interupt_on´ (DELAY 5)
´teammate_comment_computer_done´ ElementTeammateComment 100053
	TRIGGER TIMES 1
	Make teammate close to instigator say: v24
´jam_hacking_input´ ElementInstanceInput 100032
	Upon mission event "jam_hacking":
		´kill_power´
		´kill_power_interupt´
		´kill_power_before_hack´
´unjam_hacking_input´ ElementInstanceInput 100033
	Upon mission event "unjam_hacking":
		´restore_power_resume´
´5_sec_to_done´ ElementTimerTrigger 100071
	Upon timer "´hack_timer（30）´ == 5:
		Toggle off: ´jammed´
		remove ´interupt_action_none´
		remove ´interupt_action´
