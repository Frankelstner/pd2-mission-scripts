﻿´startup´ MissionScriptElement 100069
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Execute sequence: "state_close_door" - units/world/props/bank/security_door_02/001 (-1815.5, 1000.64, 1.0)
	Execute sequence: "state_close_door" - units/world/props/bank/security_door_02/002 (-1815.5, 1400.64, 1.0)
	Hide object: core/units/light_spot/001 (-1768.5, 1214.64, 55.0)
´lookat_spooky_man´ ElementLookAtTrigger 100074
	DISABLED
	TRIGGER TIMES 1
	Upon player looking in direction (0.0, 0.0, 0.707107, -0.707107) from position (-1825.0, 1261.0, 175.0):
		´looked_at´
´looked_at´ MissionScriptElement 100076
	TRIGGER TIMES 1
	Debug message: looked_at
	Execute sequence: "state_video_start" - units/pd2_dlc_des/props/des_prop_movie_screen/001 (-2515.0, 1424.0, 300.0)
	Make instigator say: g29 (DELAY 2)
	Debug message: start_leave (DELAY 15-18)
	´exit´ (DELAY 15-18)
	Execute sequence: "state_video_stop" - units/pd2_dlc_des/props/des_prop_movie_screen/001 (-2515.0, 1424.0, 300.0) (DELAY 18-21)
	Execute sequence: "lamp_off" - units/pd2_dlc_des/props/des_prop_long_lamp/001 (-2149.5, 1160.64, 326.0) (DELAY 25-28)
	Execute sequence: "lamp_off" - units/pd2_dlc_des/props/des_prop_long_lamp/002 (-2149.5, 1360.64, 326.0) (DELAY 25-28)
	Execute sequence: "lamp_off" - units/pd2_dlc_des/props/des_prop_long_lamp/003 (-2149.5, 1560.64, 326.0) (DELAY 25-28)
´exit´ ElementSpecialObjective 100067
	Create objective for someone from group: ´civ_male_des´
	Make person path towards (-1903.0, 1187.0, 13.0) and show "so_des_walk_exit".
´civ_male_des´ ElementSpawnCivilian 100085
	Spawn civ: civ_male_des (-2450.0, 850.0, 0.0)
	Team: criminal1
´area_player_infront_of_room´ ElementAreaTrigger 100082
	DISABLED
	TRIGGER TIMES 1
	Box, width 1000.0, height 500.0, depth 1000.0, at position (-1378.0, 1312.0, 13.0) with rotation (0.0, 0.0, -3.27826e-07, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Toggle on: ´lookat_spooky_man´
´input_start_event´ ElementInstanceInput 100091
	Upon mission event "start_event":
		Toggle on: ´area_player_in_snake_tunnel´
´area_player_in_snake_tunnel´ ElementAreaTrigger 100093
	DISABLED
	TRIGGER TIMES 1
	Box, width 3000.0, height 500.0, depth 1400.0, at position (-1012.0, 1067.0, 13.0) with rotation (0.0, 0.0, 0.182235, -0.983255)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Debug message: from instance: start_event
		Execute sequence: "open_door_slow" - units/world/props/bank/security_door_02/001 (-1815.5, 1000.64, 1.0) (DELAY 2)
		Execute sequence: "open_door_slow" - units/world/props/bank/security_door_02/002 (-1815.5, 1400.64, 1.0) (DELAY 2)
		Play audio "des_shutters" at position (-1825.0, 1250.0, 100.0) (DELAY 2)
		´civ_male_des´ (DELAY 2)
		Play audio "des_shutters" at position (-1825.0, 1550.0, 100.0) (DELAY 2.05)
		´enter´ (DELAY 2.1)
		Execute sequence: "anim_light_turn_on" - units/pd2_dlc_des/props/des_prop_long_lamp/001 (-2149.5, 1160.64, 326.0) (DELAY 3)
		Execute sequence: "anim_light_turn_on" - units/pd2_dlc_des/props/des_prop_long_lamp/002 (-2149.5, 1360.64, 326.0) (DELAY 3)
		Execute sequence: "anim_light_turn_on" - units/pd2_dlc_des/props/des_prop_long_lamp/003 (-2149.5, 1560.64, 326.0) (DELAY 3)
		Play audio "des_lights_flicker" at position (-2086.0, 1163.0, 290.0) (DELAY 3)
		Play audio "des_lights_flicker" at position (-2086.0, 1363.0, 290.0) (DELAY 3.1)
		Play audio "des_lights_flicker" at position (-2086.0, 1563.0, 290.0) (DELAY 3.3)
		Execute sequence: "anim_light_turn_on" - units/pd2_dlc_des/props/des_prop_long_lamp/001 (-2149.5, 1160.64, 326.0) (DELAY 4.5)
		Execute sequence: "anim_light_turn_on" - units/pd2_dlc_des/props/des_prop_long_lamp/002 (-2149.5, 1360.64, 326.0) (DELAY 4.5)
		Execute sequence: "anim_light_turn_on" - units/pd2_dlc_des/props/des_prop_long_lamp/003 (-2149.5, 1560.64, 326.0) (DELAY 4.5)
		Toggle on: ´area_player_infront_of_room´ (DELAY 8)
		Show object: core/units/light_spot/001 (-1768.5, 1214.64, 55.0) (DELAY 8)
´enter´ ElementSpecialObjective 100066
	Create objective for someone from group: ´civ_male_des´
	Make person path towards (-2450.0, 850.0, 0.0) and show "so_des_walk_enter".
´assault_end´ ElementGlobalEventTrigger 100010
	Upon global event "end_assault":
		Make teammate close to instigator say: g50 (DELAY 25-30)
		Debug message: teammate_comment_get_ready (DELAY 25-30)
´assault_start´ ElementGlobalEventTrigger 100101
	Upon global event "start_assault":
		Make teammate close to instigator say: g68 (DELAY 40-50)
		Debug message: teammate_comment_they_are_everywhere (DELAY 40-50)
		Make teammate close to instigator say: g16 (DELAY 55-70)
		Debug message: teammate_comment_fight_back (DELAY 55-70)
´area_player_spawned´ ElementAreaTrigger 100103
	TRIGGER TIMES 1
	Box, width 500000.0, height 50000.0, depth 500000.0, at position (-1500.0, 2200.0, 0.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Make teammate close to instigator say: g67 (DELAY 3)
		Debug message: teammate_comment_wait (DELAY 3)
		Make teammate close to instigator say: p47 (DELAY 9.5)
		Debug message: teammate_comment_lets_get_it_on (DELAY 9.5)
