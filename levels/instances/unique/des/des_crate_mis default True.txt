﻿´show_crate_input´ ElementInstanceInput 100016
	Upon mission event "show_crate":
		´show_crate´
´show_crate´ ElementUnitSequence 100013
	TRIGGER TIMES 1
	Execute sequence: "show" - units/pd2_dlc_des/props/des_prop_crane_crate/001 (0.0, 10.0, -0.500002)
	Execute sequence: "enable_interaction" - units/pd2_dlc_des/props/des_prop_crane_crate/001 (0.0, 10.0, -0.500002) (DELAY 1)
´loot_collected´ ElementUnitSequenceTrigger 100031
	Upon any sequence:
		"interact" - units/pd2_dlc_des/props/des_prop_unknown/001 (0.0, -7.0, 56.5)
		"interact" - units/pd2_dlc_des/props/des_prop_unknown_02/001 (0.0, -7.0, 56.5)
	Execute:
		Send instance event "loot_collected" to mission.
		remove ´loot_wp´
´loot_wp´ ElementWaypoint 100007
	Place waypoint with icon "pd2_loot" at position (0.0, -15.0, 113.0)
´crowbared_trigger´ ElementUnitSequenceTrigger 100002
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_des/props/des_prop_crane_crate/001 (0.0, 10.0, -0.500002):
		Send instance event "opened" to mission.
		´enable_unknown_loot_001´
		´enable_unknown_loot_002´
		Make teammate close to instigator say: v04 (DELAY 1)
		Debug message: from crate instance: teammate_comment_found_it (DELAY 1)
´enable_unknown_loot_001´ ElementUnitSequence 100027
	DISABLED
	Execute sequence: "show" - units/pd2_dlc_des/props/des_prop_unknown/001 (0.0, -7.0, 56.5)
	´enable_unknown_loot_004´ (DELAY 1)
´enable_unknown_loot_002´ ElementUnitSequence 100011
	DISABLED
	Execute sequence: "show" - units/pd2_dlc_des/props/des_prop_unknown_02/001 (0.0, -7.0, 56.5)
	´enable_unknown_loot_003´ (DELAY 1)
´enable_unknown_loot_004´ ElementUnitSequence 100015
	DISABLED
	Execute sequence: "enable_interaction" - units/pd2_dlc_des/props/des_prop_unknown/001 (0.0, -7.0, 56.5)
	´loot_wp´
´enable_unknown_loot_003´ ElementUnitSequence 100008
	DISABLED
	Execute sequence: "enable_interaction" - units/pd2_dlc_des/props/des_prop_unknown_02/001 (0.0, -7.0, 56.5)
	´loot_wp´
´set_prop_unknown_input_001´ ElementInstanceInput 100003
	Upon mission event "set_prop_unknown_001":
		Toggle on: ´enable_unknown_loot_001´
		Toggle on: ´enable_unknown_loot_004´
´set_prop_unknown_input_002´ ElementInstanceInput 100006
	Upon mission event "set_prop_unknown_002":
		Toggle on: ´enable_unknown_loot_002´
		Toggle on: ´enable_unknown_loot_003´
