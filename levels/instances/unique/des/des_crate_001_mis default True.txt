﻿´hide_all´ ElementUnitSequence 100009
	EXECUTE ON STARTUP
	Execute sequence: "hide" - units/pd2_dlc_des/props/des_prop_unknown/002 (0.0, -19.0, 57.0)
	Execute sequence: "hide" - units/pd2_dlc_des/props/des_prop_unknown_02/003 (0.0, -19.0, 57.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_des/props/des_prop_unknown/002 (0.0, -19.0, 57.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_des/props/des_prop_unknown_02/003 (0.0, -19.0, 57.0)
´set_input_unknown_001´ ElementInstanceInput 100000
	Upon mission event "set_prop_unknown_001":
		Execute sequence: "show" - units/pd2_dlc_des/props/des_prop_unknown/002 (0.0, -19.0, 57.0)
		Toggle on: ´set_prop_unknown_004´
´set_prop_unknown_004´ ElementUnitSequence 100018
	DISABLED
	Execute sequence: "enable_interaction" - units/pd2_dlc_des/props/des_prop_unknown/002 (0.0, -19.0, 57.0)
´set_input_unknown_002´ ElementInstanceInput 100001
	Upon mission event "set_prop_unknown_002":
		Execute sequence: "show" - units/pd2_dlc_des/props/des_prop_unknown_02/003 (0.0, -19.0, 57.0)
		Toggle on: ´set_prop_unknown_003´
´set_prop_unknown_003´ ElementUnitSequence 100017
	DISABLED
	Execute sequence: "enable_interaction" - units/pd2_dlc_des/props/des_prop_unknown_02/003 (0.0, -19.0, 57.0)
´show_wp´ ElementInstanceInput 100006
	Upon mission event "show_wp":
		Execute sequence: "enable_interaction" - units/pd2_dlc_des/props/des_prop_crane_crate_no_hooks/002 (0.0, 0.0, 0.0)
		´interact_wp´
´interact_wp´ ElementWaypoint 100004
	Place waypoint with icon "pd2_generic_interact" at position (0.0, -3.0, 135.0)
´crate_opened´ ElementUnitSequenceTrigger 100008
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_des/props/des_prop_crane_crate_no_hooks/002 (0.0, 0.0, 0.0):
		remove ´interact_wp´
		Send instance event "crate_opened" to mission.
		If 10% chance to execute succeeds: (DELAY 0.2)
			Play audio "bats_fly_away" at position (0.0, 7.78723, 115.094)
			Play effect effects/payday2/particles/explosions/des_bats_02 at position (0.0, -6.21277, 88.0942)
		´loot_wp´ (DELAY 1)
		´set_prop_unknown_003´ (DELAY 1)
		´set_prop_unknown_004´ (DELAY 1)
´loot_wp´ ElementWaypoint 100005
	Place waypoint with icon "pd2_loot" at position (0.0, -20.0, 132.0)
´loot_taken´ ElementUnitSequenceTrigger 100014
	TRIGGER TIMES 1
	Upon any sequence:
		"interact" - units/pd2_dlc_des/props/des_prop_unknown/002 (0.0, -19.0, 57.0)
		"interact" - units/pd2_dlc_des/props/des_prop_unknown_02/003 (0.0, -19.0, 57.0)
	Execute:
		remove ´loot_wp´
		Send instance event "loot_taken" to mission.
