ID range vs continent name:
	100000: world

statics
	100008 units/payday2/equipment/gen_interactable_spawn_contraband_crate/001 (100.0, 0.0, 7.62939e-06)
		mesh_variation state_requires_crowbar
	100001 units/payday2/equipment/hlm_interactable_bar_code_downtown/001 (39.0, -18.0, 28.0)
	100009 units/payday2/equipment/hlm_interactable_bar_code_foggy_bottom/001 (38.0, 24.0, 28.0)
	100006 units/payday2/equipment/hlm_interactable_bar_code_georgetown/001 (160.0, -16.0, 28.0)
	100002 units/payday2/equipment/hlm_interactable_bar_code_shaw/001 (110.0, 52.0, 28.0)
	100003 units/payday2/equipment/hlm_interactable_bar_code_westend/001 (84.0, 52.0, 28.0)
	100042 units/payday2/pickups/gen_pku_methlab_caustic_soda/001 (59.0, 18.0, 25.0)
	100043 units/payday2/pickups/gen_pku_methlab_hydrogen_cloride/001 (55.0, -20.0, 26.0)
	100041 units/payday2/pickups/gen_pku_methlab_muriatic_acid/001 (92.0, -1.0, 23.0)
