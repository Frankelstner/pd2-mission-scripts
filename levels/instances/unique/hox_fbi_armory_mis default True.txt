﻿´opened_armoury_door´ ElementUnitSequenceTrigger 100021
	TRIGGER TIMES 1
	Upon sequence "done_opened" - units/pd2_dlc2/architecture/gov_d_int_door_b/001 (-882.0, -27.0, 0.0):
		Send instance event "allow_armoury_graph" to mission.
		Play dialogue: Play_pln_hb2_14
		Play audio "Play_sys_hb2_07" at position (-900.0, -150.0, 100.0)
´used_keycard´ ElementUnitSequenceTrigger 100031
	TRIGGER TIMES 1
	Upon sequence "open_door_keycard" - units/pd2_dlc2/architecture/gov_d_int_door_b/001 (-882.0, -27.0, 0.0):
		Send instance event "used_keycard" to mission.
