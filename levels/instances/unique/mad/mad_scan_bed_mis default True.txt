﻿´start´ ElementUnitSequence 100056
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_mad/props/mad_prop_interactable_tablet_stand/001 (-40.6832, -168.0, 123.004)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_mad/props/mad_prop_compound_test_subject_04/001 (-15.5277, -4.37022, 107.004)
	Execute sequence: "state_interaction_disabled" - units/pd2_dlc_mad/props/mad_prop_compound_test_subject_04/001 (-15.5277, -4.37022, 107.004)
´Activate´ ElementInstanceInput 100001
	Upon mission event "Activate":
		If ´number_of_bodies_scanned（0）´ < 4:
			Toggle on, trigger times 1: ´trigger_area_001´
			Execute sequence: "show" - units/payday2/props/gen_prop_square_drop_marker_4x15/001 (8.0, 8.0, 98.0)
			´loot_drop_wp´
		If ´number_of_bodies_scanned（0）´ >= 4:
			Send instance event "All bodies scanned" to mission.
´number_of_bodies_scanned（0）´ ElementCounter 100037
´trigger_area_001´ ElementAreaTrigger 100006
	DISABLED
	TRIGGER TIMES 1
	Box, width 228.9, height 132.717, depth 123.6, at position (-4.0, -7.0, 146.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: unique_loot
	Interval: 0.1
	Upon on_enter, execute:
		Loot action: remove
		Debug message: HEY HEY
		Execute sequence: "hide" - units/payday2/props/gen_prop_square_drop_marker_4x15/001 (8.0, 8.0, 98.0)
		Execute sequence: "state_vis_show" - units/pd2_dlc_mad/props/mad_prop_compound_test_subject_04/001 (-15.5277, -4.37022, 107.004)
		Execute sequence: "state_interaction_disabled" - units/pd2_dlc_mad/props/mad_prop_compound_test_subject_04/001 (-15.5277, -4.37022, 107.004)
		Execute sequence: "enable_interaction" - units/pd2_dlc_mad/props/set/mad_prop_medical_carm/001 (-50.0, -25.0, 0.00400394)
		remove ´loot_drop_wp´
		Send instance event "Body Added" to mission.
		´computer_wp´
´loot_drop_wp´ ElementWaypoint 100012
	Place waypoint with icon "pd2_lootdrop" at position (0.0, 0.0, 209.694)
´computer_wp´ ElementWaypoint 100005
	Place waypoint with icon "pd2_computer" at position (-3.0, -79.0, 236.0)
´scan_timer（90）´ ElementTimer 100017
	Timer: 90
	Send instance event "Scan Completed" to mission.
	´number_of_bodies_scanned（0）´ += 1
	Toggle on: ´Scan_Started´
	Execute sequence: "state_vis_hide" - units/pd2_dlc_mad/props/mad_prop_compound_test_subject_04/001 (-15.5277, -4.37022, 107.004)
	Execute sequence: "reset_anim" - units/pd2_dlc_mad/props/set/mad_prop_medical_carm/001 (-50.0, -25.0, 0.00400394)
	Execute sequence: "paus_anim_x_ray" - units/pd2_dlc_mad/props/set/mad_prop_medical_carm/001 (-50.0, -25.0, 0.00400394)
	Debug message: Stop scan anim
	Make teammate close to instigator say: v08
	Debug message: teammate_comment_done
	Toggle off: ´point_special_objective_001´
	Toggle off: ´anim_completed´
	Toggle off: ´anim_failed´
	Toggle off: ´execute_turn_on_so´
	Toggle off: ´attempt_interrupt_so´
	remove ´point_special_objective_001´
	remove ´defend_wp´
	remove ´point_area_min_police_force_001´
	Execute sequence: "state_vis_hide" - units/pd2_dlc_mad/props/mad_prop_compound_test_subject_04/001 (-15.5277, -4.37022, 107.004)
	´scan_timer（90）´ = 90 (DELAY 1)
	If ´number_of_bodies_scanned（0）´ < 4: (DELAY 1)
		Toggle on, trigger times 1: ´trigger_area_001´
		Execute sequence: "show" - units/payday2/props/gen_prop_square_drop_marker_4x15/001 (8.0, 8.0, 98.0)
		´loot_drop_wp´
	If ´number_of_bodies_scanned（0）´ >= 4: (DELAY 1)
		Send instance event "All bodies scanned" to mission.
	´teammate_comment_that_enough?´ (DELAY 17)
´Scan_Started´ ElementInstanceOutput 100003
	Send instance event "Scan Started" to mission.
´teammate_comment_that_enough?´ ElementTeammateComment 100080
	TRIGGER TIMES 1
	Make teammate close to instigator say: v47
	Debug message: teammate_comment_that_enough?
´point_special_objective_001´ ElementSpecialObjective 100043
	TRIGGER TIMES 1
	Create objective for enemies in 3750 radius around (0.0, -100.0, 0.00400543)
	Make chosen person path towards objective and show "e_so_push_button_low".
´anim_completed´ ElementSpecialObjectiveTrigger 100047
	DISABLED
	TRIGGER TIMES 1
	Upon special objective event "complete" - ´point_special_objective_001´:
		Debug message: interrupted
		Send instance event "Scan interrupted" to mission.
		Execute sequence: "enable_interaction" - units/pd2_dlc_mad/props/set/mad_prop_medical_carm/001 (-50.0, -25.0, 0.00400394)
		Toggle off: ´Scan_Started´
		Pause timer: ´scan_timer（90）´
		Toggle off: ´point_special_objective_001´
		Toggle off: ´anim_completed´
		Toggle off: ´anim_failed´
		Toggle off: ´execute_turn_on_so´
		Toggle off: ´attempt_interrupt_so´
		remove ´point_special_objective_001´
		remove ´defend_wp´
		Execute sequence: "paus_anim_x_ray" - units/pd2_dlc_mad/props/set/mad_prop_medical_carm/001 (-50.0, -25.0, 0.00400394)
		Debug message: Stop scan anim
		´computer_wp´
		Make teammate close to instigator say: g29 (DELAY 2)
´anim_failed´ ElementSpecialObjectiveTrigger 100048
	DISABLED
	Upon special objective event "fail" - ´point_special_objective_001´:
		´attempt_interrupt_so´
´execute_turn_on_so´ ElementToggle 100020
	DISABLED
	Toggle on, trigger times 1: ´point_special_objective_001´
	´point_special_objective_001´ (DELAY 1)
´attempt_interrupt_so´ MissionScriptElement 100046
	´execute_turn_on_so´
´defend_wp´ ElementWaypoint 100074
	Place waypoint with icon "pd2_defend" at position (25.0, 0.0, 209.694)
´point_area_min_police_force_001´ ElementAreaMinPoliceForce 100052
	Reinforce task: 1 enemy at position (0.0, -98.0, 24.0)
´deactivate´ ElementInstanceInput 100032
	Upon mission event "deactivate scanner":
		Execute sequence: "hide" - units/payday2/props/gen_prop_square_drop_marker_4x15/001 (8.0, 8.0, 98.0)
		Toggle off: ´trigger_area_001´
		remove ´loot_drop_wp´
		remove ´computer_wp´
		Execute sequence: "disable_interaction" - units/pd2_dlc_mad/props/set/mad_prop_medical_carm/001 (-50.0, -25.0, 0.00400394)
´set_hand_state´ ElementInstanceInput 100062
	Upon mission event "Set state hand":
		Execute sequence: "state_vis_show" - units/pd2_dlc_mad/props/mad_prop_interactable_tablet_stand/001 (-40.6832, -168.0, 123.004) (DELAY 2)
		Execute sequence: "activate_tablet" - units/pd2_dlc_mad/props/mad_prop_interactable_tablet_stand/001 (-40.6832, -168.0, 123.004) (DELAY 2)
´activate_hand_wp´ ElementInstanceInput 100055
	Upon mission event "Activate hand wp":
		´generic_interact´
´generic_interact´ ElementWaypoint 100060
	Place waypoint with icon "pd2_generic_interact" at position (-25.0, -169.0, 156.694)
´external_interrupt´ ElementInstanceInput 100069
	Upon mission event "Stop scanning process":
		Pause timer: ´scan_timer（90）´
		Toggle off: ´point_special_objective_001´
		Toggle off: ´anim_completed´
		Toggle off: ´anim_failed´
		Toggle off: ´execute_turn_on_so´
		Toggle off: ´attempt_interrupt_so´
		remove ´point_special_objective_001´
		remove ´defend_wp´
		Execute sequence: "paus_anim_x_ray" - units/pd2_dlc_mad/props/set/mad_prop_medical_carm/001 (-50.0, -25.0, 0.00400394)
		Debug message: Stop scan anim
´start_resume´ ElementInstanceInput 100072
	Upon mission event "Activate restart":
		Execute sequence: "enable_interaction" - units/pd2_dlc_mad/props/set/mad_prop_medical_carm/001 (-50.0, -25.0, 0.00400394)
		Toggle off: ´Scan_Started´
		´computer_wp´
´started_scan´ ElementUnitSequenceTrigger 100022
	Upon sequence "interact" - units/pd2_dlc_mad/props/set/mad_prop_medical_carm/001 (-50.0, -25.0, 0.00400394):
		Start timer: ´scan_timer（90）´
		remove ´computer_wp´
		Toggle on: ´attempt_interrupt_so´
		Execute sequence: "start_anim_x_ray" - units/pd2_dlc_mad/props/set/mad_prop_medical_carm/001 (-50.0, -25.0, 0.00400394)
		Debug message: SHOULD START SCAN ANIM
		´Scan_Started´
		´point_area_min_police_force_001´
		´defend_wp´
		Toggle on: ´anim_completed´ (DELAY 5)
		Toggle on: ´anim_failed´ (DELAY 5)
		Toggle on: ´execute_turn_on_so´ (DELAY 5)
		´attempt_interrupt_so´ (DELAY 6)
´placed_hand´ ElementUnitSequenceTrigger 100054
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_mad/props/mad_prop_interactable_tablet_stand/001 (-40.6832, -168.0, 123.004):
		remove ´generic_interact´
		Send instance event "Placed hand" to mission.
		Execute sequence: "state_vis_hide" - units/pd2_dlc_mad/props/mad_prop_interactable_tablet_stand/001 (-40.6832, -168.0, 123.004)
		Execute sequence: "state_vis_tablet_show" - units/pd2_dlc_mad/props/set/mad_prop_medical_carm/001 (-50.0, -25.0, 0.00400394)
