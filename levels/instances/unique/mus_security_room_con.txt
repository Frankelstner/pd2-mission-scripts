ID range vs continent name:
	100000: world

statics
	100030 units/dev_tools/level_tools/door_blocker/001 (-7.62939e-06, -49.0, 50.0)
	100041 units/payday2/equipment/gen_interactable_hack_computer_b/001 (94.263, -39.9185, 79.0)
		mesh_variation state_interaction_disabled
	100028 units/payday2/props/gen_prop_security_console/001 (151.0, -64.0, 80.6646)
	100016 units/payday2/props/gen_prop_security_monitors_four_wall/001 (125.0, 1.52588e-05, 31.9112)
		mesh_variation office
	100014 units/payday2/props/off_prop_appliance_computer/001 (192.0, -49.0, 79.997)
	100015 units/payday2/props/off_prop_appliance_keyboard/001 (94.08, -66.0, 80.997)
	100013 units/pd2_indiana/props/desk_props/mus_prop_curator_lamp/001 (50.0, -50.0, 80.6646)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 1.0
			enabled True
			falloff_exponent 4
			far_range 200
			multiplier desklight
			name li_light
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
	100027 units/pd2_indiana/props/mus_prop_curator_desk/001 (125.0, -50.0, 0.0)
