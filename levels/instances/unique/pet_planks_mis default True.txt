﻿´startup´ ElementDisableUnit 100002
	EXECUTE ON STARTUP
	Hide object: units/pd2_dlc_peta/props/barricade/pta_prop_barn_planks_pile/001 (0.0, 15.0, 0.0)
´once_interacted´ ElementUnitSequenceTrigger 100004
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_peta/props/barricade/pta_prop_barn_planks_pile/001 (0.0, 15.0, 0.0):
		Give 1 planks to instigator.
´player_close´ ElementAreaTrigger 100006
	DISABLED
	TRIGGER TIMES 1
	Cylinder, radius 735.0, height 700.0, at position (0.0, 0.0, 77.5) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Execute sequence: "enable_interaction" - units/pd2_dlc_peta/props/barricade/pta_prop_barn_planks_pile/001 (0.0, 15.0, 0.0)
´enable_planks´ ElementInstanceInput 100011
	Upon mission event "enable_planks":
		Show object: units/pd2_dlc_peta/props/barricade/pta_prop_barn_planks_pile/001 (0.0, 15.0, 0.0)
		Toggle on: ´player_close´
