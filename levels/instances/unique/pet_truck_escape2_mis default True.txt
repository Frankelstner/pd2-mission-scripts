﻿´startup´ ElementDisableUnit 100003
	EXECUTE ON STARTUP
	Hide object: units/payday2/props/gen_prop_square_goal_marker_8x15/001 (5.0, 350.0, 116.704)
´enable_escape´ ElementInstanceInput 100006
	Upon mission event "enable_escape":
		Execute sequence: "anim_open" - units/pd2_dlc_peta/vehicles/anim_vehicle_truck_semi/001 (0.00012368, -321.0, 0.0)
		Place waypoint with icon "pd2_escape" at position (0.0, 350.0, 276.704)
´trigger_area_001´ ElementAreaTrigger 100000
	TRIGGER TIMES 1
	Box, width 318.5, height 325.0, depth 662.4, at position (0.0, -125.0, 282.204) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: vehicle
	Interval: 0.1
	Upon on_enter, execute:
		Hide object: units/payday2/props/gen_prop_car_goal_marker/001 (-1.90735e-05, 6.0, 116.704)
		Toggle on: ´trigger_area_002´
		Show object: units/payday2/props/gen_prop_square_goal_marker_8x15/001 (5.0, 350.0, 116.704)
		Send instance event "car_secured" to mission.
´trigger_area_002´ ElementAreaTrigger 100001
	DISABLED
	TRIGGER TIMES 1
	Box, width 328.25, height 468.0, depth 1168.63, at position (0.0, 97.0, 242.204) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: all
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Make teammate close to instigator say: g24
		´players_escaped_2_sec_delay´
´players_escaped_2_sec_delay´ ElementMissionEnd 100057
	BASE DELAY  (DELAY 2)
	TRIGGER TIMES 1
	End mission: success
	Debug message: MISSION COMPLETE!!!
