﻿´spawned´ ElementEnemyDummyTrigger 100009
	Upon any event:
		spawn: ´ene_swat_1´
		spawn: ´ene_swat_1_2´
		spawn: ´ene_swat_1_3´
		spawn: ´ene_swat_1_4´
		spawn: ´ene_swat_1_5´
		spawn: ´ene_swat_1_6´
		spawn: ´ene_swat_1_7´
		spawn: ´ene_swat_1_8´
	Execute:
		´number_alive（0）´ += 1
		If ´number_alive（0）´ == 0:
			´spawn_new_group_-_delayed´
´ene_swat_1´ ElementSpawnEnemyDummy 100005
	Spawn ene_swat_1 (-100.0, 9.15527e-05, 0.0) with action e_sp_up_ledge.
	No group AI
´ene_swat_1_2´ ElementSpawnEnemyDummy 100006
	Spawn ene_swat_1 (-250.0, 9.15527e-05, 0.0) with action e_sp_up_ledge.
	No group AI
´ene_swat_1_3´ ElementSpawnEnemyDummy 100004
	Spawn ene_swat_1 (100.0, 9.15527e-05, 0.0) with action e_sp_up_ledge.
	No group AI
´ene_swat_1_4´ ElementSpawnEnemyDummy 100007
	Spawn ene_swat_1 (250.0, 7.62939e-05, 0.0) with action e_sp_up_ledge.
	No group AI
´ene_swat_1_5´ ElementSpawnEnemyDummy 100032
	Spawn ene_swat_1 (25.0, 50.0, 0.0).
	No group AI
´ene_swat_1_6´ ElementSpawnEnemyDummy 100029
	Spawn ene_swat_1 (25.0, 0.0, 0.0).
	No group AI
´ene_swat_1_7´ ElementSpawnEnemyDummy 100030
	Spawn ene_swat_1 (-25.0, 0.0, 0.0).
	No group AI
´ene_swat_1_8´ ElementSpawnEnemyDummy 100031
	Spawn ene_swat_1 (-25.0, 50.0, 0.0).
	No group AI
´number_alive（0）´ ElementCounter 100011
´spawn_new_group_-_delayed´ MissionScriptElement 100016
	BASE DELAY  (DELAY 20-55)
	´over_wall´
	´off_highway´
´over_wall´ MissionScriptElement 100035
	DISABLED
	´ai_enemy_group：group_02（0）´
´off_highway´ MissionScriptElement 100034
	´ai_enemy_group：group_01（0）´
´ai_enemy_group：group_02（0）´ ElementSpawnEnemyGroup 100033
	Preferred spawn groups: tac_shield_wall_charge, FBI_spoocs, tac_tazer_charge, tac_shield_wall, tac_tazer_flanking, tac_swat_rifle_flank, tac_shield_wall_ranged, tac_bull_rush
	Debug message: spawned_group
	Spawn 0 enemies, chosen at random:
		´ene_swat_1_8´
		´ene_swat_1_7´
		´ene_swat_1_6´
		´ene_swat_1_5´
´ai_enemy_group：group_01（0）´ ElementSpawnEnemyGroup 100008
	Preferred spawn groups: tac_shield_wall_charge, FBI_spoocs, tac_tazer_charge, tac_tazer_flanking, tac_shield_wall, tac_swat_rifle_flank, tac_shield_wall_ranged, tac_bull_rush
	Debug message: spawned_group
	Spawn 0 enemies, chosen at random:
		´ene_swat_1_4´
		´ene_swat_1_3´
		´ene_swat_1´
		´ene_swat_1_2´
´died´ ElementEnemyDummyTrigger 100010
	Upon any event:
		death: ´ene_swat_1´
		death: ´ene_swat_1_2´
		death: ´ene_swat_1_3´
		death: ´ene_swat_1_4´
		death: ´ene_swat_1_8´
		death: ´ene_swat_1_7´
		death: ´ene_swat_1_6´
		death: ´ene_swat_1_5´
	Execute:
		´number_alive（0）´ -= 1
		If ´number_alive（0）´ == 0:
			´spawn_new_group_-_delayed´
´Start_-_resume´ ElementInstanceInput 100017
	Upon mission event "start_resume":
		´Off´
´Off´ ElementToggle 100028
	Toggle off: ´Off´
	Toggle on: ´ai_enemy_group：group_01（0）´
	Toggle on: ´spawn_new_group_-_delayed´
	Toggle on: ´ai_enemy_group：group_02（0）´
	´spawn_new_group_-_delayed´ (DELAY 0.25)
´cancel´ ElementInstanceInput 100018
	Upon mission event "cancel":
		Toggle off: ´spawn_new_group_-_delayed´
		Toggle off: ´ai_enemy_group：group_01（0）´
		Toggle off: ´ai_enemy_group：group_02（0）´
		Toggle on: ´Off´
		Debug message: cancel group spawns
´End´ ElementInstanceInput 100019
	Upon mission event "end":
		Toggle on: ´Off´
		Toggle on: ´spawn_new_group_-_delayed´
		Toggle on: ´ai_enemy_group：group_01（0）´
		Toggle on: ´ai_enemy_group：group_02（0）´
´use_group_02_over_wall´ ElementInstanceInput 100036
	Upon mission event "use_group_02_over_wall":
		Toggle on: ´over_wall´
		Toggle off: ´off_highway´
´kill_and_pause´ ElementInstanceInput 100001
	Upon mission event "kill_and_pause":
		Toggle off: ´spawn_new_group_-_delayed´
		Toggle off: ´ai_enemy_group：group_01（0）´
		Toggle off: ´ai_enemy_group：group_02（0）´
		Toggle on: ´Off´
		Debug message: cancel group spawns
		Kill NPC: ´ene_swat_1_2´
		Kill NPC: ´ene_swat_1_8´
		Kill NPC: ´ene_swat_1´
		Kill NPC: ´ene_swat_1_7´
		Kill NPC: ´ene_swat_1_6´
		Kill NPC: ´ene_swat_1_3´
		Kill NPC: ´ene_swat_1_5´
		Kill NPC: ´ene_swat_1_4´
