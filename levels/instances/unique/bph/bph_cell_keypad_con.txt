ID range vs continent name:
	100000: world

statics
	100228 units/pd2_dlc_bph/props/bph_prop_keypad/001 (-2.0, 0.0, 143.109)
	100000 units/pd2_dlc_bph/props/prop_keypad/bph_interactable_button/button000 (-5.0, -7.15254e-07, 136.989)
		mesh_variation set_button_0
	100229 units/pd2_dlc_bph/props/prop_keypad/bph_interactable_button/button001 (-5.0, 4.0, 148.989)
		mesh_variation set_button_01
	100236 units/pd2_dlc_bph/props/prop_keypad/bph_interactable_button/button002 (-5.0, -7.15254e-07, 148.989)
		mesh_variation set_button_02
	100237 units/pd2_dlc_bph/props/prop_keypad/bph_interactable_button/button003 (-5.0, -4.0, 148.989)
		mesh_variation set_button_03
	100238 units/pd2_dlc_bph/props/prop_keypad/bph_interactable_button/button004 (-5.0, 4.0, 144.989)
		mesh_variation set_button_04
	100239 units/pd2_dlc_bph/props/prop_keypad/bph_interactable_button/button005 (-5.0, -4.76837e-07, 144.989)
		mesh_variation set_button_05
	100240 units/pd2_dlc_bph/props/prop_keypad/bph_interactable_button/button006 (-5.0, -4.0, 144.989)
		mesh_variation set_button_06
	100241 units/pd2_dlc_bph/props/prop_keypad/bph_interactable_button/button007 (-5.0, 4.0, 140.989)
		mesh_variation set_button_07
	100242 units/pd2_dlc_bph/props/prop_keypad/bph_interactable_button/button008 (-5.0, -7.15254e-07, 140.989)
		mesh_variation set_button_08
	100243 units/pd2_dlc_bph/props/prop_keypad/bph_interactable_button/button009 (-5.0, -4.0, 140.989)
		mesh_variation set_button_09
	100244 units/pd2_dlc_bph/props/prop_keypad/bph_interactable_button/buttonC (-5.0, 4.0, 136.989)
		mesh_variation set_button_clear
	100245 units/pd2_dlc_bph/props/prop_keypad/bph_interactable_button/buttonE (-5.0, -4.0, 136.989)
		mesh_variation set_button_enter
