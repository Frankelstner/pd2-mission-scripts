ID range vs continent name:
	100000: world

statics
	100039 units/dev_tools/level_tools/dev_bag_collision_4x3m/collision_force_close (-200.0, 8.0, 0.0)
	100017 units/dev_tools/level_tools/dev_collision_1x3m/001 (200.0, -17.0, 0.0)
	100018 units/dev_tools/level_tools/dev_collision_1x3m/002 (150.0, -17.0, 0.0)
	100019 units/dev_tools/level_tools/dev_collision_1x3m/003 (-100.0, -17.0, 0.0)
	100020 units/dev_tools/level_tools/dev_collision_1x3m/004 (-50.0, -17.0, 0.0)
	100021 units/dev_tools/level_tools/dev_collision_1x3m/005 (150.0, -17.0, 310.0)
	100016 units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-50.0, -50.0, 0.0)
	100008 units/payday2/architecture/bnk_int_fence_door/001 (50.0, 0.0, 0.0)
		mesh_variation open_door
	100001 units/payday2/architecture/bnk_int_fence_gate/001 (100.0, 0.0, 0.0)
		disable_collision True
	100002 units/payday2/architecture/bnk_int_fence_wall_short/001 (200.0, 0.0, 0.0)
		disable_collision True
	100003 units/payday2/architecture/bnk_int_fence_wall_short/002 (-100.0, 0.0, 0.0)
		disable_collision True
