ID range vs continent name:
	100000: world

statics
	100008 units/dev_tools/level_tools/dev_ai_vis_blocker_005x2x2m/001 (100.0, 125.0, 1580.89)
	100010 units/dev_tools/level_tools/dev_ai_vis_blocker_005x2x2m/002 (100.0, -50.0, 1580.89)
	100011 units/dev_tools/level_tools/dev_ai_vis_blocker_005x2x2m/003 (100.0, -50.0, 1780.89)
	100012 units/dev_tools/level_tools/dev_ai_vis_blocker_005x2x2m/004 (100.0, -49.9999, 1580.89)
	100027 units/dev_tools/level_tools/dev_ai_vis_blocker_005x2x2m/006 (200.0, -50.0, 1580.89)
	100028 units/dev_tools/level_tools/dev_ai_vis_blocker_005x2x2m/007 (200.0, -50.0, 1780.89)
	100029 units/dev_tools/level_tools/dev_ai_vis_blocker_005x2x2m/008 (200.0, 125.0, 1580.89)
	100030 units/dev_tools/level_tools/dev_ai_vis_blocker_005x2x2m/left_blocker (-100.0, 150.0, 1380.89)
	100013 units/dev_tools/level_tools/dev_ai_vis_blocker_005x2x2m/right_blocker (200.0, 150.0, 1380.89)
	100003 units/payday2/architecture/mkp_int_floor_2x2m_a/001 (-85.0, -63.0, 1600.5)
	100018 units/payday2/architecture/mkp_int_floor_2x2m_a/002 (-85.0, -63.0, 1600.5)
	100024 units/payday2/architecture/mkp_int_floor_2x2m_a/003 (-10.0, -63.0, 1600.5)
	100009 units/payday2/vehicles/air_vehicle_blackhawk/vehicle_blackhawk/001 (50.0, 75.0, -250.0)
		disable_on_ai_graph True
		mesh_variation hover_idle
