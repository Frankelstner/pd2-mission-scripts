ID range vs continent name:
	100000: world

statics
	100038 core/units/light_omni/001 (0.0, 2101.0, 229.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.968628, 0.368627, 0.368627
			enabled True
			falloff_exponent 1
			far_range 400
			multiplier searchlight
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100039 core/units/light_omni/002 (203.0, 1501.0, 232.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.968628, 0.368627, 0.368627
			enabled True
			falloff_exponent 1
			far_range 400
			multiplier reddot
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100040 core/units/light_omni/003 (-204.0, 1501.0, 232.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.968628, 0.368627, 0.368627
			enabled True
			falloff_exponent 1
			far_range 400
			multiplier reddot
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100041 core/units/light_omni/004 (-204.0, 501.0, 232.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.968628, 0.368627, 0.368627
			enabled True
			falloff_exponent 1
			far_range 400
			multiplier reddot
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100042 core/units/light_omni/005 (203.0, 501.0, 232.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.968628, 0.368627, 0.368627
			enabled True
			falloff_exponent 1
			far_range 400
			multiplier reddot
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100043 core/units/light_omni/006 (0.0, -90.0, 229.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.968628, 0.368627, 0.368627
			enabled True
			falloff_exponent 1
			far_range 400
			multiplier searchlight
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100032 units/pd2_dlc_chew/props/chw_prop_ext_train_lamp/001 (275.0, 1500.0, 229.119)
	100033 units/pd2_dlc_chew/props/chw_prop_ext_train_lamp/002 (-271.0, 1518.0, 229.119)
	100034 units/pd2_dlc_chew/props/chw_prop_ext_train_lamp/003 (-271.0, 510.0, 229.119)
	100035 units/pd2_dlc_chew/props/chw_prop_ext_train_lamp/004 (275.0, 492.0, 229.119)
	100036 units/pd2_dlc_chew/props/chw_prop_ext_train_lamp/005 (3.00021, 2002.0, 249.119)
	100037 units/pd2_dlc_chew/props/chw_prop_ext_train_lamp/006 (2.99939, 4.00012, 249.119)
	100045 units/pd2_dlc_chew/props/excess_height_car/chw_prop_cargo_net_02/001 (0.0, 1000.0, -140.0)
	100044 units/pd2_dlc_chew/props/excess_height_car/chw_prop_cargo_net_02/002 (0.0, 1000.0, -140.0)
	100046 units/pd2_dlc_chew/props/excess_height_car/chw_prop_cargo_net_02/003 (-0.000139475, 1585.0, -140.0)
	100047 units/pd2_dlc_chew/props/excess_height_car/chw_prop_cargo_net_02/004 (0.0, 418.0, -140.0)
	100092 units/pd2_dlc_chew/props/excess_height_car/chw_prop_train_excess_height/001 (0.0, 1000.0, -140.0)
	100026 units/pd2_dlc_chew/props/excess_height_car/chw_prop_train_weld_door/001 (0.0, 1000.0, -140.0)
	100027 units/pd2_dlc_chew/props/excess_height_car/chw_prop_train_weld_door/002 (0.0, 1000.0, -140.0)
