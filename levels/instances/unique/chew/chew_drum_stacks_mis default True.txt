﻿´startup´ ElementDisableUnit 100025
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 0.35)
	Hide object: units/dev_tools/level_tools/dev_collision_8x32m/001 (350.0, 875.0, 0.0)
	Hide object: units/dev_tools/level_tools/dev_collision_8x32m/002 (350.0, 1550.0, 0.0)
	Hide object: units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch/003 (-26.0, 999.0, -143.0)
	Hide object: units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch/005 (20.0, 1686.0, -143.0)
	Hide object: units/payday2/props/gen_prop_crate_wood_a/002 (94.9729, 251.793, 0.0)
	Hide object: units/world/props/street/cementbags_cover/001 (151.0, 777.0, 0.0)
	Hide object: units/world/props/street/cementbags_cover/002 (-174.0, 1464.0, 0.0)
	Execute sequence: "hide" - units/pd2_dlc_chew/props/chw_prop_oil_drum_group/001 (-227.0, 999.0, 3.0)
	Execute sequence: "hide" - units/pd2_dlc_chew/props/chw_prop_oil_drum_group/002 (-227.0, 1124.0, 3.0)
	Execute sequence: "hide" - units/pd2_dlc_chew/props/chw_prop_oil_drum_group/003 (227.0, 1683.0, 3.0)
	Execute sequence: "hide" - units/pd2_dlc_chew/props/chw_prop_oil_drum_group/004 (227.0, 1560.0, 3.0)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch/001 (24.0, 1000.0, -143.0)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch_2/001 (-25.0, 1685.0, -143.0)
´set_blocker´ ElementInstanceInput 100027
	BASE DELAY  (DELAY 1)
	TRIGGER TIMES 1
	Upon mission event "set_blocker":
		Show object: units/dev_tools/level_tools/dev_collision_8x32m/001 (350.0, 875.0, 0.0)
		Show object: units/dev_tools/level_tools/dev_collision_8x32m/002 (350.0, 1550.0, 0.0)
		Show object: units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch/003 (-26.0, 999.0, -143.0)
		Show object: units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch/005 (20.0, 1686.0, -143.0)
		Show object: units/payday2/props/gen_prop_crate_wood_a/002 (94.9729, 251.793, 0.0)
		Show object: units/world/props/street/cementbags_cover/001 (151.0, 777.0, 0.0)
		Show object: units/world/props/street/cementbags_cover/002 (-174.0, 1464.0, 0.0)
		Toggle on: ´enable_wp_area_001´
		Toggle on: ´enable_wp_area_002´
		Execute sequence: "show" - units/pd2_dlc_chew/props/chw_prop_oil_drum_group/001 (-227.0, 999.0, 3.0)
		Execute sequence: "show" - units/pd2_dlc_chew/props/chw_prop_oil_drum_group/002 (-227.0, 1124.0, 3.0)
		Execute sequence: "show" - units/pd2_dlc_chew/props/chw_prop_oil_drum_group/003 (227.0, 1683.0, 3.0)
		Execute sequence: "show" - units/pd2_dlc_chew/props/chw_prop_oil_drum_group/004 (227.0, 1560.0, 3.0)
		Execute sequence: "state_vis_show" - units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch/001 (24.0, 1000.0, -143.0)
		Execute sequence: "state_vis_show" - units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch_2/001 (-25.0, 1685.0, -143.0)
		Add nav obstacle: units/world/props/street/cementbags_cover/001 (151.0, 777.0, 0.0) (DELAY 1)
		Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x4x3/001 (150.0, 975.0, -9.42454) (DELAY 1)
		Add nav obstacle: units/world/props/street/cementbags_cover/002 (-174.0, 1464.0, 0.0) (DELAY 1)
		Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_3x4x3/001 (-178.0, 1851.0, -39.728) (DELAY 1)
		Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/002 (145.0, 350.0, 0.0) (DELAY 1)
		´point_special_objective_002´ (DELAY 1)
		´point_special_objective_001´ (DELAY 1)
		´point_special_objective_004´ (DELAY 1)
		´point_special_objective_003´ (DELAY 1)
´point_special_objective_002´ ElementSpecialObjective 100012
	Create nav link for None with action "e_nl_up_2_25_fwd_2_5_dwn_2_25m" from (-100.0, 1225.0, 0.0) to (-100.818, 757.97, 0.0)
´point_special_objective_001´ ElementSpecialObjective 100090
	Create nav link for None with action "e_nl_up_2_25_fwd_2_5_dwn_2_25m" from (100.0, 1225.0, 0.0) to (105.103, 774.838, 0.0)
´point_special_objective_004´ ElementSpecialObjective 100014
	Create nav link for None with action "e_nl_up_2_25_fwd_2_5_dwn_2_25m" from (99.9995, 1900.0, 0.0) to (97.2243, 1466.57, 0.0)
´point_special_objective_003´ ElementSpecialObjective 100013
	Create nav link for None with action "e_nl_up_2_25_fwd_2_5_dwn_2_25m" from (-100.001, 1900.0, 0.0) to (-98.1866, 1462.24, 0.0)
´enable_wp_area_001´ ElementAreaTrigger 100097
	DISABLED
	TRIGGER TIMES 1
	Box, width 500.0, height 500.0, depth 500.0, at position (0.0, 700.0, 100.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Execute sequence: "state_interaction_enabled" - units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch/001 (24.0, 1000.0, -143.0)
		Play dialogue: Play_pln_chw_19
		´point_waypoint_001´
´enable_wp_area_002´ ElementAreaTrigger 100098
	DISABLED
	TRIGGER TIMES 1
	Box, width 500.0, height 500.0, depth 500.0, at position (0.0, 1300.0, 100.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Execute sequence: "state_interaction_enabled" - units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch_2/001 (-25.0, 1685.0, -143.0)
		´point_waypoint_002´
´point_waypoint_001´ ElementWaypoint 100099
	Place waypoint with icon "pd2_generic_interact" at position (-200.0, 850.0, 50.0)
´point_waypoint_002´ ElementWaypoint 100100
	Place waypoint with icon "pd2_generic_interact" at position (200.0, 1525.0, 50.0)
´interacted´ ElementUnitSequenceTrigger 100010
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch/001 (24.0, 1000.0, -143.0):
		remove ´point_waypoint_001´
		remove ´point_special_objective_001´
		remove ´point_special_objective_002´
		Execute sequence: "anim_barrel_fall_off" - units/pd2_dlc_chew/props/chw_prop_oil_drum_group/001 (-227.0, 999.0, 3.0)
		Play audio "anim_barrel_fall_off" at position (-4.0, 1000.0, 207.617)
		Make instigator say: g12
		Execute sequence: "anim_barrel_fall_off" - units/pd2_dlc_chew/props/chw_prop_oil_drum_group/002 (-227.0, 1124.0, 3.0) (DELAY 0.05-0.1)
		Hide object: units/dev_tools/level_tools/dev_collision_8x32m/001 (350.0, 875.0, 0.0) (DELAY 3)
		Remove nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x4x3/001 (150.0, 975.0, -9.42454) (DELAY 3)
´interacted001´ ElementUnitSequenceTrigger 100011
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_chew/props/flatbed_hatch/chw_prop_train_flatbed_hatch_2/001 (-25.0, 1685.0, -143.0):
		remove ´point_waypoint_002´
		remove ´point_special_objective_004´
		remove ´point_special_objective_003´
		Execute sequence: "anim_barrel_fall_off" - units/pd2_dlc_chew/props/chw_prop_oil_drum_group/003 (227.0, 1683.0, 3.0)
		Play audio "anim_barrel_fall_off" at position (-4.0, 1700.0, 207.617)
		Make instigator say: g09
		Execute sequence: "anim_barrel_fall_off" - units/pd2_dlc_chew/props/chw_prop_oil_drum_group/004 (227.0, 1560.0, 3.0) (DELAY 0.05-0.1)
		Hide object: units/dev_tools/level_tools/dev_collision_8x32m/002 (350.0, 1550.0, 0.0) (DELAY 3)
		Remove nav obstacle: units/dev_tools/level_tools/dev_door_blocker_3x4x3/001 (-178.0, 1851.0, -39.728) (DELAY 3)
