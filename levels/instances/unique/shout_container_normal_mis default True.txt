﻿´setup_startup´ MissionScriptElement 100003
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 0.5)
	Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/001 (-28.0, -292.0, -42.0)
	Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/002 (72.0, -292.0, -42.0)
	Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/003 (122.0, -292.0, -42.0)
	Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/004 (122.0, 268.0, -42.0)
	Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/005 (71.9999, 268.0, -42.0)
	Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/006 (-28.0001, 268.0, -42.0)
	Hide object: units/payday2/props/gen_prop_cementbags_f/001 (-0.00012207, -194.0, 13.0)
	Hide object: units/payday2/props/gen_prop_cementbags_f/002 (-0.000335693, 206.0, 13.0)
	Hide object: units/payday2/props/gen_prop_cementbags_f/003 (-0.000213623, 5.99982, 13.0)
	Hide object: units/world/props/mansion/man_cover_int_shipping_crate_wide/001 (-81.0, 2.26498e-06, 14.0)
	Hide object: units/world/props/mansion/man_cover_int_shipping_crate_wide/002 (-79.0, 2.5034e-06, 125.0)
	Hide object: units/payday2/props/ind_prop_military_ibc/001 (-65.9879, -59.4482, 13.0)
	Hide object: units/payday2/props/ind_prop_military_ibc/004 (-71.0001, 52.0, 13.0)
	Hide object: units/payday2/props/ind_prop_military_ibc/005 (-66.0374, 51.3906, 130.0)
	Hide object: units/payday2/props/set/ind_prop_warehouse_pallet/001 (-101.0, -80.0, 91.0)
	Hide object: units/payday2/props/set/ind_prop_warehouse_pallet_pile/001 (-69.0, 58.0, 15.0)
	Hide object: units/world/props/mansion/man_cover_int_shipping_crate_wide/gold001 (-81.0, -75.0, 14.0)
	Hide object: units/world/props/mansion/man_cover_int_shipping_crate_wide/gold002 (-79.0, -75.0, 125.0)
	Hide object: units/pd2_dlc1/equipment/gen_interactable_spawn_contraband_crate_2x1x1m/001 (-66.0, 96.0, 10.0)
	Execute 1: (DELAY 0.25)
		´set_colour_001´
		´set_colour_002´
		´set_colour_003´
		´set_colour_004´
	Execute 1: (DELAY 0.25)
		´cement_block´
		´two_crates´
		´military_crates_x2´
		´pallets´
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/back_blocker (100.0, -325.0, 0.0) (DELAY 1)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/front_blocker (99.9999, 229.0, 0.0) (DELAY 1)
´set_colour_001´ ElementUnitSequence 100005
	Execute sequence: "var_set_color_blue" - units/payday2/props/gen_prop_container_murky/001 (0.0, 0.0, 0.0)
	Execute sequence: "var_set_color_blue_front" - units/payday2/props/gen_prop_container_murky_doors/001 (116.0, 295.0, 18.0)
	Execute sequence: "var_set_color_blue_rear" - units/payday2/props/gen_prop_container_murky_doors_vented/001 (-115.0, -295.0, 18.0)
´set_colour_002´ ElementUnitSequence 100006
	Execute sequence: "var_set_color_green" - units/payday2/props/gen_prop_container_murky/001 (0.0, 0.0, 0.0)
	Execute sequence: "var_set_color_green_front" - units/payday2/props/gen_prop_container_murky_doors/001 (116.0, 295.0, 18.0)
	Execute sequence: "var_set_color_green_rear" - units/payday2/props/gen_prop_container_murky_doors_vented/001 (-115.0, -295.0, 18.0)
´set_colour_003´ ElementUnitSequence 100007
	Execute sequence: "var_set_color_red" - units/payday2/props/gen_prop_container_murky/001 (0.0, 0.0, 0.0)
	Execute sequence: "var_set_color_red_front" - units/payday2/props/gen_prop_container_murky_doors/001 (116.0, 295.0, 18.0)
	Execute sequence: "var_set_color_red_rear" - units/payday2/props/gen_prop_container_murky_doors_vented/001 (-115.0, -295.0, 18.0)
´set_colour_004´ ElementUnitSequence 100008
	Execute sequence: "var_set_color_yellow" - units/payday2/props/gen_prop_container_murky/001 (0.0, 0.0, 0.0)
	Execute sequence: "var_set_color_yellow_front" - units/payday2/props/gen_prop_container_murky_doors/001 (116.0, 295.0, 18.0)
	Execute sequence: "var_set_color_yellow_rear" - units/payday2/props/gen_prop_container_murky_doors_vented/001 (-115.0, -295.0, 18.0)
´cement_block´ MissionScriptElement 100039
	Show object: units/payday2/props/gen_prop_cementbags_f/001 (-0.00012207, -194.0, 13.0)
	Show object: units/payday2/props/gen_prop_cementbags_f/002 (-0.000335693, 206.0, 13.0)
	Show object: units/payday2/props/gen_prop_cementbags_f/003 (-0.000213623, 5.99982, 13.0)
	Show object: units/dev_tools/level_tools/dev_bag_collision_1x3m/001 (-28.0, -292.0, -42.0)
	Show object: units/dev_tools/level_tools/dev_bag_collision_1x3m/002 (72.0, -292.0, -42.0)
	Show object: units/dev_tools/level_tools/dev_bag_collision_1x3m/003 (122.0, -292.0, -42.0)
	Show object: units/dev_tools/level_tools/dev_bag_collision_1x3m/004 (122.0, 268.0, -42.0)
	Show object: units/dev_tools/level_tools/dev_bag_collision_1x3m/005 (71.9999, 268.0, -42.0)
	Show object: units/dev_tools/level_tools/dev_bag_collision_1x3m/006 (-28.0001, 268.0, -42.0)
	Toggle off: ´removeObstacle´
	Toggle off: ´removeObstacle_2´
	Toggle off: ´chance_of_goodie（5％）´
´two_crates´ ElementEnableUnit 100047
	Show object: units/world/props/mansion/man_cover_int_shipping_crate_wide/001 (-81.0, 2.26498e-06, 14.0)
	Show object: units/world/props/mansion/man_cover_int_shipping_crate_wide/002 (-79.0, 2.5034e-06, 125.0)
´military_crates_x2´ MissionScriptElement 100049
	Show object: units/payday2/props/ind_prop_military_ibc/001 (-65.9879, -59.4482, 13.0)
	Show object: units/payday2/props/ind_prop_military_ibc/004 (-71.0001, 52.0, 13.0)
	If 50% chance to execute succeeds:
		Show object: units/payday2/props/ind_prop_military_ibc/005 (-66.0374, 51.3906, 130.0)
´pallets´ ElementEnableUnit 100061
	Show object: units/payday2/props/set/ind_prop_warehouse_pallet/001 (-101.0, -80.0, 91.0)
	Show object: units/payday2/props/set/ind_prop_warehouse_pallet_pile/001 (-69.0, 58.0, 15.0)
´removeObstacle´ ElementNavObstacle 100030
	Remove nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/front_blocker (99.9999, 229.0, 0.0)
´removeObstacle_2´ ElementNavObstacle 100031
	Remove nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/back_blocker (100.0, -325.0, 0.0)
´chance_of_goodie（5％）´ ElementLogicChance 100070
	TRIGGER TIMES 1
	If chance to execute succeeds:
		Spawn grenade_crate at (82.9999, 3.00003, 15.0).
´opened_front´ ElementUnitSequenceTrigger 100015
	TRIGGER TIMES 1
	Upon sequence "interact" - units/payday2/props/gen_prop_container_murky_doors/001 (116.0, 295.0, 18.0):
		Toggle off: ´（－0．000221252、－166．0、17．0）´
		´spawn_dozer´
		´removeObstacle´
		´chance_of_goodie（5％）´
´（－0．000221252、－166．0、17．0）´ MissionScriptElement 100011
´spawn_dozer´ MissionScriptElement 100014
	DISABLED
	TRIGGER TIMES 1
	Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/001 (-28.0, -292.0, -42.0)
	Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/002 (72.0, -292.0, -42.0)
	Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/003 (122.0, -292.0, -42.0)
	Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/004 (122.0, 268.0, -42.0)
	Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/005 (71.9999, 268.0, -42.0)
	Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/006 (-28.0001, 268.0, -42.0)
	Hide object: units/payday2/props/gen_prop_cementbags_f/001 (-0.00012207, -194.0, 13.0)
	Hide object: units/payday2/props/gen_prop_cementbags_f/002 (-0.000335693, 206.0, 13.0)
	Hide object: units/payday2/props/gen_prop_cementbags_f/003 (-0.000213623, 5.99982, 13.0)
	Hide object: units/world/props/mansion/man_cover_int_shipping_crate_wide/001 (-81.0, 2.26498e-06, 14.0)
	Hide object: units/world/props/mansion/man_cover_int_shipping_crate_wide/002 (-79.0, 2.5034e-06, 125.0)
	Hide object: units/payday2/props/ind_prop_military_ibc/001 (-65.9879, -59.4482, 13.0)
	Hide object: units/payday2/props/ind_prop_military_ibc/004 (-71.0001, 52.0, 13.0)
	Hide object: units/payday2/props/ind_prop_military_ibc/005 (-66.0374, 51.3906, 130.0)
	Hide object: units/payday2/props/set/ind_prop_warehouse_pallet/001 (-101.0, -80.0, 91.0)
	Hide object: units/payday2/props/set/ind_prop_warehouse_pallet_pile/001 (-69.0, 58.0, 15.0)
	Hide object: units/world/props/mansion/man_cover_int_shipping_crate_wide/gold001 (-81.0, -75.0, 14.0)
	Hide object: units/world/props/mansion/man_cover_int_shipping_crate_wide/gold002 (-79.0, -75.0, 125.0)
	Hide object: units/pd2_dlc1/equipment/gen_interactable_spawn_contraband_crate_2x1x1m/001 (-66.0, 96.0, 10.0)
	If Normal, Hard:
		´dozer_001´ (800.0, 300.0, 0.0)
	If Very Hard, Overkill:
		´dozer_002´ (700.0, 300.0, 0.0)
	If Mayhem, Death Wish, One Down:
		´dozer_003´ (600.0, 300.0, 0.0)
	Show object: units/pd2_dlc1/equipment/gen_interactable_spawn_contraband_crate_2x1x1m/001 (-66.0, 96.0, 10.0) (DELAY 0.01)
	Show object: units/world/props/mansion/man_cover_int_shipping_crate_wide/gold001 (-81.0, -75.0, 14.0) (DELAY 0.01)
	Show object: units/world/props/mansion/man_cover_int_shipping_crate_wide/gold002 (-79.0, -75.0, 125.0) (DELAY 0.01)
	Execute sequence: "state_interaction_enabled" - units/pd2_dlc1/equipment/gen_interactable_spawn_contraband_crate_2x1x1m/001 (-66.0, 96.0, 10.0) (DELAY 0.01)
	Execute sequence: "state_requires_crowbar" - units/pd2_dlc1/equipment/gen_interactable_spawn_contraband_crate_2x1x1m/001 (-66.0, 96.0, 10.0) (DELAY 0.01)
´dozer_001´ ElementSpawnEnemyDummy 100009
	Spawn ene_bulldozer_1.
	No group AI
	Make instigator path towards (400.0, 300.0, 0.0) and show "AI_hunt".
	Orientations:
		´（0．000137329、182．0、16．0）´
		´（－0．000221252、－166．0、17．0）´
´（0．000137329、182．0、16．0）´ MissionScriptElement 100010
´dozer_002´ ElementSpawnEnemyDummy 100021
	Spawn ene_bulldozer_2.
	No group AI
	Make instigator path towards (400.0, 300.0, 0.0) and show "AI_hunt".
	Orientations:
		´（0．000137329、182．0、16．0）´
		´（－0．000221252、－166．0、17．0）´
´dozer_003´ ElementSpawnEnemyDummy 100022
	Spawn ene_bulldozer_3.
	No group AI
	Make instigator path towards (400.0, 300.0, 0.0) and show "AI_hunt".
	Orientations:
		´（0．000137329、182．0、16．0）´
		´（－0．000221252、－166．0、17．0）´
´opened_back´ ElementUnitSequenceTrigger 100017
	TRIGGER TIMES 1
	Upon sequence "interact" - units/payday2/props/gen_prop_container_murky_doors_vented/001 (-115.0, -295.0, 18.0):
		Toggle off: ´（0．000137329、182．0、16．0）´
		´spawn_dozer´
		´removeObstacle_2´
		´chance_of_goodie（5％）´
´enable_dozer´ ElementInstanceInput 100024
	BASE DELAY  (DELAY 3)
	Upon mission event "enable_dozer":
		Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/001 (-28.0, -292.0, -42.0)
		Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/002 (72.0, -292.0, -42.0)
		Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/003 (122.0, -292.0, -42.0)
		Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/004 (122.0, 268.0, -42.0)
		Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/005 (71.9999, 268.0, -42.0)
		Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/006 (-28.0001, 268.0, -42.0)
		Hide object: units/payday2/props/gen_prop_cementbags_f/001 (-0.00012207, -194.0, 13.0)
		Hide object: units/payday2/props/gen_prop_cementbags_f/002 (-0.000335693, 206.0, 13.0)
		Hide object: units/payday2/props/gen_prop_cementbags_f/003 (-0.000213623, 5.99982, 13.0)
		Hide object: units/world/props/mansion/man_cover_int_shipping_crate_wide/001 (-81.0, 2.26498e-06, 14.0)
		Hide object: units/world/props/mansion/man_cover_int_shipping_crate_wide/002 (-79.0, 2.5034e-06, 125.0)
		Hide object: units/payday2/props/ind_prop_military_ibc/001 (-65.9879, -59.4482, 13.0)
		Hide object: units/payday2/props/ind_prop_military_ibc/004 (-71.0001, 52.0, 13.0)
		Hide object: units/payday2/props/ind_prop_military_ibc/005 (-66.0374, 51.3906, 130.0)
		Hide object: units/payday2/props/set/ind_prop_warehouse_pallet/001 (-101.0, -80.0, 91.0)
		Hide object: units/payday2/props/set/ind_prop_warehouse_pallet_pile/001 (-69.0, 58.0, 15.0)
		Hide object: units/world/props/mansion/man_cover_int_shipping_crate_wide/gold001 (-81.0, -75.0, 14.0)
		Hide object: units/world/props/mansion/man_cover_int_shipping_crate_wide/gold002 (-79.0, -75.0, 125.0)
		Hide object: units/pd2_dlc1/equipment/gen_interactable_spawn_contraband_crate_2x1x1m/001 (-66.0, 96.0, 10.0)
		Toggle off: ´chance_of_goodie（5％）´
		´enable_dozer_2´
´enable_dozer_2´ ElementToggle 100023
	Toggle on: ´spawn_dozer´
	Toggle on: ´removeObstacle´
	Toggle on: ´removeObstacle_2´
´hide_container´ ElementInstanceInput 100025
	BASE DELAY  (DELAY 2)
	Upon mission event "hide_container":
		Execute sequence: "state_vis_hide" - units/payday2/props/gen_prop_container_murky/001 (0.0, 0.0, 0.0)
		Execute sequence: "state_vis_hide" - units/payday2/props/gen_prop_container_murky_doors/001 (116.0, 295.0, 18.0)
		Execute sequence: "state_interaction_disabled" - units/payday2/props/gen_prop_container_murky_doors/001 (116.0, 295.0, 18.0)
		Execute sequence: "state_vis_hide" - units/payday2/props/gen_prop_container_murky_doors_vented/001 (-115.0, -295.0, 18.0)
		Execute sequence: "state_interaction_disabled" - units/payday2/props/gen_prop_container_murky_doors_vented/001 (-115.0, -295.0, 18.0)
		Toggle off: ´spawn_dozer´
		Toggle off: ´enable_dozer_2´
		Remove nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/back_blocker (100.0, -325.0, 0.0)
		Remove nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/front_blocker (99.9999, 229.0, 0.0)
		Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/001 (-28.0, -292.0, -42.0)
		Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/002 (72.0, -292.0, -42.0)
		Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/003 (122.0, -292.0, -42.0)
		Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/004 (122.0, 268.0, -42.0)
		Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/005 (71.9999, 268.0, -42.0)
		Hide object: units/dev_tools/level_tools/dev_bag_collision_1x3m/006 (-28.0001, 268.0, -42.0)
		Hide object: units/payday2/props/gen_prop_cementbags_f/001 (-0.00012207, -194.0, 13.0)
		Hide object: units/payday2/props/gen_prop_cementbags_f/002 (-0.000335693, 206.0, 13.0)
		Hide object: units/payday2/props/gen_prop_cementbags_f/003 (-0.000213623, 5.99982, 13.0)
		Hide object: units/world/props/mansion/man_cover_int_shipping_crate_wide/001 (-81.0, 2.26498e-06, 14.0)
		Hide object: units/world/props/mansion/man_cover_int_shipping_crate_wide/002 (-79.0, 2.5034e-06, 125.0)
		Hide object: units/payday2/props/ind_prop_military_ibc/001 (-65.9879, -59.4482, 13.0)
		Hide object: units/payday2/props/ind_prop_military_ibc/004 (-71.0001, 52.0, 13.0)
		Hide object: units/payday2/props/ind_prop_military_ibc/005 (-66.0374, 51.3906, 130.0)
		Hide object: units/payday2/props/set/ind_prop_warehouse_pallet/001 (-101.0, -80.0, 91.0)
		Hide object: units/payday2/props/set/ind_prop_warehouse_pallet_pile/001 (-69.0, 58.0, 15.0)
		Hide object: units/world/props/mansion/man_cover_int_shipping_crate_wide/gold001 (-81.0, -75.0, 14.0)
		Hide object: units/world/props/mansion/man_cover_int_shipping_crate_wide/gold002 (-79.0, -75.0, 125.0)
		Hide object: units/pd2_dlc1/equipment/gen_interactable_spawn_contraband_crate_2x1x1m/001 (-66.0, 96.0, 10.0)
´opened_crate´ ElementUnitSequenceTrigger 100083
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc1/equipment/gen_interactable_spawn_contraband_crate_2x1x1m/001 (-66.0, 96.0, 10.0):
		Execute sequence: "show_and_enable_interaction" - units/payday2/pickups/gen_pku_gold/001 (-83.0, 67.0, 43.0)
