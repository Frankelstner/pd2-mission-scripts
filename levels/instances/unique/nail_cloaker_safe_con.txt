ID range vs continent name:
	100000: world

statics
	100162 core/units/light_omni/001 (0.0, 0.0, 272.0)
		disable_shadows True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.529412, 1.0, 0.392157
			enabled True
			falloff_exponent 1
			far_range 800
			multiplier streetlight
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100391 units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (767.0, 263.0, -179.0)
		disable_shadows True
	100392 units/dev_tools/level_tools/dev_door_blocker_1x1x3/002 (767.0, 163.0, -179.0)
		disable_shadows True
	100393 units/dev_tools/level_tools/dev_door_blocker_1x1x3/003 (667.0, 163.0, -179.0)
		disable_shadows True
	100394 units/dev_tools/level_tools/dev_door_blocker_1x1x3/004 (667.0, 263.0, -179.0)
		disable_shadows True
	100395 units/dev_tools/level_tools/dev_door_blocker_1x1x3/005 (375.0, -375.0, -50.0)
		disable_shadows True
	100396 units/dev_tools/level_tools/dev_door_blocker_1x1x3/006 (475.0, -375.0, -50.0)
		disable_shadows True
	100397 units/dev_tools/level_tools/dev_door_blocker_1x1x3/007 (475.0, -475.0, -50.0)
		disable_shadows True
	100398 units/dev_tools/level_tools/dev_door_blocker_1x1x3/008 (375.0, -475.0, -50.0)
		disable_shadows True
	100399 units/dev_tools/level_tools/dev_door_blocker_1x1x3/009 (-380.0, 193.0, -26.4608)
		disable_shadows True
	100400 units/dev_tools/level_tools/dev_door_blocker_1x1x3/010 (-430.0, -232.0, -26.4608)
		disable_shadows True
	100401 units/dev_tools/level_tools/dev_door_blocker_1x1x3/011 (-430.0, -332.0, -26.4608)
		disable_shadows True
	100402 units/dev_tools/level_tools/dev_door_blocker_1x1x3/012 (-330.0, -332.0, -26.4608)
		disable_shadows True
	100163 units/lights/light_projection_01/001 (5.38403, 100.0, 589.711)
		disable_shadows True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.509804, 1.0, 0.184314
			enabled True
			falloff_exponent 1
			far_range 3000
			multiplier searchlight
			name ls_spot
			near_range 10
			spot_angle_end 117
			spot_angle_start 117
		projection_light ls_spot
	100164 units/lights/light_projection_01/002 (5.38408, 396.41, 303.109)
		disable_shadows True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.509804, 1.0, 0.184314
			enabled True
			falloff_exponent 1
			far_range 3000
			multiplier searchlight
			name ls_spot
			near_range 10
			spot_angle_end 172
			spot_angle_start 152
		projection_light ls_spot
	100076 units/payday2/equipment/gen_interactable_sec_safe_2x1_titan/001 (50.0, -49.0, -1.0)
		disable_shadows True
		mesh_variation deactivate_door
	100012 units/payday2/props/gen_prop_circle_goal_marker_7x7/001 (0.0, 0.0, -5.0)
		disable_shadows True
		mesh_variation hide
	100078 units/pd2_dlc2/architecture/gov_d_int_decal_mull_3x3m/001 (0.0, 1.0, -5.0)
		disable_shadows True
	100471 units/pd2_dlc_nails/props/nls_prop_giant_cloaker/001 (1900.0, -2300.0, -7.86523)
		disable_shadows True
		mesh_variation state_vis_hide
	100362 units/pd2_dlc_nails/props/nls_prop_methlab_ephedrinejar_closed/006 (765.689, 260.908, -180.0)
		disable_on_ai_graph True
		disable_shadows True
	100363 units/pd2_dlc_nails/props/nls_prop_methlab_ephedrinejar_closed/007 (478.236, -373.315, -25.8969)
		disable_on_ai_graph True
		disable_shadows True
	100365 units/pd2_dlc_nails/props/nls_prop_methlab_lithiumbattery/017 (-367.678, -267.678, -20.0)
		disable_on_ai_graph True
		disable_shadows True
	100364 units/pd2_dlc_nails/props/nls_prop_methlab_lithiumbattery/018 (-393.552, -152.122, 48.0886)
		disable_on_ai_graph True
		disable_shadows True
	100366 units/pd2_dlc_nails/props/nls_prop_methlab_lithiumbattery/019 (-330.849, 258.93, -17.0491)
		disable_on_ai_graph True
		disable_shadows True
	100452 units/pd2_dlc_nails/props/nls_prop_methlab_spider_med/007 (-1847.33, -1557.85, -157.755)
		disable_on_ai_graph True
	100014 units/pd2_indiana/props/gen_prop_security_timer/001 (-47.0, 1.00002, 148.0)
		disable_shadows True
		mesh_variation green_on_black
	100056 units/pd2_indiana/props/gen_prop_security_timer/002 (47.0, 1.00002, 148.0)
		disable_shadows True
		mesh_variation green_on_black
	100226 units/pd2_indiana/props/gen_prop_security_timer/003 (3.8147e-06, 48.0, 148.0)
		disable_shadows True
		mesh_variation green_on_black
	100227 units/pd2_indiana/props/gen_prop_security_timer/004 (0.0, -56.0, 148.0)
		disable_shadows True
		mesh_variation green_on_black
