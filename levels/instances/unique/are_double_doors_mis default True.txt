﻿´func_sequence_001´ ElementUnitSequence 100005
	position -150.0, -275.0, 25.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/props/are_int_double_door/001 (0.0, 0.0, 0.0)
			notify_unit_sequence activate_door
			time 0
´trigger_global_event_001´ ElementGlobalEventTrigger 100004
	DISABLED
	TRIGGER TIMES 1
	global_event police_called
	position -150.0, -225.0, 25.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´func_sequence_001´ (delay 0)
´close_door´ ElementInstanceInput 100006
	event stealth_event_close
	on_executed
		´logic_toggle_001´ (delay 0)
		´func_sequence_002´ (delay 0)
´logic_toggle_001´ ElementToggle 100007
	elements
		1 ´trigger_global_event_001´ DISABLED
	set_trigger_times -1
	toggle on
´func_sequence_002´ ElementUnitSequence 100008
	position -50.0, -175.0, 25.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/props/are_int_double_door/001 (0.0, 0.0, 0.0)
			notify_unit_sequence close_door
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/props/are_int_double_door/001 (0.0, 0.0, 0.0)
			notify_unit_sequence deactivate_door
			time 0
