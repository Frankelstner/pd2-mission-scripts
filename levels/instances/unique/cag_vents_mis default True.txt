﻿´auto_start´ ElementUnitSequence 100031
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 0.1)
	Execute sequence: "interaction_disabled" - units/pd2_dlc_dark/props/drk_prop_grilles/001 (0.0, -1.0, 0.0)
´vent_used´ ElementUnitSequenceTrigger 100017
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_dark/props/drk_prop_grilles/001 (0.0, -1.0, 0.0):
		Toggle off: ´inside´
		Toggle off: ´outside´
		Toggle off: ´in_or_out´
		Toggle off: ´vent_shape´
		Toggle off: ´enable´
		Toggle off: ´disable´
		Execute sequence: "open_vent_right_short" - units/pd2_dlc_dark/props/drk_prop_grilles/001 (0.0, -1.0, 0.0)
´inside´ MissionScriptElement 100002
	´enable´
´outside´ MissionScriptElement 100022
	´disable´
´in_or_out´ ElementAreaReportTrigger 100001
	Use shape: ´vent_shape´ (-50.0, -65.0, 50.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon trigger, execute:
		´inside´ (ALTERNATIVE enter)
		´outside´ (ALTERNATIVE empty)
´vent_shape´ ElementShape 100000
	Box, width 205, height 155, depth 230, at position (-50.0, -65.0, 50.0) with rotation (0.0, 0.0, 0.0, -1.0)
´enable´ ElementUnitSequence 100028
	Execute sequence: "interaction_enabled" - units/pd2_dlc_dark/props/drk_prop_grilles/001 (0.0, -1.0, 0.0)
´disable´ ElementUnitSequence 100029
	Execute sequence: "interaction_disabled" - units/pd2_dlc_dark/props/drk_prop_grilles/001 (0.0, -1.0, 0.0)
