﻿´door_reinforced_setup´ MissionScriptElement 100002
	BASE DELAY 2
	on_executed
		´door_block´ (delay 0)
		´door_close´ (delay 0)
		´point_play_sound_001´ (delay 0)
		´breach_door001´ (delay 0)
		´breach_door002´ (delay 0)
		´door_block_thin´ (delay 0) DISABLED
´door_block´ ElementNavObstacle 100004
	obstacle_list
		1
			guis_id 1
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker/001 (-50.0, 0.0, 0.0)
	operation add
	position -300.0, 0.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
´door_close´ ElementUnitSequence 100005
	position -300.0, -100.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc2/architecture/gov_d_int_door_a/001 (-102.0, -3.05176e-05, 0.0)
			notify_unit_sequence state_door_close
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc2/architecture/gov_d_int_door_a/001 (-102.0, -3.05176e-05, 0.0)
			notify_unit_sequence activate_door
			time 0
´door_opened´ ElementUnitSequenceTrigger 100006
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence done_opened
			unit_id units/pd2_dlc2/architecture/gov_d_int_door_a/001 (-102.0, -3.05176e-05, 0.0)
	on_executed
		´door_unblock´ (delay 0)
		´door_opened_output´ (delay 0)
		´remove_navlinks´ (delay 0)
		´door_unblock_thin´ (delay 0) DISABLED
´door_unblock´ ElementNavObstacle 100007
	obstacle_list
		1
			guis_id 1
			obj_name 2ab9bd0bca42eed8
			unit_id units/dev_tools/level_tools/door_blocker/001 (-50.0, 0.0, 0.0)
	operation remove
	position -300.0, 200.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
´door_opened_output´ ElementInstanceOutput 100008
	event door_opened
´breach_door001´ ElementSpecialObjective 100009
	SO_access 13792
	action_duration_max 0
	action_duration_min 0
	ai_group enemies
	align_position True
	align_rotation True
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis 7
	interrupt_dmg 0
	interval 30
	is_navigation_link True
	needs_pos_rsrv True
	path_haste none
	path_stance none
	path_style none
	patrol_path none
	pose none
	position -50.0, -143.0, 2.5
	rotation 0.0, 0.0, 0.0, -1.0
	scan True
	search_distance 0
	search_position -43.9917, 104.933, 2.5
	so_action e_nl_kick_enter
	trigger_on none
´breach_door002´ ElementSpecialObjective 100001
	SO_access 259552
	action_duration_max 0
	action_duration_min 0
	ai_group enemies
	align_position True
	align_rotation True
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis 7
	interrupt_dmg 0
	interval 30
	is_navigation_link True
	needs_pos_rsrv True
	path_haste none
	path_stance none
	path_style none
	patrol_path none
	pose none
	position -50.0, 143.0, 0.0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	scan True
	search_distance 0
	search_position -49.2291, -107.969, 2.5
	so_action e_nl_kick_enter
	trigger_on none
´door_navlinked_open´ ElementSpecialObjectiveTrigger 100010
	TRIGGER TIMES 1
	elements
		1 ´breach_door002´
		2 ´breach_door001´
	event anim_act_04
	position 100.0, -100.0, 300.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	on_executed
		´open_door´ (delay 0)
		´door_opened´ (delay 0)
´remove_breach_navlinks´ ElementOperator 100011
	elements
		1 ´breach_door002´
		2 ´breach_door001´
	operation remove
´disable_breach_navlinks´ ElementToggle 100012
	elements
		1 ´breach_door002´
		2 ´breach_door001´
		3 ´door_navlinked_open´
	set_trigger_times -1
	toggle off
´open_door´ ElementUnitSequence 100013
	position 100.0, 100.0, 300.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc2/architecture/gov_d_int_door_a/001 (-102.0, -3.05176e-05, 0.0)
			notify_unit_sequence open_door
			time 0
´remove_navlinks´ MissionScriptElement 100014
	on_executed
		´remove_breach_navlinks´ (delay 0)
		´disable_breach_navlinks´ (delay 0)
		´point_debug_001´ (delay 0)
´point_debug_001´ ElementDebug 100015
	as_subtitle False
	debug_string navlinks removed
	show_instigator False
´close_security_door´ ElementInstanceInput 100018
	event close_security_door
	on_executed
		´door_reinforced_setup´ (delay 0.05)
´point_play_sound_001´ ElementPlaySound 100021
	append_prefix False
	elements
	position -50.0, 0.0, 102.5
	rotation 0.0, 0.0, 0.0, -1.0
	sound_event security_barrier_close
	use_instigator False
´door_block_thin´ ElementNavObstacle 100023
	DISABLED
	obstacle_list
		1
			guis_id 1
			obj_name d6780cea5f022bc0
			unit_id units/dev_tools/level_tools/door_blocker_1m/001 (-50.0, -25.0, 0.0)
	operation add
	position -400.0, 0.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
´door_block_thin_enable´ ElementToggle 100024
	elements
		1 ´door_unblock_thin´ DISABLED
		2 ´door_block_thin´ DISABLED
	set_trigger_times -1
	toggle on
´door_unblock_thin´ ElementNavObstacle 100025
	DISABLED
	obstacle_list
		1
			guis_id 1
			obj_name d6780cea5f022bc0
			unit_id units/dev_tools/level_tools/door_blocker_1m/001 (-50.0, -25.0, 0.0)
	operation remove
	position -400.0, 200.0, 300.0
	rotation 0.0, 0.0, 0.0, -1.0
´close_security_door_thin´ ElementInstanceInput 100026
	event close_security_door_thin
	on_executed
		´door_block_thin_enable´ (delay 0)
		´close_security_door´ (delay 0)
		´door_block_regular_disable´ (delay 0)
´door_block_regular_disable´ ElementToggle 100027
	elements
		1 ´door_unblock´
		2 ´door_block´
	set_trigger_times -1
	toggle off
