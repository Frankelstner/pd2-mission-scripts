ID range vs continent name:
	100000: world

statics
	100062 core/units/light_omni/001 (-165.0, -619.0, 127.604)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 1.0
			enabled True
			falloff_exponent 1
			far_range 300
			multiplier match
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100063 core/units/light_omni/002 (-165.0, -119.0, 127.604)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 1.0, 1.0
			enabled True
			falloff_exponent 1
			far_range 300
			multiplier match
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100065 core/units/light_omni/003 (67.0, -210.0, 199.604)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.988235, 0.972549, 0.745098
			enabled True
			falloff_exponent 1
			far_range 250
			multiplier match
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100064 core/units/light_spot/001 (-176.0, 51.0, 234.604)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.458824, 0.819608, 0.941177
			enabled True
			falloff_exponent 1
			far_range 300
			multiplier identity
			name ls_spot
			near_range 80
			spot_angle_end 115
			spot_angle_start 45
	100000 units/dev_tools/level_tools/dev_bag_collision_1x3m/001 (-164.0, 199.0, 43.0)
	100012 units/dev_tools/level_tools/dev_bag_collision_1x3m/002 (-64.0, 199.0, 43.0)
	100016 units/dev_tools/level_tools/dev_bag_collision_1x3m/003 (27.0, 199.0, 43.0)
	100085 units/dev_tools/level_tools/dev_bag_collision_1x3m/004 (110.0, 199.0, 43.0)
	100096 units/dev_tools/level_tools/dev_bag_collision_1x3m/005 (110.0, 99.0001, 43.0)
	100097 units/dev_tools/level_tools/dev_bag_collision_1x3m/006 (110.0, 32.0001, 43.0)
	100098 units/dev_tools/level_tools/dev_bag_collision_1x3m/007 (34.6412, 191.087, 43.0)
	100034 units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-475.0, -436.0, -0.395782)
	100044 units/dev_tools/level_tools/dev_door_blocker_1x1x3/002 (-214.0, -745.0, -0.395769)
	100035 units/dev_tools/level_tools/dev_door_blocker_1x2x3/001 (-128.0, -572.0, -4.87335)
	100036 units/dev_tools/level_tools/dev_door_blocker_1x2x3/002 (-1.00002, -572.0, -4.87335)
	100037 units/dev_tools/level_tools/dev_door_blocker_1x2x3/003 (174.0, -611.0, -4.87335)
	100038 units/dev_tools/level_tools/dev_door_blocker_1x2x3/004 (140.0, -202.0, -4.87335)
	100039 units/dev_tools/level_tools/dev_door_blocker_1x2x3/005 (140.0, -402.0, -4.87335)
	100051 units/dev_tools/level_tools/dev_door_blocker_1x2x3/006 (121.0, 98.0, -4.87335)
	100057 units/dev_tools/level_tools/dev_door_blocker_1x2x3/007 (-79.0002, 198.0, -4.87335)
	100058 units/dev_tools/level_tools/dev_door_blocker_1x2x3/008 (-279.0, 198.0, -4.87335)
	100059 units/dev_tools/level_tools/dev_door_blocker_1x2x3/009 (-479.0, 198.0, -4.87335)
	100060 units/dev_tools/level_tools/dev_door_blocker_1x2x3/010 (-479.0, -102.0, -4.87335)
	100061 units/dev_tools/level_tools/dev_door_blocker_1x2x3/011 (-479.0, -248.0, -4.87335)
	100022 units/payday2/equipment/gen_interactable_door_reinforced/001 (-478.998, -441.001, 0.0)
		disable_on_ai_graph True
		mesh_variation activate_door
	100081 units/payday2/props/air_prop_office_binder_pack_3/001 (-312.0, 199.0, 151.347)
	100084 units/payday2/props/air_prop_office_binder_pack_4/001 (-244.0, 201.0, 151.347)
	100027 units/payday2/props/gen_prop_security_monitors_four/001 (135.0, 48.0, 6.31374)
	100046 units/payday2/props/gen_prop_security_monitors_four/002 (-55.0, 230.0, 11.6042)
	100050 units/payday2/props/gen_prop_security_monitors_two/002 (88.8017, 188.459, 11.1244)
	100069 units/payday2/props/lxa_prop_livingroom_lamp_floor/001 (-70.0, -574.0, -0.395813)
	100093 units/payday2/props/off_prop_officehigh_books_professional_01/001 (119.0, -549.0, 73.9402)
	100092 units/payday2/props/off_prop_officehigh_books_professional_03/001 (119.0, -461.0, 73.9402)
	100091 units/payday2/props/off_prop_officehigh_books_professional_05/001 (119.0, -453.0, 36.9402)
	100005 units/payday2/props/off_prop_officehigh_bulletinboard_long_02/001 (-474.0, -200.0, 158.181)
	100020 units/payday2/props/off_prop_officehigh_chair_deluxe/001 (-96.0, 70.0, -0.395769)
	100028 units/payday2/props/off_prop_officehigh_chair_deluxe/002 (-347.0, 86.0, -0.395769)
	100045 units/payday2/props/off_prop_officehigh_chair_deluxe/003 (-351.0, -91.0, -0.395769)
	100007 units/payday2/props/off_prop_officehigh_desk_dark_drawers/002 (-224.0, 205.0, 0.0)
	100006 units/payday2/props/off_prop_officehigh_desk_dark_drawers/003 (-184.0, 205.0, 0.0)
	100019 units/payday2/props/off_prop_officehigh_desk_dark_drawers/006 (-184.0, 205.0, 59.0)
	100025 units/payday2/props/off_prop_officehigh_desk_dark_drawers/007 (-224.0, 205.0, 59.0)
	100001 units/payday2/props/servers/gen_prop_security_server_hightech/001 (41.9998, -107.0, 0.0)
	100009 units/payday2/props/servers/gen_prop_security_server_hightech/002 (41.9999, -173.0, 0.0)
	100017 units/payday2/props/servers/gen_prop_security_server_hightech/003 (41.9999, -240.0, 0.0)
	100082 units/payday2/props/shelves/off_prop_officehigh_shelf_wenge/001 (-275.0, 214.0, 145.555)
	100049 units/payday2/props/signs/gen_prop_security_sign_1/001 (-500.998, -474.001, 128.0)
	100072 units/pd2_dlc_chill/props/chl_prop_livingroom_coffeetable_a/001 (-194.0, -664.0, -0.395817)
	100079 units/pd2_dlc_chill/props/chl_prop_livingroom_sideboard_b/001 (102.0, -434.0, -0.395805)
	100080 units/pd2_dlc_chill/props/chl_prop_livingroom_sideboard_b/002 (102.0, -586.0, -0.395805)
	100004 units/pd2_dlc_dah/props/dah_prop_desk_a/001 (-474.0, 212.0, 0.0)
	100014 units/pd2_dlc_dah/props/dah_prop_desk_c/002 (-474.0, 12.0, 0.0)
	100011 units/pd2_dlc_dah/props/dah_prop_fan_table/001 (93.0, -39.0, 96.6042)
	100090 units/pd2_dlc_dah/props/dah_prop_fan_table/002 (-438.0, -24.0, 80.6042)
	100024 units/pd2_dlc_dah/props/dah_prop_keyboard/001 (-387.174, 54.2898, 81.0522)
	100068 units/pd2_dlc_dah/props/dah_prop_keyboard/002 (-393.508, -125.295, 81.0522)
	100002 units/pd2_dlc_dah/props/dah_prop_light_switch/001 (-474.0, -468.0, 100.561)
	100087 units/pd2_dlc_dah/props/dah_prop_magazine_pile_a/001 (-410.0, -38.0, 80.9673)
	100086 units/pd2_dlc_dah/props/dah_prop_magazine_pile_b/001 (-316.0, 142.0, 81.0522)
	100008 units/pd2_dlc_dah/props/dah_prop_outlet/001 (-473.0, -202.0, 32.6042)
	100010 units/pd2_dlc_dah/props/dah_prop_outlet/002 (-473.0, -219.0, 32.6042)
	100015 units/pd2_dlc_dah/props/dah_prop_outlet/003 (-473.0, -694.0, 32.6042)
	100073 units/pd2_dlc_dah/props/dah_prop_outlet/004 (-473.0, -677.0, 32.6042)
	100074 units/pd2_dlc_dah/props/dah_prop_outlet/005 (123.0, -698.0, 32.6042)
	100075 units/pd2_dlc_dah/props/dah_prop_outlet/006 (122.0, -323.0, 32.6042)
	100043 units/pd2_dlc_dah/props/dah_prop_plant_desk/002 (-196.0, -666.0, 22.2942)
	100013 units/pd2_dlc_dah/props/dah_prop_plant_large/001 (72.0, -838.0, -0.395777)
	100078 units/pd2_dlc_dah/props/dah_prop_plant_large/002 (-409.0, -847.0, -0.395782)
	100003 units/pd2_dlc_dah/props/dah_prop_security_desk/002 (110.0, 206.0, -2.0)
	100070 units/pd2_dlc_dah/props/dah_prop_sofa_b/001 (-200.0, -504.0, -0.395782)
	100083 units/pd2_dlc_dah/props/dah_prop_sofa_b_single/001 (-62.0, -663.0, -0.395789)
	100071 units/pd2_dlc_dah/props/dah_prop_sofa_b_single/002 (-338.0, -663.0, -0.395793)
	100076 units/pd2_dlc_dah/props/dah_prop_wall_lamp_a/001 (113.0, -661.0, 117.604)
	100077 units/pd2_dlc_dah/props/dah_prop_wall_lamp_a/002 (-462.0, -661.0, 117.604)
	100088 units/pd2_dlc_flat/props/flt_prop_coffe_cup/001 (-366.0, 121.0, 80.2887)
	100089 units/pd2_dlc_flat/props/flt_prop_coffe_cup/002 (-211.0, -625.0, 29.7033)
	100066 units/pd2_dlc_lxy/architecture/yacht/pilot_house/lxy_int_pilothouse_bridge_monitor2/001 (-446.142, 36.1421, 81.0522)
	100054 units/pd2_dlc_red/props/red_prop_monitor/001 (-394.998, 147.645, 80.0522)
	100053 units/pd2_dlc_red/props/red_prop_monitor/002 (-430.722, 99.6206, 80.0522)
	100055 units/pd2_dlc_red/props/red_prop_monitor/003 (-428.733, -142.356, 81.0522)
	100056 units/pd2_dlc_red/props/red_prop_monitor/004 (-434.954, -81.9908, 80.0522)
	100094 units/world/props/bank/paintings/suburbia_painting_21/001 (127.0, -575.0, 154.03)
	100023 units/world/props/bank/paintings/suburbia_painting_23/001 (127.0, -450.0, 154.03)
	100095 units/world/props/diamond_heist/apartment/apartment_livingroom/officebooks/office_books/001 (103.0, -407.0, 35.4129)
	100047 units/world/props/office/computer_mouse/001 (-379.726, 96.1671, 87.0522)
	100067 units/world/props/office/computer_mouse/002 (-389.0, -83.0, 87.0522)
