﻿´Auto_Start´ ElementDisableUnit 100005
	EXECUTE ON STARTUP
	Hide object: units/payday2/props/previs/mannequins/previs_mannequin_01 (0.0, 0.0, 0.0)
	Hide object: units/payday2/props/previs/mannequins/previs_mannequin_02 (0.0, 0.0, 0.0)
	Hide object: units/payday2/props/previs/mannequins/previs_mannequin_03 (-1.19209e-05, 0.0, 0.0)
	Hide object: units/pd2_dlc_tng/masks/tng_cloaker/msk_tng_cloaker/03 (3.89566, 0.779007, 156.0)
	Hide object: units/pd2_dlc_tng/masks/tng_cloaker/msk_tng_cloaker/01 (5.37096, 2.34708, 157.0)
	Hide object: units/pd2_dlc_tng/masks/tng_cloaker/msk_tng_cloaker/02 (8.71434, -7.457, 157.0)
	Hide object: units/payday2/architecture/mkp_int_floor_2x2m_a/001 (-100.0, -100.0, 0.0)
´spawn_mannequin´ ElementInstanceInput 100007
	Upon mission event "spawn_mannequin":
		´random´
´random´ ElementRandom 100009
	TRIGGER TIMES 1
	Execute 1:
		´1´
		´2´
		´3´
´1´ ElementEnableUnit 100010
	Show object: units/payday2/props/previs/mannequins/previs_mannequin_01 (0.0, 0.0, 0.0)
	Show object: units/pd2_dlc_tng/masks/tng_cloaker/msk_tng_cloaker/01 (5.37096, 2.34708, 157.0)
	Toggle on: ´1_2´
´2´ ElementEnableUnit 100011
	Show object: units/payday2/props/previs/mannequins/previs_mannequin_02 (0.0, 0.0, 0.0)
	Show object: units/pd2_dlc_tng/masks/tng_cloaker/msk_tng_cloaker/02 (8.71434, -7.457, 157.0)
	Toggle on: ´2_2´
´3´ ElementEnableUnit 100012
	Show object: units/payday2/props/previs/mannequins/previs_mannequin_03 (-1.19209e-05, 0.0, 0.0)
	Show object: units/pd2_dlc_tng/masks/tng_cloaker/msk_tng_cloaker/03 (3.89566, 0.779007, 156.0)
	Toggle on: ´3_2´
´1_2´ MissionScriptElement 100036
	DISABLED
	TRIGGER TIMES 1
	´cloaker_01´ (0.0, -3.05176e-05, 0.0)
	´dis_all´
´2_2´ MissionScriptElement 100037
	DISABLED
	TRIGGER TIMES 1
	´cloaker_02´ (1.52588e-05, 0.0, 0.0)
	´dis_all´
´3_2´ MissionScriptElement 100038
	DISABLED
	TRIGGER TIMES 1
	´cloaker_03´ (-1.52588e-05, -9.15527e-05, 0.0)
	´dis_all´
´cloaker_01´ ElementSpawnEnemyDummy 100032
	Spawn ene_spook_hvh_1 (0.0, -3.05176e-05, 0.0) with action e_sp_stand_sniper_loop.
´dis_all´ ElementDisableUnit 100044
	TRIGGER TIMES 1
	Hide object: units/pd2_dlc_tng/masks/tng_cloaker/msk_tng_cloaker/01 (5.37096, 2.34708, 157.0)
	Hide object: units/pd2_dlc_tng/masks/tng_cloaker/msk_tng_cloaker/02 (8.71434, -7.457, 157.0)
	Hide object: units/pd2_dlc_tng/masks/tng_cloaker/msk_tng_cloaker/03 (3.89566, 0.779007, 156.0)
	Hide object: units/payday2/props/previs/mannequins/previs_mannequin_01 (0.0, 0.0, 0.0)
	Hide object: units/payday2/props/previs/mannequins/previs_mannequin_02 (0.0, 0.0, 0.0)
	Hide object: units/payday2/props/previs/mannequins/previs_mannequin_03 (-1.19209e-05, 0.0, 0.0)
	Toggle off: ´3_2´
	Toggle off: ´2_2´
	Toggle off: ´1_2´
	Toggle off: ´pick_available´
	Toggle off: ´link´
	Toggle on, trigger times 1: ´trigger_area´
´cloaker_02´ ElementSpawnEnemyDummy 100039
	Spawn ene_spook_hvh_1 (1.52588e-05, 0.0, 0.0) with action e_sp_stand_sniper_loop.
´cloaker_03´ ElementSpawnEnemyDummy 100040
	Spawn ene_spook_hvh_1 (-1.52588e-05, -9.15527e-05, 0.0) with action e_sp_stand_sniper_loop.
´pick_available´ ElementRandom 100035
	Execute 1:
		´1_2´
		´2_2´
		´3_2´
´link´ MissionScriptElement 100033
	´pick_available´ (DELAY 0.1)
´trigger_area´ ElementAreaTrigger 100049
	DISABLED
	Cylinder, radius 889.017, height 2534.4, at position (0.0, 0.0, 229.084) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		´down_001´
		´down_003´
		´down_002´
´down_001´ ElementSpecialObjective 100026
	Create objective for someone from group: ´cloaker_01´
	Make person path towards (0.0, 0.0, 0.0) and show "e_nl_down_3m".
´down_003´ ElementSpecialObjective 100048
	Create objective for someone from group: ´cloaker_03´
	Make person path towards (2.91038e-11, 0.0, 0.0) and show "e_nl_down_3m".
´down_002´ ElementSpecialObjective 100047
	Create objective for someone from group: ´cloaker_02´
	Make person path towards (0.0, 0.0, 0.0) and show "e_nl_down_3m".
´break_01´ ElementUnitSequenceTrigger 100016
	TRIGGER TIMES 1
	Upon sequence "make_dynamic" - units/payday2/props/previs/mannequins/previs_mannequin_01 (0.0, 0.0, 0.0):
		Hide object: units/pd2_dlc_tng/masks/tng_cloaker/msk_tng_cloaker/01 (5.37096, 2.34708, 157.0)
		Play effect effects/particles/explosions/burnpuff at position (0.0, 0.0, 163.0)
		Toggle on, trigger times 1: ´hide_dead´
		Toggle off: ´link´
´hide_dead´ ElementDisableUnit 100028
	DISABLED
	Hide object: units/payday2/props/previs/mannequins/previs_mannequin_01 (0.0, 0.0, 0.0)
	Hide object: units/payday2/props/previs/mannequins/previs_mannequin_02 (0.0, 0.0, 0.0)
	Hide object: units/payday2/props/previs/mannequins/previs_mannequin_03 (-1.19209e-05, 0.0, 0.0)
´break_03´ ElementUnitSequenceTrigger 100019
	TRIGGER TIMES 1
	Upon sequence "make_dynamic" - units/payday2/props/previs/mannequins/previs_mannequin_03 (-1.19209e-05, 0.0, 0.0):
		Hide object: units/pd2_dlc_tng/masks/tng_cloaker/msk_tng_cloaker/03 (3.89566, 0.779007, 156.0)
		Play effect effects/particles/explosions/burnpuff at position (0.0, 0.0, 162.0)
		Toggle on, trigger times 1: ´hide_dead´
		Toggle off: ´link´
´break_02´ ElementUnitSequenceTrigger 100020
	TRIGGER TIMES 1
	Upon sequence "make_dynamic" - units/payday2/props/previs/mannequins/previs_mannequin_02 (0.0, 0.0, 0.0):
		Hide object: units/pd2_dlc_tng/masks/tng_cloaker/msk_tng_cloaker/02 (8.71434, -7.457, 157.0)
		Play effect effects/particles/explosions/burnpuff at position (6.0, -7.0, 164.0)
		Toggle on, trigger times 1: ´hide_dead´
		Toggle off: ´link´
´clean_up_dead´ ElementInstanceInput 100031
	Upon mission event "clean_up_dead":
		´hide_dead´
´spawn_cloaker´ ElementInstanceInput 100034
	Upon mission event "spawn_cloaker":
		´link´
