ID range vs continent name:
	100000: world

statics
	100038 core/units/light_omni/001 (0.0, 0.0, 300.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.701961, 0.105882, 0.952941
			enabled True
			falloff_exponent 1
			far_range 600
			multiplier desklight
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
	100116 units/dev_tools/level_tools/dev_bag_collision_1x1m/001 (-50.0, 52.0, 201.0)
	100112 units/dev_tools/level_tools/dev_bag_collision_1x3m/001 (-50.0, 76.0, -100.0)
	100113 units/dev_tools/level_tools/dev_bag_collision_1x3m/002 (51.0, 57.0001, -100.0)
	100114 units/dev_tools/level_tools/dev_bag_collision_1x3m/003 (50.0, -50.9999, -100.0)
	100115 units/dev_tools/level_tools/dev_bag_collision_1x3m/004 (-52.0, -50.9999, -100.0)
	100119 units/dev_tools/level_tools/dev_bag_collision_1x3m/interior (50.0, 27.0001, -100.0)
	100117 units/dev_tools/level_tools/dev_collision_1x3m/001 (-50.0, 76.0, -100.0)
	100065 units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-50.0, -50.0, 0.0)
	100007 units/payday2/equipment/gen_interactable_sec_safe_2x1_titan/safe (-50.0, 50.0, 0.0)
		disable_collision True
		disable_on_ai_graph True
		hide_on_projection_light True
	100008 units/payday2/props/gen_prop_circle_goal_marker_7x7/marker (0.0, 0.0, 0.0)
		mesh_variation show
	100027 units/pd2_indiana/props/gen_prop_security_timer/005 (50.0, 0.0, 125.0)
		disable_collision True
	100028 units/pd2_indiana/props/gen_prop_security_timer/006 (-50.0, 0.0, 125.0)
		disable_collision True
	100029 units/pd2_indiana/props/gen_prop_security_timer/007 (0.0, -50.0, 125.0)
		disable_collision True
