ID range vs continent name:
	100000: world

statics
	100143 units/dev_tools/editable_text_short/hs - title (698.0, -209.0, 96.0)
		align left
		alpha 1
		blend_mode normal
		font fonts/font_medium_shadow_mf
		font_color 0.0, 0.0, 0.0
		font_size 0.6
		render_template diffuse_vc_decal
		shape
			1 0
			2 0
			3 1
			4 1
		text Headshot -

		vertical center
		word_wrap False
		wrap False
	100248 units/dev_tools/level_tools/dev_collision_4x3m/003 (125.0, 25.0, 0.0)
	100245 units/dev_tools/level_tools/dev_collision_4x3m/004 (0.000227034, 6.10352e-05, 0.0)
	100007 units/payday2/architecture/ind/level/ind_ext_stripes_03_4m/001 (-200.0, -200.0, 0.0)
	100000 units/payday2/props/are_int_double_door/001 (25.0, 100.0, 0.0)
		mesh_variation deactivate_door
	100001 units/pd2_dlc_arena/props/are_prop_info_button_gensec/001 (29.0, 350.0, 0.0)
		mesh_variation state_interaction_enable
	100027 units/pd2_dlc_berry/props/bry_prop_vault_lamp/001 (25.0, 150.0, 225.0)
	100028 units/pd2_dlc_berry/props/bry_prop_vault_lamp/002 (25.0, 200.0, 225.0)
	100029 units/pd2_dlc_berry/props/bry_prop_vault_lamp/003 (25.0, 250.0, 225.0)
	100023 units/pd2_dlc_chill/props/chl_props_reset_button/001 (46.0, 364.0, 113.0)
