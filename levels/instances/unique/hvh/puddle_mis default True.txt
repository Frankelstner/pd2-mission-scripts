﻿´AUTO_START´ ElementInstanceInput 100005
	EXECUTE ON STARTUP
	Upon mission event "hide":
		Hide object: units/payday2/props/mop/com_prop_janitor_sign_wetfloor_standing/001 (0.0, 0.0, 0.0)
		Hide object: units/world/architecture/street/decals/ground/decal_ground_middle_puddle/001 (6.0, -4.0, 0.0)
		Hide object: units/pd2_dlc_berry/props/bry_prop_vault_lamp/001 (4.25209e-07, 1.0, 76.0)
		Hide object: units/pd2_dlc_berry/props/bry_prop_vault_lamp/002 (-9.21848e-07, 1.0, 76.0)
		Toggle off: ´link´
´link´ ElementPlayerState 100009
	DISABLED
	Make instigator enter state "incapacitated"
´puddle_radius´ ElementAreaTrigger 100003
	Cylinder, radius 92.2453, height 163.087, at position (0.0, 0.0, 50.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		´link´
´show´ ElementInstanceInput 100008
	Upon mission event "show":
		Show object: units/payday2/props/mop/com_prop_janitor_sign_wetfloor_standing/001 (0.0, 0.0, 0.0)
		Show object: units/world/architecture/street/decals/ground/decal_ground_middle_puddle/001 (6.0, -4.0, 0.0)
		Show object: units/pd2_dlc_berry/props/bry_prop_vault_lamp/001 (4.25209e-07, 1.0, 76.0)
		Show object: units/pd2_dlc_berry/props/bry_prop_vault_lamp/002 (-9.21848e-07, 1.0, 76.0)
		Toggle on: ´link´
