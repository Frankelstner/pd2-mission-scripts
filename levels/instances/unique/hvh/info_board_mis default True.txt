﻿´clear´ ElementDisableUnit 100001
	EXECUTE ON STARTUP
	Hide object: units/dev_tools/editable_text_long/text_open_safe (-93.2076, 0.0, 40.8801)
	Hide object: units/dev_tools/editable_text_long/text_tazer_drone (-92.8664, 0.0, 57.4019)
	Hide object: units/dev_tools/editable_text_long/text_dont_slip (-500.795, 0.0, 45.6182)
	Hide object: units/dev_tools/editable_text_long/text_lance (-80.2076, 0.0, 48.8801)
	Hide object: units/dev_tools/editable_text_long/text_exit (-95.6493, 0.0, 51.6351)
	Hide object: units/dev_tools/editable_text_long/text_safes (-85.0937, -7.62939e-06, 49.2079)
	Hide object: units/dev_tools/editable_text_long/text_watch_clock (-81.1713, -1.29422e-06, 49.9318)
	´default´ (DELAY 0.2)
´default´ ElementEnableUnit 100026
	TRIGGER TIMES 1
	Show object: units/dev_tools/editable_text_long/text_watch_clock (-81.1713, -1.29422e-06, 49.9318)
´lance´ ElementInstanceInput 100004
	Upon mission event "lance":
		Hide object: units/dev_tools/editable_text_long/text_open_safe (-93.2076, 0.0, 40.8801)
		Hide object: units/dev_tools/editable_text_long/text_tazer_drone (-92.8664, 0.0, 57.4019)
		Hide object: units/dev_tools/editable_text_long/text_dont_slip (-500.795, 0.0, 45.6182)
		Hide object: units/dev_tools/editable_text_long/text_lance (-80.2076, 0.0, 48.8801)
		Hide object: units/dev_tools/editable_text_long/text_exit (-95.6493, 0.0, 51.6351)
		Hide object: units/dev_tools/editable_text_long/text_safes (-85.0937, -7.62939e-06, 49.2079)
		Hide object: units/dev_tools/editable_text_long/text_watch_clock (-81.1713, -1.29422e-06, 49.9318)
		´default´ (DELAY 0.2)
		Show object: units/dev_tools/editable_text_long/text_lance (-80.2076, 0.0, 48.8801) (DELAY 0.25)
´t-drone´ ElementInstanceInput 100006
	Upon mission event "tazer_drone":
		Hide object: units/dev_tools/editable_text_long/text_open_safe (-93.2076, 0.0, 40.8801)
		Hide object: units/dev_tools/editable_text_long/text_tazer_drone (-92.8664, 0.0, 57.4019)
		Hide object: units/dev_tools/editable_text_long/text_dont_slip (-500.795, 0.0, 45.6182)
		Hide object: units/dev_tools/editable_text_long/text_lance (-80.2076, 0.0, 48.8801)
		Hide object: units/dev_tools/editable_text_long/text_exit (-95.6493, 0.0, 51.6351)
		Hide object: units/dev_tools/editable_text_long/text_safes (-85.0937, -7.62939e-06, 49.2079)
		Hide object: units/dev_tools/editable_text_long/text_watch_clock (-81.1713, -1.29422e-06, 49.9318)
		´default´ (DELAY 0.2)
		Show object: units/dev_tools/editable_text_long/text_tazer_drone (-92.8664, 0.0, 57.4019) (DELAY 0.25)
´wet_floor´ ElementInstanceInput 100012
	Upon mission event "floor_wet":
		Hide object: units/dev_tools/editable_text_long/text_open_safe (-93.2076, 0.0, 40.8801)
		Hide object: units/dev_tools/editable_text_long/text_tazer_drone (-92.8664, 0.0, 57.4019)
		Hide object: units/dev_tools/editable_text_long/text_dont_slip (-500.795, 0.0, 45.6182)
		Hide object: units/dev_tools/editable_text_long/text_lance (-80.2076, 0.0, 48.8801)
		Hide object: units/dev_tools/editable_text_long/text_exit (-95.6493, 0.0, 51.6351)
		Hide object: units/dev_tools/editable_text_long/text_safes (-85.0937, -7.62939e-06, 49.2079)
		Hide object: units/dev_tools/editable_text_long/text_watch_clock (-81.1713, -1.29422e-06, 49.9318)
		´default´ (DELAY 0.2)
		Show object: units/dev_tools/editable_text_long/text_dont_slip (-500.795, 0.0, 45.6182) (DELAY 0.25)
´safe´ ElementInstanceInput 100014
	Upon mission event "Open the Safe":
		Hide object: units/dev_tools/editable_text_long/text_open_safe (-93.2076, 0.0, 40.8801)
		Hide object: units/dev_tools/editable_text_long/text_tazer_drone (-92.8664, 0.0, 57.4019)
		Hide object: units/dev_tools/editable_text_long/text_dont_slip (-500.795, 0.0, 45.6182)
		Hide object: units/dev_tools/editable_text_long/text_lance (-80.2076, 0.0, 48.8801)
		Hide object: units/dev_tools/editable_text_long/text_exit (-95.6493, 0.0, 51.6351)
		Hide object: units/dev_tools/editable_text_long/text_safes (-85.0937, -7.62939e-06, 49.2079)
		Hide object: units/dev_tools/editable_text_long/text_watch_clock (-81.1713, -1.29422e-06, 49.9318)
		´default´ (DELAY 0.2)
		Show object: units/dev_tools/editable_text_long/text_open_safe (-93.2076, 0.0, 40.8801) (DELAY 0.25)
´wet_floor002´ ElementInstanceInput 100015
	Upon mission event "extra":
		Hide object: units/dev_tools/editable_text_long/text_open_safe (-93.2076, 0.0, 40.8801)
		Hide object: units/dev_tools/editable_text_long/text_tazer_drone (-92.8664, 0.0, 57.4019)
		Hide object: units/dev_tools/editable_text_long/text_dont_slip (-500.795, 0.0, 45.6182)
		Hide object: units/dev_tools/editable_text_long/text_lance (-80.2076, 0.0, 48.8801)
		Hide object: units/dev_tools/editable_text_long/text_exit (-95.6493, 0.0, 51.6351)
		Hide object: units/dev_tools/editable_text_long/text_safes (-85.0937, -7.62939e-06, 49.2079)
		Hide object: units/dev_tools/editable_text_long/text_watch_clock (-81.1713, -1.29422e-06, 49.9318)
		´default´ (DELAY 0.2)
´exit´ ElementInstanceInput 100021
	Upon mission event "Exit":
		Hide object: units/dev_tools/editable_text_long/text_open_safe (-93.2076, 0.0, 40.8801)
		Hide object: units/dev_tools/editable_text_long/text_tazer_drone (-92.8664, 0.0, 57.4019)
		Hide object: units/dev_tools/editable_text_long/text_dont_slip (-500.795, 0.0, 45.6182)
		Hide object: units/dev_tools/editable_text_long/text_lance (-80.2076, 0.0, 48.8801)
		Hide object: units/dev_tools/editable_text_long/text_exit (-95.6493, 0.0, 51.6351)
		Hide object: units/dev_tools/editable_text_long/text_safes (-85.0937, -7.62939e-06, 49.2079)
		Hide object: units/dev_tools/editable_text_long/text_watch_clock (-81.1713, -1.29422e-06, 49.9318)
		´default´ (DELAY 0.2)
		Show object: units/dev_tools/editable_text_long/text_exit (-95.6493, 0.0, 51.6351) (DELAY 0.25)
´safes´ ElementInstanceInput 100023
	Upon mission event "safes":
		Hide object: units/dev_tools/editable_text_long/text_open_safe (-93.2076, 0.0, 40.8801)
		Hide object: units/dev_tools/editable_text_long/text_tazer_drone (-92.8664, 0.0, 57.4019)
		Hide object: units/dev_tools/editable_text_long/text_dont_slip (-500.795, 0.0, 45.6182)
		Hide object: units/dev_tools/editable_text_long/text_lance (-80.2076, 0.0, 48.8801)
		Hide object: units/dev_tools/editable_text_long/text_exit (-95.6493, 0.0, 51.6351)
		Hide object: units/dev_tools/editable_text_long/text_safes (-85.0937, -7.62939e-06, 49.2079)
		Hide object: units/dev_tools/editable_text_long/text_watch_clock (-81.1713, -1.29422e-06, 49.9318)
		´default´ (DELAY 0.2)
		Show object: units/dev_tools/editable_text_long/text_safes (-85.0937, -7.62939e-06, 49.2079) (DELAY 0.25)
