ID range vs continent name:
	100000: world

statics
	100025 units/payday2/props/gen_prop_square_goal_marker_8x15/001 (-3.6478e-05, 306.0, -8.0)
	100020 units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/heli (4.0, 386.0, 65.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.861, 0.584
			enabled True
			falloff_exponent 4
			far_range 150
			multiplier identity
			name lo_omni
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
		mesh_variation hover_land2
