ID range vs continent name:
	100000: world

statics
	100069 units/payday2/pickups/circuit_board/gen_pku_grab_circuit_board/001 (361.0, 234.0, 98.7846)
		mesh_variation hide
	100011 units/payday2/pickups/circuit_board/gen_pku_grab_circuit_board/002 (206.0, 317.0, 73.7846)
		mesh_variation hide
	100006 units/payday2/pickups/circuit_board/gen_pku_grab_circuit_board/003 (176.0, 333.0, 98.7846)
		mesh_variation hide
	100015 units/payday2/pickups/circuit_board/gen_pku_grab_circuit_board/004 (378.0, 344.0, 98.7846)
		mesh_variation hide
	100018 units/payday2/pickups/circuit_board/gen_pku_grab_circuit_board/005 (393.0, 294.0, 98.7846)
		mesh_variation hide
	100019 units/payday2/pickups/circuit_board/gen_pku_grab_circuit_board/006 (388.0, 202.0, 25.0)
		mesh_variation hide
	100020 units/payday2/pickups/circuit_board/gen_pku_grab_circuit_board/007 (149.0, 332.0, 73.7846)
		mesh_variation hide
	100022 units/payday2/pickups/circuit_board/gen_pku_grab_circuit_board/008 (74.0, 332.0, 171.785)
		mesh_variation hide
	100024 units/payday2/pickups/circuit_board/gen_pku_grab_circuit_board/009 (228.0, 291.0, 25.0001)
		mesh_variation hide
	100025 units/payday2/pickups/circuit_board/gen_pku_grab_circuit_board/010 (103.0, 331.0, 100.0)
		mesh_variation hide
	100026 units/payday2/pickups/circuit_board/gen_pku_grab_circuit_board/011 (365.63, 294.227, 73.7846)
		mesh_variation hide
	100027 units/payday2/pickups/circuit_board/gen_pku_grab_circuit_board/012 (28.0, 331.0, 197.0)
		mesh_variation hide
	100028 units/payday2/pickups/circuit_board/gen_pku_grab_circuit_board/013 (406.0, 171.0, 25.0)
		mesh_variation hide
	100098 units/payday2/props/com_prop_vent_ceiling_b/001 (225.0, 225.0, 325.0)
	100062 units/payday2/props/lights/off_prop_officehigh_ceilinglight_01/001 (225.0, 325.0, 325.0)
	100012 units/payday2/props/off_prop_appliance_computer/right_computer (383.0, 266.0, 0.0)
	100009 units/payday2/props/off_prop_appliance_monitor/right_monitor (259.0, 324.0, 75.9439)
		mesh_variation on
	100013 units/payday2/props/off_prop_officehigh_chair_standard/right_chair (293.168, 278.073, 0.0)
	100100 units/payday2/props/pictures/lxa_prop_hallway_picture_01/left_wall_pic001 (25.0, 175.0, 132.181)
	100101 units/payday2/props/pictures/lxa_prop_hallway_picture_03/front_wall_pic001 (300.0, 375.0, 130.077)
	100102 units/payday2/props/pictures/lxa_prop_hallway_picture_04/front_wall_pic002 (189.0, 375.0, 130.1)
	100106 units/payday2/props/pictures/lxa_prop_hallway_picture_05/front_wall_pic003 (243.0, 375.0, 130.051)
	100103 units/payday2/props/pictures/lxa_prop_hallway_picture_05/left_wall_pic002 (25.0, 175.0, 129.237)
	100014 units/payday2/props/stn_prop_office_deskset_right_a/001 (350.0, 200.0, 0.0)
	100008 units/payday2/props/stn_prop_office_filecabinet_a_02/001 (140.0, 300.0, 0.0)
	100007 units/payday2/props/stn_prop_office_filecabinet_a_02/002 (102.0, 300.0, 0.0)
	100010 units/payday2/props/stn_prop_office_filecabinet_d_02/001 (64.0, 301.0, 0.0)
	100021 units/payday2/props/stn_prop_office_filecabinet_d_02/002 (26.0, 301.0, 0.0)
	100094 units/pd2_dlc2/architecture/gov_d_int_ceiling_tile_b_vent_4x4m/001 (25.0, 25.0, 325.0)
	100005 units/pd2_dlc2/architecture/gov_d_int_door_a/001 (100.0, 25.0, 0.0)
		disable_on_ai_graph True
		mesh_variation deactivate_door
	100001 units/pd2_dlc2/architecture/gov_d_int_floor_carpet_4x4m/001 (25.0, 0.0, 0.0)
	100000 units/pd2_dlc2/architecture/gov_d_int_wall_a_4m/001 (0.0, 0.0, 0.0)
	100002 units/pd2_dlc2/architecture/gov_d_int_wall_a_4m/002 (425.0, 375.0, 0.0)
	100003 units/pd2_dlc2/architecture/gov_d_int_wall_a_4m/003 (450.0, 400.0, 0.0)
	100004 units/pd2_dlc2/architecture/gov_d_int_wall_door_4m_a/001 (25.0, 24.9998, 0.0)
	100104 units/pd2_dlc2/props/bnk_prop_lobby_plant_dracaenafragrans_b/plant_001 (62.0, 64.0, 2.5)
	100105 units/pd2_dlc2/props/bnk_prop_lobby_plant_dracaenafragrans_b/plant_002 (386.0, 61.0, 2.5)
