ID range vs continent name:
	100000: world

statics
	100019 units/dev_tools/level_tools/dev_door_blocker_1x2x3/001 (50.0, -25.0, 0.0)
		disable_on_ai_graph True
	100001 units/payday2/equipment/gen_interactable_drill_small_no_jam/001 (-77.0, -6.0, 84.0)
	100002 units/payday2/equipment/gen_interactable_drill_small_no_jam/002 (-92.0, -1.00001, 84.0)
	100000 units/pd2_dlc_casino/environment/cas_int/cas_int_fence_door/001 (0.0, 0.0, 0.0)
		mesh_variation state_door_open
