﻿´STARTUP_´ ElementUnitSequence 100001
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Execute sequence: "hide_all" - units/pd2_dlc_casino/props/cas_prop_fireworks/fireworks (0.0, 0.0, 0.0)
	Execute sequence: "hide" - units/pd2_dlc_casino/dev/cas_gen_square_drop_marker_2x3_fireworks/fireworks_drop_marker (0.0, 0.0, 0.0)
	Execute sequence: "interaction_disable" - units/pd2_dlc_casino/dev/invisible_interaction_ignite_fireworks/fireworks_ignition (25.0, 0.0, 25.0)
´input_show_drop_marker´ ElementInstanceInput 100005
	Upon mission event "show_drop_marker":
		Execute sequence: "show" - units/pd2_dlc_casino/dev/cas_gen_square_drop_marker_2x3_fireworks/fireworks_drop_marker (0.0, 0.0, 0.0)
		Toggle on: ´trg_fireworks´
´trg_fireworks´ ElementAreaTrigger 100015
	DISABLED
	TRIGGER TIMES 1
	Box, width 200.0, height 200.0, depth 300.0, at position (0.0, 0.0, 100.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: loot
	Interval: 0.1
	Rule: ´dtr_fireworks´
	Upon on_enter, execute:
		Send instance event "fireworks_dropped" to mission.
		Execute sequence: "show_open" - units/pd2_dlc_casino/props/cas_prop_fireworks/fireworks (0.0, 0.0, 0.0)
		Execute sequence: "hide" - units/pd2_dlc_casino/dev/cas_gen_square_drop_marker_2x3_fireworks/fireworks_drop_marker (0.0, 0.0, 0.0)
		Execute sequence: "interaction_enable" - units/pd2_dlc_casino/dev/invisible_interaction_ignite_fireworks/fireworks_ignition (25.0, 0.0, 25.0)
		Execute sequence: "enable_interaction" - units/pd2_dlc_casino/props/cas_prop_fireworks/fireworks (0.0, 0.0, 0.0)
		Execute sequence: "enable_contour" - units/pd2_dlc_casino/props/cas_prop_fireworks/fireworks (0.0, 0.0, 0.0)
		Execute sequence: "disable_interaction" - units/pd2_dlc_casino/props/cas_prop_fireworks/fireworks (0.0, 0.0, 0.0)
		remove ´wp_drop_fireworks´
		If instigator loot type is "fireworks":
			Loot action: remove
´dtr_fireworks´ ElementInstigatorRule 100016
	fireworks
´wp_drop_fireworks´ ElementWaypoint 100003
	Place waypoint with icon "pd2_goto" at position (0.0, 0.0, 120.0)
´seq_trg_ignition_INTERACT´ ElementUnitSequenceTrigger 100007
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_casino/dev/invisible_interaction_ignite_fireworks/fireworks_ignition (25.0, 0.0, 25.0):
		Send instance event "signal_activated" to mission.
		remove ´wp_ignite_fireworks´
		Execute sequence: "burning" - units/pd2_dlc_casino/props/cas_prop_fireworks/fireworks (0.0, 0.0, 0.0)
		Execute sequence: "disable_contour" - units/pd2_dlc_casino/props/cas_prop_fireworks/fireworks (0.0, 0.0, 0.0)
		Execute sequence: "disable_interaction" - units/pd2_dlc_casino/props/cas_prop_fireworks/fireworks (0.0, 0.0, 0.0)
´wp_ignite_fireworks´ ElementWaypoint 100021
	Place waypoint with icon "pd2_fire" at position (0.0, 0.0, 120.0)
´input_show_WP_drop´ ElementInstanceInput 100009
	Upon mission event "show_WP_drop":
		´wp_drop_fireworks´ (DELAY 0.5)
´input_show_WP_ignite´ ElementInstanceInput 100025
	Upon mission event "show_WP_ignite":
		´wp_ignite_fireworks´
