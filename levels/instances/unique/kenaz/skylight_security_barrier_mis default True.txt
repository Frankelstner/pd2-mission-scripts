﻿´input_close_security_gates´ ElementInstanceInput 100020
	Upon mission event "close_security_gates":
		Execute sequence: "anim_close" - units/pd2_dlc_casino/props/cas_prop_skylight_lock/skylight_security_barrier_TEMP (175.0, 75.0, 243.001)
´input_open_security_gates´ ElementInstanceInput 100022
	Upon mission event "open_security_gates":
		Execute sequence: "anim_open" - units/pd2_dlc_casino/props/cas_prop_skylight_lock/skylight_security_barrier_TEMP (175.0, 75.0, 243.001)
´input_skylight_destroy´ ElementInstanceInput 100000
	Upon mission event "skylight_destroy":
		Execute sequence: "destroy" - units/pd2_dlc_casino/environment/cas_int/cas_int_skylight_main_hall/001 (850.0, 675.0, 275.0)
		Play effect effects/payday2/particles/window/office_divider_glass at position (125.0, -175.0, 275.0)
		Play effect effects/payday2/particles/window/office_divider_glass at position (475.0, 0.0, 275.0)
		Play effect effects/payday2/particles/window/office_divider_glass at position (125.0, 250.0, 275.0)
		Play effect effects/payday2/particles/window/office_divider_glass at position (-250.0, 0.0, 275.0)
		Play effect effects/payday2/particles/window/office_divider_glass at position (125.0, 50.0, 125.0)
		Play effect effects/payday2/particles/window/office_divider_glass at position (-150.0, 250.0, 275.0)
		Play effect effects/payday2/particles/window/office_divider_glass at position (-125.0, -175.0, 275.0)
		´point_feedback_001´
		Play effect effects/payday2/particles/window/office_divider_glass at position (421.0, 100.0, 191.0) (DELAY 0.5)
		Play effect effects/payday2/particles/window/office_divider_glass at position (-100.0, 100.0, 200.0) (DELAY 0.5)
		Play effect effects/payday2/particles/window/office_divider_glass at position (-150.0, -100.0, 150.0) (DELAY 0.5)
		Play effect effects/payday2/particles/window/office_divider_glass at position (311.0, 121.0, 319.0) (DELAY 0.5)
´point_feedback_001´ ElementFeedback 100035
	TRIGGER TIMES 1
	Screen shake.
