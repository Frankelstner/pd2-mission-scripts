ID range vs continent name:
	100000: world

statics
	100071 units/pd2_dlc_casino/architecture/boot_door/cas_toillet_door/001 (-407.0, -2012.0, 300.0)
	100182 units/pd2_dlc_casino/architecture/boot_door/cas_toillet_door/002 (-539.0, -2012.0, 300.0)
	100185 units/pd2_dlc_casino/architecture/boot_door/cas_toillet_door/003 (-673.0, -2012.0, 300.0)
	100202 units/pd2_dlc_casino/architecture/boot_door/cas_toillet_door/004 (-275.0, -2012.0, 300.0)
	100227 units/pd2_dlc_casino/architecture/boot_door/cas_toillet_door/006 (-419.0, 3270.0, 300.0)
	100229 units/pd2_dlc_casino/architecture/boot_door/cas_toillet_door/007 (-287.0, 3270.0, 300.0)
	100231 units/pd2_dlc_casino/architecture/boot_door/cas_toillet_door/008 (-153.0, 3270.0, 300.0)
	100140 units/pd2_dlc_casino/architecture/boot_door/cas_toillet_door/009 (-21.0002, 3270.0, 300.0)
	100318 units/pd2_dlc_casino/architecture/cas_int_toilet_01/001 (-75.0, -1200.0, 300.0)
	100028 units/pd2_dlc_casino/architecture/cas_int_toilet_02/001 (-225.0, 2950.0, 300.0)
	100164 units/pd2_dlc_casino/dev/cas_gen_square_drop_marker_2x3_watertank/watertank_drop_marker_left_01 (-598.0, -1577.0, 292.0)
	100268 units/pd2_dlc_casino/dev/cas_gen_square_drop_marker_2x3_watertank/watertank_drop_marker_left_02 (-398.0, -1352.0, 292.0)
	100139 units/pd2_dlc_casino/dev/cas_gen_square_drop_marker_2x3_watertank/watertank_drop_marker_right_01 (-498.0, 2598.0, 292.0)
	100273 units/pd2_dlc_casino/dev/cas_gen_square_drop_marker_2x3_watertank/watertank_drop_marker_right_02 (-498.0, 2873.0, 292.0)
	100225 units/pd2_dlc_casino/environment/cas_int/cas_walls/casino_toillet_wall/cas_toillet_boot_wall/007 (-476.0, 3452.0, 300.0)
	100228 units/pd2_dlc_casino/environment/cas_int/cas_walls/casino_toillet_wall/cas_toillet_boot_wall/008 (-344.0, 3452.0, 300.0)
	100230 units/pd2_dlc_casino/environment/cas_int/cas_walls/casino_toillet_wall/cas_toillet_boot_wall/009 (-210.0, 3452.0, 300.0)
	100232 units/pd2_dlc_casino/environment/cas_int/cas_walls/casino_toillet_wall/cas_toillet_boot_wall/010 (-78.0002, 3452.0, 300.0)
	100142 units/pd2_dlc_casino/environment/cas_int/cas_walls/casino_toillet_wall/cas_toillet_boot_wall/011 (53.9998, 3452.0, 300.0)
	100014 units/pd2_dlc_casino/environment/cas_int/lavatory/cas_toil_lavatory/002 (-750.0, -1325.0, 300.0)
	100017 units/pd2_dlc_casino/environment/cas_int/lavatory/cas_toil_lavatory/003 (-750.0, -1425.0, 300.0)
	100018 units/pd2_dlc_casino/environment/cas_int/lavatory/cas_toil_lavatory/004 (-750.0, -1525.0, 300.0)
	100022 units/pd2_dlc_casino/environment/cas_int/lavatory/cas_toil_lavatory/005 (-750.0, -1625.0, 300.0)
	100144 units/pd2_dlc_casino/environment/cas_int/lavatory/cas_toil_lavatory/006 (-625.0, 2675.0, 300.0)
	100145 units/pd2_dlc_casino/environment/cas_int/lavatory/cas_toil_lavatory/007 (-625.0, 2775.0, 300.0)
	100146 units/pd2_dlc_casino/environment/cas_int/lavatory/cas_toil_lavatory/008 (-625.0, 2875.0, 300.0)
	100147 units/pd2_dlc_casino/environment/cas_int/lavatory/cas_toil_lavatory/009 (-625.0, 2975.0, 300.0)
	100019 units/pd2_dlc_casino/environment/cas_int/lavatory/cas_toil_lavatory/010 (-550.0, -1200.0, 300.0)
	100020 units/pd2_dlc_casino/environment/cas_int/lavatory/cas_toil_lavatory/011 (-450.0, -1200.0, 300.0)
	100218 units/pd2_dlc_casino/environment/cas_int/lavatory/cas_toil_lavatory/012 (-350.0, -1200.0, 300.0)
	100148 units/pd2_dlc_casino/environment/cas_int/lavatory/cas_toil_lavatory/013 (-625.0, 2575.0, 300.0)
	100161 units/pd2_dlc_casino/environment/cas_int/lavatory/cas_toil_lavatory/014 (-250.0, -1200.0, 300.0)
	100221 units/pd2_dlc_casino/props/cas_prop_urinal/001 (-75.0, -2200.0, 300.0)
	100025 units/pd2_dlc_casino/props/cas_prop_urinal/002 (25.0, -2200.0, 300.0)
	100260 units/pd2_dlc_casino/props/cas_prop_urinal/003 (125.0, -2200.0, 300.0)
	100034 units/pd2_dlc_casino/props/cas_prop_urinal/004 (250.0, -2025.0, 300.0)
	100038 units/pd2_dlc_casino/props/cas_prop_urinal/005 (250.0, -1925.0, 300.0)
	100261 units/pd2_dlc_casino/props/cas_prop_urinal/006 (250.0, -1825.0, 300.0)
	100246 units/pd2_dlc_casino/props/cas_prop_water_hose/water_hose_left_01 (-750.0, -1525.0, 300.0)
	100270 units/pd2_dlc_casino/props/cas_prop_water_hose/water_hose_left_02 (-350.52, -1201.11, 300.0)
	100245 units/pd2_dlc_casino/props/cas_prop_water_hose/water_hose_right_01 (-625.0, 2675.0, 300.0)
	100272 units/pd2_dlc_casino/props/cas_prop_water_hose/water_hose_right_02 (-625.0, 2875.0, 300.0)
	100165 units/pd2_dlc_casino/props/drill/cas_prop_watertank_static/watertank_left_01 (-631.0, -1577.0, 300.0)
	100269 units/pd2_dlc_casino/props/drill/cas_prop_watertank_static/watertank_left_02 (-402.52, -1320.11, 300.0)
	100155 units/pd2_dlc_casino/props/drill/cas_prop_watertank_static/watertank_right_01 (-506.0, 2623.0, 300.0)
	100271 units/pd2_dlc_casino/props/drill/cas_prop_watertank_static/watertank_right_02 (-506.0, 2823.0, 300.0)
	100027 units/pd2_dlc_casino/props/prop_toillet/cas_toillet/001 (-351.0, -2199.0, 295.0)
	100183 units/pd2_dlc_casino/props/prop_toillet/cas_toillet/002 (-483.0, -2199.0, 295.0)
	100186 units/pd2_dlc_casino/props/prop_toillet/cas_toillet/003 (-617.0, -2199.0, 295.0)
	100203 units/pd2_dlc_casino/props/prop_toillet/cas_toillet/004 (-219.0, -2199.0, 295.0)
	100141 units/pd2_dlc_casino/props/prop_toillet/cas_toillet/009 (-75.0, 3449.0, 294.0)
	100120 units/pd2_dlc_casino/props/prop_toillet/cas_toillet/010 (-215.0, 3449.0, 294.0)
	100204 units/pd2_dlc_casino/props/prop_toillet/cas_toillet/011 (-340.0, 3449.0, 294.0)
	100208 units/pd2_dlc_casino/props/prop_toillet/cas_toillet/012 (-480.0, 3449.0, 294.0)
