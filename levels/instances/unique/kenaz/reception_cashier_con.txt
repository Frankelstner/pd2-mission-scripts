ID range vs continent name:
	100000: world

statics
	100084 units/dev_tools/level_tools/dev_ai_vis_blocker_005x1x2m/007 (300.0, 64.0, 3.20141)
	100121 units/dev_tools/level_tools/dev_ai_vis_blocker_005x1x2m/009 (481.0, -225.0, 9.0675)
	100113 units/dev_tools/level_tools/dev_ai_vis_blocker_005x1x2m/010 (481.0, 100.0, 9.0675)
	100051 units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-25.0, 350.0, 0.0)
	100194 units/dev_tools/level_tools/dev_door_blocker_1x1x3/002 (-25.0, -450.0, 0.0)
	100196 units/dev_tools/level_tools/dev_door_blocker_1x1x3/003 (-275.0, -575.0, 0.0)
	100197 units/dev_tools/level_tools/dev_door_blocker_1x1x3/004 (-275.0, 475.0, 0.0)
	100074 units/dev_tools/level_tools/dev_nav_blocker_1x3m/001 (25.0, 200.0, 0.0)
	100289 units/dev_tools/level_tools/dev_nav_blocker_1x3m/003 (25.0, -325.0, 0.0)
	100290 units/dev_tools/level_tools/dev_nav_blocker_1x3m/004 (25.0, -575.0, 0.0)
	100143 units/dev_tools/level_tools/dev_nav_blocker_2x3m/004 (-200.0, 875.0, -87.5)
	100144 units/dev_tools/level_tools/dev_nav_blocker_2x3m/005 (-25.0, 900.0, -87.5)
	100146 units/dev_tools/level_tools/dev_nav_blocker_2x3m/006 (200.241, 779.164, -87.5)
	100147 units/dev_tools/level_tools/dev_nav_blocker_2x3m/007 (0.0, -925.0, -87.5)
	100229 units/dev_tools/level_tools/dev_nav_blocker_2x3m/008 (-200.0, -900.0, -87.5)
	100020 units/dev_tools/level_tools/dev_nav_blocker_2x3m/009 (175.0, -950.0, -87.5)
	100232 units/dev_tools/level_tools/dev_nav_blocker_2x3m/010 (281.0, -748.0, -87.5)
	100287 units/dev_tools/level_tools/dev_nav_blocker_2x3m/011 (254.0, 525.0, -87.5)
	100018 units/dev_tools/level_tools/dev_nav_blocker_2x3m/012 (100.0, 925.0, -87.5)
	100021 units/dev_tools/level_tools/dev_nav_blocker_2x3m/013 (275.0, -556.0, -87.5)
	100022 units/dev_tools/level_tools/dev_nav_blocker_2x3m/014 (265.847, 703.693, -87.5)
	100023 units/dev_tools/level_tools/dev_nav_blocker_2x3m/015 (230.815, -803.736, -87.5)
	100076 units/payday2/equipment/gen_equipment_security_camera/security_camera_002 (16.0, 0.0, 247.0)
	100016 units/payday2/props/bnk_prop_office_chair_visitor/009 (-189.035, 935.097, -1.9978)
	100137 units/payday2/props/bnk_prop_office_chair_visitor/011 (268.26, -307.729, 0.00205761)
	100234 units/payday2/props/bnk_prop_office_chair_visitor/012 (-194.035, -898.903, -1.9978)
	100129 units/payday2/props/bnk_prop_teller_pen/006 (1015.0, 148.0, 99.4)
	100191 units/payday2/props/bnk_prop_teller_pen/007 (1020.26, -104.524, 99.4)
	100011 units/payday2/props/off_prop_appliance_computer/009 (-251.0, 987.0, -2.0)
	100067 units/payday2/props/off_prop_appliance_computer/010 (325.548, 295.85, -0.7682)
	100150 units/payday2/props/off_prop_appliance_computer/011 (326.604, -420.907, -0.76819)
	100238 units/payday2/props/off_prop_appliance_computer/012 (-261.0, -990.0, 0.0)
	100013 units/payday2/props/shelves/bnk_prop_office_desk_top_straight/010 (-200.0, 1000.0, -1.9978)
	100065 units/payday2/props/shelves/bnk_prop_office_desk_top_straight/011 (338.548, 350.85, -1.97223)
	100153 units/payday2/props/shelves/bnk_prop_office_desk_top_straight/012 (338.604, -364.907, -0.972211)
	100249 units/payday2/props/shelves/bnk_prop_office_desk_top_straight/013 (-205.0, -1000.0, -1.9978)
	100046 units/pd2_dlc_arena/props/editable_text_caslon/001 (18.0001, 47.0, 213.11)
		align left
		alpha 1
		blend_mode normal
		font fonts/font_caslon
		font_color 1.0, 1.0, 1.0
		font_size 0.75
		render_template diffuse_vc_decal
		shape
			1 0
			2 0
			3 1
			4 1
		text RECEPTION
		vertical center
		word_wrap False
		wrap False
	100017 units/pd2_dlc_casino/architecture/cas_int_arch_left_pillar/009 (3.03308, 1111.61, -1.92014)
	100043 units/pd2_dlc_casino/architecture/cas_int_arch_left_pillar/010 (359.737, 764.263, -2.00001)
	100245 units/pd2_dlc_casino/architecture/cas_int_arch_left_pillar/011 (427.297, -687.347, -2.0)
	100270 units/pd2_dlc_casino/architecture/cas_int_arch_left_pillar/012 (73.7438, -1040.9, -2.0)
	100019 units/pd2_dlc_casino/architecture/cas_int_lobby_pillar_5m/007 (75.865, 1074.14, 0.0)
	100045 units/pd2_dlc_casino/architecture/cas_int_lobby_pillar_5m/008 (387.699, 762.302, 0.0)
	100087 units/pd2_dlc_casino/architecture/cas_int_lobby_pillar_5m/013 (660.0, 500.0, 0.0)
	100239 units/pd2_dlc_casino/architecture/cas_int_lobby_pillar_5m/014 (660.0, -500.0, 0.0)
	100252 units/pd2_dlc_casino/architecture/cas_int_lobby_pillar_5m/015 (386.127, -763.873, 4.47063)
	100274 units/pd2_dlc_casino/architecture/cas_int_lobby_pillar_5m/016 (78.5355, -1071.46, 4.47063)
	100034 units/pd2_dlc_casino/architecture/cas_int_lobby_pillar_5m/017 (660.0, -500.0, 500.0)
	100035 units/pd2_dlc_casino/architecture/cas_int_lobby_pillar_5m/018 (660.0, 500.0, 500.0)
	100001 units/pd2_dlc_casino/architecture/cas_int_reception_bd/001 (23.6325, 992.017, 400.0)
	100002 units/pd2_dlc_casino/architecture/cas_int_reception_bd/002 (165.054, 850.596, 400.0)
	100003 units/pd2_dlc_casino/architecture/cas_int_reception_bd/003 (306.475, -707.868, 400.0)
	100004 units/pd2_dlc_casino/architecture/cas_int_reception_bd/004 (165.054, -849.289, 400.0)
	100028 units/pd2_dlc_casino/architecture/cas_int_reception_counter/007 (232.136, 917.864, -1.0)
	100044 units/pd2_dlc_casino/architecture/cas_int_reception_counter/008 (373.557, 776.442, -1.0)
	100090 units/pd2_dlc_casino/architecture/cas_int_reception_counter/013 (475.0, 300.0, 0.0)
	100133 units/pd2_dlc_casino/architecture/cas_int_reception_counter/014 (475.0, -125.0, 0.0)
	100262 units/pd2_dlc_casino/architecture/cas_int_reception_counter/015 (232.843, -917.157, -2.0)
	100273 units/pd2_dlc_casino/architecture/cas_int_reception_counter/016 (91.4216, -1058.58, -2.0)
	100037 units/pd2_dlc_casino/architecture/cas_int_wall_patch_01/001 (646.0, -393.0, 0.0)
	100038 units/pd2_dlc_casino/architecture/cas_int_wall_patch_01/002 (644.0, 404.0, 0.0)
	100039 units/pd2_dlc_casino/architecture/cas_int_wall_patch_01/003 (379.828, 636.172, 0.0)
	100024 units/pd2_dlc_casino/architecture/reception_cashier/001 (-450.0, 100.0, 0.0)
	100288 units/pd2_dlc_casino/environment/cas_int/lobby/cas_int_reception_02/001 (1082.0, -2.00007, 0.0)
	100093 units/pd2_dlc_casino/props/cart/cas_prop_lobby_luggage/007 (250.0, 75.0, 3.20141)
	100106 units/pd2_dlc_casino/props/cart/cas_prop_lobby_luggage/008 (300.0, 14.0, 3.20141)
	100108 units/pd2_dlc_casino/props/cart/cas_prop_lobby_luggage/009 (300.0, 14.0, 3.20141)
	100036 units/pd2_dlc_casino/props/cas_bedroom_door/007 (-225.0, 511.0, -1.0)
		disable_on_ai_graph True
		mesh_variation open_door
	100083 units/pd2_dlc_casino/props/cas_bedroom_door/008 (37.0006, -400.0, 0.0)
		disable_on_ai_graph True
		mesh_variation open_door
	100119 units/pd2_dlc_casino/props/cas_bedroom_door/009 (-225.0, -511.0, -1.0)
		disable_on_ai_graph True
		mesh_variation open_door
	100063 units/pd2_dlc_casino/props/cas_bedroom_door/010 (37.0006, 400.0, 0.0)
		disable_on_ai_graph True
		mesh_variation open_door
	100103 units/pd2_dlc_casino/props/cas_prop_int_plant_tall/005 (442.0, 139.0, -1.79859)
	100135 units/pd2_dlc_casino/props/cas_prop_int_plant_tall/006 (442.0, -157.0, -0.79859)
	100026 units/pd2_dlc_casino/props/cas_prop_lobby_counter/009 (232.843, 917.158, 100.0)
	100040 units/pd2_dlc_casino/props/cas_prop_lobby_counter/010 (374.264, 775.736, 100.0)
	100258 units/pd2_dlc_casino/props/cas_prop_lobby_counter/011 (215.165, -899.479, 100.0)
	100263 units/pd2_dlc_casino/props/cas_prop_lobby_counter/012 (73.7439, -1040.9, 100.0)
	100050 units/pd2_dlc_casino/props/cas_prop_lobby_counter/013 (763.948, -448.422, 100.0)
	100052 units/pd2_dlc_casino/props/cas_prop_lobby_counter/014 (923.675, -328.059, 100.0)
	100059 units/pd2_dlc_casino/props/cas_prop_lobby_counter/015 (1014.47, -149.858, 100.0)
	100060 units/pd2_dlc_casino/props/cas_prop_lobby_counter/016 (1035.38, 49.0465, 100.0)
	100062 units/pd2_dlc_casino/props/cas_prop_lobby_counter/017 (980.252, 241.299, 100.0)
	100068 units/pd2_dlc_casino/props/cas_prop_lobby_counter/018 (846.425, 389.928, 100.0)
	100107 units/pd2_dlc_casino/props/cas_prop_lobby_luggage_cart/004 (300.0, 14.0, 3.20141)
	100072 units/pd2_dlc_casino/props/cas_prop_lobby_rose_tree/005 (0.0, 100.0, -0.79859)
	100095 units/pd2_dlc_casino/props/cas_prop_lobby_rose_tree/006 (0.0, -100.0, -0.79859)
	100145 units/pd2_dlc_casino/props/cas_prop_lobby_rose_vase/006 (501.0, -188.0, 105.361)
	100006 units/pd2_dlc_casino/props/cas_prop_security_computer/computer_cashier_left (-268.0, 989.0, 75.2318)
	100237 units/pd2_dlc_casino/props/cas_prop_security_computer/computer_cashier_right (-266.0, -991.0, 77.0)
	100055 units/pd2_dlc_casino/props/cas_prop_security_computer/computer_reception_left (331.095, 345.751, 74.0022)
	100139 units/pd2_dlc_casino/props/cas_prop_security_computer/computer_reception_right (328.673, -350.711, 75.2573)
	100053 units/pd2_dlc_casino/props/cas_prop_terrace_bin/003 (0.0, 275.0, -1.30234)
	100030 units/pd2_dlc_casino/props/cas_prop_terrace_bin/004 (0.0, -275.0, -1.30234)
	100007 units/pd2_dlc_casino/props/cas_usb_key_data/usb_cashier_left (-262.657, 961.514, 75.2318)
	100235 units/pd2_dlc_casino/props/cas_usb_key_data/usb_cashier_right (-272.774, -963.832, 77.0)
	100057 units/pd2_dlc_casino/props/cas_usb_key_data/usb_reception_left (303.095, 343.751, 74.0022)
	100132 units/pd2_dlc_casino/props/cas_usb_key_data/usb_reception_right (300.673, -350.711, 75.2573)
	100058 units/pd2_dlc_casino/props/door/cas_bedroom_double_door/door_main_entrance (-437.0, -1.0, 0.0)
		mesh_variation open_door
	100041 units/pd2_dlc_casino/props/hanging_wall/cas_prop_lobby_sign_wall/003 (25.0001, 1.0, 197.11)
	100102 units/pd2_dlc_casino/props/wall/cas_prop_lobby_sign_hanging/007 (472.0, 100.0, 220.361)
	100125 units/pd2_dlc_casino/props/wall/cas_prop_lobby_sign_hanging/008 (472.0, -125.0, 220.361)
	100012 units/world/props/bank/money_wrap_single_bundle/015 (-128.059, 975.669, 75.002)
		mesh_variation interact_enable
	100015 units/world/props/bank/money_wrap_single_bundle/016 (-105.57, 979.424, 75.002)
		mesh_variation interact_enable
	100071 units/world/props/bank/money_wrap_single_bundle/017 (314.612, 257.306, 75.002)
		mesh_variation interact_enable
	100128 units/world/props/bank/money_wrap_single_bundle/018 (321.834, -256.642, 76.002)
		mesh_variation interact_enable
	100167 units/world/props/bank/money_wrap_single_bundle/019 (319.633, -273.967, 76.002)
		mesh_variation interact_enable
	100240 units/world/props/bank/money_wrap_single_bundle/022 (-175.0, -986.0, 77.0)
		mesh_variation interact_enable
	100241 units/world/props/bank/money_wrap_single_bundle/023 (-159.886, -980.5, 77.0)
		mesh_variation interact_enable
	100243 units/world/props/bank/money_wrap_single_bundle/024 (-132.547, -977.671, 76.9998)
		mesh_variation interact_enable
	100244 units/world/props/bank/money_wrap_single_bundle/025 (-106.271, -974.949, 76.9998)
		mesh_variation interact_enable
	100056 units/world/props/bank/paintings/suburbia_painting_25/003 (276.0, 503.0, 169.514)
	100064 units/world/props/gym/stn_prop_fan_ceiling/007 (225.0, 225.0, 300.0)
		projection_light ls_light
	100094 units/world/props/gym/stn_prop_fan_ceiling/008 (275.0, -25.0, 300.0)
		projection_light ls_light
	100126 units/world/props/gym/stn_prop_fan_ceiling/009 (175.0, -225.0, 300.0)
		projection_light ls_light
	100054 units/world/props/office/airconditioner_01/003 (250.0, 500.0, 258.621)
