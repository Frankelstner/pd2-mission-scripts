﻿´startup_storage_room´ MissionScriptElement 100038
	EXECUTE ON STARTUP
	´func_nav_obstacle_storage_rooms_ADD´
	Execute sequence: "hide" - units/pd2_dlc_arena/dev/are_gen_square_goal_marker_2_4x4_35/escape_marker (-397.0, -413.0, -6.50023) (DELAY 1)
	´func_nav_obstacle_storage_rooms_ADD´ (DELAY 1)
	´seq_garage_door_open´ (DELAY 1)
´func_nav_obstacle_storage_rooms_ADD´ ElementNavObstacle 100056
	TRIGGER TIMES 1
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-199.999, -74.9975, 0.00457764)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/002 (-299.999, -74.9977, 0.00457764)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/003 (-399.999, -74.9978, 0.00457764)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/004 (-499.999, -74.9979, 0.00457764)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/005 (-574.999, -74.998, 0.00457764)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/006 (-574.999, -549.998, 0.0037486)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/007 (-549.999, -474.998, 0.0038795)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/008 (-549.999, -374.998, 0.00405404)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/009 (-574.999, -274.998, 0.00422857)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/010 (-574.999, -174.998, 0.0044031)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/011 (125.002, -849.997, 0.003225)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/012 (25.0019, -849.997, 0.003225)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/013 (-74.9981, -849.997, 0.003225)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/014 (-174.998, -849.997, 0.003225)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/015 (-274.998, -849.997, 0.003225)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/016 (-374.998, -849.998, 0.003225)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/017 (-474.998, -849.998, 0.003225)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/018 (-574.998, -849.998, 0.003225)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/019 (-574.998, -749.998, 0.00339954)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/020 (-574.998, -649.998, 0.00357407)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/021 (175.001, -74.9969, 0.00457764)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/022 (275.001, -99.9967, 0.004534)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/023 (275.001, -149.997, 0.00444674)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/024 (275.001, -249.997, 0.0042722)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/025 (275.001, -349.997, 0.00409767)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/026 (275.001, -449.997, 0.00392314)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/027 (275.001, -549.997, 0.00374861)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/028 (275.002, -649.997, 0.00357407)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/029 (275.002, -749.997, 0.00339954)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/030 (225.002, -849.997, 0.00322501)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/034 (-324.998, -574.997, 0.00370497)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/035 (-399.998, -674.997, 0.00353044)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/036 (-274.998, -674.997, 0.00353044)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/037 (-174.998, -674.997, 0.00353044)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/038 (-324.998, -749.997, 0.00339954)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/039 (-174.998, -749.997, 0.00339954)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/040 (150.001, -74.9969, 0.00457764)
´seq_garage_door_open´ ElementUnitSequence 100024
	Execute sequence: "open_doors" - units/payday2/architecture/pox/com_int_parking_garage_door/garage_door (-200.0, -824.998, -6.66016)
´input_show_drill_parts_WP´ ElementInstanceInput 100221
	Upon mission event "show_drill_parts_WP":
		´wp_drill_parts´
´wp_drill_parts´ ElementWaypoint 100220
	Place waypoint with icon "pd2_fix" at position (-711.0, -67.0, 68.4701)
´input_remove_drill_parts_WP´ ElementInstanceInput 100222
	Upon mission event "remove_drill_parts_WP":
		remove ´wp_drill_parts´
´input_close_garage_door´ ElementInstanceInput 100105
	Upon mission event "close_garage_door":
		Toggle off: ´seq_garage_door_open´
		´seq_close_garage_door´
´seq_close_garage_door´ ElementUnitSequence 100106
	Execute sequence: "close_doors" - units/payday2/architecture/pox/com_int_parking_garage_door/garage_door (-200.0, -824.998, -6.66016)
	Execute sequence: "anim_door_rear_both_close" - units/payday2/vehicles/str_vehicle_van_player/001 (9.0, -1080.0, -19.4999)
	Toggle off: ´trg_invalid_loot´
	Toggle off: ´trg_loot_drop´
	Toggle off: ´trg_mandatory_loot´
	remove ´wp_lootdrop´
	Execute sequence: "hide" - units/pd2_dlc_arena/dev/are_gen_square_goal_marker_2_4x4_35/escape_marker (-397.0, -413.0, -6.50023)
´trg_invalid_loot´ ElementAreaTrigger 100074
	Box, width 200.0, height 200.0, depth 400.0, at position (17.0, -1011.0, 145.5) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: loot
	Interval: 0.1
	Rule: ´dir_invalid_loot´
	Upon on_enter, execute:
		If instigator loot type is "fireworks":
			Loot action: add_to_respawn
			´fireworks´
		If instigator loot type is "winch_part":
			Loot action: add_to_respawn
			´winch_part´
		If instigator loot type is "winch_part_e3":
			Loot action: add_to_respawn
			´winch_part_e3´
		If instigator loot type is "watertank_empty":
			Loot action: add_to_respawn
			´watertank_empty´
		If instigator loot type is "watertank_full":
			Loot action: add_to_respawn
			´watertank_full´
´trg_loot_drop´ ElementAreaTrigger 100032
	Box, width 200.0, height 200.0, depth 400.0, at position (17.0, -1011.0, 145.5) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: loot
	Interval: 0.1
	Rule: ´dir_valid_loot´
	Upon on_enter, execute:
		Loot action: secure
		Loot action: remove
		Execute sequence: "state_add_loot_bag" - units/payday2/vehicles/str_vehicle_van_player/001 (9.0, -1080.0, -19.4999)
´trg_mandatory_loot´ ElementAreaTrigger 100059
	Box, width 200.0, height 200.0, depth 400.0, at position (17.0, -1011.0, 145.5) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: loot
	Interval: 0.1
	Rule: ´dir_unknown_loot´
	Upon on_enter, execute:
		Loot action: secure
		Loot action: remove
		Execute sequence: "state_add_loot_bag" - units/payday2/vehicles/str_vehicle_van_player/001 (9.0, -1080.0, -19.4999)
		Execute sequence: "show" - units/pd2_dlc_arena/dev/are_gen_square_goal_marker_2_4x4_35/escape_marker (-397.0, -413.0, -6.50023)
		Toggle on: ´trg_escape´
		Place waypoint with icon "pd2_escape" at position (-359.0, -413.0, 144.482)
		Send instance event "mandatory_loot_secured" to mission.
´wp_lootdrop´ ElementWaypoint 100093
	Place waypoint with icon "pd2_lootdrop" at position (3.0, -836.0, 114.5)
´dir_invalid_loot´ ElementInstigatorRule 100070
	fireworks,watertank_empty,watertank_full,winch_part,winch_part_e3
´fireworks´ ElementLootBag 100083
	Spawn loot bag "fireworks" at position (77.0, -573.0, 17.4821)
´winch_part´ ElementLootBag 100087
	Spawn loot bag "winch_part" at position (-35.0, -573.0, 17.4821)
´winch_part_e3´ ElementLootBag 100089
	Spawn loot bag "winch_part_e3" at position (80.0, -646.0, 17.4821)
´watertank_empty´ ElementLootBag 100092
	Spawn loot bag "watertank_empty" at position (-35.0, -646.0, 17.4821)
´watertank_full´ ElementLootBag 100090
	Spawn loot bag "watertank_full" at position (80.0, -717.0, 17.4821)
´dir_valid_loot´ ElementInstigatorRule 100037
	ammo,artifact_statue,cage_bag,circuit,coke,coke_pure,cro_loot1,cro_loot2,diamonds,engine_01,engine_02,engine_03,engine_04,engine_05,engine_06,engine_07,engine_08,engine_09,engine_10,engine_11,engine_12,equipment_bag,evidence_bag,gold,grenades,hope_diamond,ladder_bag,lance_bag,lance_bag_large,meth,money,mus_artifact,mus_artifact_paint,painting,person,samurai_suit,sandwich,special_person,turret,vehicle_falcogini,warhead,weapon,weapons
´dir_unknown_loot´ ElementInstigatorRule 100066
	unknown
´trg_escape´ ElementAreaTrigger 100028
	DISABLED
	TRIGGER TIMES 1
	Box, width 400.0, height 200.0, depth 250.0, at position (-384.0, -413.0, 96.4821) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: all
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Send instance event "mission_end" to mission.
		End mission: success (DELAY 2)
´input_enable_lootdrop´ ElementInstanceInput 100027
	Upon mission event "enable_lootdrop":
		´wp_lootdrop´
´input_van_escape´ ElementInstanceInput 100078
	Upon mission event "van_escape":
		Toggle off: ´seq_close_garage_door´
