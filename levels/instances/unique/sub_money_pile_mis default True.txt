﻿´money_bag_counter´ ElementCounter 100013
	counter_target 0
	digital_gui_unit_ids
	instance_var_names
		counter_target total_money
´setup_money´ MissionScriptElement 100012
	EXECUTE ON STARTUP
	on_executed
		´hide_money´ (delay 0.1)
		´spawn_money_´ (delay 0.5)
		´spawn_money´ (delay 1)
´hide_money´ ElementUnitSequence 100016
	position 800.0, -850.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/001 (0.0, 0.0, 0.0)
			notify_unit_sequence hide
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/002 (0.0, 50.0, 0.0)
			notify_unit_sequence hide
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/003 (0.0, -50.0, 0.0)
			notify_unit_sequence hide
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/004 (-43.0, -50.0, 0.0)
			notify_unit_sequence hide
			time 0
		5
			id 5
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/005 (-43.0, 0.0, 0.0)
			notify_unit_sequence hide
			time 0
		6
			id 6
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/006 (-43.0, 50.0, 0.0)
			notify_unit_sequence hide
			time 0
		7
			id 7
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/007 (-43.0, 50.0, 27.0)
			notify_unit_sequence hide
			time 0
		8
			id 8
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/008 (0.0, 50.0, 27.0)
			notify_unit_sequence hide
			time 0
		9
			id 9
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/009 (0.0, 0.0, 27.0)
			notify_unit_sequence hide
			time 0
		10
			id 10
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/010 (-43.0, 0.0, 27.0)
			notify_unit_sequence hide
			time 0
		11
			id 11
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/011 (-43.0, -50.0, 27.0)
			notify_unit_sequence hide
			time 0
		12
			id 12
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/012 (0.0, -50.0, 27.0)
			notify_unit_sequence hide
			time 0
´spawn_money´ MissionScriptElement 100017
	on_executed
		´first_row_or_less´ (delay 0)
		´second_row_or_more´ (delay 0)
´first_row_or_less´ ElementCounterFilter 100022
	check_type less_or_equal
	elements
		1 ´money_bag_counter´
	needed_to_execute all
	value 6
	on_executed
		´spawn_first_row´ (delay 0)
´second_row_or_more´ ElementCounterFilter 100023
	check_type greater_than
	elements
		1 ´money_bag_counter´
	needed_to_execute all
	value 6
	on_executed
		´spawn_second_row´ (delay 0)
´spawn_second_row_no_interaction´ ElementUnitSequence 100025
	position -200.0, -1250.0, 0.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/001 (0.0, 0.0, 0.0)
			notify_unit_sequence show
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/002 (0.0, 50.0, 0.0)
			notify_unit_sequence show
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/003 (0.0, -50.0, 0.0)
			notify_unit_sequence show
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/004 (-43.0, -50.0, 0.0)
			notify_unit_sequence show
			time 0
		5
			id 5
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/005 (-43.0, 0.0, 0.0)
			notify_unit_sequence show
			time 0
		6
			id 6
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/006 (-43.0, 50.0, 0.0)
			notify_unit_sequence show
			time 0
´spawn_first_row_interaction001´ ElementUnitSequence 100026
	position -400.0, -800.0, 0.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/001 (0.0, 0.0, 0.0)
			notify_unit_sequence show_and_enable_interaction
			time 0
´spawn_second_row´ MissionScriptElement 100027
	on_executed
		´spawn_second_row_no_interaction´ (delay 0)
		´7_bags´ (delay 0)
		´8_bags´ (delay 0)
		´9_bags´ (delay 0)
		´10_bags´ (delay 0)
		´11_bags´ (delay 0)
		´12_bags´ (delay 0)
		´spawn_set_amount_of_bags002´ (delay 0.5)
		´enable_rest_interactions´ (delay 1)
´spawn_first_row´ MissionScriptElement 100028
	on_executed
		´spawn_set_amount_of_bags001´ (delay 0)
´spawn_first_row_interaction002´ ElementUnitSequence 100029
	position -400.0, -700.0, 0.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/002 (0.0, 50.0, 0.0)
			notify_unit_sequence show_and_enable_interaction
			time 0
´spawn_first_row_interaction003´ ElementUnitSequence 100030
	position -500.0, -800.0, 0.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/003 (0.0, -50.0, 0.0)
			notify_unit_sequence show_and_enable_interaction
			time 0
´spawn_first_row_interaction004´ ElementUnitSequence 100031
	position -500.0, -700.0, 0.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/004 (-43.0, -50.0, 0.0)
			notify_unit_sequence show_and_enable_interaction
			time 0
´spawn_first_row_interaction005´ ElementUnitSequence 100032
	position -600.0, -800.0, 0.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/005 (-43.0, 0.0, 0.0)
			notify_unit_sequence show_and_enable_interaction
			time 0
´spawn_first_row_interaction006´ ElementUnitSequence 100033
	position -600.0, -700.0, 0.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/006 (-43.0, 50.0, 0.0)
			notify_unit_sequence show_and_enable_interaction
			time 0
´spawn_set_amount_of_bags001´ ElementRandom 100034
	amount 1
	counter_id ´money_bag_counter´
	ignore_disabled True
	on_executed
		´spawn_first_row_interaction002´ (delay 0)
		´spawn_first_row_interaction001´ (delay 0)
		´spawn_first_row_interaction003´ (delay 0)
		´spawn_first_row_interaction004´ (delay 0)
		´spawn_first_row_interaction006´ (delay 0)
		´spawn_first_row_interaction005´ (delay 0)
´spawn_set_amount_of_bags002´ ElementRandom 100035
	amount 1
	counter_id ´2nd_row_money_counter´
	ignore_disabled True
	on_executed
		´spawn_2nd_row_interaction001´ (delay 0)
		´spawn_2nd_row_interaction002´ (delay 0)
		´spawn_2nd_row_interaction004´ (delay 0)
		´spawn_2nd_row_interaction003´ (delay 0)
		´spawn_2nd_row_interaction005´ (delay 0)
		´spawn_2nd_row_interaction006´ (delay 0)
´2nd_row_money_counter´ ElementCounter 100036
	counter_target 0
	digital_gui_unit_ids
´add_extra_bag001´ ElementCounterOperator 100037
	amount 1
	elements
		1 ´2nd_row_money_counter´
	operation set
´add_extra_bag002´ ElementCounterOperator 100044
	amount 2
	elements
		1 ´2nd_row_money_counter´
	operation set
´add_extra_bag003´ ElementCounterOperator 100045
	amount 3
	elements
		1 ´2nd_row_money_counter´
	operation set
´add_extra_bag004´ ElementCounterOperator 100046
	amount 4
	elements
		1 ´2nd_row_money_counter´
	operation set
´add_extra_bag005´ ElementCounterOperator 100047
	amount 5
	elements
		1 ´2nd_row_money_counter´
	operation set
´add_extra_bag006´ ElementCounterOperator 100048
	amount 6
	elements
		1 ´2nd_row_money_counter´
	operation set
´spawn_2nd_row_interaction001´ ElementUnitSequence 100051
	position -400.0, -900.0, 0.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/007 (-43.0, 50.0, 27.0)
			notify_unit_sequence show_and_enable_interaction
			time 0
	on_executed
		´disable_interaction_moneybag_below001´ (delay 0)
´spawn_2nd_row_interaction002´ ElementUnitSequence 100052
	position -400.0, -1000.0, 0.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/008 (0.0, 50.0, 27.0)
			notify_unit_sequence show_and_enable_interaction
			time 0
	on_executed
		´disable_interaction_moneybag_below002´ (delay 0)
´spawn_2nd_row_interaction003´ ElementUnitSequence 100053
	position -400.0, -1100.0, 0.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/009 (0.0, 0.0, 27.0)
			notify_unit_sequence show_and_enable_interaction
			time 0
	on_executed
		´disable_interaction_moneybag_below003´ (delay 0)
´spawn_2nd_row_interaction004´ ElementUnitSequence 100054
	position -400.0, -1200.0, 0.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/010 (-43.0, 0.0, 27.0)
			notify_unit_sequence show_and_enable_interaction
			time 0
	on_executed
		´disable_interaction_moneybag_below004´ (delay 0)
´spawn_2nd_row_interaction005´ ElementUnitSequence 100055
	position -400.0, -1300.0, 0.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/011 (-43.0, -50.0, 27.0)
			notify_unit_sequence show_and_enable_interaction
			time 0
	on_executed
		´disable_interaction_moneybag_below005´ (delay 0)
´spawn_2nd_row_interaction006´ ElementUnitSequence 100056
	position -400.0, -1400.0, 0.0
	rotation 0.0, 0.0, -0.707107, -0.707107
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/012 (0.0, -50.0, 27.0)
			notify_unit_sequence show_and_enable_interaction
			time 0
	on_executed
		´disable_interaction_moneybag_below006´ (delay 0)
´interacted_with_2nd_row001´ ElementUnitSequenceTrigger 100057
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/pickups/gen_pku_money/007 (-43.0, 50.0, 27.0)
	on_executed
		´activate_interaction_below001´ (delay 0)
´interacted_with_2nd_row002´ ElementUnitSequenceTrigger 100058
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/pickups/gen_pku_money/008 (0.0, 50.0, 27.0)
	on_executed
		´activate_interaction_below002´ (delay 0)
´interacted_with_2nd_row003´ ElementUnitSequenceTrigger 100059
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/pickups/gen_pku_money/009 (0.0, 0.0, 27.0)
	on_executed
		´activate_interaction_below003´ (delay 0)
´interacted_with_2nd_row004´ ElementUnitSequenceTrigger 100060
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/pickups/gen_pku_money/010 (-43.0, 0.0, 27.0)
	on_executed
		´activate_interaction_below004´ (delay 0)
´interacted_with_2nd_row005´ ElementUnitSequenceTrigger 100061
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/pickups/gen_pku_money/011 (-43.0, -50.0, 27.0)
	on_executed
		´activate_interaction_below005´ (delay 0)
´interacted_with_2nd_row006´ ElementUnitSequenceTrigger 100062
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/pickups/gen_pku_money/012 (0.0, -50.0, 27.0)
	on_executed
		´activate_interaction_below006´ (delay 0)
´activate_interaction_below001´ ElementUnitSequence 100063
	position -900.0, -600.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/006 (-43.0, 50.0, 0.0)
			notify_unit_sequence enable_interaction
			time 0
´activate_interaction_below002´ ElementUnitSequence 100064
	position -900.0, -700.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/002 (0.0, 50.0, 0.0)
			notify_unit_sequence show
			time 0
´activate_interaction_below003´ ElementUnitSequence 100065
	position -900.0, -800.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/001 (0.0, 0.0, 0.0)
			notify_unit_sequence enable_interaction
			time 0
´activate_interaction_below004´ ElementUnitSequence 100066
	position -900.0, -900.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/005 (-43.0, 0.0, 0.0)
			notify_unit_sequence enable_interaction
			time 0
´activate_interaction_below005´ ElementUnitSequence 100067
	position -900.0, -1000.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/004 (-43.0, -50.0, 0.0)
			notify_unit_sequence enable_interaction
			time 0
´activate_interaction_below006´ ElementUnitSequence 100068
	position -900.0, -1100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/pickups/gen_pku_money/003 (0.0, -50.0, 0.0)
			notify_unit_sequence enable_interaction
			time 0
´7_bags´ ElementCounterFilter 100038
	check_type equal
	elements
		1 ´money_bag_counter´
	needed_to_execute all
	value 7
	on_executed
		´add_extra_bag001´ (delay 0)
´8_bags´ ElementCounterFilter 100039
	check_type equal
	elements
		1 ´money_bag_counter´
	needed_to_execute all
	value 8
	on_executed
		´add_extra_bag002´ (delay 0)
´9_bags´ ElementCounterFilter 100040
	check_type equal
	elements
		1 ´money_bag_counter´
	needed_to_execute all
	value 9
	on_executed
		´add_extra_bag003´ (delay 0)
´10_bags´ ElementCounterFilter 100041
	check_type equal
	elements
		1 ´money_bag_counter´
	needed_to_execute all
	value 10
	on_executed
		´add_extra_bag004´ (delay 0)
´11_bags´ ElementCounterFilter 100042
	check_type equal
	elements
		1 ´money_bag_counter´
	needed_to_execute all
	value 11
	on_executed
		´add_extra_bag005´ (delay 0)
´12_bags´ ElementCounterFilter 100043
	check_type equal
	elements
		1 ´money_bag_counter´
	needed_to_execute all
	value 12
	on_executed
		´add_extra_bag006´ (delay 0)
´func_sequence_trigger_001´ ElementUnitSequenceTrigger 100015
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/pickups/gen_pku_money/001 (0.0, 0.0, 0.0)
		2
			guis_id 2
			sequence interact
			unit_id units/payday2/pickups/gen_pku_money/002 (0.0, 50.0, 0.0)
		3
			guis_id 3
			sequence interact
			unit_id units/payday2/pickups/gen_pku_money/003 (0.0, -50.0, 0.0)
		4
			guis_id 4
			sequence interact
			unit_id units/payday2/pickups/gen_pku_money/004 (-43.0, -50.0, 0.0)
		5
			guis_id 5
			sequence interact
			unit_id units/payday2/pickups/gen_pku_money/005 (-43.0, 0.0, 0.0)
		6
			guis_id 6
			sequence interact
			unit_id units/payday2/pickups/gen_pku_money/006 (-43.0, 50.0, 0.0)
		7
			guis_id 7
			sequence interact
			unit_id units/payday2/pickups/gen_pku_money/007 (-43.0, 50.0, 27.0)
		8
			guis_id 8
			sequence interact
			unit_id units/payday2/pickups/gen_pku_money/008 (0.0, 50.0, 27.0)
		9
			guis_id 9
			sequence interact
			unit_id units/payday2/pickups/gen_pku_money/009 (0.0, 0.0, 27.0)
		10
			guis_id 10
			sequence interact
			unit_id units/payday2/pickups/gen_pku_money/010 (-43.0, 0.0, 27.0)
		11
			guis_id 11
			sequence interact
			unit_id units/payday2/pickups/gen_pku_money/011 (-43.0, -50.0, 27.0)
		12
			guis_id 12
			sequence interact
			unit_id units/payday2/pickups/gen_pku_money/012 (0.0, -50.0, 27.0)
	on_executed
		´instance_output_picked_up_money´ (delay 0)
´instance_output_picked_up_money´ ElementInstanceOutput 100019
	event picked_up_money
´point_spawn_player_001´ ElementPlayerSpawner 100049
	EXECUTE ON STARTUP
	DISABLED
	position 0.0, 950.0, 0.0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	state standard
´normal´ ElementFilter 100014
	difficulty_easy True
	difficulty_hard False
	difficulty_normal True
	difficulty_overkill False
	difficulty_overkill_145 False
	difficulty_overkill_290 False
	mode_assault True
	mode_control True
	platform_ps3 True
	platform_win32 True
	player_1 True
	player_2 True
	player_3 True
	player_4 True
	position 600.0, -650.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´logic_random_001´ (delay 0)
		´point_debug_003´ (delay 0)
´spawn_money_´ MissionScriptElement 100018
	on_executed
		´normal´ (delay 0)
		´hard´ (delay 0)
		´vhard´ (delay 0)
		´ovk´ (delay 0)
		´DW´ (delay 0)
´hard´ ElementFilter 100024
	difficulty_easy False
	difficulty_hard True
	difficulty_normal False
	difficulty_overkill False
	difficulty_overkill_145 False
	difficulty_overkill_290 False
	mode_assault True
	mode_control True
	platform_ps3 True
	platform_win32 True
	player_1 True
	player_2 True
	player_3 True
	player_4 True
	position 600.0, -750.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´logic_random_002´ (delay 0)
		´point_debug_005´ (delay 0)
´vhard´ ElementFilter 100070
	difficulty_easy False
	difficulty_hard False
	difficulty_normal False
	difficulty_overkill True
	difficulty_overkill_145 False
	difficulty_overkill_290 False
	mode_assault True
	mode_control True
	platform_ps3 True
	platform_win32 True
	player_1 True
	player_2 True
	player_3 True
	player_4 True
	position 600.0, -850.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´logic_random_003´ (delay 0)
		´point_debug_006´ (delay 0)
´ovk´ ElementFilter 100071
	difficulty_easy False
	difficulty_hard False
	difficulty_normal False
	difficulty_overkill False
	difficulty_overkill_145 True
	difficulty_overkill_290 False
	mode_assault True
	mode_control True
	platform_ps3 True
	platform_win32 True
	player_1 True
	player_2 True
	player_3 True
	player_4 True
	position 600.0, -950.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´logic_random_004´ (delay 0)
		´point_debug_007´ (delay 0)
´DW´ ElementFilter 100072
	difficulty_easy False
	difficulty_hard False
	difficulty_normal False
	difficulty_overkill False
	difficulty_overkill_145 False
	difficulty_overkill_290 True
	mode_assault True
	mode_control True
	platform_ps3 True
	platform_win32 True
	player_1 True
	player_2 True
	player_3 True
	player_4 True
	position 600.0, -1050.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´logic_random_005´ (delay 0)
		´point_debug_008´ (delay 0)
´logic_random_001´ ElementRandom 100073
	amount 1
	ignore_disabled True
	on_executed
		´001´ (delay 0)
		´002´ (delay 0)
		´003´ (delay 0)
´logic_random_002´ ElementRandom 100074
	amount 1
	ignore_disabled True
	on_executed
		´002´ (delay 0)
		´003´ (delay 0)
		´004´ (delay 0)
´logic_random_003´ ElementRandom 100075
	amount 1
	ignore_disabled True
	on_executed
		´003´ (delay 0)
		´004´ (delay 0)
		´005´ (delay 0)
´logic_random_004´ ElementRandom 100076
	amount 1
	ignore_disabled True
	on_executed
		´008´ (delay 0)
		´007´ (delay 0)
		´006´ (delay 0)
´logic_random_005´ ElementRandom 100077
	amount 1
	ignore_disabled True
	on_executed
		´008´ (delay 0)
		´007´ (delay 0)
´001´ ElementCounterOperator 100078
	amount 3
	elements
		1 ´money_bag_counter´
	operation set
´002´ ElementCounterOperator 100079
	amount 4
	elements
		1 ´money_bag_counter´
	operation set
´003´ ElementCounterOperator 100080
	amount 5
	elements
		1 ´money_bag_counter´
	operation set
´004´ ElementCounterOperator 100081
	amount 6
	elements
		1 ´money_bag_counter´
	operation set
´005´ ElementCounterOperator 100082
	amount 7
	elements
		1 ´money_bag_counter´
	operation set
´006´ ElementCounterOperator 100083
	amount 8
	elements
		1 ´money_bag_counter´
	operation set
´007´ ElementCounterOperator 100084
	amount 9
	elements
		1 ´money_bag_counter´
	operation set
´008´ ElementCounterOperator 100085
	amount 10
	elements
		1 ´money_bag_counter´
	operation set
´point_debug_003´ ElementDebug 100087
	as_subtitle False
	debug_string norm
	show_instigator False
´point_debug_005´ ElementDebug 100088
	as_subtitle False
	debug_string hard
	show_instigator False
´point_debug_006´ ElementDebug 100089
	as_subtitle False
	debug_string vhard
	show_instigator False
´point_debug_007´ ElementDebug 100090
	as_subtitle False
	debug_string ovk
	show_instigator False
´point_debug_008´ ElementDebug 100091
	as_subtitle False
	debug_string dw
	show_instigator False
´enable_rest_interactions´ MissionScriptElement 100050
	on_executed
		´spawn_first_row_interaction001´ (delay 0)
		´spawn_first_row_interaction002´ (delay 0)
		´spawn_first_row_interaction004´ (delay 0)
		´spawn_first_row_interaction003´ (delay 0)
		´spawn_first_row_interaction005´ (delay 0)
		´spawn_first_row_interaction006´ (delay 0)
´disable_interaction_moneybag_below001´ ElementToggle 100101
	elements
		1 ´spawn_first_row_interaction006´
	set_trigger_times -1
	toggle off
´disable_interaction_moneybag_below002´ ElementToggle 100102
	elements
		1 ´spawn_first_row_interaction002´
	set_trigger_times -1
	toggle off
´disable_interaction_moneybag_below003´ ElementToggle 100103
	elements
		1 ´spawn_first_row_interaction001´
	set_trigger_times -1
	toggle off
´disable_interaction_moneybag_below004´ ElementToggle 100104
	elements
		1 ´spawn_first_row_interaction005´
	set_trigger_times -1
	toggle off
´disable_interaction_moneybag_below005´ ElementToggle 100105
	elements
		1 ´spawn_first_row_interaction004´
	set_trigger_times -1
	toggle off
´disable_interaction_moneybag_below006´ ElementToggle 100106
	elements
		1 ´spawn_first_row_interaction003´
	set_trigger_times -1
	toggle off
