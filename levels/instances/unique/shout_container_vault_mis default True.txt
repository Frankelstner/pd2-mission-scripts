﻿´startup´ MissionScriptElement 100004
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1.5)
	Hide object: units/payday2/props/gen_prop_long_lamp_v2/003 (-112.0, -125.0, 249.784)
	Hide object: units/payday2/props/gen_prop_long_lamp_v2/004 (-112.0, 74.9999, 249.784)
	Hide object: units/payday2/props/gen_prop_long_lamp_v2/005 (110.0, -125.0, 249.784)
	Hide object: units/payday2/props/gen_prop_long_lamp_v2/006 (110.0, 74.9999, 249.784)
	Execute sequence: "light_off" - units/world/props/flickering_light/001 (0.0, 75.0, 125.0)
	Execute sequence: "light_off" - units/world/props/flickering_light/002 (0.0, -100.0, 125.0)
	Execute 1:
		´colour_001´
		´colour_002´
		´colour_003´
		´colour_004´
	If Normal, Hard, Very Hard:
		Hide object: units/pd2_dlc_shoutout_raid/props/ind_prop_murky_box/004 (62.0, 123.0, 25.0)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/002 (100.0, 250.0, 0.0) (DELAY 8.5)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/001 (100.0, -350.0, 0.0) (DELAY 8.5)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/003 (-99.9999, -150.0, 0.0) (DELAY 8.5)
´colour_001´ ElementUnitSequence 100005
	Execute sequence: "var_set_color_blue" - units/payday2/props/gen_prop_container_murky/container_with_vault (0.0, 0.0, 0.0)
	Execute sequence: "var_set_color_blue_front" - units/payday2/props/gen_prop_container_murky_doors/container_with_vault_doors_front (116.0, 295.0, 18.0)
	Execute sequence: "var_set_color_blue_rear" - units/payday2/props/gen_prop_container_murky_doors_vented/back_door_vented (-115.0, -295.0, 18.0)
´colour_002´ ElementUnitSequence 100007
	Execute sequence: "var_set_color_green" - units/payday2/props/gen_prop_container_murky/container_with_vault (0.0, 0.0, 0.0)
	Execute sequence: "var_set_color_green_front" - units/payday2/props/gen_prop_container_murky_doors/container_with_vault_doors_front (116.0, 295.0, 18.0)
	Execute sequence: "var_set_color_green_rear" - units/payday2/props/gen_prop_container_murky_doors_vented/back_door_vented (-115.0, -295.0, 18.0)
´colour_003´ ElementUnitSequence 100008
	Execute sequence: "var_set_color_red" - units/payday2/props/gen_prop_container_murky/container_with_vault (0.0, 0.0, 0.0)
	Execute sequence: "var_set_color_red_front" - units/payday2/props/gen_prop_container_murky_doors/container_with_vault_doors_front (116.0, 295.0, 18.0)
	Execute sequence: "var_set_color_red_rear" - units/payday2/props/gen_prop_container_murky_doors_vented/back_door_vented (-115.0, -295.0, 18.0)
´colour_004´ ElementUnitSequence 100009
	Execute sequence: "var_set_color_yellow" - units/payday2/props/gen_prop_container_murky/container_with_vault (0.0, 0.0, 0.0)
	Execute sequence: "var_set_color_yellow_front" - units/payday2/props/gen_prop_container_murky_doors/container_with_vault_doors_front (116.0, 295.0, 18.0)
	Execute sequence: "var_set_color_yellow_rear" - units/payday2/props/gen_prop_container_murky_doors_vented/back_door_vented (-115.0, -295.0, 18.0)
´discovered_vault´ ElementUnitSequenceTrigger 100011
	BASE DELAY  (DELAY 1.5)
	TRIGGER TIMES 1
	Upon sequence "interact" - units/payday2/props/gen_prop_container_murky_doors/container_with_vault_doors_front (116.0, 295.0, 18.0):
		Toggle off: ´open_front_doors_if_not_opened´
		´discovered_front_link´
´discovered_front_link´ MissionScriptElement 100018
	Send instance event "discovered_front" to mission.
	´vent_wp´ (DELAY 2)
´open_front_doors_if_not_opened´ ElementUnitSequence 100031
	Execute sequence: "anim_door_open" - units/payday2/props/gen_prop_container_murky_doors/container_with_vault_doors_front (116.0, 295.0, 18.0)
	Execute sequence: "state_interaction_disabled" - units/payday2/props/gen_prop_container_murky_doors/container_with_vault_doors_front (116.0, 295.0, 18.0)
´vent_wp´ ElementWaypoint 100020
	Place waypoint with icon "pd2_generic_look" at position (0.0, -300.0, 136.0)
´discovered_back´ ElementUnitSequenceTrigger 100017
	BASE DELAY  (DELAY 1.5)
	TRIGGER TIMES 1
	Upon sequence "interact" - units/payday2/props/gen_prop_container_murky_doors_vented/back_door_vented (-115.0, -295.0, 18.0):
		Send instance event "discovered_vent" to mission.
		Execute sequence: "state_interaction_enable" - units/pd2_dlc_shoutout_raid/props/gen_prop_container_a_vault_seq/container_vault (0.0, 0.0, 0.0)
		Toggle off: ´discovered_front_link´
		remove ´vent_wp´
		Make teammate close to instigator say: g14
		´acunit_wp´
´acunit_wp´ ElementWaypoint 100023
	Place waypoint with icon "pd2_fix" at position (3.0, -257.0, 216.0)
´cooler_temp（0）´ ElementTimer 100028
	Timer: 0
	Digital GUI unit: units/pd2_indiana/props/gen_prop_security_timer/001 (46.0, -267.0, 121.0)
´entered_vault´ ElementAreaTrigger 100037
	TRIGGER TIMES 1
	Use shape: ´entered_container_shape´ (0.0, 100.0, 124.5)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Execute sequence: "state_interaction_enabled" - units/pd2_dlc_shoutout_raid/props/ind_prop_murky_box/001 (-56.0001, -31.0, 25.0)
		Execute sequence: "state_interaction_enabled" - units/pd2_dlc_shoutout_raid/props/ind_prop_murky_box/002 (-56.0001, 124.0, 25.0)
		Execute sequence: "state_interaction_enabled" - units/pd2_dlc_shoutout_raid/props/ind_prop_murky_box/003 (62.0, -34.0, 25.0)
		remove ´enter_vault_wp´
		Send instance event "entered_vault" to mission.
		Make teammate close to instigator say: g16
		If Overkill, Mayhem, Death Wish, One Down:
			Execute sequence: "state_interaction_enabled" - units/pd2_dlc_shoutout_raid/props/ind_prop_murky_box/004 (62.0, 123.0, 25.0)
		´loot_wp´
´entered_container_shape´ ElementShape 100038
	Box, width 250, height 200, depth 250, at position (0.0, 100.0, 124.5) with rotation (0.0, 0.0, 0.0, -1.0)
´enter_vault_wp´ ElementWaypoint 100067
	Place waypoint with icon "pd2_goto" at position (49.0, 236.0, 136.0)
´loot_wp´ ElementWaypoint 100078
	Place waypoint with icon "pd2_loot" at position (0.0, 0.0, 100.0)
´added_crowbar´ ElementUnitSequenceTrigger 100051
	TRIGGER TIMES 3
	Upon sequence "interact" - units/pd2_dlc_shoutout_raid/props/gen_prop_container_a_vault_seq/container_vault (0.0, 0.0, 0.0):
		´number_of_crowbars（0）´ += 1
		Send instance event "started_vent_blocking" to mission.
		´enable_interaction_3´
		´started_crowbar_loop_once´
´enable_interaction_3´ ElementUnitSequence 100052
	TRIGGER TIMES 2
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_shoutout_raid/props/gen_prop_container_a_vault_seq/container_vault (0.0, 0.0, 0.0)
´number_of_crowbars（0）´ ElementCounter 100053
´started_crowbar_loop_once´ MissionScriptElement 100026
	TRIGGER TIMES 1
	´tick´
´tick´ MissionScriptElement 100056
	If ´number_of_crowbars（0）´ == 1:
		´cooler_temp（0）´ += 0.1
		´counter_temp（500）´ -= 1
		Send instance event "crowbar_1_inserted" to mission.
	If ´number_of_crowbars（0）´ == 2:
		´cooler_temp（0）´ += 0.2
		´counter_temp（500）´ -= 2
		Send instance event "crowbar_2_inserted" to mission.
	If ´number_of_crowbars（0）´ == 3:
		´cooler_temp（0）´ += 0.5
		´counter_temp（500）´ -= 5
		remove ´acunit_wp´
		Send instance event "crowbar_3_inserted" to mission.
		´wait_wp´
	´re_tick´ (DELAY 1)
´re_tick´ MissionScriptElement 100057
	´tick´
	´logic_counter_filter_003´
	´logic_counter_filter_001´
	´logic_counter_filter_004´
	´logic_counter_filter_002´
´counter_temp（500）´ ElementCounter 100063
´wait_wp´ ElementWaypoint 100101
	TRIGGER TIMES 1
	Place waypoint with icon "pd2_generic_look" at position (0.0, 0.0, 150.0)
´logic_counter_filter_003´ ElementCounterFilter 100035
	TRIGGER TIMES 1
	If ´counter_temp（500）´ <= 300:
		Execute sequence: "black_on_light_yellow" - units/pd2_indiana/props/gen_prop_security_timer/001 (46.0, -267.0, 121.0)
´logic_counter_filter_001´ ElementCounterFilter 100065
	TRIGGER TIMES 1
	If ´counter_temp（500）´ <= 200:
		´turn_orange´
´logic_counter_filter_004´ ElementCounterFilter 100043
	TRIGGER TIMES 1
	If ´counter_temp（500）´ <= 100:
		´alternate_001´
´logic_counter_filter_002´ ElementCounterFilter 100066
	TRIGGER TIMES 1
	If ´counter_temp（500）´ <= 0:
		remove ´acunit_wp´
		Send instance event "completed_vent_blocking" to mission.
		Remove nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/002 (100.0, 250.0, 0.0)
		remove ´wait_wp´
		Execute sequence: "black_on_light_red" - units/pd2_indiana/props/gen_prop_security_timer/001 (46.0, -267.0, 121.0)
		Toggle off: ´tick´
		Toggle off: ´re_tick´
		Toggle off: ´turn_orange´
		Toggle off: ´alternate_loop´
		Toggle off: ´alternate_001´
		´open_front_doors_if_not_opened´
		´enter_vault_wp´
		´overheat´
		Execute sequence: "black_on_light_red" - units/pd2_indiana/props/gen_prop_security_timer/001 (46.0, -267.0, 121.0) (DELAY 0.05)
		´overheat´ (DELAY 0.05)
		Execute sequence: "anim_open_door" - units/pd2_dlc_shoutout_raid/props/gen_prop_container_a_vault_seq/container_vault (0.0, 0.0, 0.0) (DELAY 1)
		Execute sequence: "light_on" - units/world/props/flickering_light/001 (0.0, 75.0, 125.0) (DELAY 1)
		Execute sequence: "light_on" - units/world/props/flickering_light/002 (0.0, -100.0, 125.0) (DELAY 1)
		Show object: units/payday2/props/gen_prop_long_lamp_v2/003 (-112.0, -125.0, 249.784) (DELAY 1)
		Show object: units/payday2/props/gen_prop_long_lamp_v2/004 (-112.0, 74.9999, 249.784) (DELAY 1)
		Show object: units/payday2/props/gen_prop_long_lamp_v2/005 (110.0, -125.0, 249.784) (DELAY 1)
		Show object: units/payday2/props/gen_prop_long_lamp_v2/006 (110.0, 74.9999, 249.784) (DELAY 1)
		Play audio "vault_container_magnet_lock_open" at position (0.0, 75.0, 100.0) (DELAY 1)
		Play audio "alarm_vault_container_overheat_off" at position (-2175.0, 100.0, 2.5) (DELAY 1)
		´p17´ (DELAY 2)
		´p17´ (DELAY 2.05)
		Play audio "vault_container_airlock_open" at position (0.0, 75.0, 75.0) (DELAY 2.5)
		Execute sequence: "black_on_light_yellow" - units/pd2_indiana/props/gen_prop_security_timer/001 (46.0, -267.0, 121.0) (DELAY 3)
		´reduce_temp´ (DELAY 3)
´turn_orange´ ElementUnitSequence 100060
	Execute sequence: "black_on_light_orange" - units/pd2_indiana/props/gen_prop_security_timer/001 (46.0, -267.0, 121.0)
´alternate_001´ MissionScriptElement 100044
	´turn_orange´
	´alternate_loop´ (DELAY 0.5)
´reduce_temp´ ElementTimerOperator 100047
	Start timer: ´cooler_temp（0）´
	´reduce_temp_pause´ (DELAY 0.5)
´overheat´ ElementPlaySound 100120
	TRIGGER TIMES 1
	Play audio "alarm_vault_container_overheat" at position (0.0, -300.0, 177.5)
´p17´ ElementTeammateComment 100112
	TRIGGER TIMES 1
	Make teammate close to instigator say: p17
´alternate_loop´ MissionScriptElement 100046
	Execute sequence: "black_on_light_red" - units/pd2_indiana/props/gen_prop_security_timer/001 (46.0, -267.0, 121.0)
	´overheat´
	´alternate_001´ (DELAY 0.5)
	´p17´ (DELAY 2)
´reduce_temp_pause´ ElementTimerOperator 100069
	Pause timer: ´cooler_temp（0）´
	´reduce_temp´ (DELAY 0.2)
´cooled_enough´ ElementTimerTrigger 100071
	Upon timer "´cooler_temp（0）´ == 20:
		Execute sequence: "black_on_light_blue" - units/pd2_indiana/props/gen_prop_security_timer/001 (46.0, -267.0, 121.0)
		Toggle off: ´reduce_temp´ (DELAY 8.5-13)
		Toggle off: ´reduce_temp_pause´ (DELAY 8.5-13)
´pku_warhead´ ElementGlobalEventTrigger 100085
	TRIGGER TIMES 8
	Upon global event "pku_warhead":
		Debug message: warhead plus 1
		Send instance event "picked_up_warhead" to mission.
		Send instance event "warhead_picked_up" to mission.
		If Normal, Hard, Very Hard:
			´counting_thefts_6（6）´
		If Overkill, Mayhem, Death Wish, One Down:
			´counting_thefts_8（8）´
´counting_thefts_6（6）´ ElementCounter 100080
	When counter target reached: 
		remove ´loot_wp´
		Make teammate close to instigator say: g17
´counting_thefts_8（8）´ ElementCounter 100092
	When counter target reached: 
		remove ´loot_wp´
		Make teammate close to instigator say: g17
´when_interacted´ ElementUnitSequenceTrigger 100099
	TRIGGER TIMES 1
	Upon any sequence:
		"interact" - units/pd2_dlc_shoutout_raid/props/ind_prop_murky_box/001 (-56.0001, -31.0, 25.0)
		"interact" - units/pd2_dlc_shoutout_raid/props/ind_prop_murky_box/002 (-56.0001, 124.0, 25.0)
		"interact" - units/pd2_dlc_shoutout_raid/props/ind_prop_murky_box/003 (62.0, -34.0, 25.0)
		"interact" - units/pd2_dlc_shoutout_raid/props/ind_prop_murky_box/004 (62.0, 123.0, 25.0)
	Execute:
		´warhead_case_opened´
´warhead_case_opened´ ElementInstanceOutput 100098
	TRIGGER TIMES 1
	Send instance event "warhead_case_opened" to mission.
