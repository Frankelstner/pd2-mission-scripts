ID range vs continent name:
	100000: world

statics
	100079 units/dev_tools/level_tools/dev_collision_1x1m/s3_collision_for_painting (517.0, -697.0, 382.7)
	100080 units/dev_tools/level_tools/dev_collision_1x1m/s3_collision_for_painting001 (517.0, -697.0, 382.7)
	100082 units/dev_tools/level_tools/dev_collision_1x1m/s3_collision_for_painting002 (588.934, -766.466, 382.7)
	100083 units/dev_tools/level_tools/dev_collision_1x1m/s3_collision_for_painting003 (532.825, -712.282, 482.7)
	100008 units/payday2/architecture/com_int_construction_floor_paper_4x1m/S3_com_int_construction_floor_paper_4x1m_001 (204.0, -739.0, 399.0)
		delayed_load True
	100074 units/payday2/architecture/com_int_construction_floor_paper_4x1m/S3_com_int_construction_floor_paper_4x1m_002 (599.0, -739.0, 399.0)
		delayed_load True
	100039 units/payday2/props/off_prop_officehigh_filebox_long_open_b/S2_off_prop_officehigh_filebox_long_open_b_001 (34.0, -689.0, 505.0)
		delayed_load True
	100040 units/payday2/props/off_prop_officehigh_filebox_long_open_b/S2_off_prop_officehigh_filebox_long_open_b_002 (35.0, -464.0, 552.0)
		delayed_load True
	100056 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/S2_off_prop_officehigh_filebox_short_whole_b_001 (270.0, -748.0, 483.0)
		delayed_load True
	100061 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/S2_off_prop_officehigh_filebox_short_whole_b_002 (34.0, -603.0, 504.0)
		delayed_load True
	100063 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/S2_off_prop_officehigh_filebox_short_whole_b_003 (34.0, -570.0, 504.0)
		delayed_load True
	100064 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/S2_off_prop_officehigh_filebox_short_whole_b_004 (35.0, -596.0, 408.0)
		delayed_load True
	100076 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/S2_off_prop_officehigh_filebox_short_whole_b_005 (270.0, -748.0, 483.0)
		delayed_load True
	100011 units/payday2/props/paintings/bnk_prop_lobby_painting_d/S3_bnk_prop_lobby_painting_d_001 (569.413, -689.0, 515.553)
		delayed_load True
	100066 units/payday2/props/paperstacks/off_prop_officehigh_paper_stack_01/S2_off_prop_officehigh_paper_stack_01_001 (34.0, -600.0, 551.0)
		delayed_load True
	100067 units/payday2/props/paperstacks/off_prop_officehigh_paper_stack_01/S2_off_prop_officehigh_paper_stack_01_002 (38.0, -653.0, 505.0)
		delayed_load True
	100053 units/payday2/props/paperstacks/off_prop_officehigh_paper_stack_02/S2_off_prop_officehigh_paper_stack_02_001 (215.0, -728.0, 483.0)
		delayed_load True
	100068 units/payday2/props/paperstacks/off_prop_officehigh_paper_stack_02/S2_off_prop_officehigh_paper_stack_02_002 (36.0, -502.0, 456.0)
		delayed_load True
	100069 units/payday2/props/paperstacks/off_prop_officehigh_paper_stack_02/S2_off_prop_officehigh_paper_stack_02_003 (36.0, -461.0, 456.0)
		delayed_load True
	100054 units/payday2/props/set/ind_prop_warehouse_box_d/S1_ind_prop_warehouse_box_d_003 (156.0, -712.0, 444.0)
		delayed_load True
	100055 units/payday2/props/set/ind_prop_warehouse_box_d/S1_ind_prop_warehouse_box_d_004 (156.0, -712.0, 391.0)
		delayed_load True
	100031 units/payday2/props/set/ind_prop_warehouse_box_d/S2_ind_prop_warehouse_box_d_001 (156.0, -712.0, 391.0)
		delayed_load True
	100037 units/payday2/props/set/ind_prop_warehouse_box_d/S2_ind_prop_warehouse_box_d_002 (156.0, -712.0, 444.0)
		delayed_load True
	100070 units/payday2/props/set/ind_prop_warehouse_box_d/S2_ind_prop_warehouse_box_d_003 (114.0, -445.0, 400.0)
		delayed_load True
	100043 units/payday2/props/set/ind_prop_warehouse_box_f/S1_ind_prop_warehouse_box_f_003 (148.0, -709.0, 498.0)
		delayed_load True
	100029 units/payday2/props/set/ind_prop_warehouse_box_f/S2_ind_prop_warehouse_box_f_001 (723.0, -759.0, 453.0)
		delayed_load True
	100032 units/payday2/props/set/ind_prop_warehouse_box_f/S2_ind_prop_warehouse_box_f_004 (723.0, -759.0, 453.0)
		delayed_load True
	100058 units/payday2/props/set/ind_prop_warehouse_box_g/S1_ind_prop_warehouse_box_g_004 (90.5505, -730.508, 399.0)
		delayed_load True
	100041 units/payday2/props/set/ind_prop_warehouse_box_g/S1_ind_prop_warehouse_box_g_005 (90.5505, -730.508, 449.0)
		delayed_load True
	100028 units/payday2/props/set/ind_prop_warehouse_box_g/S2_ind_prop_warehouse_box_g_001 (735.0, -754.0, 399.0)
		delayed_load True
	100038 units/payday2/props/set/ind_prop_warehouse_box_g/S2_ind_prop_warehouse_box_g_002 (82.0, -754.0, 399.0)
		delayed_load True
	100051 units/payday2/props/stn_prop_medic_stool/S1_stn_prop_medic_stool_002 (295.0, -718.0, 399.0)
		delayed_load True
	100027 units/payday2/props/stn_prop_medic_stool/S2_stn_prop_medic_stool_001 (294.0, -708.0, 399.0)
		delayed_load True
	100014 units/pd2_dlc_arena/props/are_prop_backstage_camera/S3_are_prop_backstage_camera_002 (194.123, -716.57, 398.0)
		delayed_load True
	100077 units/pd2_dlc_myh/props/myh_prop_painting/001 (666.0, -788.0, 452.0)
		delayed_load True
	100048 units/pd2_dlc_myh/props/myh_props_easel/S1_myh_props_easel_002 (475.0, -700.0, 400.0)
		delayed_load True
	100021 units/pd2_dlc_myh/props/myh_props_easel/S2_myh_props_easel_001 (475.0, -700.0, 400.0)
		delayed_load True
	100002 units/pd2_dlc_myh/props/myh_props_easel/S3_myh_props_easel_001 (551.042, -665.0, 400.0)
		delayed_load True
	100042 units/pd2_dlc_myh/props/myh_props_wrapped_painting/S1_myh_props_wrapped_painting_007 (636.0, -784.97, 427.499)
		delayed_load True
	100030 units/pd2_dlc_myh/props/myh_props_wrapped_painting/S1_myh_props_wrapped_painting_008 (546.0, -777.923, 436.845)
		delayed_load True
	100022 units/pd2_dlc_myh/props/myh_props_wrapped_painting/S2_myh_props_wrapped_painting_001 (471.001, -721.94, 503.666)
		delayed_load True
	100024 units/pd2_dlc_myh/props/myh_props_wrapped_painting/S2_myh_props_wrapped_painting_002 (355.0, -746.0, 485.0)
		delayed_load True
	100033 units/pd2_dlc_myh/props/myh_props_wrapped_painting/S2_myh_props_wrapped_painting_003 (611.0, -784.875, 425.265)
		delayed_load True
	100034 units/pd2_dlc_myh/props/myh_props_wrapped_painting/S2_myh_props_wrapped_painting_004 (689.0, -789.191, 427.303)
		delayed_load True
	100036 units/pd2_dlc_myh/props/myh_props_wrapped_painting/S2_myh_props_wrapped_painting_005 (57.391, -664.33, 435.784)
		delayed_load True
	100003 units/pd2_dlc_myh/props/myh_props_wrapped_painting/S3_myh_props_wrapped_painting_001 (554.0, -787.7, 430.171)
		delayed_load True
	100009 units/pd2_dlc_myh/props/myh_props_wrapped_painting/S3_myh_props_wrapped_painting_002 (716.0, -789.4, 428.341)
		delayed_load True
	100010 units/pd2_dlc_myh/props/myh_props_wrapped_painting/S3_myh_props_wrapped_painting_003 (538.0, -783.293, 433.204)
		delayed_load True
	100017 units/pd2_dlc_myh/props/myh_props_wrapped_painting/S3_myh_props_wrapped_painting_004 (163.0, -786.4, 427.452)
		delayed_load True
	100075 units/pd2_dlc_myh/props/myh_props_wrapped_painting/S3_myh_props_wrapped_painting_005 (247.0, -787.7, 430.171)
		delayed_load True
	100000 units/pd2_dlc_myh/props/shelves/myh_props_shelf_1m/S3_myh_props_shelf_1m_001 (48.0001, -739.0, 400.0)
		delayed_load True
	100060 units/pd2_dlc_myh/props/shelves/myh_props_shelf_1m_empty/S1_myh_props_shelf_1m_empty_002 (50.0001, -725.0, 400.0)
		delayed_load True
	100019 units/pd2_dlc_myh/props/shelves/myh_props_shelf_1m_empty/S2_myh_props_shelf_1m_empty_001 (50.0001, -725.0, 400.0)
		delayed_load True
	100001 units/pd2_dlc_myh/props/shelves/myh_props_shelf_2m/S3_myh_props_shelf_2m_001 (48.0001, -629.0, 400.0)
		delayed_load True
	100059 units/pd2_dlc_myh/props/shelves/myh_props_shelf_2m_empty/S1_myh_props_shelf_2m_empty_002 (50.0001, -625.0, 400.0)
		delayed_load True
	100020 units/pd2_dlc_myh/props/shelves/myh_props_shelf_2m_empty/S2_myh_props_shelf_2m_empty_001 (50.0001, -625.0, 400.0)
		delayed_load True
	100025 units/pd2_indiana/props/mus_prop_construction_paint_coverpaper/S2_mus_prop_construction_paint_coverpaper_001 (519.282, -768.44, 400.0)
		delayed_load True
	100049 units/pd2_indiana/props/mus_prop_construction_paint_coverpaper/S3_mus_prop_construction_paint_coverpaper_002 (773.0, -766.0, 400.0)
		delayed_load True
	100050 units/pd2_indiana/props/mus_prop_construction_paint_coverpaper/S3_mus_prop_construction_paint_coverpaper_003 (781.684, -749.1, 414.0)
		delayed_load True
	100007 units/pd2_indiana/props/mus_prop_curator_chair/S3_mus_prop_curator_chair_001 (254.384, -727.026, 393.0)
		delayed_load True
	100052 units/pd2_indiana/props/mus_prop_curator_desk/S1_mus_prop_curator_desk_002 (292.0, -732.0, 402.0)
		delayed_load True
	100023 units/pd2_indiana/props/mus_prop_curator_desk/S2_mus_prop_curator_desk_001 (292.0, -732.0, 402.0)
		delayed_load True
	100006 units/pd2_indiana/props/mus_prop_curator_desk/S3_mus_prop_curator_desk_001 (394.0, -741.0, 400.0)
		delayed_load True
	100016 units/world/props/bank/bank_plant/S3_bank_plant_001 (113.0, -738.0, 406.0)
		delayed_load True
	100071 units/world/props/diamond_heist/apartment/apartment_livingroom/books_coffetable/S2_books_coffetable_001 (34.0, -553.0, 553.0)
		delayed_load True
	100072 units/world/props/diamond_heist/apartment/apartment_livingroom/books_coffetable/S2_books_coffetable_002 (34.0, -504.0, 504.0)
		delayed_load True
	100015 units/world/props/mansion/carpets/man_prop_int_carpet_long/S3_man_prop_int_carpet_long_001 (408.0, -572.0, 400.0)
		delayed_load True
	100013 units/world/props/office/accessories/desk_folderpile_01/S3_desk_folderpile_01_001 (387.0, -766.0, 480.0)
		delayed_load True
	100044 units/world/props/office/accessories/desk_lamp_04/S2__desk_lamp_04_002 (289.088, -769.96, 482.0)
		delayed_load True
	100005 units/world/props/office/accessories/desk_lamp_04/S3_desk_lamp_04_001 (344.0, -765.0, 480.0)
		delayed_load True
	100047 units/world/props/western/wst_prop_int_cutlery_mug/S2_wst_prop_int_cutlery_mug_001 (359.0, -703.0, 482.0)
		delayed_load True
	100018 units/world/props/western/wst_prop_int_cutlery_mug/S3_wst_prop_int_cutlery_mug_001 (480.0, -752.0, 480.0)
		delayed_load True
