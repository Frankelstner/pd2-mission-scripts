ID range vs continent name:
	100000: world

statics
	100032 core/units/light_omni_seq/001 (1.14441e-05, 23.0, 129.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.996078, 0.980392, 0.823529
			enabled False
			falloff_exponent 1
			far_range 85
			multiplier identity
			name lo_omni
			near_range 10
			spot_angle_end -1
			spot_angle_start -1
		mesh_variation light_off
	100069 core/units/light_omni_seq/red (1.14441e-05, 23.0, 129.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.968628, 0.2, 0.2
			enabled False
			falloff_exponent 1
			far_range 85
			multiplier identity
			name lo_omni
			near_range 10
			spot_angle_end -1
			spot_angle_start -1
		mesh_variation light_off
	100045 units/pd2_dlc_chill/props/chl_prop_cardboard_civilian/001 (50.0, 0.0, 0.0)
		mesh_variation anim_down
	100047 units/pd2_dlc_chill/props/chl_prop_cardboard_swat_a/001 (50.0, 0.0, 0.0)
		mesh_variation anim_down
	100048 units/pd2_dlc_chill/props/chl_prop_cardboard_swat_b/001 (50.0, 0.0, 0.0)
		mesh_variation anim_down
