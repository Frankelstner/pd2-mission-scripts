﻿´STARTUP´ MissionScriptElement 100035
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	If livingroom_room_upgrade == 1:
		If ecp_room_upgrade == 3:
			Load delayed:
				units/pd2_dlc_arena/props/are_prop_backstage_camera/002 (381.792, 699.534, 2.61755)
				units/pd2_dlc_arena/architecture/are_int_stage_spot_light/003 (86.9537, 519.732, 4.32635)
				units/pd2_dlc_arena/architecture/are_int_stage_spot_light/004 (322.591, 221.38, 2.32635)
				units/pd2_dlc_ecp/props/ecp_props_green_screen_open/002 (129.978, 295.375, 0.0)
		If ecp_room_upgrade == 2:
			Load delayed:
				units/pd2_dlc_arena/props/are_prop_backstage_camera/002 (381.792, 699.534, 2.61755)
				units/pd2_dlc_fish/lxy_prop_camera_case_open/002 (137.266, 196.781, 108.134)
				units/pd2_dlc1/props/res_prop_store_chipsbag_a/001 (188.561, 351.784, 49.1337)
				units/pd2_dlc_ecp/props/ecp_props_green_screen_closed/002 (185.515, 510.391, 0.0)
		If ecp_room_upgrade == 1:
			Load delayed:
				units/pd2_dlc1/props/res_prop_store_chipsbag_a/001 (188.561, 351.784, 49.1337)
				units/pd2_dlc_fish/lxy_prop_camera_bag/001 (32.0, 531.0, 4.32634)
				units/pd2_dlc_fish/lxy_prop_camera_case_closed/003 (140.739, 216.477, 108.134)
				units/pd2_dlc_ecp/props/ecp_props_green_screen_closed/001 (19.976, 565.299, 0.0)
	If livingroom_room_upgrade == 2:
		If ecp_room_upgrade == 2:
			Load delayed:
				units/world/props/gym/cutscene_microphone/stn_cs_microphone/002 (123.0, 355.0, 46.2624)
				units/pd2_dlc_arena/props/are_prop_backstage_camera/001 (290.0, 354.0, 2.61758)
				units/pd2_dlc_fish/lxy_prop_camera_case_open/001 (101.621, 192.737, 49.177)
				units/pd2_dlc1/props/res_prop_store_chipsbag_a/001 (188.561, 351.784, 49.1337)
				units/pd2_dlc_ecp/props/ecp_props_green_screen_closed/002 (185.515, 510.391, 0.0)
		If ecp_room_upgrade == 3:
			Load delayed:
				units/pd2_dlc_arena/architecture/are_int_stage_spot_light/002 (306.591, 152.38, 2.32635)
				units/pd2_dlc_arena/props/are_prop_backstage_camera/001 (290.0, 354.0, 2.61758)
				units/pd2_dlc_arena/architecture/are_int_stage_spot_light/001 (385.761, 743.384, 4.32635)
				units/world/props/gym/cutscene_microphone/stn_cs_microphone/002 (123.0, 355.0, 46.2624)
				units/pd2_dlc_ecp/props/ecp_props_green_screen_open/001 (-50.9999, 371.0, 0.0)
		If ecp_room_upgrade == 1:
			Load delayed:
				units/pd2_dlc_fish/lxy_prop_camera_bag/001 (32.0, 531.0, 4.32634)
				units/pd2_dlc1/props/res_prop_store_chipsbag_a/001 (188.561, 351.784, 49.1337)
				units/pd2_dlc_ecp/props/ecp_props_green_screen_closed/001 (19.976, 565.299, 0.0)
				units/pd2_dlc_fish/lxy_prop_camera_case_closed/004 (106.922, 209.543, 49.0)
	If livingroom_room_upgrade == 3:
		If ecp_room_upgrade == 2:
			Load delayed:
				units/world/props/gym/cutscene_microphone/stn_cs_microphone/002 (123.0, 355.0, 46.2624)
				units/pd2_dlc_arena/props/are_prop_backstage_camera/001 (290.0, 354.0, 2.61758)
				units/pd2_dlc_fish/lxy_prop_camera_case_open/001 (101.621, 192.737, 49.177)
				units/pd2_dlc1/props/res_prop_store_chipsbag_a/001 (188.561, 351.784, 49.1337)
				units/pd2_dlc_ecp/props/ecp_props_green_screen_closed/002 (185.515, 510.391, 0.0)
		If ecp_room_upgrade == 3:
			Load delayed:
				units/pd2_dlc_arena/architecture/are_int_stage_spot_light/002 (306.591, 152.38, 2.32635)
				units/pd2_dlc_arena/props/are_prop_backstage_camera/001 (290.0, 354.0, 2.61758)
				units/pd2_dlc_arena/architecture/are_int_stage_spot_light/001 (385.761, 743.384, 4.32635)
				units/world/props/gym/cutscene_microphone/stn_cs_microphone/002 (123.0, 355.0, 46.2624)
				units/pd2_dlc_ecp/props/ecp_props_green_screen_open/001 (-50.9999, 371.0, 0.0)
		If ecp_room_upgrade == 1:
			Load delayed:
				units/pd2_dlc_fish/lxy_prop_camera_bag/001 (32.0, 531.0, 4.32634)
				units/pd2_dlc1/props/res_prop_store_chipsbag_a/001 (188.561, 351.784, 49.1337)
				units/pd2_dlc_ecp/props/ecp_props_green_screen_closed/001 (19.976, 565.299, 0.0)
				units/pd2_dlc_fish/lxy_prop_camera_case_closed/004 (106.922, 209.543, 49.0)
