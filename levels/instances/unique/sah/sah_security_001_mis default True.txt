﻿´startup´ MissionScriptElement 100002
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Execute sequence: "activate_door" - units/pd2_dlc_rvd/props/prop_door_reinforced/rvd_interactable_door_reinforced/001 (93.0, -22.0, -1.996)
	Execute sequence: "state_door_close" - units/pd2_dlc_rvd/props/prop_door_reinforced/rvd_interactable_door_reinforced/001 (93.0, -22.0, -1.996)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (100.0, -50.0, 0.0)
	´security_camera_guard´ (-70.0, -244.0, 10.004)
´security_camera_guard´ ElementSpawnEnemyDummy 100001
	Spawn ene_secret_service_1 (-70.0, -244.0, 10.004).
´death´ ElementEnemyDummyTrigger 100003
	Upon event "death" of ´security_camera_guard´:
		´cameras_off´
´cameras_off´ ElementInstanceOutput 100006
	TRIGGER TIMES 1
	Send instance event "cameras_off" to mission.
	Execute sequence: "state_interaction_enabled" - units/payday2/props/gen_prop_security_console/002 (-135.0, -212.0, 73.0)
´tied´ ElementEnemyDummyTrigger 100004
	Upon event "tied" of ´security_camera_guard´:
		´cameras_off´
´func_sequence_trigger_001´ ElementUnitSequenceTrigger 100031
	TRIGGER TIMES 1
	Upon any sequence:
		"done_exploded" - units/pd2_dlc_rvd/props/prop_door_reinforced/rvd_interactable_door_reinforced/001 (93.0, -22.0, -1.996)
		"done_opened" - units/pd2_dlc_rvd/props/prop_door_reinforced/rvd_interactable_door_reinforced/001 (93.0, -22.0, -1.996)
		"open_door" - units/pd2_dlc_rvd/props/prop_door_reinforced/rvd_interactable_door_reinforced/001 (93.0, -22.0, -1.996)
	Execute:
		Remove nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (100.0, -50.0, 0.0)
