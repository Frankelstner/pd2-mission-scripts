﻿´startup´ MissionScriptElement 100000
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Hide object: units/pd2_dlc_dah/props/dah_prop_whiteboard_b/001 (0.0, 0.0, 100.0)
	Hide object: core/units/light_omni/001 (1.00606, -67.0, 180.685)
	Execute sequence: "hide" - units/pd2_dlc_tag/props/signs/tag_prop_whiteboard_sign_01/001 (-83.0, 7.0, 133.0)
´test_delay´ ElementInstanceInput 100004
	Upon mission event "show_whiteboard":
		Show object: units/pd2_dlc_dah/props/dah_prop_whiteboard_b/001 (0.0, 0.0, 100.0)
		Show object: core/units/light_omni/001 (1.00606, -67.0, 180.685)
		Execute sequence: "show" - units/pd2_dlc_tag/props/signs/tag_prop_whiteboard_sign_01/001 (-83.0, 7.0, 133.0)
		Execute 1: (DELAY 0.5)
			´HR´
			´server´
			´storage´
			´operations´
			´training´
			´kitchen´
´HR´ MissionScriptElement 100022
	Send instance event "HR" to mission.
	Execute sequence: "hr_room" - units/pd2_dlc_tag/props/signs/tag_prop_whiteboard_sign_01/001 (-83.0, 7.0, 133.0)
´server´ MissionScriptElement 100023
	Send instance event "server" to mission.
	Execute sequence: "server_room" - units/pd2_dlc_tag/props/signs/tag_prop_whiteboard_sign_01/001 (-83.0, 7.0, 133.0)
´storage´ MissionScriptElement 100024
	Send instance event "storage" to mission.
	Execute sequence: "storage_room" - units/pd2_dlc_tag/props/signs/tag_prop_whiteboard_sign_01/001 (-83.0, 7.0, 133.0)
´operations´ MissionScriptElement 100025
	Send instance event "operations" to mission.
	Execute sequence: "operations_room" - units/pd2_dlc_tag/props/signs/tag_prop_whiteboard_sign_01/001 (-83.0, 7.0, 133.0)
´training´ MissionScriptElement 100026
	DISABLED
	Send instance event "training" to mission.
	Execute sequence: "training_room" - units/pd2_dlc_tag/props/signs/tag_prop_whiteboard_sign_01/001 (-83.0, 7.0, 133.0)
´kitchen´ MissionScriptElement 100027
	Send instance event "kitchen" to mission.
	Execute sequence: "kitchen" - units/pd2_dlc_tag/props/signs/tag_prop_whiteboard_sign_01/001 (-83.0, 7.0, 133.0)
´input_allow_training´ ElementInstanceInput 100044
	Upon mission event "allow_training":
		Toggle on: ´training´
´input_disable_HR´ ElementInstanceInput 100014
	Upon mission event "disable_HR":
		Toggle off: ´HR´
´input_disable_server´ ElementInstanceInput 100015
	Upon mission event "disable_server":
		Toggle off: ´server´
´input_disable_storage´ ElementInstanceInput 100016
	Upon mission event "disable_storage":
		Toggle off: ´storage´
´input_disable_operations´ ElementInstanceInput 100017
	Upon mission event "disable_operations":
		Toggle off: ´operations´
´input_disable_kitchen´ ElementInstanceInput 100030
	Upon mission event "disable_kitchen":
		Toggle off: ´kitchen´
