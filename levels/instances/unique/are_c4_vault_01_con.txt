ID range vs continent name:
	100000: world

statics
	100005 units/dev_tools/level_tools/dev_door_blocker_1x2x3/001 (525.0, -100.0, 0.0)
	100024 units/dev_tools/level_tools/dev_door_blocker_1x4x3/004 (525.0, 0.0, 0.0)
	100032 units/dev_tools/level_tools/dev_door_blocker_1x4x3/006 (525.0, -100.0, 0.0)
	100075 units/equipment/c4_charge/gen_plantable_c4_x10/001 (250.0, 0.0, 400.0)
		mesh_variation state_vis_hide
	100153 units/equipment/c4_charge/gen_plantable_c4_x10/002 (400.0, 0.0, 400.0)
		mesh_variation state_vis_hide
	100154 units/equipment/c4_charge/gen_plantable_c4_x10/003 (550.0, 0.0, 400.0)
		mesh_variation state_vis_hide
	100003 units/payday2/equipment/gen_interactable_drill_small_no_jam/drill (400.0, 0.0, 450.0)
		mesh_variation deactivate
	100067 units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/001 (195.0, 0.0, 500.0)
		mesh_variation state_set_timer_30
	100093 units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/002 (395.0, 0.0, 500.0)
		mesh_variation state_set_timer_30
	100094 units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/003 (595.0, 0.0, 500.0)
		mesh_variation state_set_timer_30
	100002 units/pd2_dlc_arena/equipment/gen_pku_circle_cutter/001 (195.0, 0.0, 500.0)
		mesh_variation state_vis_hide
	100158 units/pd2_dlc_arena/equipment/gen_pku_circle_cutter/002 (395.0, 0.0, 500.0)
		mesh_variation state_vis_hide
	100004 units/pd2_dlc_arena/equipment/gen_pku_circle_cutter/003 (595.0, 0.0, 500.0)
		mesh_variation state_vis_hide
	100026 units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/001 (200.0, 13.0, 524.0)
		mesh_variation hide
	100037 units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/002 (200.0, -14.4019, 572.696)
		mesh_variation hide
	100085 units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/003 (387.121, 10.7004, 523.153)
		mesh_variation hide
	100086 units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/004 (405.446, -7.62486, 573.259)
		mesh_variation hide
	100087 units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/005 (608.002, 1.8217, 530.978)
		mesh_variation hide
	100088 units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/006 (579.186, 1.8217, 580.767)
		mesh_variation hide
	100134 units/pd2_dlc_arena/props/are_prop_vault_concrete/001 (0.0, 0.0, 400.0)
		mesh_variation default
	100006 units/pd2_dlc_arena/props/are_prop_vault_concrete_cap_01/001 (0.0, 0.0, 400.0)
	100007 units/pd2_dlc_arena/props/are_prop_vault_concrete_cap_02/001 (0.0, 0.0, 400.0)
	100008 units/pd2_dlc_arena/props/are_prop_vault_concrete_cap_03/001 (0.0, 0.0, 400.0)
