ID range vs continent name:
	100000: world

statics
	100003 units/payday2/pickups/gen_pku_bucket_of_money/001 (-18.0, 7.0, 0.0)
		disable_on_ai_graph True
	100004 units/payday2/pickups/gen_pku_bucket_of_money/002 (61.0, 38.0, 0.0)
		disable_on_ai_graph True
	100005 units/payday2/pickups/gen_pku_bucket_of_money/003 (74.0, 27.0, 0.0)
		disable_on_ai_graph True
	100006 units/payday2/pickups/gen_pku_bucket_of_money/004 (-70.0, 31.0, 0.0)
		disable_on_ai_graph True
	100000 units/payday2/props/hlm_prop_bombstrapped/001 (-25.0, 25.0, 0.0)
		disable_on_ai_graph True
