﻿´setup_door´ MissionScriptElement 100001
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	If 95% chance to execute succeeds:
		Execute sequence: "a_open_90" - units/equipment/suburbia_door_crowbar_interaction/001 (0.0, 0.0, 0.0)
		Remove nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/door_blocker (-100.0, -50.0, 0.0)
	Else:
		Execute sequence: "a_closed" - units/equipment/suburbia_door_crowbar_interaction/001 (0.0, 0.0, 0.0)
		Execute sequence: "state_lockpick" - units/equipment/suburbia_door_crowbar_interaction/001 (0.0, 0.0, 0.0)
		Execute sequence: "enable_interaction" - units/equipment/suburbia_door_crowbar_interaction/001 (0.0, 0.0, 0.0)
		Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/door_blocker (-100.0, -50.0, 0.0)
´interact´ ElementUnitSequenceTrigger 100012
	TRIGGER TIMES 1
	Upon sequence "interact" - units/equipment/suburbia_door_crowbar_interaction/001 (0.0, 0.0, 0.0):
		Execute sequence: "a_open_90" - units/equipment/suburbia_door_crowbar_interaction/001 (0.0, 0.0, 0.0)
