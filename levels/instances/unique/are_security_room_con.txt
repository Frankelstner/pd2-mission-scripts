ID range vs continent name:
	100000: world

statics
	100037 units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (200.0, -38.0, 0.0)
	100035 units/payday2/equipment/gen_interactable_door_reinforced/001 (95.0, 0.0, 0.0)
		disable_on_ai_graph True
		mesh_variation activate_door
	100041 units/payday2/equipment/gen_interactable_hack_computer_b/001 (356.263, 270.082, 75.0)
		mesh_variation state_interaction_disabled
	100034 units/payday2/equipment/gen_interactable_hack_computer_b/005 (120.0, 357.0, 74.4073)
		mesh_variation state_interaction_disabled
	100043 units/payday2/equipment/gen_interactable_hack_computer_b/006 (179.0, 349.0, 74.4073)
		mesh_variation state_interaction_disabled
	100013 units/payday2/props/are_prop_hallway_firehosecabinet/001 (275.0, 25.0, 85.39)
	100105 units/payday2/props/com_prop_barista_cup_medium/001 (238.0, 317.0, 74.9602)
	100103 units/payday2/props/com_prop_barista_cup_medium/002 (250.0, 304.0, 4.38983)
	100028 units/payday2/props/gen_prop_security_console/001 (115.181, 320.345, 74.6646)
	100014 units/payday2/props/off_prop_appliance_computer/001 (237.0, 338.0, -0.00299835)
	100030 units/payday2/props/off_prop_appliance_computer/002 (49.3141, 323.005, -0.00299835)
	100015 units/payday2/props/off_prop_appliance_keyboard/001 (326.205, 271.326, 74.997)
	100031 units/payday2/props/off_prop_appliance_mouse/001 (319.951, 224.526, 74.997)
	100024 units/payday2/props/off_prop_appliance_mouse/002 (159.0, 325.0, 74.4073)
	100095 units/payday2/props/off_prop_ceiling_duct_3_a/002 (357.0, 250.0, 295.0)
	100096 units/payday2/props/off_prop_ceiling_duct_3_a/003 (357.0, 150.0, 295.0)
	100044 units/payday2/props/off_prop_ceiling_duct_3_a/004 (357.0, 350.0, 295.0)
	100098 units/payday2/props/off_prop_ceiling_duct_3_a_curve/001 (357.0, 350.0, 270.0)
	100099 units/payday2/props/off_prop_ceiling_duct_3_a_curve/002 (307.0, 50.0001, 270.0)
	100097 units/payday2/props/off_prop_ceiling_duct_patch/001 (307.0, 75.0, 295.0)
	100100 units/payday2/props/off_prop_ceiling_duct_patch/002 (307.0, 275.0, 295.0)
	100016 units/payday2/props/off_prop_generic_filecabinet_h/001 (375.0, 108.0, -0.610169)
		mesh_variation grey
	100094 units/payday2/props/off_prop_officehigh_bulletinboard_02/001 (26.0, 113.0, 135.39)
	100033 units/payday2/props/off_prop_officehigh_chair_standard_white/001 (270.0, 75.0, 0.389831)
	100067 units/payday2/props/off_prop_officehigh_filebox_long_open_b/001 (313.0, 154.0, 25.0)
	100074 units/payday2/props/off_prop_officehigh_filebox_short_base_b/001 (270.0, 347.0, 2.43983)
	100068 units/payday2/props/off_prop_officehigh_filebox_short_lid_b/001 (313.0, 148.0, 16.0)
	100075 units/payday2/props/off_prop_officehigh_filebox_short_lid_b/002 (277.792, 347.0, -0.538322)
	100065 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/001 (316.0, 73.0, -0.61017)
	100066 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/002 (313.0, 154.0, -1.0)
	100069 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/003 (352.0, 76.0, 220.172)
	100070 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/004 (358.0, 145.0, 220.172)
	100072 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/005 (352.0, 202.0, 75.9602)
	100073 units/payday2/props/off_prop_officehigh_filebox_short_whole_b/006 (343.0, 336.0, -0.61017)
	100062 units/payday2/props/paperstacks/off_prop_officehigh_paper_stack_01/001 (270.971, 79.3553, 43.9602)
	100092 units/payday2/props/paperstacks/off_prop_officehigh_paper_stack_01/002 (352.0, 202.0, 104.01)
	100064 units/payday2/props/paperstacks/off_prop_officehigh_paper_stack_02/001 (317.0, 194.0, 74.9602)
	100077 units/payday2/props/paperstacks/off_prop_officehigh_paper_stack_03/001 (222.0, 348.0, 74.9602)
	100104 units/payday2/props/pictures/off_prop_hallway_picture_04/001 (375.0, 289.0, 150.798)
	100101 units/pd2_dlc2/architecture/gov_d_int_decal_mull_5x2m/001 (150.0, 386.0, -1.0)
	100102 units/pd2_dlc2/architecture/gov_d_int_decal_mull_5x2m/002 (386.0, 225.0, -1.0)
	100032 units/pd2_dlc2/props/console/gen_prop_security_monitor_hanging_01/001 (116.0, 365.0, 300.0)
	100021 units/pd2_dlc_arena/architecture/are_security_room/001 (0.0, 25.0, 0.0)
	100047 units/pd2_dlc_arena/props/are_prop_office_lamp_ceiling_short/001 (200.0, 200.0, 300.241)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.47451, 0.717647, 0.984314
			enabled True
			falloff_exponent 4
			far_range 350
			multiplier desklight
			name li_light
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
		mesh_variation light_on
	100029 units/pd2_dlc_arena/props/are_prop_office_telephone/001 (263.232, 345.774, 74.9602)
	100022 units/pd2_dlc_arena/props/set/are_prop_office_desk_left/001 (337.0, 297.0, 0.0)
	100027 units/pd2_dlc_arena/props/set/are_prop_office_desk_straight/001 (128.0, 334.0, 0.0)
	100053 units/pd2_dlc_cage/props/are_prop_office_lamp_desk/lamp_01 (322.867, 326.767, 75.9602)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.490196, 0.976471, 1.0
			enabled True
			falloff_exponent 4
			far_range 65
			multiplier inside of borg queen
			name li_desk_lamp
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
		mesh_variation light_on
	100056 units/pd2_dlc_cage/props/are_prop_office_lamp_desk/lamp_02 (56.8665, 326.767, 75.9602)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.490196, 0.976471, 1.0
			enabled True
			falloff_exponent 4
			far_range 65
			multiplier inside of borg queen
			name li_desk_lamp
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
		mesh_variation light_on
	100093 units/world/architecture/hospital/props/clock/001 (150.0, 25.0, 252.39)
	100076 units/world/props/bottle_3/001 (254.0, 338.0, 9.43983)
	100106 units/world/props/trashcan/trash_dustbin_01/001 (53.0, 53.0, 0.0)
