﻿´startup´ MissionScriptElement 100003
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Execute sequence: "enable_interaction" - units/world/architecture/secret_stash/props/secret_stash_planks_outline_no_sunray/planks_no_ray (1.0, -4.0, 208.0)
	´navlink001´
	´navlink002´
´navlink001´ ElementSpecialObjective 100013
	Create nav link for enemies with action "e_nl_over_1m_jump_window" from (25.9902, -91.0, 0.0) to (53.9665, 65.0255, 0.0)
´navlink002´ ElementSpecialObjective 100015
	Create nav link for enemies with action "e_nl_over_1m_jump_window" from (-27.0098, 86.9998, 0.0) to (-40.6842, -79.0088, 0.0)
´input_toggle_to_godray_planks´ ElementInstanceInput 100011
	Upon mission event "toggle_to_godray_planks":
		Execute sequence: "disable_interaction" - units/world/architecture/secret_stash/props/secret_stash_planks_outline_no_sunray/planks_no_ray (1.0, -4.0, 208.0) (DELAY 1)
		Execute sequence: "enable_interaction" - units/world/architecture/secret_stash/props/secret_stash_planks_outline/planks_ray (1.0, -4.0, 208.0) (DELAY 1)
´broke_window´ ElementUnitSequenceTrigger 100017
	TRIGGER TIMES 1
	Upon sequence "window_broken" - units/pd2_dlc_rvd/props/rvd_prop_warehouse_window/001 (-75.0, 0.0, 100.0):
		Toggle off: ´lemme_smash´
		´check_if_both_window_and_planks_broken（2）´
´check_if_both_window_and_planks_broken（2）´ ElementCounter 100024
	When counter target reached: 
		´navlink001´ (DELAY 0.1)
		´navlink002´ (DELAY 0.1)
´lemme_smash´ ElementUnitSequence 100030
	Execute sequence: "break_window" - units/pd2_dlc_rvd/props/rvd_prop_warehouse_window/001 (-75.0, 0.0, 100.0)
´added_planks´ ElementUnitSequenceTrigger 100019
	TRIGGER TIMES 1
	Upon any sequence:
		"interact" - units/world/architecture/secret_stash/props/secret_stash_planks_outline_no_sunray/planks_no_ray (1.0, -4.0, 208.0)
		"interact" - units/world/architecture/secret_stash/props/secret_stash_planks_outline/planks_ray (1.0, -4.0, 208.0)
	Execute:
		Send instance event "planked_window" to mission.
		remove ´navlink001´
		remove ´navlink002´
		Toggle off: ´navlink001´
		Toggle off: ´navlink002´
´destroyed_planks´ ElementUnitSequenceTrigger 100022
	TRIGGER TIMES 1
	Upon any sequence:
		"destroy_planks" - units/world/architecture/secret_stash/props/secret_stash_planks_outline_no_sunray/planks_no_ray (1.0, -4.0, 208.0)
		"destroy_planks" - units/world/architecture/secret_stash/props/secret_stash_planks_outline/planks_ray (1.0, -4.0, 208.0)
	Execute:
		Send instance event "destroyed_planks" to mission.
		´check_if_both_window_and_planks_broken（2）´
´input_disable_navlinks´ ElementInstanceInput 100032
	Upon mission event "disable_navlinks":
		Toggle off: ´navlink001´
		Toggle off: ´navlink002´
		Toggle off: ´broke_window´
		Toggle off: ´added_planks´
		Toggle off: ´destroyed_planks´
´trigger_special_objective_001´ ElementSpecialObjectiveTrigger 100028
	TRIGGER TIMES 1
	Upon any special objective event:
		"anim_act_04" - ´navlink001´
		"anim_act_04" - ´navlink002´
	Execute:
		´lemme_smash´
´destroy_window´ ElementInstanceInput 100036
	Upon mission event "destroy_window":
		Execute sequence: "break_window" - units/pd2_dlc_rvd/props/rvd_prop_warehouse_window/001 (-75.0, 0.0, 100.0)
		Toggle off: ´lemme_smash´
