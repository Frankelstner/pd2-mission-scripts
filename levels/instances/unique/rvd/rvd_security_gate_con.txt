ID range vs continent name:
	100000: world

statics
	100040 units/dev_tools/level_tools/dev_door_blocker_1x2x3/001 (-100.0, 75.0, 0.0)
	100058 units/dev_tools/level_tools/dev_nav_blocker_1x1x3m/003 (125.0, -25.0, 0.0)
	100059 units/dev_tools/level_tools/dev_nav_blocker_1x1x3m/004 (-225.0, -25.0, 0.0)
	100060 units/dev_tools/level_tools/dev_nav_splitter_2x3m/002 (-100.0, 0.0, 0.0)
	100011 units/payday2/props/gen_interactable_panel_keycard/001 (-155.0, -2.0, 125.0)
		mesh_variation state_5
	100010 units/payday2/props/interactable_panel_keycard/gen_panel_keycard_hack/hacking_device (-155.0, -2.0, 125.0)
	100007 units/pd2_dlc2/architecture/gov_d_int_security_door/gate (200.0, 0.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
