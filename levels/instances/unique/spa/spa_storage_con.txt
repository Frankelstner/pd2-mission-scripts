ID range vs continent name:
	100000: world

statics
	100028 units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-25.0, -50.0, 0.0)
	100004 units/payday2/architecture/bnk_int_fence_door/001 (9.00002, -50.0, 1.0)
		disable_on_ai_graph True
		mesh_variation deactivate_door
	100002 units/payday2/architecture/bnk_int_fence_gate/002 (1.52588e-05, 99.9999, 0.0)
	100003 units/payday2/architecture/bnk_int_fence_wall_short/001 (0.0, 200.0, 0.0)
	100001 units/payday2/architecture/bnk_int_fence_wall_short/002 (6.10352e-05, -100.0, 0.0)
	100012 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_above_door_grey/002 (3.34253, 101.031, 200.0)
	100010 units/payday2/architecture/ind/level/ind_ext_fence_1m1m_grey/003 (5.71692, 201.003, 200.0)
	100009 units/payday2/architecture/ind/level/ind_ext_fence_1m_grey/004 (5.71692, 201.003, 0.0)
	100007 units/payday2/architecture/ind/level/ind_ext_fence_2m1m_grey/001 (3.03418, -23.9651, 200.0)
	100006 units/payday2/architecture/ind/level/ind_ext_fence_2m_shorter_grey/001 (3.0343, -198.965, 0.0)
	100008 units/payday2/architecture/ind/level/ind_ext_fence_door_grey/002 (3.03418, 88.0349, 0.0)
		mesh_variation activate_door
	100000 units/payday2/architecture/mkp_int_floor_4x4m_a/001 (0.0, -200.0, 0.0)
	100025 units/payday2/architecture/mkp_int_floor_4x4m_a/002 (0.0, -200.0, 0.0)
	100063 units/payday2/equipment/gen_interactable_drill_small/001 (-1.52588e-05, 38.0, 85.0)
	100027 units/payday2/equipment/gen_interactable_weapon_case_2x1/001 (166.0, -173.0, 85.0)
	100031 units/payday2/equipment/gen_interactable_weapon_case_2x1/002 (363.617, 4.31476, 25.0)
	100021 units/payday2/equipment/gen_interactable_weapon_case_2x1/003 (368.165, 38.024, 86.0)
	100033 units/payday2/equipment/gen_interactable_weapon_case_2x1/004 (224.256, -164.04, 145.0)
	100041 units/payday2/pickups/gen_pku_crowbar/001 (331.079, -106.721, 43.0)
	100020 units/pd2_dlc_arena/architecture/room_01/are_supply_long_shelf/001 (109.0, -139.0, 0.0)
	100032 units/pd2_dlc_arena/architecture/room_01/are_supply_long_shelf/002 (341.0, -97.0, 0.0)
	100035 units/pd2_dlc_mad/props/mad_interactable_weapon_case_1x1_closed/001 (371.33, -60.619, 85.0)
		mesh_variation show
	100038 units/pd2_dlc_mad/props/mad_interactable_weapon_case_1x1_closed/002 (228.534, -162.487, 26.0)
		mesh_variation disable_interaction
	100085 units/pd2_dlc_spa/props/spa_prop_armory_shelf_ammo/001 (300.0, -150.0, 0.0)
	100088 units/pd2_dlc_spa/props/spa_prop_armory_shelf_ammo/002 (350.0, 100.0, 0.0)
	100083 units/pd2_indiana/architecture/mus_int_lab_wall_a_4x4/001 (0.0, 200.0, 0.0)
	100074 units/world/props/bank/money_wrap_single_bundle/002 (356.874, 73.3556, 146.0)
	100075 units/world/props/bank/money_wrap_single_bundle/003 (371.492, 83.9062, 26.0)
	100076 units/world/props/bank/money_wrap_single_bundle/004 (358.702, 74.3966, 26.0)
	100077 units/world/props/bank/money_wrap_single_bundle/005 (373.081, -80.1839, 27.0)
	100078 units/world/props/bank/money_wrap_single_bundle/006 (355.507, -71.0283, 27.0)
	100079 units/world/props/bank/money_wrap_single_bundle/007 (285.534, -169.337, 86.0)
	100080 units/world/props/bank/money_wrap_single_bundle/008 (265.947, -162.775, 86.0)
	100081 units/world/props/bank/money_wrap_single_bundle/009 (247.334, -163.277, 86.0)
	100082 units/world/props/bank/money_wrap_single_bundle/010 (142.722, -156.645, 146.0)
