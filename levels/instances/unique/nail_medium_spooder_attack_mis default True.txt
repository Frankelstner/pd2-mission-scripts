﻿´spooder_attack_tigger´ ElementAreaTrigger 100000
	DISABLED
	TRIGGER TIMES 1
	Box, width 800.0, height 400.0, depth 800.0, at position (0.0, 300.0, 100.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon while_inside, execute:
		Toggle off: ´inst_in_spooder_flee´
		Execute sequence: "anim_attack" - units/pd2_dlc_nails/props/nls_prop_methlab_spider_med/001 (0.0, 700.0, -550.0)
		Explosion damage 100 with range 500 at position (0.0, 200.0, 100.0) (DELAY 0.5)
		Explosion damage 100 with range 500 at position (0.0, 225.0, 100.0) (DELAY 0.5)
		Send instance event "spooder_attacked" to mission. (DELAY 1.2)
		Toggle on: ´inst_in_spooder_flee´ (DELAY 1.7)
		´inst_in_spooder_flee´ (DELAY 2.2)
		´inst_in_spooder_flee´ (DELAY 2.2)
´inst_in_spooder_flee´ ElementInstanceInput 100055
	TRIGGER TIMES 1
	Upon mission event "spooder_flee":
		Toggle off: ´spooder_attack_tigger´
		´seq_flee_anim´
		Send instance event "spooder_fled" to mission. (DELAY 1)
´seq_flee_anim´ ElementUnitSequence 100013
	TRIGGER TIMES 1
	Execute sequence: "anim_exit" - units/pd2_dlc_nails/props/nls_prop_methlab_spider_med/001 (0.0, 700.0, -550.0)
´inst_in_ready_up_spooder´ ElementInstanceInput 100006
	Upon mission event "spooder_ready_up":
		Execute sequence: "anim_enter" - units/pd2_dlc_nails/props/nls_prop_methlab_spider_med/001 (0.0, 700.0, -550.0)
		Toggle on, trigger times 1: ´spooder_attack_tigger´ (DELAY 3)
		Toggle on, trigger times 1: ´inst_in_spooder_flee´ (DELAY 3)
		Toggle on, trigger times 1: ´seq_flee_anim´ (DELAY 3)
		´inst_in_spooder_flee´ (DELAY 20-30)
´inst_in_spooder_startup´ ElementInstanceInput 100008
	Upon mission event "spooder_startup_reset":
		Toggle off: ´spooder_attack_tigger´
		Execute sequence: "state_vis_hide" - units/pd2_dlc_nails/props/nls_prop_methlab_spider_med/001 (0.0, 700.0, -550.0)
´func_sequence_trigger_001´ ElementUnitSequenceTrigger 100015
	Upon sequence "anim_die" - units/pd2_dlc_nails/props/nls_prop_methlab_spider_med/001 (0.0, 700.0, -550.0):
		Toggle off: ´seq_flee_anim´
		´inst_in_spooder_flee´
