﻿´startup´ MissionScriptElement 100052
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 0.25)
	Execute sequence: "state_hide" - units/pd2_indiana/props/mus_prop_electric_box/masterpiece_electric_box (-650.0, 375.0, 50.0)
	Execute sequence: "hide" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/001 (-350.0, -66.0, 73.1558)
	Execute sequence: "hide" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/002 (-556.0, -66.0, 73.1558)
	Execute sequence: "hide" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/003 (-745.0, 100.0, 73.1558)
	Execute sequence: "hide" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/004 (-745.0, 306.0, 73.1558)
	Execute sequence: "hide" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/005 (-538.0, 460.0, 73.1558)
	Execute sequence: "hide" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/006 (-350.0, 460.0, 73.1558)
	Execute sequence: "hide" - units/pd2_dlc_berry/pickups/bry_pku_master_server/001 (-501.0, 199.0, 99.0)
	Hide object: units/payday2/props/bnk_prop_vault_table/artifact_table (-500.0, 200.0, 0.0)
	Hide object: units/pd2_indiana/props/mus_prop_bars/masterpiece_bars (-700.0, 200.0, 25.0)
	Hide object: units/pd2_dlc_berry/architecture/bry_vault_room_inner_walls/001 (0.0, 0.0, 0.0)
	Hide object: units/pd2_dlc_berry/props/servers/bry_prop_master_server_01/001 (-724.0, -25.0, 0.0)
	Hide object: units/pd2_dlc_berry/props/servers/bry_prop_master_server_01/002 (-724.0, 350.0, 0.0)
	Hide object: units/pd2_dlc_berry/props/servers/bry_prop_master_server_02/001 (-724.0, 50.0, 0.0)
	Hide object: units/pd2_dlc_berry/props/servers/bry_prop_master_server_02/002 (-724.0, 163.0, 0.0)
	Hide object: units/pd2_dlc_berry/props/servers/bry_prop_master_server_03/001 (-724.0, 238.0, 0.0)
	Hide object: units/pd2_dlc_berry/props/servers/bry_prop_master_server_03/002 (-724.0, 425.0, 0.0)
	Hide object: units/pd2_dlc_berry/architecture/security_room/bry_vault_pedestal_cables/server_pedestal_art (0.000152588, 0.000213623, 0.0)
	Hide object: units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/001 (-350.0, -65.0, 2.5)
	Hide object: units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/002 (-550.0, -65.0, 2.5)
	Hide object: units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/003 (-744.0, 99.9999, 2.5)
	Hide object: units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/004 (-744.0, 300.0, 2.5)
	Hide object: units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/005 (-544.0, 458.0, 2.5)
	Hide object: units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/006 (-353.0, 458.0, 2.5)
	Hide object: units/pd2_dlc_berry/pickups/bry_pku_lost_artifact/001 (-500.0, 200.0, 85.0)
	Hide object: units/pd2_dlc_berry/architecture/security_room/bry_vault_pedestal_no_cables/001 (0.000152588, 0.000213623, 0.0)
	Hide object: units/payday2/equipment/gen_equipment_security_camera/001 (-75.0, 71.0, 268.908)
	Hide object: units/payday2/equipment/gen_equipment_security_camera/002 (-166.0, 498.0, 244.0)
	Hide object: units/payday2/equipment/gen_equipment_security_camera/003 (-307.0, 498.0, 213.0)
	Hide object: units/payday2/equipment/gen_equipment_security_camera/004 (-304.0, -109.0, 203.0)
	Hide object: units/payday2/props/bnk_prop_vault_table/artifact_table (-500.0, 200.0, 0.0)
	Hide object: units/pd2_indiana/props/mus_prop_bars/masterpiece_bars (-700.0, 200.0, 25.0)
	Hide object: units/pd2_dlc_berry/architecture/bry_vault_room_inner_walls/001 (0.0, 0.0, 0.0)
	Hide object: units/pd2_dlc_berry/props/servers/bry_prop_master_server_01/001 (-724.0, -25.0, 0.0)
	Hide object: units/pd2_dlc_berry/props/servers/bry_prop_master_server_01/002 (-724.0, 350.0, 0.0)
	Hide object: units/pd2_dlc_berry/props/servers/bry_prop_master_server_02/001 (-724.0, 50.0, 0.0)
	Hide object: units/pd2_dlc_berry/props/servers/bry_prop_master_server_02/002 (-724.0, 163.0, 0.0)
	Hide object: units/pd2_dlc_berry/props/servers/bry_prop_master_server_03/001 (-724.0, 238.0, 0.0)
	Hide object: units/pd2_dlc_berry/props/servers/bry_prop_master_server_03/002 (-724.0, 425.0, 0.0)
	Hide object: units/pd2_dlc_berry/architecture/security_room/bry_vault_pedestal_cables/server_pedestal_art (0.000152588, 0.000213623, 0.0)
	Hide object: units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/001 (-350.0, -65.0, 2.5)
	Hide object: units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/002 (-550.0, -65.0, 2.5)
	Hide object: units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/003 (-744.0, 99.9999, 2.5)
	Hide object: units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/004 (-744.0, 300.0, 2.5)
	Hide object: units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/005 (-544.0, 458.0, 2.5)
	Hide object: units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/006 (-353.0, 458.0, 2.5)
	Hide object: units/pd2_dlc_berry/pickups/bry_pku_lost_artifact/001 (-500.0, 200.0, 85.0)
	Hide object: units/pd2_dlc_berry/architecture/security_room/bry_vault_pedestal_no_cables/001 (0.000152588, 0.000213623, 0.0)
	Execute 1: (DELAY 1)
		´camera_001´
		´camera_003´
		´camera_004´
´camera_001´ ElementEnableUnit 100105
	BASE DELAY  (DELAY 1)
	Show object: units/payday2/equipment/gen_equipment_security_camera/001 (-75.0, 71.0, 268.908)
	Disable camera: units/payday2/equipment/gen_equipment_security_camera/001 (-75.0, 71.0, 268.908)
	Execute sequence: "deathwish" - units/payday2/equipment/gen_equipment_security_camera/001 (-75.0, 71.0, 268.908)
	Toggle on: ´player_camera_001´
´camera_003´ ElementEnableUnit 100109
	BASE DELAY  (DELAY 1)
	Show object: units/payday2/equipment/gen_equipment_security_camera/003 (-307.0, 498.0, 213.0)
	Disable camera: units/payday2/equipment/gen_equipment_security_camera/003 (-307.0, 498.0, 213.0)
	Execute sequence: "deathwish" - units/payday2/equipment/gen_equipment_security_camera/003 (-307.0, 498.0, 213.0)
	Toggle on: ´player_camera_003´
´camera_004´ ElementEnableUnit 100115
	BASE DELAY  (DELAY 1)
	Show object: units/payday2/equipment/gen_equipment_security_camera/004 (-304.0, -109.0, 203.0)
	Disable camera: units/payday2/equipment/gen_equipment_security_camera/004 (-304.0, -109.0, 203.0)
	Execute sequence: "deathwish" - units/payday2/equipment/gen_equipment_security_camera/004 (-304.0, -109.0, 203.0)
	Toggle on: ´player_camera_004´
´player_camera_001´ ElementAccessCamera 100102
	DISABLED
	Create invisible player camera at position: -153.148, 66.8586, 284.619
´player_camera_003´ ElementAccessCamera 100125
	DISABLED
	Create invisible player camera at position: -339.344, 438.64, 240.847
´player_camera_004´ ElementAccessCamera 100130
	DISABLED
	Create invisible player camera at position: -315.359, -46.7709, 204.98
´set_codename_ares´ ElementInstanceInput 100057
	BASE DELAY  (DELAY 1)
	Upon mission event "set_codename_ares":
		Toggle on: ´open_ares´
		Toggle on: ´chosen_ares´
´open_ares´ ElementInstanceInput 100075
	DISABLED
	Upon mission event "open_ares":
		Execute sequence: "state_01" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		Execute sequence: "state_02" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		Execute sequence: "state_03" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		Execute sequence: "green_light" - units/pd2_dlc_berry/props/bry_prop_vault_lamp/new_lamp (10.0, 200.0, 227.169)
		Toggle on, trigger times 1: ´found_vault´
		Toggle on, trigger times 1: ´went_inside_vault´
		Debug message: BAIN: Okay, the vault you selected is open. Go find it. It should be out in the hallways.
		´enable_weapon´
		Toggle on, trigger times 7: ´open_vault_interaction´ (DELAY 5.5)
´chosen_ares´ ElementInstanceInput 100198
	DISABLED
	Upon mission event "chosen_ares":
		Send instance event "vault_chosen" to mission.
		´chosen_vault_contains_loot´
´found_vault´ ElementAreaTrigger 100168
	DISABLED
	TRIGGER TIMES 1
	Box, width 400.0, height 500.0, depth 400.0, at position (100.0, 200.0, 100.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Make teammate close to instigator say: v04
		´open_vault_wp´
´went_inside_vault´ ElementAreaTrigger 100219
	DISABLED
	TRIGGER TIMES 1
	Box, width 400.0, height 500.0, depth 400.0, at position (-225.0, 200.0, 100.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		´func_dialogue_006´
		Send instance event "found_vault" to mission. (DELAY 0.05)
		´loot´ (DELAY 0.05)
		´close´ (DELAY 0.05)
´enable_weapon´ ElementToggle 100200
	DISABLED
	Toggle on, trigger times 1: ´picked_up_weapon´
´picked_up_weapon´ ElementGlobalEventTrigger 100162
	DISABLED
	TRIGGER TIMES 1
	Upon global event "pku_weapons":
		Send instance event "got_loot_from_vault" to mission.
		Toggle off: ´loot_wp´
		Toggle off: ´loot´
		remove ´loot_wp´
		remove ´loot_wp_masterpiece´
		remove ´loot_wp_questionmark´
		Make teammate close to instigator say: v21
		Play dialogue: Play_loc_jr1_36
´open_vault_interaction´ ElementInteraction 100163
	DISABLED
	Create interaction object with ID "hold_open_vault" with timer 1.0at position (19.0, 202.0, 100.0)
	Upon interaction:
		remove ´open_vault_wp´
		Send instance event "door_is_opened" to mission.
		Execute sequence: "state_04" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		´found_loot_vault_open´
´chosen_vault_contains_loot´ ElementInstanceOutput 100199
	DISABLED
	Send instance event "chosen_vault_contains_loot" to mission.
´open_vault_wp´ ElementWaypoint 100160
	Place waypoint with icon "pd2_generic_look" at position (20.0, 202.0, 100.0)
´loot´ MissionScriptElement 100172
	DISABLED
	´loot_wp´
	´loot_wp_masterpiece´
	´loot_wp_questionmark´
´close´ MissionScriptElement 100173
	Debug message: BAIN: Guys, this vault is empty. Use those cameras to spot the good stuff!
	Quit all current dialogue. Play dialogue: Play_loc_jr1_65
´func_dialogue_006´ ElementDialogue 100065
	DISABLED
	TRIGGER TIMES 1
	Quit all current dialogue. Play dialogue: Play_loc_jr1_70
´loot_wp´ ElementWaypoint 100176
	Place waypoint with icon "pd2_loot" at position (-500.0, 200.0, 100.0)
´loot_wp_masterpiece´ ElementWaypoint 100226
	DISABLED
	Place waypoint with icon "pd2_loot" at position (-750.0, 200.0, 100.0)
´loot_wp_questionmark´ ElementWaypoint 100285
	DISABLED
	Place waypoint with icon "pd2_question" at position (-500.0, 200.0, 100.0)
´found_loot_vault_open´ ElementInstanceOutput 100063
	DISABLED
	Send instance event "found_loot_vault_open" to mission.
´set_codename_zeus´ ElementInstanceInput 100058
	BASE DELAY  (DELAY 1)
	Upon mission event "set_codename_zeus":
		Toggle on: ´open_zeus´
		Toggle on: ´chosen_zeus´
´open_zeus´ ElementInstanceInput 100076
	DISABLED
	Upon mission event "open_zeus":
		Execute sequence: "state_01" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		Execute sequence: "state_02" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		Execute sequence: "state_03" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		Execute sequence: "green_light" - units/pd2_dlc_berry/props/bry_prop_vault_lamp/new_lamp (10.0, 200.0, 227.169)
		Toggle on, trigger times 1: ´found_vault´
		Toggle on, trigger times 1: ´went_inside_vault´
		Debug message: BAIN: Okay, the vault you selected is open. Go find it. It should be out in the hallways.
		´enable_weapon´
		Toggle on, trigger times 7: ´open_vault_interaction´ (DELAY 5.5)
´chosen_zeus´ ElementInstanceInput 100197
	DISABLED
	Upon mission event "chosen_zeus":
		Send instance event "vault_chosen" to mission.
		´chosen_vault_contains_loot´
´set_codename_poseidon´ ElementInstanceInput 100059
	BASE DELAY  (DELAY 1)
	Upon mission event "set_codename_poseidon":
		Toggle on: ´open_poseidon´
		Toggle on: ´chosen_poseidon´
´open_poseidon´ ElementInstanceInput 100077
	DISABLED
	Upon mission event "open_poseidon":
		Execute sequence: "state_01" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		Execute sequence: "state_02" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		Execute sequence: "state_03" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		Execute sequence: "green_light" - units/pd2_dlc_berry/props/bry_prop_vault_lamp/new_lamp (10.0, 200.0, 227.169)
		Toggle on, trigger times 1: ´found_vault´
		Toggle on, trigger times 1: ´went_inside_vault´
		Debug message: BAIN: Okay, the vault you selected is open. Go find it. It should be out in the hallways.
		´enable_weapon´
		Toggle on, trigger times 7: ´open_vault_interaction´ (DELAY 5.5)
´chosen_poseidon´ ElementInstanceInput 100196
	DISABLED
	Upon mission event "chosen_poseidon":
		Send instance event "vault_chosen" to mission.
		´chosen_vault_contains_loot´
´set_codename_demeter´ ElementInstanceInput 100060
	BASE DELAY  (DELAY 1)
	Upon mission event "set_codename_demeter":
		Toggle on: ´open_demeter´
		Toggle on: ´chosen_demeter´
´open_demeter´ ElementInstanceInput 100078
	DISABLED
	Upon mission event "open_demeter":
		Execute sequence: "state_01" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		Execute sequence: "state_02" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		Execute sequence: "state_03" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		Execute sequence: "green_light" - units/pd2_dlc_berry/props/bry_prop_vault_lamp/new_lamp (10.0, 200.0, 227.169)
		Toggle on, trigger times 1: ´found_vault´
		Toggle on, trigger times 1: ´went_inside_vault´
		Debug message: BAIN: Okay, the vault you selected is open. Go find it. It should be out in the hallways.
		´enable_weapon´
		Toggle on, trigger times 7: ´open_vault_interaction´ (DELAY 5.5)
´chosen_demeter´ ElementInstanceInput 100195
	DISABLED
	Upon mission event "chosen_demeter":
		Send instance event "vault_chosen" to mission.
		´chosen_vault_contains_loot´
´set_codename_hades´ ElementInstanceInput 100061
	BASE DELAY  (DELAY 1)
	Upon mission event "set_codename_hades":
		Toggle on: ´open_hades´
		Toggle on: ´chosen_hades´
´open_hades´ ElementInstanceInput 100079
	DISABLED
	Upon mission event "open_hades":
		Execute sequence: "state_01" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		Execute sequence: "state_02" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		Execute sequence: "state_03" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		Execute sequence: "green_light" - units/pd2_dlc_berry/props/bry_prop_vault_lamp/new_lamp (10.0, 200.0, 227.169)
		Toggle on, trigger times 1: ´found_vault´
		Toggle on, trigger times 1: ´went_inside_vault´
		Debug message: BAIN: Okay, the vault you selected is open. Go find it. It should be out in the hallways.
		´enable_weapon´
		Toggle on, trigger times 7: ´open_vault_interaction´ (DELAY 5.5)
´chosen_hades´ ElementInstanceInput 100194
	DISABLED
	Upon mission event "chosen_hades":
		Send instance event "vault_chosen" to mission.
		´chosen_vault_contains_loot´
´set_codename_chronos´ ElementInstanceInput 100062
	BASE DELAY  (DELAY 1)
	Upon mission event "set_codename_chronos":
		Toggle on: ´open_chronos´
		Toggle on: ´chosen_chronos´
´open_chronos´ ElementInstanceInput 100080
	DISABLED
	Upon mission event "open_chronos":
		Execute sequence: "state_01" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		Execute sequence: "state_02" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		Execute sequence: "state_03" - units/pd2_dlc_berry/props/bry_prop_vault_door/001 (-99.9999, 300.0, 2.5)
		Execute sequence: "green_light" - units/pd2_dlc_berry/props/bry_prop_vault_lamp/new_lamp (10.0, 200.0, 227.169)
		Toggle on, trigger times 1: ´found_vault´
		Toggle on, trigger times 1: ´went_inside_vault´
		Debug message: BAIN: Okay, the vault you selected is open. Go find it. It should be out in the hallways.
		´enable_weapon´
		Toggle on, trigger times 7: ´open_vault_interaction´ (DELAY 5.5)
´chosen_chronos´ ElementInstanceInput 100145
	DISABLED
	Upon mission event "chosen_chronos":
		Send instance event "vault_chosen" to mission.
		´chosen_vault_contains_loot´
´set_content_weapon´ ElementInstanceInput 100131
	BASE DELAY  (DELAY 1)
	Upon mission event "set_content_weapon":
		Toggle off: ´close´
		Toggle on: ´loot´
		Toggle on: ´chosen_vault_contains_loot´
		Toggle on: ´found_loot_vault_open´
		Toggle on: ´enable_weapon´
		Toggle on: ´func_dialogue_006´
		Show object: units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/001 (-350.0, -65.0, 2.5)
		Show object: units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/002 (-550.0, -65.0, 2.5)
		Show object: units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/003 (-744.0, 99.9999, 2.5)
		Show object: units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/004 (-744.0, 300.0, 2.5)
		Show object: units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/005 (-544.0, 458.0, 2.5)
		Show object: units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/006 (-353.0, 458.0, 2.5)
		Toggle off: ´loot_wp´
		Toggle on: ´loot_wp_questionmark´
		Execute sequence: "enable_interaction" - units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/001 (-350.0, -65.0, 2.5) (DELAY 0.05)
		Execute sequence: "enable_interaction" - units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/002 (-550.0, -65.0, 2.5) (DELAY 0.05)
		Execute sequence: "enable_interaction" - units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/003 (-744.0, 99.9999, 2.5) (DELAY 0.05)
		Execute sequence: "enable_interaction" - units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/004 (-744.0, 300.0, 2.5) (DELAY 0.05)
		Execute sequence: "enable_interaction" - units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/005 (-544.0, 458.0, 2.5) (DELAY 0.05)
		Execute sequence: "enable_interaction" - units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/006 (-353.0, 458.0, 2.5) (DELAY 0.05)
		Execute 1: (DELAY 0.05)
			´case_001´
			´case_002´
			´case_003´
			´case_004´
			´case_005´
			´case_006´
		Execute 1: (DELAY 1.05)
			Execute 1:
				´enable_c4_006´
				´enable_c4_005´
				´enable_c4_004´
				´enable_c4_003´
				´enable_c4_002´
				´enable_c4_001´
			Execute 2:
				´enable_c4_006´
				´enable_c4_005´
				´enable_c4_004´
				´enable_c4_003´
				´enable_c4_002´
				´enable_c4_001´
´case_001´ ElementToggle 100015
	Toggle on: ´enable_case001´
	Execute sequence: "show" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/001 (-350.0, -66.0, 73.1558)
	Toggle off: ´enable_c4_001´
´enable_case001´ ElementUnitSequence 100037
	DISABLED
	Execute sequence: "enable_interaction" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/001 (-350.0, -66.0, 73.1558)
´case_002´ ElementToggle 100016
	Toggle on: ´enable_case002´
	Execute sequence: "show" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/002 (-556.0, -66.0, 73.1558)
	Toggle off: ´enable_c4_002´
´enable_case002´ ElementUnitSequence 100041
	DISABLED
	Execute sequence: "enable_interaction" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/002 (-556.0, -66.0, 73.1558)
´case_003´ ElementToggle 100017
	Toggle on: ´enable_case003´
	Execute sequence: "show" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/003 (-745.0, 100.0, 73.1558)
	Toggle off: ´enable_c4_003´
´enable_case003´ ElementUnitSequence 100050
	DISABLED
	Execute sequence: "enable_interaction" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/003 (-745.0, 100.0, 73.1558)
´case_004´ ElementToggle 100018
	Toggle on: ´enable_case004´
	Execute sequence: "show" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/004 (-745.0, 306.0, 73.1558)
	Toggle off: ´enable_c4_004´
´enable_case004´ ElementUnitSequence 100054
	DISABLED
	Execute sequence: "enable_interaction" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/004 (-745.0, 306.0, 73.1558)
´case_005´ ElementToggle 100019
	Toggle on: ´enable_case005´
	Execute sequence: "show" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/005 (-538.0, 460.0, 73.1558)
	Toggle off: ´enable_c4_005´
´enable_case005´ ElementUnitSequence 100186
	DISABLED
	Execute sequence: "enable_interaction" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/005 (-538.0, 460.0, 73.1558)
´case_006´ ElementToggle 100020
	Toggle on: ´enable_case006´
	Execute sequence: "show" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/006 (-350.0, 460.0, 73.1558)
	Toggle off: ´enable_c4_006´
´enable_case006´ ElementUnitSequence 100191
	DISABLED
	Execute sequence: "enable_interaction" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/006 (-350.0, 460.0, 73.1558)
´enable_c4_006´ ElementToggle 100275
	Toggle on: ´omg_c4_006´
´omg_c4_006´ ElementUnitSequence 100254
	DISABLED
	Execute sequence: "show_interactive" - units/equipment/charge/c4_diffusible/006 (-354.0, 462.0, 73.0)
	´xplode_006´ (DELAY 6)
´enable_c4_005´ ElementToggle 100274
	Toggle on: ´omg_c4_005´
´omg_c4_005´ ElementUnitSequence 100253
	DISABLED
	Execute sequence: "show_interactive" - units/equipment/charge/c4_diffusible/005 (-554.0, 462.0, 73.0)
	´xplode_005´ (DELAY 6)
´enable_c4_004´ ElementToggle 100273
	Toggle on: ´omg_c4_004´
´omg_c4_004´ ElementUnitSequence 100252
	DISABLED
	Execute sequence: "show_interactive" - units/equipment/charge/c4_diffusible/004 (-754.0, 295.0, 73.0)
	´xplode_004´ (DELAY 6)
´enable_c4_003´ ElementToggle 100272
	Toggle on: ´omg_c4_003´
´omg_c4_003´ ElementUnitSequence 100251
	DISABLED
	Execute sequence: "show_interactive" - units/equipment/charge/c4_diffusible/003 (-754.0, 94.9999, 73.0)
	´xplode_003´ (DELAY 6)
´enable_c4_002´ ElementToggle 100271
	Toggle on: ´omg_c4_002´
´omg_c4_002´ ElementUnitSequence 100250
	DISABLED
	Execute sequence: "show_interactive" - units/equipment/charge/c4_diffusible/002 (-554.0, -55.0, 73.0)
	´xplode_002´ (DELAY 6)
´enable_c4_001´ ElementToggle 100248
	Toggle on: ´omg_c4_001´
´omg_c4_001´ ElementUnitSequence 100249
	DISABLED
	Execute sequence: "show_interactive" - units/equipment/charge/c4_diffusible/001 (-354.0, -55.0, 73.0)
	´xplode_001´ (DELAY 6)
´xplode_006´ MissionScriptElement 100305
	Play effect effects/particles/explosions/apartment_explosion at position (-352.0, 454.0, 78.5)
	Play audio "swat_explosion" at position (-351.0, 451.0, 120.231)
	Execute sequence: "hide" - units/equipment/charge/c4_diffusible/006 (-354.0, 462.0, 73.0)
	Explosion damage 100 with range 550 at position (-509.0, 196.0, 102.5)
´xplode_005´ MissionScriptElement 100304
	Play effect effects/particles/explosions/apartment_explosion at position (-538.0, 454.0, 78.5)
	Play audio "swat_explosion" at position (-533.0, 450.0, 120.231)
	Execute sequence: "hide" - units/equipment/charge/c4_diffusible/005 (-554.0, 462.0, 73.0)
	Explosion damage 100 with range 550 at position (-509.0, 196.0, 102.5)
´xplode_004´ MissionScriptElement 100303
	Play effect effects/particles/explosions/apartment_explosion at position (-738.0, 304.0, 78.5)
	Play audio "swat_explosion" at position (-733.0, 300.0, 120.231)
	Execute sequence: "hide" - units/equipment/charge/c4_diffusible/004 (-754.0, 295.0, 73.0)
	Explosion damage 100 with range 550 at position (-509.0, 196.0, 102.5)
´xplode_003´ MissionScriptElement 100302
	Play effect effects/particles/explosions/apartment_explosion at position (-738.0, 104.0, 78.5)
	Play audio "swat_explosion" at position (-733.0, 100.0, 120.231)
	Execute sequence: "hide" - units/equipment/charge/c4_diffusible/003 (-754.0, 94.9999, 73.0)
	Explosion damage 100 with range 550 at position (-509.0, 196.0, 102.5)
´xplode_002´ MissionScriptElement 100301
	Play effect effects/particles/explosions/apartment_explosion at position (-550.0, -62.0, 78.5)
	Play audio "swat_explosion" at position (-550.0, -50.0, 120.231)
	Execute sequence: "hide" - units/equipment/charge/c4_diffusible/002 (-554.0, -55.0, 73.0)
	Explosion damage 100 with range 550 at position (-509.0, 196.0, 102.5)
´xplode_001´ MissionScriptElement 100300
	Play effect effects/particles/explosions/apartment_explosion at position (-350.0, -62.0, 78.5)
	Explosion damage 100 with range 550 at position (-509.0, 196.0, 102.5)
	Play audio "swat_explosion" at position (-350.0, -50.0, 119.231)
	Execute sequence: "hide" - units/equipment/charge/c4_diffusible/001 (-354.0, -55.0, 73.0)
´set_content_art´ ElementInstanceInput 100132
	BASE DELAY  (DELAY 1)
	Upon mission event "set_content_art":
		Show object: units/pd2_indiana/props/mus_prop_bars/masterpiece_bars (-700.0, 200.0, 25.0)
		Show object: units/pd2_dlc_berry/architecture/bry_vault_room_inner_walls/001 (0.0, 0.0, 0.0)
		Toggle on: ´loot_wp_masterpiece´
		Toggle off: ´close´
		Toggle on: ´loot´
		Toggle on: ´chosen_vault_contains_loot´
		Toggle on: ´found_loot_vault_open´
		Toggle on: ´loot_wp_masterpiece´
		Toggle off: ´loot_wp´
		Execute sequence: "state_show" - units/pd2_indiana/props/mus_prop_electric_box/masterpiece_electric_box (-650.0, 375.0, 50.0)
		Execute sequence: "show" - units/pd2_dlc_berry/pickups/bry_pku_masterpiece/001 (-753.0, 202.0, 156.5)
		Toggle on: ´open_powerbox´
´open_powerbox´ ElementInteraction 100224
	DISABLED
	Create interaction object with ID "hold_open" at position (-641.0, 350.0, 150.0)
	Upon interaction:
		Execute sequence: "open_door" - units/pd2_indiana/props/mus_prop_electric_box/masterpiece_electric_box (-650.0, 375.0, 50.0)
		Execute sequence: "interact_enabled" - units/pd2_indiana/props/mus_prop_electric_box/masterpiece_electric_box (-650.0, 375.0, 50.0)
´set_content_server´ ElementInstanceInput 100133
	BASE DELAY  (DELAY 1)
	Upon mission event "set_content_server":
		Execute sequence: "show_and_enable_interaction" - units/pd2_dlc_berry/pickups/bry_pku_master_server/001 (-501.0, 199.0, 99.0)
		Execute sequence: "state_contour_disable" - units/pd2_dlc_berry/pickups/bry_pku_master_server/001 (-501.0, 199.0, 99.0)
		Show object: units/pd2_dlc_berry/props/servers/bry_prop_master_server_01/001 (-724.0, -25.0, 0.0)
		Show object: units/pd2_dlc_berry/props/servers/bry_prop_master_server_01/002 (-724.0, 350.0, 0.0)
		Show object: units/pd2_dlc_berry/props/servers/bry_prop_master_server_02/001 (-724.0, 50.0, 0.0)
		Show object: units/pd2_dlc_berry/props/servers/bry_prop_master_server_02/002 (-724.0, 163.0, 0.0)
		Show object: units/pd2_dlc_berry/props/servers/bry_prop_master_server_03/001 (-724.0, 238.0, 0.0)
		Show object: units/pd2_dlc_berry/props/servers/bry_prop_master_server_03/002 (-724.0, 425.0, 0.0)
		Show object: units/pd2_dlc_berry/architecture/security_room/bry_vault_pedestal_cables/server_pedestal_art (0.000152588, 0.000213623, 0.0)
		Toggle off: ´close´
		Toggle on: ´loot´
		Toggle on: ´chosen_vault_contains_loot´
		Toggle on: ´found_loot_vault_open´
´got_server_or_art´ ElementUnitSequenceTrigger 100161
	Upon sequence "load" - units/pd2_dlc_berry/pickups/bry_pku_lost_artifact/001 (-500.0, 200.0, 85.0):
		Send instance event "got_loot_from_vault" to mission.
		Toggle off: ´loot_wp´
		Toggle off: ´loot´
		remove ´loot_wp´
		remove ´loot_wp_masterpiece´
		remove ´loot_wp_questionmark´
		Make teammate close to instigator say: v21
		Play dialogue: Play_loc_jr1_34
´point_access_camera_trigger_001´ ElementAccessCameraTrigger 100158
	Upon any camera event:
		accessed: ´player_camera_003´
		accessed: ´player_camera_001´
		accessed: ´player_camera_004´
	Execute:
		Send instance event "used_camera" to mission.
´remove_all_wp´ ElementInstanceInput 100208
	Upon mission event "remove_all_wp":
		remove ´loot_wp´
		remove ´burn_wp´
		remove ´loot_wp_masterpiece´
		remove ´loot_wp_questionmark´
´burn_wp´ ElementWaypoint 100184
	Place waypoint with icon "pd2_fire" at position (400.0, 1200.0, 0.0)
´opened_crate001´ ElementUnitSequenceTrigger 100031
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/001 (-350.0, -65.0, 2.5):
		´enable_case001´
		´omg_c4_001´
´opened_crate002´ ElementUnitSequenceTrigger 100040
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/002 (-550.0, -65.0, 2.5):
		´enable_case002´
		´omg_c4_002´
´opened_crate003´ ElementUnitSequenceTrigger 100049
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/003 (-744.0, 99.9999, 2.5):
		´enable_case003´
		´omg_c4_003´
´opened_crate004´ ElementUnitSequenceTrigger 100053
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/004 (-744.0, 300.0, 2.5):
		´enable_case004´
		´omg_c4_004´
´opened_crate005´ ElementUnitSequenceTrigger 100166
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/005 (-544.0, 458.0, 2.5):
		´enable_case005´
		´omg_c4_005´
´opened_crate006´ ElementUnitSequenceTrigger 100189
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/006 (-353.0, 458.0, 2.5):
		´enable_case006´
		´omg_c4_006´
´set_content_artifact´ ElementInstanceInput 100204
	BASE DELAY  (DELAY 1)
	Upon mission event "set_content_artifact":
		Show object: units/payday2/props/bnk_prop_vault_table/artifact_table (-500.0, 200.0, 0.0)
		Show object: units/pd2_dlc_berry/pickups/bry_pku_lost_artifact/001 (-500.0, 200.0, 85.0)
		Toggle off: ´close´
		Toggle on: ´loot´
		Toggle on: ´chosen_vault_contains_loot´
		Toggle on: ´found_loot_vault_open´
		Execute sequence: "enable_interaction" - units/pd2_dlc_berry/pickups/bry_pku_lost_artifact/001 (-500.0, 200.0, 85.0) (DELAY 0.5)
´func_sequence_trigger_001´ ElementUnitSequenceTrigger 100227
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_indiana/props/mus_prop_electric_box/masterpiece_electric_box (-650.0, 375.0, 50.0):
		Execute sequence: "bars_play_open" - units/pd2_indiana/props/mus_prop_bars/masterpiece_bars (-700.0, 200.0, 25.0)
		Execute sequence: "enable_interaction" - units/pd2_dlc_berry/pickups/bry_pku_masterpiece/001 (-753.0, 202.0, 156.5)
´got_server_or_art001´ ElementUnitSequenceTrigger 100026
	Upon sequence "load" - units/pd2_dlc_berry/pickups/bry_pku_masterpiece/001 (-753.0, 202.0, 156.5):
		Send instance event "got_loot_from_vault" to mission.
		Toggle off: ´loot_wp´
		Toggle off: ´loot´
		remove ´loot_wp´
		remove ´loot_wp_masterpiece´
		remove ´loot_wp_questionmark´
		Make teammate close to instigator say: v21
		Play dialogue: Play_loc_jr1_33
´got_server_or_art002´ ElementUnitSequenceTrigger 100027
	Upon sequence "load" - units/pd2_dlc_berry/pickups/bry_pku_master_server/001 (-501.0, 199.0, 99.0):
		Send instance event "got_loot_from_vault" to mission.
		Toggle off: ´loot_wp´
		Toggle off: ´loot´
		remove ´loot_wp´
		remove ´loot_wp_masterpiece´
		remove ´loot_wp_questionmark´
		Make teammate close to instigator say: v21
		Play dialogue: Play_loc_jr1_35
		Show object: units/pd2_dlc_berry/architecture/security_room/bry_vault_pedestal_no_cables/001 (0.000152588, 0.000213623, 0.0)
		Hide object: units/pd2_dlc_berry/architecture/security_room/bry_vault_pedestal_cables/server_pedestal_art (0.000152588, 0.000213623, 0.0)
´got_prototype´ ElementUnitSequenceTrigger 100009
	Upon any sequence:
		"load" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/001 (-350.0, -66.0, 73.1558)
		"load" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/002 (-556.0, -66.0, 73.1558)
		"load" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/003 (-745.0, 100.0, 73.1558)
		"load" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/004 (-745.0, 306.0, 73.1558)
		"load" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/005 (-538.0, 460.0, 73.1558)
		"load" - units/pd2_dlc_berry/pickups/bry_pku_prototype_box/006 (-350.0, 460.0, 73.1558)
	Execute:
		Send instance event "got_loot_from_vault" to mission.
		Toggle off: ´loot_wp´
		Toggle off: ´loot´
		remove ´loot_wp´
		remove ´loot_wp_masterpiece´
		remove ´loot_wp_questionmark´
		Make teammate close to instigator say: v21
		Play dialogue: Play_loc_jr1_36
´defused_001´ ElementUnitSequenceTrigger 100294
	TRIGGER TIMES 1
	Upon sequence "interact" - units/equipment/charge/c4_diffusible/001 (-354.0, -55.0, 73.0):
		Toggle off: ´xplode_001´
´defused_002´ ElementUnitSequenceTrigger 100295
	TRIGGER TIMES 1
	Upon sequence "interact" - units/equipment/charge/c4_diffusible/002 (-554.0, -55.0, 73.0):
		Toggle off: ´xplode_002´
´defused_003´ ElementUnitSequenceTrigger 100296
	TRIGGER TIMES 1
	Upon sequence "interact" - units/equipment/charge/c4_diffusible/003 (-754.0, 94.9999, 73.0):
		Toggle off: ´xplode_003´
´defused_004´ ElementUnitSequenceTrigger 100297
	TRIGGER TIMES 1
	Upon sequence "interact" - units/equipment/charge/c4_diffusible/004 (-754.0, 295.0, 73.0):
		Toggle off: ´xplode_004´
´defused_005´ ElementUnitSequenceTrigger 100298
	TRIGGER TIMES 1
	Upon sequence "interact" - units/equipment/charge/c4_diffusible/005 (-554.0, 462.0, 73.0):
		Toggle off: ´xplode_005´
´defused_006´ ElementUnitSequenceTrigger 100299
	TRIGGER TIMES 1
	Upon sequence "interact" - units/equipment/charge/c4_diffusible/006 (-354.0, 462.0, 73.0):
		Toggle off: ´xplode_006´
