﻿´startup´ ElementDisableUnit 100052
	EXECUTE ON STARTUP
	Hide object: units/payday2/architecture/mkp_int_wall_1m_a/001 (50.0, -25.0, 0.0)
	Hide object: units/payday2/architecture/mkp_int_wall_1x1m_a_trim/001 (-8.0, -25.0, 135.9)
´spawn´ ElementInstanceInput 100019
	Upon mission event "spawn":
		Execute 1:
			´dozers´
			´shields´
´dozers´ MissionScriptElement 100041
	If Normal:
		If Normal, Hard:
			´dozer_green001´ (475.0, 0.0, 277.5)
		If Very Hard, Overkill:
			´dozer_black001_changed_to_green´ (475.0, 0.0, 502.5)
		If Mayhem, Death Wish, One Down:
			´dozer_skull001´ (475.0, 0.0, 727.5)
	If Very Hard, Overkill:
		If Normal, Hard:
			´dozer_green´ (600.0, 0.0, 277.5)
		If Very Hard, Overkill:
			´dozer_black_changed_to_green´ (600.0, 0.0, 502.5)
		If Mayhem, Death Wish, One Down:
			´dozer_skull´ (600.0, 0.0, 727.5)
		If Normal, Hard:
			´dozer_green001´ (475.0, 0.0, 277.5)
		If Very Hard, Overkill:
			´dozer_black001_changed_to_green´ (475.0, 0.0, 502.5)
		If Mayhem, Death Wish, One Down:
			´dozer_skull001´ (475.0, 0.0, 727.5)
		If Normal, Hard:
			´dozer_green002´ (350.0, 0.0, 277.5)
		If Very Hard, Overkill:
			´dozer_black002_changed_to_green´ (350.0, 0.0, 502.5)
		If Mayhem, Death Wish, One Down:
			´dozer_skull002´ (350.0, 0.0, 727.5)
	If Mayhem, Death Wish, One Down:
		If Normal, Hard:
			´dozer_green´ (600.0, 0.0, 277.5)
		If Very Hard, Overkill:
			´dozer_black_changed_to_green´ (600.0, 0.0, 502.5)
		If Mayhem, Death Wish, One Down:
			´dozer_skull´ (600.0, 0.0, 727.5)
		If Normal, Hard:
			´dozer_green001´ (475.0, 0.0, 277.5)
		If Very Hard, Overkill:
			´dozer_black001_changed_to_green´ (475.0, 0.0, 502.5)
		If Mayhem, Death Wish, One Down:
			´dozer_skull001´ (475.0, 0.0, 727.5)
		If Normal, Hard:
			´dozer_green002´ (350.0, 0.0, 277.5)
		If Very Hard, Overkill:
			´dozer_black002_changed_to_green´ (350.0, 0.0, 502.5)
		If Mayhem, Death Wish, One Down:
			´dozer_skull002´ (350.0, 0.0, 727.5)
	If Hard:
		If Normal, Hard:
			´dozer_green´ (600.0, 0.0, 277.5)
		If Very Hard, Overkill:
			´dozer_black_changed_to_green´ (600.0, 0.0, 502.5)
		If Mayhem, Death Wish, One Down:
			´dozer_skull´ (600.0, 0.0, 727.5)
		If Normal, Hard:
			´dozer_green002´ (350.0, 0.0, 277.5)
		If Very Hard, Overkill:
			´dozer_black002_changed_to_green´ (350.0, 0.0, 502.5)
		If Mayhem, Death Wish, One Down:
			´dozer_skull002´ (350.0, 0.0, 727.5)
´shields´ MissionScriptElement 100042
	If Normal:
		If Normal, Hard:
			´shield_blue´ (-725.0, 0.0, 502.5)
		If Very Hard, Overkill, Mayhem, Death Wish, One Down:
			´shield_green´ (-725.0, 0.0, 727.5)
		If Normal, Hard:
			´shield_blue2´ (-850.0, 0.0, 502.5)
		If Very Hard, Overkill, Mayhem, Death Wish, One Down:
			´shield_green2´ (-850.0, 0.0, 727.5)
		If Normal, Hard:
			´shield_blue3´ (-975.0, 0.0, 502.5)
		If Very Hard, Overkill, Mayhem, Death Wish, One Down:
			´shield_green3´ (-950.0, 0.0, 727.5)
	If Hard:
		If Normal, Hard:
			´shield_blue´ (-725.0, 0.0, 502.5)
		If Very Hard, Overkill, Mayhem, Death Wish, One Down:
			´shield_green´ (-725.0, 0.0, 727.5)
		If Normal, Hard:
			´shield_blue2´ (-850.0, 0.0, 502.5)
		If Very Hard, Overkill, Mayhem, Death Wish, One Down:
			´shield_green2´ (-850.0, 0.0, 727.5)
		If Normal, Hard:
			´shield_blue3´ (-975.0, 0.0, 502.5)
		If Very Hard, Overkill, Mayhem, Death Wish, One Down:
			´shield_green3´ (-950.0, 0.0, 727.5)
		If Normal, Hard:
			´dozer_green001´ (475.0, 0.0, 277.5)
		If Very Hard, Overkill:
			´dozer_black001_changed_to_green´ (475.0, 0.0, 502.5)
		If Mayhem, Death Wish, One Down:
			´dozer_skull001´ (475.0, 0.0, 727.5)
	If Very Hard, Overkill:
		If Normal, Hard:
			´dozer_green001´ (475.0, 0.0, 277.5)
		If Very Hard, Overkill:
			´dozer_black001_changed_to_green´ (475.0, 0.0, 502.5)
		If Mayhem, Death Wish, One Down:
			´dozer_skull001´ (475.0, 0.0, 727.5)
		If Normal, Hard:
			´shield_blue´ (-725.0, 0.0, 502.5)
		If Very Hard, Overkill, Mayhem, Death Wish, One Down:
			´shield_green´ (-725.0, 0.0, 727.5)
		If Normal, Hard:
			´shield_blue2´ (-850.0, 0.0, 502.5)
		If Very Hard, Overkill, Mayhem, Death Wish, One Down:
			´shield_green2´ (-850.0, 0.0, 727.5)
		If Normal, Hard:
			´shield_blue3´ (-975.0, 0.0, 502.5)
		If Very Hard, Overkill, Mayhem, Death Wish, One Down:
			´shield_green3´ (-950.0, 0.0, 727.5)
	If Mayhem, Death Wish, One Down:
		If Normal, Hard:
			´dozer_green001´ (475.0, 0.0, 277.5)
		If Very Hard, Overkill:
			´dozer_black001_changed_to_green´ (475.0, 0.0, 502.5)
		If Mayhem, Death Wish, One Down:
			´dozer_skull001´ (475.0, 0.0, 727.5)
		If Normal, Hard:
			´shield_blue´ (-725.0, 0.0, 502.5)
		If Very Hard, Overkill, Mayhem, Death Wish, One Down:
			´shield_green´ (-725.0, 0.0, 727.5)
		If Normal, Hard:
			´shield_blue2´ (-850.0, 0.0, 502.5)
		If Very Hard, Overkill, Mayhem, Death Wish, One Down:
			´shield_green2´ (-850.0, 0.0, 727.5)
		If Normal, Hard:
			´shield_blue3´ (-975.0, 0.0, 502.5)
		If Very Hard, Overkill, Mayhem, Death Wish, One Down:
			´shield_green3´ (-950.0, 0.0, 727.5)
´dozer_green001´ ElementSpawnEnemyDummy 100008
	Spawn ene_bulldozer_1.
	No group AI
	Orientations:
		(0.0, -500.0, 2.5)
´dozer_black001_changed_to_green´ ElementSpawnEnemyDummy 100007
	Spawn ene_bulldozer_1.
	No group AI
	Orientations:
		(0.0, -500.0, 2.5)
´dozer_skull001´ ElementSpawnEnemyDummy 100004
	Spawn ene_bulldozer_3.
	No group AI
	Orientations:
		(0.0, -500.0, 2.5)
´dozer_green´ ElementSpawnEnemyDummy 100001
	Spawn ene_bulldozer_1.
	No group AI
	Orientations:
		(100.0, -125.0, 34.3561)
´dozer_black_changed_to_green´ ElementSpawnEnemyDummy 100002
	Spawn ene_bulldozer_1.
	No group AI
	Orientations:
		(100.0, -125.0, 34.3561)
´dozer_skull´ ElementSpawnEnemyDummy 100003
	Spawn ene_bulldozer_3.
	No group AI
	Orientations:
		(100.0, -125.0, 34.3561)
´dozer_green002´ ElementSpawnEnemyDummy 100011
	Spawn ene_bulldozer_1.
	No group AI
	Orientations:
		(-100.0, -250.0, 32.8519)
´dozer_black002_changed_to_green´ ElementSpawnEnemyDummy 100010
	Spawn ene_bulldozer_1.
	No group AI
	Orientations:
		(-100.0, -250.0, 32.8519)
´dozer_skull002´ ElementSpawnEnemyDummy 100009
	Spawn ene_bulldozer_3.
	No group AI
	Orientations:
		(-100.0, -250.0, 32.8519)
´shield_blue´ ElementSpawnEnemyDummy 100017
	Spawn ene_shield_2.
	No group AI
	Orientations:
		(100.0, -125.0, 34.3561)
´shield_green´ ElementSpawnEnemyDummy 100014
	Spawn ene_shield_1.
	No group AI
	Orientations:
		(100.0, -125.0, 34.3561)
´shield_blue2´ ElementSpawnEnemyDummy 100015
	Spawn ene_shield_2.
	No group AI
	Orientations:
		(0.0, 0.0, 2.5)
´shield_green2´ ElementSpawnEnemyDummy 100012
	Spawn ene_shield_1.
	No group AI
	Orientations:
		(0.0, 0.0, 2.5)
´shield_blue3´ ElementSpawnEnemyDummy 100016
	Spawn ene_shield_2.
	No group AI
	Orientations:
		(-100.0, -250.0, 32.8519)
´shield_green3´ ElementSpawnEnemyDummy 100013
	Spawn ene_shield_1.
	No group AI
	Orientations:
		(-100.0, -250.0, 32.8519)
