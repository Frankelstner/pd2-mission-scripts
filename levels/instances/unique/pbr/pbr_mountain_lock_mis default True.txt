﻿´startup´ ElementDisableUnit 100121
	EXECUTE ON STARTUP
	Hide object: units/dev_tools/level_tools/dev_bag_collision_8x32m/collision_back (50.0, -100.0, 0.0)
´time_to_open´ ElementInstanceInput 100108
	Upon mission event "time_to_open":
		Play audio "alarm_the_bomb_on_slow_fade" at position (-400.0, 350.0, 325.0)
		Execute sequence: "state_light_red_blink" - units/pd2_dlc_berry/props/bry_prop_mountain_blastdoor/001 (11.0, 350.0, 0.0)
		´go_inside_wp´
		Execute sequence: "open_door" - units/pd2_dlc_berry/props/bry_prop_mountain_blastdoor/001 (11.0, 350.0, 0.0) (DELAY 13)
		Debug message: done_opened (DELAY 45)
		Execute sequence: "state_light_off" - units/pd2_dlc_berry/props/bry_prop_mountain_blastdoor/001 (11.0, 350.0, 0.0) (DELAY 60)
´go_inside_wp´ ElementWaypoint 100145
	Place waypoint with icon "pd2_generic_look" at position (-50.0, 350.0, 102.5)
´trigger_area_001´ ElementAreaTrigger 100118
	DISABLED
	TRIGGER TIMES 1
	Box, width 700.0, height 500.0, depth 750.0, at position (550.0, 350.0, 0.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: all
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Show object: units/dev_tools/level_tools/dev_bag_collision_8x32m/collision_back (50.0, -100.0, 0.0)
		Execute sequence: "close_door" - units/pd2_dlc_berry/props/bry_prop_mountain_blastdoor/001 (11.0, 350.0, 0.0)
		Execute sequence: "open_door" - units/pd2_dlc_berry/props/bry_prop_mountain_blastdoor/002 (990.0, 350.0, 0.0)
		remove ´go_inside_wp_2´
		Send instance event "all_inside" to mission.
		Send instance event "door_sealed_behind" to mission.
´go_inside_wp_2´ ElementWaypoint 100126
	Place waypoint with icon "pd2_goto" at position (550.0, 350.0, 100.0)
´trigger_area_002´ ElementAreaTrigger 100130
	DISABLED
	TRIGGER TIMES 1
	Box, width 1000.0, height 500.0, depth 1000.0, at position (-400.0, 350.0, 200.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		remove ´point_waypoint_002´
		Send instance event "reached_vault_output" to mission.
´point_waypoint_002´ ElementWaypoint 100131
	Place waypoint with icon "pd2_goto" at position (-450.0, 350.0, 102.5)
´goal_is_vault´ ElementInstanceInput 100135
	Upon mission event "goal_is_vault":
		Toggle on: ´trigger_area_002´
		´point_waypoint_002´
´keycard_is_goal´ ElementInstanceInput 100137
	Upon mission event "keycard_is_goal":
		´point_waypoint_003´
´point_waypoint_003´ ElementWaypoint 100138
	Place waypoint with icon "pd2_generic_interact" at position (-25.0, 75.0, 155.681)
´hold_to_open´ ElementInteraction 100141
	Create interaction object with ID "hold_open" with timer 3.0at position (-50.0, 75.0, 125.0)
	Upon interaction:
		Execute sequence: "open" - units/pd2_dlc_berry/props/bry_prop_keycard_box/001 (-25.0, 75.0, 125.0)
		Toggle on: ´keycard_interaction´ (DELAY 0.5)
´keycard_interaction´ ElementInteraction 100008
	DISABLED
	Create interaction object with ID "mcm_panicroom_keycard" at position (-50.0, 75.0, 125.0)
	Upon interaction:
		´keycard_used´
´keycard_used´ MissionScriptElement 100113
	TRIGGER TIMES 1
	Send instance event "used_keycard" to mission.
	remove ´go_inside_wp_2´
	remove ´point_waypoint_003´
	Play audio "keypad_correct_code" at position (-50.0, 75.0, 125.0)
	Execute sequence: "int_insert_keycard" - units/pd2_dlc_berry/props/bry_prop_keycard_box/001 (-25.0, 75.0, 125.0)
	Execute sequence: "close" - units/pd2_dlc_berry/props/bry_prop_keycard_box/001 (-25.0, 75.0, 125.0)
	Make teammate close to instigator say: v12
´trigger_area_003´ ElementAreaTrigger 100139
	TRIGGER TIMES 1
	Box, width 200.0, height 500.0, depth 800.0, at position (200.0, 400.0, 100.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		remove ´go_inside_wp´
		Send instance event "entered_vault" to mission.
´goal_is_to_gather´ ElementInstanceInput 100148
	Upon mission event "goal_is_to_gather":
		´go_inside_wp_2´
		Toggle on: ´trigger_area_001´ (DELAY 0.5)
