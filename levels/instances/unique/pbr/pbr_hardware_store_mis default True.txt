﻿´startup´ MissionScriptElement 100084
	EXECUTE ON STARTUP
	Hide object: units/pd2_dlc_jerry/architecture/jry_hardware_store_int_ducts_broken/001 (425.0, 1175.0, -0.000488281)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/001 (350.0, -1050.0, 0.000183105)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/002 (544.454, -1296.97, 0.000183105)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/003 (850.0, -1350.0, 0.000183105)
´destroy_vent´ ElementInstanceInput 100086
	Upon mission event "destroy_vent":
		Hide object: units/pd2_dlc_jerry/architecture/jry_hardware_store_int_ducts_unbroken/001 (425.0, 1175.0, -0.000488281)
		Show object: units/pd2_dlc_jerry/architecture/jry_hardware_store_int_ducts_broken/001 (425.0, 1175.0, -0.000488281)
´door_opened_001´ ElementUnitSequenceTrigger 100142
	TRIGGER TIMES 1
	Upon sequence "done_opened" - units/payday2/equipment/gen_interactable_door_hcm_double/001 (403.0, -954.0, -3.8147e-06):
		Remove nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/001 (350.0, -1050.0, 0.000183105)
´door_opened_002´ ElementUnitSequenceTrigger 100165
	TRIGGER TIMES 1
	Upon sequence "done_opened" - units/payday2/equipment/gen_interactable_door_hcm_double/002 (504.95, -1192.22, 9.53674e-05):
		Remove nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/002 (544.454, -1296.97, 0.000183105)
´door_opened_003´ ElementUnitSequenceTrigger 100166
	TRIGGER TIMES 1
	Upon sequence "done_opened" - units/payday2/equipment/gen_interactable_door_hcm_double/003 (747.0, -1303.0, 9.53674e-05):
		Remove nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x2x3/003 (850.0, -1350.0, 0.000183105)
