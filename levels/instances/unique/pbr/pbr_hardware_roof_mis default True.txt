﻿´hide_destroyed´ ElementDisableUnit 100019
	EXECUTE ON STARTUP
	Hide object: units/pd2_dlc_jerry/architecture/jry_hardware_store_roof_broken/001 (34.0, 39.0, -25.0)
´destroy_roof´ ElementInstanceInput 100018
	BASE DELAY  (DELAY 1)
	Upon mission event "destroy_roof":
		Show object: units/pd2_dlc_jerry/architecture/jry_hardware_store_roof_broken/001 (34.0, 39.0, -25.0)
		Hide object: units/pd2_dlc_jerry/architecture/jry_hardware_store_roof_unbroken/001 (34.0, 39.0, -25.0)
