ID range vs continent name:
	100000: world

statics
	100037 units/dev_tools/level_tools/dev_nav_blocker_2x3m/001 (-75.0, 150.0, 0.0)
	100038 units/dev_tools/level_tools/dev_nav_blocker_2x3m/002 (-75.0, 575.0, 0.0)
	100041 units/dev_tools/level_tools/splitter/dev_nav_doorway_splitter_2x3m/001 (100.0, 150.0, 0.0)
	100042 units/dev_tools/level_tools/splitter/dev_nav_doorway_splitter_2x3m/002 (100.0, 350.0, 0.0)
	100046 units/dev_tools/level_tools/splitter/dev_nav_doorway_splitter_2x3m/004 (-75.0, 350.0, 0.0)
	100047 units/dev_tools/level_tools/splitter/dev_nav_doorway_splitter_2x3m/005 (-75.0, 150.0, 0.0)
	100049 units/pd2_dlc_berry/architecture/interiors/bry_mountain_arch_door/001 (25.0, 350.0, -100.0)
	100001 units/pd2_dlc_berry/architecture/interiors/bry_mountain_solid_wall/001 (46.0, 350.0, -100.0)
		disable_on_ai_graph True
