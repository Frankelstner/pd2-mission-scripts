ID range vs continent name:
	100000: world

statics
	100029 units/dev_tools/level_tools/dev_bag_collision_1x3m/001 (-54.0, -231.0, 480.548)
	100030 units/dev_tools/level_tools/dev_bag_collision_1x3m/002 (-154.0, -231.0, 480.548)
	100031 units/dev_tools/level_tools/dev_bag_collision_1x3m/003 (-154.0, -131.0, 480.548)
	100032 units/dev_tools/level_tools/dev_bag_collision_1x3m/004 (-154.0, -231.0, 780.548)
	100033 units/dev_tools/level_tools/dev_bag_collision_1x3m/005 (-54.0, -231.0, 780.548)
	100038 units/dev_tools/level_tools/dev_bag_collision_1x3m/006 (-79.0002, 194.0, 480.548)
	100039 units/dev_tools/level_tools/dev_bag_collision_1x3m/007 (-4.00022, 194.0, 480.548)
	100036 units/dev_tools/level_tools/dev_bag_collision_1x3m/008 (95.9998, 194.0, 480.548)
	100040 units/dev_tools/level_tools/dev_bag_collision_1x3m/009 (95.9998, 94.0001, 480.548)
	100027 units/dev_tools/level_tools/dev_bag_collision_4x3m/001 (-79.0, -231.0, 480.548)
	100028 units/dev_tools/level_tools/dev_bag_collision_4x3m/002 (245.0, -101.0, 480.548)
	100034 units/dev_tools/level_tools/dev_bag_collision_4x3m/003 (-79.0, -231.0, 780.548)
	100035 units/dev_tools/level_tools/dev_bag_collision_4x3m/004 (245.0, -101.0, 780.548)
	100037 units/dev_tools/level_tools/dev_bag_collision_4x3m/006 (245.0, -0.999985, 480.548)
	100000 units/pd2_dlc_berry/props/bry_prop_exterior_satellitedish_large/001 (0.0, 0.0, 0.0)
		disable_on_ai_graph True
	100002 units/pd2_dlc_jolly/equipment/gen_plant_c4/001 (-130.0, -327.0, 601.859)
		mesh_variation state_vis_hide
	100003 units/pd2_dlc_jolly/equipment/gen_plant_c4/002 (-133.0, -291.0, 545.859)
		mesh_variation state_vis_hide
	100001 units/pd2_indiana/props/mus_prop_electric_box/001 (-121.0, -305.0, 467.548)
		mesh_variation open_door
