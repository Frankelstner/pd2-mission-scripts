﻿´point_spawn_player_001´ ElementPlayerSpawner 100009
	EXECUTE ON STARTUP
	DISABLED
	position 0.0, -375.0, 0.0039978
	rotation 0.0, 0.0, 0.0, -1.0
	state standard
´crate_opened´ ElementUnitSequenceTrigger 100000
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/pd2_dlc1/equipment/gen_interactable_spawn_contraband_crate_2x1x1m/stash_crate (-99.0268, -64.0, -13.9173)
	on_executed
		´enable_interactions´ (delay 0)
´enable_interactions´ ElementUnitSequence 100007
	position 250.0, 50.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_arena/equipment/gen_pku_circle_cutter/001 (-164.211, -65.0, 23.5187)
			notify_unit_sequence state_interaction_enabled
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_red/pickups/gen_pku_keycard_outlined/001 (-88.9556, -64.0, 46.7238)
			notify_unit_sequence enable_interaction
			time 0
	on_executed
		´enable_bomb_int´ (delay 0) DISABLED
´enable_final_bomb_part´ ElementUnitSequence 100011
	position 350.0, 50.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_cro/train_prop_cargo_bomb/ind_mid_bomb/002 (-80.0268, -64.0, -9.9173)
			notify_unit_sequence state_interaction_enabled
			time 0
´point_debug_001´ ElementDebug 100012
	as_subtitle False
	debug_string yah
	show_instigator False
´got_keycard´ ElementGlobalEventTrigger 100013
	TRIGGER TIMES 1
	global_event keycard
	position 350.0, -50.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´enable_final_bomb_part´ (delay 0)
		´point_debug_001´ (delay 0)
		´plus_one_equipment´ (delay 0)
´plus_one_equipment´ ElementInstanceOutput 100005
	event plus_one_equipment
	on_executed
		´got_all_stuff´ (delay 0)
		´point_debug_003´ (delay 0)
´got_bomb´ ElementUnitSequenceTrigger 100014
	sequence_list
		1
			guis_id 1
			sequence load
			unit_id units/pd2_dlc_cro/train_prop_cargo_bomb/ind_lower_bomb/002 (-80.0268, -64.0, -9.9173)
		2
			guis_id 2
			sequence load
			unit_id units/pd2_dlc_cro/train_prop_cargo_bomb/ind_mid_bomb/002 (-80.0268, -64.0, -9.9173)
		3
			guis_id 3
			sequence load
			unit_id units/pd2_dlc_cro/train_prop_cargo_bomb/ind_upmid_bomb/002 (-80.0268, -64.0, -9.9173)
		4
			guis_id 4
			sequence load
			unit_id units/pd2_dlc_cro/train_prop_cargo_bomb/ind_upper_bomb/002 (-80.0268, -64.0, -9.9173)
	on_executed
		´plus_one_equipment´ (delay 0)
		´got_bomb_part´ (delay 0)
		´temp_hide_bomb_parts´ (delay 0)
´got_all_stuff´ ElementCounter 100015
	counter_target 3
	digital_gui_unit_ids
	on_executed
		´logic_operator_001´ (delay 0)
´logic_operator_001´ ElementOperator 100016
	elements
		1 ´point_waypoint_001´
	operation remove
´point_waypoint_001´ ElementWaypoint 100017
	icon pd2_generic_look
	only_in_civilian False
	position -100.0, -63.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´crate_is_goal´ ElementInstanceInput 100018
	event crate_is_goal
	on_executed
		´point_waypoint_001´ (delay 0)
´found_crate´ ElementInstanceOutput 100019
	TRIGGER TIMES 1
	event found_crate
´trigger_area_001´ ElementAreaTrigger 100020
	TRIGGER TIMES 1
	amount 1
	depth 300
	height 650
	instigator player
	interval 0.1
	position -100.0, -138.0, 0.00402832
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 350
	on_executed
		´found_crate´ (delay 0)
´got_circle_cutter´ ElementUnitSequenceTrigger 100021
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence load
			unit_id units/pd2_dlc_arena/equipment/gen_pku_circle_cutter/001 (-164.211, -65.0, 23.5187)
	on_executed
		´plus_one_equipment´ (delay 0)
´point_debug_003´ ElementDebug 100023
	as_subtitle False
	debug_string got one equipment
	show_instigator False
´got_bomb_part´ ElementInstanceOutput 100022
	TRIGGER TIMES 1
	event got_bomb_part
´enable_bomb_int´ ElementUnitSequence 100024
	DISABLED
	position 150.0, 50.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_cro/train_prop_cargo_bomb/ind_lower_bomb/002 (-80.0268, -64.0, -9.9173)
			notify_unit_sequence state_interaction_enabled
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_cro/train_prop_cargo_bomb/ind_upmid_bomb/002 (-80.0268, -64.0, -9.9173)
			notify_unit_sequence state_interaction_enabled
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/pd2_dlc_cro/train_prop_cargo_bomb/ind_upper_bomb/002 (-80.0268, -64.0, -9.9173)
			notify_unit_sequence state_interaction_enabled
			time 0
´temp_hide_bomb_parts´ ElementUnitSequence 100025
	position 150.0, 350.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_cro/train_prop_cargo_bomb/ind_lower_bomb/002 (-80.0268, -64.0, -9.9173)
			notify_unit_sequence state_vis_hide
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_cro/train_prop_cargo_bomb/ind_upmid_bomb/002 (-80.0268, -64.0, -9.9173)
			notify_unit_sequence state_vis_hide
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/pd2_dlc_cro/train_prop_cargo_bomb/ind_upper_bomb/002 (-80.0268, -64.0, -9.9173)
			notify_unit_sequence state_vis_hide
			time 0
