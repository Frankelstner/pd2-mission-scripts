ID range vs continent name:
	100000: world

statics
	100755 units/dev_tools/dev_ladder/001 (-154.0, 252.0, 8300.0)
		disable_shadows True
		height 470
		width 80
	100020 units/dev_tools/level_tools/dev_ai_vis_blocker_005x6x12m/001 (-600.0, 602.0, 1214.01)
		disable_shadows True
	100021 units/dev_tools/level_tools/dev_ai_vis_blocker_005x6x12m/002 (-600.0, 1.99988, 1214.01)
		disable_shadows True
	100647 units/dev_tools/level_tools/dev_bag_collision_4x3m/001 (-175.0, 200.0, 8749.5)
		disable_shadows True
	100748 units/dev_tools/level_tools/dev_bag_collision_4x3m/002 (-175.0, 200.0, 8774.5)
		disable_shadows True
	100749 units/dev_tools/level_tools/dev_bag_collision_4x3m/003 (-175.0, 200.0, 8499.5)
		disable_shadows True
	100750 units/dev_tools/level_tools/dev_bag_collision_4x3m/004 (-175.0, -200.0, 8774.5)
		disable_shadows True
	100751 units/dev_tools/level_tools/dev_bag_collision_4x3m/005 (-175.0, -200.0, 8474.5)
		disable_shadows True
	100752 units/dev_tools/level_tools/dev_bag_collision_4x3m/006 (-175.0, 175.0, 8474.5)
		disable_shadows True
	100753 units/dev_tools/level_tools/dev_bag_collision_4x3m/007 (-175.0, 175.0, 8774.5)
		disable_shadows True
	100325 units/dev_tools/level_tools/dev_bag_collision_4x3m/008 (-175.0, -200.0, 7874.5)
		disable_shadows True
	100329 units/dev_tools/level_tools/dev_bag_collision_4x3m/009 (-175.0, 175.0, 8174.5)
		disable_shadows True
	100333 units/dev_tools/level_tools/dev_bag_collision_4x3m/010 (-175.0, 175.0, 7874.5)
		disable_shadows True
	100337 units/dev_tools/level_tools/dev_bag_collision_4x3m/011 (-175.0, -200.0, 8174.5)
		disable_shadows True
	100341 units/dev_tools/level_tools/dev_bag_collision_4x3m/012 (-175.0, 200.0, 8199.5)
		disable_shadows True
	100069 units/dev_tools/level_tools/dev_bag_collision_4x3m/013 (125.0, 200.0, 8749.5)
		disable_shadows True
	100673 units/payday2/equipment/gen_equipment_zipline_motor/zipline_bag_001 (146.0, -1.0, 331.0)
		disable_shadows True
		mesh_variation state_zipline_enable
		end_pos 147.397, -1.10326, 8913.03
		slack 0
		speed 1000
		usage_type bag
	100671 units/payday2/equipment/gen_equipment_zipline_motor/zipline_player_003 (-27.0, 63.0, 350.0)
		disable_shadows True
		mesh_variation state_zipline_enable
		end_pos -25.9963, 63.3792, 8810.07
		slack 0
		speed 1000
		usage_type person
	100672 units/payday2/equipment/gen_equipment_zipline_motor/zipline_player_004 (-27.0, -63.0, 350.0)
		disable_shadows True
		mesh_variation state_zipline_enable
		end_pos -25.9918, -63.509, 8828.08
		slack 0
		speed 1000
		usage_type person
	100006 units/payday2/props/gen_prop_square_goal_marker_27x24/marker (-22.0, 0.0, 362.0)
		disable_shadows True
		mesh_variation hide
	100002 units/pd2_dlc2/csgo_models/props_equipment/metalladder002/001 (-153.0, 215.0, 8111.74)
		disable_shadows True
	100008 units/pd2_dlc2/csgo_models/props_equipment/metalladder002/002 (-153.0, 215.0, 8436.74)
		disable_shadows True
	100004 units/pd2_dlc_berry/architecture/beams/bry_mountain_elevator_cage/001 (100.0, 150.0, 350.0)
		disable_shadows True
	100076 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/001 (-225.0, 100.0, 0.0)
		disable_shadows True
	100030 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/002 (-225.0, 100.0, 800.0)
		disable_shadows True
	100035 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/003 (-225.0, 100.0, 1200.0)
		disable_shadows True
	100077 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/004 (-225.0, 100.0, 1600.0)
		disable_shadows True
	100078 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/005 (-225.0, 100.0, 2000.0)
		disable_shadows True
	100079 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/006 (-225.0, 100.0, 2400.0)
		disable_shadows True
	100080 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/007 (-225.0, 100.0, 2800.0)
		disable_shadows True
	100081 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/008 (-225.0, 100.0, 3200.0)
		disable_shadows True
	100082 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/009 (-225.0, 100.0, 3600.0)
		disable_shadows True
	100083 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/010 (-225.0, 100.0, 4000.0)
		disable_shadows True
	100084 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/011 (-225.0, 100.0, 4400.0)
		disable_shadows True
	100085 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/012 (-225.0, 100.0, 4800.0)
		disable_shadows True
	100086 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/013 (-225.0, 100.0, 5200.0)
		disable_shadows True
	100087 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/014 (-225.0, 100.0, 5600.0)
		disable_shadows True
	100088 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/015 (-225.0, 100.0, 6000.0)
		disable_shadows True
	100089 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/016 (-225.0, 100.0, 6400.0)
		disable_shadows True
	100090 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/017 (-225.0, 100.0, 6800.0)
		disable_shadows True
	100091 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/018 (-225.0, 100.0, 7200.0)
		disable_shadows True
	100092 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/019 (-225.0, 100.0, 7600.0)
		disable_shadows True
	100093 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/020 (-225.0, 100.0, 8000.0)
		disable_shadows True
	100095 units/pd2_dlc_berry/architecture/bry_mountain_elevator_beams/022 (-225.0, 100.0, 400.0)
		disable_shadows True
	100299 units/pd2_dlc_berry/architecture/interiors/bry_mountain_elevator_shaft/001 (-1100.0, -12700.0, 900.0)
		disable_shadows True
	100061 units/pd2_dlc_berry/architecture/tunnel_railings/bry_mountain_stairs_b/001 (425.0, -150.0, 200.0)
		disable_shadows True
	100023 units/pd2_dlc_berry/architecture/tunnel_railings/bry_mountain_stairs_b_ending_leftbend/001 (-75.0, -450.0, 0.0)
	100067 units/pd2_dlc_berry/architecture/tunnel_railings/bry_mountain_stairs_c_half/002 (425.0, -250.0, 200.0)
		disable_shadows True
	100024 units/pd2_dlc_berry/architecture/tunnel_railings/bry_mountain_stairs_c_half_cupped/001 (425.0, 150.0, 400.0)
	100068 units/pd2_dlc_berry/architecture/tunnel_railings/bry_mountain_stairs_c_one_side/001 (225.0, 450.0, 400.0)
		disable_shadows True
	100064 units/pd2_dlc_berry/architecture/tunnel_railings/bry_mountain_stairs_c_outer_corner/002 (425.0, 250.0, 400.0)
		disable_shadows True
	100065 units/pd2_dlc_berry/architecture/tunnel_railings/bry_mountain_stairs_c_outer_corner/003 (225.0, -450.0, 200.0)
		disable_shadows True
	100025 units/pd2_dlc_berry/architecture/tunnel_railings/bry_mountain_stairs_c_outer_corner_cupped/001 (24.9998, 450.0, 400.0)
	100010 units/pd2_dlc_berry/characters/ene_murkywater_pose_dead_1/001 (-934.0, 405.0, 8299.74)
		disable_shadows True
	100011 units/pd2_dlc_berry/characters/ene_murkywater_pose_dead_2/001 (-641.323, 584.81, 8299.74)
		disable_shadows True
	100012 units/pd2_dlc_berry/characters/ene_murkywater_pose_dead_3/001 (-300.0, 44.0004, 8300.0)
		disable_shadows True
	100019 units/pd2_dlc_berry/props/editable_text_euro_stencil/001 (-1827.0, 500.0, 323.0)
		disable_shadows True
		align center
		alpha 1
		blend_mode normal
		font fonts/font_euro_stencil
		font_color 1.0, 1.0, 1.0
		font_size 1.5
		render_template diffuse_vc_decal
		shape
			1 0
			2 0
			3 1
			4 1
		text SURFACE ELEVATOR
		vertical center
		word_wrap False
		wrap False
	100759 units/world/props/mansion/man_cover_int_shipping_crate_wide/001 (-300.0, 127.0, 8300.0)
		disable_shadows True
	100760 units/world/props/mansion/man_cover_int_shipping_crate_wide/002 (-300.0, 219.0, 8300.0)
		disable_shadows True
	100765 units/world/props/mansion/man_cover_int_shipping_crate_wide/003 (-300.0, 184.0, 8414.0)
		disable_shadows True
