ID range vs continent name:
	100000: world

statics
	100090 units/dev_tools/dev_ladder/001 (-300.0, -1705.0, -25.0)
		disable_shadows True
		height 510
		width 80
	100004 units/payday2/equipment/gen_interactable_door_reinforced_lab/001 (379.0, -1276.0, -23.0)
		disable_on_ai_graph True
		mesh_variation state_door_open
	100005 units/payday2/equipment/gen_interactable_door_reinforced_lab/002 (78.0, -1340.0, -25.0)
		disable_on_ai_graph True
		mesh_variation state_door_open
	100006 units/payday2/equipment/gen_interactable_door_reinforced_lab/003 (-454.0, -1529.0, -25.0)
		disable_on_ai_graph True
		mesh_variation state_door_open
	100007 units/payday2/equipment/gen_interactable_door_reinforced_lab/004 (469.0, -1671.0, -25.0)
		disable_on_ai_graph True
		mesh_variation state_door_open
	100088 units/payday2/props/a/ind_prop_rooftop_ladder_b_mid/001 (-300.0, -1675.0, -243.0)
		disable_shadows True
	100089 units/payday2/props/a/ind_prop_rooftop_ladder_b_top/001 (-300.0, -1675.0, 157.0)
		disable_on_ai_graph True
	100024 units/payday2/props/air_prop_hangar_toolbox/001 (-1196.0, 699.0, 0.0)
		disable_shadows True
	100038 units/payday2/props/air_prop_hangar_toolbox/002 (-1496.0, 192.0, 0.0)
		disable_shadows True
	100040 units/payday2/props/air_prop_hangar_toolbox/003 (-1543.0, 238.0, 0.0)
		disable_shadows True
	100025 units/payday2/props/air_prop_hangar_toolbox_addon/001 (-1196.0, 699.0, 86.0)
		disable_shadows True
	100039 units/payday2/props/air_prop_hangar_toolbox_addon/002 (-1496.0, 192.0, 86.0)
		disable_shadows True
	100041 units/payday2/props/air_prop_hangar_toolbox_addon/003 (-1199.0, 222.0, 87.0)
		disable_shadows True
	100009 units/payday2/props/are_prop_bathroom_handsanitizer/001 (-376.0, -1619.0, 87.0)
		disable_shadows True
	100008 units/payday2/props/are_prop_bathroom_sink/001 (-322.0, -1607.0, 52.0)
		disable_shadows True
	100216 units/payday2/props/com_prop_carcleaning_spray_a/001 (-1526.0, 231.0, 86.0113)
		disable_shadows True
	100207 units/payday2/props/com_prop_carcleaning_spray_grp_a/001 (-56.0, -964.0, 46.5574)
		disable_shadows True
	100157 units/payday2/props/com_prop_store_cashbox/001 (452.0, -790.0, 63.4599)
		disable_shadows True
	100159 units/payday2/props/com_prop_store_cashbox/003 (534.192, -1037.48, 64.0)
		disable_shadows True
	100026 units/payday2/props/com_prop_store_selfservice_counter_drink/001 (544.0, -1268.0, -23.0)
	100017 units/payday2/props/com_prop_store_selfservice_counter_straight/001 (644.0, -1268.0, -23.0)
	100015 units/payday2/props/com_prop_store_selfservice_counter_trash/001 (444.0, -1268.0, -23.0)
	100031 units/payday2/props/com_prop_store_selfservice_counter_trash/002 (-158.0, -1268.0, -23.0)
	100123 units/payday2/props/com_prop_store_shelf_cigarettes/001 (673.0, -870.0, 98.0)
		disable_shadows True
	100027 units/payday2/props/com_prop_store_sodadispenser/001 (592.0, -1247.0, 77.0)
	100149 units/payday2/props/com_prop_store_teller_monitor/001 (455.0, -769.0, 71.4599)
		disable_shadows True
	100155 units/payday2/props/com_prop_store_teller_monitor/002 (522.172, -1022.63, 72.0)
		disable_shadows True
	100182 units/payday2/props/gen_prop_long_lamp_v2/002 (-1584.0, 150.0, 277.575)
		disable_shadows True
	100032 units/payday2/props/gen_prop_long_lamp_v2/003 (-1584.0, -150.0, 277.575)
		disable_shadows True
	100033 units/payday2/props/gen_prop_long_lamp_v2/004 (-1584.0, 650.0, 277.575)
		disable_shadows True
	100184 units/payday2/props/gen_prop_long_lamp_v2/005 (225.0, -1468.0, 320.0)
		disable_shadows True
	100222 units/payday2/props/mcm_prop_kitchen_wastebin/001 (409.0, -1315.0, -23.0)
		disable_shadows True
	100224 units/payday2/props/mcm_prop_kitchen_wastebin/002 (658.0, -1518.0, -23.0)
		disable_shadows True
	100110 units/payday2/props/mechnical_ac_wall_a/str_prop_mechanical_ac_wall_a/001 (-476.0, -1150.0, 311.616)
	100158 units/payday2/props/mechnical_ac_wall_a/str_prop_mechanical_ac_wall_a/002 (-476.0, -1275.16, 308.62)
	100154 units/payday2/props/monitor/com_prop_store_teller_reader/001 (436.0, -821.0, 75.4334)
		disable_shadows True
	100156 units/payday2/props/monitor/com_prop_store_teller_reader/002 (494.995, -1030.18, 75.0)
		disable_shadows True
	100013 units/payday2/props/off_prop_generic_filecabinet_d/002 (674.0, -1595.0, -25.0)
		disable_shadows True
	100166 units/payday2/props/res_prop_kitchen_chair_a_worn_clean/001 (193.227, -1510.75, -25.0)
		disable_shadows True
	100172 units/payday2/props/res_prop_kitchen_chair_a_worn_clean/002 (261.813, -1557.22, -25.0)
		disable_shadows True
	100173 units/payday2/props/res_prop_kitchen_chair_a_worn_clean/003 (318.719, -1520.69, -25.0)
		disable_shadows True
	100174 units/payday2/props/res_prop_kitchen_chair_a_worn_clean/004 (610.56, -1427.37, -25.0)
		disable_shadows True
	100176 units/payday2/props/res_prop_kitchen_fridge_worn_clean/001 (451.0, -1648.0, -25.0)
		disable_shadows True
	100190 units/payday2/props/res_prop_kitchen_table_a_long_worn_clean/002 (183.0, -1559.0, -25.0)
		disable_shadows True
	100209 units/payday2/props/spray/com_prop_carcleaning_motoroil_a/001 (-1362.02, -220.928, 0.000296899)
		disable_shadows True
	100210 units/payday2/props/spray/com_prop_carcleaning_motoroil_b/001 (-1197.0, 164.0, 0.502479)
		disable_shadows True
	100211 units/payday2/props/spray/com_prop_carcleaning_motoroil_c/001 (-1856.0, 728.0, 87.0)
		disable_shadows True
	100213 units/payday2/props/spray/com_prop_carcleaning_motoroil_c/002 (-1838.0, 728.0, 87.0)
		disable_shadows True
	100214 units/payday2/props/spray/com_prop_carcleaning_motoroil_d/001 (-1467.0, 231.0, 0.0112534)
		disable_shadows True
	100215 units/payday2/props/spray/com_prop_carcleaning_motoroil_e/001 (-1483.0, 243.0, 0.0112534)
		disable_shadows True
	100212 units/payday2/props/spray/com_prop_carcleaning_motoroil_grp_a/001 (5.0, -951.0, -5.0)
		disable_shadows True
	100204 units/payday2/props/spray/com_prop_carcleaning_motoroil_grp_b/001 (37.0, -963.0, 47.0)
		disable_shadows True
	100208 units/payday2/props/spray/com_prop_carcleaning_motoroil_grp_c/001 (-136.0, -962.0, 101.088)
		disable_shadows True
	100206 units/payday2/props/spray/com_prop_carcleaning_motoroil_grp_d/001 (51.0, -968.0, 101.28)
		disable_shadows True
	100064 units/payday2/props/spray/com_prop_carcleaning_motoroil_grp_e/001 (-42.0, -968.0, 101.28)
		disable_shadows True
	100164 units/payday2/props/stn_prop_office_deskset_straight_a/001 (582.0, -1312.0, -23.0)
		disable_shadows True
	100165 units/payday2/props/stn_prop_office_drinkingfountain_small/001 (-451.0, -1384.0, -5.60891)
		disable_shadows True
	100223 units/payday2/props/stn_prop_office_drinkingfountain_small/002 (-450.0, -1340.0, -21.6089)
		disable_shadows True
	100231 units/payday2/props/str_prop_rooftop_ac_unit_a_twin/001 (375.0, -1375.0, 455.0)
		disable_on_ai_graph True
	100119 units/payday2/vehicles/str_vehicle_truck_boxvan/001 (-740.0, -1224.0, -25.0)
	100003 units/pd2_dlc1/architecture/ext_gas_station/res_int_gas_building/001 (-476.0, -674.0, -25.0)
		disable_shadows True
	100044 units/pd2_dlc1/architecture/ext_gas_station/res_prop_gas_barrier_a/001 (545.0, 526.0, -25.0)
	100045 units/pd2_dlc1/architecture/ext_gas_station/res_prop_gas_barrier_a/002 (-282.0, 516.0, -25.0)
	100046 units/pd2_dlc1/architecture/ext_gas_station/res_prop_gas_barrier_a/003 (-282.0, -28.0001, -25.0)
	100047 units/pd2_dlc1/architecture/ext_gas_station/res_prop_gas_barrier_a/004 (367.0, -607.0, -25.0)
	100043 units/pd2_dlc1/architecture/ext_gas_station/res_prop_gas_barrier_b/001 (541.0, -26.0, -25.0)
	100049 units/pd2_dlc1/architecture/ext_gas_station/res_prop_gas_barrier_b/002 (-231.0, -604.0, -25.0)
	100091 units/pd2_dlc1/architecture/ext_gas_station/res_prop_gas_pump/001 (25.0, -25.0, -25.0)
		disable_shadows True
	100092 units/pd2_dlc1/architecture/ext_gas_station/res_prop_gas_pump/002 (275.0, -25.0, -25.0)
		disable_shadows True
	100093 units/pd2_dlc1/architecture/ext_gas_station/res_prop_gas_pump/003 (25.0, 525.0, -25.0)
		disable_shadows True
	100094 units/pd2_dlc1/architecture/ext_gas_station/res_prop_gas_pump/004 (275.0, 525.0, -25.0)
		disable_shadows True
	100085 units/pd2_dlc1/architecture/ext_gas_station/res_prop_gas_window_01/001 (619.0, -672.0, 50.0)
		disable_shadows True
	100151 units/pd2_dlc1/architecture/ext_gas_station/res_prop_gas_window_01/002 (419.0, -672.0, 50.0)
		disable_shadows True
	100152 units/pd2_dlc1/architecture/ext_gas_station/res_prop_gas_window_01/003 (-131.0, -672.0, 50.0)
		disable_shadows True
	100153 units/pd2_dlc1/architecture/ext_gas_station/res_prop_gas_window_01/004 (-231.0, -672.0, 50.0)
		disable_shadows True
	100000 units/pd2_dlc1/architecture/res_ext_gas_station/001 (150.0, 225.0, -50.0)
	100002 units/pd2_dlc1/architecture/station/res_ext_gas_building/001 (-476.0, -674.0, -25.0)
		mesh_variation front_door_state_open
	100001 units/pd2_dlc1/props/res_ext_gas_station_mascot/001 (-150.0, 500.0, 558.0)
	100018 units/pd2_dlc1/props/res_prop_bathroom_stall/001 (24.0, -1587.0, -25.0)
		disable_shadows True
	100148 units/pd2_dlc1/props/res_prop_store_beer/001 (-1112.85, 171.528, -23.8681)
		disable_shadows True
	100150 units/pd2_dlc1/props/res_prop_store_beer/002 (319.0, -1508.0, 51.348)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_c
	100218 units/pd2_dlc1/props/res_prop_store_beer/003 (214.0, -1528.0, 51.348)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_c
	100219 units/pd2_dlc1/props/res_prop_store_beer/004 (369.0, -1617.0, -23.0)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_c
	100101 units/pd2_dlc1/props/res_prop_store_beer/a_001 (-1102.85, 189.741, -24.9787)
		disable_shadows True
	100102 units/pd2_dlc1/props/res_prop_store_beer/a_002 (-1094.61, 164.704, -19.9787)
		disable_shadows True
	100060 units/pd2_dlc1/props/res_prop_store_beer_carton_a/001 (108.0, -1034.0, -5.00003)
		disable_shadows True
	100081 units/pd2_dlc1/props/res_prop_store_beer_carton_a/003 (109.0, -975.0, -5.00003)
		disable_shadows True
	100082 units/pd2_dlc1/props/res_prop_store_beer_carton_a/004 (146.043, -996.656, 20.0)
		disable_shadows True
	100061 units/pd2_dlc1/props/res_prop_store_beer_carton_a/005 (114.0, -1004.0, -5.00003)
		disable_shadows True
	100084 units/pd2_dlc1/props/res_prop_store_beer_carton_a/006 (149.0, -967.0, 19.0)
		disable_shadows True
	100058 units/pd2_dlc1/props/res_prop_store_beer_cutout/001 (104.43, -1028.0, 41.3293)
		disable_shadows True
	100103 units/pd2_dlc1/props/res_prop_store_beer_six/a_001 (-376.0, -1121.0, 49.7465)
		disable_shadows True
	100104 units/pd2_dlc1/props/res_prop_store_beer_six/a_002 (-376.0, -1121.0, 84.7465)
		disable_shadows True
	100105 units/pd2_dlc1/props/res_prop_store_beer_six/a_003 (-372.0, -1143.0, 49.7465)
		disable_shadows True
	100106 units/pd2_dlc1/props/res_prop_store_beer_six/a_004 (-380.746, -1161.2, 84.7465)
		disable_shadows True
	100107 units/pd2_dlc1/props/res_prop_store_beer_six/a_005 (-379.0, -1165.0, 49.7465)
		disable_shadows True
	100109 units/pd2_dlc1/props/res_prop_store_beer_six/a_006 (-375.0, -1185.0, 49.7465)
		disable_shadows True
	100111 units/pd2_dlc1/props/res_prop_store_beer_six/a_007 (-375.0, -1185.0, 84.7465)
		disable_shadows True
	100134 units/pd2_dlc1/props/res_prop_store_beer_six/a_008 (-376.0, -1121.0, 119.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_b
	100115 units/pd2_dlc1/props/res_prop_store_beer_six/a_009 (-374.0, -1138.0, 154.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_b
	100113 units/pd2_dlc1/props/res_prop_store_beer_six/a_010 (-379.0, -1142.0, 119.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_b
	100116 units/pd2_dlc1/props/res_prop_store_beer_six/a_011 (-375.0, -1160.0, 119.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_b
	100108 units/pd2_dlc1/props/res_prop_store_beer_six/a_012 (-381.0, -1165.0, 154.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_b
	100117 units/pd2_dlc1/props/res_prop_store_beer_six/a_013 (-377.0, -1185.0, 154.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_b
	100120 units/pd2_dlc1/props/res_prop_store_beer_six/a_014 (-380.0, -1182.0, 119.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_b
	100121 units/pd2_dlc1/props/res_prop_store_beer_six/a_015 (-375.0, -1085.0, 119.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_c
	100135 units/pd2_dlc1/props/res_prop_store_beer_six/a_016 (-379.0, -1065.0, 119.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_c
	100136 units/pd2_dlc1/props/res_prop_store_beer_six/a_017 (-372.0, -1043.0, 119.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_c
	100137 units/pd2_dlc1/props/res_prop_store_beer_six/a_018 (-376.0, -1021.0, 85.7465)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_d
	100138 units/pd2_dlc1/props/res_prop_store_beer_six/a_019 (-376.0, -1021.0, 154.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_c
	100139 units/pd2_dlc1/props/res_prop_store_beer_six/a_020 (-380.746, -1061.2, 154.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_c
	100141 units/pd2_dlc1/props/res_prop_store_beer_six/a_021 (-375.0, -1085.0, 154.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_c
	100140 units/pd2_dlc1/props/res_prop_store_beer_six/a_022 (-376.503, -1020.12, 50.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_d
	100142 units/pd2_dlc1/props/res_prop_store_beer_six/a_023 (-379.503, -1041.12, 50.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_d
	100143 units/pd2_dlc1/props/res_prop_store_beer_six/a_024 (-375.503, -1059.12, 50.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_d
	100144 units/pd2_dlc1/props/res_prop_store_beer_six/a_025 (-380.503, -1081.12, 50.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_d
	100145 units/pd2_dlc1/props/res_prop_store_beer_six/a_026 (-377.503, -1084.12, 85.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_d
	100146 units/pd2_dlc1/props/res_prop_store_beer_six/a_027 (-381.503, -1064.12, 85.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_d
	100147 units/pd2_dlc1/props/res_prop_store_beer_six/a_028 (-376.698, -1043.14, 85.746)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store_beer/res_prop_store_beer_six_d
	100066 units/pd2_dlc1/props/res_prop_store_candy_a/001 (-197.0, -1027.0, 46.0)
		disable_shadows True
	100178 units/pd2_dlc1/props/res_prop_store_candy_a/002 (-42.0, -1221.89, 20.8722)
		disable_shadows True
	100067 units/pd2_dlc1/props/res_prop_store_candy_b/001 (-60.9999, -998.0, 101.0)
		disable_shadows True
	100070 units/pd2_dlc1/props/res_prop_store_candy_b/002 (-29.9999, -998.0, 101.0)
		disable_shadows True
	100196 units/pd2_dlc1/props/res_prop_store_candy_b/003 (45.0, -1228.42, 46.2165)
		disable_shadows True
	100068 units/pd2_dlc1/props/res_prop_store_candy_c/001 (-17.9999, -1027.0, 47.0)
		disable_shadows True
	100185 units/pd2_dlc1/props/res_prop_store_candy_c/002 (-90.0, -1228.42, 46.2165)
		disable_shadows True
	100186 units/pd2_dlc1/props/res_prop_store_candy_c/003 (16.0, -1221.89, 20.8722)
		disable_shadows True
	100069 units/pd2_dlc1/props/res_prop_store_candy_d/001 (-195.0, -1027.0, 101.0)
		disable_shadows True
	100187 units/pd2_dlc1/props/res_prop_store_candy_d/002 (14.0, -1228.42, 46.2165)
		disable_shadows True
	100071 units/pd2_dlc1/props/res_prop_store_candy_e/001 (95.0001, -995.0, 47.0)
		disable_shadows True
	100072 units/pd2_dlc1/props/res_prop_store_candy_f/001 (-61.9999, -993.0, 47.0)
		disable_shadows True
	100083 units/pd2_dlc1/props/res_prop_store_candy_f/002 (-77.9999, -993.0, 47.0)
		disable_shadows True
	100118 units/pd2_dlc1/props/res_prop_store_candy_f/003 (-61.9999, -993.0, 56.0)
		disable_shadows True
	100168 units/pd2_dlc1/props/res_prop_store_candy_f/004 (-77.9999, -993.0, 56.0)
		disable_shadows True
	100188 units/pd2_dlc1/props/res_prop_store_candy_f/005 (31.0, -1221.89, 20.8722)
		disable_shadows True
	100189 units/pd2_dlc1/props/res_prop_store_candy_f/006 (46.0, -1221.89, 20.8722)
		disable_shadows True
	100073 units/pd2_dlc1/props/res_prop_store_chips_a/001 (-173.0, -1005.0, -3.99999)
		disable_shadows True
	100075 units/pd2_dlc1/props/res_prop_store_chips_a/002 (-170.0, -1000.0, -3.99999)
		disable_shadows True
	100074 units/pd2_dlc1/props/res_prop_store_chips_b/001 (-113.0, -1005.0, -4.99999)
		disable_shadows True
	100198 units/pd2_dlc1/props/res_prop_store_chipsbag_a/001 (-37.7296, -1018.58, 3.99997)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store/res_prop_store_chipsbag_b
	100200 units/pd2_dlc1/props/res_prop_store_chipsbag_a/002 (-55.7743, -1055.3, -19.8561)
		disable_shadows True
	100201 units/pd2_dlc1/props/res_prop_store_chipsbag_a/003 (59.0, -1242.78, 57.6125)
		disable_shadows True
	100202 units/pd2_dlc1/props/res_prop_store_chipsbag_a/004 (85.0, -1242.78, 57.6125)
		disable_shadows True
	100203 units/pd2_dlc1/props/res_prop_store_chipsbag_a/005 (110.0, -1242.78, 57.6125)
		disable_shadows True
	100226 units/pd2_dlc1/props/res_prop_store_chipsbag_a/006 (135.0, -1242.78, 57.6125)
		disable_shadows True
	100227 units/pd2_dlc1/props/res_prop_store_chipsbag_a/007 (48.5334, -1024.31, 0.796205)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store/res_prop_store_chipsbag_b
	100228 units/pd2_dlc1/props/res_prop_store_chipsbag_a/008 (135.0, -1250.67, 85.2706)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store/res_prop_store_chipsbag_b
	100229 units/pd2_dlc1/props/res_prop_store_chipsbag_a/009 (98.2406, -1244.8, 82.0261)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store/res_prop_store_chipsbag_b
	100230 units/pd2_dlc1/props/res_prop_store_chipsbag_a/010 (59.0, -1250.67, 85.2706)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store/res_prop_store_chipsbag_b
	100199 units/pd2_dlc1/props/res_prop_store_chipsbags_a/001 (-51.0, -1018.0, -2.00003)
		disable_shadows True
	100225 units/pd2_dlc1/props/res_prop_store_chipsbags_a/002 (25.132, -1022.76, -2.00003)
		disable_shadows True
		material_variation units/pd2_dlc1/props/res_prop_store/res_prop_store_chipsbag_b
	100065 units/pd2_dlc1/props/res_prop_store_counter/001 (425.0, -725.0, -25.0)
		disable_shadows True
	100019 units/pd2_dlc1/props/res_prop_store_fridge/001 (-350.0, -900.0, 0.0)
		disable_shadows True
	100020 units/pd2_dlc1/props/res_prop_store_fridge/002 (-350.0, -1000.0, 0.0)
		disable_shadows True
	100021 units/pd2_dlc1/props/res_prop_store_fridge/003 (-350.0, -1100.0, 0.0)
		disable_shadows True
	100022 units/pd2_dlc1/props/res_prop_store_fridge/004 (-350.0, -1200.0, 0.0)
		disable_shadows True
	100171 units/pd2_dlc1/props/res_prop_store_milk/001 (-371.845, -820.708, 50.0)
		disable_shadows True
	100191 units/pd2_dlc1/props/res_prop_store_milk/002 (-372.794, -840.257, 50.0)
		disable_shadows True
	100192 units/pd2_dlc1/props/res_prop_store_milk/003 (-392.779, -821.429, 50.0)
		disable_shadows True
	100193 units/pd2_dlc1/props/res_prop_store_milk/004 (-393.794, -841.257, 50.0)
		disable_shadows True
	100194 units/pd2_dlc1/props/res_prop_store_milk/005 (-393.221, -882.979, 50.0)
		disable_shadows True
	100195 units/pd2_dlc1/props/res_prop_store_milk/006 (-389.793, -863.257, 50.0)
		disable_shadows True
	100197 units/pd2_dlc1/props/res_prop_store_milk/008 (-373.361, -883.304, 50.0)
		disable_shadows True
	100078 units/pd2_dlc1/props/res_prop_store_rack_end/003 (-50.0, -1273.0, -25.0)
		disable_shadows True
	100076 units/pd2_dlc1/props/res_prop_store_rack_mid/001 (50.0, -1273.0, -25.0)
		disable_shadows True
	100079 units/pd2_dlc1/props/res_prop_store_rack_start/001 (150.0, -1273.0, -25.0)
		disable_shadows True
	100077 units/pd2_dlc1/props/res_prop_store_shelf_a/001 (150.0, -1273.0, -25.0)
		disable_shadows True
	100059 units/pd2_dlc1/props/res_prop_store_shelf_a/002 (100.0, -1044.0, -25.0)
		disable_shadows True
	100057 units/pd2_dlc1/props/res_prop_store_shelf_a/003 (50.0, -1273.0, -25.0)
		disable_shadows True
	100062 units/pd2_dlc1/props/res_prop_store_shelf_a/004 (-49.9999, -1273.0, -25.0)
		disable_shadows True
	100080 units/pd2_dlc1/props/res_prop_store_shelf_b/003 (-99.9999, -988.0, -25.0)
		disable_shadows True
	100037 units/pd2_dlc1/props/res_prop_store_shelf_c/001 (6.10352e-05, -988.0, -25.0)
		disable_shadows True
	100056 units/pd2_dlc1/props/res_prop_store_shelf_c/002 (-99.9999, -988.0, -25.0)
		disable_shadows True
	100063 units/pd2_dlc1/props/res_prop_store_shelf_c/003 (-200.0, -988.0, -25.0)
		disable_shadows True
	100205 units/pd2_dlc1/props/res_prop_store_shelf_c/004 (100.0, -988.0, -25.0)
		disable_shadows True
	100042 units/pd2_dlc1/props/res_prop_store_shelf_c/005 (0.0, -988.0, -25.0)
		disable_shadows True
	100096 units/pd2_dlc1/props/res_prop_store_soda_a/001 (-382.0, -1028.0, 0.0)
		disable_shadows True
	100097 units/pd2_dlc1/props/res_prop_store_soda_a/002 (-384.0, -1186.0, 0.0)
		disable_shadows True
	100098 units/pd2_dlc1/props/res_prop_store_soda_b/001 (-399.4, -914.502, 8.26627)
		disable_shadows True
	100086 units/pd2_dlc1/props/res_prop_store_soda_group_a/001 (-380.0, -1142.0, 0.0)
		disable_shadows True
	100087 units/pd2_dlc1/props/res_prop_store_soda_group_a/002 (-380.0, -1067.0, 0.0)
		disable_shadows True
	100095 units/pd2_dlc1/props/res_prop_store_soda_group_b/001 (-380.0, -967.0, 0.0)
		disable_shadows True
	100010 units/pd2_dlc2/csgo_models/props_interiors/water_cooler/001 (455.0, -1322.0, -23.0)
		disable_shadows True
	100023 units/pd2_dlc_jerry/architecture/jry_service_garage/001 (-1500.0, 300.0, 0.0)
	100035 units/pd2_dlc_jerry/props/jry_prop_garage_cartirechanger/001 (-1839.0, 286.0, 0.0)
	100028 units/pd2_dlc_jerry/props/jry_prop_garage_workbench/001 (-1225.0, 250.0, 0.0)
		disable_shadows True
	100029 units/pd2_dlc_jerry/props/jry_prop_garage_workbench/002 (-1789.0, 704.0, 0.0)
		disable_shadows True
	100016 units/pd2_dlc_jerry/props/jry_prop_gasstation_newspaperstand/001 (-308.0, -767.0, -23.0)
	100012 units/pd2_dlc_jerry/props/jry_prop_gasstation_newspaperstand/002 (384.0, -766.0, -23.0)
	100217 units/pd2_dlc_jerry/props/jry_prop_pokergame/001 (261.0, -1509.0, 51.348)
		disable_shadows True
	100100 units/pd2_dlc_shoutout_raid/props/gen_prop_tire_stack_5/001 (-1300.0, -204.0, 0.0)
		disable_shadows True
	100036 units/pd2_dlc_shoutout_raid/props/gen_prop_tire_stack_5/002 (-1828.0, 175.0, 0.0)
	100099 units/pd2_dlc_shoutout_raid/props/gen_prop_tire_stack_8/001 (-1219.0, -184.0, 0.0)
		disable_shadows True
	100014 units/pd2_dlc_shoutout_raid/props/gen_prop_tire_stack_8/002 (-1827.0, 403.0, 0.0)
	100124 units/pd2_mcmansion/props/book_set/mcm_prop_livingroom_magazine_a/001 (439.723, -851.0, 17.914)
		disable_shadows True
	100126 units/pd2_mcmansion/props/book_set/mcm_prop_livingroom_magazine_a/002 (438.147, -856.0, 19.1453)
		disable_shadows True
	100127 units/pd2_mcmansion/props/book_set/mcm_prop_livingroom_magazine_b/001 (439.723, -886.0, 17.914)
		disable_shadows True
	100128 units/pd2_mcmansion/props/book_set/mcm_prop_livingroom_magazine_c/001 (439.723, -916.0, 17.914)
		disable_shadows True
	100133 units/pd2_mcmansion/props/book_set/mcm_prop_livingroom_magazine_c/002 (438.935, -910.0, 18.5297)
		disable_shadows True
	100129 units/pd2_mcmansion/props/book_set/mcm_prop_livingroom_magazine_d/001 (439.723, -946.0, 17.914)
		disable_shadows True
	100130 units/pd2_mcmansion/props/book_set/mcm_prop_livingroom_magazine_e/001 (453.851, -979.114, 17.7417)
		disable_shadows True
	100131 units/pd2_mcmansion/props/book_set/mcm_prop_livingroom_magazine_f/001 (464.758, -992.249, 18.973)
		disable_shadows True
	100132 units/pd2_mcmansion/props/book_set/mcm_prop_livingroom_magazine_g/001 (483.55, -1008.81, 17.7417)
		disable_shadows True
	100125 units/pd2_mcmansion/props/mcm_prop_garage_door_closed/001 (-1375.0, -0.000335693, 0.000200272)
		disable_shadows True
	100122 units/pd2_mcmansion/props/mcm_prop_garage_door_open/001 (-1375.0, 500.0, 0.000600815)
		disable_shadows True
	100163 units/world/architecture/hospital/props/clock/001 (674.0, -1359.0, 198.773)
		disable_shadows True
	100034 units/world/architecture/hospital/props/clock/002 (674.0, -1136.0, 198.773)
	100112 units/world/architecture/hospital/props/rooftop/acunit/acunit02/001 (-300.0, -1150.0, 450.0)
	100169 units/world/architecture/hospital/props/rooftop/acunit/acunit02/002 (-1675.0, 349.999, 365.0)
	100175 units/world/props/apartment/lightswitch_01/001 (392.0, -1301.0, 86.0)
		disable_shadows True
	100181 units/world/props/apartment/lightswitch_01/002 (-1176.0, 319.0, 135.354)
		disable_shadows True
	100183 units/world/props/apartment/lightswitch_01/003 (-1873.0, -74.0, 109.444)
		disable_shadows True
	100030 units/world/props/apartment/lightswitch_01/004 (392.0, -1273.0, 126.0)
	100011 units/world/props/suburbia_bathroom/bathroom_mirror_suburbia/001 (-324.0, -1622.0, 132.0)
		disable_shadows True
