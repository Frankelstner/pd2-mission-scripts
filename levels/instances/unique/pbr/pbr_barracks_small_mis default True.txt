﻿´geo_setup´ MissionScriptElement 100053
	BASE DELAY 2
	on_executed
		´select_config´ (delay 0)
		´select_second_config´ (delay 0)
		´select_left_crate´ (delay 0)
		´select_right_crate´ (delay 0)
´select_config´ ElementRandom 100055
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´crates´ (delay 0)
		´wall´ (delay 0)
		´lockers´ (delay 0)
´crates´ MissionScriptElement 100056
	on_executed
		´show_crates´ (delay 0)
´wall´ MissionScriptElement 100057
	on_executed
		´show_wall´ (delay 0)
´lockers´ MissionScriptElement 100058
	on_executed
		´func_enable_unit_001´ (delay 0)
´show_crates´ ElementEnableUnit 100060
	position 1500.0, 200.0, 900.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/props/gen_prop_crate_wood_green/001 (629.0, 661.0, 0.0)
		2 units/payday2/props/ind_prop_military_mre_1x2m/001 (400.0, 664.0, 0.0)
		3 units/payday2/props/gen_prop_crate_wood_green/002 (279.0, 661.0, 0.0)
´show_wall´ ElementEnableUnit 100061
	position 1500.0, 0.0, 900.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/mkp_int_wall_2m_b/009 (200.0, 650.0, 0.0)
		2 units/payday2/architecture/mkp_int_wall_2m_a/006 (400.0, 650.0, 0.0)
		3 units/payday2/architecture/mkp_int_wall_4m_a/008 (800.0, 650.0, 0.0)
		4 units/payday2/architecture/ind/ind_ext_level_locker/020 (500.0, 700.0, 0.0)
		5 units/payday2/architecture/ind/ind_ext_level_locker/019 (600.0, 700.0, 0.0)
		6 units/payday2/architecture/ind/ind_ext_level_locker/013 (300.0, 700.0, 0.0)
		7 units/payday2/architecture/ind/ind_ext_level_locker/014 (200.0, 700.0, 0.0)
		8 units/payday2/architecture/ind/ind_ext_level_locker/016 (400.0, 625.0, 0.0)
		9 units/payday2/architecture/ind/ind_ext_level_locker/015 (300.0, 625.0, 0.0)
		10 units/payday2/architecture/ind/ind_ext_level_locker/017 (600.0, 625.0, 0.0)
		11 units/payday2/architecture/ind/ind_ext_level_locker/018 (700.0, 625.001, 0.0)
´func_enable_unit_001´ ElementEnableUnit 100062
	position 1500.0, -200.0, 900.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/ind/ind_ext_level_locker/016 (400.0, 625.0, 0.0)
		2 units/payday2/architecture/ind/ind_ext_level_locker/015 (300.0, 625.0, 0.0)
		3 units/payday2/architecture/ind/ind_ext_level_locker/014 (200.0, 700.0, 0.0)
		4 units/payday2/architecture/ind/ind_ext_level_locker/013 (300.0, 700.0, 0.0)
		5 units/payday2/props/gen_prop_crate_wood_green/001 (629.0, 661.0, 0.0)
´select_second_config´ ElementRandom 100059
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´wall_´ (delay 0)
		´crates_´ (delay 0)
		´left_wall´ (delay 0)
		´right_wall´ (delay 0)
´wall_´ MissionScriptElement 100063
	on_executed
		´show_wall_´ (delay 0)
´crates_´ MissionScriptElement 100064
	on_executed
		´show_crates_´ (delay 0)
´left_wall´ MissionScriptElement 100065
	on_executed
		´show_left_wall´ (delay 0)
´right_wall´ MissionScriptElement 100066
	on_executed
		´show_right_wall´ (delay 0)
´show_wall_´ ElementEnableUnit 100067
	position 2000.0, 300.0, 900.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/mkp_int_wall_2m_a/005 (800.0, 450.0, 0.0)
		2 units/payday2/architecture/mkp_int_wall_2m_a/001 (800.0, 650.0, 0.0)
´show_crates_´ ElementEnableUnit 100068
	position 2000.0, 100.0, 900.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/props/gen_prop_crate_wood_green/008 (843.879, 767.145, 0.0)
		2 units/payday2/props/gen_prop_crate_wood_green/007 (831.474, 553.144, 0.0)
		3 units/payday2/props/gen_prop_crate_wood_green/003 (833.881, 433.312, 0.0)
´show_left_wall´ ElementEnableUnit 100069
	position 2000.0, -100.0, 900.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/mkp_int_wall_2m_a/001 (800.0, 650.0, 0.0)
		2 units/payday2/props/gen_prop_crate_wood_green/007 (831.474, 553.144, 0.0)
		3 units/payday2/props/gen_prop_crate_wood_green/003 (833.881, 433.312, 0.0)
´show_right_wall´ ElementEnableUnit 100070
	position 2000.0, -300.0, 900.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/mkp_int_wall_2m_a/005 (800.0, 450.0, 0.0)
		2 units/payday2/props/gen_prop_crate_wood_green/008 (843.879, 767.145, 0.0)
´select_left_crate´ ElementRandom 100071
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´one´ (delay 0)
		´two´ (delay 0)
		´logic_link_001´ (delay 0)
		´one_alt´ (delay 0)
´one´ MissionScriptElement 100072
	on_executed
		´show_crate_002´ (delay 0)
´two´ MissionScriptElement 100073
	on_executed
		´show_crate_003´ (delay 0)
´logic_link_001´ MissionScriptElement 100074
´one_alt´ MissionScriptElement 100075
	on_executed
		´show_crate_001´ (delay 0)
´show_crate_001´ ElementEnableUnit 100076
	position 2500.0, 300.0, 900.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/props/gen_prop_crate_wood_green/004 (579.744, 899.322, 0.0)
´show_crate_002´ ElementEnableUnit 100077
	position 2500.0, 100.0, 900.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/props/gen_prop_crate_wood_green/005 (298.826, 877.025, 0.0)
´show_crate_003´ ElementEnableUnit 100078
	position 2500.0, -100.0, 900.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/props/gen_prop_crate_wood_green/004 (579.744, 899.322, 0.0)
		2 units/payday2/props/gen_prop_crate_wood_green/005 (298.826, 877.025, 0.0)
´select_right_crate´ ElementRandom 100079
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´there´ (delay 0)
		´not_there´ (delay 0)
´point_spawn_player_001´ ElementPlayerSpawner 100080
	EXECUTE ON STARTUP
	DISABLED
	position -700.0, 600.0, 0.0
	rotation 0.0, 0.0, 0.707107, -0.707107
	state standard
´there´ MissionScriptElement 100081
	on_executed
		´func_enable_unit_002´ (delay 0)
´not_there´ MissionScriptElement 100082
´func_enable_unit_002´ ElementEnableUnit 100083
	position 3000.0, 100.0, 900.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/props/gen_prop_crate_wood_green/006 (527.744, 418.322, 0.0)
´startup´ MissionScriptElement 100084
	EXECUTE ON STARTUP
	on_executed
		´geo_setup´ (delay 0)
		´hider´ (delay 0)
´hider´ ElementDisableUnit 100085
	position 1300.0, -400.0, 900.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/architecture/mkp_int_wall_2m_b/009 (200.0, 650.0, 0.0)
		2 units/payday2/architecture/mkp_int_wall_2m_a/006 (400.0, 650.0, 0.0)
		3 units/payday2/architecture/mkp_int_wall_4m_a/008 (800.0, 650.0, 0.0)
		4 units/payday2/architecture/ind/ind_ext_level_locker/017 (600.0, 625.0, 0.0)
		5 units/payday2/architecture/ind/ind_ext_level_locker/018 (700.0, 625.001, 0.0)
		6 units/payday2/props/ind_prop_military_mre_1x2m/001 (400.0, 664.0, 0.0)
		7 units/payday2/architecture/ind/ind_ext_level_locker/016 (400.0, 625.0, 0.0)
		8 units/payday2/architecture/ind/ind_ext_level_locker/015 (300.0, 625.0, 0.0)
		9 units/payday2/props/gen_prop_crate_wood_green/002 (279.0, 661.0, 0.0)
		10 units/payday2/props/gen_prop_crate_wood_green/001 (629.0, 661.0, 0.0)
		11 units/payday2/architecture/ind/ind_ext_level_locker/020 (500.0, 700.0, 0.0)
		12 units/payday2/architecture/ind/ind_ext_level_locker/019 (600.0, 700.0, 0.0)
		13 units/payday2/architecture/ind/ind_ext_level_locker/013 (300.0, 700.0, 0.0)
		14 units/payday2/architecture/ind/ind_ext_level_locker/014 (200.0, 700.0, 0.0)
		15 units/payday2/props/gen_prop_crate_wood_green/006 (527.744, 418.322, 0.0)
		16 units/payday2/props/gen_prop_crate_wood_green/005 (298.826, 877.025, 0.0)
		17 units/payday2/props/gen_prop_crate_wood_green/004 (579.744, 899.322, 0.0)
		18 units/payday2/architecture/mkp_int_wall_2m_a/005 (800.0, 450.0, 0.0)
		19 units/payday2/architecture/mkp_int_wall_2m_a/001 (800.0, 650.0, 0.0)
		20 units/payday2/props/gen_prop_crate_wood_green/008 (843.879, 767.145, 0.0)
		21 units/payday2/props/gen_prop_crate_wood_green/007 (831.474, 553.144, 0.0)
		22 units/payday2/props/gen_prop_crate_wood_green/003 (833.881, 433.312, 0.0)
´murky_001´ ElementSpawnEnemyDummy 100093
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 100.0, 550.0, 0.0
	rotation 0.0, 0.0, 1.0, -1.19209e-07
	spawn_action none
	team default
	voice 0
´murky_002´ ElementSpawnEnemyDummy 100094
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 714.782, 305.497, 0.0
	rotation 0.0, 0.0, 0.999962, -0.0087265
	spawn_action none
	team default
	voice 0
´murky_003´ ElementSpawnEnemyDummy 100095
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 1079.96, 351.512, 0.0
	rotation 0.0, 0.0, 0.930418, 0.366501
	spawn_action none
	team default
	voice 0
´murky_004´ ElementSpawnEnemyDummy 100096
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 1050.33, 237.511, 0.0
	rotation 0.0, 0.0, -0.366502, -0.930417
	spawn_action none
	team default
	voice 0
´murky_005´ ElementSpawnEnemyDummy 100097
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 944.02, 727.667, 0.0
	rotation 0.0, 0.0, 0.207911, -0.978148
	spawn_action none
	team default
	voice 0
´murky_006´ ElementSpawnEnemyDummy 100098
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 1038.12, 534.274, 0.0
	rotation 0.0, 0.0, 0.942642, 0.333806
	spawn_action none
	team default
	voice 0
´murky_007´ ElementSpawnEnemyDummy 100099
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 476.393, 1031.32, 0.0
	rotation 0.0, 0.0, -0.809018, -0.587784
	spawn_action none
	team default
	voice 0
´murky_008´ ElementSpawnEnemyDummy 100100
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 348.889, 772.217, 0.0
	rotation 0.0, 0.0, -0.39875, -0.91706
	spawn_action none
	team default
	voice 0
´murky_009´ ElementSpawnEnemyDummy 100101
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 465.239, 844.205, 0.0
	rotation 0.0, 0.0, -0.719341, -0.694658
	spawn_action none
	team default
	voice 0
´murky_010´ ElementSpawnEnemyDummy 100102
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 701.298, 791.459, 0.0
	rotation 0.0, 0.0, 0.629319, -0.777147
	spawn_action none
	team default
	voice 0
´murky_011´ ElementSpawnEnemyDummy 100103
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 527.806, 546.996, 0.0
	rotation 0.0, 0.0, 0.258818, -0.965926
	spawn_action none
	team default
	voice 0
´murky_012´ ElementSpawnEnemyDummy 100104
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 385.539, 292.058, 0.0
	rotation 0.0, 0.0, -0.284017, -0.958819
	spawn_action none
	team default
	voice 0
´murky_013´ ElementSpawnEnemyDummy 100105
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 698.058, 516.734, 0.0
	rotation 0.0, 0.0, 0.766044, -0.642788
	spawn_action none
	team default
	voice 0
´murky_014´ ElementSpawnEnemyDummy 100106
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 250.705, 503.859, 0.0
	rotation 0.0, 0.0, 0.930418, 0.3665
	spawn_action none
	team default
	voice 0
´murky_015´ ElementSpawnEnemyDummy 100107
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 1040.46, 766.569, 0.0
	rotation 0.0, 0.0, -0.382684, -0.923879
	spawn_action none
	team default
	voice 0
´murky_016´ ElementSpawnEnemyDummy 100108
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 216.0, 339.0, 0.0
	rotation 0.0, 0.0, -0.814116, -0.580702
	spawn_action none
	team default
	voice 0
´murky_017´ ElementSpawnEnemyDummy 100109
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 500.0, 700.0, 0.0
	rotation 0.0, 0.0, -2.68221e-07, -1.0
	spawn_action none
	team default
	voice 0
´murky_018´ ElementSpawnEnemyDummy 100110
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 1064.01, 1012.69, 0.0
	rotation 0.0, 0.0, 0.999848, -0.017453
	spawn_action none
	team default
	voice 0
´breached´ MissionScriptElement 100111
	TRIGGER TIMES 1
	on_executed
		´spawn_enemies´ (delay 0)
		´func_slow_motion_001´ (delay 0) DISABLED
´spawn_enemies´ MissionScriptElement 100112
	on_executed
		´alert´ (delay 0.75) DISABLED
		´pick_entry_1´ (delay 0)
		´pick_entry_2´ (delay 0)
		´pick_enemies´ (delay 0.01)
´pick_enemies´ ElementRandom 100113
	amount 2
	amount_random 3
	ignore_disabled True
	on_executed
		´murky_001´ (delay 0)
		´murky_002´ (delay 0)
		´murky_003´ (delay 0)
		´murky_004´ (delay 0)
		´murky_005´ (delay 0)
		´murky_006´ (delay 0)
		´murky_007´ (delay 0)
		´murky_008´ (delay 0)
		´murky_009´ (delay 0)
		´murky_010´ (delay 0)
		´murky_011´ (delay 0)
		´murky_012´ (delay 0)
		´murky_013´ (delay 0)
		´murky_014´ (delay 0)
		´murky_015´ (delay 0)
		´murky_016´ (delay 0)
		´murky_017´ (delay 0)
		´murky_018´ (delay 0)
		´murky_019´ (delay 0)
		´murky_020´ (delay 0)
		´murky_021´ (delay 0)
´func_sequence_trigger_001´ ElementUnitSequenceTrigger 100114
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence done_opened
			unit_id units/payday2/equipment/gen_interactable_door_wooden/001 (25.0, 932.0, 0.0)
		2
			guis_id 2
			sequence done_opened
			unit_id units/payday2/equipment/gen_interactable_door_wooden/002 (25.0, 232.0, 0.0)
	on_executed
		´breached´ (delay 0)
´alert´ ElementSpecialObjective 100115
	DISABLED
	SO_access 16376
	action_duration_max 0
	action_duration_min 0
	ai_group none
	align_position False
	align_rotation False
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis -1
	interrupt_dmg 1
	interval -1
	needs_pos_rsrv False
	path_haste none
	path_stance cbt
	path_style none
	patrol_path none
	pose none
	position 300.0, 1000.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	scan False
	search_distance 0
	search_position 1102.01, 804.812, 0.0
	so_action AI_hunt
	spawn_instigator_ids
		1 ´murky_017´
		2 ´murky_008´
		3 ´murky_009´
		4 ´murky_007´
		5 ´murky_010´
		6 ´murky_018´
		7 ´murky_001´
		8 ´murky_014´
		9 ´murky_016´
		10 ´murky_012´
		11 ´murky_011´
		12 ´murky_002´
		13 ´murky_013´
		14 ´murky_004´
		15 ´murky_003´
		16 ´murky_006´
		17 ´murky_015´
		18 ´murky_005´
	trigger_on none
´func_slow_motion_001´ ElementSlowMotion 100116
	DISABLED
	eff_name quickdraw_player
´trigger_area_001´ ElementAreaTrigger 100117
	TRIGGER TIMES 1
	amount 1
	depth 150
	height 200
	instigator player
	interval 0.1
	position -50.0, 1000.0, 75.0
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 150
	on_executed
		´func_sequence_001´ (delay 0)
		´point_play_sound_001´ (delay 0)
		´alert001´ (delay 0.25)
		´ai_attention_001´ (delay 0)
´func_sequence_001´ ElementUnitSequence 100118
	position -50.0, 950.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_door_wooden/001 (25.0, 932.0, 0.0)
			notify_unit_sequence state_door_open
			time 0
´trigger_area_002´ ElementAreaTrigger 100119
	TRIGGER TIMES 1
	amount 1
	depth 150
	height 200
	instigator player
	interval 0.1
	position -50.0, 300.0, 75.0
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 150
	on_executed
		´func_sequence_002´ (delay 0)
		´point_play_sound_002´ (delay 0)
		´alert002´ (delay 0.25)
		´ai_attention_002´ (delay 0)
´func_sequence_002´ ElementUnitSequence 100120
	position -50.0, 350.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_door_wooden/002 (25.0, 232.0, 0.0)
			notify_unit_sequence state_door_open
			time 0
´point_play_sound_001´ ElementPlaySound 100121
	append_prefix False
	elements
	position -50.0, 975.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	sound_event door_wooden_kicked_in
	use_instigator False
´point_play_sound_002´ ElementPlaySound 100122
	append_prefix False
	elements
	position -50.0, 275.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	sound_event door_wooden_kicked_in
	use_instigator False
´alert001´ ElementSpecialObjective 100123
	SO_access 16376
	action_duration_max 0
	action_duration_min 0
	ai_group none
	align_position False
	align_rotation False
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis -1
	interrupt_dmg 1
	interval -1
	needs_pos_rsrv False
	path_haste none
	path_stance cbt
	path_style none
	patrol_path none
	pose none
	position -200.0, 1000.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	scan False
	search_distance 0
	search_position 318.003, 461.042, 0.0
	so_action AI_hunt
	spawn_instigator_ids
		1 ´murky_017´
		2 ´murky_009´
		3 ´murky_007´
		4 ´murky_008´
	trigger_on none
´alert002´ ElementSpecialObjective 100124
	SO_access 16376
	action_duration_max 0
	action_duration_min 0
	ai_group none
	align_position False
	align_rotation False
	attitude none
	base_chance 1
	chance_inc 0
	interaction_voice none
	interrupt_dis -1
	interrupt_dmg 1
	interval -1
	needs_pos_rsrv False
	path_haste none
	path_stance cbt
	path_style none
	patrol_path none
	pose none
	position -200.0, 300.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	scan False
	search_distance 0
	search_position 318.003, 461.042, 0.0
	so_action AI_hunt
	spawn_instigator_ids
		1 ´murky_001´
		2 ´murky_016´
		3 ´murky_012´
		4 ´murky_014´
		5 ´murky_011´
	trigger_on none
´ai_attention_001´ ElementAIAttention 100125
	att_obj_u_id units/dev_tools/level_tools/ai_attention_object/001 (200.0, 900.0, 200.0)
	operation set
	override none
	position 200.0, 1000.0, 200.0
	preset enemy_enemy_cbt
	rotation 0.0, 0.0, 0.0, -1.0
´ai_attention_002´ ElementAIAttention 100128
	att_obj_u_id units/dev_tools/level_tools/ai_attention_object/002 (200.0, 300.0, 200.0)
	operation set
	override none
	position 300.0, 300.0, 200.0
	preset enemy_enemy_cbt
	rotation 0.0, 0.0, 0.0, -1.0
´murky_019´ ElementSpawnEnemyDummy 100129
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 942.027, 573.285, 0.0
	rotation 0.0, 0.0, 0.996195, -0.0871567
	spawn_action none
	team default
	voice 0
´murky_020´ ElementSpawnEnemyDummy 100130
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 853.517, 1012.34, 0.0
	rotation 0.0, 0.0, -0.0174542, -0.999848
	spawn_action none
	team default
	voice 0
´murky_021´ ElementSpawnEnemyDummy 100131
	TRIGGER TIMES 1
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_guard_national_1/ene_guard_national_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position 697.623, 1022.79, 0.0
	rotation 0.0, 0.0, -0.0261788, -0.999657
	spawn_action none
	team default
	voice 0
´pick_entry_1´ ElementRandom 100132
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´murky_017´ (delay 0)
		´murky_008´ (delay 0)
		´murky_009´ (delay 0)
		´murky_007´ (delay 0)
´pick_entry_2´ ElementRandom 100133
	amount 1
	amount_random 0
	ignore_disabled True
	on_executed
		´murky_001´ (delay 0)
		´murky_014´ (delay 0)
		´murky_016´ (delay 0)
		´murky_012´ (delay 0)
