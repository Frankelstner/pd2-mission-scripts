﻿´ares´ ElementInstanceInput 100008
	Upon mission event "ares":
		´current_vault（9）´ = 1
´current_vault（9）´ ElementCounter 100001
´chronos´ ElementInstanceInput 100009
	Upon mission event "chronos":
		´current_vault（9）´ = 2
´demeter´ ElementInstanceInput 100010
	Upon mission event "demeter":
		´current_vault（9）´ = 3
´hades´ ElementInstanceInput 100011
	Upon mission event "hades":
		´current_vault（9）´ = 4
´poseidon´ ElementInstanceInput 100012
	Upon mission event "poseidon":
		´current_vault（9）´ = 5
´zeus´ ElementInstanceInput 100013
	Upon mission event "zeus":
		´current_vault（9）´ = 6
´state_progress´ ElementInstanceInput 100026
	Upon mission event "state_progress":
		If ´current_vault（9）´ == 1:
			Execute sequence: "ares_progress" - units/pd2_dlc_berry/props/bry_prop_control_screen/001 (0.0, 0.0, 0.0)
		If ´current_vault（9）´ == 2:
			Execute sequence: "chronos_progress" - units/pd2_dlc_berry/props/bry_prop_control_screen/001 (0.0, 0.0, 0.0)
		If ´current_vault（9）´ == 3:
			Execute sequence: "demeter_progress" - units/pd2_dlc_berry/props/bry_prop_control_screen/001 (0.0, 0.0, 0.0)
		If ´current_vault（9）´ == 4:
			Execute sequence: "hades_progress" - units/pd2_dlc_berry/props/bry_prop_control_screen/001 (0.0, 0.0, 0.0)
		If ´current_vault（9）´ == 5:
			Execute sequence: "poseidon_progress" - units/pd2_dlc_berry/props/bry_prop_control_screen/001 (0.0, 0.0, 0.0)
		If ´current_vault（9）´ == 6:
			Execute sequence: "zeus_progress" - units/pd2_dlc_berry/props/bry_prop_control_screen/001 (0.0, 0.0, 0.0)
´state_interrupt´ ElementInstanceInput 100027
	Upon mission event "state_interrupt":
		If ´current_vault（9）´ == 1:
			Execute sequence: "ares_interupt" - units/pd2_dlc_berry/props/bry_prop_control_screen/001 (0.0, 0.0, 0.0)
		If ´current_vault（9）´ == 2:
			Execute sequence: "chronos_interupt" - units/pd2_dlc_berry/props/bry_prop_control_screen/001 (0.0, 0.0, 0.0)
		If ´current_vault（9）´ == 3:
			Execute sequence: "demeter_interupt" - units/pd2_dlc_berry/props/bry_prop_control_screen/001 (0.0, 0.0, 0.0)
		If ´current_vault（9）´ == 4:
			Execute sequence: "hades_interupt" - units/pd2_dlc_berry/props/bry_prop_control_screen/001 (0.0, 0.0, 0.0)
		If ´current_vault（9）´ == 5:
			Execute sequence: "poseidon_interupt" - units/pd2_dlc_berry/props/bry_prop_control_screen/001 (0.0, 0.0, 0.0)
		If ´current_vault（9）´ == 6:
			Execute sequence: "zeus_interupt" - units/pd2_dlc_berry/props/bry_prop_control_screen/001 (0.0, 0.0, 0.0)
´state_open´ ElementInstanceInput 100040
	Upon mission event "state_open":
		If ´current_vault（9）´ == 1:
			Execute sequence: "ares_unlocked" - units/pd2_dlc_berry/props/bry_prop_control_screen/001 (0.0, 0.0, 0.0)
		If ´current_vault（9）´ == 2:
			Execute sequence: "chronos_unlocked" - units/pd2_dlc_berry/props/bry_prop_control_screen/001 (0.0, 0.0, 0.0)
		If ´current_vault（9）´ == 3:
			Execute sequence: "demeter_unlocked" - units/pd2_dlc_berry/props/bry_prop_control_screen/001 (0.0, 0.0, 0.0)
		If ´current_vault（9）´ == 4:
			Execute sequence: "hades_unlocked" - units/pd2_dlc_berry/props/bry_prop_control_screen/001 (0.0, 0.0, 0.0)
		If ´current_vault（9）´ == 5:
			Execute sequence: "poseidon_unlocked" - units/pd2_dlc_berry/props/bry_prop_control_screen/001 (0.0, 0.0, 0.0)
		If ´current_vault（9）´ == 6:
			Execute sequence: "zeus_unlocked" - units/pd2_dlc_berry/props/bry_prop_control_screen/001 (0.0, 0.0, 0.0)
