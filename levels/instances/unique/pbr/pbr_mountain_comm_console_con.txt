ID range vs continent name:
	100000: world
	200000: editor_only

statics
	100000 units/payday2/props/stn_prop_office_desk_communication/001 (100.0, 0.0, 2.5)
	100006 units/pd2_dlc_jolly/equipment/gen_plant_c4/001 (-103.75, 97.8349, 235.535)
		mesh_variation state_vis_hide
	100001 units/pd2_dlc_jolly/equipment/gen_plant_c4/002 (75.0, 50.0, 77.3655)
		mesh_variation state_vis_hide
	100007 units/pd2_dlc_jolly/equipment/gen_plant_c4/003 (75.0, 225.0, 77.3655)
		mesh_variation state_vis_hide
	200000 units/test/jocke/plane_black_temp/002 (0.0, 0.0, 0.0)
	200001 units/test/jocke/plane_black_temp/003 (0.0, 1000.0, 0.0)
