ID range vs continent name:
	100000: world

statics
	100150 units/dev_tools/dev_ladder/001 (518.0, -1438.0, -12.996)
		disable_shadows True
		height 470
		width 110
	100047 units/dev_tools/level_tools/dev_collision_1x3m/001 (275.0, -800.0, -3.99594)
		disable_shadows True
	100048 units/dev_tools/level_tools/dev_collision_1x3m/002 (25.0, -800.0, -3.99594)
		disable_shadows True
	100017 units/dev_tools/level_tools/dev_collision_1x3m/003 (-362.0, -50.0, 50.0041)
	100050 units/dev_tools/level_tools/dev_collision_1x3m/004 (-462.0, -50.0, 50.0041)
	100051 units/dev_tools/level_tools/dev_collision_1x3m/005 (-512.0, -50.0, 50.0041)
	100093 units/dev_tools/level_tools/dev_collision_1x3m/006 (-512.0, -350.0, 50.0041)
	100094 units/dev_tools/level_tools/dev_collision_1x3m/007 (-462.0, -350.0, 50.0041)
	100095 units/dev_tools/level_tools/dev_collision_1x3m/008 (-362.0, -350.0, 50.0041)
	100096 units/dev_tools/level_tools/dev_collision_1x3m/009 (-512.0, -491.0, 50.0041)
	100097 units/dev_tools/level_tools/dev_collision_1x3m/010 (-462.0, -491.0, 50.0041)
	100102 units/dev_tools/level_tools/dev_collision_1x3m/011 (-362.0, -491.0, 50.0041)
	100154 units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-75.0, -1450.0, 0.00406057)
		disable_shadows True
	100155 units/dev_tools/level_tools/dev_door_blocker_1x1x3/002 (450.0, -1250.0, 0.00406057)
		disable_shadows True
	100469 units/payday2/architecture/com_ext_parking_garage_decal_leak_a_1m/001 (-64.0, 198.0, 358.004)
		disable_shadows True
	100474 units/payday2/architecture/com_ext_parking_garage_decal_leak_a_1m/002 (-156.0, 198.0, 371.004)
		disable_shadows True
	100475 units/payday2/architecture/com_ext_parking_garage_decal_leak_a_1m/003 (-226.0, 198.0, 360.004)
		disable_shadows True
	100235 units/payday2/architecture/com_ext_parking_garage_decal_stain_4m/001 (60.0, 3.0, 0.0)
		disable_shadows True
	100236 units/payday2/architecture/com_ext_parking_garage_decal_stain_4m/002 (-9.0, -1208.0, 139.0)
		disable_shadows True
	100043 units/payday2/architecture/com_ext_parking_garage_decal_stain_4m/003 (-12.0, -2.5805e-05, -98.0)
		disable_shadows True
	100126 units/payday2/architecture/com_ext_parking_garage_decal_stain_4m/004 (176.0, -9.36956e-06, -124.0)
		disable_shadows True
	100363 units/payday2/architecture/com_ext_parking_garage_decal_stain_4m/005 (626.0, 0.0, -64.0)
		disable_shadows True
	100364 units/payday2/architecture/com_ext_parking_garage_decal_stain_4m/006 (626.0, -442.0, -58.9999)
		disable_shadows True
	100365 units/payday2/architecture/com_ext_parking_garage_decal_stain_4m/007 (626.0, -735.0, -58.9999)
		disable_shadows True
	100366 units/payday2/architecture/com_ext_parking_garage_decal_stain_4m/008 (626.0, -790.0, -96.9999)
		disable_shadows True
	100367 units/payday2/architecture/com_ext_parking_garage_decal_stain_4m/009 (626.0, -1147.0, -96.9999)
		disable_shadows True
	100216 units/payday2/architecture/com_int_basement_garage_parkingline_a/001 (-825.0, 200.0, -25.0)
		disable_shadows True
	100217 units/payday2/architecture/com_int_basement_garage_parkingline_a/002 (-825.0, -200.0, -25.0)
		disable_shadows True
	100218 units/payday2/architecture/com_int_basement_garage_parkingline_a/003 (-825.0, -600.0, -25.0)
		disable_shadows True
	100219 units/payday2/architecture/com_int_basement_garage_parkingline_a/004 (-825.0, -1000.0, -25.0)
		disable_shadows True
	100228 units/payday2/architecture/com_int_basement_garage_parkingline_a/005 (-825.0, -1400.0, -25.0)
		disable_shadows True
	100257 units/payday2/architecture/com_int_basement_garage_parkingline_a/006 (-825.0, -1800.0, -25.0)
		disable_shadows True
	100259 units/payday2/architecture/com_int_basement_garage_parkingline_a/007 (-2475.0, 400.0, -25.0)
		disable_shadows True
	100260 units/payday2/architecture/com_int_basement_garage_parkingline_a/008 (-2475.0, 0.0, -25.0)
		disable_shadows True
	100261 units/payday2/architecture/com_int_basement_garage_parkingline_a/009 (-2475.0, -400.0, -25.0)
		disable_shadows True
	100262 units/payday2/architecture/com_int_basement_garage_parkingline_a/010 (-2475.0, -800.0, -25.0)
		disable_shadows True
	100263 units/payday2/architecture/com_int_basement_garage_parkingline_a/011 (-2475.0, -1200.0, -25.0)
		disable_shadows True
	100264 units/payday2/architecture/com_int_basement_garage_parkingline_a/012 (-2475.0, -1600.0, -25.0)
		disable_shadows True
	100265 units/payday2/architecture/com_int_basement_garage_parkingline_a/013 (-2475.0, -2000.0, -25.0)
		disable_shadows True
	100266 units/payday2/architecture/com_int_basement_garage_parkingline_a/014 (-2475.0, -2400.0, -25.0)
		disable_shadows True
	100361 units/payday2/architecture/ind/ind_ext_level_duct_1_a/002 (25.0, -1394.0, 273.004)
		disable_shadows True
	100360 units/payday2/architecture/ind/ind_ext_level_duct_1_a/003 (25.0, -1194.0, 273.004)
		disable_shadows True
	100018 units/payday2/equipment/gen_interactable_door_hcm_double/001 (-200.0, 189.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		mesh_variation state_door_open
	100052 units/payday2/equipment/gen_interactable_door_hcm_double/002 (-199.0, -9.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		mesh_variation state_door_open
	100054 units/payday2/equipment/gen_interactable_door_reinforced_lab/001 (-142.0, -1577.0, -0.639722)
		disable_on_ai_graph True
		mesh_variation state_door_open
	100141 units/payday2/pickups/gen_pku_keycard/002 (125.0, -1125.0, 110.472)
		disable_shadows True
	100140 units/payday2/pickups/gen_pku_keycard/003 (550.783, -653.818, 114.966)
		disable_shadows True
	100164 units/payday2/pickups/gen_pku_keycard/004 (-400.0, -250.0, 51.5014)
		disable_shadows True
	100165 units/payday2/pickups/gen_pku_keycard/005 (-462.865, -641.183, 80.8536)
		disable_shadows True
	100166 units/payday2/pickups/gen_pku_keycard/006 (-525.0, -1125.0, 80.4951)
		disable_shadows True
	100152 units/payday2/props/a/ind_prop_rooftop_ladder_b_mid/001 (550.0, -1440.0, 53.004)
		disable_shadows True
	100282 units/payday2/props/air_prop_terminal_coffecup/001 (574.0, -961.0, 200.418)
		disable_shadows True
	100283 units/payday2/props/air_prop_terminal_coffecup/002 (575.0, -973.0, 200.418)
		disable_shadows True
	100284 units/payday2/props/air_prop_terminal_coffecup/003 (565.0, -968.0, 200.418)
		disable_shadows True
	100285 units/payday2/props/air_prop_terminal_coffecup/004 (591.0, -991.0, 200.418)
		disable_shadows True
	100286 units/payday2/props/air_prop_terminal_coffecup/005 (576.0, -986.0, 200.418)
		disable_shadows True
	100287 units/payday2/props/air_prop_terminal_coffecup/006 (589.0, -973.0, 200.418)
		disable_shadows True
	100288 units/payday2/props/air_prop_terminal_coffecup/007 (584.0, -953.0, 200.418)
		disable_shadows True
	100291 units/payday2/props/air_prop_terminal_coffecup/008 (570.0, -1000.0, 200.418)
		disable_shadows True
	100292 units/payday2/props/air_prop_terminal_coffecup/009 (570.0, -1016.0, 200.418)
		disable_shadows True
	100354 units/payday2/props/bnk_prop_phone/001 (21.0181, -826.07, 111.55)
		disable_shadows True
	100348 units/payday2/props/com_prop_club_kitchen_egg_box/001 (284.0, -1145.0, 110.133)
		disable_shadows True
	100340 units/payday2/props/com_prop_club_kitchen_pot_b/001 (267.0, -1153.0, 151.133)
		disable_shadows True
	100343 units/payday2/props/com_prop_club_kitchen_pot_c/001 (326.0, -1146.0, 112.133)
		disable_shadows True
	100084 units/payday2/props/com_prop_club_sign_open/001 (121.0, -34.0, 289.004)
		disable_shadows True
	100085 units/payday2/props/com_prop_club_sign_open/002 (-593.0, -126.0, 288.004)
		disable_shadows True
	100352 units/payday2/props/com_prop_gallery_brochurestand_wall/001 (7.62939e-06, -828.0, 129.004)
		disable_shadows True
	100329 units/payday2/props/com_prop_janitor_mop_lying/001 (71.4045, -1532.03, 0.00406057)
		disable_shadows True
	100331 units/payday2/props/com_prop_janitor_mop_press/001 (18.9836, -1529.31, 0.00406057)
		disable_shadows True
	100053 units/payday2/props/com_prop_store_teller_monitor/001 (13.0, -772.0, 106.55)
		disable_shadows True
	100117 units/payday2/props/counter/com_prop_bar_fryer/001 (100.0, -1175.0, 0.00406057)
		disable_shadows True
	100109 units/payday2/props/counter/com_prop_bar_kitchen_fridge/001 (-8.99988, -996.0, 0.00405884)
		disable_shadows True
	100349 units/payday2/props/counter/com_prop_bar_kitchen_fridge/002 (48.9997, -713.0, -115.996)
		disable_shadows True
	100071 units/payday2/props/counter/com_prop_bar_kitchen_fridge/003 (48.9998, -812.0, -111.996)
		disable_shadows True
	100110 units/payday2/props/counter/com_prop_bar_kitchen_oven/001 (350.0, -1175.0, 0.00406057)
		disable_shadows True
	100111 units/payday2/props/counter/com_prop_bar_kitchen_shelf/001 (262.0, -1175.0, 200.004)
		disable_shadows True
	100112 units/payday2/props/counter/com_prop_bar_kitchen_shelf/002 (600.0, -896.0, 200.004)
		disable_shadows True
	100113 units/payday2/props/counter/com_prop_bar_kitchen_sink/001 (600.0, -896.0, 0.00405884)
		disable_shadows True
	100114 units/payday2/props/counter/com_prop_bar_kitchen_tray_stand/001 (415.0, -1115.0, 0.00405884)
		disable_shadows True
	100116 units/payday2/props/counter/com_prop_bar_kitchen_unit_b/001 (200.0, -1175.0, 0.00406057)
		disable_shadows True
	100158 units/payday2/props/counter/com_prop_bar_kitchen_washer/003 (600.0, -759.999, 0.00406057)
		disable_shadows True
	100119 units/payday2/props/gen_interactable_panel_keycard/001 (-36.0, -1472.0, 89.4462)
		disable_shadows True
		mesh_variation state_5
	100142 units/payday2/props/gen_interactable_panel_keycard/002 (433.0, -1175.0, 100.0)
		disable_shadows True
		mesh_variation state_5
	100022 units/payday2/props/gen_prop_long_lamp_v2/001 (240.0, -1400.0, 338.163)
		disable_shadows True
	100375 units/payday2/props/ind_prop_alley_garbage_bags_01/001 (727.0, -908.0, -24.996)
		disable_shadows True
	100379 units/payday2/props/ind_prop_alley_garbage_bags_02/001 (159.0, -1660.0, -9.99594)
	100376 units/payday2/props/ind_prop_alley_garbage_bags_03/001 (676.0, -1200.0, -24.996)
		disable_shadows True
	100045 units/payday2/props/off_prop_generic_shelf_wire_b/001 (93.0, -1240.0, 0.00406057)
		disable_shadows True
	100086 units/payday2/props/set/ind_prop_warehouse_box_a/001 (75.0, -1239.0, 100.175)
		disable_shadows True
	100091 units/payday2/props/set/ind_prop_warehouse_box_a/002 (75.0, -1239.0, 116.175)
		disable_shadows True
	100092 units/payday2/props/set/ind_prop_warehouse_box_c/001 (125.0, -1245.0, 161.004)
		disable_shadows True
	100078 units/payday2/props/set/ind_prop_warehouse_box_d/001 (107.0, -1235.0, 40.0041)
	100371 units/payday2/props/set/ind_prop_warehouse_pallet/001 (650.604, -871.052, 23.5795)
		disable_shadows True
	100255 units/payday2/props/str_prop_alley_trash_container/001 (305.0, -1675.0, -24.996)
	100256 units/payday2/props/str_prop_alley_trash_container/002 (535.0, -1675.0, -24.996)
	100034 units/payday2/props/str_prop_rooftop_ac_unit_a_twin/001 (223.0, -802.0, 400.004)
		disable_on_ai_graph True
	100320 units/pd2_dlc1/props/res_prop_motel_tv/001 (567.383, -1141.21, 264.169)
		disable_shadows True
	100359 units/pd2_dlc1/props/res_prop_motel_tv/002 (570.805, -55.2469, 267.184)
		disable_shadows True
	100446 units/pd2_dlc1/props/res_prop_store_beer/002 (250.0, -893.0, 400.004)
		disable_shadows True
	100447 units/pd2_dlc1/props/res_prop_store_beer/003 (227.27, -880.272, 404.004)
		disable_shadows True
	100077 units/pd2_dlc1/props/res_prop_store_beer/004 (154.0, -1162.0, 269.418)
		disable_shadows True
	100449 units/pd2_dlc1/props/res_prop_store_beer/005 (133.0, -867.0, 409.004)
		disable_shadows True
	100079 units/pd2_dlc1/props/res_prop_store_beer/007 (247.0, -1162.0, 269.418)
		disable_shadows True
	100087 units/pd2_dlc1/props/res_prop_store_beer/009 (185.0, -1160.0, 269.418)
		disable_shadows True
	100088 units/pd2_dlc1/props/res_prop_store_beer/010 (588.589, -1001.26, 273.418)
		disable_shadows True
	100089 units/pd2_dlc1/props/res_prop_store_beer/011 (197.0, -1162.0, 269.418)
		disable_shadows True
	100090 units/pd2_dlc1/props/res_prop_store_beer/012 (586.828, -1012.17, 269.418)
		disable_shadows True
	100127 units/pd2_dlc2/csgo_models/props_equipment/phone_booth/001 (-552.0, 8.0, -5.996)
		disable_shadows True
	100368 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/001 (600.0, -100.0, -35.0)
		disable_shadows True
	100369 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/002 (615.0, -741.0, -35.0)
		disable_shadows True
	100380 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/003 (639.0, -946.0, -35.0)
		disable_shadows True
	100381 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/004 (601.0, -1471.0, -49.0)
		disable_shadows True
	100382 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/005 (601.0, -240.0, -55.0)
		disable_shadows True
	100383 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/006 (601.0, -606.0, -62.0)
		disable_shadows True
	100384 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/007 (615.0, -218.0, -35.0)
		disable_shadows True
	100385 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/008 (416.0, 60.0, -38.0)
		disable_shadows True
	100386 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/009 (158.0, 60.0, -37.0)
		disable_shadows True
	100387 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/010 (281.0, 38.0, -37.0)
		disable_shadows True
	100388 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/011 (522.0, 25.0, -52.0)
		disable_shadows True
	100389 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/012 (-644.0, -65.0, -28.0)
		disable_shadows True
	100390 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/013 (-614.0, -496.0, -21.0)
		disable_shadows True
	100391 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/014 (-597.0, -608.0, -32.0)
		disable_shadows True
	100392 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/015 (-597.0, -1426.0, -12.0)
		disable_shadows True
	100393 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/016 (89.0, -1579.0, -12.0)
		disable_shadows True
	100394 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/017 (217.0, -1579.0, -21.0)
		disable_shadows True
	100395 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/018 (565.0, -1597.0, -46.0)
		disable_shadows True
	100396 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/019 (-487.0, 380.0, -46.0)
		disable_shadows True
	100137 units/pd2_dlc2/csgo_models/props_foliage/urban_bush_angled_64/020 (-392.0, 38.0, -9.0)
		disable_shadows True
	100013 units/pd2_dlc2/csgo_models/props_unique/skylight_broken_large/001 (-50.0, -125.0, 400.0)
	100039 units/pd2_dlc2/csgo_models/props_unique/skylight_broken_large/002 (-500.0, -1275.0, 400.0)
	100115 units/pd2_dlc_arena/equipment/gen_interactable_door_reinforced_v2/001 (447.0, -1197.0, 0.00406057)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
		mesh_variation deactivate_door
	100099 units/pd2_dlc_arena/equipment/gen_interactable_door_reinforced_v2/002 (-14.9999, -1453.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
		mesh_variation deactivate_door
	100294 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/002 (576.0, -923.0, 236.418)
		disable_shadows True
	100298 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/005 (582.0, -948.0, 151.418)
		disable_shadows True
	100299 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/006 (582.0, -948.0, 152.418)
		disable_shadows True
	100300 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/007 (582.0, -948.0, 153.418)
		disable_shadows True
	100301 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/008 (582.0, -948.0, 154.418)
		disable_shadows True
	100303 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/010 (582.0, -948.0, 155.418)
		disable_shadows True
	100304 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/011 (582.0, -948.0, 156.418)
		disable_shadows True
	100305 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/012 (582.0, -948.0, 157.418)
		disable_shadows True
	100306 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/013 (582.0, -948.0, 158.418)
		disable_shadows True
	100307 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/014 (582.0, -948.0, 159.418)
		disable_shadows True
	100308 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/015 (582.0, -948.0, 160.418)
		disable_shadows True
	100309 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/016 (582.0, -970.0, 151.418)
		disable_shadows True
	100310 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/017 (582.0, -970.0, 152.418)
		disable_shadows True
	100311 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/018 (582.0, -970.0, 153.418)
		disable_shadows True
	100312 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/019 (582.0, -970.0, 154.418)
		disable_shadows True
	100313 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/020 (572.0, -932.0, 97.4179)
		disable_shadows True
	100314 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/021 (572.0, -920.0, 99.4179)
		disable_shadows True
	100244 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/022 (576.0, -923.0, 235.418)
		disable_shadows True
	100333 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/023 (576.0, -923.0, 238.418)
		disable_shadows True
	100296 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/024 (582.0, -930.0, 203.418)
		disable_shadows True
	100297 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/025 (582.0, -930.0, 202.418)
		disable_shadows True
	100334 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/026 (582.0, -930.0, 201.418)
		disable_shadows True
	100335 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/027 (582.0, -930.0, 200.418)
		disable_shadows True
	100120 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/028 (-508.0, -440.0, 79.418)
		disable_shadows True
	100121 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/029 (-533.0, -410.0, 79.418)
		disable_shadows True
	100122 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/030 (-412.0, -443.0, 79.418)
		disable_shadows True
	100123 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/031 (-514.0, -200.0, 79.418)
		disable_shadows True
	100125 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/032 (145.0, -651.0, 115.418)
		disable_shadows True
	100128 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/033 (-83.9998, -889.0, 115.418)
		disable_shadows True
	100129 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/034 (-83.9997, -993.0, 115.418)
		disable_shadows True
	100131 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/035 (338.0, -330.0, 79.418)
		disable_shadows True
	100132 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/036 (379.0, -384.0, 79.418)
		disable_shadows True
	100133 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/037 (376.0, -301.0, 79.418)
		disable_shadows True
	100134 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/038 (-399.0, -920.0, 79.418)
		disable_shadows True
	100463 units/pd2_dlc_casino/props/cas_prop_int_dinning_set_table_plate/039 (-553.0, -1081.0, 79.418)
		disable_shadows True
	100199 units/pd2_dlc_cro/metal_scratches_decal/001 (-459.0, -299.0, 1.0)
		disable_shadows True
	100200 units/pd2_dlc_cro/metal_scratches_decal/002 (-453.0, -566.0, 1.0)
		disable_shadows True
	100201 units/pd2_dlc_cro/metal_scratches_decal/003 (241.0, -320.0, 1.0)
		disable_shadows True
	100202 units/pd2_dlc_cro/metal_scratches_decal/004 (241.0, -278.0, 1.0)
		disable_shadows True
	100203 units/pd2_dlc_cro/metal_scratches_decal/005 (241.0, -656.0, 1.0)
		disable_shadows True
	100204 units/pd2_dlc_cro/metal_scratches_decal/006 (-64.9996, -979.0, 1.0)
		disable_shadows True
	100025 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_ceilinglamp/001 (-475.0, -125.0, 374.0)
		disable_shadows True
	100038 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_ceilinglamp/002 (-475.0, -300.0, 374.0)
		disable_shadows True
	100040 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_ceilinglamp/003 (-475.0, -500.0, 374.0)
		disable_shadows True
	100108 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_ceilinglamp_short/001 (134.0, -672.0, 340.0)
		disable_shadows True
	100237 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_ceilinglamp_short/002 (334.0, -672.0, 340.0)
		disable_shadows True
	100238 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_ceilinglamp_short/003 (-61.0, -825.0, 340.0)
		disable_shadows True
	100239 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_ceilinglamp_short/004 (509.0, -672.0, 340.0)
		disable_shadows True
	100240 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_ceilinglamp_short/005 (-61.0, -1000.0, 340.0)
		disable_shadows True
	100241 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_ceilinglamp_short/006 (-61.0, -1200.0, 340.0)
		disable_shadows True
	100024 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_chair/001 (-416.0, -850.0, 0.0)
		disable_shadows True
	100030 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_chair/002 (-541.0, -863.0, 0.0)
		disable_shadows True
	100032 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_chair/003 (-403.385, -949.615, 0.0)
		disable_shadows True
	100033 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_chair/004 (-541.0, -956.0, 0.0)
		disable_shadows True
	100035 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_chair/006 (-542.0, -1051.0, 0.0)
		disable_shadows True
	100037 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_chair/007 (-417.0, -1150.0, 0.0)
		disable_shadows True
	100049 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_chair/008 (-542.0, -1150.0, 0.0)
		disable_shadows True
	100107 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_ketchup/001 (374.553, -632.623, 15.0041)
		disable_shadows True
	100001 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_menu/001 (124.0, -204.0, 79.2526)
		disable_shadows True
	100041 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_menu/002 (356.0, -212.0, 79.2526)
		disable_shadows True
	100057 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_menu/003 (-559.0, -178.0, 79.2526)
		disable_shadows True
	100060 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_menu/004 (-562.0, -424.0, 79.2526)
		disable_shadows True
	100066 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_menu/005 (186.0, -674.0, 115.253)
		disable_shadows True
	100067 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_menu/006 (-58.9999, -752.0, 115.253)
		disable_shadows True
	100068 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_menu/007 (-559.0, -668.0, 79.2526)
		disable_shadows True
	100069 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_menu/008 (586.0, -674.0, 115.253)
		disable_shadows True
	100070 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_menu/009 (-438.0, -905.0, 79.2526)
		disable_shadows True
	100075 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_menu/010 (-58.9998, -1052.0, 115.253)
		disable_shadows True
	100076 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_menu/011 (-568.0, -903.0, 79.2526)
		disable_shadows True
	100135 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_menu/012 (-441.0, -1102.0, 79.2526)
		disable_shadows True
	100136 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_menu/013 (-567.0, -1102.0, 79.2526)
		disable_shadows True
	100042 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_menu_b/001 (390.0, -674.0, 115.253)
		disable_shadows True
	100008 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_seat/001 (432.0, -300.0, 0.0)
		disable_shadows True
	100009 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_seat/002 (285.0, -300.0, 0.0)
		disable_shadows True
	100010 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_seat/003 (51.9999, -300.0, 0.0)
		disable_shadows True
	100014 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_seat/004 (199.0, -300.0, 0.0)
		disable_shadows True
	100026 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_seat/005 (-473.0, -97.0, 0.0)
		disable_shadows True
	100028 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_seat/006 (-473.0, -261.0, 0.0)
		disable_shadows True
	100029 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_seat/007 (-473.0, -347.0, 0.0)
		disable_shadows True
	100031 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_seat/008 (-473.0, -505.0, 0.0)
		disable_shadows True
	100081 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_seat/009 (-473.0, -749.0, 0.0)
		disable_shadows True
	100083 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_seat/010 (-473.0, -591.0, 0.0)
		disable_shadows True
	100003 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_stool/001 (-336.0, -427.0, -28.9959)
		disable_shadows True
	100004 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_stool/002 (144.0, -604.0, 12.0041)
		disable_shadows True
	100005 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_stool/003 (244.0, -604.0, 12.0041)
		disable_shadows True
	100006 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_stool/004 (344.0, -604.0, 12.0041)
		disable_shadows True
	100007 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_stool/005 (444.0, -604.0, 12.0041)
		disable_shadows True
	100016 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_stool/006 (-133.0, -790.0, 12.0041)
		disable_shadows True
	100019 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_stool/007 (544.0, -604.0, 12.0041)
		disable_shadows True
	100072 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_stool/008 (-133.0, -890.0, 12.0041)
		disable_shadows True
	100073 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_stool/009 (-133.0, -990.0, 12.0041)
		disable_shadows True
	100074 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_stool/010 (-133.0, -1090.0, 12.0041)
		disable_shadows True
	100080 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_stool/011 (-133.0, -1190.0, 12.0041)
		disable_shadows True
	100055 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_table/001 (-414.0, -902.0, 0.0)
		disable_shadows True
	100058 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_table/002 (-539.0, -902.0, 0.0)
		disable_shadows True
	100061 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_table/003 (-415.0, -1100.0, 0.0)
		disable_shadows True
	100064 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_table/004 (-540.0, -1100.0, 0.0)
		disable_shadows True
	100011 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_tablelong/001 (124.0, -300.0, 0.0)
		disable_shadows True
	100012 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_tablelong/002 (357.0, -300.0, 0.0)
		disable_shadows True
	100015 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_tablelong/003 (-473.0, -178.0, 0.0)
		disable_shadows True
	100027 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_tablelong/004 (-473.0, -425.0, 0.0)
		disable_shadows True
	100082 units/pd2_dlc_jerry/architecture/diner/jry_prop_diner_tablelong/005 (-473.0, -669.0, 0.0)
		disable_shadows True
	100160 units/pd2_dlc_jerry/architecture/jry_diner_chips/001 (-514.0, -200.0, 79.3979)
	100197 units/pd2_dlc_jerry/architecture/jry_diner_chips/002 (-413.0, -442.0, 79.3979)
	100453 units/pd2_dlc_jerry/architecture/jry_diner_chips/003 (146.0, -652.0, 116.398)
	100456 units/pd2_dlc_jerry/architecture/jry_diner_chips/004 (-83.0, -890.0, 116.398)
	100457 units/pd2_dlc_jerry/architecture/jry_diner_chips/005 (-83.0, -890.0, 116.398)
	100462 units/pd2_dlc_jerry/architecture/jry_diner_chips/006 (-553.0, -1081.0, 79.3979)
	100002 units/pd2_dlc_jerry/architecture/jry_diner_ext/001 (1.0, 0.0, 0.0039978)
	100000 units/pd2_dlc_jerry/architecture/jry_diner_int/001 (1.0, 0.0, 0.0039978)
	100059 units/pd2_dlc_jerry/architecture/jry_diner_light_spot/002 (132.0, -674.0, 116.0)
	100062 units/pd2_dlc_jerry/architecture/jry_diner_light_spot/003 (332.0, -674.0, 116.0)
	100063 units/pd2_dlc_jerry/architecture/jry_diner_light_spot/004 (-68.0, -817.0, 116.0)
	100065 units/pd2_dlc_jerry/architecture/jry_diner_light_spot/005 (507.0, -674.0, 116.0)
	100196 units/pd2_dlc_jerry/architecture/jry_diner_light_spot/006 (-68.0, -1017.0, 116.0)
	100242 units/pd2_dlc_jerry/architecture/jry_diner_light_spot/007 (-68.0, -1192.0, 116.0)
	100020 units/pd2_dlc_jerry/architecture/jry_diner_music_record/001 (-215.0, -1575.0, 188.004)
	100138 units/pd2_dlc_jerry/architecture/jry_diner_prop_milkshake_empty/001 (371.0, -356.0, 77.8536)
		disable_shadows True
	100139 units/pd2_dlc_jerry/architecture/jry_diner_prop_milkshake_empty/002 (-500.0, -199.0, 77.8536)
		disable_shadows True
	100198 units/pd2_dlc_jerry/architecture/jry_diner_prop_milkshake_empty/003 (-534.0, -161.0, 77.8536)
		disable_shadows True
	100215 units/pd2_dlc_jerry/architecture/jry_diner_prop_milkshake_empty/004 (129.0, -656.0, 114.854)
		disable_shadows True
	100452 units/pd2_dlc_jerry/architecture/jry_diner_prop_milkshake_empty/005 (-69.0, -1015.0, 114.854)
		disable_shadows True
	100459 units/pd2_dlc_jerry/architecture/jry_diner_prop_milkshake_empty/006 (571.0, -962.0, 110.44)
		disable_shadows True
	100460 units/pd2_dlc_jerry/architecture/jry_diner_prop_milkshake_empty/007 (579.0, -966.0, 110.44)
		disable_shadows True
	100461 units/pd2_dlc_jerry/architecture/jry_diner_prop_milkshake_empty/008 (567.0, -975.0, 110.44)
		disable_shadows True
	100464 units/pd2_dlc_jerry/architecture/jry_diner_prop_milkshake_empty/009 (-541.0, -1090.0, 77.8536)
		disable_shadows True
	100056 units/pd2_dlc_jerry/architecture/jry_diner_sign/001 (-105.0, 222.0, 412.0)
	100036 units/pd2_dlc_jerry/architecture/jry_diner_wall_deco_a/001 (599.0, -258.0, 225.0)
		disable_shadows True
	100483 units/pd2_dlc_jerry/architecture/jry_diner_windows/001 (304.0, -13.0, 169.0)
		disable_shadows True
	100484 units/pd2_dlc_jerry/architecture/jry_diner_windows/002 (-609.0, -297.0, 169.0)
		disable_shadows True
	100485 units/pd2_dlc_jerry/architecture/jry_diner_windows/003 (-609.0, -897.0, 169.0)
		disable_shadows True
	100021 units/pd2_dlc_jerry/props/prop_diner_bin/jry_diner_prop_bin/001 (-27.7148, -324.763, 0.0)
		disable_shadows True
	100046 units/pd2_dlc_jerry/props/prop_diner_jukebox/jry_diner_prop_jukebox/001 (-535.764, -1515.96, 13.0)
		disable_shadows True
	100315 units/pd2_mcmansion/props/mcm_prop_kitchen_coffeemaker/001 (184.0, -1154.0, 109.472)
		disable_shadows True
	100105 units/world/architecture/hospital/props/clock/002 (362.0, -1174.0, 271.004)
		disable_shadows True
	100402 units/world/architecture/hospital/props/locker/hospital_locker_closed/001 (202.0, -1200.0, 0.00406057)
		disable_shadows True
	100403 units/world/architecture/hospital/props/locker/hospital_locker_closed/002 (237.0, -1197.0, 0.00406057)
		disable_shadows True
	100327 units/world/architecture/street/decals/ground/decal_ground_middle_puddle/001 (532.596, -953.874, 0.00406075)
	100338 units/world/architecture/street/decals/ground/decal_ground_middle_puddle/002 (570.888, -802.092, 0.00406075)
	100326 units/world/architecture/street/decals/ground/decal_ground_small_puddle/001 (528.876, -933.459, 0.00406075)
		disable_shadows True
	100328 units/world/architecture/street/decals/ground/decal_ground_small_puddle/002 (561.0, -928.0, 99.4399)
		disable_shadows True
	100100 units/world/architecture/suburbia/water_leaking/001 (-14.0, -987.0, 229.004)
		disable_shadows True
	100336 units/world/architecture/suburbia/water_leaking/002 (-14.0, -993.0, 226.004)
		disable_shadows True
	100337 units/world/architecture/suburbia/water_leaking/003 (604.0, -724.0, 124.004)
		disable_shadows True
	100355 units/world/props/apartment/cords/extension_socket_cord_02/001 (392.0, -1174.0, 31.0041)
		disable_shadows True
	100408 units/world/props/apartment/cords/extension_socket_cord_02/002 (601.0, -759.0, 39.0041)
		disable_shadows True
	100486 units/world/props/apartment/cords/extension_socket_cord_02/003 (-448.975, -1576.32, 46.867)
		disable_shadows True
	100358 units/world/props/apartment/cords/wall_socket/001 (601.0, -736.0, 12.0041)
		disable_shadows True
	100488 units/world/props/apartment/cords/wall_socket/002 (-423.0, -1575.0, 14.0)
		disable_shadows True
	100489 units/world/props/apartment/cords/wall_socket/003 (-423.0, -1574.0, 14.0)
		disable_shadows True
	100490 units/world/props/apartment/cords/wall_socket/004 (-423.0, -1573.0, 14.0)
		disable_shadows True
	100491 units/world/props/apartment/cords/wall_socket/005 (-423.0, -1572.0, 14.0)
		disable_shadows True
	100324 units/world/props/apartment/exit_sign_01/001 (-198.0, -1572.0, 259.0)
		disable_shadows True
	100339 units/world/props/apartment/kitchen_counter/apartment_frying_pan/001 (224.482, -1123.93, 113.133)
		disable_shadows True
	100023 units/world/props/bank/bank_plant/001 (-25.0, -223.0, -13.9959)
		disable_shadows True
	100293 units/world/props/bank/counter/bank_kitchen_fan_01/001 (62.0, -1175.0, 235.004)
		disable_shadows True
	100344 units/world/props/bank/counter/bank_kitchen_pot/001 (238.0, -1156.0, 151.38)
		disable_shadows True
	100341 units/world/props/bank/counter/bank_kitchen_pot/002 (267.399, -1156.9, 153.133)
		disable_shadows True
	100342 units/world/props/bank/counter/bank_kitchen_pot/003 (266.174, -1157.61, 161.582)
		disable_shadows True
	100345 units/world/props/bank/counter/bank_kitchen_spatula/001 (163.0, -1174.0, 177.38)
		disable_shadows True
	100346 units/world/props/bank/counter/bank_kitchen_spatula/002 (207.657, -1128.9, 116.688)
		disable_shadows True
	100347 units/world/props/bank/counter/bank_kitchen_spatula/003 (197.0, -1174.0, 177.38)
		disable_shadows True
	100044 units/world/props/fire_extinguisher/001 (-90.0, -1566.0, 67.0)
		disable_shadows True
	100101 units/world/props/fire_extinguisher/002 (593.0, -1067.0, 64.2467)
		disable_shadows True
	100357 units/world/props/gym/cutscene_socketwire/stn_cs_socket_wire/001 (299.0, -1175.0, 36.0041)
		disable_shadows True
	100410 units/world/props/gym/cutscene_socketwire/stn_cs_socket_wire/003 (601.0, -858.0, 50.0041)
		disable_shadows True
	100317 units/world/props/gym/stn_prop_cup_single/001 (166.0, -1136.0, 108.472)
		disable_shadows True
	100289 units/world/props/gym/stn_prop_cup_stacked/001 (583.0, -1009.0, 198.418)
		disable_shadows True
	100290 units/world/props/gym/stn_prop_cup_stacked/002 (589.0, -1033.0, 198.418)
		disable_shadows True
	100316 units/world/props/gym/stn_prop_cup_stacked/003 (161.0, -1162.0, 108.418)
		disable_shadows True
	100319 units/world/props/gym/stn_prop_cup_stacked/005 (137.0, -1162.0, 150.418)
		disable_shadows True
	100321 units/world/props/gym/stn_prop_cup_stacked/007 (124.0, -1148.0, 150.418)
		disable_shadows True
	100322 units/world/props/gym/stn_prop_cup_stacked/008 (118.0, -1162.0, 150.418)
		disable_shadows True
	100295 units/world/props/office/fan_table/001 (583.0, -912.0, 150.004)
		disable_shadows True
	100106 units/world/props/office/light/spotlight_celing_small/001 (-212.0, -607.0, 374.0)
		disable_shadows True
		mesh_variation light_on
	100243 units/world/props/office/light/spotlight_celing_small/002 (-437.0, -607.0, 374.0)
		disable_shadows True
	100441 units/world/props/office/light/spotlight_celing_small/003 (-437.0, -182.0, 374.0)
		disable_shadows True
	100245 units/world/props/office/light/spotlight_celing_small/004 (113.0, -817.0, 339.0)
		disable_shadows True
		mesh_variation light_off
	100246 units/world/props/office/light/spotlight_celing_small/005 (313.0, -817.0, 339.0)
		disable_shadows True
	100247 units/world/props/office/light/spotlight_celing_small/006 (488.0, -817.0, 339.0)
		disable_shadows True
	100248 units/world/props/office/light/spotlight_celing_small/007 (413.0, -1032.0, 339.0)
		disable_shadows True
		mesh_variation light_off
	100249 units/world/props/office/light/spotlight_celing_small/008 (188.0, -1032.0, 339.0)
		disable_shadows True
	100250 units/world/props/office/light/spotlight_celing_small/009 (-437.0, -1432.0, 374.0)
		disable_shadows True
	100251 units/world/props/office/light/spotlight_celing_small/010 (-212.0, -1432.0, 374.0)
		disable_shadows True
		mesh_variation light_on
	100442 units/world/props/office/light/spotlight_celing_small/011 (-212.0, -182.0, 374.0)
		disable_shadows True
		mesh_variation light_on
	100214 units/world/props/oil_stains_decal_01/002 (251.335, -870.866, 0.0)
		disable_shadows True
	100220 units/world/props/oil_stains_decal_01/003 (471.0, -642.0, 11.0)
		disable_shadows True
	100232 units/world/props/oil_stains_decal_01/004 (-476.0, -1040.0, 0.0)
		disable_shadows True
	100233 units/world/props/oil_stains_decal_01/005 (-117.0, -360.0, 0.0)
		disable_shadows True
	100234 units/world/props/oil_stains_decal_01/006 (-11.0, -980.0, 155.0)
		disable_shadows True
	100207 units/world/props/oil_stains_decal_02/001 (68.0, -1109.0, 0.0)
		disable_shadows True
	100208 units/world/props/oil_stains_decal_02/002 (307.0, -1109.0, 0.0)
		disable_shadows True
	100209 units/world/props/oil_stains_decal_02/003 (236.0, -1109.0, 0.0)
		disable_shadows True
	100221 units/world/props/oil_stains_decal_02/004 (417.0, -620.0, 10.0)
		disable_shadows True
	100098 units/world/props/oil_stains_decal_02/005 (258.0, -1138.0, 110.472)
		disable_shadows True
	100325 units/world/props/oil_stains_decal_02/006 (540.0, -946.0, 0.00405884)
		disable_shadows True
	100210 units/world/props/oil_stains_decal_03/001 (77.0, -1167.0, 0.0)
		disable_shadows True
	100212 units/world/props/oil_stains_decal_03/002 (61.0, -784.0, 0.0)
		disable_shadows True
	100213 units/world/props/oil_stains_decal_03/003 (545.0, -818.0, 0.0)
		disable_shadows True
	100222 units/world/props/oil_stains_decal_03/004 (196.0, -685.0, 0.0)
		disable_shadows True
	100223 units/world/props/oil_stains_decal_03/005 (432.0, -662.0, 0.0)
		disable_shadows True
	100224 units/world/props/oil_stains_decal_03/006 (141.0, -313.0, 0.0)
		disable_shadows True
	100229 units/world/props/oil_stains_decal_03/007 (359.0, -290.0, 0.0)
		disable_shadows True
	100230 units/world/props/oil_stains_decal_03/008 (-465.0, -162.0, 0.0)
		disable_shadows True
	100231 units/world/props/oil_stains_decal_03/009 (-465.0, -570.0, 0.0)
		disable_shadows True
	100399 units/world/props/suburbia_newspaper/001 (-61.0, -937.0, 115.004)
		disable_shadows True
	100424 units/world/props/suburbia_newspaper/002 (-295.0, -1501.0, -1.996)
		disable_shadows True
	100374 units/world/props/survivalist/box/cardboard_box02/001 (672.0, -1040.0, -24.996)
		disable_shadows True
	100373 units/world/props/survivalist/box/cardboard_box03/001 (663.0, -1280.0, -22.996)
		disable_shadows True
	100372 units/world/props/survivalist/box/cardboard_box03/002 (663.0, -1023.0, 30.004)
		disable_shadows True
