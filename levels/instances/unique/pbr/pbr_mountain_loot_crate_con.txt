ID range vs continent name:
	100000: world

statics
	100008 units/payday2/pickups/gen_pku_gold/001 (-35.0, 20.0, 70.0)
		mesh_variation hide
	100009 units/payday2/pickups/gen_pku_money/001 (0.0, 0.0, 70.0)
		mesh_variation hide
	100000 units/pd2_dlc_berry/props/bry_prop_crate_wood_murkywater/loot_crate (0.0, 0.0, 0.0)
		mesh_variation disable_interaction
	100001 units/world/props/mansion/man_cover_int_shipping_crate_wide/generic_crate (0.0, 0.0, 0.0)
