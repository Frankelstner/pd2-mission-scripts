﻿´startup´ MissionScriptElement 100043
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Execute sequence: "state_vis_hide" - units/payday2/equipment/ind_interactable_door_reinforced/001 (-595.0, 428.0, -76.0)
	Add nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-625.0, 325.0, -100.0)
	Hide object: core/units/light_omni/001 (101.0, 466.0, 10.0)
	Hide object: units/payday2/props/com_prop_vent_ceiling/001 (-250.0, 350.0, 225.0)
´done_opened_door´ ElementUnitSequenceTrigger 100053
	TRIGGER TIMES 1
	Upon sequence "done_opened" - units/payday2/equipment/ind_interactable_door_reinforced/001 (-595.0, 428.0, -76.0):
		Remove nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-625.0, 325.0, -100.0)
		remove ´WP_goto´
		Send instance event "door_opened" to mission.
		´obj_given_and_door_opened（2）´
´obj_given_and_door_opened（2）´ ElementCounter 100039
	When counter target reached: 
		´WP_safe´
´WP_goto´ ElementWaypoint 100048
	Place waypoint with icon "pd2_goto" at position (-775.0, 375.0, 75.0)
´WP_safe´ ElementWaypoint 100007
	Place waypoint with icon "pd2_drill" at position (53.0, 454.0, 28.0)
´instance_input_the_chosen_one´ ElementInstanceInput 100071
	Upon mission event "the_chosen_one":
		Execute sequence: "state_vis_show" - units/payday2/equipment/ind_interactable_door_reinforced/001 (-595.0, 428.0, -76.0)
		Execute sequence: "state_door_close" - units/payday2/equipment/ind_interactable_door_reinforced/001 (-595.0, 428.0, -76.0)
		Execute sequence: "state_lockpick_off" - units/payday2/equipment/ind_interactable_door_reinforced/001 (-595.0, 428.0, -76.0)
		Execute sequence: "state_lockpick_off" - units/payday2/equipment/ind_interactable_door_reinforced/001 (-595.0, 428.0, -76.0)
		Show object: units/payday2/props/com_prop_vent_ceiling/001 (-250.0, 350.0, 225.0)
´done_opened´ ElementUnitSequenceTrigger 100018
	TRIGGER TIMES 1
	Upon sequence "done_opened" - units/payday2/equipment/gen_interactable_sec_safe_05x05/001 (77.0, 429.0, 1.0):
		Show object: core/units/light_omni/001 (101.0, 466.0, 10.0)
		remove ´WP_safe´
		remove ´min_police_force´
		Execute sequence: "enable_interaction" - units/pd2_dlc_born/props/bor_prop_garage_bike_assembly/parts/bor_prop_garage_assembly_part_skull/001 (98.0, 461.0, 11.0)
		´WP_mask´
		´teammate_comment_its_finished´
´WP_mask´ ElementWaypoint 100034
	Place waypoint with icon "pd2_generic_interact" at position (88.0, 459.0, 31.0)
´min_police_force´ ElementAreaMinPoliceForce 100107
	Reinforce task: 1 enemy at position (-150.0, 400.0, -75.0)
´teammate_comment_its_finished´ ElementTeammateComment 100015
	TRIGGER TIMES 1
	Make teammate close to instigator say: v07
	Debug message: teammate_comment_its_finished
´instance_input_start_obj_get_inside´ ElementInstanceInput 100046
	Upon mission event "start_obj_get_inside":
		Toggle on: ´interaction_hold_blowtorch001´
		Toggle on: ´interaction_hold_blowtorch002´
		Toggle on: ´interaction_hold_blowtorch003´
		Toggle on: ´area_goto_door´
		´obj_given_and_door_opened（2）´
		´WP_goto´
´interaction_hold_blowtorch001´ ElementInteraction 100056
	DISABLED
	Create interaction object with ID "hold_blow_torch" at position (-600.0, 325.0, -50.0)
	Upon interaction:
		Stop effect: ´effect_blowtorch001´
		remove ´WP_weld001´
		Play audio "weld_lock_break" at position (-600.0, 325.0, -50.0)
		Stop effect: ´effect_blowtorch001´
		´effect_blowtorch001´
		´3（3）´
´interaction_hold_blowtorch002´ ElementInteraction 100057
	DISABLED
	Create interaction object with ID "hold_blow_torch" at position (-600.0, 325.0, 37.0)
	Upon interaction:
		Stop effect: ´effect_blowtorch002´
		remove ´WP_weld002´
		Play audio "weld_lock_break" at position (-600.0, 325.0, 37.0)
		Stop effect: ´effect_blowtorch002´
		´effect_blowtorch002´
		´3（3）´
´interaction_hold_blowtorch003´ ElementInteraction 100058
	DISABLED
	Create interaction object with ID "hold_blow_torch" at position (-600.0, 325.0, 112.0)
	Upon interaction:
		Stop effect: ´effect_blowtorch003´
		remove ´WP_weld003´
		Play audio "weld_lock_break" at position (-600.0, 325.0, 112.0)
		Stop effect: ´effect_blowtorch003´
		´effect_blowtorch003´
		´3（3）´
´area_goto_door´ ElementAreaTrigger 100105
	DISABLED
	TRIGGER TIMES 1
	Box, width 600.0, height 500.0, depth 700.0, at position (-775.0, 375.0, 75.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		remove ´WP_goto´
		´WP_weld001´
		´WP_weld002´
		´WP_weld003´
´effect_blowtorch001´ ElementPlayEffect 100097
	Play effect effects/payday2/particles/equipment/welding_effect_blowtorch at position (-600.0, 325.0, -50.0)
´3（3）´ ElementCounter 100082
	When counter target reached: 
		Execute sequence: "anim_weldsplosion_out" - units/payday2/equipment/ind_interactable_door_reinforced/001 (-595.0, 428.0, -76.0)
		Remove nav obstacle: units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-625.0, 325.0, -100.0)
		remove ´WP_goto´
		Send instance event "door_opened" to mission.
		´obj_given_and_door_opened（2）´
		´teammate_comment_we_are_through´ (DELAY 1)
´WP_weld001´ ElementWaypoint 100089
	Place waypoint with icon "pd2_fire" at position (-600.0, 325.0, -50.0)
´effect_blowtorch002´ ElementPlayEffect 100098
	Play effect effects/payday2/particles/equipment/welding_effect_blowtorch at position (-600.0, 325.0, 37.0)
´WP_weld002´ ElementWaypoint 100090
	Place waypoint with icon "pd2_fire" at position (-600.0, 325.0, 37.0)
´effect_blowtorch003´ ElementPlayEffect 100099
	Play effect effects/payday2/particles/equipment/welding_effect_blowtorch at position (-600.0, 325.0, 112.0)
´WP_weld003´ ElementWaypoint 100091
	Place waypoint with icon "pd2_fire" at position (-600.0, 325.0, 112.0)
´teammate_comment_we_are_through´ ElementTeammateComment 100009
	TRIGGER TIMES 1
	Make teammate close to instigator say: v03
	Debug message: teammate_comment_we_are_through
´cloaker_vent_spawn´ ElementEnemyDummyTrigger 100116
	Upon event "spawn" of ´enemy_cloaker´:
		´open_ceiling´
		´SO_hunt´
´enemy_cloaker´ ElementSpawnEnemyDummy 100113
	Spawn ene_spook_1 with action e_sp_clk_3_5m_dwn_vent.
	No group AI
	Orientations:
		(-225.0, 350.0, -75.0)
´open_ceiling´ ElementUnitSequence 100115
	TRIGGER TIMES 1
	Execute sequence: "release_vent" - units/payday2/props/com_prop_vent_ceiling/001 (-250.0, 350.0, 225.0)
´SO_hunt´ ElementSpecialObjective 100121
	Make person path towards (-100.0, 1400.0, 2.5) and show "None".
	Create objective for someone from any group:
		´enemy_cloaker´
		´enemy_swat001´
		´enemy_swat002´
		´enemy_swat_fbi001´
		´enemy_swat_fbi002´
		´enemy_swat_city001´
		´enemy_swat_city002´
		´enemy_swat004´
		´enemy_swat003´
		´enemy_swat_fbi004´
		´enemy_swat_fbi003´
		´enemy_swat_heavy002´
		´enemy_swat_city005´
		´enemy_swat_city006´
´enemy_swat001´ ElementSpawnEnemyDummy 100123
	Spawn ene_swat_1 with action e_sp_clk_3_5m_dwn_vent.
	Orientations:
		(-225.0, 350.0, -75.0)
´enemy_swat002´ ElementSpawnEnemyDummy 100124
	Spawn ene_swat_2 with action e_sp_clk_3_5m_dwn_vent.
	Orientations:
		(-225.0, 350.0, -75.0)
´enemy_swat_fbi001´ ElementSpawnEnemyDummy 100125
	Spawn ene_fbi_swat_1 with action e_sp_clk_3_5m_dwn_vent.
	Orientations:
		(-225.0, 350.0, -75.0)
´enemy_swat_fbi002´ ElementSpawnEnemyDummy 100130
	Spawn ene_fbi_swat_2 with action e_sp_clk_3_5m_dwn_vent.
	Orientations:
		(-225.0, 350.0, -75.0)
´enemy_swat_city001´ ElementSpawnEnemyDummy 100132
	Spawn ene_city_swat_1 with action e_sp_clk_3_5m_dwn_vent.
	Orientations:
		(-225.0, 350.0, -75.0)
´enemy_swat_city002´ ElementSpawnEnemyDummy 100133
	Spawn ene_city_swat_2 with action e_sp_clk_3_5m_dwn_vent.
	Orientations:
		(-225.0, 350.0, -75.0)
´enemy_swat004´ ElementSpawnEnemyDummy 100155
	Spawn ene_swat_1 with action e_sp_clk_exit_vent_1_5m.
	Orientations:
		(62.0, 100.0, -75.0)
´enemy_swat003´ ElementSpawnEnemyDummy 100151
	Spawn ene_swat_2 with action e_sp_clk_exit_vent_1_5m.
	Orientations:
		(62.0, 100.0, -75.0)
´enemy_swat_fbi004´ ElementSpawnEnemyDummy 100148
	Spawn ene_fbi_swat_1 with action e_sp_clk_exit_vent_1_5m.
	Orientations:
		(62.0, 100.0, -75.0)
´enemy_swat_fbi003´ ElementSpawnEnemyDummy 100131
	Spawn ene_fbi_swat_2 with action e_sp_clk_exit_vent_1_5m.
	Orientations:
		(62.0, 100.0, -75.0)
´enemy_swat_heavy002´ ElementSpawnEnemyDummy 100149
	Spawn ene_swat_heavy_1 with action e_sp_clk_exit_vent_1_5m.
	Orientations:
		(62.0, 100.0, -75.0)
´enemy_swat_city005´ ElementSpawnEnemyDummy 100154
	Spawn ene_city_swat_1 (-1150.0, 1250.0, 0.0) with action e_sp_clk_exit_vent_1_5m.
´enemy_swat_city006´ ElementSpawnEnemyDummy 100156
	Spawn ene_city_swat_2 with action e_sp_clk_exit_vent_1_5m.
	Orientations:
		(62.0, 100.0, -75.0)
´enemy_vent_spawn_ceiling´ ElementEnemyDummyTrigger 100146
	Upon any event:
		spawn: ´enemy_swat001´
		spawn: ´enemy_swat002´
		spawn: ´enemy_swat_fbi001´
		spawn: ´enemy_swat_heavy001´
		spawn: ´enemy_swat_fbi002´
		spawn: ´enemy_swat_city001´
		spawn: ´enemy_swat_city002´
	Execute:
		´open_ceiling´
		´teammate_comment_enemy_in_vent´ (DELAY 2)
		´SO_hunt´ (DELAY 2)
´enemy_swat_heavy001´ ElementSpawnEnemyDummy 100150
	Spawn ene_swat_heavy_1 with action e_sp_clk_3_5m_dwn_vent.
	Orientations:
		(-225.0, 350.0, -75.0)
´teammate_comment_enemy_in_vent´ ElementTeammateComment 100011
	TRIGGER TIMES 1
	Make teammate close to instigator say: v27
	Debug message: teammate_comment_we_are_through
´enemy_vent_spawn_vent´ ElementEnemyDummyTrigger 100162
	Upon any event:
		spawn: ´enemy_swat004´
		spawn: ´enemy_swat003´
		spawn: ´enemy_swat_fbi004´
		spawn: ´enemy_swat_fbi003´
		spawn: ´enemy_swat_heavy002´
		spawn: ´enemy_swat_city004´
		spawn: ´enemy_swat_city006´
	Execute:
		´open_vent´ (DELAY 0.5)
		´teammate_comment_enemy_in_vent´ (DELAY 2)
		´SO_hunt´ (DELAY 2)
´enemy_swat_city004´ ElementSpawnEnemyDummy 100152
	Spawn ene_city_swat_1 with action e_sp_clk_3_5m_dwn_vent.
	No group AI
	Orientations:
		(62.0, 100.0, -75.0)
´open_vent´ ElementUnitSequence 100172
	TRIGGER TIMES 1
	Execute sequence: "release_vent" - units/world/architecture/secret_stash/props/secret_stash_vent_7/001 (104.0, 27.0, 75.0)
´c4_completed´ ElementUnitSequenceTrigger 100020
	TRIGGER TIMES 1
	Upon sequence "c4_completed" - units/payday2/equipment/gen_interactable_sec_safe_05x05/001 (77.0, 429.0, 1.0):
		Toggle off: ´teammate_comment_its_finished´
´if_broken´ ElementUnitSequenceTrigger 100205
	TRIGGER TIMES 1
	Upon sequence "kill_light_bulb" - units/world/props/apartment/lightbulb/hanging_lightbulb2/001 (-159.0, 150.0, 225.0):
		Hide object: units/payday2/props/gen_prop_glow_neon_04/001 (-159.0, 150.0, 150.0)
		Hide object: units/lights/light_omni_shadow_projection_01/001 (-159.0, 150.0, 125.0)
´death´ ElementEnemyDummyTrigger 100013
	Upon any event:
		fled: ´enemy_swat004´
		fled: ´enemy_swat003´
		fled: ´enemy_swat_fbi004´
		fled: ´enemy_swat_fbi003´
		fled: ´enemy_swat_heavy002´
		fled: ´enemy_swat_city004´
		fled: ´enemy_swat_city006´
		fled: ´enemy_swat001´
		fled: ´enemy_swat002´
		fled: ´enemy_swat_fbi001´
		fled: ´enemy_swat_fbi002´
		fled: ´enemy_swat_heavy001´
		fled: ´enemy_swat_city003´
		fled: ´enemy_swat_city002´
	Execute:
		´2（2）´
´enemy_swat_city003´ ElementSpawnEnemyDummy 100153
	Spawn ene_city_swat_1 with action e_sp_clk_3_5m_dwn_vent.
	No group AI
	Orientations:
		(-225.0, 350.0, -75.0)
´2（2）´ ElementCounter 100014
	When counter target reached: 
		´loop´
		´loop_2´
		´2（2）´ = 2 (DELAY 1)
´loop´ MissionScriptElement 100170
	TRIGGER TIMES 3
	´spawn_ceilings´ (DELAY 25-30)
´loop_2´ MissionScriptElement 100169
	TRIGGER TIMES 3
	´spawn_vents´ (DELAY 25-30)
´spawn_ceilings´ MissionScriptElement 100164
	If Normal:
		Execute 1: (DELAY 0-5)
			´enemy_swat001´ (-750.0, 1700.0, 0.0)
			´enemy_swat002´ (-750.0, 1600.0, 0.0)
			´enemy_swat_heavy001´ (-750.0, 1350.0, 0.0)
	If Hard, Very Hard, Overkill:
		Execute 1: (DELAY 0-5)
			´enemy_swat_fbi001´ (-750.0, 1500.0, 0.0)
			´enemy_swat_fbi002´ (-750.0, 1450.0, 0.0)
	If Mayhem, Death Wish, One Down:
		Execute 1: (DELAY 5)
			´enemy_swat_city001´ (-750.0, 1250.0, 0.0)
			´enemy_swat_city002´ (-750.0, 1200.0, 0.0)
´spawn_vents´ MissionScriptElement 100163
	If Normal:
		Execute 1: (DELAY 0-5)
			´enemy_swat004´ (-1150.0, 1650.0, 0.0)
			´enemy_swat003´ (-1150.0, 1600.0, 0.0)
			´enemy_swat_heavy002´ (-1150.0, 1350.0, 0.0)
	If Hard, Very Hard, Overkill:
		Execute 1: (DELAY 0-5)
			´enemy_swat_fbi004´ (-1150.0, 1500.0, 0.0)
			´enemy_swat_fbi003´ (-1150.0, 1450.0, 0.0)
	If Mayhem, Death Wish, One Down:
		Execute 1: (DELAY 0-5)
			´enemy_swat_city004´ (-1150.0, 1250.0, 0.0)
			´enemy_swat_city006´ (-1150.0, 1200.0, 0.0)
´death_2´ ElementEnemyDummyTrigger 100063
	Upon any event:
		death: ´enemy_swat001´
		death: ´enemy_swat002´
		death: ´enemy_swat_fbi001´
		death: ´enemy_swat_heavy001´
		death: ´enemy_swat_fbi002´
		death: ´enemy_swat_city001´
		death: ´enemy_swat_city002´
		death: ´enemy_swat004´
		death: ´enemy_swat003´
		death: ´enemy_swat_fbi004´
		death: ´enemy_swat_fbi003´
		death: ´enemy_swat_heavy002´
		death: ´enemy_swat_city004´
		death: ´enemy_swat_city006´
	Execute:
		´2（2）´
´area_player_inside´ ElementAreaTrigger 100128
	TRIGGER TIMES 1
	Box, width 500.0, height 300.0, depth 500.0, at position (-125.0, 350.0, 50.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		´min_police_force´
		´spawn_enemies´
´spawn_enemies´ MissionScriptElement 100119
	TRIGGER TIMES 1
	If Hard, Very Hard, Overkill, Mayhem, Death Wish, One Down:
		´enemy_cloaker´ (0.0, 1400.0, 0.0) (DELAY 0-5)
	´SO_destination001´
	´SO_destination002´
	´SO_destination005´
	´SO_destination004´
	´SO_destination003´
	´SO_destination_tank´
	´SO_destination_defend´
	´spawn_ceilings´ (DELAY 5)
	´spawn_vents´ (DELAY 5-10)
´SO_destination001´ ElementSpecialObjective 100120
	Create objective for enemies in 4000 radius around (-159.767, 436.131, -75.0)
	Make chosen person path towards objective and show "None". Afterwards, choose new objective: ´SO_destination_defend´
´SO_destination_defend´ ElementSpecialObjective 100144
	Create objective for enemies in 4000 radius around (-141.798, 248.785, -75.0)
	Make chosen person path towards objective and show "AI_defend".
´SO_destination002´ ElementSpecialObjective 100140
	Create objective for enemies in 4000 radius around (-159.767, 436.131, -75.0)
	Make chosen person path towards objective and show "None". Afterwards, choose new objective: ´SO_destination_defend´
´SO_destination005´ ElementSpecialObjective 100143
	Create objective for enemies in 4000 radius around (-159.767, 436.131, -75.0)
	Make chosen person path towards objective and show "None". Afterwards, choose new objective: ´SO_destination_defend´
´SO_destination004´ ElementSpecialObjective 100142
	Create objective for enemies in 4000 radius around (-159.767, 436.131, -75.0)
	Make chosen person path towards objective and show "None". Afterwards, choose new objective: ´SO_destination_defend´
´SO_destination003´ ElementSpecialObjective 100141
	Create objective for enemies in 4000 radius around (-159.767, 436.131, -75.0)
	Make chosen person path towards objective and show "None". Afterwards, choose new objective: ´SO_destination_defend´
´SO_destination_tank´ ElementSpecialObjective 100139
	Create objective for enemies in 4000 radius around (-141.798, 248.785, -75.0)
	Make chosen person path towards objective and show "None". Afterwards, choose new objective: ´SO_destination_defend´
´interacted_with_skull´ ElementUnitSequenceTrigger 100037
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_born/props/bor_prop_garage_bike_assembly/parts/bor_prop_garage_assembly_part_skull/001 (98.0, 461.0, 11.0):
		remove ´WP_mask´
		Hide object: core/units/light_omni/001 (101.0, 466.0, 10.0)
		Hide object: units/pd2_dlc_born/props/bor_prop_garage_bike_assembly/parts/bor_prop_garage_assembly_part_skull/001 (98.0, 461.0, 11.0)
		Send instance event "got_a_part" to mission.
		Give 1 chrome_skull to instigator.
		Toggle off: ´SO_destination001´
		Toggle off: ´SO_destination005´
		Toggle off: ´SO_destination002´
		Toggle off: ´SO_destination004´
		Toggle off: ´SO_destination003´
		Toggle off: ´SO_destination_tank´
		Toggle off: ´SO_destination_defend´
		Toggle off: ´spawn_vents´
		Toggle off: ´loop_2´
		Toggle off: ´spawn_ceilings´
		Toggle off: ´loop´
		Toggle off: ´death´
		Toggle off: ´death_2´
		Toggle off: ´2（2）´
