ID range vs continent name:
	100000: world

statics
	100072 units/dev_tools/level_tools/door_blocker_1m/001 (-110.0, 227.0, 0.0)
		disable_on_ai_graph True
	100000 units/payday2/equipment/hlm_interactable_win_planks/001 (145.0, 225.0, 190.0)
		mesh_variation enable_interaction
	100003 units/payday2/equipment/hlm_interactable_win_planks/002 (-83.0, -655.0, 188.0)
		mesh_variation enable_interaction
	100043 units/pd2_dlc1/architecture/res_int_motel_window_01/001 (211.0, 224.0, 73.0)
	100046 units/pd2_dlc1/architecture/res_int_motel_window_01/002 (-151.999, -656.0, 74.0003)
	100054 units/pd2_dlc1/equipment/gen_interactable_door_wooden_c/001 (-49.0, 231.0, 0.0)
		hide_on_projection_light True
		mesh_variation state_door_open
	100057 units/pd2_dlc2/props/gen_prop_swat_tripmine_shootable/001 (-58.0, 241.0, 45.0)
