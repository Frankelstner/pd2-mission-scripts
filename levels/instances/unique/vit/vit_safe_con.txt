ID range vs continent name:
	100000: world

statics
	100239 units/pd2_dlc_bph/equipment/gen_interactable_powered_drill (-6.0, 58.0, 134.0)
	100016 units/pd2_dlc_vit/equipment/vit_interactable_button/button_000 (-3.0, 30.0, 134.0)
		mesh_variation set_button_0
	100007 units/pd2_dlc_vit/equipment/vit_interactable_button/button_001 (-3.0, 35.0, 149.0)
		mesh_variation set_button_01
	100008 units/pd2_dlc_vit/equipment/vit_interactable_button/button_002 (-3.0, 30.0, 149.0)
		mesh_variation set_button_02
	100009 units/pd2_dlc_vit/equipment/vit_interactable_button/button_003 (-3.0, 25.0, 149.0)
		mesh_variation set_button_03
	100010 units/pd2_dlc_vit/equipment/vit_interactable_button/button_004 (-3.0, 35.0, 144.0)
		mesh_variation set_button_04
	100011 units/pd2_dlc_vit/equipment/vit_interactable_button/button_005 (-3.0, 30.0, 144.0)
		mesh_variation set_button_05
	100012 units/pd2_dlc_vit/equipment/vit_interactable_button/button_006 (-3.0, 25.0, 144.0)
		mesh_variation set_button_06
	100013 units/pd2_dlc_vit/equipment/vit_interactable_button/button_007 (-3.0, 35.0, 139.0)
		mesh_variation set_button_07
	100014 units/pd2_dlc_vit/equipment/vit_interactable_button/button_008 (-3.0, 30.0, 139.0)
		mesh_variation set_button_08
	100015 units/pd2_dlc_vit/equipment/vit_interactable_button/button_009 (-3.0, 25.0, 139.0)
		mesh_variation set_button_09
	100017 units/pd2_dlc_vit/equipment/vit_interactable_button/button_c (-3.0, 35.0, 134.0)
		mesh_variation set_button_clear
	100018 units/pd2_dlc_vit/equipment/vit_interactable_button/button_e (-3.0, 25.0, 134.0)
		mesh_variation set_button_enter
	100000 units/pd2_dlc_vit/equipment/vit_interactable_sec_safe/001 (-1.14441e-05, 0.0, 125.0)
	100295 units/pd2_dlc_vit/props/vit_prop_presidential_keycard/001 (13.1433, 36.6375, 130.004)
