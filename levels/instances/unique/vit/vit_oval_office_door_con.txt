ID range vs continent name:
	100000: world

statics
	100009 units/dev_tools/level_tools/dev_door_blocker_1x1x3/001 (-64.0, -50.0, 0.0)
	100013 units/pd2_dlc_cro/lockpick_interactable/001 (-62.0, -19.0, 89.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100014 units/pd2_dlc_cro/thermite_interactable/001 (-16.0, -24.0, 107.0)
		disable_on_ai_graph True
		disable_shadows True
		hide_on_projection_light True
	100006 units/pd2_dlc_vit/props/vit_prop_oval_office_wall_door/001 (50.0, 0.0, 0.0)
		disable_on_ai_graph True
		disable_shadows True
		mesh_variation door_open
