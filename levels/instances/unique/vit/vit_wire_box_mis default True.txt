﻿´startup´ ElementUnitSequence 100032
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 0.5)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/props/vit_prop_hacking_device/001 (-17.8776, 33.3936, 48.0)
	Execute sequence: "enable_interaction" - units/pd2_dlc_vit/props/vit_prop_cable_box/001 (4.00543e-05, -6.48499e-05, 50.0)
´set_red_wire_input´ ElementInstanceInput 100001
	Upon mission event "set_red_wire":
		Execute sequence: "set_cable_color_red" - units/pd2_dlc_vit/props/vit_prop_cable_box/001 (4.00543e-05, -6.48499e-05, 50.0)
		Toggle on: ´red_wire_cut´
		´logic_counter_001（3）´
´red_wire_cut´ ElementInstanceOutput 100012
	DISABLED
	Send instance event "red_wire_cut" to mission.
´logic_counter_001（3）´ ElementCounter 100091
	When counter target reached: 
		Execute sequence: "set_cable_interaction_red" - units/pd2_dlc_vit/props/vit_prop_cable_box/001 (4.00543e-05, -6.48499e-05, 50.0)
´set_green_wire_input´ ElementInstanceInput 100002
	Upon mission event "set_green_wire":
		Execute sequence: "set_cable_color_green" - units/pd2_dlc_vit/props/vit_prop_cable_box/001 (4.00543e-05, -6.48499e-05, 50.0)
		Toggle on: ´green_wire_cut´
		´logic_counter_002（3）´
´green_wire_cut´ ElementInstanceOutput 100011
	DISABLED
	Send instance event "green_wire_cut" to mission.
´logic_counter_002（3）´ ElementCounter 100092
	When counter target reached: 
		Execute sequence: "set_cable_interaction_green" - units/pd2_dlc_vit/props/vit_prop_cable_box/001 (4.00543e-05, -6.48499e-05, 50.0)
´set_blue_wire_input´ ElementInstanceInput 100003
	Upon mission event "set_blue_wire":
		Execute sequence: "set_cable_color_blue" - units/pd2_dlc_vit/props/vit_prop_cable_box/001 (4.00543e-05, -6.48499e-05, 50.0)
		Toggle on: ´blue_wire_cut´
		´logic_counter_003（3）´
´blue_wire_cut´ ElementInstanceOutput 100013
	DISABLED
	Send instance event "blue_wire_cut" to mission.
´logic_counter_003（3）´ ElementCounter 100093
	When counter target reached: 
		Execute sequence: "set_cable_interaction_blue" - units/pd2_dlc_vit/props/vit_prop_cable_box/001 (4.00543e-05, -6.48499e-05, 50.0)
´set_yellow_wire_input´ ElementInstanceInput 100004
	Upon mission event "set_yellow_wire":
		Execute sequence: "set_cable_color_yellow" - units/pd2_dlc_vit/props/vit_prop_cable_box/001 (4.00543e-05, -6.48499e-05, 50.0)
		Toggle on: ´yellow_wire_cut´
		´logic_counter_004（3）´
´yellow_wire_cut´ ElementInstanceOutput 100014
	DISABLED
	Send instance event "yellow_wire_cut" to mission.
´logic_counter_004（3）´ ElementCounter 100094
	When counter target reached: 
		Execute sequence: "set_cable_interaction_yellow" - units/pd2_dlc_vit/props/vit_prop_cable_box/001 (4.00543e-05, -6.48499e-05, 50.0)
´enable_interaction_input´ ElementInstanceInput 100009
	Upon mission event "enable_interaction":
		´2（2）´
´2（2）´ ElementCounter 100057
	When counter target reached: 
		´enable_interaction´ (DELAY 0.5)
´enable_interaction´ ElementUnitSequence 100010
	Execute sequence: "enable_interaction_cut_wires" - units/pd2_dlc_vit/props/vit_prop_cable_box/001 (4.00543e-05, -6.48499e-05, 50.0)
	Debug message: from instance: enable_interaction
	´logic_counter_001（3）´
	´logic_counter_002（3）´
	´logic_counter_003（3）´
	´logic_counter_004（3）´
´wire_cut´ ElementUnitSequenceTrigger 100019
	TRIGGER TIMES 1
	Upon sequence "done_cable_cut" - units/pd2_dlc_vit/props/vit_prop_cable_box/001 (4.00543e-05, -6.48499e-05, 50.0):
		Debug message: from instance: wire_cut
		Play effect effects/payday2/particles/electric/electric_sparks_small at position (-12.0, -1.0, 60.0)
		´red_wire_cut´
		´green_wire_cut´
		´blue_wire_cut´
		´yellow_wire_cut´
´box_open´ ElementUnitSequenceTrigger 100020
	TRIGGER TIMES 1
	Upon sequence "open_box" - units/pd2_dlc_vit/props/vit_prop_cable_box/001 (4.00543e-05, -6.48499e-05, 50.0):
		Debug message: from instance: box_open
		Send instance event "opened_a_box" to mission.
		´logic_counter_001（3）´
		´logic_counter_002（3）´
		´logic_counter_003（3）´
		´logic_counter_004（3）´
		´2（2）´ (DELAY 1)
		´box_open_and_loud_and_gate_seen（3）´ (DELAY 1.5)
´box_open_and_loud_and_gate_seen（3）´ ElementCounter 100029
	When counter target reached: 
		Debug message: box_open_and_loud_and_gate_seen
		´start_hack´
		´start_incorrect_hack´
´start_hack´ MissionScriptElement 100027
	DISABLED
	´enable_device_interaction´
	´disable_cable_interaction´
	´2（2）_2´
´start_incorrect_hack´ MissionScriptElement 100065
	´disable_cable_interaction´
´enable_device_interaction´ ElementUnitSequence 100021
	TRIGGER TIMES 1
	Execute sequence: "enable_interaction" - units/pd2_dlc_vit/props/vit_prop_hacking_device/001 (-17.8776, 33.3936, 48.0)
	Debug message: from instance: enable_device_interaction
´disable_cable_interaction´ ElementUnitSequence 100055
	TRIGGER TIMES 1
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/props/vit_prop_cable_box/001 (4.00543e-05, -6.48499e-05, 50.0)
	Toggle off: ´enable_interaction´
	Debug message: from instance: disable_cable_interaction
´2（2）_2´ ElementCounter 100069
´police_called´ ElementGlobalEventTrigger 100022
	TRIGGER TIMES 1
	Upon global event "police_called":
		Debug message: from instance: police_called
		´box_open_and_loud_and_gate_seen（3）´
´timer（120）´ ElementTimer 100045
	Timer: 120
´done´ ElementTimerTrigger 100048
	Upon timer "´timer（120）´ == 0:
		´output_device_done´
´output_device_done´ ElementInstanceOutput 100035
	TRIGGER TIMES 1
	Send instance event "device_done" to mission.
	remove ´WP_defend´
´WP_defend´ ElementWaypoint 100074
	Place waypoint with icon "pd2_defend" at position (-34.0, 22.0, 42.0)
´complete´ ElementSpecialObjectiveTrigger 100049
	Upon special objective event "complete" - ´SO_interact´:
		Pause timer: ´timer（120）´
		Execute sequence: "state_device_jammed" - units/pd2_dlc_vit/props/vit_prop_hacking_device/001 (-17.8776, 33.3936, 48.0)
		remove ´WP_defend´
		Send instance event "enemy_interrupted" to mission.
		´WP_fix´ (DELAY 1)
´SO_interact´ ElementSpecialObjective 100043
	Make chosen person path towards (-100.0, -75.0, 0.00400543) and show "e_so_low_kicks".
´WP_fix´ ElementWaypoint 100050
	Place waypoint with icon "pd2_fix" at position (-34.0, 22.0, 42.0)
´fail´ ElementSpecialObjectiveTrigger 100054
	Upon any special objective event:
		"fail" - ´SO_interact´
		"fail" - ´SO_none´
	Execute:
		´start_interrupt´
´SO_none´ ElementSpecialObjective 100036
	Create objective for enemies in 2000 radius around (-100.0, -75.0, 0.00400543)
	Make chosen person path towards objective and show "None". Afterwards, choose new objective: ´SO_interact´
´start_interrupt´ MissionScriptElement 100044
	Debug message: from instance: start interrupt
	´SO_none´ (DELAY 15-20)
´input_chosen_one´ ElementInstanceInput 100066
	Upon mission event "chosen_one":
		Toggle: ´start_incorrect_hack´
		Toggle: ´start_hack´
		´2（2）_2´
´placed_device´ ElementUnitSequenceTrigger 100025
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_vit/props/vit_prop_hacking_device/001 (-17.8776, 33.3936, 48.0):
		Start timer: ´timer（120）´
		´device_placed´
		´start_interrupt´
´device_placed´ MissionScriptElement 100033
	BASE DELAY  (DELAY 1)
	TRIGGER TIMES 1
	Send instance event "device_placed" to mission.
	´WP_defend´
	Toggle on: ´fixed_device´ (DELAY 1)
´fixed_device´ ElementUnitSequenceTrigger 100024
	DISABLED
	Upon sequence "interact" - units/pd2_dlc_vit/props/vit_prop_hacking_device/001 (-17.8776, 33.3936, 48.0):
		Start timer: ´timer（120）´
		´device_fixed´
´device_fixed´ MissionScriptElement 100052
	remove ´WP_fix´
	Execute sequence: "state_device_resumed" - units/pd2_dlc_vit/props/vit_prop_hacking_device/001 (-17.8776, 33.3936, 48.0)
	Debug message: device_fixed
	´start_interrupt´
	´WP_defend´ (DELAY 1)
´10s´ ElementTimerTrigger 100075
	Upon timer "´timer（120）´ == 10:
		Toggle off: ´start_interrupt´
		Toggle off: ´start_hack´
		Toggle off: ´complete´
		Toggle off: ´fail´
		Toggle off: ´SO_none´
		Toggle off: ´SO_interact´
		Toggle off: ´device_fixed´
		remove ´SO_none´
		remove ´SO_interact´
´input_disable_interaction´ ElementInstanceInput 100058
	Upon mission event "disable_interaction":
		Toggle off: ´start_interrupt´
		Toggle off: ´start_hack´
		Toggle off: ´complete´
		Toggle off: ´fail´
		Toggle off: ´SO_none´
		Toggle off: ´SO_interact´
		Toggle off: ´device_fixed´
		remove ´SO_none´
		remove ´SO_interact´
		remove ´WP_defend´
		remove ´WP_fix´
		Execute sequence: "disable_interaction" - units/pd2_dlc_vit/props/vit_prop_cable_box/001 (4.00543e-05, -6.48499e-05, 50.0)
		Execute sequence: "disable_interaction" - units/pd2_dlc_vit/props/vit_prop_hacking_device/001 (-17.8776, 33.3936, 48.0)
´timer_done´ ElementUnitSequenceTrigger 100061
	TRIGGER TIMES 1
	Upon sequence "timer_done" - units/pd2_dlc_vit/props/vit_prop_hacking_device/001 (-17.8776, 33.3936, 48.0):
		Toggle off: ´SO_interact´
		Toggle off: ´SO_none´
		Toggle off: ´start_interrupt´
		Toggle off: ´2（2）_2´
		Toggle off: ´fail´
		Toggle off: ´disable_cable_interaction´
		Toggle off: ´enable_device_interaction´
		Toggle off: ´start_incorrect_hack´
		Toggle off: ´start_hack´
		´output_device_done´
´point_lookat_trigger_001´ ElementLookAtTrigger 100078
	TRIGGER TIMES 1
	Upon player looking in direction (0.0, 0.0, -0.707107, -0.707107) from position (-21.0, 0.0, 53.0):
		Send instance event "wirebox_found" to mission.
´rfid_open´ ElementInstanceInput 100090
	Upon mission event "rfid_open":
		´box_open_and_loud_and_gate_seen（3）´
