﻿´startup´ MissionScriptElement 100000
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Execute sequence: "state_interaction_disable" - units/pd2_dlc_arena/props/are_prop_security_button/001 (-38.0, 24.0, 117.0)
	Execute sequence: "state_interaction_disable" - units/pd2_dlc_arena/props/are_prop_security_button/002 (55.0, 24.0, 114.0)
	Execute sequence: "state_interaction_disable" - units/pd2_dlc_arena/props/are_prop_security_button/003 (-65.0, 24.0, 158.0)
	Execute sequence: "state_interaction_disable" - units/pd2_dlc_arena/props/are_prop_security_button/004 (74.0, 24.0, 159.0)
	Execute sequence: "state_interaction_disable" - units/pd2_dlc_arena/props/are_prop_security_button/005 (-49.0, 24.0, 197.0)
	Execute sequence: "state_interaction_disable" - units/pd2_dlc_arena/props/are_prop_security_button/006 (61.0, 24.0, 195.0)
	Hide object: units/pd2_dlc_arena/props/are_prop_security_button/001 (-38.0, 24.0, 117.0)
	Hide object: units/pd2_dlc_arena/props/are_prop_security_button/002 (55.0, 24.0, 114.0)
	Hide object: units/pd2_dlc_arena/props/are_prop_security_button/003 (-65.0, 24.0, 158.0)
	Hide object: units/pd2_dlc_arena/props/are_prop_security_button/004 (74.0, 24.0, 159.0)
	Hide object: units/pd2_dlc_vit/architecture/vit_int/trims/vit_int_wall_book_shelf_1_m/001 (0.0, 0.0, 0.0)
	Hide object: units/pd2_dlc_arena/props/are_prop_security_button/005 (-49.0, 24.0, 197.0)
	Hide object: units/pd2_dlc_arena/props/are_prop_security_button/006 (61.0, 24.0, 195.0)
´input_enable_bookshelf_interactions´ ElementInstanceInput 100007
	Upon mission event "enable_bookshelf_interactions":
		´enable_2m_interactions´
		´enable_1m_interactions´
´enable_2m_interactions´ ElementUnitSequence 100014
	Execute sequence: "enable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/001 (-53.0, 20.0, 96.0)
	Execute sequence: "enable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_04/001 (-48.0, 18.0, 139.0)
	Execute sequence: "enable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/002 (-45.0, 19.0, 181.0)
	Execute sequence: "enable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/001 (64.0, 17.0, 96.0)
	Execute sequence: "enable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_03/001 (75.0, 20.0, 138.0)
	Execute sequence: "enable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/002 (62.0, 22.0, 181.0)
´enable_1m_interactions´ ElementUnitSequence 100023
	DISABLED
	Execute sequence: "enable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/001 (64.0, 17.0, 96.0)
	Execute sequence: "enable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_03/001 (75.0, 20.0, 138.0)
	Execute sequence: "enable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/002 (62.0, 22.0, 181.0)
´input_set_keycard_reader´ ElementInstanceInput 100053
	Upon mission event "set_keycard_reader":
		Execute 1:
			´relay´
			´relay_2´
			´relay_3´
			´relay_4´
			´relay001´
			´relay002´
´relay´ MissionScriptElement 100049
	Show object: units/pd2_dlc_arena/props/are_prop_security_button/001 (-38.0, 24.0, 117.0)
	Toggle on: ´enable_interaction_keycard_reader001´
´relay_2´ MissionScriptElement 100050
	Show object: units/pd2_dlc_arena/props/are_prop_security_button/002 (55.0, 24.0, 114.0)
	Toggle on: ´enable_interaction_keycard_reader002´
´relay_3´ MissionScriptElement 100051
	Show object: units/pd2_dlc_arena/props/are_prop_security_button/003 (-65.0, 24.0, 158.0)
	Toggle on: ´enable_interaction_keycard_reader003´
´relay_4´ MissionScriptElement 100052
	Show object: units/pd2_dlc_arena/props/are_prop_security_button/004 (74.0, 24.0, 159.0)
	Toggle on: ´enable_interaction_keycard_reader004´
´relay001´ MissionScriptElement 100032
	Show object: units/pd2_dlc_arena/props/are_prop_security_button/005 (-49.0, 24.0, 197.0)
	Toggle on: ´enable_interaction_keycard_reader005´
´relay002´ MissionScriptElement 100033
	Show object: units/pd2_dlc_arena/props/are_prop_security_button/006 (61.0, 24.0, 195.0)
	Toggle on: ´enable_interaction_keycard_reader006´
´enable_interaction_keycard_reader001´ ElementUnitSequence 100062
	DISABLED
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_arena/props/are_prop_security_button/001 (-38.0, 24.0, 117.0)
	Send instance event "found_keycard_reader" to mission.
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/001 (-53.0, 20.0, 96.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/002 (-45.0, 19.0, 181.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/001 (64.0, 17.0, 96.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/002 (62.0, 22.0, 181.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_03/001 (75.0, 20.0, 138.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_04/001 (-48.0, 18.0, 139.0)
	Debug message: from instance: found_keycard_reader
	´WP_interact001´
´enable_interaction_keycard_reader002´ ElementUnitSequence 100063
	DISABLED
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_arena/props/are_prop_security_button/002 (55.0, 24.0, 114.0)
	Send instance event "found_keycard_reader" to mission.
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/001 (-53.0, 20.0, 96.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/002 (-45.0, 19.0, 181.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/001 (64.0, 17.0, 96.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/002 (62.0, 22.0, 181.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_03/001 (75.0, 20.0, 138.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_04/001 (-48.0, 18.0, 139.0)
	Debug message: from instance: found_keycard_reader
	´WP_interact002´
´enable_interaction_keycard_reader003´ ElementUnitSequence 100064
	DISABLED
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_arena/props/are_prop_security_button/003 (-65.0, 24.0, 158.0)
	Send instance event "found_keycard_reader" to mission.
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/001 (-53.0, 20.0, 96.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/002 (-45.0, 19.0, 181.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/001 (64.0, 17.0, 96.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/002 (62.0, 22.0, 181.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_03/001 (75.0, 20.0, 138.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_04/001 (-48.0, 18.0, 139.0)
	Debug message: from instance: found_keycard_reader
	´WP_interact003´
´enable_interaction_keycard_reader004´ ElementUnitSequence 100065
	DISABLED
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_arena/props/are_prop_security_button/004 (74.0, 24.0, 159.0)
	Send instance event "found_keycard_reader" to mission.
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/001 (-53.0, 20.0, 96.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/002 (-45.0, 19.0, 181.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/001 (64.0, 17.0, 96.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/002 (62.0, 22.0, 181.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_03/001 (75.0, 20.0, 138.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_04/001 (-48.0, 18.0, 139.0)
	Debug message: from instance: found_keycard_reader
	´WP_interact004´
´enable_interaction_keycard_reader005´ ElementUnitSequence 100101
	DISABLED
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_arena/props/are_prop_security_button/005 (-49.0, 24.0, 197.0)
	Send instance event "found_keycard_reader" to mission.
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/001 (-53.0, 20.0, 96.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/002 (-45.0, 19.0, 181.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/001 (64.0, 17.0, 96.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/002 (62.0, 22.0, 181.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_03/001 (75.0, 20.0, 138.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_04/001 (-48.0, 18.0, 139.0)
	Debug message: from instance: found_keycard_reader
	´WP_interact005´
´enable_interaction_keycard_reader006´ ElementUnitSequence 100102
	DISABLED
	Execute sequence: "state_interaction_enable" - units/pd2_dlc_arena/props/are_prop_security_button/006 (61.0, 24.0, 195.0)
	Send instance event "found_keycard_reader" to mission.
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/001 (-53.0, 20.0, 96.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/002 (-45.0, 19.0, 181.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/001 (64.0, 17.0, 96.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/002 (62.0, 22.0, 181.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_03/001 (75.0, 20.0, 138.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_04/001 (-48.0, 18.0, 139.0)
	Debug message: from instance: found_keycard_reader
	´WP_interact006´
´WP_interact001´ ElementWaypoint 100081
	Place waypoint with icon "pd2_generic_interact" at position (-37.0, -3.0, 116.0)
´WP_interact002´ ElementWaypoint 100082
	Place waypoint with icon "pd2_generic_interact" at position (54.0, -7.0, 114.0)
´WP_interact003´ ElementWaypoint 100083
	Place waypoint with icon "pd2_generic_interact" at position (-62.0, -1.0, 160.0)
´WP_interact004´ ElementWaypoint 100084
	Place waypoint with icon "pd2_generic_interact" at position (73.0, 2.0, 155.0)
´WP_interact005´ ElementWaypoint 100040
	Place waypoint with icon "pd2_generic_interact" at position (-50.0, -0.999997, 203.0)
´WP_interact006´ ElementWaypoint 100041
	Place waypoint with icon "pd2_generic_interact" at position (62.0, 2.0, 198.0)
´input_disable_bookshelf_interactions´ ElementInstanceInput 100039
	Upon mission event "disable_bookshelf_interactions":
		Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/001 (-53.0, 20.0, 96.0)
		Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/002 (-45.0, 19.0, 181.0)
		Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/001 (64.0, 17.0, 96.0)
		Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/002 (62.0, 22.0, 181.0)
		Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_03/001 (75.0, 20.0, 138.0)
		Execute sequence: "disable_interaction" - units/pd2_dlc_vit/equipment/vit_interactable_books_04/001 (-48.0, 18.0, 139.0)
´interacted_with_button´ ElementUnitSequenceTrigger 100069
	TRIGGER TIMES 1
	Upon any sequence:
		"interact" - units/pd2_dlc_arena/props/are_prop_security_button/001 (-38.0, 24.0, 117.0)
		"interact" - units/pd2_dlc_arena/props/are_prop_security_button/002 (55.0, 24.0, 114.0)
		"interact" - units/pd2_dlc_arena/props/are_prop_security_button/003 (-65.0, 24.0, 158.0)
		"interact" - units/pd2_dlc_arena/props/are_prop_security_button/004 (74.0, 24.0, 159.0)
		"interact" - units/pd2_dlc_arena/props/are_prop_security_button/005 (-49.0, 24.0, 197.0)
		"interact" - units/pd2_dlc_arena/props/are_prop_security_button/006 (61.0, 24.0, 195.0)
	Execute:
		Send instance event "used_keycard" to mission.
		remove ´WP_interact001´
		remove ´WP_interact002´
		remove ´WP_interact003´
		remove ´WP_interact004´
		remove ´WP_interact005´
		remove ´WP_interact006´
´input_move_bookshelf_left´ ElementInstanceInput 100072
	Upon mission event "move_bookshelf_left":
		Execute sequence: "open_door_right" - units/pd2_dlc_vit/architecture/vit_int/trims/vit_int_wall_book_shelf_2_m/001 (-100.0, 0.0, 0.0)
		Execute sequence: "make_books_dynamic" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/001 (-53.0, 20.0, 96.0)
		Execute sequence: "make_books_dynamic" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/002 (-45.0, 19.0, 181.0)
		Execute sequence: "make_books_dynamic" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/001 (64.0, 17.0, 96.0)
		Execute sequence: "make_books_dynamic" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/002 (62.0, 22.0, 181.0)
		Execute sequence: "make_books_dynamic" - units/pd2_dlc_vit/equipment/vit_interactable_books_03/001 (75.0, 20.0, 138.0)
		Execute sequence: "make_books_dynamic" - units/pd2_dlc_vit/equipment/vit_interactable_books_04/001 (-48.0, 18.0, 139.0)
		´point_play_sound_001´
´point_play_sound_001´ ElementPlaySound 100025
	TRIGGER TIMES 1
	Play audio "vit_bookshelf_move" at position (0.0, -25.0, 125.0)
´input_move_bookshelf_right´ ElementInstanceInput 100073
	Upon mission event "move_bookshelf_right":
		Execute sequence: "open_door_left" - units/pd2_dlc_vit/architecture/vit_int/trims/vit_int_wall_book_shelf_2_m/001 (-100.0, 0.0, 0.0) (DELAY 1)
		Execute sequence: "make_books_dynamic" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/001 (-53.0, 20.0, 96.0) (DELAY 1)
		Execute sequence: "make_books_dynamic" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/002 (-45.0, 19.0, 181.0) (DELAY 1)
		Execute sequence: "make_books_dynamic" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/001 (64.0, 17.0, 96.0) (DELAY 1)
		Execute sequence: "make_books_dynamic" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/002 (62.0, 22.0, 181.0) (DELAY 1)
		Execute sequence: "make_books_dynamic" - units/pd2_dlc_vit/equipment/vit_interactable_books_03/001 (75.0, 20.0, 138.0) (DELAY 1)
		Execute sequence: "make_books_dynamic" - units/pd2_dlc_vit/equipment/vit_interactable_books_04/001 (-48.0, 18.0, 139.0) (DELAY 1)
		´point_play_sound_001´ (DELAY 1)
´input_switch_to_1m_bookshelf´ ElementInstanceInput 100097
	Upon mission event "switch_to_1m_bookshelf":
		Show object: units/pd2_dlc_vit/architecture/vit_int/trims/vit_int_wall_book_shelf_1_m/001 (0.0, 0.0, 0.0)
		Hide object: units/pd2_dlc_vit/architecture/vit_int/trims/vit_int_wall_book_shelf_2_m/001 (-100.0, 0.0, 0.0)
		Toggle off: ´relay´
		Toggle off: ´relay_3´
		Toggle off: ´relay001´
		Toggle: ´enable_1m_interactions´
		Toggle: ´enable_2m_interactions´
		Execute sequence: "hide" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/001 (-53.0, 20.0, 96.0)
		Execute sequence: "hide" - units/pd2_dlc_vit/equipment/vit_interactable_books_04/001 (-48.0, 18.0, 139.0)
		Execute sequence: "hide" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/002 (-45.0, 19.0, 181.0)
´func_sequence_trigger_001´ ElementUnitSequenceTrigger 100015
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/001 (-53.0, 20.0, 96.0):
		Debug message: from instance: removed books
		´enable_interaction_keycard_reader001´
		Play audio "books_impact_floor" at position (0.0, -25.0, 0.0) (DELAY 0.5)
´func_sequence_trigger_002´ ElementUnitSequenceTrigger 100016
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/001 (64.0, 17.0, 96.0):
		Debug message: from instance: removed books
		´enable_interaction_keycard_reader002´
		Play audio "books_impact_floor" at position (0.0, -25.0, 0.0) (DELAY 0.5)
´func_sequence_trigger_003´ ElementUnitSequenceTrigger 100017
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_vit/equipment/vit_interactable_books_04/001 (-48.0, 18.0, 139.0):
		Debug message: from instance: removed books
		´enable_interaction_keycard_reader003´
		Play audio "books_impact_floor" at position (0.0, -25.0, 0.0) (DELAY 0.7)
´func_sequence_trigger_004´ ElementUnitSequenceTrigger 100018
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_vit/equipment/vit_interactable_books_03/001 (75.0, 20.0, 138.0):
		Debug message: from instance: removed books
		´enable_interaction_keycard_reader004´
		Play audio "books_impact_floor" at position (0.0, -25.0, 0.0) (DELAY 0.7)
´func_sequence_trigger_005´ ElementUnitSequenceTrigger 100020
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_vit/equipment/vit_interactable_books_01/002 (-45.0, 19.0, 181.0):
		Debug message: from instance: removed books
		´enable_interaction_keycard_reader005´
		Play audio "books_impact_floor" at position (0.0, -25.0, 0.0) (DELAY 0.8)
´func_sequence_trigger_006´ ElementUnitSequenceTrigger 100022
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_vit/equipment/vit_interactable_books_02/002 (62.0, 22.0, 181.0):
		Debug message: from instance: removed books
		´enable_interaction_keycard_reader006´
		Play audio "books_impact_floor" at position (0.0, -25.0, 0.0) (DELAY 0.8)
