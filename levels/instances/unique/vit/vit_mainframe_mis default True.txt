﻿´startup´ MissionScriptElement 100000
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	´seq_setup´
´seq_setup´ ElementUnitSequence 100005
	Execute sequence: "state_vis_hide" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
´input_enable_setup_interaction´ ElementInstanceInput 100007
	Upon mission event "enable_setup_interaction":
		´seq_enable_setup_interaction´
´seq_enable_setup_interaction´ ElementUnitSequence 100011
	Execute sequence: "enable_interaction" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	´WP_computer´
´WP_computer´ ElementWaypoint 100018
	Place waypoint with icon "pd2_computer" at position (0.0, 1.0, 165.0)
´set_up_computer´ ElementUnitSequenceTrigger 100013
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0):
		Send instance event "set_up_computer" to mission.
		´seq_startup´
		Toggle on: ´fixed_computer´ (DELAY 1)
´seq_startup´ ElementUnitSequence 100015
	Execute sequence: "anim_bootup" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	´seq_first_hack´ (DELAY 11)
´fixed_computer´ ElementUnitSequenceTrigger 100035
	DISABLED
	Upon sequence "interact" - units/world/architecture/secret_stash/props/secret_stash_hack_keyboard_interaction/001 (-1.0, -28.0, 87.0):
		Debug message: computer_fixed
		´link´
		´output_resumed_computer´
		´seq_resume002´
		´seq_resume003´
		´seq_resume004´
		Toggle on: ´SO_interrupt_none´ (DELAY 1)
		Toggle on: ´SO_interact_mid´ (DELAY 1)
		Toggle on: ´complete´ (DELAY 1)
		Toggle on: ´police_called´ (DELAY 1)
		Debug message: from mainframe instance: enable_interrupt_again (DELAY 1)
		´output_resumed_computer´ (DELAY 1)
´seq_first_hack´ ElementUnitSequence 100017
	Execute sequence: "state_vis_show" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
	Execute sequence: "state_vis_show" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
	Execute sequence: "state_vis_show" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	´hack_start_001´
	´hack_start_002´
	´hack_start_003´
	´SO_interrupt_none´ (DELAY 10-13)
´link´ MissionScriptElement 100034
	TRIGGER TIMES 1
	´seq_resume´
	´remove_WP_fix´
	´WP_computer´ (DELAY 1)
´output_resumed_computer´ ElementInstanceOutput 100045
	Send instance event "resumed_computer" to mission.
´SO_interrupt_none´ ElementSpecialObjective 100021
	DISABLED
	Create objective for enemies in 2000 radius around (0.0, -100.0, 0.0)
	Make chosen person path towards objective and show "None". Afterwards, choose new objective: ´SO_interact_mid´
´SO_interact_mid´ ElementSpecialObjective 100026
	DISABLED
	Make chosen person path towards (0.0, -100.0, 0.0) and show "e_so_interact_mid".
´complete´ ElementSpecialObjectiveTrigger 100028
	Upon special objective event "complete" - ´SO_interact_mid´:
		´enemy_interrupted´
´police_called´ ElementGlobalEventTrigger 100056
	DISABLED
	TRIGGER TIMES 1
	Upon global event "police_called":
		Toggle on: ´SO_interrupt_none´
		Toggle on: ´SO_interact_mid´
		Toggle on: ´complete´
		Toggle on: ´police_called´
		Debug message: from mainframe instance: enable_interrupt_again
		Debug message: from mainframe instance: police_called
		´output_resumed_computer´
		´SO_interact_mid´ (DELAY 1)
		´SO_interrupt_none´ (DELAY 1)
´seq_resume002´ ElementUnitSequence 100075
	Execute sequence: "show_hack_screen" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Execute sequence: "power_on" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
	Execute sequence: "power_on" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
	Execute sequence: "power_on" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	Execute sequence: "state_vis_show" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
	Execute sequence: "state_vis_show" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
	Execute sequence: "state_vis_show" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	Execute sequence: "set_interaction_hacking" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Debug message: RESUMADE HACKJÄVELN
	Toggle on: ´SO_interrupt_none´
	Toggle on: ´SO_interact_mid´
	Toggle on: ´complete´
	Toggle on: ´police_called´
	Debug message: from mainframe instance: enable_interrupt_again
	Toggle off: ´SO_interrupt_none´
	Toggle off: ´SO_interact_mid´
	Toggle off: ´complete´
	´link´
	´output_resumed_computer´
	´remove_interrupt_SOs´
	´resumed_hack_output´
´seq_resume003´ ElementUnitSequence 100076
	DISABLED
	Execute sequence: "show_hack_screen_lock_1_done" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Execute sequence: "power_on" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
	Execute sequence: "power_on" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
	Execute sequence: "power_on" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	Execute sequence: "state_vis_show" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
	Execute sequence: "state_vis_show" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
	Execute sequence: "state_vis_show" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	Execute sequence: "set_interaction_hacking" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Debug message: RESUMADE HACKJÄVELN
	Toggle on: ´SO_interrupt_none´
	Toggle on: ´SO_interact_mid´
	Toggle on: ´complete´
	Toggle on: ´police_called´
	Debug message: from mainframe instance: enable_interrupt_again
	Toggle off: ´SO_interrupt_none´
	Toggle off: ´SO_interact_mid´
	Toggle off: ´complete´
	´link´
	´output_resumed_computer´
	´remove_interrupt_SOs´
	´resumed_hack_output´
´seq_resume004´ ElementUnitSequence 100077
	DISABLED
	Execute sequence: "show_hack_screen_lock_2_done" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Execute sequence: "show_hack_screen_lock_1_done" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Execute sequence: "power_on" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
	Execute sequence: "power_on" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
	Execute sequence: "power_on" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	Execute sequence: "state_vis_show" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
	Execute sequence: "state_vis_show" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
	Execute sequence: "state_vis_show" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	Execute sequence: "set_interaction_hacking" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Debug message: RESUMADE HACKJÄVELN
	Toggle on: ´SO_interrupt_none´
	Toggle on: ´SO_interact_mid´
	Toggle on: ´complete´
	Toggle on: ´police_called´
	Debug message: from mainframe instance: enable_interrupt_again
	Toggle off: ´SO_interrupt_none´
	Toggle off: ´SO_interact_mid´
	Toggle off: ´complete´
	´link´
	´output_resumed_computer´
	´remove_interrupt_SOs´
	´resumed_hack_output´
´hack_start_001´ ElementUnitSequence 100136
	Execute sequence: "set_hack_time_60" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
	Execute sequence: "state_start_hack" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
	Execute sequence: "set_interaction_hacking" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Execute sequence: "show_hack_screen" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Debug message: START HACKJÄVLEN
´hack_start_002´ ElementUnitSequence 100137
	DISABLED
	Execute sequence: "set_hack_time_60" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
	Execute sequence: "state_start_hack" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
	Execute sequence: "show_hack_screen_lock_1_done" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Execute sequence: "set_interaction_hacking" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Debug message: START HACKJÄVLEN
	Send instance event "set_up_computer" to mission.
´hack_start_003´ ElementUnitSequence 100138
	DISABLED
	Execute sequence: "set_hack_time_60" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	Execute sequence: "state_start_hack" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	Execute sequence: "show_hack_screen_lock_1_done" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Execute sequence: "show_hack_screen_lock_2_done" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Execute sequence: "set_interaction_hacking" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Debug message: START HACKJÄVLEN
	Send instance event "set_up_computer" to mission.
´seq_resume´ ElementDebug 100182
	Debug message: resumed_hacking
´remove_WP_fix´ ElementOperator 100037
	remove ´WP_fix´
´WP_fix´ ElementWaypoint 100036
	Place waypoint with icon "pd2_fix" at position (0.0, 1.0, 165.0)
´enemy_interrupted´ MissionScriptElement 100029
	TRIGGER TIMES 1
	Toggle off: ´SO_interrupt_none´
	Toggle off: ´SO_interact_mid´
	Toggle off: ´complete´
	´remove_interrupt_SOs´
	´seq_interrupt´
	´remove_WP_computer´
	´WP_fix´ (DELAY 1)
´remove_interrupt_SOs´ ElementOperator 100031
	remove ´SO_interrupt_none´
	remove ´SO_interact_mid´
	´output_computer_got_interrupted´
´resumed_hack_output´ ElementInstanceOutput 100111
	Send instance event "resumed_hack" to mission.
	remove ´WP_fix´
´seq_interrupt´ ElementUnitSequence 100032
	Execute sequence: "enable_interaction" - units/world/architecture/secret_stash/props/secret_stash_hack_keyboard_interaction/001 (-1.0, -28.0, 87.0)
	Execute sequence: "show_jammed" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
´remove_WP_computer´ ElementOperator 100016
	remove ´WP_computer´
´output_computer_got_interrupted´ ElementInstanceOutput 100047
	Send instance event "computer_got_interrupted" to mission.
´fail´ ElementSpecialObjectiveTrigger 100027
	Upon any special objective event:
		"fail" - ´SO_interrupt_none´
		"fail" - ´SO_interact_mid´
	Execute:
		´SO_interrupt_none´ (DELAY 10-13)
´hack_1_done´ ElementUnitSequenceTrigger 100039
	TRIGGER TIMES 1
	Upon sequence "timer_done" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0):
		Toggle off: ´SO_interrupt_none´
		Toggle off: ´SO_interact_mid´
		Toggle off: ´complete´
		Debug message: hack_done_debug_001 !!!!!!!!!!!!!!!
		´show_hack_done_001´
		´remove_interrupt_SOs´
´show_hack_done_001´ ElementUnitSequence 100133
	Execute sequence: "show_hack_screen_lock_1_done" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	´output_hack_1_done´
´output_hack_1_done´ ElementInstanceOutput 100048
	Send instance event "hack_1_done" to mission.
´hack_2_done´ ElementUnitSequenceTrigger 100040
	TRIGGER TIMES 1
	Upon sequence "timer_done" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0):
		Toggle off: ´SO_interrupt_none´
		Toggle off: ´SO_interact_mid´
		Toggle off: ´complete´
		Debug message: hack_done_debug_002!!!!!!!!!!!!!!!!
		´show_hack_done_002´
		´remove_interrupt_SOs´
´show_hack_done_002´ ElementUnitSequence 100134
	Execute sequence: "show_hack_screen_lock_2_done" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	´output_hack_2_done´
´output_hack_2_done´ ElementInstanceOutput 100049
	Send instance event "hack_2_done" to mission.
´hack_3_done´ ElementUnitSequenceTrigger 100041
	TRIGGER TIMES 1
	Upon sequence "timer_done" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0):
		Debug message: hack_done_debug_003!!!!!!!!!!!!!!!!!!!!!!!!
		´show_hack_done_003´
		´remove_WP_computer´
´show_hack_done_003´ ElementUnitSequence 100135
	Execute sequence: "show_hack_screen_lock_3_done" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	´output_hack_3_done´
´output_hack_3_done´ ElementInstanceOutput 100050
	Send instance event "hack_3_done" to mission.
´power_off_input´ ElementInstanceInput 100052
	Upon mission event "power_off":
		´seq_interrupt001´
´seq_interrupt001´ ElementUnitSequence 100054
	Execute sequence: "show_screen_power_off" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Execute sequence: "power_off" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
	Execute sequence: "power_off" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
	Execute sequence: "power_off" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Toggle on: ´restarted_hack´
	Toggle off: ´SO_interrupt_none´
	Toggle off: ´SO_interact_mid´
	Toggle off: ´complete´
	´remove_WP_computer´
	´remove_interrupt_SOs´
´restarted_hack´ ElementUnitSequenceTrigger 100074
	DISABLED
	Upon sequence "interact" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0):
		Toggle off: ´restarted_hack´
		´seq_resume002´
		´seq_resume003´
		´seq_resume004´
´power_on_input´ ElementInstanceInput 100053
	Upon mission event "power_on":
		´restart_the_hack´
´restart_the_hack´ ElementUnitSequence 100073
	Execute sequence: "anim_bootup" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	´enable_interaction´ (DELAY 11)
´enable_interaction´ ElementUnitSequence 100099
	Execute sequence: "enable_interaction" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Execute sequence: "set_interaction_hacking" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	´WP_fix´
´bluescreen_input´ ElementInstanceInput 100059
	Upon mission event "bluscreen":
		´bsod´
´bsod´ ElementUnitSequence 100187
	Execute sequence: "state_show_bsod" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Execute sequence: "power_off" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
	Execute sequence: "power_off" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
	Execute sequence: "power_off" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	Execute sequence: "enable_interaction" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	Execute sequence: "set_interaction_reboot" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Toggle off: ´fixed_computer´
	Toggle on: ´fixed_computer001´
	Toggle off: ´SO_interrupt_none´
	Toggle off: ´SO_interact_mid´
	Toggle off: ´complete´
	´remove_WP_computer´
	´remove_interrupt_SOs´
	´remove_interrupt_SOs´
	´WP_fix´
´fixed_computer001´ ElementUnitSequenceTrigger 100063
	DISABLED
	Upon sequence "interact" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0):
		´reboot´
´reboot´ ElementUnitSequence 100064
	Execute sequence: "anim_bootup" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	´remove_WP_fix´
	Toggle on: ´fixed_computer´ (DELAY 11)
	Toggle off: ´fixed_computer001´ (DELAY 11)
	´seq_resume002´ (DELAY 11)
	´seq_resume003´ (DELAY 11)
	´seq_resume004´ (DELAY 11)
	´bluescreen_rebooted_output´ (DELAY 11)
´bluescreen_rebooted_output´ ElementInstanceOutput 100067
	Send instance event "bluescreen_rebooted" to mission.
	´fixed_computer´
	´WP_computer´
´first_hack_done´ ElementUnitSequenceTrigger 100094
	TRIGGER TIMES 1
	Upon sequence "timer_done" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0):
		Toggle off: ´seq_resume002´
		Toggle off: ´hack_start_001´
		Toggle on: ´seq_resume003´
		Toggle on: ´second_hack_started_output´
		Toggle on: ´hack_start_002´
		Debug message: first_hack_done
		´enable_keyboard_interaction´
´second_hack_started_output´ ElementInstanceOutput 100104
	DISABLED
	Send instance event "second_hack_started" to mission.
	Toggle off: ´second_hack_started_output´
´enable_keyboard_interaction´ ElementUnitSequence 100098
	Execute sequence: "enable_interaction" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Toggle off: ´SO_interrupt_none´
	Toggle off: ´SO_interact_mid´
	Toggle off: ´complete´
	Toggle on, trigger times 1: ´next_hack_started´
	´remove_interrupt_SOs´
´next_hack_started´ ElementUnitSequenceTrigger 100103
	DISABLED
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0):
		´seq_first_hack´
		´second_hack_started_output´
		´third_hack_started_output´
		´hack_start_002´
		´hack_start_003´
´third_hack_started_output´ ElementInstanceOutput 100105
	DISABLED
	Send instance event "third_hack_started" to mission.
	Toggle off: ´third_hack_started_output´
´second_hack_done´ ElementUnitSequenceTrigger 100095
	TRIGGER TIMES 1
	Upon sequence "timer_done" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0):
		Toggle off: ´seq_resume003´
		Toggle off: ´hack_start_002´
		Toggle on: ´seq_resume004´
		Toggle on: ´third_hack_started_output´
		Toggle on: ´hack_start_003´
		Debug message: second_hack_done
		´enable_keyboard_interaction´
´third_hack_done´ ElementUnitSequenceTrigger 100096
	TRIGGER TIMES 1
	Upon sequence "timer_done" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0):
		Toggle off: ´seq_resume004´
		Toggle off: ´hack_start_003´
		Debug message: third_hack_done
´os_update_input´ ElementInstanceInput 100156
	Upon mission event "os_update":
		Toggle off: ´fixed_computer´
		Toggle on, trigger times 1: ´update_aborted´
		´os_update´
´update_aborted´ ElementUnitSequenceTrigger 100166
	DISABLED
	TRIGGER TIMES 1
	Upon sequence "interact" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0):
		Toggle off: ´os_updating´
		Toggle off: ´enable_interaction_2´
		Toggle off: ´logic_toggle_022´
		Toggle off: ´logic_toggle_019´
		Toggle off: ´logic_toggle_023´
		Toggle off: ´os_updated_output´
		Toggle off: ´update_finished´
		Toggle on: ´fixed_computer´
		Toggle on: ´SO_interrupt_none´
		Toggle on: ´SO_interact_mid´
		Toggle on: ´complete´
		Toggle on: ´police_called´
		Debug message: from mainframe instance: enable_interrupt_again
		Debug message: updated_aborted
		´link´
		´output_resumed_computer´
		´seq_resume002´
		´seq_resume004´
		´seq_resume003´
		´remove_WP_fix´
		´updated_aborted_output´
´os_update´ ElementUnitSequence 100057
	Execute sequence: "state_show_update" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Execute sequence: "enable_interaction" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Execute sequence: "power_off" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
	Execute sequence: "power_off" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
	Execute sequence: "power_off" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	Execute sequence: "set_interaction_postpone_update" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Toggle off: ´SO_interrupt_none´
	Toggle off: ´SO_interact_mid´
	Toggle off: ´complete´
	´remove_interrupt_SOs´
	´os_updating´ (DELAY 10)
´os_updating´ ElementUnitSequence 100159
	Execute sequence: "anim_update" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Execute sequence: "disable_interaction" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Toggle off: ´update_aborted´
	´enable_interaction_2´ (DELAY 50)
´enable_interaction_2´ ElementUnitSequence 100160
	Execute sequence: "enable_interaction" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Execute sequence: "set_interaction_hacking" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	´logic_toggle_019´
	´WP_fix´
´logic_toggle_022´ ElementToggle 100167
	Toggle on, trigger times 1: ´update_finished´
´update_finished´ ElementUnitSequenceTrigger 100163
	DISABLED
	TRIGGER TIMES 1
	Upon any sequence:
		"interact" - units/world/architecture/secret_stash/props/secret_stash_hack_keyboard_interaction/001 (-1.0, -28.0, 87.0)
		"interact" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Execute:
		´logic_toggle_023´
		´seq_resume002´
		´seq_resume003´
		´seq_resume004´
		´remove_WP_fix´
´logic_toggle_019´ ElementToggle 100161
	Toggle off: ´update_aborted´
	´logic_toggle_022´
´logic_toggle_023´ ElementToggle 100168
	Toggle on: ´fixed_computer´
	Toggle on: ´SO_interrupt_none´
	Toggle on: ´SO_interact_mid´
	Toggle on: ´complete´
	Toggle on: ´police_called´
	Debug message: from mainframe instance: enable_interrupt_again
	´os_updated_output´
	´link´
	´output_resumed_computer´
´os_updated_output´ ElementInstanceOutput 100162
	Send instance event "os_updated" to mission.
´updated_aborted_output´ ElementInstanceOutput 100008
	Send instance event "Updated_aborted" to mission.
´firewall_stopped_breach_input´ ElementInstanceInput 100172
	Upon mission event "firewall_stopped_breach":
		´disable_interaction´
´disable_interaction´ ElementUnitSequence 100175
	Execute sequence: "state_show_firewall" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
	Execute sequence: "power_off" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
	Execute sequence: "power_off" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
	Execute sequence: "power_off" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
	Execute sequence: "state_vis_hide" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	´remove_WP_computer´
´firewall_breached_input´ ElementInstanceInput 100176
	Upon mission event "firewall_bypassed":
		´seq_resume002´
		´seq_resume003´
		´seq_resume004´
		´remove_WP_fix´
		´WP_computer´
´time_left_timer（45－50）´ ElementTimer 100112
	Timer: 45-50
´timer_out_trigger´ ElementTimerTrigger 100116
	Upon timer "´time_left_timer（45－50）´ == 0:
		´not_much_time_left_oputput´
´not_much_time_left_oputput´ ElementInstanceOutput 100117
	Send instance event "not_much_time_left" to mission.
´start_timer_trigger´ ElementUnitSequenceTrigger 100118
	Upon any sequence:
		"state_start_hack" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
		"state_start_hack" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
		"state_start_hack" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
		"set_unjammed" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
		"set_unjammed" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
		"set_unjammed" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
		"power_on" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
		"power_on" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
		"power_on" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	Execute:
		Start timer: ´time_left_timer（45－50）´
´pause_timer_trigger´ ElementUnitSequenceTrigger 100119
	Upon any sequence:
		"set_jammed" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
		"set_jammed" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
		"set_jammed" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
		"power_off" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
		"power_off" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
		"power_off" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	Execute:
		Pause timer: ´time_left_timer（45－50）´
´reset_timer_trigger´ ElementUnitSequenceTrigger 100120
	Upon any sequence:
		"done_hacking" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
		"done_hacking" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
		"done_hacking" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
	Execute:
		Pause timer: ´time_left_timer（45－50）´
		´time_left_timer（45－50）´ = 45-50 (DELAY 0.1)
´disable_mainframe´ ElementInstanceInput 100070
	Upon mission event "DISABLE_MAINFRAME":
		Debug message: DISABLE_MAINFRAME!
		Toggle off: ´not_much_time_left_oputput´
		Toggle off: ´timer_out_trigger´
		Toggle off: ´resumed_hack_output´
		Toggle off: ´hack_start_001´
		Toggle off: ´hack_start_002´
		Toggle off: ´hack_start_003´
		Toggle off: ´seq_resume004´
		Toggle off: ´seq_resume003´
		Toggle off: ´seq_resume002´
		Toggle off: ´first_hack_done´
		Toggle off: ´second_hack_done´
		Toggle off: ´third_hack_done´
		Toggle off: ´enable_keyboard_interaction´
		Toggle off: ´next_hack_started´
		Toggle off: ´seq_setup´
		Toggle off: ´set_up_computer´
		Toggle off: ´seq_enable_setup_interaction´
		Toggle off: ´seq_startup´
		Toggle off: ´seq_first_hack´
		Toggle off: ´seq_interrupt001´
		Toggle off: ´enable_interaction´
		Toggle off: ´restart_the_hack´
		Toggle off: ´restarted_hack´
		Toggle off: ´fail´
		Toggle off: ´complete´
		Toggle off: ´fixed_computer´
		Toggle off: ´remove_WP_computer´
		Toggle off: ´seq_interrupt´
		Toggle off: ´seq_resume´
		Toggle off: ´remove_WP_fix´
		Toggle off: ´remove_interrupt_SOs´
		Toggle off: ´link´
		Toggle off: ´show_hack_done_001´
		Toggle off: ´output_hack_1_done´
		Toggle off: ´output_hack_2_done´
		Toggle off: ´output_resumed_computer´
		Toggle off: ´output_computer_got_interrupted´
		Toggle off: ´output_hack_3_done´
		Toggle off: ´show_hack_done_002´
		Toggle off: ´show_hack_done_003´
		Toggle off: ´hack_1_done´
		Toggle off: ´hack_2_done´
		Toggle off: ´hack_3_done´
		Toggle off: ´bsod´
		Toggle off: ´fixed_computer001´
		Toggle off: ´reboot´
		Toggle off: ´bluescreen_rebooted_output´
		Toggle off: ´os_update´
		Toggle off: ´os_updating´
		Toggle off: ´enable_interaction_2´
		Toggle off: ´os_update_input´
		Toggle off: ´update_finished´
		Toggle off: ´update_aborted´
		Toggle off: ´os_updated_output´
		Toggle off: ´disable_interaction´
		Toggle off: ´firewall_breached_input´
		Toggle off: ´firewall_stopped_breach_input´
		Toggle off: ´updated_aborted_output´
		remove ´WP_computer´
		remove ´WP_fix´
		remove ´SO_interrupt_none´
		remove ´SO_interact_mid´
		Execute sequence: "show_screen_power_off" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
		Execute sequence: "power_off" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
		Execute sequence: "power_off" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
		Execute sequence: "power_off" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
		Execute sequence: "state_vis_hide" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_01/001 (0.0, 5.0, 88.0)
		Execute sequence: "state_vis_hide" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_02/001 (0.0, 5.0, 90.0)
		Execute sequence: "state_vis_hide" - units/pd2_dlc_vit/props/computer_monitor/vit_interactable_hack_gui_03/001 (0.0, 5.0, 90.0)
		Execute sequence: "disable_interaction" - units/pd2_dlc_vit/props/vit_interactable_computer_monitor/001 (0.0, 5.0, 88.0)
