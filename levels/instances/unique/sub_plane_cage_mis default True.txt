﻿´setup´ MissionScriptElement 100002
	EXECUTE ON STARTUP
	BASE DELAY 1
	on_executed
		´hider´ (delay 0)
		´seq_hider´ (delay 0)
´hider´ ElementDisableUnit 100003
	position -600.0, 1200.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/props/gen_prop_square_drop_marker_2x3/001 (0.0, 0.0, 0.0)
´seq_hider´ ElementUnitSequence 100005
	position -600.0, 1100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc2/vehicles/air_vehicle_cessna_206_csgo/001 (0.0, 0.0, 0.0)
			notify_unit_sequence state_vis_hide
			time 0
´assemble_cage´ MissionScriptElement 100011
	on_executed
		´enable_area_assemble_cage´ (delay 0)
		´WP_cage´ (delay 0)
		´enable_marker´ (delay 0)
		´point_debug_002´ (delay 0)
´area_cage_parts´ ElementAreaTrigger 100013
	DISABLED
	amount 1
	depth 314.15
	height 500
	instigator loot
	interval 0.1
	position 0.0, 0.0, 0.0
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	rules_element_ids
		1 ´isntigator_rule_loot_cage_bag´
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 199.5
	on_executed
		´cage_part_in´ (delay 0)
		´remove_cage_part_bag´ (delay 0)
´enable_area_assemble_cage´ ElementToggle 100014
	elements
		1 ´area_cage_parts´ DISABLED
	set_trigger_times -1
	toggle on
´counter_cage_parts´ ElementCounter 100015
	counter_target 3
	digital_gui_unit_ids
	on_executed
		´cage_assembled´ (delay 0)
		´bain_cage_assembled´ (delay 0)
´cage_assembled´ MissionScriptElement 100016
	on_executed
		´point_debug_001´ (delay 0)
		´enable_area_loot_in_cage´ (delay 0)
		´bain_load_cage_hurry´ (delay 15-30)
		´instance_output_cage_assembled´ (delay 0)
´WP_cage´ ElementWaypoint 100017
	icon pd2_lootdrop
	position 0.0, 0.0, 200.0
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´remove_WP_cage´ ElementOperator 100018
	elements
		1 ´WP_cage´
	operation remove
´point_debug_001´ ElementDebug 100019
	as_subtitle False
	debug_string cage_assembled
	show_instigator False
´point_debug_002´ ElementDebug 100020
	as_subtitle False
	debug_string assemble_cage
	show_instigator False
´area_loot_in_cage´ ElementAreaTrigger 100021
	DISABLED
	TRIGGER TIMES 1
	amount 1
	depth 314
	height 500
	instigator loot
	interval 0.1
	position 0.0, 0.0, 100.0
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 200
	on_executed
		´bag_in_cage´ (delay 0)
´bag_in_cage´ MissionScriptElement 100022
	on_executed
		´raise_balloon´ (delay 0)
		´point_debug_004´ (delay 0)
		´disable_VO_load_cage_hurry´ (delay 0)
´enable_area_loot_in_cage´ ElementToggle 100023
	elements
		1 ´area_loot_in_cage´ DISABLED
	set_trigger_times -1
	toggle on
´isntigator_rule_loot_cage_bag´ ElementInstigatorRule 100024
	instigator loot
	rules
		loot
			carry_ids
				1 cage_bag
´area_loot_secure´ ElementAreaTrigger 100025
	DISABLED
	amount 1
	depth 314
	height 500
	instigator loot
	interval 0.01
	position 0.0, 0.0, 150.0
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 200
	on_executed
		´secure_loot´ (delay 0)
´raise_balloon´ ElementUnitSequence 100026
	position 100.0, 1100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc2/vehicles/air_vehicle_cessna_206_csgo/001 (0.0, 0.0, 0.0)
			notify_unit_sequence state_interaction_enabled
			time 0
´balloon_raised´ ElementUnitSequenceTrigger 100027
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/pd2_dlc2/vehicles/air_vehicle_cessna_206_csgo/001 (0.0, 0.0, 0.0)
	on_executed
		´point_debug_003´ (delay 0)
		´get_plane_in´ (delay 0)
		´bain_balloon_up´ (delay 0)
´secure_loot´ ElementCarry 100028
	operation secure
	type_filter none
	on_executed
		´remove_loot´ (delay 0)
´remove_loot´ ElementCarry 100029
	operation remove
	type_filter none
´point_debug_003´ ElementDebug 100030
	as_subtitle False
	debug_string balloon_raised
	show_instigator False
´point_debug_004´ ElementDebug 100031
	as_subtitle False
	debug_string bag_in_cage
	show_instigator False
´get_plane_in´ MissionScriptElement 100032
	on_executed
		´start_timer´ (delay 0)
		´point_debug_010´ (delay 0)
´cage_is_going_bye_bye´ ElementLogicChance 100033
	chance 40
	position 600.0, 1100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
´fail´ ElementLogicChanceTrigger 100034
	elements
		1 ´cage_is_going_bye_bye´
	outcome fail
	position 700.0, 1000.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´plane_miss´ (delay 0)
		´logic_chance_operator_001´ (delay 0)
		´point_debug_005´ (delay 0)
		´show_plane´ (delay 0)
		´try_again´ (delay 0)
		´logic_timer_operator_001´ (delay 0)
´success´ ElementLogicChanceTrigger 100035
	elements
		1 ´cage_is_going_bye_bye´
	outcome success
	position 700.0, 1200.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´plane_catch´ (delay 0)
		´point_debug_006´ (delay 0)
		´show_plane´ (delay 0)
´plane_miss´ ElementUnitSequence 100039
	position 800.0, 1000.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc2/vehicles/air_vehicle_cessna_206_csgo/001 (0.0, 0.0, 0.0)
			notify_unit_sequence anim_plane_miss
			time 0
´plane_catch´ ElementUnitSequence 100040
	position 800.0, 1200.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc2/vehicles/air_vehicle_cessna_206_csgo/001 (0.0, 0.0, 0.0)
			notify_unit_sequence anim_plane_catch
			time 0
´logic_chance_operator_001´ ElementLogicChanceOperator 100041
	chance 20
	elements
		1 ´cage_is_going_bye_bye´
	operation add_chance
	position 800.0, 900.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
´point_debug_005´ ElementDebug 100042
	as_subtitle False
	debug_string awww miss
	show_instigator False
´point_debug_006´ ElementDebug 100043
	as_subtitle False
	debug_string plane success
	show_instigator False
´plane_got_the_stuff´ MissionScriptElement 100044
	on_executed
		´remove_WP_cage´ (delay 0)
		´enable_area_loot_secure´ (delay 0)
		´disable_marker´ (delay 0)
´enable_area_loot_secure´ ElementToggle 100045
	elements
		1 ´area_loot_secure´ DISABLED
	set_trigger_times -1
	toggle on
	on_executed
		´disable_area_loot_secure´ (delay 0.5)
´enable_marker´ ElementEnableUnit 100046
	position -400.0, 1200.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/props/gen_prop_square_drop_marker_2x3/001 (0.0, 0.0, 0.0)
´cage_part_in´ MissionScriptElement 100047
	on_executed
		´counter_cage_parts´ (delay 0)
		´build_cage´ (delay 0)
´build_cage´ ElementUnitSequence 100048
	position -200.0, 1000.0, 0.0
	rotation 0.0, 0.0, 0.707107, -0.707107
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc2/vehicles/air_vehicle_cessna_206_csgo/001 (0.0, 0.0, 0.0)
			notify_unit_sequence state_build_cage
			time 0
´remove_cage_part_bag´ ElementCarry 100049
	operation remove
	type_filter cage_bag
´show_plane´ ElementUnitSequence 100050
	position 800.0, 1100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc2/vehicles/air_vehicle_cessna_206_csgo/001 (0.0, 0.0, 0.0)
			notify_unit_sequence state_vis_show
			time 0
´disable_marker´ ElementDisableUnit 100051
	position 1000.0, 1200.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/props/gen_prop_square_drop_marker_2x3/001 (0.0, 0.0, 0.0)
´bain_bags_go_into_cage´ ElementDialogue 100052
	dialogue Play_pln_cs1_17
	execute_on_executed_when_done False
	use_position False
´soundbank_csgo_bain´ ElementPlaySound 100053
	append_prefix False
	elements
	position -700.0, 1100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	sound_event Play_pln_cs1_118
	use_instigator False
´bain_cage_assembled´ ElementDialogue 100055
	dialogue Play_pln_cs1_21
	execute_on_executed_when_done False
	use_position False
´bain_cage_assembled2´ ElementDialogue 100056
	dialogue Play_pln_cs1_22
	execute_on_executed_when_done False
	use_position False
´bain_load_cage_hurry´ ElementDialogue 100057
	dialogue Play_pln_cs1_23
	execute_on_executed_when_done False
	use_position False
´bain_load_cage_continue´ ElementDialogue 100058
	dialogue Play_pln_cs1_24
	execute_on_executed_when_done False
	use_position False
´bain_balloon_up´ ElementDialogue 100059
	dialogue Play_pln_cs1_25
	execute_on_executed_when_done False
	use_position False
´bain_missed´ ElementDialogue 100060
	dialogue Play_pln_cs1_26
	execute_on_executed_when_done False
	use_position False
´bain_catched´ ElementDialogue 100061
	dialogue Play_pln_cs1_27
	execute_on_executed_when_done False
	use_position False
´plane_missed´ ElementUnitSequenceTrigger 100062
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence done_cage_miss
			unit_id units/pd2_dlc2/vehicles/air_vehicle_cessna_206_csgo/001 (0.0, 0.0, 0.0)
	on_executed
		´bain_missed´ (delay 0)
´plane_catched´ ElementUnitSequenceTrigger 100063
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence done_cage_catch
			unit_id units/pd2_dlc2/vehicles/air_vehicle_cessna_206_csgo/001 (0.0, 0.0, 0.0)
	on_executed
		´bain_catched´ (delay 0)
		´plane_got_the_stuff´ (delay 0)
		´disable_interaction´ (delay 0)
´cage_part_picked_up´ MissionScriptElement 100064
	TRIGGER TIMES 1
	on_executed
		´bain_bags_go_into_cage´ (delay 0)
´area_player_picked_up_cage_bag´ ElementAreaTrigger 100065
	TRIGGER TIMES 1
	amount 1
	depth 80000
	height 3000
	instigator player
	interval 0.1
	position -450.0, 750.0, 0.0
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	rules_element_ids
		1 ´isntigator_rule_player_with_cage_bag´
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 80000
	on_executed
		´cage_part_picked_up´ (delay 0)
		´point_debug_007´ (delay 0)
		´assemble_cage´ (delay 0)
´isntigator_rule_player_with_cage_bag´ ElementInstigatorRule 100066
	instigator player
	rules
		player
			carry_ids
				1 cage_bag
´disable_VO_load_cage_hurry´ ElementToggle 100067
	elements
		1 ´bain_load_cage_hurry´
	set_trigger_times -1
	toggle off
´point_debug_007´ ElementDebug 100068
	as_subtitle False
	debug_string player_picked_up_cage_bag
	show_instigator False
´instance_output_cage_assembled´ ElementInstanceOutput 100001
	event cage_assembled
´instance_input_ace_pilot´ ElementInstanceInput 100012
	event ace_pilot_bought
	on_executed
		´decrease_time´ (delay 0)
		´increase_chance´ (delay 0)
		´point_debug_008´ (delay 0)
´decrease_time´ ElementTimerOperator 100054
	elements
		1 ´timer_plane´
	operation set_time
	position 500.0, 1400.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	time 45
´timer_plane´ ElementTimer 100036
	digital_gui_unit_ids
	timer 90
	on_executed
		´cage_is_going_bye_bye´ (delay 0)
´start_timer´ ElementTimerOperator 100037
	elements
		1 ´timer_plane´
	operation start
	position 400.0, 1100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	time 0
´increase_chance´ ElementLogicChanceOperator 100038
	chance 40
	elements
		1 ´cage_is_going_bye_bye´
	operation add_chance
	position 500.0, 1500.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
´point_debug_008´ ElementDebug 100069
	as_subtitle False
	debug_string almir is in the game
	show_instigator False
´try_again´ MissionScriptElement 100070
	on_executed
		´get_plane_in´ (delay 1)
´logic_timer_operator_001´ ElementTimerOperator 100071
	elements
		1 ´timer_plane´
	operation reset
	position 700.0, 800.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	time 0
	on_executed
		´point_debug_009´ (delay 0)
´point_debug_009´ ElementDebug 100072
	as_subtitle False
	debug_string reset_timer
	show_instigator False
´point_debug_010´ ElementDebug 100073
	as_subtitle False
	debug_string get plane in
	show_instigator False
´disable_area_loot_secure´ ElementToggle 100006
	BASE DELAY 0.5
	elements
		1 ´area_loot_secure´ DISABLED
	set_trigger_times -1
	toggle off
´disable_interaction´ ElementUnitSequence 100007
	position 1200.0, 1100.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc2/vehicles/air_vehicle_cessna_206_csgo/001 (0.0, 0.0, 0.0)
			notify_unit_sequence state_interaction_disabled
			time 0
