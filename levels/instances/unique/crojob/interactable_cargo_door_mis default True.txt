﻿´startup´ ElementUnitSequence 100011
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Execute sequence: "disable_interaction" - units/pd2_dlc_cro/eus_train_door_cargo/001 (200.0, 0.0, 0.0)
´used´ ElementUnitSequenceTrigger 100002
	Upon sequence "interact" - units/pd2_dlc_cro/eus_train_door_cargo/001 (200.0, 0.0, 0.0):
		´open´
		´close´
´open´ ElementUnitSequence 100003
	Execute sequence: "anim_open_door" - units/pd2_dlc_cro/eus_train_door_cargo/001 (200.0, 0.0, 0.0)
	Send instance event "door_opened" to mission.
	´re-enable´ (DELAY 1)
´close´ ElementUnitSequence 100004
	DISABLED
	Execute sequence: "anim_close_door" - units/pd2_dlc_cro/eus_train_door_cargo/001 (200.0, 0.0, 0.0)
	Send instance event "door_closed" to mission.
	´re-enable´ (DELAY 1)
´re-enable´ ElementUnitSequence 100005
	Execute sequence: "enable_interaction" - units/pd2_dlc_cro/eus_train_door_cargo/001 (200.0, 0.0, 0.0)
	Toggle: ´open´
	Toggle: ´close´
	Toggle: ´blow_up_doors´
´blow_up_doors´ ElementUnitSequence 100018
	Execute sequence: "anim_explode_l_door" - units/pd2_dlc_cro/eus_train_door_cargo/001 (200.0, 0.0, 0.0)
	Execute sequence: "anim_explode_r_door" - units/pd2_dlc_cro/eus_train_door_cargo/001 (200.0, 0.0, 0.0)
´input_enable_interaction´ ElementInstanceInput 100013
	Upon mission event "enable_interaction":
		Execute sequence: "enable_interaction" - units/pd2_dlc_cro/eus_train_door_cargo/001 (200.0, 0.0, 0.0)
´input_toggle_doors´ ElementInstanceInput 100015
	Upon mission event "Toggle Doors":
		´close´
		´open´
´input_disable_interaction´ ElementInstanceInput 100016
	Upon mission event "disable_interaction":
		Toggle off: ´re-enable´
		Execute sequence: "disable_interaction" - units/pd2_dlc_cro/eus_train_door_cargo/001 (200.0, 0.0, 0.0)
´blow_up_doors_2´ ElementInstanceInput 100001
	Upon mission event "explode_door":
		´blow_up_doors´
´input_only_openable´ ElementInstanceInput 100019
	Upon mission event "only_openable":
		Toggle off: ´re-enable´
