ID range vs continent name:
	100000: world

statics
	100004 units/payday2/props/gen_prop_burnt_evidence/002 (-3.0, 54.0, -96.0)
	100000 units/payday2/props/gen_prop_burnt_evidence/003 (23.0, 50.0, -94.0)
	100003 units/payday2/props/gen_prop_burnt_evidence/004 (-3.0, 16.0, -96.0)
	100005 units/payday2/props/gen_prop_burnt_evidence/005 (51.0, 16.0, -96.0)
	100002 units/payday2/props/gen_prop_burnt_evidence/006 (80.0, 59.0, -96.0)
	100006 units/payday2/props/gen_prop_burnt_evidence/007 (-3.0, -26.0, -96.0)
	100007 units/payday2/props/gen_prop_burnt_evidence/008 (-59.0, 24.0, -96.0)
	100009 units/payday2/props/gen_prop_burnt_evidence/009 (-52.0, -12.0, -96.0)
	100010 units/world/props/apartment/bedstand/001 (0.0, 25.0, -120.0)
	100011 units/world/props/apartment/bedstand/002 (-2.0, 25.0, -120.0)
	100014 units/world/props/apartment/bedstand/003 (0.0, -15.0, -121.0)
	100012 units/world/props/survivalist/canisters/canister_metal/001 (-30.0, 20.0, -100.0)
	100013 units/world/props/survivalist/canisters/canister_metal/002 (42.0, 12.0, -85.0)
