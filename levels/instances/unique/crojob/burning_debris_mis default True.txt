﻿´effect_fire_debris´ ElementPlayEffect 100001
	EXECUTE ON STARTUP
	Play effect effects/payday2/environment/fire_big at position (11.0, 40.0, -99.0)
´trigger_area_001´ ElementAreaTrigger 100008
	Cylinder, radius 75.0, height 150.0, at position (-12.0, 1.0, -69.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon both, execute:
		Deal fire damage to instigator.
