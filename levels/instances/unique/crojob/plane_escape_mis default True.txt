﻿´startup´ ElementUnitSequence 100016
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Execute sequence: "hide" - units/payday2/props/gen_prop_square_goal_marker_7x55/001 (10224.0, -7669.29, 575.0)
´loot_trigger´ ElementAreaTrigger 100002
	DISABLED
	Box, width 110.0, height 100.0, depth 200.0, at position (10800.6, -7806.85, 701.0) with rotation (0.0, 0.0, 0.422618, -0.906308)
	Amount: 1
	Instigator: loot
	Interval: 0.1
	Rule: ´data_instigator_rule_001´
	Upon on_enter, execute:
		Teammate comment: p27
		If instigator loot type is "cro_loot1":
			Loot action: secure
			If instigator loot type is "cro_loot1":
				Loot action: remove
				Send instance event "cro_loot_secured" to mission.
		If instigator loot type is "cro_loot2":
			Loot action: secure
			If instigator loot type is "cro_loot2":
				Loot action: remove
				Send instance event "cro_loot_secured" to mission.
		If instigator loot type is "coke":
			Loot action: secure
			If instigator loot type is "coke":
				Loot action: remove
		If instigator loot type is "diamonds":
			Loot action: secure
			If instigator loot type is "diamonds":
				Loot action: remove
		If instigator loot type is "gold":
			Loot action: secure
			If instigator loot type is "gold":
				Loot action: remove
		If instigator loot type is "money":
			Loot action: secure
			If instigator loot type is "money":
				Loot action: remove
´data_instigator_rule_001´ ElementInstigatorRule 100034
	coke,cro_loot1,cro_loot2,diamonds,gold,money
´escape_trigger´ ElementAreaTrigger 100004
	DISABLED
	TRIGGER TIMES 1
	Box, width 700.0, height 500.0, depth 550.0, at position (10227.6, -7669.54, 740.004) with rotation (0.0, 0.0, -0.743145, -0.669131)
	Amount: all
	Instigator: player
	Interval: 1.0
	Upon on_enter, execute:
		Teammate comment: g24
		Send instance event "output_everyone_area" to mission.
		Send instance event "mission_end" to mission.
		End mission: success (DELAY 2)
´input_call_plane´ ElementInstanceInput 100005
	Upon mission event "call_plane":
		Execute sequence: "show" - units/pd2_dlc_cro/vehicles/cessna_206/air_interactable_vehicle_cessna_206/001 (20.9063, -198.904, 0.0)
		Execute sequence: "anim_land" - units/pd2_dlc_cro/vehicles/cessna_206/air_interactable_vehicle_cessna_206/001 (20.9063, -198.904, 0.0)
´is_anim_done´ ElementUnitSequenceTrigger 100007
	TRIGGER TIMES 1
	Upon sequence "done_landed" - units/pd2_dlc_cro/vehicles/cessna_206/air_interactable_vehicle_cessna_206/001 (20.9063, -198.904, 0.0):
		Execute sequence: "anim_door_open_passanger" - units/pd2_dlc_cro/vehicles/cessna_206/air_interactable_vehicle_cessna_206/001 (20.9063, -198.904, 0.0)
		Toggle on: ´loot_trigger´
		Send instance event "plane_landed" to mission.
		Add nav obstacle: units/pd2_dlc_cro/vehicles/cessna_206/air_interactable_vehicle_cessna_206/001 (20.9063, -198.904, 0.0)
´input_enable_escape´ ElementInstanceInput 100012
	Upon mission event "enable_escape":
		Toggle on, trigger times 1: ´escape_trigger´
		Execute sequence: "show" - units/payday2/props/gen_prop_square_goal_marker_7x55/001 (10224.0, -7669.29, 575.0)
