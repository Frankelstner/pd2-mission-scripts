﻿´ai_spawn_enemy_002´ ElementSpawnEnemyDummy 100003
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_swat_1/ene_swat_1
	force_pickup none
	interval 5
	participate_to_group_ai False
	position 6673.01, -8557.81, 576.004
	rotation 0.0, 0.0, -0.838671, -0.544639
	spawn_action e_sp_armored_truck_1st
	team default
	voice 0
´ai_spawn_enemy_004´ ElementSpawnEnemyDummy 100005
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_swat_1/ene_swat_1
	force_pickup none
	interval 5
	participate_to_group_ai False
	position 6718.69, -8537.48, 576.004
	rotation 0.0, 0.0, -0.838671, -0.544639
	spawn_action e_sp_armored_truck_2nd
	team default
	voice 0
´ai_spawn_enemy_006´ ElementSpawnEnemyDummy 100007
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_swat_1/ene_swat_1
	force_pickup none
	interval 5
	participate_to_group_ai False
	position 6764.37, -8517.14, 576.004
	rotation 0.0, 0.0, -0.838671, -0.544639
	spawn_action e_sp_armored_truck_3rd
	team default
	voice 0
´play anim swat response´ ElementUnitSequence 100009
	position 6624.0, -7984.0, 625.004
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_cro/vehicles/veh_truck_police_anim_01_stage_03/001 (0.0, 0.0, 0.0)
			notify_unit_sequence state_show
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_cro/vehicles/veh_truck_police_anim_01_stage_03/001 (0.0, 0.0, 0.0)
			notify_unit_sequence anim_police_response
			time 0
´is anim done´ ElementUnitSequenceTrigger 100010
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence done_police_response
			unit_id units/pd2_dlc_cro/vehicles/veh_truck_police_anim_01_stage_03/001 (0.0, 0.0, 0.0)
	on_executed
		´open doors´ (delay 0)
		´func_nav_obstacle_001´ (delay 0)
´open doors´ ElementUnitSequence 100011
	BASE DELAY 0.2
	position 6324.0, -7984.0, 625.004
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_cro/vehicles/veh_truck_police_anim_01_stage_03/001 (0.0, 0.0, 0.0)
			notify_unit_sequence anim_door_left_open
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/pd2_dlc_cro/vehicles/veh_truck_police_anim_01_stage_03/001 (0.0, 0.0, 0.0)
			notify_unit_sequence anim_door_right_open
			time 0
	on_executed
		´ai_spawn_enemy_002´ (delay 0)
		´ai_spawn_enemy_004´ (delay 0)
		´ai_spawn_enemy_006´ (delay 0)
		´swat spawned´ (delay 0)
´startup´ MissionScriptElement 100001
	EXECUTE ON STARTUP
	on_executed
		´hide van´ (delay 1)
´hide van´ ElementUnitSequence 100002
	position 6474.0, -8084.0, 625.004
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/pd2_dlc_cro/vehicles/veh_truck_police_anim_01_stage_03/001 (0.0, 0.0, 0.0)
			notify_unit_sequence state_hide
			time 0
´input call swat response´ ElementInstanceInput 100004
	event call_swat_response
	on_executed
		´play anim swat response´ (delay 0)
´swat spawned´ ElementInstanceOutput 100006
	event swat_spawned
´func_nav_obstacle_001´ ElementNavObstacle 100008
	obstacle_list
		1
			guis_id 1
			obj_name ab94fd2004494c60
			unit_id units/pd2_dlc_cro/vehicles/veh_truck_police_anim_01_stage_03/001 (0.0, 0.0, 0.0)
	operation add
	position 6475.0, -7875.0, 625.0
	rotation 0.0, 0.0, 0.0, -1.0
