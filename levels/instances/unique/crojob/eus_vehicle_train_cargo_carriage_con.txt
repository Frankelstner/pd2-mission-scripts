ID range vs continent name:
	100000: world

statics
	100015 units/payday2/props/ind_prop_military_mre_1x1_5m/001 (80.0003, 487.0, 151.0)
	100013 units/payday2/props/set/ind_prop_warehouse_pallet_stack_a/001 (-145.0, 1141.0, 150.0)
	100014 units/payday2/props/set/ind_prop_warehouse_pallet_stack_a/002 (-145.0, 357.0, 150.0)
	100041 units/pd2_dlc_cro/vault/eus_vehicle_train_cargo_carriage_docks/001 (0.0, 0.0, 0.0)
		mesh_variation state_vis_hide_vault
	100016 units/world/props/mansion/man_cover_int_shipping_crate_small/001 (152.0, 305.0, 149.615)
	100017 units/world/props/mansion/man_cover_int_shipping_crate_small/002 (152.0, 388.0, 149.615)
	100020 units/world/props/mansion/man_cover_int_shipping_crate_small/003 (147.001, -1119.0, 153.0)
	100021 units/world/props/mansion/man_cover_int_shipping_crate_small/004 (147.001, -1119.0, 266.0)
	100036 units/world/props/mansion/man_cover_int_shipping_crate_small/005 (150.0, 1096.0, 150.0)
	100037 units/world/props/mansion/man_cover_int_shipping_crate_small/006 (150.0, 1179.0, 150.0)
	100039 units/world/props/mansion/man_cover_int_shipping_crate_small/007 (155.0, -21.0, 149.615)
	100040 units/world/props/mansion/man_cover_int_shipping_crate_small/008 (155.0, -104.0, 149.615)
	100018 units/world/props/mansion/man_cover_int_shipping_crate_wide/001 (-143.009, -51.9899, 150.0)
	100019 units/world/props/mansion/man_cover_int_shipping_crate_wide/002 (-143.009, -192.99, 150.0)
	100022 units/world/props/mansion/man_cover_int_shipping_crate_wide/003 (-154.009, -478.99, 150.0)
	100023 units/world/props/mansion/man_cover_int_shipping_crate_wide/004 (-143.009, -192.99, 261.0)
	100038 units/world/props/mansion/man_cover_int_shipping_crate_wide/006 (-150.0, -1099.0, 149.615)
