﻿´disable_X_by_default´ ElementDisableUnit 100090
	EXECUTE ON STARTUP
	Hide object: units/pd2_dlc_arena/props/are_prop_vault_concrete/001 (0.0, 0.0, 400.0)
´Setup_Cutters´ ElementUnitSequence 100068
	EXECUTE ON STARTUP
	Execute sequence: "hide_dummy" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/001 (195.0, 0.0, 500.0)
	Execute sequence: "hide_dummy" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/002 (395.0, 0.0, 500.0)
	Execute sequence: "hide_dummy" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/003 (595.0, 0.0, 500.0)
	Execute sequence: "state_set_timer_30" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/001 (195.0, 0.0, 500.0)
	Execute sequence: "state_set_timer_30" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/002 (395.0, 0.0, 500.0)
	Execute sequence: "state_set_timer_30" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/003 (595.0, 0.0, 500.0)
	Execute sequence: "stop_cut_sound" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/001 (195.0, 0.0, 500.0)
	Execute sequence: "stop_cut_sound" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/002 (395.0, 0.0, 500.0)
	Execute sequence: "stop_cut_sound" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/003 (595.0, 0.0, 500.0)
´prepare_c4´ ElementInstanceInput 100021
	Upon mission event "Mark_C4":
		Toggle on: ´enable_c4´
		Show object: units/pd2_dlc_arena/props/are_prop_vault_concrete/001 (0.0, 0.0, 400.0)
´enable_c4´ ElementInstanceInput 100046
	DISABLED
	Upon mission event "enable c4":
		Toggle on: ´trigger´
		´waypoint_-__goto´
´waypoint_-__goto´ ElementWaypoint 100048
	Place waypoint with icon "pd2_goto" at position (400.0, 0.0, 271.553)
´trigger´ ElementAreaTrigger 100050
	DISABLED
	TRIGGER TIMES 1
	Box, width 800.0, height 400.0, depth 400.0, at position (400.0, 0.0, 200.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		remove ´waypoint_-__goto´
		Execute sequence: "state_vis_show_enable" - units/equipment/c4_charge/gen_plantable_c4_x10/001 (250.0, 0.0, 400.0)
		Execute sequence: "state_vis_show_enable" - units/equipment/c4_charge/gen_plantable_c4_x10/002 (400.0, 0.0, 400.0)
		Execute sequence: "state_vis_show_enable" - units/equipment/c4_charge/gen_plantable_c4_x10/003 (550.0, 0.0, 400.0)
		´waypoint_-__c4´
´waypoint_-__c4´ ElementWaypoint 100052
	Place waypoint with icon "pd2_c4" at position (400.0, 0.0, 346.553)
´c4_planted´ ElementUnitSequenceTrigger 100054
	Upon any sequence:
		"interact" - units/equipment/c4_charge/gen_plantable_c4_x10/001 (250.0, 0.0, 400.0)
		"interact" - units/equipment/c4_charge/gen_plantable_c4_x10/002 (400.0, 0.0, 400.0)
		"interact" - units/equipment/c4_charge/gen_plantable_c4_x10/003 (550.0, 0.0, 400.0)
	Execute:
		´c4_counter（3）´
´c4_counter（3）´ ElementCounter 100053
	When counter target reached: 
		remove ´waypoint_-__c4´
		Send instance event "c4 all placed" to mission.
		Toggle on: ´blow_up_c4´
		´select_placement´
´blow_up_c4´ MissionScriptElement 100064
	DISABLED
	Execute sequence: "activate_explode_sequence" - units/equipment/c4_charge/gen_plantable_c4_x10/001 (250.0, 0.0, 400.0)
	Execute sequence: "activate_explode_sequence" - units/equipment/c4_charge/gen_plantable_c4_x10/002 (400.0, 0.0, 400.0)
	Execute sequence: "activate_explode_sequence" - units/equipment/c4_charge/gen_plantable_c4_x10/003 (550.0, 0.0, 400.0)
´select_placement´ MissionScriptElement 100143
	´link_3´
	´link_2´
	´link_1´
´link_3´ ElementCounterOperator 100124
	DISABLED
	´which_place_am_i_in_the_line_to_blow_up（1）´ = 3
	Send instance event "3rd was picked" to mission.
	Toggle off: ´select_placement´
	Debug message: 3 set
´which_place_am_i_in_the_line_to_blow_up（1）´ ElementCounter 100129
	When counter target reached: 
		Debug message: VAULT Should eXPLODE NOW
		´blow_up_c4´
´link_2´ ElementCounterOperator 100123
	DISABLED
	´which_place_am_i_in_the_line_to_blow_up（1）´ = 2
	Send instance event "2nd was picked" to mission.
	Toggle off: ´select_placement´
	Debug message: 2 set
´link_1´ ElementCounterOperator 100122
	´which_place_am_i_in_the_line_to_blow_up（1）´ = 1
	Send instance event "1st was picked" to mission.
	Toggle off: ´select_placement´
	Debug message: 1 set
´blow_up_c4_3´ ElementInstanceInput 100062
	Upon mission event "c4 blow up":
		´which_place_am_i_in_the_line_to_blow_up（1）´
´explode_c4´ ElementUnitSequenceTrigger 100066
	BASE DELAY  (DELAY 5)
	TRIGGER TIMES 1
	Upon any sequence:
		"activate_explode_sequence" - units/equipment/c4_charge/gen_plantable_c4_x10/001 (250.0, 0.0, 400.0)
		"activate_explode_sequence" - units/equipment/c4_charge/gen_plantable_c4_x10/002 (400.0, 0.0, 400.0)
		"activate_explode_sequence" - units/equipment/c4_charge/gen_plantable_c4_x10/003 (550.0, 0.0, 400.0)
	Execute:
		Execute sequence: "state_vis_hide" - units/equipment/c4_charge/gen_plantable_c4_x10/001 (250.0, 0.0, 400.0)
		Execute sequence: "state_vis_hide" - units/equipment/c4_charge/gen_plantable_c4_x10/002 (400.0, 0.0, 400.0)
		Execute sequence: "state_vis_hide" - units/equipment/c4_charge/gen_plantable_c4_x10/003 (550.0, 0.0, 400.0)
		Execute sequence: "spawn_dummy" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/001 (195.0, 0.0, 500.0)
		Execute sequence: "spawn_dummy" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/002 (395.0, 0.0, 500.0)
		Execute sequence: "spawn_dummy" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/003 (595.0, 0.0, 500.0)
		Send instance event "remove cap" to mission.
		Explosion damage 80 with range 500 at position (400.0, 0.0, 375.0)
		Play effect effects/payday2/particles/explosions/c4_wall_explosion at position (400.0, 0.0, 400.0)
		´show_waypoint（2）´
´show_waypoint（2）´ ElementCounter 100020
	When counter target reached: 
		´show_saw_waypoint´
´show_saw_waypoint´ MissionScriptElement 100034
	TRIGGER TIMES 1
	´waypoint_saw´
´waypoint_saw´ ElementWaypoint 100059
	Place waypoint with icon "pd2_generic_saw" at position (400.0, 0.0, 400.0)
´drill_setup_done´ ElementUnitSequenceTrigger 100027
	TRIGGER TIMES 1
	Upon any sequence:
		"interact" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/001 (195.0, 0.0, 500.0)
		"interact" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/002 (395.0, 0.0, 500.0)
		"interact" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/003 (595.0, 0.0, 500.0)
	Execute:
		remove ´waypoint_saw´
		Toggle off: ´show_saw_waypoint´
		Send instance event "saw_used" to mission.
´cutter_1_timer_done´ ElementUnitSequenceTrigger 100096
	TRIGGER TIMES 1
	Upon sequence "timer_done" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/001 (195.0, 0.0, 500.0):
		Execute sequence: "hide_dummy" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/001 (195.0, 0.0, 500.0)
		Execute sequence: "state_interaction_enabled" - units/pd2_dlc_arena/equipment/gen_pku_circle_cutter/001 (195.0, 0.0, 500.0)
		Execute sequence: "state_vis_show" - units/pd2_dlc_arena/equipment/gen_pku_circle_cutter/001 (195.0, 0.0, 500.0)
		Send instance event "Saw_Finished" to mission.
		´waypoint_grab_cutter001´
´waypoint_grab_cutter001´ ElementWaypoint 100012
	Place waypoint with icon "pd2_generic_interact" at position (200.0, 0.0, 475.0)
´cutter_2_timer_done´ ElementUnitSequenceTrigger 100106
	TRIGGER TIMES 1
	Upon sequence "timer_done" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/002 (395.0, 0.0, 500.0):
		Execute sequence: "hide_dummy" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/002 (395.0, 0.0, 500.0)
		Execute sequence: "state_vis_show" - units/pd2_dlc_arena/equipment/gen_pku_circle_cutter/002 (395.0, 0.0, 500.0)
		Execute sequence: "state_interaction_enabled" - units/pd2_dlc_arena/equipment/gen_pku_circle_cutter/002 (395.0, 0.0, 500.0)
		Send instance event "Saw_Finished" to mission.
		´waypoint_grab_cutter002´
´waypoint_grab_cutter002´ ElementWaypoint 100010
	Place waypoint with icon "pd2_generic_interact" at position (400.0, 0.0, 475.0)
´cutter_3_timer_done´ ElementUnitSequenceTrigger 100108
	TRIGGER TIMES 1
	Upon sequence "timer_done" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/003 (595.0, 0.0, 500.0):
		Execute sequence: "hide_dummy" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/003 (595.0, 0.0, 500.0)
		Execute sequence: "state_vis_show" - units/pd2_dlc_arena/equipment/gen_pku_circle_cutter/003 (595.0, 0.0, 500.0)
		Execute sequence: "state_interaction_enabled" - units/pd2_dlc_arena/equipment/gen_pku_circle_cutter/003 (595.0, 0.0, 500.0)
		Send instance event "Saw_Finished" to mission.
		´waypoint_grab_cutter003´
´waypoint_grab_cutter003´ ElementWaypoint 100011
	Place waypoint with icon "pd2_generic_interact" at position (600.0, 0.0, 475.0)
´disable_1st´ ElementInstanceInput 100128
	Upon mission event "disable 1st":
		Toggle off: ´link_1´
		Toggle on: ´link_2´
´disable_2nd´ ElementInstanceInput 100135
	Upon mission event "disable 2nd":
		Toggle off: ´link_2´
		Toggle on: ´link_3´
´disable_3rd´ ElementInstanceInput 100136
	Upon mission event "disable 3rd":
		Toggle off: ´link_3´
´Clean_Up´ ElementInstanceInput 100030
	Upon mission event "Clean Up":
		remove ´waypoint_-__c4´
		remove ´waypoint_-__goto´
		remove ´waypoint_saw´
		remove ´waypoint_grab_cutter001´
		remove ´waypoint_grab_cutter002´
		remove ´waypoint_grab_cutter003´
		Execute sequence: "hide_dummy" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/001 (195.0, 0.0, 500.0)
		Execute sequence: "hide_dummy" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/002 (395.0, 0.0, 500.0)
		Execute sequence: "hide_dummy" - units/pd2_dlc_arena/equipment/gen_interactable_circle_cutter/003 (595.0, 0.0, 500.0)
		Execute sequence: "state_vis_hide" - units/equipment/c4_charge/gen_plantable_c4_x10/001 (250.0, 0.0, 400.0)
		Execute sequence: "state_vis_hide" - units/equipment/c4_charge/gen_plantable_c4_x10/002 (400.0, 0.0, 400.0)
		Execute sequence: "state_vis_hide" - units/equipment/c4_charge/gen_plantable_c4_x10/003 (550.0, 0.0, 400.0)
		Execute sequence: "state_vis_hide" - units/pd2_dlc_arena/equipment/gen_pku_circle_cutter/001 (195.0, 0.0, 500.0)
		Execute sequence: "state_vis_hide" - units/pd2_dlc_arena/equipment/gen_pku_circle_cutter/002 (395.0, 0.0, 500.0)
		Execute sequence: "state_vis_hide" - units/pd2_dlc_arena/equipment/gen_pku_circle_cutter/003 (595.0, 0.0, 500.0)
´cutter_2_pku´ ElementUnitSequenceTrigger 100014
	TRIGGER TIMES 1
	Upon sequence "load" - units/pd2_dlc_arena/equipment/gen_pku_circle_cutter/002 (395.0, 0.0, 500.0):
		Hide object: units/pd2_dlc_arena/props/are_prop_vault_concrete_cap_02/001 (0.0, 0.0, 400.0)
		remove ´waypoint_grab_cutter002´
		Play effect effects/payday2/environment/falling_money at position (397.0, 0.0, 525.0)
		Send instance event "Saw_Removed" to mission.
		Execute sequence: "drop_bundle" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/003 (387.121, 10.7004, 523.153) (DELAY 1)
		Execute sequence: "drop_bundle" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/004 (405.446, -7.62486, 573.259) (DELAY 1)
´cutter_3_pku´ ElementUnitSequenceTrigger 100015
	TRIGGER TIMES 1
	Upon sequence "load" - units/pd2_dlc_arena/equipment/gen_pku_circle_cutter/003 (595.0, 0.0, 500.0):
		Hide object: units/pd2_dlc_arena/props/are_prop_vault_concrete_cap_01/001 (0.0, 0.0, 400.0)
		remove ´waypoint_grab_cutter003´
		Play effect effects/payday2/environment/falling_money at position (597.0, 0.0, 525.0)
		Send instance event "Saw_Removed" to mission.
		Execute sequence: "drop_bundle" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/005 (608.002, 1.8217, 530.978) (DELAY 1)
		Execute sequence: "drop_bundle" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/006 (579.186, 1.8217, 580.767) (DELAY 1)
´cutter_1_pku´ ElementUnitSequenceTrigger 100016
	TRIGGER TIMES 1
	Upon sequence "load" - units/pd2_dlc_arena/equipment/gen_pku_circle_cutter/001 (195.0, 0.0, 500.0):
		Hide object: units/pd2_dlc_arena/props/are_prop_vault_concrete_cap_03/001 (0.0, 0.0, 400.0)
		remove ´waypoint_grab_cutter001´
		Play effect effects/payday2/environment/falling_money at position (197.0, 0.0, 525.0)
		Send instance event "Saw_Removed" to mission.
		Execute sequence: "drop_bundle" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/001 (200.0, 13.0, 524.0) (DELAY 1)
		Execute sequence: "drop_bundle" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/002 (200.0, -14.4019, 572.696) (DELAY 1)
´show_saw_waypoint_2´ ElementInstanceInput 100031
	Upon mission event "show_saw_waypoint":
		´show_waypoint（2）´
´Money_bundles_01´ ElementUnitSequenceTrigger 100082
	Upon any sequence:
		"load" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/001 (200.0, 13.0, 524.0)
		"load" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/002 (200.0, -14.4019, 572.696)
	Execute:
		Send instance event "Money Acquired" to mission.
		Debug message: SHOW 1
´Money_bundles001´ ElementUnitSequenceTrigger 100084
	Upon any sequence:
		"load" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/003 (387.121, 10.7004, 523.153)
		"load" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/004 (405.446, -7.62486, 573.259)
	Execute:
		Send instance event "Money Acquired" to mission.
		Debug message: SHOW 2
´Money_bundles002´ ElementUnitSequenceTrigger 100092
	Upon any sequence:
		"load" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/005 (608.002, 1.8217, 530.978)
		"load" - units/pd2_dlc_arena/pickups/gen_pku_money_plasic_wrapped/006 (579.186, 1.8217, 580.767)
	Execute:
		Send instance event "Money Acquired" to mission.
		Debug message: SHOW 3
´QA_CHEAT´ ElementInstanceInput 100078
	Upon mission event "QA_CHEAT":
		Toggle on: ´enable_c4´
		Show object: units/pd2_dlc_arena/props/are_prop_vault_concrete/001 (0.0, 0.0, 400.0)
		remove ´waypoint_-__goto´ (DELAY 1)
		Execute sequence: "state_vis_show_enable" - units/equipment/c4_charge/gen_plantable_c4_x10/001 (250.0, 0.0, 400.0) (DELAY 1)
		Execute sequence: "state_vis_show_enable" - units/equipment/c4_charge/gen_plantable_c4_x10/002 (400.0, 0.0, 400.0) (DELAY 1)
		Execute sequence: "state_vis_show_enable" - units/equipment/c4_charge/gen_plantable_c4_x10/003 (550.0, 0.0, 400.0) (DELAY 1)
		´c4_counter（3）´ (DELAY 3)
		Execute sequence: "activate_explode_sequence" - units/equipment/c4_charge/gen_plantable_c4_x10/001 (250.0, 0.0, 400.0) (DELAY 4)
		Execute sequence: "activate_explode_sequence" - units/equipment/c4_charge/gen_plantable_c4_x10/002 (400.0, 0.0, 400.0) (DELAY 4)
		Execute sequence: "activate_explode_sequence" - units/equipment/c4_charge/gen_plantable_c4_x10/003 (550.0, 0.0, 400.0) (DELAY 4)
		Send instance event "Money Acquired" to mission. (DELAY 5)
