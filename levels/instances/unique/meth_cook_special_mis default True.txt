﻿´input_start_cooking´ ElementInstanceInput 100000
	event start_cooking_debug
	on_executed
		´func_sequence_001´ (delay 0)
´output_need_ingredient´ ElementInstanceOutput 100024
	event need_ingredient
´output_ingredient_added´ ElementInstanceOutput 100042
	event output_ingredient_added
	on_executed
		´start_cooking´ (delay 0)
´ingredienses_fixed´ ElementCounter 100043
	counter_target 0
	digital_gui_unit_ids
´1´ ElementCounterTrigger 100044
	amount 1
	elements
		1 ´ingredienses_fixed´
	trigger_type value
	on_executed
		´point_debug_003´ (delay 0)
´2´ ElementCounterTrigger 100045
	amount 2
	elements
		1 ´ingredienses_fixed´
	trigger_type value
	on_executed
		´point_debug_002´ (delay 0)
´3´ ElementCounterTrigger 100046
	amount 3
	elements
		1 ´ingredienses_fixed´
	trigger_type value
	on_executed
		´ingredienses_done´ (delay 1)
		´point_debug_001´ (delay 0)
		´logic_toggle_004´ (delay 0)
´4´ ElementCounterTrigger 100047
	amount 4
	elements
		1 ´ingredienses_fixed´
	trigger_type value
	on_executed
		´point_debug_004´ (delay 0)
´5´ ElementCounterTrigger 100048
	amount 5
	elements
		1 ´ingredienses_fixed´
	trigger_type value
	on_executed
		´point_debug_005´ (delay 0)
´6´ ElementCounterTrigger 100049
	amount 6
	elements
		1 ´ingredienses_fixed´
	trigger_type value
	on_executed
		´logic_toggle_005´ (delay 0)
		´point_debug_006´ (delay 0)
		´ingredienses_done´ (delay 1)
´7´ ElementCounterTrigger 100050
	amount 7
	elements
		1 ´ingredienses_fixed´
	trigger_type value
	on_executed
		´point_debug_007´ (delay 0)
´8´ ElementCounterTrigger 100051
	amount 8
	elements
		1 ´ingredienses_fixed´
	trigger_type value
	on_executed
		´point_debug_008´ (delay 0)
´9´ ElementCounterTrigger 100052
	amount 9
	elements
		1 ´ingredienses_fixed´
	trigger_type value
	on_executed
		´logic_toggle_006´ (delay 0)
		´point_debug_009´ (delay 0)
		´ingredienses_done´ (delay 1)
´ingredient_added´ ElementCounterOperator 100053
	amount 1
	elements
		1 ´ingredienses_fixed´
	operation add
´func_sequence_trigger_001´ ElementUnitSequenceTrigger 100054
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/equipment/interactive_template/ingrediense2 (-100.0, 0.0, 0.0)
		2
			guis_id 2
			sequence interact
			unit_id units/equipment/interactive_template/ingrediense1 (-199.0, 76.0, 400.0)
	on_executed
		´ingredient_added´ (delay 0)
		´output_ingredient_added´ (delay 0)
´startupHider´ ElementUnitSequence 100057
	EXECUTE ON STARTUP
	position -275.0, -375.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/equipment/interactive_template/ingrediense2 (-100.0, 0.0, 0.0)
			notify_unit_sequence hide
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/props/gen_prop_methlab_meth/001 (275.0, -25.0, 93.5)
			notify_unit_sequence state_vis_hide
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/props/gen_prop_methlab_meth/001 (275.0, -25.0, 93.5)
			notify_unit_sequence state_interaction_disabled
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/equipment/interactive_template/ingrediense1 (-199.0, 76.0, 400.0)
			notify_unit_sequence hide
			time 0
		5
			id 5
			name run_sequence
			notify_unit_id units/equipment/interactive_template/startCookingTemp (150.0, 0.0, 0.0)
			notify_unit_sequence hide
			time 0
´input_easy´ ElementInstanceInput 100058
	event easy
	on_executed
		´logic_toggle_001´ (delay 0)
´input_hard´ ElementInstanceInput 100059
	event hard
	on_executed
		´logic_toggle_002´ (delay 0)
´input_ovk´ ElementInstanceInput 100060
	event overkill
	on_executed
		´logic_toggle_003´ (delay 0)
´logic_toggle_001´ ElementToggle 100061
	elements
		1 ´cooking_timer_75´
		2 ´cooking_timer_60´
		3 ´start001´
		4 ´start´
	set_trigger_times -1
	toggle off
	on_executed
		´need_ing3´ (delay 0)
´logic_toggle_002´ ElementToggle 100062
	elements
		1 ´3´
		2 ´cooking_timer_75´
		3 ´cooking_timer_30´
		4 ´start004´
		5 ´start´
	set_trigger_times -1
	toggle off
	on_executed
		´need_ing6´ (delay 0)
´logic_toggle_003´ ElementToggle 100063
	elements
		1 ´6´
		2 ´3´
		3 ´cooking_timer_40´
		4 ´cooking_timer_30´
		5 ´start004´
		6 ´start003´
	set_trigger_times -1
	toggle off
	on_executed
		´need_ing9´ (delay 0)
´ingredienses_done´ MissionScriptElement 100064
	on_executed
		´output_cooking_done´ (delay 0)
		´env_effect_stop_001´ (delay 0)
		´spawn_meth´ (delay 5-30)
´output_cooking_done´ ElementInstanceOutput 100065
	event cooking_done
´input_ingredient_picked_up´ ElementInstanceInput 100066
	event ingredient_picked_up
	on_executed
		´logic_random_002´ (delay 0)
´addIngrediense2´ ElementUnitSequence 100067
	position -400.0, -475.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/equipment/interactive_template/ingrediense2 (-100.0, 0.0, 0.0)
			notify_unit_sequence show_interactive
			time 0
´start_cooking´ MissionScriptElement 100056
	on_executed
		´logic_random_001´ (delay 0)
		´env_effect_play_001´ (delay 0)
		´point_debug_010´ (delay 0)
´cooking_timer_30´ ElementTimer 100068
	digital_gui_unit_ids
	timer 60
	on_executed
		´logic_link_001´ (delay 0)
		´point_debug_020´ (delay 0)
´start´ ElementTimerOperator 100069
	elements
		1 ´cooking_timer_75´
	operation start
	position 475.0, 175.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	time 0
	on_executed
		´point_debug_011´ (delay 0)
´cooking_timer_50´ ElementTimer 100072
	digital_gui_unit_ids
	timer 100
	on_executed
		´logic_link_001´ (delay 0)
		´point_debug_018´ (delay 0)
´cooking_timer_75´ ElementTimer 100073
	digital_gui_unit_ids
	timer 150
	on_executed
		´logic_link_001´ (delay 0)
		´point_debug_016´ (delay 0)
´cooking_timer_60´ ElementTimer 100074
	digital_gui_unit_ids
	timer 120
	on_executed
		´logic_link_001´ (delay 0)
		´point_debug_017´ (delay 0)
´cooking_timer_40´ ElementTimer 100075
	digital_gui_unit_ids
	timer 80
	on_executed
		´logic_link_001´ (delay 0)
		´point_debug_019´ (delay 0)
´start001´ ElementTimerOperator 100076
	elements
		1 ´cooking_timer_60´
	operation start
	position 475.0, 225.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	time 0
	on_executed
		´point_debug_012´ (delay 0)
´start002´ ElementTimerOperator 100077
	elements
		1 ´cooking_timer_50´
	operation start
	position 475.0, 275.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	time 0
	on_executed
		´point_debug_013´ (delay 0)
´start003´ ElementTimerOperator 100078
	elements
		1 ´cooking_timer_40´
	operation start
	position 475.0, 325.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	time 0
	on_executed
		´point_debug_014´ (delay 0)
´start004´ ElementTimerOperator 100079
	elements
		1 ´cooking_timer_30´
	operation start
	position 475.0, 375.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	time 0
	on_executed
		´point_debug_015´ (delay 0)
´logic_random_001´ ElementRandom 100080
	amount 1
	ignore_disabled True
	on_executed
		´start004´ (delay 0)
		´start003´ (delay 0)
		´start002´ (delay 0)
		´start001´ (delay 0)
		´start´ (delay 0)
´logic_link_001´ MissionScriptElement 100070
	on_executed
		´output_need_ingredient´ (delay 0)
		´logic_timer_operator_001´ (delay 0)
´spawn_meth´ ElementUnitSequence 100081
	position 1200.0, -275.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/props/gen_prop_methlab_meth/001 (275.0, -25.0, 93.5)
			notify_unit_sequence state_vis_show
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/props/gen_prop_methlab_meth/001 (275.0, -25.0, 93.5)
			notify_unit_sequence state_interaction_enabled
			time 0
	on_executed
		´logic_toggle_007´ (delay 0)
		´restart_cooking´ (delay 1)
		´point_waypoint_001´ (delay 0)
´picked_up_meth´ ElementUnitSequenceTrigger 100082
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/props/gen_prop_methlab_meth/001 (275.0, -25.0, 93.5)
	on_executed
		´func_instance_output_001´ (delay 0)
		´logic_operator_001´ (delay 0)
´point_debug_001´ ElementDebug 100083
	as_subtitle False
	debug_string 3 ingrediens added
	show_instigator False
´point_debug_002´ ElementDebug 100084
	as_subtitle False
	debug_string 2 ingrediens added
	show_instigator False
´point_debug_003´ ElementDebug 100085
	as_subtitle False
	debug_string 1 ingrediens added
	show_instigator False
´env_effect_play_001´ ElementPlayEffect 100086
	base_time 0
	effect effects/payday2/environment/environment_smoke_blue
	max_amount 0
	position -25.0, -50.0, 168.5
	random_time 0
	rotation 0.0, 0.0, 0.0, -1.0
	screen_space False
´env_effect_stop_001´ ElementStopEffect 100087
	elements
		1 ´env_effect_play_001´
	operation fade_kill
	position 1200.0, -525.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
´logic_toggle_004´ ElementToggle 100088
	elements
		1 ´start_cooking´
	set_trigger_times -1
	toggle off
´logic_toggle_005´ ElementToggle 100089
	elements
		1 ´start_cooking´
	set_trigger_times -1
	toggle off
´logic_toggle_006´ ElementToggle 100090
	elements
		1 ´start_cooking´
	set_trigger_times -1
	toggle off
´reset´ ElementCounterOperator 100091
	amount 0
	elements
		1 ´ingredienses_fixed´
	operation set
´point_debug_004´ ElementDebug 100092
	as_subtitle False
	debug_string 4 ingrediens added
	show_instigator False
´point_debug_005´ ElementDebug 100093
	as_subtitle False
	debug_string 5 ingrediens added
	show_instigator False
´point_debug_006´ ElementDebug 100094
	as_subtitle False
	debug_string 6 ingrediens added
	show_instigator False
´point_debug_007´ ElementDebug 100095
	as_subtitle False
	debug_string 7 ingrediens added
	show_instigator False
´point_debug_008´ ElementDebug 100096
	as_subtitle False
	debug_string 8 ingrediens added
	show_instigator False
´point_debug_009´ ElementDebug 100097
	as_subtitle False
	debug_string 9 ingrediens added
	show_instigator False
´point_debug_010´ ElementDebug 100098
	as_subtitle False
	debug_string started cooking
	show_instigator False
´point_debug_011´ ElementDebug 100099
	as_subtitle False
	debug_string started 150
	show_instigator False
´point_debug_012´ ElementDebug 100100
	as_subtitle False
	debug_string started 120
	show_instigator False
´point_debug_013´ ElementDebug 100101
	as_subtitle False
	debug_string started 100
	show_instigator False
´point_debug_014´ ElementDebug 100102
	as_subtitle False
	debug_string started 80
	show_instigator False
´point_debug_015´ ElementDebug 100103
	as_subtitle False
	debug_string started 60
	show_instigator False
´point_debug_016´ ElementDebug 100104
	as_subtitle False
	debug_string TIMER DONE
	show_instigator False
´point_debug_017´ ElementDebug 100105
	as_subtitle False
	debug_string TIMER DONE
	show_instigator False
´point_debug_018´ ElementDebug 100106
	as_subtitle False
	debug_string TIMER DONE
	show_instigator False
´point_debug_019´ ElementDebug 100107
	as_subtitle False
	debug_string TIMER DONE
	show_instigator False
´point_debug_020´ ElementDebug 100108
	as_subtitle False
	debug_string TIMER DONE
	show_instigator False
´logic_timer_operator_001´ ElementTimerOperator 100071
	elements
		1 ´cooking_timer_75´
		2 ´cooking_timer_60´
		3 ´cooking_timer_50´
		4 ´cooking_timer_40´
		5 ´cooking_timer_30´
	operation reset
	position 700.0, 175.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	time 0
´logic_toggle_007´ ElementToggle 100109
	elements
		1 ´start_cooking´
	set_trigger_times -1
	toggle on
´restart_cooking´ MissionScriptElement 100110
	on_executed
		´reset´ (delay 0)
´addIngrediense1´ ElementUnitSequence 100124
	position -500.0, -475.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/equipment/interactive_template/ingrediense1 (-199.0, 76.0, 400.0)
			notify_unit_sequence show_interactive
			time 0
´logic_random_002´ ElementRandom 100125
	amount 1
	ignore_disabled True
	on_executed
		´addIngrediense1´ (delay 0)
		´addIngrediense2´ (delay 0)
´ingrediense_bag_area´ ElementAreaTrigger 100127
	TRIGGER TIMES 1
	amount 1
	depth 290
	height 170
	instigator loot
	interval 0.1
	position 25.0, -125.0, 54.0
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 581.924
´debugStartCooking´ MissionScriptElement 100128
	on_executed
		´start_cooking´ (delay 0)
´startedCooking´ ElementUnitSequenceTrigger 100134
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/equipment/interactive_template/startCookingTemp (150.0, 0.0, 0.0)
	on_executed
		´start_cooking´ (delay 0)
´func_instance_output_001´ ElementInstanceOutput 100137
	event picked_up_meth
´need_ing3´ ElementInstanceOutput 100126
	event need_3_ingrediens
´need_ing6´ ElementInstanceOutput 100130
	event need_6_ingrediens
´need_ing9´ ElementInstanceOutput 100131
	event need_9_ingrediens
´func_sequence_001´ ElementUnitSequence 100132
	position -25.0, 200.0, 100.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/equipment/interactive_template/startCookingTemp (150.0, 0.0, 0.0)
			notify_unit_sequence show_interactive
			time 0
´point_waypoint_001´ ElementWaypoint 100133
	icon pd2_loot
	position 273.0, -33.0, 108.652
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´logic_operator_001´ ElementOperator 100135
	elements
		1 ´point_waypoint_001´
	operation remove
