﻿´navlink_jump_out´ ElementSpecialObjective 100003
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Create nav link for enemies with action "e_nl_cs_jump_window" from (-50.0, 0.0, 75.0) to (-330.31, -62.7451, 50.0)
´navlink_jump_in´ ElementSpecialObjective 100005
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Create nav link for enemies with action "e_nl_cs_jump_window" from (-325.0, 23.0, 83.0) to (17.3845, 20.4756, 75.0)
´trigger_special_objective_001´ ElementSpecialObjectiveTrigger 100004
	TRIGGER TIMES 1
	Upon any special objective event:
		"anim_act_04" - ´navlink_jump_out´
		"anim_act_04" - ´navlink_jump_in´
	Execute:
		Execute sequence: "destroy" - units/payday2/architecture/res_ext_apartment_window_05/002 (-195.0, 65.0, 155.0)
´window_destroyed´ ElementUnitSequenceTrigger 100007
	TRIGGER TIMES 1
	Upon sequence "destroy" - units/payday2/architecture/res_ext_apartment_window_05/002 (-195.0, 65.0, 155.0):
		Toggle off: ´trigger_special_objective_001´
