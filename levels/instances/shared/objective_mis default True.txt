﻿´trigger_objective´ ElementInstanceInput 100006
	Upon mission event "trigger_objective":
		´func_objective_004´
		´next_objective´
´func_objective_004´ ElementObjective 100017
	TRIGGER TIMES 1
	Update objective: complete_and_activate None
	Toggle on: ´objective_completed´
	Send instance event "objective_started" to mission.
´next_objective´ ElementInstanceOutput 100008
	DISABLED
	Send instance event "next_objective" to mission.
´objective_completed´ ElementInstanceOutput 100007
	DISABLED
	Send instance event "objective_completed" to mission.
´complete_objective´ ElementInstanceInput 100009
	Upon mission event "complete_objective":
		Update objective: complete None
		´logic_counter_001（1）´ -= 1
´logic_counter_001（1）´ ElementCounter 100019
	Instance var name: var_amount
´bypass´ ElementInstanceInput 100002
	Upon mission event "bypass_objective":
		´logic_toggle_005´
		´logic_toggle_006´
´logic_toggle_005´ ElementToggle 100018
	TRIGGER TIMES 1
	Toggle off: ´func_objective_004´
´logic_toggle_006´ ElementToggle 100016
	TRIGGER TIMES 1
	Toggle on: ´next_objective´
´restart´ ElementInstanceInput 100003
	Upon mission event "restart_objective":
		Toggle on, trigger times 1: ´func_objective_004´
		Toggle on, trigger times 1: ´logic_toggle_006´
		Toggle on, trigger times 1: ´logic_toggle_005´
		Toggle off: ´next_objective´
		´logic_counter_001（1）´ = 1
´logic_counter_trigger_001´ ElementCounterTrigger 100010
	Upon event ´logic_counter_001（1）´ == 0:
		´logic_toggle_005´
		´logic_toggle_006´
		´objective_completed´
