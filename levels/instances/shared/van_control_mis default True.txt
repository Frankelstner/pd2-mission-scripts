﻿´player trigger area´ ElementAreaTrigger 100001
	TRIGGER TIMES 1
	amount 1
	depth 700
	height 500
	instigator player
	interval 0.1
	position 0.0, 100.0, 250.0
	radius 250
	rotation 0.0, 0.0, 0.0, -1.0
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 400
	on_executed
		´func_mission_end_001´ (delay 0)
´loot trigger area´ ElementAreaTrigger 100002
	amount 1
	depth 150
	height 150
	instigator loot
	interval 1
	position 0.0, -100.0, 150.0
	radius 250
	rotation 0.0, 0.0, -0.707107, -0.707107
	shape_type box
	spawn_unit_elements
	trigger_on on_enter
	use_disabled_shapes False
	width 300
	on_executed
		´add bag´ (delay 0.5)
		´remove loot money´ (delay 0.5)
		´remove loot diamonds´ (delay 0)
		´remove loot gold´ (delay 0)
		´remove loot weapon´ (delay 0)
´secure loot money´ ElementCarry 100004
	operation secure
	type_filter money
	on_executed
		´money secured´ (delay 0)
´add bag´ ElementUnitSequence 100005
	position 250.0, -100.0, 223.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_ally (-1450.0, -5625.0, -21.0)
			notify_unit_sequence state_add_loot_bag
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_left_lower (-1784.0, -3481.0, -2.0)
			notify_unit_sequence state_add_loot_bag
			time 0
		3
			id 3
			name run_sequence
			notify_unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_left_upper (2350.0, -4125.0, -2.0)
			notify_unit_sequence state_add_loot_bag
			time 0
		4
			id 4
			name run_sequence
			notify_unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_right_corner (2209.0, -2965.0, -2.0)
			notify_unit_sequence state_add_loot_bag
			time 0
		5
			id 5
			name run_sequence
			notify_unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_leave (-1400.0, -3335.0, -23.0)
			notify_unit_sequence state_add_loot_bag
			time 0
´startup´ MissionScriptElement 100008
	EXECUTE ON STARTUP
	on_executed
		´hide van and escape´ (delay 1)
		´disable triggers´ (delay 0)
´hide van and escape´ ElementDisableUnit 100009
	position -375.0, -25.0, 150.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/props/gen_prop_square_goal_marker_7x4/001 (0.000244141, 100.0, 0.0)
		2 units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_ally (-1450.0, -5625.0, -21.0)
		3 units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_left_lower (-1784.0, -3481.0, -2.0)
		4 units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_left_upper (2350.0, -4125.0, -2.0)
		5 units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_right_corner (2209.0, -2965.0, -2.0)
		6 units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_leave (-1400.0, -3335.0, -23.0)
´disable triggers´ ElementToggle 100010
	TRIGGER TIMES 1
	elements
		1 ´player trigger area´
		2 ´loot trigger area´
	set_trigger_times -1
	toggle off
´play anim arrive ally´ ElementUnitSequence 100011
	position -375.0, -275.0, 203.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_ally (-1450.0, -5625.0, -21.0)
			notify_unit_sequence anim_van_arrive_ally
			time 0
´is anim done´ ElementUnitSequenceTrigger 100012
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence done_van_arrive_ally
			unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_ally (-1450.0, -5625.0, -21.0)
	on_executed
		´open doors´ (delay 0)
´show van arrive alley´ ElementEnableUnit 100013
	position -272.0, -275.0, 200.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_ally (-1450.0, -5625.0, -21.0)
´open doors´ ElementUnitSequence 100014
	position -375.0, -425.0, 200.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_ally (-1450.0, -5625.0, -21.0)
			notify_unit_sequence open_rear_doors
			time 0
	on_executed
		´enable loot trigger´ (delay 0)
´enable loot trigger´ ElementToggle 100015
	TRIGGER TIMES 1
	elements
		1 ´loot trigger area´
	set_trigger_times 0
	toggle on
´enable escape´ ElementInstanceInput 100016
	event enable escape
	on_executed
		´enable escape trigger´ (delay 0)
		´show escape marker´ (delay 0)
´enable escape trigger´ ElementToggle 100017
	elements
		1 ´player trigger area´
	set_trigger_times 1
	toggle on
´show escape marker´ ElementEnableUnit 100007
	position -100.0, -575.0, 150.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/props/gen_prop_square_goal_marker_7x4/001 (0.000244141, 100.0, 0.0)
´func_mission_end_001´ ElementMissionEnd 100018
	position 125.0, -575.0, 150.0
	rotation 0.0, 0.0, 0.0, -1.0
	state success
	on_executed
		´point_debug_002´ (delay 0)
´input van arrive ally´ ElementInstanceInput 100006
	event van arrive ally
	on_executed
		´play anim arrive ally´ (delay 0)
		´show van arrive alley´ (delay 0)
´remove loot money´ ElementCarry 100019
	operation remove
	type_filter money
	on_executed
		´secure loot money´ (delay 0)
´point_debug_002´ ElementDebug 100021
	as_subtitle False
	debug_string Mission Ended
	show_instigator False
´remove loot diamonds´ ElementCarry 100020
	operation remove
	type_filter diamonds
	on_executed
		´secure loot diamonds´ (delay 0)
´secure loot diamonds´ ElementCarry 100022
	operation secure
	type_filter diamonds
	on_executed
		´diamonds secured´ (delay 0)
´remove loot gold´ ElementCarry 100023
	operation remove
	type_filter gold
	on_executed
		´secure loot gold´ (delay 0)
´secure loot gold´ ElementCarry 100024
	operation secure
	type_filter gold
	on_executed
		´gold secured´ (delay 0)
´remove loot weapon´ ElementCarry 100025
	operation remove
	type_filter weapon
	on_executed
		´secure loot weapon´ (delay 0)
´secure loot weapon´ ElementCarry 100026
	operation secure
	type_filter weapon
	on_executed
		´weapon secured´ (delay 0)
´diamonds secured´ ElementInstanceOutput 100028
	event diamonds secured
´money secured´ ElementInstanceOutput 100029
	event money secured
´gold secured´ ElementInstanceOutput 100030
	event gold secured
´weapon secured´ ElementInstanceOutput 100027
	event weapon secured
´input van arrive left lower´ ElementInstanceInput 100032
	event van arrive left lower
	on_executed
		´play anim arrive lower left´ (delay 0)
		´show van arrive lower left´ (delay 0)
´play anim arrive lower left´ ElementUnitSequence 100033
	position -375.0, -275.0, 278.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_left_lower (-1784.0, -3481.0, -2.0)
			notify_unit_sequence anim_van_arrive_left_lower
			time 0
´input van arrive left upper´ ElementInstanceInput 100034
	event van arrive left upper
	on_executed
		´play anim arrive left upper´ (delay 0)
		´show van arrive left upper´ (delay 0)
´play anim arrive left upper´ ElementUnitSequence 100035
	position -375.0, -275.0, 353.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_left_upper (2350.0, -4125.0, -2.0)
			notify_unit_sequence anim_van_arrive_left_upper
			time 0
´input van arrive right corner´ ElementInstanceInput 100036
	event van arrive right corner
	on_executed
		´play anim van arrive right corner´ (delay 0)
		´show van arrive right corner´ (delay 0)
´play anim van arrive right corner´ ElementUnitSequence 100037
	position -375.0, -275.0, 428.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_right_corner (2209.0, -2965.0, -2.0)
			notify_unit_sequence anim_van_arrive_right_corner
			time 0
´show van arrive lower left´ ElementEnableUnit 100038
	position -272.0, -275.0, 275.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_left_lower (-1784.0, -3481.0, -2.0)
´show van arrive right corner´ ElementEnableUnit 100039
	position -272.0, -275.0, 425.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_right_corner (2209.0, -2965.0, -2.0)
´show van arrive left upper´ ElementEnableUnit 100040
	position -272.0, -275.0, 350.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_left_upper (2350.0, -4125.0, -2.0)
´is anim done_´ ElementUnitSequenceTrigger 100041
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 2
			sequence done_van_arrive_left_lower
			unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_left_lower (-1784.0, -3481.0, -2.0)
	on_executed
		´open doors003´ (delay 0)
´is anim done__´ ElementUnitSequenceTrigger 100042
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence done_van_arrive_right_corner
			unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_right_corner (2209.0, -2965.0, -2.0)
	on_executed
		´open doors001´ (delay 0)
´is anim done___´ ElementUnitSequenceTrigger 100043
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence done_van_arrive_left_upper
			unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_left_upper (2350.0, -4125.0, -2.0)
	on_executed
		´open doors002´ (delay 0)
´open doors001´ ElementUnitSequence 100045
	position -375.0, -425.0, 425.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_right_corner (2209.0, -2965.0, -2.0)
			notify_unit_sequence open_rear_doors
			time 0
	on_executed
		´enable loot trigger´ (delay 0)
´open doors002´ ElementUnitSequence 100047
	position -375.0, -425.0, 350.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_left_upper (2350.0, -4125.0, -2.0)
			notify_unit_sequence open_rear_doors
			time 0
	on_executed
		´enable loot trigger´ (delay 0)
´open doors003´ ElementUnitSequence 100048
	position -375.0, -425.0, 275.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_arrive_left_lower (-1784.0, -3481.0, -2.0)
			notify_unit_sequence open_rear_doors
			time 0
	on_executed
		´enable loot trigger´ (delay 0)
´input show van leave´ ElementInstanceInput 100051
	event show van leave
	on_executed
		´show van leave´ (delay 0)
´show van leave´ ElementEnableUnit 100052
	position -272.0, -275.0, 500.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_leave (-1400.0, -3335.0, -23.0)
´input van leave´ ElementInstanceInput 100053
	event van leave
	on_executed
		´play anim van leave´ (delay 0)
		´disable open door animation´ (delay 0)
		´disable triggers´ (delay 0)
´play anim van leave´ ElementUnitSequence 100054
	position -375.0, -350.0, 500.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_leave (-1400.0, -3335.0, -23.0)
			notify_unit_sequence anim_van_leave
			time 0
	on_executed
		´play anim close doors´ (delay 0) DISABLED
´input enable loot´ ElementInstanceInput 100055
	event enable loot
	on_executed
		´enable loot trigger´ (delay 0)
		´play anim van leave open doors´ (delay 0)
		´enable close doors animation´ (delay 0)
´play anim close doors´ ElementUnitSequence 100056
	DISABLED
	position -375.0, -425.0, 500.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_leave (-1400.0, -3335.0, -23.0)
			notify_unit_sequence close_rear_doors
			time 0
´play anim van leave open doors´ ElementUnitSequence 100057
	position -375.0, -600.0, 150.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_leave (-1400.0, -3335.0, -23.0)
			notify_unit_sequence open_rear_doors
			time 0
´enable close doors animation´ ElementToggle 100058
	elements
		1 ´play anim close doors´ DISABLED
	set_trigger_times -1
	toggle on
´van leave anim done´ ElementUnitSequenceTrigger 100050
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence done_van_leave
			unit_id units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_leave (-1400.0, -3335.0, -23.0)
	on_executed
		´hide van leave´ (delay 0)
´hide van leave´ ElementDisableUnit 100059
	position -375.0, -625.0, 500.0
	rotation 0.0, 0.0, 0.0, -1.0
	unit_ids
		1 units/payday2/vehicles/dia_vehicle_van_player_anim_qdia/van_leave (-1400.0, -3335.0, -23.0)
´disable open door animation´ ElementToggle 100060
	elements
		1 ´play anim van leave open doors´
	set_trigger_times -1
	toggle off
