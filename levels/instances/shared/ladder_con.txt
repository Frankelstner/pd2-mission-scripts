ID range vs continent name:
	100000: world

statics
	100000 units/dev_tools/dev_ladder/long (20.0, 0.0, -4.0)
		height 600
		width 80
	100001 units/pd2_dlc_cro/gen_prop_ladder_marker (50.0, 0.0, 0.0)
	100014 units/pd2_dlc_cro/invisible_interaction_ladder/001 (-25.0, 0.0, 100.0)
	100002 units/pd2_dlc_cro/ladder_train/long (0.0, 0.0, 0.0)
	100004 units/pd2_dlc_cro/ladder_train_compact/compact (0.0, 0.0, 0.0)
	100003 units/pd2_dlc_cro/ladder_train_transparent/long (0.0, 0.0, 0.0)
