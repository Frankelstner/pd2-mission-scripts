﻿´ai_spawn_enemy_002´ ElementSpawnEnemyDummy 100003
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_swat_1/ene_swat_1
	force_pickup none
	interval 5
	participate_to_group_ai False
	position 512.91, -141.826, 0.0
	rotation 0.0, 0.0, 0.793353, -0.608762
	spawn_action e_sp_armored_truck_1st
	voice 0
´ai_spawn_enemy_004´ ElementSpawnEnemyDummy 100005
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_swat_1/ene_swat_1
	force_pickup none
	interval 5
	participate_to_group_ai False
	position 464.614, -128.885, 0.0
	rotation 0.0, 0.0, 0.793353, -0.608762
	spawn_action e_sp_armored_truck_2nd
	voice 0
´ai_spawn_enemy_006´ ElementSpawnEnemyDummy 100007
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_swat_1/ene_swat_1
	force_pickup none
	interval 5
	participate_to_group_ai False
	position 416.318, -115.944, 0.0
	rotation 0.0, 0.0, 0.793353, -0.608762
	spawn_action e_sp_armored_truck_3rd
	voice 0
´play anim swat response´ ElementUnitSequence 100009
	position 1100.0, -150.0, 50.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/bnk_vehicle_truck_police_anim_2/001 (-5075.0, 1000.0, 23.0)
			notify_unit_sequence state_show
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/vehicles/bnk_vehicle_truck_police_anim_2/001 (-5075.0, 1000.0, 23.0)
			notify_unit_sequence anim_police_responce
			time 0
´is anim done´ ElementUnitSequenceTrigger 100010
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence done_police_responce
			unit_id units/payday2/vehicles/bnk_vehicle_truck_police_anim_2/001 (-5075.0, 1000.0, 23.0)
	on_executed
		´open doors´ (delay 0)
´open doors´ ElementUnitSequence 100011
	BASE DELAY 0.2
	position 850.0, -150.0, 50.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/bnk_vehicle_truck_police_anim_2/001 (-5075.0, 1000.0, 23.0)
			notify_unit_sequence anim_door_right_open
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/vehicles/bnk_vehicle_truck_police_anim_2/001 (-5075.0, 1000.0, 23.0)
			notify_unit_sequence anim_door_left_open
			time 0
	on_executed
		´ai_spawn_enemy_002´ (delay 0)
		´ai_spawn_enemy_004´ (delay 0)
		´ai_spawn_enemy_006´ (delay 0)
		´output swat spawned´ (delay 0)
´input swat response´ ElementInstanceInput 100000
	event call_swat_response
	on_executed
		´play anim swat response´ (delay 0)
´hide truck´ ElementUnitSequence 100002
	position 1100.0, -275.0, 50.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/vehicles/bnk_vehicle_truck_police_anim_2/001 (-5075.0, 1000.0, 23.0)
			notify_unit_sequence state_hide
			time 0
´startup´ MissionScriptElement 100004
	EXECUTE ON STARTUP
	on_executed
		´hide truck´ (delay 1)
´output swat spawned´ ElementInstanceOutput 100006
	event swat_spawned
