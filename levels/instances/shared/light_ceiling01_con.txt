ID range vs continent name:
	100000: world

statics
	100001 units/lights/light_omni_shadow_projection_01/001 (0.0, 0.0, -75.0)
		disable_shadows True
		hide_on_projection_light True
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 0.776471, 0.933333, 0.984314
			enabled True
			falloff_exponent 1
			far_range 280
			multiplier identity
			name lo_omni
			near_range 80
			spot_angle_end -1
			spot_angle_start -1
		projection_light lo_omni
	100004 units/pd2_dlc_man/props/lamp/man_prop_int_cellar_ceiling_lamp/001 (0.0, 0.0, 0.0)
		disable_shadows True
		hide_on_projection_light True
		mesh_variation light_on
