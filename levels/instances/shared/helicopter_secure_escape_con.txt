ID range vs continent name:
	100000: world

statics
	100020 units/payday2/props/gen_prop_square_goal_marker_8x15/escape_marker (25.0, 0.0, 0.0)
	100000 units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/001 (300.0, 0.0, -1760.0)
		1
			clipping_values 10000.0, -10000.0, 0.0
			color 1.0, 0.861, 0.584
			enabled True
			falloff_exponent 4
			far_range 150
			multiplier identity
			name lo_omni
			near_range 0
			spot_angle_end 0
			spot_angle_start 0
