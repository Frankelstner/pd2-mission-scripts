﻿´startup´ ElementUnitSequence 100014
	EXECUTE ON STARTUP
	BASE DELAY  (DELAY 1)
	Execute sequence: "hidden" - units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/001 (300.0, 0.0, -1760.0)
	Execute sequence: "hide" - units/payday2/props/gen_prop_square_goal_marker_8x15/escape_marker (25.0, 0.0, 0.0)
´secure_area_trigger´ ElementAreaTrigger 100001
	DISABLED
	Box, width 150.0, height 150.0, depth 150.0, at position (300.0, 15.0, 159.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: loot
	Interval: 0.1
	Rule: ´data_instigator_rule_001´
	Upon on_enter, execute:
		Teammate comment: p27
		If instigator loot type is "cro_loot1":
			Loot action: secure
			If instigator loot type is "cro_loot1":
				Loot action: remove
				Send instance event "cro_loot_secured" to mission.
		If instigator loot type is "cro_loot2":
			Loot action: secure
			If instigator loot type is "cro_loot2":
				Loot action: remove
				Send instance event "cro_loot_secured" to mission.
		If instigator loot type is "coke":
			Loot action: secure
			If instigator loot type is "coke":
				Loot action: remove
		If instigator loot type is "diamonds":
			Loot action: secure
			If instigator loot type is "diamonds":
				Loot action: remove
		If instigator loot type is "gold":
			Loot action: secure
			If instigator loot type is "gold":
				Loot action: remove
		If instigator loot type is "money":
			Loot action: secure
			If instigator loot type is "money":
				Loot action: remove
´data_instigator_rule_001´ ElementInstigatorRule 100031
	coke,cro_loot1,cro_loot2,diamonds,gold,money
´end_level_trigger´ ElementAreaTrigger 100004
	DISABLED
	TRIGGER TIMES 1
	Box, width 300.0, height 300.0, depth 400.0, at position (34.0, 0.0, 100.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: all
	Instigator: player
	Interval: 1.0
	Upon on_enter, execute:
		Teammate comment: g24
		Send instance event "output_all_escape_area" to mission.
		Send instance event "mission_end" to mission.
		End mission: success (DELAY 2)
´input_call_heli´ ElementInstanceInput 100005
	Upon mission event "call_helicopter":
		Execute sequence: "diamondheist" - units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/001 (300.0, 0.0, -1760.0)
		Execute sequence: "flyin_fwd_hover" - units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/001 (300.0, 0.0, -1760.0)
		Execute sequence: "open_door_left" - units/payday2/vehicles/air_vehicle_blackhawk/helicopter_cops_ref/001 (300.0, 0.0, -1760.0) (DELAY 25)
		´logic_toggle_001´ (DELAY 25)
´logic_toggle_001´ ElementToggle 100008
	TRIGGER TIMES 1
	Toggle on, trigger times 0: ´secure_area_trigger´
´input_enable_escape´ ElementInstanceInput 100012
	Upon mission event "enable_escape":
		Execute sequence: "show" - units/payday2/props/gen_prop_square_goal_marker_8x15/escape_marker (25.0, 0.0, 0.0)
		Toggle on, trigger times 1: ´end_level_trigger´
