﻿´FX_flare´ ElementPlayEffect 100001
	base_time 0
	effect effects/payday2/environment/bengal_flare
	max_amount 0
	position 0.0, 0.0, 0.0
	random_time 0
	rotation 0.0, 0.0, 0.0, -1.0
	screen_space False
´sound_flare_start´ ElementPlaySound 100002
	append_prefix False
	elements
	position 0.0, 0.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	sound_event flare_start_loop
	use_instigator False
´input_show´ ElementInstanceInput 100003
	event show
	on_executed
		´seq_flare_show´ (delay 0)
´input_enable_interaction´ ElementInstanceInput 100004
	event enable_interaction
	on_executed
		´seq_flare_enable_interaction´ (delay 0)
´input_show_interactable´ ElementInstanceInput 100005
	event show_interactable
	on_executed
		´seq_flare_show´ (delay 0)
		´seq_flare_enable_interaction´ (delay 0)
´STARTUP_flare´ MissionScriptElement 100006
	EXECUTE ON STARTUP
	on_executed
		´DELAY_flare´ (delay 0.2)
´DELAY_flare´ MissionScriptElement 100007
	on_executed
		´seq_flare_disable´ (delay 0)
´seq_flare_disable´ ElementUnitSequence 100008
	position -700.0, -250.0, 200.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 2
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_roadflare/flare (0.0, 0.0, 0.0)
			notify_unit_sequence state_interaction_disabled
			time 0
´seq_flare_show´ ElementUnitSequence 100009
	position -475.0, -125.0, 200.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_roadflare/flare (0.0, 0.0, 0.0)
			notify_unit_sequence state_vis_show
			time 0
´seq_flare_enable_interaction´ ElementUnitSequence 100010
	position -325.0, -125.0, 200.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_roadflare/flare (0.0, 0.0, 0.0)
			notify_unit_sequence state_interaction_enabled
			time 0
	on_executed
		´seq_trg_flare_interact_ON´ (delay 0)
´seq_trg_flare_interact´ ElementUnitSequenceTrigger 100011
	DISABLED
	TRIGGER TIMES 1
	sequence_list
		1
			guis_id 1
			sequence interact
			unit_id units/payday2/equipment/gen_interactable_roadflare/flare (0.0, 0.0, 0.0)
	on_executed
		´FX_flare´ (delay 0)
		´sound_flare_start´ (delay 0)
		´flare_state_valid_OFF´ (delay 0)
		´output_flare_used´ (delay 0)
		´wp_flare_REMOVE´ (delay 0)
´seq_trg_flare_interact_ON´ ElementToggle 100012
	elements
		1 ´seq_trg_flare_interact´ DISABLED
	set_trigger_times 1
	toggle on
´input_stop´ ElementInstanceInput 100013
	event stop
	on_executed
		´FX_flare_stop´ (delay 0)
		´sound_flare_stop´ (delay 0)
		´flare_state_valid_ON´ (delay 0)
´FX_flare_stop´ ElementStopEffect 100014
	elements
		1 ´FX_flare´
	operation fade_kill
	position -325.0, -475.0, 200.0
	rotation 0.0, 0.0, 0.0, -1.0
´sound_flare_stop´ ElementPlaySound 100015
	append_prefix False
	elements
	position 0.0, 0.0, 0.0
	rotation 0.0, 0.0, 0.0, -1.0
	sound_event flare_end
	use_instigator False
´input_reset´ ElementInstanceInput 100016
	event reset
	on_executed
		´flare_state_valid´ (delay 0) DISABLED
´seq_trg_flare_interact_ON_´ ElementToggle 100017
	elements
		1 ´seq_trg_flare_interact´ DISABLED
	set_trigger_times 1
	toggle on
´seq_flare_reset´ ElementUnitSequence 100018
	position -225.0, -575.0, 200.0
	rotation 0.0, 0.0, 0.0, -1.0
	trigger_list
		1
			id 1
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_roadflare/flare (0.0, 0.0, 0.0)
			notify_unit_sequence state_vis_show
			time 0
		2
			id 2
			name run_sequence
			notify_unit_id units/payday2/equipment/gen_interactable_roadflare/flare (0.0, 0.0, 0.0)
			notify_unit_sequence state_interaction_enabled
			time 0
´flare_state_valid´ MissionScriptElement 100019
	DISABLED
	on_executed
		´seq_flare_reset´ (delay 0)
		´seq_trg_flare_interact_ON_´ (delay 0)
´flare_state_valid_ON´ ElementToggle 100020
	elements
		1 ´flare_state_valid´ DISABLED
	set_trigger_times -1
	toggle on
´flare_state_valid_OFF´ ElementToggle 100021
	elements
		1 ´flare_state_valid´ DISABLED
	set_trigger_times -1
	toggle off
´output_flare_used´ ElementInstanceOutput 100022
	event flare_used
´wp_flare´ ElementWaypoint 100023
	icon pd2_fire
	position 0.0, 0.0, 75.0
	rotation 0.0, 0.0, 0.0, -1.0
	text_id debug_none
´wp_flare_REMOVE´ ElementOperator 100024
	elements
		1 ´wp_flare´
	operation remove
´input_show_WP´ ElementInstanceInput 100025
	event show_WP
	on_executed
		´wp_flare´ (delay 0)
