﻿´logic_link_056´ MissionScriptElement 102531
	EXECUTE ON STARTUP
	on_executed
		´logic_operator_022´ (delay 0)
´func_scenario_event_009´ ElementScenarioEvent 102578
	TRIGGER TIMES 1
	amount 1
	base_chance 1
	chance_inc 0
	position -1975.0, -2725.0, 500.001
	rotation 0.0, 0.0, 0.0, -1.0
	task any
	on_executed
		´logic_link_057´ (delay 0)
		´logic_link_058´ (delay 0)
´logic_link_057´ MissionScriptElement 102580
	on_executed
		´logic_operator_021´ (delay 1)
´logic_operator_021´ ElementOperator 102581
	elements
		1 ´func_scenario_event_009´
	operation remove
´logic_operator_022´ ElementOperator 102582
	elements
		1 ´func_scenario_event_009´
	operation add
´logic_link_058´ MissionScriptElement 102583
	TRIGGER TIMES 1
	on_executed
		´func_execute_in_other_mission_013´ (delay 0)
		´ai_spawn_enemy_window_meeting005´ (delay 2)
		´ai_spawn_enemy_window_meeting003´ (delay 1.4)
		´ai_spawn_enemy_window_meeting002´ (delay 3)
´ai_spawn_enemy_window_meeting001´ ElementSpawnEnemyDummy 100217
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_tazer_1/ene_tazer_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position -2725.0, -2725.0, 475.0
	rotation 0.0, 0.0, 0.707107, -0.707107
	spawn_action e_sp_repel_into_window
	team default
	voice 0
´ai_spawn_enemy_window_meeting002´ ElementSpawnEnemyDummy 100483
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_tazer_1/ene_tazer_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position -2725.0, -2300.0, 475.0
	rotation 0.0, 0.0, 0.707107, -0.707107
	spawn_action e_sp_repel_into_window
	team default
	voice 0
´ai_spawn_enemy_window_meeting003´ ElementSpawnEnemyDummy 103797
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_swat_1/ene_swat_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position -2725.0, -2725.0, 475.0
	rotation 0.0, 0.0, 0.707107, -0.707107
	spawn_action e_sp_repel_into_window
	team default
	voice 0
´ai_spawn_enemy_window_meeting004´ ElementSpawnEnemyDummy 103798
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_swat_1/ene_swat_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position -2725.0, -2300.0, 475.0
	rotation 0.0, 0.0, 0.707107, -0.707107
	spawn_action e_sp_repel_into_window
	team default
	voice 0
´func_execute_in_other_mission_013´ ElementExecuteInOtherMission 103980
	position -3150.0, -2300.0, 625.0
	rotation 0.0, 0.0, 0.0, -1.0
	on_executed
		´logic_toggle_034´ (delay 0)
´ai_spawn_enemy_window_meeting005´ ElementSpawnEnemyDummy 103019
	accessibility any
	amount 0
	enemy units/payday2/characters/ene_swat_1/ene_swat_1
	force_pickup none
	interval 5
	participate_to_group_ai True
	position -2725.0, -1825.0, 475.0
	rotation 0.0, 0.0, 0.707107, -0.707107
	spawn_action e_sp_repel_into_window
	team default
	voice 0
