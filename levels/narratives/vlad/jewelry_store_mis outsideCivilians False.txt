﻿´startup_2´ MissionScriptElement 101969
	EXECUTE ON STARTUP
	remove ´hookersso2a001´
	remove ´hookersso2b001´
	remove ´hookersso2a´
	remove ´hookersso2b´
	´civspawn_ai_civilian_group_006（6）´
´civspawn_ai_civilian_group_006（6）´ ElementSpawnCivilianGroup 101968
	Spawn 6 civilians, chosen at random (do not ignore disabled):
		´civ_male_casual_5´
		´civ_male_casual_6´
		´civ_male_casual_3_2´
		´civ_male_casual_2´
		´civ_male_casual_6_2´
		´civ_male_casual_1´
´civ_male_casual_5´ ElementSpawnCivilian 101955
	Spawn civ: civ_male_casual_5 (-4525.0, 1025.0, 25.0)
´civ_male_casual_6´ ElementSpawnCivilian 101957
	Spawn civ: civ_male_casual_6 (-8900.0, 925.0, 25.0)
´civ_male_casual_3_2´ ElementSpawnCivilian 101962
	Spawn civ: civ_male_casual_3 (-6875.0, 3125.0, 25.0)
´civ_male_casual_2´ ElementSpawnCivilian 101949
	Spawn civ: civ_male_casual_2 (-3675.0, 7650.0, 25.0)
´civ_male_casual_6_2´ ElementSpawnCivilian 101947
	Spawn civ: civ_male_casual_6 (5200.0, 1050.0, 25.0)
´civ_male_casual_1´ ElementSpawnCivilian 101941
	Spawn civ: civ_male_casual_1 (4200.0, 2875.0, 24.9999)
´hookersso2a001´ ElementSpecialObjective 103567
	Make instigator path towards (-1425.0, 1000.0, 25.1865) and show "None" for 0-1 seconds.
	Afterwards, choose new objective:
		´ai_so_group_001´
		´hookersso2b001´
´ai_so_group_001´ ElementSpecialObjectiveGroup 101920
	Position: (-3025.0, 1750.0, 775.0)
	Followup objectives for instigator:
		´point_special_objective_030´
		´point_special_objective_018´
		´point_special_objective_029´
		´point_special_objective_015´
		´point_special_objective_031´
		´point_special_objective_021´
		´point_special_objective_032´
		´point_special_objective_033´
		´point_special_objective_025´
		´point_special_objective_026´
		´hookersso2a001´
		´hookersso2a´
		´point_special_objective_085´
		´point_special_objective_087´
´point_special_objective_030´ ElementSpecialObjective 101915
	Make instigator path towards (-3425.0, 1250.0, 25.0) and show "None" for 5-25 seconds. Afterwards, choose new objective: ´ai_so_group_001´
´point_special_objective_018´ ElementSpecialObjective 101793
	Make instigator path towards (-3500.0, 1375.0, 25.0) and show "None" for 5-25 seconds.
	Afterwards, choose new objective:
		´point_special_objective_017´
		´ai_so_group_001´
´point_special_objective_017´ ElementSpecialObjective 101792
	Make instigator path towards (-3500.0, 2525.0, 25.0) and show "None" for 5-25 seconds. Afterwards, choose new objective: ´ai_so_group_002´
´ai_so_group_002´ ElementSpecialObjectiveGroup 101922
	Position: (-3050.0, 1900.0, 775.0)
	Followup objectives for instigator:
		´point_special_objective_016´
		´point_special_objective_017´
		´point_special_objective_019´
		´point_special_objective_022´
		´point_special_objective_028´
´point_special_objective_016´ ElementSpecialObjective 101791
	Make instigator path towards (-6850.0, 2850.0, 25.0) and show "None" for 5-25 seconds. Afterwards, choose new objective: ´ai_so_group_002´
´point_special_objective_019´ ElementSpecialObjective 101794
	Make instigator path towards (-3300.0, 2700.0, 25.0) and show "None" for 5-25 seconds.
	Afterwards, choose new objective:
		´point_special_objective_020´
		´ai_so_group_002´
´point_special_objective_020´ ElementSpecialObjective 101795
	Make instigator path towards (-2175.0, 2725.0, 25.0) and show "None" for 5-25 seconds.
	Afterwards, choose new objective:
		´point_special_objective_019´
		´ai_so_group_003´
´ai_so_group_003´ ElementSpecialObjectiveGroup 101895
	Position: (-2875.0, 1900.0, 775.0)
	Followup objectives for instigator:
		´point_special_objective_020´
		´point_special_objective_035´
		´point_special_objective_034´
		´point_special_objective_024´
		´point_special_objective_027´
		´point_special_objective_084´
		´point_special_objective_085´
		´point_special_objective_086´
		´point_special_objective_088´
´point_special_objective_035´ ElementSpecialObjective 101921
	Make instigator path towards (-1800.0, 2525.0, 25.0) and show "None" for 5-25 seconds. Afterwards, choose new objective: ´ai_so_group_003´
´point_special_objective_034´ ElementSpecialObjective 101919
	Make instigator path towards (2800.0, 2675.0, 25.0) and show "None" for 5-25 seconds. Afterwards, choose new objective: ´ai_so_group_003´
´point_special_objective_024´ ElementSpecialObjective 101896
	Make instigator path towards (3875.0, 2525.0, 25.0) and show "None" for 5-25 seconds. Afterwards, choose new objective: ´ai_so_group_003´
´point_special_objective_027´ ElementSpecialObjective 101899
	Make instigator path towards (4200.0, 3000.0, 25.0) and show "None" for 5-25 seconds.
´point_special_objective_084´ ElementSpecialObjective 103572
	Make instigator path towards (495.0, 2604.0, 25.0) and show "cmf_so_sit_in_chair" for 30-45 seconds. Afterwards, choose new objective: ´ai_so_group_003´
´point_special_objective_085´ ElementSpecialObjective 103577
	Make instigator path towards (-601.0, 1302.0, 25.0) and show "cmf_so_sit_in_chair" for 30-45 seconds. Afterwards, choose new objective: ´ai_so_group_001´
´point_special_objective_086´ ElementSpecialObjective 103578
	Make instigator path towards (-250.0, 2700.0, 25.0) and show "cmf_so_answer_phone" for 30-45 seconds. Afterwards, choose new objective: ´ai_so_group_003´
´point_special_objective_088´ ElementSpecialObjective 103580
	Make instigator path towards (275.0, 3225.0, 25.0) and show "cmf_so_answer_phone" for 30-45 seconds.
	Afterwards, choose new objective:
		´ai_so_group_003´
		´point_special_objective_089shouldbeLoop´
´point_special_objective_089shouldbeLoop´ ElementSpecialObjective 103581
	Make instigator path towards (274.0, 3233.0, 25.0) and show "cmf_so_answer_phone" for 30-45 seconds.
	Afterwards, choose new objective:
		´ai_so_group_003´
		´point_special_objective_088´
´point_special_objective_022´ ElementSpecialObjective 101894
	Make instigator path towards (-3375.0, 5375.0, 25.0) and show "None" for 5-25 seconds. Afterwards, choose new objective: ´ai_so_group_002´
´point_special_objective_028´ ElementSpecialObjective 101900
	Make instigator path towards (-4000.0, 7650.0, 25.0) and show "None" for 5-25 seconds.
´point_special_objective_029´ ElementSpecialObjective 101914
	Make instigator path towards (-4675.0, 800.0, 25.0) and show "None" for 5-10 seconds.
´point_special_objective_015´ ElementSpecialObjective 101796
	Make instigator path towards (-9037.0, 1042.0, 25.0004) and show "None" for 5-25 seconds.
´point_special_objective_031´ ElementSpecialObjective 101916
	Make instigator path towards (-1850.0, 1300.0, 25.0) and show "None" for 5-25 seconds. Afterwards, choose new objective: ´ai_so_group_001´
´point_special_objective_021´ ElementSpecialObjective 101797
	Make instigator path towards (-1550.0, 1350.0, 25.0) and show "None" for 5-25 seconds.
	Afterwards, choose new objective:
		´point_special_objective_035´
		´ai_so_group_001´
´point_special_objective_032´ ElementSpecialObjective 101917
	Make instigator path towards (-175.0, 1225.0, 25.0) and show "cmf_so_look_windows" for 5-25 seconds.
	Afterwards, choose new objective:
		´ai_so_group_001´
		´point_special_objective_073´
´point_special_objective_073´ ElementSpecialObjective 103464
	Make instigator path towards (216.0, 763.0, 24.9999) and show "None" for 2-3 seconds.
	Afterwards, choose new objective:
		´point_special_objective_075´
		´point_special_objective_074´
´point_special_objective_075´ ElementSpecialObjective 103466
	Make instigator path towards (-74.9167, 350.0, 25.0) and show "None" for 9-21 seconds. Afterwards, choose new objective: ´point_special_objective_074´
´point_special_objective_074´ ElementSpecialObjective 103465
	Make instigator path towards (425.0, 300.0, 24.9999) and show "None" for 9-21 seconds. Afterwards, choose new objective: ´point_special_objective_075´
´point_special_objective_033´ ElementSpecialObjective 101918
	Make instigator path towards (1800.0, 1325.0, 25.0) and show "cmf_so_look_windows" for 5-25 seconds. Afterwards, choose new objective: ´ai_so_group_001´
´point_special_objective_025´ ElementSpecialObjective 101897
	Make instigator path towards (3850.0, 1350.0, 25.0) and show "None" for 5-25 seconds.
	Afterwards, choose new objective:
		´point_special_objective_024´
		´ai_so_group_001´
´point_special_objective_026´ ElementSpecialObjective 101898
	Make instigator path towards (4975.0, 1050.0, 25.0) and show "None" for 5-25 seconds.
´hookersso2a´ ElementSpecialObjective 103564
	Make instigator path towards (1150.0, 1225.0, 25.0) and show "None" for 2-5 seconds.
	Afterwards, choose new objective:
		´ai_so_group_001´
		´hookersso2b´
´hookersso2b´ ElementSpecialObjective 103565
	Make instigator path towards (1075.0, 275.0, 24.9999) and show "None" for 20-45 seconds. Afterwards, choose new objective: ´ai_so_group_001´
´point_special_objective_087´ ElementSpecialObjective 103579
	Make instigator path towards (550.0, 1350.0, 25.0) and show "cmf_so_answer_phone" for 30-45 seconds. Afterwards, choose new objective: ´ai_so_group_001´
´hookersso2b001´ ElementSpecialObjective 103568
	Make instigator path towards (-1325.0, -275.0, 30.8839) and show "None" for 20-45 seconds. Afterwards, choose new objective: ´ai_so_group_001´
´ai_enemy_trigger_004´ ElementEnemyDummyTrigger 101944
	Upon any event:
		spawn: ´civ_male_casual_5´
		spawn: ´civ_male_casual_3_3´
		spawn: ´civ_male_casual_3_4´
		spawn: ´civ_male_casual_6´
		spawn: ´civ_male_casual_3_6´
		spawn: ´civ_male_casual_3_5´
		spawn: ´civ_male_casual_6_2´
		spawn: ´civ_male_casual_4_2´
		spawn: ´civ_male_casual_5_3´
	Execute:
		´ai_so_group_001´
´civ_male_casual_3_3´ ElementSpawnCivilian 101954
	Spawn civ: civ_male_casual_3 (-4650.0, 1000.0, 25.0)
´civ_male_casual_3_4´ ElementSpawnCivilian 101953
	Spawn civ: civ_male_casual_3 (-4750.0, 1000.0, 25.0)
´civ_male_casual_3_6´ ElementSpawnCivilian 101958
	Spawn civ: civ_male_casual_3 (-9000.0, 925.0, 25.0)
´civ_male_casual_3_5´ ElementSpawnCivilian 101959
	Spawn civ: civ_male_casual_3 (-8925.0, 1025.0, 25.0)
´civ_male_casual_4_2´ ElementSpawnCivilian 101945
	Spawn civ: civ_male_casual_4 (5100.0, 1050.0, 25.0)
´civ_male_casual_5_3´ ElementSpawnCivilian 101946
	Spawn civ: civ_male_casual_5 (5065.0, 1033.0, 25.0)
´ai_enemy_trigger_005´ ElementEnemyDummyTrigger 101965
	Upon any event:
		spawn: ´civ_male_casual_3_8´
		spawn: ´civ_male_casual_6_3´
		spawn: ´civ_male_casual_3_2´
		spawn: ´civ_male_casual_5_2´
		spawn: ´civ_male_casual_4´
		spawn: ´civ_male_casual_2´
	Execute:
		´ai_so_group_002´
´civ_male_casual_3_8´ ElementSpawnCivilian 101963
	Spawn civ: civ_male_casual_3 (-7050.0, 3125.0, 25.0)
´civ_male_casual_6_3´ ElementSpawnCivilian 101961
	Spawn civ: civ_male_casual_6 (-6975.0, 3125.0, 25.0)
´civ_male_casual_5_2´ ElementSpawnCivilian 101951
	Spawn civ: civ_male_casual_5 (-3900.0, 7625.0, 25.0)
´civ_male_casual_4´ ElementSpawnCivilian 101950
	Spawn civ: civ_male_casual_4 (-3775.0, 7650.0, 25.0)
´ai_enemy_trigger_006´ ElementEnemyDummyTrigger 101967
	Upon any event:
		spawn: ´civ_male_casual_1´
		spawn: ´civ_male_casual_2_2´
		spawn: ´civ_male_casual_3_7´
	Execute:
		´ai_so_group_003´
´civ_male_casual_2_2´ ElementSpawnCivilian 101942
	Spawn civ: civ_male_casual_2 (4162.0, 2903.0, 25.0)
´civ_male_casual_3_7´ ElementSpawnCivilian 101943
	Spawn civ: civ_male_casual_3 (4275.0, 2875.0, 25.0)
´ai_enemy_trigger_003´ ElementEnemyDummyTrigger 102534
	TRIGGER TIMES 4
	Upon any event:
		death: ´civspawn_ai_civilian_group_006（6）´
		death: ´civspawn_ai_civilian_group_001（1）´
		death: ´civspawn_ai_civilian_group_002（1）´
		death: ´civspawn_ai_civilian_group_004（1）´
		death: ´civspawn_ai_civilian_group_003（1）´
	Execute:
		´func_execute_in_other_mission_001´
´civspawn_ai_civilian_group_001（1）´ ElementSpawnCivilianGroup 101956
	Spawn 1 civilians, chosen at random (do not ignore disabled):
		´civ_male_casual_5´
		´civ_male_casual_3_3´
		´civ_male_casual_3_4´
´civspawn_ai_civilian_group_002（1）´ ElementSpawnCivilianGroup 101966
	Spawn 1 civilians, chosen at random (do not ignore disabled):
		´civ_male_casual_3_5´
		´civ_male_casual_3_6´
		´civ_male_casual_6´
´civspawn_ai_civilian_group_004（1）´ ElementSpawnCivilianGroup 101952
	Spawn 1 civilians, chosen at random (do not ignore disabled):
		´civ_male_casual_6_2´
		´civ_male_casual_4_2´
		´civ_male_casual_5_3´
´civspawn_ai_civilian_group_003（1）´ ElementSpawnCivilianGroup 101960
	Spawn 1 civilians, chosen at random (do not ignore disabled):
		´civ_male_casual_5_2´
		´civ_male_casual_4´
		´civ_male_casual_2´
´func_execute_in_other_mission_001´ ElementExecuteInOtherMission 102535
	´civkilledoutside´
´trigger_special_objective_001´ ElementSpecialObjectiveTrigger 103017
	Upon special objective event "complete" - ´point_special_objective_026´:
		Remove instigator from game.
		´civSpawner003´
´civSpawner003´ MissionScriptElement 101932
	´logic_random_018´
´logic_random_018´ ElementRandom 103576
	Execute 1:
		´civ_male_casual_5_3´
		´civ_male_casual_4_2´
		´civ_male_casual_6_2´
´trigger_special_objective_004´ ElementSpecialObjectiveTrigger 103018
	Upon special objective event "complete" - ´point_special_objective_027´:
		Remove instigator from game.
		´civSpawner004´
´civSpawner004´ MissionScriptElement 101933
	´civspawn_ai_civilian_group_005（1）´
´civspawn_ai_civilian_group_005（1）´ ElementSpawnCivilianGroup 101948
	Spawn 1 civilians, chosen at random (do not ignore disabled):
		´civ_male_casual_1´
		´civ_male_casual_2_2´
		´civ_male_casual_3_7´
´trigger_special_objective_005´ ElementSpecialObjectiveTrigger 103019
	Upon special objective event "complete" - ´point_special_objective_028´:
		Remove instigator from game.
		´civSpawner002´
´civSpawner002´ MissionScriptElement 101931
	´civspawn_ai_civilian_group_003（1）´
´trigger_special_objective_006´ ElementSpecialObjectiveTrigger 103020
	Upon special objective event "complete" - ´point_special_objective_029´:
		Remove instigator from game.
		´civSpawner005´
´civSpawner005´ MissionScriptElement 101934
	´civspawn_ai_civilian_group_001（1）´
´trigger_special_objective_007´ ElementSpecialObjectiveTrigger 103021
	Upon special objective event "complete" - ´point_special_objective_016´:
		Remove instigator from game.
		´civSpawner001´
´civSpawner001´ MissionScriptElement 101930
	´logic_random_008´
´logic_random_008´ ElementRandom 101964
	Execute 1 (do not ignore disabled):
		´civ_male_casual_3_8´
		´civ_male_casual_6_3´
		´civ_male_casual_3_2´
´trigger_special_objective_009´ ElementSpecialObjectiveTrigger 103022
	Upon special objective event "complete" - ´point_special_objective_015´:
		Remove instigator from game.
		Debug message: please remove
		Debug message: completed
		´civSpawner´
´civSpawner´ MissionScriptElement 101929
	´logic_random_017´
´logic_random_017´ ElementRandom 103573
	Execute 1:
		´civ_male_casual_3_6´
		´civ_male_casual_3_5´
		´civ_male_casual_6´
