﻿´trigger_special_objective_001´ ElementSpecialObjectiveTrigger 103257
	Upon special objective event "anim_start" - ´SO_place_C4001´:
		´logic_counter_002（3）´
		Execute sequence: "show" - units/equipment/charge/c4_plantable/006 (-3336.0, 2731.0, 144.949) (DELAY 1)
		remove ´SO_place_C4001´ (DELAY 1)
´SO_place_C4001´ ElementSpecialObjective 100598
	Create objective for enemies in 3000 radius around (-3469.22, 2687.59, -24.9984)
	Make chosen person path towards objective and show "e_so_plant_c4_hi".
´logic_counter_002（3）´ ElementCounter 100358
	When counter target reached: 
		´logic_counter_001（1）´ (DELAY 3)
´logic_counter_001（1）´ ElementCounter 100361
	When counter target reached: 
		Execute sequence: "trigger_expolsive" - units/equipment/charge/c4_plantable/006 (-3336.0, 2731.0, 144.949)
		Execute sequence: "trigger_expolsive" - units/equipment/charge/c4_plantable/007 (-3335.0, 2284.0, 149.949)
		Execute sequence: "trigger_expolsive" - units/equipment/charge/c4_plantable/004 (-3336.0, 1842.0, 146.949)
		Execute sequence: "destroy_02" - units/world/architecture/bank/bank_glass_a/012 (-2588.0, 2597.0, 196.0)
		Execute sequence: "destroy_02" - units/world/architecture/bank/bank_glass_a/011 (-2588.0, 2597.0, -12.0)
		Execute sequence: "destroy_02" - units/world/architecture/bank/bank_glass_a/009 (-2338.0, 2597.0, -12.0)
		Execute sequence: "destroy_02" - units/world/architecture/bank/bank_glass_a/010 (-2338.0, 2597.0, 196.0)
		Execute sequence: "destroy_01" - units/world/architecture/bank/bank_glass_a/008 (-2078.0, 2597.0, 196.0)
		Execute sequence: "destroy_01" - units/world/architecture/bank/bank_glass_a/007 (-2078.0, 2597.0, -12.0)
		Execute sequence: "destroy_01" - units/world/architecture/bank/bank_glass_a/006 (-2028.0, 2338.0, 196.0)
		Execute sequence: "destroy_01" - units/world/architecture/bank/bank_glass_a/005 (-2028.0, 2338.0, -12.0)
		Execute sequence: "destroy_01" - units/world/architecture/bank/bank_glass_a/003 (-2028.0, 1953.0, -12.0)
		Execute sequence: "destroy_01" - units/world/architecture/bank/bank_glass_a/004 (-2028.0, 1953.0, 196.0)
		Execute sequence: "destroy_01" - units/world/architecture/bank/bank_glass_a/001 (-2028.0, 1568.0, 196.0)
		Execute sequence: "destroy_01" - units/world/architecture/bank/bank_glass_a/002 (-2028.0, 1568.0, -12.0)
		Execute sequence: "hide" - units/equipment/charge/c4_plantable/006 (-3336.0, 2731.0, 144.949) (DELAY 5)
		Execute sequence: "hide" - units/equipment/charge/c4_plantable/007 (-3335.0, 2284.0, 149.949) (DELAY 5)
		Execute sequence: "hide" - units/equipment/charge/c4_plantable/004 (-3336.0, 1842.0, 146.949) (DELAY 5)
		´func_execute_in_other_mission_006´ (DELAY 5)
´func_execute_in_other_mission_006´ ElementExecuteInOtherMission 103866
	´env_effect_play_010´
	´env_effect_play_009´
	´func_execute_in_other_mission_007´ (DELAY 20)
´func_execute_in_other_mission_007´ ElementExecuteInOtherMission 103878
	Stop effect: ´env_effect_play_009´
	Stop effect: ´env_effect_play_010´
´trigger_special_objective_002´ ElementSpecialObjectiveTrigger 100348
	Upon special objective event "anim_start" - ´SO_place_C4002´:
		´logic_counter_002（3）´
		Execute sequence: "show" - units/equipment/charge/c4_plantable/007 (-3335.0, 2284.0, 149.949) (DELAY 1)
		remove ´SO_place_C4002´ (DELAY 1)
´SO_place_C4002´ ElementSpecialObjective 102509
	Create objective for enemies in 3000 radius around (-3474.91, 2248.23, -24.9983)
	Make chosen person path towards objective and show "e_so_plant_c4_hi".
´trigger_special_objective_003´ ElementSpecialObjectiveTrigger 103261
	Upon special objective event "anim_start" - ´SO_place_C4003´:
		Debug message: wndw offff
		´logic_counter_002（3）´
		Execute sequence: "show" - units/equipment/charge/c4_plantable/004 (-3336.0, 1842.0, 146.949) (DELAY 1)
		remove ´SO_place_C4003´ (DELAY 1)
´SO_place_C4003´ ElementSpecialObjective 103238
	Create objective for enemies in 3000 radius around (-3477.61, 1860.35, -24.9983)
	Make chosen person path towards objective and show "e_so_plant_c4_hi".
