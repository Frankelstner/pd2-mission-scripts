﻿´trigger_special_objective_004´ ElementSpecialObjectiveTrigger 103801
	Upon special objective event "anim_start" - ´SO_window_C4003´:
		remove ´SO_window_C4003´
		´logic_counter_005（3）´
		Execute sequence: "show" - units/equipment/charge/c4_plantable/011 (-2807.0, -1844.0, 103.949) (DELAY 0.7)
´SO_window_C4003´ ElementSpecialObjective 103800
	Create nav link for enemies with action "e_so_plant_c4_hi" from (-2906.0, -1828.0, -25.0) to (-2886.31, -1882.15, -24.9983)
´logic_counter_005（3）´ ElementCounter 102258
	When counter target reached: 
		Execute sequence: "trigger_expolsive" - units/equipment/charge/c4_plantable/011 (-2807.0, -1844.0, 103.949)
		Execute sequence: "trigger_expolsive" - units/equipment/charge/c4_plantable/010 (-2806.0, -2280.0, 142.949)
		Execute sequence: "trigger_expolsive" - units/equipment/charge/c4_plantable/009 (-2806.0, -2690.0, 56.9487)
		´func_execute_in_other_mission_003´
		Execute sequence: "hide" - units/equipment/charge/c4_plantable/011 (-2807.0, -1844.0, 103.949) (DELAY 5)
		Execute sequence: "hide" - units/equipment/charge/c4_plantable/010 (-2806.0, -2280.0, 142.949) (DELAY 5)
		Execute sequence: "hide" - units/equipment/charge/c4_plantable/009 (-2806.0, -2690.0, 56.9487) (DELAY 5)
		Create nav link for enemies with action "None" from (-2950.0, -2875.0, -24.9979) to (-2685.06, -2629.89, -24.9991) (DELAY 5)
		Create nav link for enemies with action "None" from (-2950.0, -2525.0, -24.9979) to (-2689.9, -2219.05, -24.9991) (DELAY 5)
		Create nav link for enemies with action "None" from (-2950.0, -2100.0, -24.9979) to (-2696.97, -1795.36, -24.9991) (DELAY 5)
		´func_execute_in_other_mission_008´ (DELAY 5)
´func_execute_in_other_mission_008´ ElementExecuteInOtherMission 103884
	´env_effect_play_008´
	´env_effect_play_011´
	´func_execute_in_other_mission_009´ (DELAY 15)
´func_execute_in_other_mission_003´ ElementExecuteInOtherMission 100703
	´logic_link_cafeteria_window_happened（2）´
´func_execute_in_other_mission_009´ ElementExecuteInOtherMission 103885
	Stop effect: ´env_effect_play_009´
	Stop effect: ´env_effect_play_010´
	Stop effect: ´env_effect_play_011´
	Stop effect: ´env_effect_play_008´
´trigger_special_objective_005´ ElementSpecialObjectiveTrigger 103802
	Upon special objective event "anim_start" - ´SO_window_C4001´:
		remove ´SO_window_C4001´
		´logic_counter_005（3）´
		Execute sequence: "show" - units/equipment/charge/c4_plantable/010 (-2806.0, -2280.0, 142.949) (DELAY 0.7)
´SO_window_C4001´ ElementSpecialObjective 101502
	Create nav link for enemies with action "e_so_plant_c4_hi" from (-2906.0, -2278.0, -25.0) to (-2955.25, -2304.34, -24.9983)
´trigger_special_objective_006´ ElementSpecialObjectiveTrigger 103835
	Upon special objective event "anim_start" - ´SO_window_C4002´:
		remove ´SO_window_C4002´
		Debug message: mercedes off
		´logic_counter_005（3）´
		Execute sequence: "show" - units/equipment/charge/c4_plantable/009 (-2806.0, -2690.0, 56.9487) (DELAY 0.7)
´SO_window_C4002´ ElementSpecialObjective 103799
	Create nav link for enemies with action "e_so_plant_c4_hi" from (-2906.0, -2728.0, -25.0) to (-2909.74, -2772.38, -24.9983)
