﻿´enable´ ElementToggle 104194
	EXECUTE ON STARTUP
	Toggle on: ´trigger_area_100´
	Toggle on: ´trigger_area_098´
	Toggle on: ´trigger_area_101´
	Toggle on: ´trigger_area_099´
´trigger_area_100´ ElementAreaTrigger 103957
	DISABLED
	Box, width 170.0, height 500.0, depth 201.229, at position (2400.0, 1775.0, 50.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Toggle on: ´logic_link_184´ (DELAY 2.1)
		Toggle on: ´trigger_area_103´ (DELAY 2.1)
		Debug message: 2 in (DELAY 2.1)
		Toggle on, trigger times 1: ´lookat_002´ (DELAY 2.1)
		´logic_counter_014（2）´ (DELAY 2.1)
´trigger_area_098´ ElementAreaTrigger 101289
	DISABLED
	Box, width 142.8, height 500.0, depth 173.0, at position (1200.0, 1250.0, 50.1047) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Toggle on: ´logic_link_183´ (DELAY 2)
		Toggle on: ´trigger_area_102´ (DELAY 2)
		Debug message: 1 in (DELAY 2)
		Toggle on, trigger times 1: ´lookat_001´ (DELAY 2)
		´logic_counter_013（2）´ (DELAY 2)
´trigger_area_101´ ElementAreaTrigger 103958
	DISABLED
	Box, width 168.35, height 500.0, depth 175.0, at position (2400.0, 700.0, 50.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Toggle on: ´you_came_this_far´ (DELAY 2.1)
		Toggle on: ´trigger_area_105´ (DELAY 2.1)
		Debug message: 4 in (DELAY 2.1)
		Toggle on, trigger times 1: ´lookat_003´ (DELAY 2.1)
		´logic_counter_015（2）´ (DELAY 2.1)
´trigger_area_099´ ElementAreaTrigger 101293
	DISABLED
	Box, width 196.588, height 500.0, depth 237.35, at position (2750.0, 1250.0, 50.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Toggle on: ´logic_link_185´ (DELAY 2.1)
		Toggle on: ´trigger_area_104´ (DELAY 2.1)
		Debug message: 3 in (DELAY 2.1)
		Toggle on, trigger times 1: ´lookat_004´ (DELAY 2.1)
		´logic_counter_017（2）´ (DELAY 2.1)
´logic_link_184´ MissionScriptElement 104140
	DISABLED
	´logic_link_185´
´trigger_area_103´ ElementAreaTrigger 104151
	DISABLED
	Box, width 170.0, height 500.0, depth 201.0, at position (2400.0, 1775.0, 100.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: criminals
	Interval: 0.1
	Upon on_empty, execute:
		Toggle off: ´logic_link_184´
		Toggle off: ´trigger_area_103´
		Debug message: 2 out
		Toggle off: ´lookat_002´
		Without informing triggers, set ´logic_counter_014（2）´=2
´lookat_002´ ElementLookAtTrigger 102778
	DISABLED
	TRIGGER TIMES 1
	Upon player looking in direction (0.0, 0.0, 0.0, -1.0) from position (2400.0, 1600.0, 150.0):
		Debug message: lookat_002
		´logic_counter_014（2）´
´logic_counter_014（2）´ ElementCounter 103164
	When counter target reached: 
		Debug message: check
		´logic_link_183´ (DELAY 0.1)
´logic_link_183´ MissionScriptElement 104139
	DISABLED
	´logic_link_184´
´trigger_area_102´ ElementAreaTrigger 104150
	DISABLED
	Box, width 143.0, height 500.0, depth 173.0, at position (1200.0, 1250.0, 100.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: criminals
	Interval: 0.1
	Upon on_empty, execute:
		Toggle off: ´logic_link_183´
		Toggle off: ´trigger_area_102´
		Debug message: 1 out
		Toggle off: ´lookat_001´
		Without informing triggers, set ´logic_counter_013（2）´=2
´lookat_001´ ElementLookAtTrigger 102771
	DISABLED
	TRIGGER TIMES 1
	Upon player looking in direction (0.0, 0.0, -0.707107, -0.707107) from position (1550.0, 1250.0, 150.0):
		Debug message: lookat_001
		´logic_counter_013（2）´
´logic_counter_013（2）´ ElementCounter 102815
	When counter target reached: 
		Debug message: check
		´logic_link_183´ (DELAY 0.1)
´you_came_this_far´ ElementFilter 104143
	DISABLED
	If Death Wish, One Down and 4 players:
		´ok´
´trigger_area_105´ ElementAreaTrigger 104156
	DISABLED
	Box, width 175.0, height 500.0, depth 168.0, at position (2400.0, 700.0, 125.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: criminals
	Interval: 0.1
	Upon on_empty, execute:
		Toggle off: ´you_came_this_far´
		Toggle off: ´trigger_area_105´
		Toggle off: ´lookat_003´
		Debug message: 4 out
		Toggle off: ´lookat_003´
		Without informing triggers, set ´logic_counter_015（2）´=2
´lookat_003´ ElementLookAtTrigger 102796
	DISABLED
	TRIGGER TIMES 1
	Upon player looking in direction (0.0, 0.0, 0.707107, -0.707107) from position (1950.0, 700.0, 150.0):
		Debug message: lookat_003
		´logic_counter_015（2）´
´logic_counter_015（2）´ ElementCounter 103175
	When counter target reached: 
		Debug message: check
		´logic_link_183´ (DELAY 0.1)
´logic_link_185´ MissionScriptElement 104141
	DISABLED
	´you_came_this_far´
´trigger_area_104´ ElementAreaTrigger 104155
	DISABLED
	Box, width 197.0, height 500.0, depth 237.0, at position (2750.0, 1250.0, 125.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: criminals
	Interval: 0.1
	Upon on_empty, execute:
		Toggle off: ´logic_link_185´
		Toggle off: ´trigger_area_104´
		Debug message: 3 out
		Toggle off: ´lookat_004´
		Without informing triggers, set ´logic_counter_017（2）´=2
´lookat_004´ ElementLookAtTrigger 102845
	DISABLED
	TRIGGER TIMES 1
	Upon player looking in direction (0.707107, 0.707107, 1.19209e-07, 7.45058e-08) from position (2550.0, 1250.0, 150.0):
		´logic_counter_017（2）´
´logic_counter_017（2）´ ElementCounter 103179
	When counter target reached: 
		Debug message: check
		´logic_link_183´ (DELAY 0.1)
´ok´ MissionScriptElement 103974
	TRIGGER TIMES 1
	Debug message: all four are in
	Play audio "security_door_close" at position (3075.0, 1250.0, 225.0)
	´yes´
´yes´ MissionScriptElement 104136
	TRIGGER TIMES 1
	Play audio "c4_beep" at position (2727.0, 1456.0, 60.0)
	´point_teammate_comment_087´
	Toggle off: ´trigger_area_100´ (DELAY 2)
	Toggle off: ´trigger_area_098´ (DELAY 2)
	Toggle off: ´trigger_area_101´ (DELAY 2)
	Toggle off: ´trigger_area_099´ (DELAY 2)
´point_teammate_comment_087´ ElementTeammateComment 104351
	TRIGGER TIMES 1
	Make teammate close to instigator say: g14
´trigger_area_110´ ElementAreaTrigger 104326
	TRIGGER TIMES 4
	Box, width 730.0, height 1193.18, depth 580.0, at position (8875.0, 1325.0, 275.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		´func_experience_012´
´func_experience_012´ ElementExperience 106672
	TRIGGER TIMES 1
	Award experience: 40000
	Debug message: + 40000
	If Mayhem, Death Wish, One Down:
		Award achievement: green_6 (to all players that have been there from start)
		Debug message: func_award_achievement_green_6
´trigger_area_111´ ElementAreaTrigger 104327
	TRIGGER TIMES 1
	Box, width 500.0, height 1193.18, depth 1002.67, at position (8800.0, 1325.0, 275.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: player
	Interval: 0.1
	Upon on_enter, execute:
		Make instigator say: g24
		Make teammate close to instigator say: v21 (DELAY 2)
