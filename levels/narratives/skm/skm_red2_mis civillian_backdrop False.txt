﻿´trigger_area_087´ ElementAreaTrigger 102977
	Box, width 6450.6, height 500.0, depth 2331.73, at position (-11550.0, 3200.0, 0.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: civilians
	Interval: 1.0
	Upon on_enter, execute:
		Remove instigator from game.
´trigger_area_088´ ElementAreaTrigger 103086
	TRIGGER TIMES 1
	Box, width 500.0, height 500.0, depth 500.0, at position (-8122.0, -8376.0, -126.0) with rotation (0.0, 0.0, 0.0, -1.0)
	Amount: 1
	Instigator: civilians
	Interval: 1.0
	Upon on_enter, execute:
		Remove instigator from game.
